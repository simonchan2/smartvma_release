#!/bin/bash

requestUrl="http://"$1"/Ws/Xml/XMLremoteprn/?func=request&account="$2"&AspxAutoDetectCookieSupport=1"

echo "requestUrl: "$requestUrl >> /smartvma/PrintClientBash.log

rm -r /smartvma/tmpprint
mkdir -p /smartvma/tmpprint
xmlFilesName="fileslist.xml"
path="/smartvma/tmpprint/"$xmlFilesName

wget -O $path $requestUrl

echo "path: "$path >> /smartvma/PrintClientBash.log

itemsCount=$(/usr/local/bin/xmllint --xpath 'count(//doc)' $path)

echo "itemsCount: "$itemsCount >> /smartvma/PrintClientBash.log

for (( i=1; i <= $itemsCount; i++ )); 
do
	docUrl="$(/usr/local/bin/xmllint --xpath 'string(//doc['$i']/@url)' $path)"
	prn="$(/usr/local/bin/xmllint --xpath 'string(//doc['$i']/@prn)' $path)"
	id="$(/usr/local/bin/xmllint --xpath '//doc['$i']/text()' $path)"
	cpy="$(/usr/local/bin/xmllint --xpath 'string(//doc['$i']/@cpy)' $path)"
	fileName=${docUrl##*/}
	
	echo "Downloading file" >> /smartvma/PrintClientBash.log
	
	wget -O /smartvma/tmpprint/$fileName $docUrl
	lp -d $prn -n $cpy /smartvma/tmpprint/$timestamp"/"$fileName
	printUrl="http://"$1"/Ws/Xml/XMLremoteprn/?func=printed&account="$2"&id="$id
	
	echo "Triggering change print state in DB" >> /smartvma/PrintClientBash.log
	curl -X GET -L $printUrl
done