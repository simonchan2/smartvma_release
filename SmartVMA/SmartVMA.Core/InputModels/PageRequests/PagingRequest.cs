﻿using System.Collections.Generic;

namespace SmartVMA.Core.InputModels.PageRequests
{
    public class PagingRequest
    {
        public PagingRequest()
        {
            Length = 10;
        }

        public int Start { get; set; }

        public int Length { get; set; }

        public PagingSearch Search { get; set; }

        public IEnumerable<PagingColumn> Columns { get; set; }

        public IEnumerable<PagingColumnOrder> Order { get; set; }

        public int Draw { get; set; }
    }

    public class PagingColumn
    {
        public string Data { get; set; }

        public string Name { get; set; }
    }

    public class PagingColumnOrder
    {
        public int Column { get; set; }

        public string Dir { get; set; }
    }

    public class PagingSearch
    {
        public string Value { get; set; }
    }
}
