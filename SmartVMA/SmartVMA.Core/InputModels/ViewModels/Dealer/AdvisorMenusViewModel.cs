﻿using SmartVMA.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class AdvisorMenusViewModel
    {
        public SearchCustomerViewModel SearchCustomer { get; set; }
        public VehicleFilterViewModel VehicleLookup { get; set; }
        public ParkedMenu ParkedMenu { get; set; }
        public AcceptedMenu AcceptedMenu { get; set; }
        public bool IsParkedMenuVisible { get; set; }
    }
}
