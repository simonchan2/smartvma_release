﻿using SmartVMA.Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class MenuPresentationPrintModel : PdfDocumentViewModel
    {
        //public bool PrintSingleMenu { get; set; }
        public bool IncludePrices { get; set; }
        //public int SelectedMenuLevel { get; set; }
        public double MenuLevel1Price { get; set; }
        public double MenuLevel2Price { get; set; }
        public double MenuLevel3Price { get; set; }
        public double MenuLevel1ShopCharges { get; set; }
        public double MenuLevel2ShopCharges { get; set; }
        public double MenuLevel3ShopCharges { get; set; }
        public double MenuLevel1TaxCharges { get; set; }
        public double MenuLevel2TaxCharges { get; set; }
        public double MenuLevel3TaxCharges { get; set; }
        public double MenuLevel1TotalPrice { get; set; }
        public double MenuLevel2TotalPrice { get; set; }
        public double MenuLevel3TotalPrice { get; set; }
        public string MenuLevel1Description { get; set; }
        public string MenuLevel2Description { get; set; }
        public string MenuLevel3Description { get; set; }

        public bool ShowShopCharges { get; set; }
        public bool ShowTaxes { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "Parked")]
        public string ParkedAppointmentTime { get; set; }

        public List<MenuPresentationPrintServices> PrintServices { get; set; }
    }

    public class MenuPresentationPrintServices
    {
        public string ServiceId { get; set; }
        public bool ServiceIsStrikeOut { get; set; }
        public int ServiceMenuLevel { get; set; }
        [Display(ResourceType = typeof(Labels), Name = "Description")]
        public string ServiceDescription { get; set; }
        [Display(ResourceType = typeof(Labels), Name = "MenuPresentation_Price")]
        public double ServicePrice { get; set; }
        public double PartsPrice { get; set; }
        public double LaborPrice { get; set; }
        //[Display(ResourceType = typeof(Labels), Name = "MenuPresentation_Price")]
        //public string ServicePrice { get; set; }
        //public string ServiceLabourHours { get; set; }
        //[Display(ResourceType = typeof(Labels), Name = "MenuPresentation_IsPreviouslyServed")]
        //public bool ServiceIsPrevServ { get; set; }
        //public bool ServiceIsLpp { get; set; }
        //[Display(ResourceType = typeof(Labels), Name = "MenuPresentation_IsDeclined")]
        //public bool ServiceIsPrevDecline { get; set; }
    }
}
