﻿using System.Collections.Generic;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class PdfDocumentViewModel
    {
        public PdfDocumentViewModel()
        {
            Header = new PdfHeaderViewModel();
            Footer = new PdfFooterViewModel();
        }

        public int PdfDocumentId { get; set; }
        public long? AppointmentPresentationId { get; set; }
        public int? DealerCustomerId { get; set; }
        public string Name { get; set; }
        public string FilePathName { get; set; }
        public string VIN { get; set; }
        public string InvoiceNumber { get; set; }
        public int DocumentType { get; set; }
        public int DealerId { get; set; }
        public int? LanguageId { get; set; }

        public PdfHeaderViewModel Header { get; set; }
        public PdfFooterViewModel Footer { get; set; }



    }
}
