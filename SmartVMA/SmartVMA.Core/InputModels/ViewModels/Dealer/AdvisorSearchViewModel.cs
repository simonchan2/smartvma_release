﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class AdvisorSearchViewModel
    {
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public string CustomerPhone { get; set; }
        public string Vin { get; set; }
        public int? Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public int? CarId { get; set; }
        public DateTime? AppointmentTime { get; set; }
        public long? AppointmentPresentationId { get; set; }
        public string AppointmentPresentationStatus { get; set; }
        public bool IsLofMenu { get; set; }
        public int? DealerVehicleId { get; set; }
        public string AdvisorName { get; set; }
        public bool? CarIdUndetermined { get; set; }
    }
}
