﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class AdvisorInfo
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
