﻿using SmartVMA.Core.Enums;
using SmartVMA.Resources;
using System;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class AppointmentPresentationViewModel
    {
        public string OpCode { get; set; }
        public DateTime? ParkedDate { get; set; }
        public DateTime? AcceptedDate { get; set; }
        public CustomerInfoViewModel CustomerInfo { get; set; }
        public string AdvisorInfo { get; set; }
        public long? AdvisorId { get; set; }
        public string AdvisorCode { get; set; }
        public long? AppointmentID { get; set; }
        public long AppointmentPresentationID { get; set; }
        public int DealerID { get; set; }
        public int? DealerCustomerID { get; set; }
        public int Mileage { get; set; }
        public string RoNumber { get; set; }
        public DateTime? DateRoCreated { get; set; }
        [Display(ResourceType = typeof(Labels), Name = "InvoiceNumberText")]
        public string InvoiceNumber { get; set; }

        public int? CarId { get; set; }
        public string PresentationStatus { get; set; }
        public int? DealerVehicleId { get; set; }

        public bool IsCurrentUserOwner { get; set; }
        public bool AdvisorsHaveDeleteParkedMenuPermission { get; set; }

        public int CarIdUndetermined { get; set; }

        public bool ILifeVisibility { get; set; }

        public string ILifeColorClass { get; set; }

        public string ILifeButtonName { get; set; }

        public string Vin { get; set; }
    }
}
