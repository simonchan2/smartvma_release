﻿namespace SmartVMA.Core.InputModels.ViewModels
{
    public class LOFMenuViewModel
    {
        public long? AppointmentPresentationId { get; set; }
        public bool? IsValidPresentationMenu { get; set; }
        public CustomerInfoViewModel CustomerInfo { get; set; }
        public MenuLevelServicesIndexViewModel Services { get; set; }
    }
}
