﻿using System;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class LOFMenuImage
    {
        public Guid Id { get; set; }
        public string ImageName { get; set; }

        public string ImageUrl { get; set; }

        public bool IsSelected { get; set; }

        public int LOFMenuLevel { get; set; }
    }
}
