﻿using SmartVMA.Resources;
using System;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class PdfHeaderDealerViewModel
    {
        public byte[] LogoImgByteArray { get; set; }
        public string LogoImg { get; set; }
        [Display(Name = "DealerName", ResourceType = typeof(Labels))]
        public string CompanyName { get; set; }
        [Display(Name = "CompanyAddress", ResourceType = typeof(Labels))]
        public string CompanyAddress { get; set; }
        [Display(Name = "CompanyPhone", ResourceType = typeof(Labels))]
        public string CompanyPhone { get; set; }
        [Display(Name = "CompanyWebSite", ResourceType = typeof(Labels))]
        public string CompanyWebSite { get; set; }
    }
}
