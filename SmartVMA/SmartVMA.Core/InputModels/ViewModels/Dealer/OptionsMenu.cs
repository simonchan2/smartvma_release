﻿using SmartVMA.Core.Enums;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System;
using System.Linq;
using System.Collections.Generic;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class OptionsMenu
    {
        public OptionsMenu(long appointmentPresentationID, int? dealerCustomerID, int dealerID, int? mileage, int? carId = null, string presentationStatus = null, int? dealerVehicleId = null, string vin = null, DateTime? startDate = null, DateTime? endDate = null, int? timeFrameId = null, long? advisorOwnerId = null, List<long> advisorIds = null, int carIdUndetermined = 1, DateTime? appointmentDate = null, long? appointmentId = null)
        {
            AppointmentPresentationID = appointmentPresentationID;
            DealerCustomerID = dealerCustomerID;
            DealerID = dealerID;
            CarId = carId;
            PresentationStatus = presentationStatus;
            DealerVehicleId = dealerVehicleId;
            Vin = vin;
            ShowDeleteOption = presentationStatus == AppointmentPresentationStatus.MK.ToString();
            StartDate = startDate;
            EndDate = endDate;
            TimeFrameId = timeFrameId;
            AdvisorOwnerId = advisorOwnerId;
            var appContext = IocManager.Resolve<IAppContext>();
            IsCurrentUserOwner = AdvisorOwnerId == appContext.CurrentIdentity.UserId;
            var companySettings = IocManager.Resolve<ICompanyService>();
            AdvisorsHaveDeleteParkedMenuPermission = companySettings.GetSettingAsBool(CompanySettingsNames.AdvisorsHaveDeleteParkedMenuPermission);
            AdvisorIds = advisorIds;
            CarIdUndetermined = carIdUndetermined;
            Mileage = mileage;
            var iLifeService = IocManager.Resolve<IILifeService>();
            ILifeViewModel iLifeModel = iLifeService.ButtonInfo(vin);
            ILifeVisibility = iLifeModel != null ? iLifeModel.IsVisible : false;
            ILifeColorClass = iLifeModel != null ? iLifeModel.ColorClass : Resources.Labels.ILifeGreyColour;
            ILifeButtonName = iLifeModel != null ? iLifeModel.ButtonName : Resources.Labels.ILife;
            
            AppointmentDate = appointmentDate;

            AppointmentID = appointmentId;
        }

        public long AppointmentPresentationID { get; set; }
        public long? AppointmentID { get; set; }
        public int? DealerCustomerID { get; set; }
        public int DealerID { get; set; }
        public int? CarId { get; set; }
        public string PresentationStatus { get; set; }
        public int? DealerVehicleId { get; set; }

        public string Vin { get; set; }

        public bool ShowDeleteOption { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? TimeFrameId { get; set; }
        public long? AdvisorOwnerId { get; set; }

        public bool IsCurrentUserOwner { get; set; }
        public bool AdvisorsHaveDeleteParkedMenuPermission { get; set; }
        public List<long> AdvisorIds { get; set; }
        public int CarIdUndetermined { get; set; }
        public int? Mileage { get; set; }
        public bool ILifeVisibility { get; set; }
        public string ILifeColorClass { get; set; }

        public string ILifeButtonName { get; set; }

        public DateTime? AppointmentDate { get; set; }
    }
}
