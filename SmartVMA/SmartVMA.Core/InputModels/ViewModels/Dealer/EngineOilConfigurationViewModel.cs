﻿using SmartVMA.Resources;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class EngineOilConfigurationViewModel
    {
        public int? Id { get; set; }
        
        [Required]
        [Display(Name = "PartNumber", ResourceType = typeof(Labels))]
        public string PartNumber { get; set; }

        [Display(Name = "PartName", ResourceType = typeof(Labels))]
        public string PartName { get; set; }
        [Required]
        [Display(Name = "UnitPrice", ResourceType = typeof(Labels))]
        public decimal? UnitPrice { get; set; }
        [Required]
        [Display(Name = "OilType", ResourceType = typeof(Labels))]
        public string OilType { get; set; }
        
        [Display(Name = "OilType", ResourceType = typeof(Labels))]
        public int? OilTypeInt { get; set; }
        [Required]
        [Display(Name = "OilViscosity", ResourceType = typeof(Labels))]
        public string OilViscosity { get; set; }
        //[Required]
        [Display(Name = "OilViscosity", ResourceType = typeof(Labels))]
        public int? OilViscosityInt { get; set; }
        [Required]
        [Display(Name = "OEMOilType", ResourceType = typeof(Labels))]
        public string OEMOilType { get; set; }
       
        [Display(Name = "OEMOilType", ResourceType = typeof(Labels))]
        public int? OEMOilTypeInt { get; set; }
        [Required]
        [Display(Name = "OEMOilViscosity", ResourceType = typeof(Labels))]
        public string OEMOilViscosity { get; set; }
        
        [Display(Name = "OEMOilViscosity", ResourceType = typeof(Labels))]
        public int? OEMOilViscosityInt { get; set; }
      
        [Display(Name = "MenuLevel", ResourceType = typeof(Labels))]
        public int? MenuLevel { get; set; }
        public int? CompanyId { get; set; }
        public int EngineOilProductId { get; set; }

        public string Actions { get; set; }
    }
}
