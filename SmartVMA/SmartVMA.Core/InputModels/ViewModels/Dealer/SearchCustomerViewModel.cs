﻿using System.ComponentModel.DataAnnotations;
using SmartVMA.Resources;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class SearchCustomerViewModel
    {
        [Display(ResourceType = typeof(Labels), Name = "CustomerName")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = null, ErrorMessageResourceName = "SearchFieldMinLength3", ErrorMessageResourceType = typeof(Messages))]
        public string CustomerName { get; set; }
        [Display(ResourceType = typeof(Labels), Name = "CustomerNumber")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = null, ErrorMessageResourceName = "SearchFieldMinLength3", ErrorMessageResourceType = typeof(Messages))]
        public string CustomerNumber { get; set; }
        [Display(ResourceType = typeof(Labels), Name = "CustomerPhone")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = null, ErrorMessageResourceName = "SearchFieldMinLength3", ErrorMessageResourceType = typeof(Messages))]
        public string CustomerPhone { get; set; }
        [Display(ResourceType = typeof(Labels), Name = "VIN")]
        [StringLength(17, MinimumLength = 17, ErrorMessage = null, ErrorMessageResourceName = "VINRequiredLength", ErrorMessageResourceType = typeof(Messages))]
        public string Vin { get; set; }
        [Display(ResourceType = typeof(Labels), Name = "VehicleStockNumber")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = null, ErrorMessageResourceName = "SearchFieldMinLength3", ErrorMessageResourceType = typeof(Messages))]
        public string VehicleStockNumber { get; set; }
        [Display(ResourceType = typeof(Labels), Name = "LicensePlate")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = null, ErrorMessageResourceName = "SearchFieldMinLength3", ErrorMessageResourceType = typeof(Messages))]
        public string LicensePlate { get; set; }
    }
}