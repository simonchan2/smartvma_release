﻿using System;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class AppointmentDetailsViewModel
    {
        public string OpType { get; set; }
        public string OpDescription { get; set; }
        public string OpCode { get; set; }
        public bool InStock { get; set; }
    }
}