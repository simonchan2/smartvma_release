﻿using SmartVMA.Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class CustomerInfoForServicesViewModel
    {
        public CustomerInfoForServicesViewModel()
        {
            CustomerInfo = new CustomerInfoViewModel();
            VehicleFilterModel = new VehicleFilterViewModel();
        }
        public CustomerInfoViewModel CustomerInfo { get; set; }
        public VehicleFilterViewModel VehicleFilterModel { get; set; }
    }
}
