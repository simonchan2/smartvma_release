﻿namespace SmartVMA.Core.InputModels.ViewModels
{
    public class VehicleFilterItemsViewModel
    {
        public int? Id { get; set; }

        public string Text { get; set; }

        public int EntityType  { get; set; }
    }
}
