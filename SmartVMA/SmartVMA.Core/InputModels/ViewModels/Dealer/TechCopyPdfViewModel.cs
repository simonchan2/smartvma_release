﻿using SmartVMA.Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class TechCopyPdfViewModel : PdfDocumentViewModel
    {
        public TechCopyPdfViewModel()
        {
            Services = new List<TechCopyPdfService>();

        }
        public string Preferred { get; set; }
        public List<TechCopyPdfService> Services { get; set; }

        public string TotalHours { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }

    }

    public class TechCopyPdfService
    {
        public long AppointmentServicesId { get; set; }
        [Display(Name = "OpCodePDF", ResourceType = typeof(Labels))]
        public string OpCode { get; set; }
        [Display(Name = "OpDescriptionPDF", ResourceType = typeof(Labels))]
        public string OpDescription { get; set; }
        [Display(Name = "PartNamePDF", ResourceType = typeof(Labels))]
        public string PartNo { get; set; }
        [DisplayFormat(DataFormatString = "{0:F1}")]
        [Display(Name = "QuantityPDF", ResourceType = typeof(Labels))]
        public double Quantity { get; set; }
        [Display(Name = "LaborHourPDF", ResourceType = typeof(Labels))]
        public double? LaborHour { get; set; }

        public bool? IsAdditional { get; set; }
        public bool IsDeclined { get; set; }
    }
}
