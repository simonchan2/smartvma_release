﻿using SmartVMA.Resources;
using System;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class PdfHeaderAdvisorViewModel
    {
        [Display(Name = "AdvisorName", ResourceType = typeof(Labels))]
        public string AdvisorName { get; set; }
        [Display(Name = "AdvisorCode", ResourceType = typeof(Labels))]
        public string AdvisorCode { get; set; }
        [Display(Name = "AdvisorEmail", ResourceType = typeof(Labels))]
        public string AdvisorEmail { get; set; }
        [Display(Name = "AdvisorPhoneNumber", ResourceType = typeof(Labels))]
        public string AdvisorPhoneNumber { get; set; }
    }
}
