﻿using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class EngineOilTypeViewModel
    {
        public int Id { get; set; }

        [Required]
        public string OilType { get; set; }

        [Required]
        public int SortOrder { get; set; }

    }
}
