﻿namespace SmartVMA.Core.InputModels.ViewModels
{
    public class PdfFooterViewModel
    {
        public string LogoImg { get; set; }
        public string From { get; set; }

        public string Printed { get; set; }

        //public string Accepted { get; set; }

        //public string ParentView { get; set; }

        //public int CurrentPage { get; set; }
        //public int TotalPage { get; set; }
    }
}
