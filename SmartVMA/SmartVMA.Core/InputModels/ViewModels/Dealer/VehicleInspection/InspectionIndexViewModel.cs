﻿using SmartVMA.Core.Contracts;
using System.Collections.Generic;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class InspectionIndexViewModel : BaseIndexViewModel<InspectionViewModel>
    {
        public InspectionIndexViewModel()
        {
            DamageIndicationTypes = new List<DamageIndicationTypeViewModel>();
            ViewPoints = new List<ViewPointViewModel>();
            Inspection = new InspectionViewModel();
        }
        public InspectionViewModel Inspection { get; set; }
        public List<ViewPointViewModel> ViewPoints { get; set; }
        public List<DamageIndicationTypeViewModel> DamageIndicationTypes { get; set; }
    }
}
