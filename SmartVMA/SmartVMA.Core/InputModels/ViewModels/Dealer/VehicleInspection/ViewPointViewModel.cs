﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace SmartVMA.Core.InputModels.ViewModels
{
    public class ViewPointViewModel
    {
        public ViewPointViewModel()
        {
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public int Position { get; set; }
    }
}