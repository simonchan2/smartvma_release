﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace SmartVMA.Core.InputModels.ViewModels
{
    [Serializable]
    public class DamageIndicationViewModel
    {
        public long? Id { get; set; }
        public int TypeId { get; set; }
        public string Comment { get; set; }
        public decimal OffsetLeft { get; set; }
        public decimal OffsetTop { get; set; }
        public int ViewPointId { get; set; }
        public long InspectionId { get; set; }
    }
}