﻿using SmartVMA.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class DamageIndicationJsonViewModel
    {
        public List<DamageIndicationViewModel> CreatedIndications { get; set; }
        public List<DamageIndicationViewModel> DeletedIndications { get; set; }
    }
}
