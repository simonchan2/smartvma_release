﻿using SmartVMA.Core.Enums;
using SmartVMA.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class InspectionViewModel
    {
        public InspectionViewModel()
        {
            VehiclePhotos = new List<VehiclePhotoViewModel>();
            DamageIndications = new List<DamageIndicationViewModel>();
            DamageIndicationTypes = new List<DamageIndicationTypeViewModel>();
            ViewPoints = new List<ViewPointViewModel>();
            CustomerInfo = new CustomerInfoViewModel();
            DamageIndicationJsonViewModel = new DamageIndicationJsonViewModel();
            TireTypes = new List<string>();
            TireTypes.Add(Labels.TireType1);
            TireTypes.Add(Labels.TireType2);
            TireTypes.Add(Labels.TireType3);
        }

        public long? Id { get; set; }
        [Display(Name = "RimScratch", ResourceType = typeof(Labels))]
        public bool LFRimScratch { get; set; }
        [Display(Name = "RimScratch", ResourceType = typeof(Labels))]
        public bool LRRimScratch { get; set; }
        [Display(Name = "RimScratch", ResourceType = typeof(Labels))]
        public bool RFRimScratch { get; set; }
        [Display(Name = "RimScratch", ResourceType = typeof(Labels))]
        public bool RRRimScratch { get; set; }
        public int? LFTireTypeId { get; set; }
        public int? LRTireTypeId { get; set; }
        public int? RFTireTypeId { get; set; }
        public int? RRTireTypeId { get; set; }
        [Display(Name = "Fuel", ResourceType = typeof(Labels))]
        public int? Fuel { get; set; }
        public long? AppointmentPresentationId { get; set; }
        public int TenantId { get; set; }
        public List<VehiclePhotoViewModel> VehiclePhotos { get; set; }
        public List<DamageIndicationViewModel> DamageIndications { get; set; }
        public List<ViewPointViewModel> ViewPoints { get; set; }
        public List<DamageIndicationTypeViewModel> DamageIndicationTypes { get; set; }
        public CustomerInfoViewModel CustomerInfo { get; set; }
        public List<string> TireTypes { get; set; }
        public string DamageIndicationJson { get; set; }
        public DamageIndicationJsonViewModel DamageIndicationJsonViewModel { get; set; }
        public bool Reset { get; set; }
        public string VehicleImageName { get; set; }
        public int? Transmission { get; set; }
        public int? Driveline { get; set; }
        public int? EnteredMileage { get; set; }
        public int CarIdUndetermined { get; set; }
        public string AppointmentPresentationStatus { get; set; }
    }
}
