﻿using System.Collections.Generic;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class PdfCustomerCopyViewModel
    {
        //TODO -> get company information either

        public AppointmentPresentationViewModel AppointmentPresentation { get; set; }
        public AppointmentViewModel Appointment { get; set; }
        public List<AppointmentDetailsViewModel> AppointmentDetails { get; set; }
        public CompanyViewModel Company { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string CompanyLogoPath { get; set; }
    }
}
