﻿using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class FluidViscosityViewModel
    {
        public int Id { get; set; }

        [Required]
        [StringLength(25)]
        public string Viscosity { get; set; }

        [Required]
        public bool InDealerOilViscosityList { get; set; }
    }
}
