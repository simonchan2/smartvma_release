﻿using System.ComponentModel.DataAnnotations;
using SmartVMA.Core.Utils;
using SmartVMA.Resources;
using System.Collections.Generic;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class ConfigurationLOFMenuViewModel
    {
        public int Id { get; set; }

        public List<LOFMenuImage> Images { get; set; }

        public string LOFMenuLevel1Images { get; set; }

        public string LOFMenuLevel2Images { get; set; }

        public string LOFMenuLevel3Images { get; set; }
        [Display(Name = "SelectLOFMenuLevel", ResourceType = typeof(Labels))]
        public int? LOFMenuSelectedLevel { get; set; }

    }
}
