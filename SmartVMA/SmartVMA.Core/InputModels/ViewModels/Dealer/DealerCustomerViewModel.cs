﻿using SmartVMA.Resources;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class DealerCustomerViewModel
    {
        public int? Id { get; set; }
        
        [Display(ResourceType = typeof(Labels), Name = "FirstName")]
        public string FirstName { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "LastName")]
        public string LastName { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "CompanyName")]
        public string CompanyName { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "AddCustomerInfo_Address")]
        public string Address { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "HomePhone")]
        public string HomePhone { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "WorkPhone")]
        public string WorkPhone { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "CellPhone")]
        public string MobilePhone { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "AddCustomerInfo_Email")]
        [EmailAddress(ErrorMessage = null, ErrorMessageResourceName = "NotValidEmail", ErrorMessageResourceType = typeof(Messages))]
        public string Email { get; set; }

        [Required]
        [Display(ResourceType = typeof(Labels), Name = "VIN")]
        [StringLength(17, MinimumLength = 17, ErrorMessage = null, ErrorMessageResourceName = "VINRequiredLength", ErrorMessageResourceType = typeof(Messages))]
        public string VIN { get; set; }

        public string Transmission { get; set; }

        public string Driveline { get; set; }

        public SaveConfirmServiceRequestModel ConfirmServicesRequest { get; set; }        
    }
}
