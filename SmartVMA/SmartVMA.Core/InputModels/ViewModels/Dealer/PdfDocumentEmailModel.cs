﻿using SmartVMA.Resources;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class PdfDocumentEmailModel
    {
        public int DealerId { get; set; }
        public long AppointmentPresentationId { get; set; }
        public int? DealerCustomerId { get; set; }
        public int DocumentType { get; set; }
        [Display(Name = "MessageSubject", ResourceType = typeof(Labels))]
        public string MessageSubject { get; set; }
        [Display(Name = "MessageBody", ResourceType = typeof(Labels))]
        [AllowHtml]
        public string MessageBody { get; set; }
        [Display(Name = "MessageTo", ResourceType = typeof(Labels))]
        [Required]
        public string MessageTo { get; set; }
        [Display(Name = "MessageFrom", ResourceType = typeof(Labels))]
        [Required]
        public string MessageFrom { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
    }
}
