﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SmartVMA.Resources;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class PartsCopyPdfViewModel : PdfDocumentViewModel
    {
        public PartsCopyPdfViewModel()
        {
            Services = new List<PartsCopyPdfService>();

        }
        public string Preferred { get; set; }
        public List<PartsCopyPdfService> Services { get; set; }
        [DisplayFormat(DataFormatString = "{0:F2}")]
        public double TotalERL { get; set; }
        [DisplayFormat(DataFormatString = "{0:F2}")]
        public double ModifiedTotalERL { get; set; }
        [DisplayFormat(DataFormatString = "{0:F2}")]
        public double TotalHours { get; set; }
        [DisplayFormat(DataFormatString = "{0:F2}")]
        public double ModfiedTotalHours { get; set; }

        [DisplayFormat(DataFormatString = "{0:F2}")]
        public double TotalQTY { get; set; }
        [DisplayFormat(DataFormatString = "{0:F2}")]
        public double ModifiedTotalQTY { get; set; }
        [DisplayFormat(DataFormatString = "{0:F2}")]
        public decimal TotalParts { get; set; }
        [DisplayFormat(DataFormatString = "{0:F2}")]
        public decimal ModifiedTotalParts { get; set; }
        [DisplayFormat(DataFormatString = "{0:F2}")]
        public decimal TotalLabor { get; set; }
        [DisplayFormat(DataFormatString = "{0:F2}")]
        public decimal ModifiedTotalLabor { get; set; }
        [DisplayFormat(DataFormatString = "{0:F2}")]
        public decimal Total { get; set; }
        [DisplayFormat(DataFormatString = "{0:F2}")]
        public decimal ModifiedTotal { get; set; }

        public string Title { get; set; }
        public string SubTitle { get; set; }        
    }

    public class PartsCopyPdfService
    {
        public long AppointmentServicesId { get; set; }
        [Display(Name = "OpCodePDF", ResourceType = typeof(Labels))]
        public string OpCode { get; set; }
        [Display(Name = "OpDescriptionPDF", ResourceType = typeof(Labels))]
        public string OpDescription { get; set; }
        [Display(Name = "PartNamePDF", ResourceType = typeof(Labels))]
        public string PartName { get; set; }

        [DisplayFormat(DataFormatString = "{0:F1}")]
        [Display(Name = "QuantityPDF", ResourceType = typeof(Labels))]
        public double Quantity { get; set; }

        [DisplayFormat(DataFormatString = "{0:F2}")]
        [Display(Name = "LaborHourPDF", ResourceType = typeof(Labels))]
        public double LaborHour { get; set; }

        [Display(Name = "LaborHour", ResourceType = typeof(Labels))]
        [DisplayFormat(DataFormatString = "{0:F2}")]
        public double ServiceLaborHour { get; set; }

        [DisplayFormat(DataFormatString = "{0:F2}")]
        [Display(Name = "PartsPricePDF", ResourceType = typeof(Labels))]
        public decimal PartsPrice { get; set; }

        [DisplayFormat(DataFormatString = "{0:F2}")]
        [Display(Name = "LaborPricePDF", ResourceType = typeof(Labels))]
        public decimal LaborPrice { get; set; }

        [DisplayFormat(DataFormatString = "{0:F2}")]
        [Display(Name = "PricePDF", ResourceType = typeof(Labels))]
        public decimal Price { get; set; }

        public bool IsService { get; set; }

        public bool IsAdditional { get; set; }
        public bool IsDeclined { get; set; }
    }

}
