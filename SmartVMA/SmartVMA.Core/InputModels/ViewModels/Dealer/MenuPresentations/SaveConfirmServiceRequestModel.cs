﻿using SmartVMA.Core.Enums;
using SmartVMA.Core.Utils;
using System.Collections.Generic;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class SaveConfirmServiceRequestModel
    {
        //[RequiredIfSetting(Setting = CompanySettingsNames.DisplayInvoiceNumber)]
        public string Invoice { get; set; }
        public bool IsInvoiceRequired { get; set; }
        public int CarId { get; set; }
        public int? Mileage { get; set; }
        public int? EnteredMileage { get; set; }
        public int? DealerCustomerId { get; set; }
        public long? AppointmentPresentationId { get; set; }
        public string VIN { get; set; }

        public bool IsAccepted { get; set; }
        public bool IsDeclined { get; set; }
        public bool IsParked { get; set; }
        public bool IsLof { get; set; }

        public int Transmission { get; set; }
        public int Driveline { get; set; }
        public double? TaxChargesAmount { get; set; }
        public double? ShopChargesAmount { get; set; }
        public byte MenuLevel { get; set; }
        public IEnumerable<ConfirmServiceRequestItemsModel> Services { get; set; }
        public string MenuPackageOpCode { get; set; }

        public long InspectionId { get; set; }

        public IEnumerable<ConfirmServiceRequestItemsModel> AllServices { get; set; }
    }
}