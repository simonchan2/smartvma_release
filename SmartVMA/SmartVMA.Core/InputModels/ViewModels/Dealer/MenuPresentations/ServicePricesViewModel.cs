﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class ServicePricesViewModel
    {
        public double Price { get; set; }

        public double LaborPrice { get; set; }

        public double PartsPrice { get; set; }

        public double PartsPriceShopChargeAmount { get; set; }

        public double LaborPriceShopChargeAmount { get; set; }

        public double PartsPriceTaxAmount { get; set; }

        public double LaborPriceTaxAmount { get; set; }

        public double GrossPrice { get; set; }

        public double ShopChargesAmount => Math.Round(PartsPriceShopChargeAmount + LaborPriceShopChargeAmount, 2);
        public double TaxChargesAmount => Math.Round(PartsPriceTaxAmount + LaborPriceTaxAmount, 2);
    }
}
