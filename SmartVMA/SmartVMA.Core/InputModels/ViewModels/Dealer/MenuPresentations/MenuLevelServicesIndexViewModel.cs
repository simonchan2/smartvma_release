﻿using System.Collections.Generic;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class MenuLevelServicesIndexViewModel
    {
        public IEnumerable<ServiceDetailViewModel> MenuLevel1Services { get; set; }

        public IEnumerable<ServiceDetailViewModel> MenuLevel2Services { get; set; }

        public IEnumerable<ServiceDetailViewModel> MenuLevel3Services { get; set; }

        /*-- SMART-210
        public IEnumerable<ServiceDetailViewModel> JustMenuLevel1Services { get; set; }

        public IEnumerable<ServiceDetailViewModel> JustMenuLevel2Services { get; set; }

        public IEnumerable<ServiceDetailViewModel> JustMenuLevel3Services { get; set; }
        */


        public string MenuLabelLevel1 { get; set; }

        public string MenuLabelLevel2 { get; set; }

        public string MenuLabelLevel3 { get; set; }


        public double? MenuLabelLevel1Price { get; set; }

        public double? MenuLabelLevel2Price { get; set; }

        public double? MenuLabelLevel3Price { get; set; }

        public string MenuPackageOpCodeLevel1 { get; set; }

        public string MenuPackageOpCodeLevel2 { get; set; }

        public string MenuPackageOpCodeLevel3 { get; set; }

        public byte SelectedMenuLevel { get; set; }

        public List<string> MenuImagesLevel1 { get; set; }

        public List<string> MenuImagesLevel2 { get; set; }

        public List<string> MenuImagesLevel3 { get; set; }
    }
}