﻿using SmartVMA.Resources;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class ServiceDetailViewModel
    {
        public long Id { get; set; }

        [Display(Name = "MenuPresentation_ServiceName", ResourceType = typeof(Labels))]
        public string Description { get; set; }

        [Display(Name = "MenuPresentation_IsDeclined", ResourceType = typeof(Labels))]
        public bool IsDeclined { get; set; }

        [Display(Name = "MenuPresentation_IsPreviouslyServed", ResourceType = typeof(Labels))]
        public bool IsPreviouslyServed { get; set; }

        [Display(Name = "MenuPresentation_Video", ResourceType = typeof(Labels))]
        public string VideoCode { get; set; }
        public string ProductImageUrl { get; set; }

        public int? BgProtectionPlanId { get; set; }

        [Display(Name = "LPP", ResourceType = typeof(Labels))]
        public string BgProtectionPlanLink { get; set; }

        public string OpCode { get; set; }

        public bool CanBeStriked { get; set; }
        public bool IsStrikeOut { get; set; }
        public int CarId { get; set; }
        public bool IsFluid { get; set; }
        public bool IsEngineOil { get; set; }
        public bool IsLOF { get; set; }


        [Display(Name = "MenuPresentation_Price", ResourceType = typeof(Labels))]
        public double? Price { get; set; }
        public double? LaborHours { get; set; }
        public double? PartsPrice { get; set; }
        public double? LaborPrice { get; set; }
        public double LaborRate { get; set; }

        public double? GrossPrice { get; set; }

        public int? Mileage { get; set; }
        public int MenuLevel { get; set; }
        public int ServiceTypeId { get; set; }
        public string Option { get; set; }

        public double ShopChargesPartsPrice { get; set; }

        public double ShopChargesLaborPrice { get; set; }

        public double TaxIncludedPartsPrice { get; set; }

        public double TaxIncludedLaborPrice { get; set; }

        public bool? IsAddedAfter { get; set; }

        public int? MaxMileageBeforeFirstService { get; set; }

        public int? ServiceInterval { get; set; }

        public Enums.LPPColour LppColour { get; set; }

        public bool? IsAdditionalAddedAfter { get; set; }

        public int Counter { get; set; }

        public bool IsAddedNow { get; set; }

        public string PreviousMileage { get; set; }

        public string PreviousServedDate { get; set; }
    }
}