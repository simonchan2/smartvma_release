﻿using System;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class ServiceHistoryViewModel
    {
        public DateTime Date { get; set; }
        public string InvoiceNumber { get; set; }
        public int Mileage { get; set; }
        public string OpCode { get; set; }
        public string Description { get; set; }
    }
}