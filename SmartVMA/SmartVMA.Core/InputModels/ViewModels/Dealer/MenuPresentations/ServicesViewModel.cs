﻿using System.ComponentModel.DataAnnotations;
using SmartVMA.Infrastructure;
using SmartVMA.Resources;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class ServicesViewModel
    {
        public string CustomerCompanyName { get; set; }
        [RegularExpression(RegExp.PhoneNumber, ErrorMessage = null, ErrorMessageResourceName = "InvalidPhoneNumber", ErrorMessageResourceType = typeof(Messages))]
        public string CustomerHomePhoneNumber { get; set; }
        [RegularExpression(RegExp.PhoneNumber, ErrorMessage = null, ErrorMessageResourceName = "InvalidPhoneNumber", ErrorMessageResourceType = typeof(Messages))]
        public string CustomerWorkPhoneNumber { get; set; }
        public string CustomerMobilePhoneNumber { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerEmail { get; set; }
        [StringLength(17, ErrorMessage = "The Vine number value cannot exceed 17 characters. ")]
        public string VIN { get; set; }
        public int Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Engine { get; set; }
        public string Transmission { get; set; }
        public string DriveLine { get; set; }
        public int Mileage { get; set; }
    }
}