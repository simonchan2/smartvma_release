﻿using SmartVMA.Core.Contracts;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class ConfirmServiceRequestItemsModel : ServiceIdsRequestModel
    {
        public bool IsStrikeOut { get; set; }

        public int MenuLevel { get; set; }

        public double AppPrice { get; set; }

        public string AppDescription { get; set; }

        public double AppLaborHours { get; set; }

        public bool? IsAddedAfter { get; set; }

        public int Counter { get; set; }

        public bool IsAddedNow { get; set; }

        public string OpCode { get; set; }

        public double PartsPrice { get; set; }

        public double LaborPrice { get; set; }

        public double LaborRate { get; set; }
    }
}
