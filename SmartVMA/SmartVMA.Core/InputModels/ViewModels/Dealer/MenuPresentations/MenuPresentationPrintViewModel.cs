﻿namespace SmartVMA.Core.InputModels.ViewModels
{
    public class MenuPresentationPrintViewModel
    {
        public MenuPresentationPrintViewModel(bool printSingleMenu, bool includePrices, int selectedMenuLevel)
        {
            ServicePresentationViewModel = new ServiceBasePresentationViewModel();
            PrintSingleMenu = printSingleMenu;
            IncludePrices = includePrices;
            SelectedMenuLevel = selectedMenuLevel;
        }
        public ServiceBasePresentationViewModel ServicePresentationViewModel { get; set; }

        public bool PrintSingleMenu { get; set; }
        public bool IncludePrices { get; set; }
        public int SelectedMenuLevel { get; set; }

        public string AdvisorName { get; set; }
        public string AdvisorFamily { get; set; }
        public string AdvisorNumber { get; set; }
        public string AdvisorPhoneNumber { get; set; }
        public string AdvisorEmail { get; set; }
    }
}