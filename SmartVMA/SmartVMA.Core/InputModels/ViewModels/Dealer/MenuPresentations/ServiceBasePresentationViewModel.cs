﻿using System;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class ServiceBasePresentationViewModel
    {
        public ServiceBasePresentationViewModel()
        {
            // default values
            CustomerInfo = new CustomerInfoForServicesViewModel();
            Services = new MenuLevelServicesIndexViewModel();
        }

        public long? AppointmentPresentationId { get; set; }

        public long? InspectionId { get; set; }

        public bool? IsValidPresentationMenu { get; set; }

        public int? MenuCarId { get; set; }

        public int? MenuMileage { get; set; }

        public int? EnteredMileage { get; set; }

        public CustomerInfoForServicesViewModel CustomerInfo { get; set; }

        public PreviewAdditionalServicesIndexViewModel AdditionalServices { get; set; }

        public MenuLevelServicesIndexViewModel Services { get; set; }
        public string AppointmentPresentationStatus { get; internal set; }
        public string ParkedAppointmentTime { get; internal set; }
        public string Invoice { get; set; }

        public int? Transmission { get; set; }

        public int? Driveline { get; set; }

        public bool isVisiblePrice { get; set; }

        public bool AreConflictsResolved { get; set; }

        public int CarIdUndetermined { get; set; }

        public bool ILifeVisibility { get; set; }

        public string ILifeColorClass { get; set; }

        public string ILifeButtonName { get; set; }

        public bool IsDmsDealer { get; set; }

    }
}