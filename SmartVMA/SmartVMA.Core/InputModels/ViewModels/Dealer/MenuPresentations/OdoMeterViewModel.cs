﻿using SmartVMA.Core.Utils;
using SmartVMA.Resources;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class OdoMeterViewModel
    {
        [Required]
        [RegularExpression("([1-9][0-9]*)", ErrorMessageResourceName = "OdometerMileageMinMaxValue", ErrorMessageResourceType = typeof(Messages))]
        [Range(1, 1000000, ErrorMessageResourceName = "OdometerMileageMinMaxValue", ErrorMessageResourceType = typeof(Messages))]
        public int? Mileage { get; set; }
        public int CarId { get; set; }
        public int? EnteredMileage { get; set; }
    }
}
