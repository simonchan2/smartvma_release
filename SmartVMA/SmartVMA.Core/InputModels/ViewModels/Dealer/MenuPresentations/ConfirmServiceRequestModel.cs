﻿using System.Collections.Generic;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class ConfirmServiceRequestModel
    {
        public int CarId { get; set; }

        public int? Mileage { get; set; }

        public int? EnteredMileage { get; set; }

        public long? AppointmentPresentationId { get; set; }

        public int? DealerCustomerId { get; set; }

        public string VIN { get; set; }

        public string Invoice { get; set; }

        public int Transmission { get; set; }

        public int Driveline { get; set; }

        public byte MenuLevel { get; set; }

        public IEnumerable<ConfirmServiceRequestItemsModel> Services { get; set; }

        public string MenuPackageOpCode { get; set; }

        public long InspectionId { get; set; }

        public bool IsLof { get; set; }

        public IEnumerable<ConfirmServiceRequestItemsModel> AllServices { get; set; }
    }
}
