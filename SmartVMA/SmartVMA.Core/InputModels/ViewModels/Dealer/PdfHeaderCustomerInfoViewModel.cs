﻿using SmartVMA.Resources;
using System;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class PdfHeaderCustomerInfoViewModel
    {
        [Display(Name = "CustomerName", ResourceType = typeof(Labels))]
        public string CustomerName { get; set; }
        [Display(Name = "CustomerAddress", ResourceType = typeof(Labels))]
        public string CustomerAddress { get; set; }
        [Display(Name = "CustomerHomePhone", ResourceType = typeof(Labels))]
        public string CustomerHomePhone { get; set; }
        [Display(Name = "CustomerWorkPhone", ResourceType = typeof(Labels))]
        public string CustomerWorkPhone { get; set; }
        [Display(Name = "CustomerMobilePhone", ResourceType = typeof(Labels))]
        public string CustomerMobilePhone { get; set; }
        [Display(Name = "VehicleLicenseNumber", ResourceType = typeof(Labels))]
        public string VehicleLicenseNumber { get; set; }
        [Display(Name = "VehicleMake", ResourceType = typeof(Labels))]
        public string VehicleMake { get; set; }
        [Display(Name = "VehicleYear", ResourceType = typeof(Labels))]
        public string VehicleYear { get; set; }
        [Display(Name = "VehicleModel", ResourceType = typeof(Labels))]
        public string VehicleModel { get; set; }
        [Display(Name = "VehicleEngine", ResourceType = typeof(Labels))]
        public string VehicleEngine { get; set; }
        [Display(Name = "VehicleColor", ResourceType = typeof(Labels))]
        public string VehicleColor { get; set; }
        [Display(Name = "VIN", ResourceType = typeof(Labels))]
        public string VIN { get; set; }
        [Display(Name = "OdometerReading", ResourceType = typeof(Labels))]
        public string Odometer { get; set; }
        [Display(Name = "OdometerReading", ResourceType = typeof(Labels))]
        public string OdometerReading { get; set; }
        //empty for Phase 1
        [Display(Name = "Waiter", ResourceType = typeof(Labels))]
        public string Waiter { get; set; }
        //empty for Phase 1
        [Display(Name = "TagNumber", ResourceType = typeof(Labels))]
        public string TagNumber { get; set; }
        [Display(Name = "Date", ResourceType = typeof(Labels))]
        public DateTime Date { get; set; }
        [Display(Name = "Date", ResourceType = typeof(Labels))]
        public string DateString { get; set; }
        [Display(Name = "InvoiceNumberText", ResourceType = typeof(Labels))]
        public string InvoiceNumberText { get; set; }

        public byte MenuLevel { get; set; }
        public bool? IsLofMenu { get; set; }
        public bool ShowEngine { get; set; }
        public bool ShowVehicleHistoryCustomerInfo { get; set; }
    }
}
