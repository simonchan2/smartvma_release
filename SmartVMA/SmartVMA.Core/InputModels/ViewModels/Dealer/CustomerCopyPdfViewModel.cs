﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class CustomerCopyPdfViewModel : PdfDocumentViewModel
    {
        public CustomerCopyPdfViewModel()
        {
            Services = new List<CustomerCopyPdfService>();
        }
        public List<CustomerCopyPdfService> Services { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public int SelectedMenuLevel { get; set; }

        public string Disclaimer { get; set; }
        public string MenuStatus { get; set; }

        /// <summary>
        /// Price without shop and tax charges
        /// </summary>
        public double TotalPrice { get; set; }
        public double TotalShopChargesAmount { get; set; }
        public double TotalTaxChargesAmount { get; set; }
        public double GrandTotalPrice { get; set; }        
        public bool ShowTaxes { get; set; }
        public bool ShowShopCharges { get; set; }

        public bool IncludePrices { get; set; }
    }

    public class CustomerCopyPdfService
    {
        //public string OpCode { get; set; }
        public string ServiceDescription { get; set; }
        public double ServicePrice { get; set; }
        public double LaborPrice { get; set; }
        public double PartsPrice { get; set; }
        public bool IsAdditional { get; set; }
        public bool IsDeclined { get; set; }
    }
}
