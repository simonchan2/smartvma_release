﻿using System;
using System.Collections.Generic;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class AppointmentViewModel
    {
        public AppointmentViewModel()
        {
            // default values
            CustomerInfo = new CustomerInfoViewModel();
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public string CellPhone { get; set; }
        public int? Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public int Mileage { get; set; }
        public string VIN { get; set; }
        public string Engine { get; set; }
        public string DriveLine { get; set; }
        public string Transmission { get; set; }

        public string VehicleLicense { get; set; }
        public DateTime? AppointmentTime { get; set; }
        public int DealerId { get; set; }
        public int DealerCustomerId { get; set; }
        public string PresentationStatus { get; set; }
        public string AppointmentStatus { get; set; }
        public long? AdvisorId { get; set; }
        public string AdvisorName { get; set; }
        public string AdvisorFamily { get; set; }
        public string AdvisorCode { get; set; }
        public string StockNo { get; set; }
        public CustomerInfoViewModel CustomerInfo { get; set; }

        public long AppointmentPresentationId { get; set; }
        public long AppointmentId { get; set; }

        public string InvoiceNumber { get; set; }

        public string RoNumber { get; set; }

        public DateTime start { get; set; }

        public string startHHmm { get; set; }

        public List<AppointmentDetailsViewModel> AppointmentDetailsList { get; set; }

        public int? CarId { get; set; }
        public int? DealerVehicleId { get; set; }

        public bool? CarIdUndetermined { get; set; }
    }
}