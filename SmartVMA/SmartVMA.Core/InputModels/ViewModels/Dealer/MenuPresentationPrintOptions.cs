﻿using SmartVMA.Resources;
using System;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class MenuPresentationPrintOptions
    {
        public MenuPresentationPrintOptions(bool printSingleMenu, bool includePrices, int selectedMenuLevel, int? carId, int? mileage, int? customerId, long? appointmentPresentationId, long? inspectionId, int? transmission, int? driveline)
        {
            PrintSingleMenu = printSingleMenu;
            IncludePrices = includePrices;
            SelectedMenuLevel = selectedMenuLevel;
            CarId = carId;
            Mileage = mileage;
            CustomerId = customerId;
            AppointmentPresentationId = appointmentPresentationId;
            InspectionId = inspectionId;
            Transmission = transmission;
            Driveline = driveline;
        }

        [Display(ResourceType = typeof(Labels), Name = "PrintSingleMenu")]
        public bool PrintSingleMenu { get; set; }
        [Display(ResourceType = typeof(Labels), Name = "IncludePrices")]
        public bool IncludePrices { get; set; }
        public int SelectedMenuLevel { get; set; }

        public int? CarId { get; set; }
        public int? Mileage { get; set; }
        public int? CustomerId { get; set; }
        public long? AppointmentPresentationId { get; set; }
        public string ParkedAppointmentTime { get; set; }
        public long? InspectionId { get; set; }
        public int? Transmission { get; set; }
        public int? Driveline { get; set; }
    }
}
