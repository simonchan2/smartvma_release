﻿using System.ComponentModel.DataAnnotations;
using SmartVMA.Resources;
using System;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class SearchDocumentsViewModel
    {
        [Display(ResourceType = typeof(Labels), Name = "VIN")]
        [StringLength(17, MinimumLength = 17)]
        public string VIN { get; set; }
        [Display(ResourceType = typeof(Labels), Name = "InvoiceNumber")]
        [StringLength(int.MaxValue, MinimumLength = 3)]
        public string InvoiceNumber { get; set; }
        [Display(ResourceType = typeof(Labels), Name = "AppointmentPresentationStartDate")]
        public DateTime? StartDate { get; set; }
        [Display(ResourceType = typeof(Labels), Name = "AppointmentPresentationEndDate")]
        public DateTime? EndDate { get; set; }
        [Display(ResourceType = typeof(Labels), Name = "AppointmentPresentationStartDate")]
        public string StartDateString { get; set; }
        [Display(ResourceType = typeof(Labels), Name = "AppointmentPresentationEndDate")]
        public string EndDateString { get; set; }
    }
}