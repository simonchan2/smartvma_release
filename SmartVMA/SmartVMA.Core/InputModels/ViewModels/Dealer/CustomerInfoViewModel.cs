﻿using SmartVMA.Resources;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class CustomerInfoViewModel
    {
        public int? Id { get; set; }
        
        [Display(Name = "CustomerInfo_FullName", ResourceType = typeof(Labels))]
        public string FullName { get; set; }

        [Display(Name = "CustomerInfo_FirstName", ResourceType = typeof(Labels))]
        public string FirstName { get; set; }

        [Display(Name = "CustomerInfo_LastName", ResourceType = typeof(Labels))]
        public string LastName { get; set; }

        [Display(Name = "CustomerInfo_Email", ResourceType = typeof(Labels))]
        public string Email { get; set; }

        [Display(Name = "CustomerInfo_Address", ResourceType = typeof(Labels))]
        public string Address { get; set; }

        [Display(Name = "CustomerInfo_City", ResourceType = typeof(Labels))]
        public string City { get; set; }

        [Display(Name = "CustomerInfo_State", ResourceType = typeof(Labels))]
        public string State { get; set; }

        [Display(Name = "CustomerInfo_Zip", ResourceType = typeof(Labels))]
        public string Zip { get; set; }

        [Display(Name = "CustomerInfo_Country", ResourceType = typeof(Labels))]
        public string Country { get; set; }

        [Display(Name = "CustomerInfo_HomePhone", ResourceType = typeof(Labels))]
        public string HomePhone { get; set; }

        [Display(Name = "CustomerInfo_WorkPhone", ResourceType = typeof(Labels))]
        public string WorkPhone { get; set; }

        [Display(Name = "CustomerInfo_CellPhone", ResourceType = typeof(Labels))]
        public string CellPhone { get; set; }

        [Display(Name = "CustomerInfo_Year", ResourceType = typeof(Labels))]
        public int? Year { get; set; }

        [Display(Name = "CustomerInfo_Make", ResourceType = typeof(Labels))]
        public string Make { get; set; }

        public int MakeId { get; set; }

        [Display(Name = "CustomerInfo_Model", ResourceType = typeof(Labels))]
        public string Model { get; set; }

        public int ModelId { get; set; }

        [Display(Name = "CustomerInfo_CarFullName", ResourceType = typeof(Labels))]
        public string CarFullName { get; set; }

        [Display(Name = "CustomerInfo_MaintenanceInterval", ResourceType = typeof(Labels))]
        public int? Mileage { get; set; }

        [Display(Name = "CustomerInfo_VIN", ResourceType = typeof(Labels))]
        public string VIN { get; set; }

        [Display(Name = "CustomerInfo_Engine", ResourceType = typeof(Labels))]
        public string Engine { get; set; }

        public int EngineId { get; set; }

        [Display(Name = "CustomerInfo_DriveLine", ResourceType = typeof(Labels))]
        public string DriveLine { get; set; }

        [Display(Name = "CustomerInfo_Transmission", ResourceType = typeof(Labels))]
        public string Transmission { get; set; }
        
        public int CarId { get; set; }

        public int? DealerVehicleId { get; set; }

        public int? VinMasterDriveLineId { get; set; }

        public int? VinMasterTransmissionId { get; set; }

        public string CompanyName { get; set; }
    }
}
