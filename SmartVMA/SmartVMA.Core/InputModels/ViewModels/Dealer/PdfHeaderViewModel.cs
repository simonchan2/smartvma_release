﻿using System;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class PdfHeaderViewModel
    {
        public PdfHeaderViewModel()
        {
            Dealer = new PdfHeaderDealerViewModel();
            Advisor = new PdfHeaderAdvisorViewModel();
            CustomerInfo = new PdfHeaderCustomerInfoViewModel();
        }

        public string Title { get; set; }
        public string SubTitle { get; set; }

        public PdfHeaderDealerViewModel Dealer { get; set; }
        public bool ShowDealer { get; set; }
        public PdfHeaderAdvisorViewModel Advisor { get; set; }
        public bool ShowAdvisor { get; set; }
        public PdfHeaderCustomerInfoViewModel CustomerInfo { get; set; }
        public bool ShowInvoiceNumber { get; set; }
    }
}

