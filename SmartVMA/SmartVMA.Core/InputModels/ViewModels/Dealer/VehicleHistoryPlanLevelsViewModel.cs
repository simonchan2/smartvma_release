﻿using SmartVMA.Core.Enums;
using System;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class VehicleHistoryPlanLevelsViewModel
    {
        public int BgProtectionPlanId { get; set; }
        public int ProtectionPlanLevel { get; set; }
        public int MaxMileageBeforeFirstService { get; set; }
    }
}
