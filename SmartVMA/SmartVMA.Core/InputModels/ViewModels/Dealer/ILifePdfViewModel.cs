﻿using System.Collections.Generic;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class ILifePdfViewModel : PdfDocumentViewModel
    {
        public ILifeViewModel ILifeModel { get; set; }
    }
}
