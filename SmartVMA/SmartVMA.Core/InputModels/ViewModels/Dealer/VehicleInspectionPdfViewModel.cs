﻿using SmartVMA.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class VehicleInspectionPdfViewModel : PdfDocumentViewModel
    {
        public int LeftFront { get; set; }
        public int LeftRear { get; set; }
        public int RightFront { get; set; }
        public int RightRear { get; set; }

        [Display(Name = "RimScratch", ResourceType = typeof(Labels))]
        public bool LFRimScratch { get; set; }
        [Display(Name = "RimScratch", ResourceType = typeof(Labels))]
        public bool LRRimScratch { get; set; }
        [Display(Name = "RimScratch", ResourceType = typeof(Labels))]
        public bool RFRimScratch { get; set; }
        [Display(Name = "RimScratch", ResourceType = typeof(Labels))]
        public bool RRRimScratch { get; set; }
    }
}
