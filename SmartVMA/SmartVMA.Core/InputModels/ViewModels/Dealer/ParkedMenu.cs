﻿using SmartVMA.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class ParkedMenu
    {
        [Display(ResourceType = typeof(Labels), Name = "AppointmentPresentationStartDate")]
        public DateTime? StartDate { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "AppointmentPresentationStartDate")]
        public string StartDateString { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "AppointmentPresentationEndDate")]
        public DateTime? EndDate { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "AppointmentPresentationEndDate")]
        public string EndDateString { get; set; }

        public int TimeFrameId { get; set; }

        public List<long> AdvisorIds { get; set; }

        public AppointmentPresentationIndexViewModel Items { get; set; }
    }
}
