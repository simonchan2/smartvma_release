﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class CalendarFilter
    {
        public CalendarFilter(List<long> advisorIds, int status)
        {
            AdvisorIds = advisorIds;
            AppointmentFilterStatus = status;
        }

        public List<long> AdvisorIds { get; set; }
        public int AppointmentFilterStatus { get; set; }
    }
}
