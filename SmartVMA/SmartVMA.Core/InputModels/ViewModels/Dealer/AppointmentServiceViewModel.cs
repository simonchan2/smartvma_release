﻿using SmartVMA.Core.Enums;
using System;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class AppointmentServiceViewModel
    {        
        public long AppointmentPresentationID { get; set; }
        public int? DealerCustomerID { get; set; }
        public int Mileage { get; set; }
        public string InvoiceNumber { get; set; }
        public string OpDescription { get; set; }
        public string OpCode { get; set; }
        public DateTime? AppointmentTime { get; set; }
        public string VIN { get; set; }
        public bool IsStriked { get; set; }
        public int? BgProtectionPlanId { get; set; }
        public string BgProductCategory { get; set; }
        public int? ServiceInterval { get; set; }
        public int? ExpectedMileage { get; set; }
        public int AverageMilesPerYear { get; set; }
        public int? ProtectionPlanLevel { get; set; }
    }
}
