﻿using System.Collections.Generic;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class VehicleHistoryPdfViewModel : PdfDocumentViewModel
    {
        public VehicleHistoryPdfViewModel()
        {
            Services = new List<AppointmentServiceViewModel>();
        }

        public List<AppointmentServiceViewModel> Services { get; set; }
    }
}
