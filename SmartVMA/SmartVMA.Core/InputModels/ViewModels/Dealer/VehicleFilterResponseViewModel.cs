﻿using System.Collections.Generic;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class VehicleFilterResponseViewModel
    {
        public long Id { get; set; }
        public string ServiceKeyword { get; set; }
        public string OpCode { get; set; }

        [JsonProperty("#Years")]
        public IEnumerable<SelectListItem> Years { get; set; }

        [JsonProperty("#Makes")]
        public IEnumerable<SelectListItem> Makes { get; set; }

        [JsonProperty("#Models")]
        public IEnumerable<SelectListItem> Models { get; set; }

        [JsonProperty("#EngineTypes")]
        public IEnumerable<SelectListItem> EngineTypes { get; set; }

        [JsonProperty("#TransmissionTypes")]
        public IEnumerable<SelectListItem> TransmissionTypes { get; set; }

        [JsonProperty("#DriveLines")]
        public IEnumerable<SelectListItem> DriveLines { get; set; }

        [JsonProperty("#IsFromSecondarySearch")]
        public bool IsFromSecondarySearch { get; set; }
    }
}
