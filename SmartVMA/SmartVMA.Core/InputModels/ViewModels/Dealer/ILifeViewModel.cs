﻿using SmartVMA.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class ILifeViewModel
    {
        public ILifeViewModel()
       {
            this.HistoryItems = new List<ILifeHistoryViewModel>();
        }
        public int Id { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "Store")]
        public string Store { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "Company")]
        public string Company { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "RecordID")]
        public int? RecordID { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "PastDue")]
        public string PastDue { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "Active")]
        public string Active { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "LeaseTermDate")]
        public DateTime? LeaseTermDate { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "Prefix")]
        public string Prefix { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "ContractNumber")]
        public string ContractNumber { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "SalesDate")]
        public DateTime? SalesDate { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "CancelDate")]
        public DateTime? CancelDate { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "DealerNo")]
        public string DealerNo { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "StockNum")]
        public string StockNum { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "SalePrice")]
        public decimal? SalePrice { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "Surcharge")]
        public decimal? Surcharge { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "STDEXE")]
        public string STDEXE { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "Type")]
        public string Type { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "FirstName")]
        public string FirstName { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "MiddleInitial")]
        public string MiddleInitial { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "LastName")]
        public string LastName { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "ContractHolder2")]
        public string ContractHolder2 { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "Address")]
        public string Address { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "City")]
        public string City { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "State")]
        public string State { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "Zip")]
        public string Zip { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "SoldAsNewUsed")]
        public string SoldAsNewUsed { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "Mileage")]
        public string Mileage { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "VIN")]
        public string VIN { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "VinLast8")]
        public string VinLast8 { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "Make")]
        public string Make { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "Model")]
        public string Model { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "Year")]
        public string Year { get; set; }

        public string RONum { get; set; }

        public string RoDate { get; set; }

        public string Message { get; set; }

        public string EligibilityMessage { get; set; }

        public bool IsVisible { get; set; }

        public List<ILifeHistoryViewModel>  HistoryItems {get; set;}

        public string ColorClass { get; set; }

        public string ButtonName { get; set; }
    }

    public class ILifeHistoryViewModel
    {
        public string Prefix { get; set; }

        public string ContractNumber { get; set; }

        public string RONum { get; set; }

        public string Company { get; set; }

        public string Name { get; set; }

        public string Mileage { get; set; }

        public string RoDate { get; set; }
    }
}
