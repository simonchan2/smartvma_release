﻿using SmartVMA.Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class VehicleFilterViewModel
    {
        public long Id { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "ServiceKeyword")]
        [StringLength(int.MaxValue, MinimumLength = 3, ErrorMessage = null, ErrorMessageResourceName = "SearchFieldMinLength3Only", ErrorMessageResourceType = typeof(Messages))]
        public string ServiceKeyword { get; set; }
        public string OpCode { get; set; }
        [Display(ResourceType = typeof(Labels), Name = "Year")]
        [Required]
        public IEnumerable<int> Years { get; set; }
        [Display(ResourceType = typeof(Labels), Name = "Make")]
        [Required]
        public IEnumerable<int> Makes { get; set; }
        [Display(ResourceType = typeof(Labels), Name = "Model")]
        [Required]
        public IEnumerable<int> Models { get; set; }
        [Display(ResourceType = typeof(Labels), Name = "Engine")]
        [Required]
        public IEnumerable<int> EngineTypes { get; set; }
        [Display(ResourceType = typeof(Labels), Name = "Transmission")]
        [Required]
        public IEnumerable<int> TransmissionTypes { get; set; }
        [Display(ResourceType = typeof(Labels), Name = "DriveLine")]
        [Required]
        public IEnumerable<int> DriveLines { get; set; }
        [Display(ResourceType = typeof(Labels), Name = "Mileage")]
        [Required]
        [RegularExpression("([1-9][0-9]*)", ErrorMessageResourceName = "OdometerMileageMinMaxValue", ErrorMessageResourceType = typeof(Messages))]
        [Range(1, 1000000, ErrorMessageResourceName = "OdometerMileageMinMaxValue", ErrorMessageResourceType = typeof(Messages))]
        public string Mileage { get; set; }
        public bool IsLofMethod { get; set; }

        public bool PreserveNA { get; set; }

        public VehicleFilterResponseViewModel VehicleFilterResponseModel { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "MenuLevel")]
        public int MenuLevel { get; set; }

        public string VIN { get; set; }

        public int? CustomerId { get; set; }

        public int? DealerVehicleId { get; set; }

        public int? AppointmentPresentationId { get; set; }

        public string AppointmentPresentationStatus { get; set; }
    }
}
