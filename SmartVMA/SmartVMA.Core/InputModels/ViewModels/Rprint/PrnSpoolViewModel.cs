﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class PrnSpoolViewModel
    {
        public int Id { get; set; }

        public string account { get; set; }

        public int idhist { get; set; }

        public string doctype { get; set; }

        public string docname { get; set; }

        public string prnname { get; set; }

        public string prndriver { get; set; }

        public int prncopy { get; set; }

        public string prnport { get; set; }

        public string prntray { get; set; }

        public DateTime datecreated { get; set; }

        public DateTime? dateprinted { get; set; }

        public string status { get; set; }

        public string comment { get; set; }

        public string assno { get; set; }

        public string session { get; set; }

        public DateTime TimeUpdated { get; set; }

        public DateTime TimeCreated { get; set; }
    }
}
