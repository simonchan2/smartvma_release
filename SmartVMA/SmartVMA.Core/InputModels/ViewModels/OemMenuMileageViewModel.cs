﻿namespace SmartVMA.Core.InputModels.ViewModels
{
    public class OemMenuMileageViewModel
    {
        public int Id { get; set; }

        public int Mileage { get; set; }
        public string OemMenuName { get; set; }
    }
}
