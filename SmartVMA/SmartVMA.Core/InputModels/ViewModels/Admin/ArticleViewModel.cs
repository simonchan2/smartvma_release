﻿using SmartVMA.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class ArticleViewModel
    {
        public ArticleViewModel()
        {
            this.CategoryIds = new List<int>();
            this.CategoryNames = new List<string>();
        }

        public int? Id { get; set; }

        [Required]
        [Display(ResourceType = typeof(Labels), Name = "Title")]
        public string Title { get; set; }

        [Required]
        [Display(ResourceType = typeof(Labels), Name = "Content")]
        [AllowHtml]
        public string Description { get; set; }

        [Required]
        [Display(ResourceType = typeof(Labels), Name = "Categories")]
        public List<int> CategoryIds { get; set; }

        public List<string> CategoryNames { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "IsActive")]
        public bool IsActive { get; set; }

        public DateTime? TimeDeleted { get; set; }

        public int? CategoryId { get; set; }

        public string Actions { get; set; }

    }
}
