﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartVMA.Core.Contracts;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class SettingViewModel : BaseIndexViewModel<SettingViewModel>
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
