﻿using System.Collections.Generic;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class PermissionSaveViewModel
    {
        public IEnumerable<string> PermissionNames { get; set; }

        public int RoleId { get; set; }
    }
}
