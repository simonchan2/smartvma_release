﻿namespace SmartVMA.Core.InputModels.ViewModels
{
    public class UserStatusCodeViewModel
    {
        public int Id { get; set; }

        public string Description { get; set; }
    }
}