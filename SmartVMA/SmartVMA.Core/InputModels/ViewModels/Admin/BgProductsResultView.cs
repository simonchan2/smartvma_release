﻿using SmartVMA.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class BgProductsResultView
    {
        public int ProductId { get; set; }
        public string BGSKU { get; set; }
        public string ProductDescription { get; set; }

        public string CategoryIds { get; set; }

        public string BgProductProtectionPlanIds { get; set; }

        public string BgProductCategoryIds { get; set; }

        public string SelectedCategoriesNames { get; set; }

        public IEnumerable<int> SelectedCategoryIDs { get; set; }
        public IEnumerable<int> SelectedProjectPlanIDs { get; set; }

        public Dictionary<string, bool> dicSelectedProjectPlanIDs { get; set; }

        [Display(Name = "LifetimeProtection", ResourceType = typeof(Labels))]
        public string LifetimeProtection { get; set; }

        [Display(Name = "ForeverDiesel", ResourceType = typeof(Labels))]
        public string ForeverDiesel { get; set; }

        [Display(Name = "EngineAssurance", ResourceType = typeof(Labels))]
        public string EngineAssurance { get; set; }

        public int RecordCount { get; set; }
    }
}
