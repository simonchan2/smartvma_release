﻿using SmartVMA.Core.Contracts;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class CompanyIndexViewModel : BaseIndexViewModel<CompanyViewModel>
    {
        public bool IsCurrentUserDistributorAdmin { get; set; }

        public int CurentTenantId { get; set; }
    }
}
