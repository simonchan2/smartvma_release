﻿using SmartVMA.Resources;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using SmartVMA.Core.Utils;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class ConfigurationMenuSetupViewModel
    {
        public ConfigurationMenuSetupViewModel()
        {
            UseOEMMenus = true;
            DisplayServicesMenuContent = true;
            RollAdditionalServiceIntoMainMenu = true;
            MenuLevels = 3;
            LOFMenuLevels = 3;
        }

        public int Id { get; set; }

        [Display(Name = "Configuration_UseOEMMenus", ResourceType = typeof(Labels))]
        public bool UseOEMMenus { get; set; }

        [Required]
        [Display(Name = "Configuration_DefaultLaborRate", ResourceType = typeof(Labels))]
        public decimal DefaultLaborRate { get; set; }

        [Display(Name = "Configuration_ConvertCurrency", ResourceType = typeof(Labels))]
        public bool ConvertCurrency { get; set; }

        [Display(Name = "Configuration_ExchangeRate", ResourceType = typeof(Labels))]
        public decimal? ExchangeRate { get; set; }

        [Display(Name = "Configuration_DisplayServicesMenuContent", ResourceType = typeof(Labels))]
        public bool DisplayServicesMenuContent { get; set; }

        [Display(Name = "Configuration_RollAdditionalServiceIntoMainMenu", ResourceType = typeof(Labels))]
        public bool RollAdditionalServiceIntoMainMenu { get; set; }

        [Required]
        [Display(Name = "Configuration_MenuLevels", ResourceType = typeof(Labels))]
        public int MenuLevels { get; set; }

        [RequiredClientValidationOnly]
        [Display(Name = "Configuration_MenuLabelLevel1", ResourceType = typeof(Labels))]
        public string MenuLabelLevel1 { get; set; }

        [RequiredClientValidationOnly]
        [Display(Name = "Configuration_MenuLabelLevel2", ResourceType = typeof(Labels))]
        public string MenuLabelLevel2 { get; set; }

        [RequiredClientValidationOnly]
        [Display(Name = "Configuration_MenuLabelLevel3", ResourceType = typeof(Labels))]
        public string MenuLabelLevel3 { get; set; }

        [Display(Name = "Configuration_MenuPrefixLevel1", ResourceType = typeof(Labels))]
        public string MenuPrefixLevel1 { get; set; }

        [Display(Name = "Configuration_MenuPrefixLevel2", ResourceType = typeof(Labels))]
        public string MenuPrefixLevel2 { get; set; }

        [Display(Name = "Configuration_MenuPrefixLevel3", ResourceType = typeof(Labels))]
        public string MenuPrefixLevel3 { get; set; }

        [RequiredClientValidationOnly]
        [Display(Name = "Configuration_MenuSuffixLevel1", ResourceType = typeof(Labels))]
        public string MenuSuffixLevel1 { get; set; }

        [RequiredClientValidationOnly]
        [Display(Name = "Configuration_MenuSuffixLevel2", ResourceType = typeof(Labels))]
        public string MenuSuffixLevel2 { get; set; }

        [RequiredClientValidationOnly]
        [Display(Name = "Configuration_MenuSuffixLevel3", ResourceType = typeof(Labels))]
        public string MenuSuffixLevel3 { get; set; }

        [Required]
        [Display(Name = "Configuration_LOFMenuLevels", ResourceType = typeof(Labels))]
        public int LOFMenuLevels { get; set; }

        [RequiredClientValidationOnly]
        [Display(Name = "Configuration_LOFMenuLabelLevel1", ResourceType = typeof(Labels))]
        public string LOFMenuLabelLevel1 { get; set; }

        [RequiredClientValidationOnly]
        [Display(Name = "Configuration_LOFMenuLabelLevel2", ResourceType = typeof(Labels))]
        public string LOFMenuLabelLevel2 { get; set; }

        [RequiredClientValidationOnly]
        [Display(Name = "Configuration_LOFMenuLabelLevel3", ResourceType = typeof(Labels))]
        public string LOFMenuLabelLevel3 { get; set; }

        [RequiredClientValidationOnly]
        [Display(Name = "Configuration_LOFMenuOpCodeLevel1", ResourceType = typeof(Labels))]
        public string LOFMenuOpCodeLevel1 { get; set; }

        [RequiredClientValidationOnly]
        [Display(Name = "Configuration_LOFMenuOpCodeLevel2", ResourceType = typeof(Labels))]
        public string LOFMenuOpCodeLevel2 { get; set; }

        [RequiredClientValidationOnly]
        [Display(Name = "Configuration_LOFMenuOpCodeLevel3", ResourceType = typeof(Labels))]
        public string LOFMenuOpCodeLevel3 { get; set; }
    }
}
