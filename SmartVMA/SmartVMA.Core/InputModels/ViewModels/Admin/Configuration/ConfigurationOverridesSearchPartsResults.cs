﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class ConfigurationOverridesSearchPartsResults
    {
        public string PartNumber { get; set; }

        public string PartQuantity { get; set; }

        public string PartPrice { get; set; }

        public string PartType { get; set; }
    }
}
