﻿using SmartVMA.Resources;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class ConfigurationCorpApplicationSettingsViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Configuration_ApplicationName", ResourceType = typeof(Labels))]
        public string ApplicationName { get; set; }

        [Display(Name = "Configuration_DataRefreshFrequencyInDays", ResourceType = typeof(Labels))]
        public int DataRefreshFrequencyInDays { get; set; }

        [Display(Name = "Configuration_SessionExpiration", ResourceType = typeof(Labels))]
        public int SessionExpiration { get; set; }

        [Display(Name = "Configuration_SupportLink", ResourceType = typeof(Labels))]
        public string SupportLink { get; set; }

        [Display(Name = "Configuration_SupportLinkDisplayText", ResourceType = typeof(Labels))]
        public string SupportLinkDisplayText { get; set; }

        [Display(Name = "Configuration_PrivacyPolicyLink", ResourceType = typeof(Labels))]
        public string PrivacyPolicyLink { get; set; }

        [Display(Name = "Configuration_PrivacyPolicyLinkDisplayText", ResourceType = typeof(Labels))]
        public string PrivacyPolicyLinkDisplayText { get; set; }

        [Display(Name = "Configuration_SmartVmaLink", ResourceType = typeof(Labels))]
        public string SmartVmaLink { get; set; }

        [Display(Name = "Configuration_SmartVmaLinkDisplayText", ResourceType = typeof(Labels))]
        public string SmartVmaLinkDisplayText { get; set; }
    }
}
