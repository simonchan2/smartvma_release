﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class ConfigurationOverridesSearchResults
    {
        public ConfigurationOverridesSearchResults()
        {
            PartInfo = new List<ConfigurationOverridesSearchPartsResults>();
        }

        public int Id { get; set; }

        public string Opcode { get; set; }

        public string Description { get; set; }

        public string LaborTime { get; set; }

        public string LaborPrice { get; set; }

        public bool IsLof { get; set; }

        public bool Checked { get; set; }

        public string RowId { get; set; }

        public List<ConfigurationOverridesSearchPartsResults> PartInfo { get; set; }
    }
}
