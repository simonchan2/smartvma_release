﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class ConfigurationOverrideRulesViewModel
    {
        public long Id { get; set; }
        public string RuleName { get; set; }
        public string OpCode { get; set; }
        public string LaborPrice { get; set; }
        public string LaborHour { get; set; }
        public string PartPrice { get; set; }
        public string PartNumber { get; set; }
        public string Actions { get; set; }
    }
}
