﻿using SmartVMA.Resources;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class ConfigurationApplicationSettingViewModel
    {
        public ConfigurationApplicationSettingViewModel()
        {
            AdvisorsHaveDeleteParkedMenuPermission = true;
            FlagMenuAsModifiedForAnyChange = true;
        }

        public int Id { get; set; }

        [Required]
        [Display(Name = "Configuration_SessionExpiration", ResourceType = typeof(Labels))]
        public int SessionExpiration { get; set; }

        [Required]
        [Display(Name = "Configuration_DmsType", ResourceType = typeof(Labels))]
        public int? DmsTypeId { get; set; }

        [Required]
        [Display(Name = "Configuration_ParkedMenuRetentionDuration", ResourceType = typeof(Labels))]
        public int ParkedMenuRetentionDuration { get; set; }

        [Display(Name = "Configuration_AdvisorsHaveDeleteParkedMenuPermission", ResourceType = typeof(Labels))]
        public bool AdvisorsHaveDeleteParkedMenuPermission { get; set; }

        [Display(Name = "Configuration_FlagMenuAsModifiedForAnyChange", ResourceType = typeof(Labels))]
        public bool FlagMenuAsModifiedForAnyChange { get; set; }

        [Required]
        [Display(Name = "Configuration_AverageMilesPerYear", ResourceType = typeof(Labels))]
        public int AverageMilesPerYear { get; set; }

        [Display(Name = "Configuration_IsPlanActive", ResourceType = typeof(Labels))]
        public bool IsPlanActive { get; set; }

        [Display(Name = "Configuration_PlanProvider", ResourceType = typeof(Labels))]
        public string PlanProvider { get; set; }

        [Display(Name = "Configuration_PlanName", ResourceType = typeof(Labels))]
        public string PlanName { get; set; }

        [Display(Name = "Configuration_IsRemotePrint", ResourceType = typeof(Labels))]
        public bool IsRemotePrint { get; set; }

        [Display(Name = "Configuration_PrintServer", ResourceType = typeof(Labels))]
        public string PrintServer { get; set; }

        [Display(Name = "Configuration_PrintPort", ResourceType = typeof(Labels))]
        public string PrintPort { get; set; }

        [Display(Name = "Configuration_HomeButtonOrder", ResourceType = typeof(Labels))]
        public int HomeButtonOrder { get; set; }

        [Display(Name = "Configuration_AppointmentsButtonOrder", ResourceType = typeof(Labels))]
        public int AppointmentsButtonOrder { get; set; }

        [Display(Name = "Configuration_DocumentSearchButtonOrder", ResourceType = typeof(Labels))]
        public int DocumentSearchButtonOrder { get; set; }

        [Display(Name = "Configuration_ReportingButtonOrder", ResourceType = typeof(Labels))]
        public int ReportingButtonOrder { get; set; }

        [Display(Name = "Configuration_PlannerButtonOrder", ResourceType = typeof(Labels))]
        public int PlannerButtonOrder { get; set; }

        [Display(Name = "Configuration_MpiButtonOrder", ResourceType = typeof(Labels))]
        public int MpiButtonOrder { get; set; }

        [Display(Name = "Configuration_RoPrepButtonOrder", ResourceType = typeof(Labels))]
        public int RoPrepButtonOrder { get; set; }
    }
}
