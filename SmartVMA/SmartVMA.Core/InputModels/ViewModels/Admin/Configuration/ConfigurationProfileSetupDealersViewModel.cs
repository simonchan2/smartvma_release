﻿using SmartVMA.Resources;
using System.ComponentModel.DataAnnotations;
using SmartVMA.Infrastructure;
using SmartVMA.Core.Utils;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class ConfigurationProfileSetupDealersViewModel
    {
        public int? Id { get; set; }

        [RequiredClientValidationOnly]
        [Display(Name = "Configuration_AccountNumber", ResourceType = typeof(Labels))]
        public string AccountNumber { get; set; }

        [RequiredClientValidationOnly]
        [Display(Name = "Distributor", ResourceType = typeof(Labels))]
        public int? DistributorId { get; set; }
        
        [Required]
        [Display(Name = "Configuration_Name", ResourceType = typeof(Labels))]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Configuration_Address", ResourceType = typeof(Labels))]
        public string Address { get; set; }

        [Display(Name = "Configuration_Address2", ResourceType = typeof(Labels))]
        public string Address2 { get; set; }

        [Required]
        [Display(Name = "Configuration_City", ResourceType = typeof(Labels))]
        public string City { get; set; }

        [Required]
        [Display(Name = "Configuration_CountryId", ResourceType = typeof(Labels))]
        public int CountryId { get; set; }

        [Required]
        [Display(Name = "Configuration_StateId", ResourceType = typeof(Labels))]
        public int? StateId { get; set; }

        [Required]
        [Display(Name = "Configuration_ZipCode", ResourceType = typeof(Labels))]
        public string ZipCode { get; set; }

        [Required]
        [RegularExpression(RegExp.PhoneNumber, ErrorMessage = null, ErrorMessageResourceName = "InvalidPhoneNumber", ErrorMessageResourceType = typeof(Messages))]
        [Display(Name = "Configuration_PhoneNumber", ResourceType = typeof(Labels))]
        public string PhoneNumber { get; set; }

        [Display(Name = "Configuration_PhoneNumber2", ResourceType = typeof(Labels))]
        [RegularExpression(RegExp.PhoneNumber, ErrorMessage = null, ErrorMessageResourceName = "InvalidPhoneNumber", ErrorMessageResourceType = typeof(Messages))]
        public string PhoneNumber2 { get; set; }

        [Display(Name = "Configuration_FaxNumber", ResourceType = typeof(Labels))]
        public string FaxNumber { get; set; }

        [Display(Name = "Configuration_Email", ResourceType = typeof(Labels))]
        [EmailAddress(ErrorMessage = null, ErrorMessageResourceName = "NotValidEmail", ErrorMessageResourceType = typeof(Messages))]
        public string Email { get; set; }

        [Display(Name = "Configuration_Web", ResourceType = typeof(Labels))]
        public string Web { get; set; }

        [Required]
        [Display(Name = "Configuration_Language", ResourceType = typeof(Labels))]
        public int Language { get; set; }

        [Required]
        [Display(Name = "Configuration_DateFormat", ResourceType = typeof(Labels))]
        public int DateFormat { get; set; }

        [Required]
        [Display(Name = "Configuration_MeasurementUnit", ResourceType = typeof(Labels))]
        public int MeasurementUnitId { get; set; }

        public string LogoFileName { get; set; }

        public string LogoGuid { get; set; }

        public string LogoOriginalFileName { get; set; }
        public string LogoDownloadUrl { get; internal set; }

        public string ApplicationName { get; set; }
    }
}
