﻿using SmartVMA.Resources;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class ConfigurationDocumentSetupViewModel
    {
        public ConfigurationDocumentSetupViewModel()
        {
            ShopChargeAmount = 10;
        }
        public int Id { get; set; }

        [Required]
        [Display(Name = "Configuration_InvoiceNoteText", ResourceType = typeof(Labels))]
        public string InvoiceNoteText { get; set; }

        [Required]
        [Display(Name = "Configuration_DisclaimerText", ResourceType = typeof(Labels))]
        public string DisclaimerText { get; set; }

        [Display(Name = "Configuration_DisplayInvoiceNumber", ResourceType = typeof(Labels))]
        public bool DisplayInvoiceNumber { get; set; }

        [Display(Name = "Configuration_PrintAdvisorInformation", ResourceType = typeof(Labels))]
        public bool PrintAdvisorInformation { get; set; }

        [Display(Name = "Configuration_PrintShopCharges", ResourceType = typeof(Labels))]
        public bool PrintShopCharges { get; set; }
        
        [Display(Name = "Configuration_ShopChargePercent", ResourceType = typeof(Labels))]
        public decimal? ShopChargeAmount { get; set; }

        [Display(Name = "Configuration_ShopChargesApplyToParts", ResourceType = typeof(Labels))]
        public bool ShopChargesApplyToParts { get; set; }

        [Display(Name = "Configuration_ShopChargesApplyToLabor", ResourceType = typeof(Labels))]
        public bool ShopChargesApplyToLabor { get; set; }

        [Display(Name = "Configuration_ShopChargeMinimum", ResourceType = typeof(Labels))]
        public decimal? ShopChargeMinimum { get; set; }

        [Display(Name = "Configuration_ShopChargeMaximum", ResourceType = typeof(Labels))]
        public decimal? ShopChargeMaximum { get; set; }

        [Display(Name = "Configuration_PrintTaxesOnDocuments", ResourceType = typeof(Labels))]
        public bool PrintTaxesOnDocuments { get; set; }

        [Display(Name = "Configuration_DisplayTexesInMenus", ResourceType = typeof(Labels))]
        public bool DisplayTaxesInMenus { get; set; }

        [Range(0, 100, ErrorMessageResourceName = "PercentInvalidValue", ErrorMessageResourceType =typeof(Messages))]
        [Display(Name = "Configuration_TaxRateAmount", ResourceType = typeof(Labels))]
        public decimal? TaxRateAmount { get; set; }

        [Display(Name = "Configuration_TaxRateApplyToParts", ResourceType = typeof(Labels))]
        public bool TaxRateApplyToParts { get; set; }

        [Display(Name = "Configuration_TaxRateApplyToLabor", ResourceType = typeof(Labels))]
        public bool TaxRateApplyToLabor { get; set; }
    }
}
