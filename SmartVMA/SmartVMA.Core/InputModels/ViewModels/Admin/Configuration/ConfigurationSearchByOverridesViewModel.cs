﻿using SmartVMA.Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class ConfigurationSearchByOverridesViewModel
    {
        public ConfigurationSearchByOverridesViewModel()
        {
            Filter = new VehicleFilterViewModel();
            FilterResponse = new VehicleFilterResponseViewModel();
            OverrideConflicts = new List<ConfigurationOverrideRulesConflictsViewModel>();
            OemBasicServices = new List<ConfigurationOverrideListModel>();
            //SelectedServices = new List<int>();
        }

        public long Id { get; set; }

        [Display(Name = "Configuration_Keyword", ResourceType = typeof(Labels))]
        public string Keyword { get; set; }

        [Display(Name = "OpCode", ResourceType = typeof(Labels))]
        public string OpCode { get; set; }

        [Display(Name = "PartNumber", ResourceType = typeof(Labels))]
        public string PartNumber { get; set; }

        public List<ConfigurationOverridesSearchResults> Overrides { get; set; }

        [Display(Name = "RuleName", ResourceType = typeof(Labels))]
        [Required]
        public string RuleName { get; set; }

        [Display(Name = "OpCode", ResourceType = typeof(Labels))]
        public bool? OverrideOpcodeChecked { get; set; }

        [Display(Name = "Configuration_OverrideOpCode", ResourceType = typeof(Labels))]
        public string OverrideOpcode { get; set; }

        public string OverridePartNumber { get; set; }

        [Display(Name = "LaborPrice", ResourceType = typeof(Labels))]
        public bool? LaborPriceChecked { get; set; }

        public bool LaborPricePercentageChecked { get; set; }

        [Display(Name = "Configuration_OverridesPercentage", ResourceType = typeof(Labels))]
        public int? LaborPricePercentage { get; set; }

        public bool LaborPriceAmountChecked { get; set; }

        [Display(Name = "Configuration_OverridesAmount", ResourceType = typeof(Labels))]
        public decimal? LaborPriceAmount { get; set; }

        [Display(Name = "LaborHours", ResourceType = typeof(Labels))]
        public bool? LaborHourChecked { get; set; }

        public bool LaborHourPercentageChecked { get; set; }

        [Display(Name = "Configuration_OverridesPercentage", ResourceType = typeof(Labels))]
        public int? LaborHourPercentage { get; set; }

        public bool LaborHourAmountChecked { get; set; }

        [Display(Name = "Configuration_OverridesHours", ResourceType = typeof(Labels))]
        public decimal? LaborHourAmount { get; set; }

        [Display(Name = "PartPrice", ResourceType = typeof(Labels))]
        public bool? PartPriceChecked { get; set; }

        public bool PartPricePercentageChecked { get; set; }

        [Display(Name = "Configuration_OverridesPercentage", ResourceType = typeof(Labels))]
        public int? PartPricePercentage { get; set; }

        public bool PartPriceAmountChecked { get; set; }

        [Display(Name = "Configuration_OverridesAmount", ResourceType = typeof(Labels))]
        public decimal? PartPriceAmount { get; set; }

        [Display(Name = "PartNumber", ResourceType = typeof(Labels))]
        public bool? PartNumberChecked { get; set; }

        public List<ConfigurationOverrideRulesConflictsViewModel> OverrideConflicts { get; set; }
        public VehicleFilterViewModel Filter { get; set; }
        public VehicleFilterResponseViewModel FilterResponse { get; set; }
        public long FilteredServicesCount { get; set; }
        public long TotalServicesCount { get; set; }
        public bool AreServicesChecked { get; set; }
        public List<ConfigurationOverrideListModel> OemBasicServices { get; set; }
        public IEnumerable<int> SelectedServices { get; set; }
        public string OriginalPartNumber { get; set; }
    }
}
