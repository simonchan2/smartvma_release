﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class ConfigurationOverrideRulesConflictsViewModel
    {
        public ConfigurationOverrideRulesConflictsViewModel()
        {
            ConflictingTypes = new List<ConfigurationOverrideRulesConflictsByTypeViewModel>();
            ServiceDescriptions = new List<string>();
        }
        public long RuleId { get; set; }
        public string RuleName { get; set; }
        public long ConflictingServices { get; set; }
        public List<string> ServiceDescriptions { get; set; }

        public List<ConfigurationOverrideRulesConflictsByTypeViewModel> ConflictingTypes { get; set; }
    }
}
