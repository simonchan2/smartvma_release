﻿namespace SmartVMA.Core.InputModels.ViewModels
{
    public class ConfigurationOverrideRulesConflictsByTypeViewModel
    {
        public string OverrideType { get; set; }
        public string ExistingValue { get; set; }
        public string NewValue { get; set; }
    }
}