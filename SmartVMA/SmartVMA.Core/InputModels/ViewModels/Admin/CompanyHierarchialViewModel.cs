﻿namespace SmartVMA.Core.InputModels.ViewModels
{
    public class CompanyHierarchialViewModel
    {
        public string Name { get; set; }

        public int Order { get; set; }
    }
}
