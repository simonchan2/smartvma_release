﻿using SmartVMA.Core.Contracts;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class PermissionIndexViewModel : BaseIndexViewModel<PermissionViewModel>
    {
        public int RoleId { get; set; }

        public string RoleName { get; set; }
    }
}
