﻿using SmartVMA.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class BgProductViewModel
    {
        public BgProductViewModel()
        {
            Filter = new BgProductCategoryViewModel();
        }

        public long Id { get; set; }

        public int BgProductId { get; set; }

        public int? ProductId { get; set; }

        [Required(ErrorMessageResourceName = "BGSKUIsRequired", ErrorMessageResourceType = typeof(Messages))]        
        [MaxLength(50, ErrorMessageResourceName = "BGSKUMaximumLenghtError", ErrorMessageResourceType = typeof(Messages))]
        [Display(Name = "BGSKU", ResourceType = typeof(Labels))]
        public string PartNumber { get; set; }

        [Required(ErrorMessageResourceName = "ProductDescriptionIsRequired", ErrorMessageResourceType = typeof(Messages))]
        [MaxLength(255, ErrorMessageResourceName = "ProductDescriptionMaximumLenghtError", ErrorMessageResourceType = typeof(Messages))]
        [Display(Name = "ProductDescription", ResourceType = typeof(Labels))]
        public string ProductDescription { get; set; }

      
        [Display(Name = "CategoryName", ResourceType = typeof(Labels))]
        public string CategoryName { get; set; }

        public int CategoryId { get; set; }

        [Display(Name = "LifetimeProtection", ResourceType = typeof(Labels))]
        public string LifetimeProtection { get; set; }

        [Display(Name = "ForeverDiesel", ResourceType = typeof(Labels))]
        public string ForeverDiesel { get; set; }

        [Display(Name = "EngineAssurance", ResourceType = typeof(Labels))]
        public string EngineAssurance { get; set; }

        public List<BgProductsResultView> BgProductProtectionPlans { get; set; }

        public BgProductCategoryViewModel Filter { get; set; }

        [Required(ErrorMessageResourceName = "ProtectionPlanCategoryIsRequired", ErrorMessageResourceType = typeof(Messages))]
        [Display(Name = "SelectCategories", ResourceType = typeof(Labels))]
        public IEnumerable<int> SelectCategories { get; set; }

        public string SelectedCategoryNames { get; set; }
        public int? BgProductsProtectionPlanId { get; set; }

        [Display(Name = "LifetimeProtection", ResourceType = typeof(Labels))]
        public bool isCheckedLifetimeProtection { get; set; }

        [Display(Name = "ForeverDiesel", ResourceType = typeof(Labels))]
        public bool isCheckedForeverDiesel { get; set; }

        [Display(Name = "EngineAssurance", ResourceType = typeof(Labels))]
        public bool isCheckedEngineAssurance { get; set; }

        public Dictionary<string, bool> SelectedProjectPlanIDs { get; set; }

        public List<string> SelectPlan { get; set; }

        public int RecordCount {get; set;}

        public string Actions { get; set; }

    }
}
