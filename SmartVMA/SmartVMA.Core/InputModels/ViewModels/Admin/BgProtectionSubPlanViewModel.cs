﻿namespace SmartVMA.Core.InputModels.ViewModels.Admin
{
    public class BgProtectionSubPlanViewModel
    {
        public int Id { get; set; }

        public string Description { get; set; }
    }
}
