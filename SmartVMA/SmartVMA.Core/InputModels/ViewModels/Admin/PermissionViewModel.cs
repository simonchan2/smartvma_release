﻿using System;
using SmartVMA.Resources;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class PermissionViewModel
    {
        public int Id { get; set; }

        [StringLength(200)]
        [Display(Name = "Permission_Name", ResourceType = typeof(Labels))]
        public string Name { get; set; }

        [StringLength(1000)]
        [Display(Name = "Permission_Description", ResourceType = typeof(Labels))]
        public string Description { get; set; }

        [StringLength(200)]
        [Display(Name = "Permission_Displayname", ResourceType = typeof(Labels))]
        public string Displayname { get; set; }

        [Display(Name = "Permission_IsGranted", ResourceType = typeof(Labels))]
        public bool IsGranted { get; set; }

        [Display(Name = "TimeUpdated", ResourceType = typeof(Labels))]
        public DateTime TimeUpdated { get; set; }

        public int RoleId { get; set; }

        public string RoleName { get; set; }
        public string Actions { get; set; }
        public string Checkboxes { get; set; }
    }
}