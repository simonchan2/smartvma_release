﻿using SmartVMA.Resources;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class ArticleCategoryViewModel
    {
      //  private object get;

        public int? Id { get; set; }

        [Required]
        [Display(ResourceType = typeof(Labels), Name = "Name")]
        public string Name { get; set; }        
    }
}
