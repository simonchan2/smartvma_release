﻿namespace SmartVMA.Core.InputModels.ViewModels
{
    public class CountryViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
