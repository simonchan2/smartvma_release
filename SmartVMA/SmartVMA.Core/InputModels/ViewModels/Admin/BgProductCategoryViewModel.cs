﻿using SmartVMA.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class BgProductCategoryViewModel
    {
        [Display(ResourceType = typeof(Labels), Name = "SelectCategory")]
        [Required]
        public IEnumerable<int> Category { get; set; }
    }
}
