﻿namespace SmartVMA.Core.InputModels.ViewModels
{
    public class DateFormatViewModel
    {
        public int Id { get; set; }
        public string Format { get; set; }
    }
}
