﻿namespace SmartVMA.Core.InputModels.ViewModels
{
    public class AlertUserViewModel
    {
        public int? Id { get; set; }

        public long UserId { get; set; }

        public int AlertId { get; set; }

        public int StatusId { get; set; }
    }
}
