﻿using SmartVMA.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class OemBasicServiceViewModel
    {
        public OemBasicServiceViewModel()
        {
            this.PartsIds = new List<int>();
            this.OemParts = new List<OemPartViewModel>();
        }
        public int? Id { get; set; }

        public int CarId { get; set; }

        public int CarIdFK { get; set; }

        public int OemComponentId { get; set; }

        public int? OemServiceExceptionId { get; set; }

        
        [StringLength(20)]
        public string OpAction { get; set; }

       
        [StringLength(255)]
        [Display(ResourceType = typeof(Labels), Name = "ServiceDescription")]
        public string ServiceDescription { get; set; }

        [StringLength(10)]
        public string LaborSkillLevel { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "LaborHour")]
        public double LaborHour { get; set; }
                
        [StringLength(2)]
        public string VehicleTransmission { get; set; }
        
        public bool? IsIncomplete { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public List<int> PartsIds { get; set; }
        [Display(ResourceType = typeof(Labels), Name = "VehicleDescription")]
        public string VehicleDescription { get; set; }

        [AllowHtml]
        public string ContentFromAllData { get; set; }

        public List<OemPartViewModel> OemParts { get; set; }

        public string Actions { get; set; }

        public string Message {get; set;}

    }
}
