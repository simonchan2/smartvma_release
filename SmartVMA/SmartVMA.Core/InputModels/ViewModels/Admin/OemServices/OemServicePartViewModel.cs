﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class OemServicePartViewModel
    {
        public int? Id { get; set; }

        public int OemBasicServiceId { get; set; }

        public int OemPartId { get; set; }

        public double Quantity { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }
    }
}
