using System;
using System.ComponentModel.DataAnnotations;
using SmartVMA.Resources;

namespace SmartVMA.Core.InputModels.ViewModels
{
    [Serializable]
    public class PartItemViewModel
    {
        public int? Id { get; set; }
        
        [Display(Name = "PartNumber", ResourceType = typeof(Labels))]
        public string PartNumber { get; set; }

        private double? _quantity;
        [Display(Name = "Quantity", ResourceType = typeof(Labels))]
        public double? Quantity
        {
            get
            {
                if (!_quantity.HasValue)
                {
                    return null;
                }
                return Math.Round(_quantity.Value, 1);
            }
            set { _quantity = value; }
        }

        [Display(Name = "PartPrice", ResourceType = typeof(Labels))]
        public decimal? Price { get; set; }

        [Display(Name = "IsFluid", ResourceType = typeof(Labels))]
        public bool IsFluid { get; set; }

        public bool IsDeleted { get; set; }
    }
}