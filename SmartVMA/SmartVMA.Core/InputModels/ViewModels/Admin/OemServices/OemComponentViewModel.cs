﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class OemComponentViewModel
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public bool IsFluid { get; set; }

        public bool IsEngineOil { get; set; }

        public bool IsLOF { get; set; }

        public bool ToBeIgnored { get; set; }

        public bool? IsShownAsOption { get; set; }

        public int? LaborTimeRuleId { get; set; }

        public int? PartRuleId { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }
    }
}
