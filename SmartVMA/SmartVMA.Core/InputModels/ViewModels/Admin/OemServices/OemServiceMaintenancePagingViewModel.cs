using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Resources;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class OemServiceMaintenancePagingViewModel : PagingRequest
    {
        public string ServiceKeyword { get; set; }

        public string OpCode { get; set; }

        [Required]
        [Display(ResourceType = typeof(Labels), Name = "Year")]
        public IEnumerable<int> Years { get; set; }

        [Required]
        [Display(ResourceType = typeof(Labels), Name = "Make")]
        public IEnumerable<int> Makes { get; set; }

        [Required]
        [Display(ResourceType = typeof(Labels), Name = "Model")]
        public IEnumerable<int> Models { get; set; }

        [Required]
        [Display(ResourceType = typeof(Labels), Name = "Engine")]
        public IEnumerable<int> EngineTypes { get; set; }

        [Required]
        [Display(ResourceType = typeof(Labels), Name = "Transmission")]
        public IEnumerable<int> TransmissionTypes { get; set; }

        [Required]
        [Display(ResourceType = typeof(Labels), Name = "DriveLine")]
        public IEnumerable<int> DriveLines { get; set; }

        //[Required]
        //[Display(ResourceType = typeof(Labels), Name = "Mileage")]
        //public string Mileage { get; set; }

        public bool IsLofMethod { get; set; }
    }
}