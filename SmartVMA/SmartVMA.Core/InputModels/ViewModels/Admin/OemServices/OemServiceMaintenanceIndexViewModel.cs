using SmartVMA.Core.Contracts;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class OemServiceMaintenanceIndexViewModel : BaseIndexViewModel<OemServiceMaintenanceViewModel>
    {
        public string FilteredServiceIds { get; set; }
    }
}