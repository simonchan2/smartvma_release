namespace SmartVMA.Core.InputModels.ViewModels
{
    public class OemServiceMaintenanceInitViewModel
    {
        public VehicleFilterViewModel VehicleFilter { get; set; }

        public OemServiceMaintenanceViewModel Data { get; set; }
        public string SelectedServices { get; set; }
    }
}