﻿using SmartVMA.Resources;
using System;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class OemPartViewModel
    {
        public int? Id { get; set; }

        public int? CarId { get; set; }
        [Display(ResourceType = typeof(Labels), Name = "OemComponentId")]
        public int? OemComponentId { get; set; }
        [Display(ResourceType = typeof(Labels), Name = "PartNumber")]
        public string OemPartNo { get; set; }
        public int? OemPartNumberId { get; set; }
        public int? OemServicePartId { get; set; }
        [Required]
        [Display(ResourceType = typeof(Labels), Name = "PartName")]
        public string OemPartName { get; set; }

        public string Unit { get; set; }
        [Display(ResourceType = typeof(Labels), Name = "Price")]
        public decimal UnitPrice { get; set; }
        [Display(ResourceType = typeof(Labels), Name = "OilType")]
        public string FluidType { get; set; }
        [Display(ResourceType = typeof(Labels), Name = "OilViscosity")]
        public string FluidViscosity { get; set; }
        public string LaborSkillLevel { get; set; }
        public double LaborHour { get; set; }
        public DateTime TimeCreated { get; set; }
        public DateTime TimeUpdated { get; set; }
        [Display(ResourceType = typeof(Labels), Name = "Quantity")]
        public double Quantity { get; set; }
        public int ListNumber { get; set; }
        public int? OemBasicServiceId { get; set; }
        [Display(ResourceType = typeof(Labels), Name = "OilType")]
        public string EngineOilType { get; set; }
    }
}
