﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SmartVMA.Resources;

namespace SmartVMA.Core.InputModels.ViewModels
{
    [Serializable]
    public class OemServiceMaintenanceViewModel
    {
        public OemServiceMaintenanceViewModel()
        {
            MileagesKm = new List<int>();
            MileagesM = new List<int>();
            ViewModelType = "OemServiceMaintenance";
        }
        public int? Id { get; set; }

        [Display(Name = "OpCode", ResourceType = typeof(Labels))]
        public string OpCode { get; set; }

        [Required]
        [Display(Name = "Description", ResourceType = typeof(Labels))]
        public string Description { get; set; }

        [Display(Name = "IsLofService", ResourceType = typeof(Labels))]
        public bool IsLof { get; set; }

        [Display(Name = "IsFluid", ResourceType = typeof(Labels))]
        public bool IsFluid { get; set; }

        [Display(Name = "LaborTime", ResourceType = typeof(Labels))]
        public double? LaborTime { get; set; }
        
        [Required]
        [Display(Name = "OemComponentId", ResourceType = typeof(Labels))]
        public int OemComponentId { get; set; }

        [Required]
        public List<int> MileagesKm { get; set; }

        [Required]
        public List<int> MileagesM { get; set; }

        public List<PartItemViewModel> Parts { get; set; }

        [Required]
        public string OpAction { get; set; }

        public string ViewModelType { get; set; }
        public string RowId { get; set; }
        public bool IsSelected { get; set; }
    }
}
