﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Core.Services
{
    public class OemComponentIndexViewModel : BaseIndexViewModel<OemComponentViewModel>
    {
    }
}