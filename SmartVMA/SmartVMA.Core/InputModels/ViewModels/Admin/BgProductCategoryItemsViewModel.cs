﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Core.InputModels.ViewModels.Admin
{
    public class BgProductCategoryItemsViewModel
    {
        public int? Id { get; set; }

        public string Text { get; set; }
    }
}
