﻿using SmartVMA.Resources;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class RoleViewModel
    {
        public int? Id { get; set; }

        public int? ParentId { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Role_Role", ResourceType = typeof(Labels))]
        public string Role { get; set; }
        
        [StringLength(100)]
        [Display(Name = "Role_Description", ResourceType = typeof(Labels))]
        public string Description { get; set; }

        [Display(Name = "Role_Configuration", ResourceType = typeof(Labels))]
        public string Configuration { get; set; }

        [Display(Name = "Role_Comment", ResourceType = typeof(Labels))]
        public string Comment { get; set; }
        public bool IsSystemRole { get; internal set; }

        public string Actions { get; set; }
        public bool IsDeletable { get; set; }
    }
}