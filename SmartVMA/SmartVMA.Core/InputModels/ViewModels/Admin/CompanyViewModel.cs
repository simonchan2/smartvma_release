﻿namespace SmartVMA.Core.InputModels.ViewModels
{
    public class CompanyViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Parent { get; set; }

        public int CompanyTypeId { get; set; }

        public string CompanyType { get; set; }

        public string CompanyGroups { get; set; }

        public bool IsCurrentUserDistributorAdmin { get; set; }

        public bool IsCurrentUserSystemAdmin { get; set; }

        public int CurentTenantId { get; set; }

        public string Actions { get; set; }
    }
}