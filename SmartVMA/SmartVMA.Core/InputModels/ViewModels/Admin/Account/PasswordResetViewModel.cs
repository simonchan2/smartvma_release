﻿using SmartVMA.Resources;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class PasswordResetViewModel
    {
        [Display(Name = "Password", ResourceType = typeof(Labels))]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,50}$", ErrorMessageResourceName = "PasswordNotShorterThan8Characters", ErrorMessageResourceType = typeof(Messages))]
        public string Password { get; set; }

        [Display(Name = "ConfirmPassword", ResourceType = typeof(Labels))]
        [Compare("Password", ErrorMessageResourceName = "PasswordsDoNotMatch", ErrorMessageResourceType = typeof(Messages))]
        public string ConfirmPassword { get; set; }

        public string Token { get; set; }
    }
}
