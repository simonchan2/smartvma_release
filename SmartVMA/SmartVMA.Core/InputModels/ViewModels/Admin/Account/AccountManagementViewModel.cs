﻿using SmartVMA.Resources;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class AccountManagementViewModel
    {
        public long Id { get; set; }

        [Display(Name = "DefaultStore", ResourceType = typeof(Labels))]
        public int TenantId { get; set; }

        [Display(Name = "CurrentPassword", ResourceType = typeof(Labels))]
        public string OldPassword { get; set; }

        [Display(Name = "Password", ResourceType = typeof(Labels))]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,50}$", ErrorMessageResourceName = "PasswordHasAnInvalidFormat", ErrorMessageResourceType = typeof(Messages))]
        public string Password { get; set; }

        [Display(Name = "ConfirmPassword", ResourceType = typeof(Labels))]
        [Compare("Password", ErrorMessageResourceName = "InvalidConfirmPassword", ErrorMessageResourceType = typeof(Messages))]
        public string ConfirmPassword { get; set; }

        public bool IsParked { get; set; }

        [Required(ErrorMessageResourceName = "PleaseEnterEmail", ErrorMessageResourceType = typeof(Messages))]
        [EmailAddress(ErrorMessage = null, ErrorMessageResourceName = "NotValidEmail", ErrorMessageResourceType = typeof(Messages))]
        [Display(Name = "User_Email", ResourceType = typeof(Labels))]
        public string Email { get; set; }

        public string UserName { get; set; }

        public string RoleName { get; set; }

        [Display(Name = "Language", ResourceType = typeof(Labels))]
        public int LanguageId { get; set; }

        public string UserImageFileName { get; set; }
        public string UserImageOriginalFileName { get; set; }
        public string UserImageDownloadUrl { get; set; }
    }
}
