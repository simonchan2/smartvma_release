﻿using SmartVMA.Resources;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class PasswordForgotViewModel
    {
        [Display(Name = "User_Email", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "PleaseEnterEmail", ErrorMessageResourceType = typeof(Messages))]
        [EmailAddress(ErrorMessage = null, ErrorMessageResourceName = "NotValidEmail", ErrorMessageResourceType = typeof(Messages))]
        public string Email { get; set; }

        public string PasswordForgotField { get; set; }
    }
}
