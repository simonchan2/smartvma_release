﻿using SmartVMA.Core.Contracts;
using SmartVMA.Resources;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class UserIndexViewModel : BaseIndexViewModel<UserViewModel>
    {
        [Display(Name = "Company", ResourceType = typeof(Labels))]
        public int? CompanyId { get; set; }
    }
}
