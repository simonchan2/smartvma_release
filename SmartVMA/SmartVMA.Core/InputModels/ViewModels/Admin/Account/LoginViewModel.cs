﻿using SmartVMA.Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessageResourceName = "PleaseEnterEmail", ErrorMessageResourceType = typeof(Messages))]
        [EmailAddress(ErrorMessage = null, ErrorMessageResourceName = "NotValidEmail", ErrorMessageResourceType = typeof(Messages))]
        [Display(Name = "User_Email", ResourceType = typeof(Labels))]
        public string Email { get; set; }

        [Required(ErrorMessageResourceName = "PleaseEnterPassword", ErrorMessageResourceType = typeof(Messages))]
        [Display(Name = "Password", ResourceType = typeof(Labels))]
        public string Password { get; set; }

        [Display(Name = "RememberMe", ResourceType = typeof(Labels))]
        public bool RememberMe { get; set; }

        public bool IsSuccess { get; set; }

        public int? TenantId { get; set; }
    }
}