﻿using System.Collections.Generic;
using SmartVMA.Resources;
using System.ComponentModel.DataAnnotations;
using SmartVMA.Infrastructure;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class UserViewModel
    {
        public UserViewModel()
        {
            Tenants = new List<int>();
            UserRoles = new List<UserCompanyRoles>();
        }

        public long? Id { get; set; }

        [Display(Name = "User_UserStatusCodeId", ResourceType = typeof(Labels))]
        public int UserStatusCodeId { get; set; }

        [Required]
        [StringLength(255)]
        [Display(Name = "User_Email", ResourceType = typeof(Labels))]
        [EmailAddress(ErrorMessage = null, ErrorMessageResourceName = "NotValidEmail", ErrorMessageResourceType = typeof(Messages))]
        public string Email { get; set; }

        [Required]
        [StringLength(150)]
        [Display(Name = "User_LastName", ResourceType = typeof(Labels))]
        public string LastName { get; set; }

        [Required]
        [StringLength(150)]
        [Display(Name = "User_FirstName", ResourceType = typeof(Labels))]
        public string FirstName { get; set; }

        [StringLength(25)]
        [Display(Name = "User_MiddleName", ResourceType = typeof(Labels))]
        public string MiddleName { get; set; }
        
        [Required(ErrorMessageResourceName = "RoleIsRequired", ErrorMessageResourceType = typeof(Messages))]
        [Display(Name = "Role_Name", ResourceType = typeof(Labels))]
        public int? RoleId { get; set; }

        [Required]
        [Display(Name = "Tenant_Name", ResourceType = typeof(Labels))]
        public IEnumerable<int> Tenants { get; set; }

        public List<UserCompanyRoles> UserRoles { get; set; }

        public int? TenantId { get; set; }

        [Required]
        [Display(Name = "PhoneNumber", ResourceType = typeof(Labels))]
        [RegularExpression(RegExp.PhoneNumber, ErrorMessage = null, ErrorMessageResourceName = "InvalidPhoneNumber", ErrorMessageResourceType = typeof(Messages))]
        public string PhoneNumber { get; set; }

        [Required]
        [Display(Name = "User_Title", ResourceType = typeof(Labels))]
        public string Title { get; set; }

        [Display(Name = "User_IsLocked", ResourceType = typeof(Labels))]
        public bool IsLocked { get; set; }

        [Display(Name = "User_AdvisorCode", ResourceType = typeof(Labels))]
        public string AdvisorCode { get; set; }

        [Display(Name = "FullName", ResourceType = typeof(Labels))]
        public string FullName { get; set; }

        [Display(Name = "Role_Role", ResourceType = typeof(Labels))]
        public string RoleName { get; set; }

        [Display(Name = "Company", ResourceType = typeof(Labels))]
        public string CompanyName { get; set; }

        [Display(Name = "Company", ResourceType = typeof(Labels))]
        public int? CompanyId { get; set; }

        public string Actions { get; set; }

        public class UserCompanyRoles
        {
            public int TenantId { get; set; }
            [Display(Name = "AdvisorCode", ResourceType = typeof(Labels))]
            public string DmsUserNumber { get; set; }
            public string CompanyName { get; set; }
        }
    }
}