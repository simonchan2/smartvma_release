﻿using SmartVMA.Core.Contracts;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class LoginResponseViewModel : ServiceResponse
    {
        public LoginViewModel Model { get; set; }
        public bool ShowStoreSelectPage { get; set; }
    }
}
