﻿using SmartVMA.Resources;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class PasswordChangeViewModel
    {
        [Display(Name = "CurrentPassword", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "PleaseEnterPassword", ErrorMessageResourceType = typeof(Messages))]
        public string OldPassword { get; set; }

        [Display(Name = "Password", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "PleaseEnterPassword", ErrorMessageResourceType = typeof(Messages))]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,50}$", ErrorMessageResourceName = "PasswordNotShorterThan8Characters", ErrorMessageResourceType = typeof(Messages))]
        public string Password { get; set; }


        [Display(Name = "ConfirmPassword", ResourceType = typeof(Labels))]
        [Required(ErrorMessageResourceName = "PleaseEnterConfirmPassword", ErrorMessageResourceType = typeof(Messages))]
        [Compare("Password", ErrorMessageResourceName = "InvalidConfirmPassword", ErrorMessageResourceType = typeof(Messages))]
        public string ConfirmPassword { get; set; }
    }
}
