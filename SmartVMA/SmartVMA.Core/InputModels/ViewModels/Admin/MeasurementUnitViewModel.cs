﻿namespace SmartVMA.Core.InputModels.ViewModels
{
    public class MeasurementUnitViewModel
    {
        public int Id { get; internal set; }
        public string Description { get; internal set; }
    }
}
