﻿using SmartVMA.Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class CompanyGroupViewModel
    {
        public CompanyGroupViewModel()
        {
            DealerIds = new List<int>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        [Display(Name = "DealerGroup_Name", ResourceType = typeof(Labels))]
        public string Name { get; set; }

        [Display(Name = "Description", ResourceType = typeof(Labels))]
        public string Description { get; set; }

        [Display(Name = "Configuration_DistributorId", ResourceType = typeof(Labels))]
        public int DistributorId { get; set; }

        [Display(Name = "Configuration_DistributorId", ResourceType = typeof(Labels))]
        public string Distributor { get; set; }

        [Display(Name = "Dealers", ResourceType = typeof(Labels))]
        public IEnumerable<int> DealerIds { get; set; }

        public string Actions { get; set; }
    }
}