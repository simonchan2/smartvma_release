﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class BgProductCategoryInfo
    {
        public int Id { get; set; }
        public string Category { get; set; }
    }
}
