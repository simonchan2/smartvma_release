﻿namespace SmartVMA.Core.InputModels.ViewModels
{
    public class DMSTypesViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SortOrder { get; set; }
    }
}
