﻿namespace SmartVMA.Core.InputModels.ViewModels
{
    public class LanguageViewModel
    {
        public int Id { get; set; }
        public string LanguageName { get; set; }
        public string LanguageCode { get; set; }
    }
}
