namespace SmartVMA.Core.InputModels.ViewModels
{
    public class CustomServiceInitViewModel
    {
        public CustomServicesVehicleFilterViewModel VehicleFilter { get; set; }

        public CustomServiceViewModel Data { get; set; }

        public string SelectedServices { get; set; }
    }
}