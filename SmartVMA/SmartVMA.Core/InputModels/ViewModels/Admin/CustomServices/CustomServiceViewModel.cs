﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SmartVMA.Resources;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class CustomServiceViewModel
    {
        public CustomServiceViewModel()
        {
            Parts = new List<CustomServicePartViewModel>();
            ViewModelType = "CustomService";
        }

        public long? Id { get; set; }

        [StringLength(50)]
        [Display(Name = "OpCode", ResourceType = typeof(Labels))]
        public string OpCode { get; set; }

        [StringLength(255)]
        [Required]
        [Display(Name = "Description", ResourceType = typeof(Labels))]
        public string OpDescription { get; set; }

        [Display(Name = "Comment", ResourceType = typeof(Labels))]
        public string Comment { get; set; }

        [Required]
        [Display(Name = "Time", ResourceType = typeof(Labels))]
        public double? LaborHour { get; set; }

        [Display(Name = "Price", ResourceType = typeof(Labels))]
        public decimal Price { get; set; }

        [Display(Name = "LOF", ResourceType = typeof(Labels))]
        public bool IsLofService { get; set; }

        [Display(Name = "Start", ResourceType = typeof(Labels))]
        public int? IntervalStart { get; set; }

        [Display(Name = "Repeat", ResourceType = typeof(Labels))]
        public int? IntervalRepeat { get; set; }

        //[Display(Name = "MenuLevel", ResourceType = typeof(Labels))]
        //public byte? MenuLevel { get; set; }

        [Display(Name = "AdvisorIncentiveShort", ResourceType = typeof(Labels))]
        public decimal? AdvisorIncentive { get; set; }

        [Display(Name = "TechIncentive", ResourceType = typeof(Labels))]
        public decimal? TechIncentive { get; set; }

        [Display(Name = "Strike", ResourceType = typeof(Labels))]
        public bool CanBeStriked { get; set; }

        [Display(Name = "ProtectionPlan", ResourceType = typeof(Labels))]
        public int? BgProtectionPlanId { get; set; }

        [Display(Name = "Category", ResourceType = typeof(Labels))]
        public int? CategoryId { get; set; }

        [Display(Name = "LifetimeProtectionSubPlan", ResourceType = typeof(Labels))]
        public int? BgProtectionSubPlanId { get; set; }

        [Display(Name = "MenuPresentation_Video", ResourceType = typeof(Labels))]
        public int? VideoId { get; set; }

        [Display(Name = "Rate", ResourceType = typeof(Labels))]
        public decimal? LaborRate { get; set; }

        // missing from mockup
        public bool IsFixLabor { get; set; }

        public bool IsSelected { get; set; }

        public List<CustomServicePartViewModel> Parts { get; set; }
        public string RowId { get; set; }
        public string BgProtectionPlanName { get; set; }
        public string BgProtectionSubPlanName { get; set; }

        public string BgProdCategoryName { get; set; }
        public string ViewModelType { get; set; }
    }

    public class CustomServicePartViewModel
    {
        [StringLength(20)]
        [Display(Name = @"#")]
        public string PartNo { get; set; }

        private double? _quantity;

        [Display(Name = "Qty", ResourceType = typeof(Labels))]
        public double? Quantity
        {
            get
            {
                if (_quantity.HasValue)
                {
                    return Math.Round(_quantity.Value, 1);
                }
                return null;
            }
            set { _quantity = value; }
        }

        [Display(Name = "Price", ResourceType = typeof(Labels))]
        public decimal? UnitPrice { get; set; }

        [Display(Name = "Fluid", ResourceType = typeof(Labels))]
        public bool IsFluid { get; set; }

        [Display(Name = "BGSKU", ResourceType = typeof(Labels))]
        public int? BgProductId { get; set; }
        public long? Id { get; set; }
        public string BgProductName { get; set; }
        public bool IsDeleted { get; set; }
    }
}
