﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SmartVMA.Resources;

namespace SmartVMA.Core.InputModels.ViewModels.Admin.CustomServices
{
    public class MapCustomServicesToVehiclesViewModel
    {
        public List<MapCustomServicesItemsViewModel> Items { get; set; }

        [Required]
        [Display(ResourceType = typeof(Labels), Name = "Year")]
        public IEnumerable<int> Years { get; set; }

        [Required]
        [Display(ResourceType = typeof(Labels), Name = "Make")]
        public IEnumerable<int> Makes { get; set; }

        [Required]
        [Display(ResourceType = typeof(Labels), Name = "Model")]
        public IEnumerable<int> Models { get; set; }

        [Required]
        [Display(ResourceType = typeof(Labels), Name = "Engine")]
        public IEnumerable<int> EngineTypes { get; set; }

        [Required]
        [Display(ResourceType = typeof(Labels), Name = "Transmission")]
        public IEnumerable<int> TransmissionTypes { get; set; }

        [Required]
        [Display(ResourceType = typeof(Labels), Name = "DriveLine")]
        public IEnumerable<int> DriveLines { get; set; }
        public int MenuLevel { get; set; }
        public string SelectedServices { get; set; }
    }


    public class MapCustomServicesItemsViewModel
    {
        public int Id { get; set; }

        public bool IsSelected { get; set; }
    }
}
