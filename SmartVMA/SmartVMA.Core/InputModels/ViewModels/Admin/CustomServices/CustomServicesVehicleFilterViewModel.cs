﻿using SmartVMA.Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class CustomServicesVehicleFilterViewModel
    {
        [Display(ResourceType = typeof(Labels), Name = "ServiceKeyword")]
        [StringLength(int.MaxValue, MinimumLength = 3, ErrorMessage = null, ErrorMessageResourceName = "SearchFieldMinLength3Only", ErrorMessageResourceType = typeof(Messages))]
        public string ServiceKeyword { get; set; }

        public string OpCode { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "Year")]
        public IEnumerable<int> Years { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "Make")]
        public IEnumerable<int> Makes { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "Model")]
        public IEnumerable<int> Models { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "Engine")]
        public IEnumerable<int> EngineTypes { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "Transmission")]
        public IEnumerable<int> TransmissionTypes { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "DriveLine")]
        public IEnumerable<int> DriveLines { get; set; }


        [Display(ResourceType = typeof(Labels), Name = "MenuLevel")]
        public int MenuLevel { get; set; }

    }
}
