﻿using SmartVMA.Resources;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class AdditionalServiceViewModel
    {
        public int? Id { get; set; }

        [Required]
        [Display(Name = "ServiceDescription", ResourceType = typeof(Labels))]
        public string Description { get; set; }

        [Required]
        [Display(Name = "ServicePrice", ResourceType = typeof(Labels))]
        public double? Price { get; set; }

        [Required]
        [Range(0, double.MaxValue)]
        [Display(Name = "LaborHours", ResourceType = typeof(Labels))]
        public double? LaborHours { get; set; }
    }
}