﻿using SmartVMA.Core.Contracts;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class CustomServiceIndexViewModel : BaseIndexViewModel<CustomServiceViewModel>
    {
        public string FilteredServiceIds { get; set; }
    }
}
