﻿using SmartVMA.Resources;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class AlertViewModel
    {
        public int? Id { get; set; }

        [Required]
        [Display(ResourceType = typeof(Labels), Name = "Description")]
        [AllowHtml]
        [StringLength(255, MinimumLength = 0, ErrorMessage = null, ErrorMessageResourceName = "MaxLengthAlerts", ErrorMessageResourceType = typeof(Messages))]
        public string Description { get; set; }

        [Required]
        [Display(ResourceType = typeof(Labels), Name = "Issue")]
        [StringLength(50, MinimumLength = 0, ErrorMessage = null, ErrorMessageResourceName = "MaxLengthAlertsTitle", ErrorMessageResourceType = typeof(Messages))]
        public string Title { get; set; }

        [Required]
        [Display(ResourceType = typeof(Labels), Name = "StartDate")]
        public DateTime? StartDate { get; set; }

        [Required]
        [Display(ResourceType = typeof(Labels), Name = "EndDate")]
        public DateTime? EndDate { get; set; }

        public long? CreatorUserId { get; set; }
        public string Status { get; set; } // returns user input value -> clicked button
        public int TypeId { get; set; }
        public string TypeName { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public string Actions { get; set; }

        public string StartDateForPresentation { get; set; }

        public string EndDateForPresentation { get; set; }

    }
}
