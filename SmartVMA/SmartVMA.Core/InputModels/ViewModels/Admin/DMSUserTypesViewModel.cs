﻿namespace SmartVMA.Core.InputModels.ViewModels
{
    public class DMSUserTypesViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
