﻿using SmartVMA.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SmartVMA.Core.InputModels.ViewModels
{
    public class DeleteObsoleteAppointmentsViewModel
    {
        public DateTime DateTo { get; set; }
    }
}
