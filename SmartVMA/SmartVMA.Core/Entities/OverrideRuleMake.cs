using System;

namespace SmartVMA.Core.Entities
{
    internal partial class OverrideRuleMake
    {
        public long Id { get; set; }

        public long OverrideRuleId { get; set; }

        public int VehicleMakeId { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual VehicleMake VehicleMake { get; set; }

        public virtual OverrideRule OverrideRule { get; set; }
    }
}
