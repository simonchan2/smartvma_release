using SmartVMA.Core.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static SmartVMA.Core.InputModels.ViewModels.UserViewModel;

namespace SmartVMA.Core.Entities
{
    [Table("dealer.AppointmentPresentations")]
    internal partial class AppointmentPresentation : IBaseTenantEntity<long>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AppointmentPresentation()
        {
            AppointmentServices = new HashSet<AppointmentService>();
            Inspections = new HashSet<Inspection>();
            PdfDocuments = new HashSet<PdfDocument>();
        }

        [Column("AppointmentPresentationId")]
        public long Id { get; set; }

        [Column("DealerId")]
        public int TenantId { get; set; }

        public long? AppointmentId { get; set; }

        public long? AdvisorId { get; set; }

        public int? DealerCustomerId { get; set; }

        public int DealerVehicleId { get; set; }

        [StringLength(15)]
        public string VehicleLicense { get; set; }

        public int Mileage { get; set; }

        public int MeasurementUnitId { get; set; }

        [StringLength(10)]
        public string PresentationStatus { get; set; }

        public DateTime? ParkedTime { get; set; }

        public DateTime? AppointmentTime { get; set; }

        public double? AppointmentDuration { get; set; }

        public DateTime? PromisedTime { get; set; }

        [StringLength(50)]
        public string CustomerPhone { get; set; }

        [StringLength(50)]
        public string MenuOpCode { get; set; }

        public bool? IsLofMenu { get; set; }

        public int? MenuMileage { get; set; }

        public byte? MenuLevel { get; set; }

        [Column(TypeName = "xml")]
        public string WalkAroundInspection { get; set; }

        public DateTime? ServiceTime { get; set; }

        [StringLength(10)]
        public string ServiceType { get; set; }

        [StringLength(50)]
        public string RoNumber { get; set; }

        public DateTime? DateRoCreated { get; set; }

        [StringLength(25)]
        public string OrderType { get; set; }

        [StringLength(255)]
        public string RoStatusMsg { get; set; }

        [StringLength(50)]
        public string InvoiceNumber { get; set; }

        [StringLength(15)]
        public string TagNumber { get; set; }

        [StringLength(15)]
        public string TagColor { get; set; }

        public bool? Waiter { get; set; }

        [StringLength(20)]
        public string TransportationType { get; set; }

        [StringLength(255)]
        public string Events { get; set; }

        public string SignatureAccept { get; set; }

        public string SignatureDeclined { get; set; }
        public double? ShopChargesAmount { get; set; }
        public double? TaxChargesAmount { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? PartitionElement { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [ForeignKey("TenantId")]
        public virtual Company Company { get; set; }

        public virtual MeasurementUnit MeasurementUnit { get; set; }

        public virtual User User { get; set; }

        public virtual Appointment Appointment { get; set; }

        public virtual DealerCustomer DealerCustomer { get; set; }

        public virtual DealerVehicle DealerVehicle { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppointmentService> AppointmentServices { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Inspection> Inspections { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PdfDocument> PdfDocuments { get; set; }
    }
}
