using SmartVMA.Core.Contracts;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    internal partial class UserCompanyRole : IBaseTenantEntity<long>
    {
        public long Id { get; set; }

        public long UserId { get; set; }

        public int RoleId { get; set; }

        [Column("CompanyId", Order = 3)]
        public int TenantId { get; set; }

        public string DmsUserNumber { get; set; }

        public DateTime TimeUpdated { get; set; }

        public DateTime TimeCreated { get; set; }

        [ForeignKey("TenantId")]
        public virtual Company Company { get; set; }

        public virtual Role Role { get; set; }

        public virtual User User { get; set; }
    }
}
