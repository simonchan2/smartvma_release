using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SmartVMA.Core.Contracts;

namespace SmartVMA.Core.Entities
{
    [Table("bg.BgVideoClips")]
    internal partial class BgVideoClip : IBaseEntity<int>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BgVideoClip()
        {
            BgVideoClipEditions = new HashSet<BgVideoClipEdition>();
            AdditionalServiceVideoClips = new HashSet<AdditionalServiceVideoClip>();
        }

        [Column("BgVideoClipId")]
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Description { get; set; }

        public string Comment { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BgVideoClipEdition> BgVideoClipEditions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AdditionalServiceVideoClip> AdditionalServiceVideoClips { get; set; }
    }
}
