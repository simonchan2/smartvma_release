using System;

namespace SmartVMA.Core.Entities
{
    internal partial class OverrideRuleYear
    {
        public long Id { get; set; }

        public DateTime TimeUpdated { get; set; }

        public DateTime TimeCreated { get; set; }

        public long OverrideRuleId { get; set; }

        public int VehicleYear { get; set; }

        public virtual OverrideRule OverrideRule { get; set; }
    }
}
