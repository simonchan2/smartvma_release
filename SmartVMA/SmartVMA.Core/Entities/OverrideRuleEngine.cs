using System;

namespace SmartVMA.Core.Entities
{
    internal partial class OverrideRuleEngine
    {
        public long Id { get; set; }

        public long OverrideRuleId { get; set; }

        public int VehicleEngineId { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual VehicleEngine VehicleEngine { get; set; }

        public virtual OverrideRule OverrideRule { get; set; }
    }
}
