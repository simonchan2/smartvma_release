using System;

namespace SmartVMA.Core.Entities
{
    internal partial class StateTimeZone
    {
        public int StateTimeZoneId { get; set; }

        public int CountryStateId { get; set; }

        public int TimeZoneId { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual TimeZone TimeZone { get; set; }
    }
}
