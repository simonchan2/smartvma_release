using System;

namespace SmartVMA.Core.Entities
{
    internal partial class VinWeight
    {
        public int VinWeightId { get; set; }

        public int Weight { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }
    }
}
