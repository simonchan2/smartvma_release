using SmartVMA.Core.Contracts;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("msg.AlertsUsers")]
    internal partial class AlertsUser: IBaseEntity<int>
    {
        public int Id { get; set; }

        public long UserId { get; set; }

        public int AlertId { get; set; }

        public int StatusId { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual User User { get; set; }

        public virtual Alert Alert { get; set; }

        public virtual AlertsStatus AlertsStatus { get; set; }
    }
}
