using System;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.Entities
{
    internal partial class VinDigit
    {
        public int VinDigitId { get; set; }

        [Required]
        [StringLength(1)]
        public string Digit { get; set; }

        public int Value { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }
    }
}
