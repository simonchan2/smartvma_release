using SmartVMA.Core.Contracts;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace SmartVMA.Core.Entities
{
    [Table("amam.CarVinShorts")]
    internal class CarVinShort : IBaseEntity<int>
    {
        [Column("CarVinShortId")]
        public int Id { get; set; }

        public int CarExtId { get; set; }

        public int? CarId { get; set; }

        public int? VinMasterId { get; set; }

        [Required]
        [StringLength(15)]
        public string VinShort { get; set; }

        [Required]
        [StringLength(25)]
        public string VIN { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public Car Car { get; set; }

        public VinMaster VinMaster { get; set; }
    }
}
