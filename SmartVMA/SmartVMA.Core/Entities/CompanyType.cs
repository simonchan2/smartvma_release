using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.Entities
{
    internal partial class CompanyType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CompanyType()
        {
            Companies = new HashSet<Company>();
        }

        public int CompanyTypeId { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Company> Companies { get; set; }
    }
}
