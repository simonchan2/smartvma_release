using System;

namespace SmartVMA.Core.Entities
{
    internal partial class OverrideRuleBasicService
    {
        public long Id { get; set; }

        public int OemBasicServiceId { get; set; }

        public long OverrideRuleId { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual OemBasicService OemBasicService { get; set; }

        public virtual OverrideRule OverrideRule { get; set; }
    }
}
