using SmartVMA.Core.Contracts;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("dbo.AdditionalServicesCars")]
    internal class AdditionalServicesCar : IBaseEntity<long>
    {
        [Key]
        [Column("AdditionalServiceId")]
        public long Id { get; set; }

        public long AdditionalServiceId { get; set; }

        public byte MenuLevel{ get; set; }
        
        public int CarId{ get; set; }
        
        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }
    }
}