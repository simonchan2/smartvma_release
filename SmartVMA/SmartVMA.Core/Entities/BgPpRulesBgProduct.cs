using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{   
    [Table("bg.BgPpRulesBgProducts")]
    internal partial class BgPpRulesBgProduct
    {
        [Key]
        public int BgPpRulesBgProductId { get; set; }

        public int BgPpRuleId { get; set; }

        public int BgProductId { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual BgPpRule BgPpRule { get; set; }

        public virtual BgProduct BgProduct { get; set; }
    }
}
