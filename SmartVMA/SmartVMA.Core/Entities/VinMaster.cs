using SmartVMA.Core.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("vinquery.VinMaster")]
    internal partial class VinMaster : IBaseEntity<int>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public VinMaster()
        {
            Vehicles = new HashSet<Vehicle>();
            CarVinShorts = new HashSet<CarVinShort>();
        }

        [Column("VinMasterId")]
        public int Id { get; set; }

        [Required]
        [StringLength(15)]
        public string VinShort { get; set; }

        [Required]
        [StringLength(25)]
        public string VIN { get; set; }

        public int VehicleYear { get; set; }

        [Required]
        [StringLength(50)]
        public string VehicleMake { get; set; }

        [Required]
        [StringLength(50)]
        public string VehicleModel { get; set; }

        [StringLength(50)]
        public string VehicleEngine { get; set; }

        [StringLength(120)]
        public string VehicleTransmission { get; set; }

        [StringLength(50)]
        public string VehicleDriveLine { get; set; }

        [StringLength(50)]
        public string VehicleSteering { get; set; }

        [StringLength(50)]
        public string VehicleTireSize { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public DateTime TimeChecked { get; set; }

        public int? VinMasterExtId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [StringLength(5)]
        public string VehicleTransmissionShort { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [StringLength(50)]
        public string VehicleDrivelineShort { get; set; }

        public int VinMasterTransmissionId { get; set; }

        public int VinMasterDrivelineId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Vehicle> Vehicles { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CarVinShort> CarVinShorts { get; set; }

        public virtual VinMasterDriveline VinMasterDriveline { get; set; }

        public virtual VinMasterTransmission VinMasterTransmission { get; set; }
    }
}
