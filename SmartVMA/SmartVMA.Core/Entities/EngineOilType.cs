using SmartVMA.Core.Contracts;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("dbo.EngineOilTypes")]
    internal partial class EngineOilType : IBaseEntity<int>
    {

        [Column("EngineOilTypeId")]
        public int Id { get; set; }

        [Required]
        [StringLength(25)]
        [Column("EngineOilType")]
        public string OilType { get; set; }
    
        [Required]
        public int SortOrder { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }
    }
}
