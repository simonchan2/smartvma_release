using SmartVMA.Core.Contracts;
using System;

namespace SmartVMA.Core.Entities
{
    internal partial class RolePermission : IBaseEntity<long>
    {
        public long Id { get; set; }

        public int RoleId { get; set; }

        public int PermissionId { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual Permission Permission { get; set; }

        public virtual Role Role { get; set; }
    }
}
