using SmartVMA.Core.Contracts;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("kb.ArticleArticleCategory")]
    internal partial class ArticleArticleCategory : IBaseEntity<int>
    {
        public int Id { get; set; }

        public int ArticleId { get; set; }

        public int ArticleCategoryId { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual Article Article { get; set; }

        public virtual ArticleCategory ArticleCategory { get; set; }
    }
}
