using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("rprint.PrnPrinters")]
    internal partial class PrnPrinter
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PrnPrinter()
        {
            PrnConfigPrinters = new HashSet<PrnConfigPrinter>();
        }

        public int PrnPrinterId { get; set; }

        public int DealerId { get; set; }

        [Required]
        [StringLength(100)]
        public string PrinterName { get; set; }

        [Required]
        [StringLength(100)]
        public string PrinterDriver { get; set; }

        [Required]
        [StringLength(100)]
        public string PrinterPort { get; set; }

        public bool IsColorPrinter { get; set; }

        [StringLength(45)]
        public string Login { get; set; }

        [StringLength(45)]
        public string Password { get; set; }

        public DateTime TimeUpdated { get; set; }

        public DateTime TimeCreated { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PrnConfigPrinter> PrnConfigPrinters { get; set; }
    }
}
