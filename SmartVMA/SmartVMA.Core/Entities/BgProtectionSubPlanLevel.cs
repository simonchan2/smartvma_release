using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("bg.BgProtectionSubPlanLevels")]
    internal partial class BgProtectionSubPlanLevel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BgProtectionSubPlanLevel()
        {
            DealerVehiclesPpRecords = new HashSet<DealerVehiclesPpRecord>();
        }

        public int BgProtectionSubPlanLevelId { get; set; }

        public int BgProtectionSubPlanId { get; set; }

        public byte PlanLevel { get; set; }

        [Column(TypeName = "money")]
        public decimal MaxReimbursement { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual BgProtectionSubPlan BgProtectionSubPlan { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DealerVehiclesPpRecord> DealerVehiclesPpRecords { get; set; }
    }
}
