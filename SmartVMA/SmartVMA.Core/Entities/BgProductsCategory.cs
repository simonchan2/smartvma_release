namespace SmartVMA.Core.Entities
{
    using Contracts;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    [Table("bg.BgProductsCategories")]
    internal partial class BgProductsCategory : IBaseEntity<int>
    {
        [Column("BgProductsCategoryId")]
        public int Id { get; set; }

        public int BgProductId { get; set; }

        public int BgProdCategoryId { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual BgProdCategory BgProdCategory { get; set; }

        public virtual BgProduct BgProduct { get; set; }
    }
}
