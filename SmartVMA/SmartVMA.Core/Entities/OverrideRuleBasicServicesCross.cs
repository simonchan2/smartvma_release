using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("dealer.OverrideRuleBasicServicesCross")]
    internal partial class OverrideRuleBasicServicesCross
    {
        public long Id { get; set; }

        public int DealerId { get; set; }

        public int OemBasicServiceId { get; set; }

        public long? LaborHourRuleId { get; set; }

        public long? LaborPriceRuleId { get; set; }

        public long? PartPriceRuleId { get; set; }

        public long? OpCodeRuleId { get; set; }

        public long? PartNoRuleId { get; set; }

        public DateTime TimeUpdated { get; set; }

        public DateTime TimeCreated { get; set; }

        public virtual OemBasicService OemBasicService { get; set; }

        public virtual Company Company { get; set; }

        public virtual OverrideRule OverrideRule { get; set; }

        public virtual OverrideRule OverrideRule1 { get; set; }

        public virtual OverrideRule OverrideRule2 { get; set; }

        public virtual OverrideRule OverrideRule3 { get; set; }

        public virtual OverrideRule OverrideRule4 { get; set; }
    }
}
