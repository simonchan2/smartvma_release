using SmartVMA.Core.Contracts;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("vi.VehiclePhotos")]
    internal partial class VehiclePhoto : IBaseEntity<long>
    {
        public long Id { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public long InspectionId { get; set; }

        [Required]
        [StringLength(255)]
        public string FileName { get; set; }
    }
}
