using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SmartVMA.Core.Contracts;

namespace SmartVMA.Core.Entities
{
    [Table("bg.BgProtectionSubPlans")]
    internal partial class BgProtectionSubPlan : IBaseEntity<int>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BgProtectionSubPlan()
        {
            BgPpRules = new HashSet<BgPpRule>();
            BgProtectionSubPlanLevels = new HashSet<BgProtectionSubPlanLevel>();
            BgProtectionSubPlanMus = new HashSet<BgProtectionSubPlanMu>();
            AdditionalServices = new HashSet<AdditionalService>();
        }

        [Column("BgProtectionSubPlanId")]
        public int Id { get; set; }

        public int BgProtectionPlanId { get; set; }

        [Required]
        [StringLength(100)]
        public string SubPlanName { get; set; }

        public int? HourInterval { get; set; }

        public string Comment { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BgPpRule> BgPpRules { get; set; }

        public virtual BgProtectionPlan BgProtectionPlan { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BgProtectionSubPlanLevel> BgProtectionSubPlanLevels { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BgProtectionSubPlanMu> BgProtectionSubPlanMus { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AdditionalService> AdditionalServices { get; set; }
    }
}
