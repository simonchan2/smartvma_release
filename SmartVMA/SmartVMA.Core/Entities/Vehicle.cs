namespace SmartVMA.Core.Entities
{
    using System;
    using SmartVMA.Core.Contracts;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    internal partial class Vehicle : IBaseEntity<int>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Vehicle()
        {
            DealerVehicles = new HashSet<DealerVehicle>();
        }
        [Key]
        [Column("VehicleId")]
        public int Id { get; set; }

        public int? VinMasterId { get; set; }

        public int? CarId { get; set; }

        [Required]
        [StringLength(25)]
        public string VIN { get; set; }

        public string Comment { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [StringLength(10)]
        public string VinShort { get; set; }

        public virtual Car Car { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DealerVehicle> DealerVehicles { get; set; }

        public virtual VinMaster VinMaster { get; set; }

        public int? VehicleStatus { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public bool? CarIdUndetermined { get; set; }
    }
}
