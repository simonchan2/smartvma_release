using SmartVMA.Core.Contracts;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("document.PdfDocuments")]
    internal partial class PdfDocument : IBaseTenantEntity<int>
    {
        [Column("PdfDocumentId")]
        public int Id { get; set; }

        [Column("DealerId")]
        public int TenantId { get; set; }

        public int? DealerCustomerId { get; set; }
        [Required]
        public long? AppointmentPresentationId { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [StringLength(255)]
        public string FilePathName { get; set; }

        [StringLength(25)]
        public string VIN { get; set; }
        
        [StringLength(50)]
        public string InvoiceNumber { get; set; }

        [Required]
        public int DocumentType { get; set; }

        public DateTime TimeCreated { get; set; }
        public DateTime TimeUpdated { get; set; }

        public int? LanguageId { get; set; }

        public virtual AppointmentPresentation AppointmentPresentation { get; set; }

        public virtual DealerCustomer DealerCustomer { get; set; }

        [ForeignKey("TenantId")]
        public virtual Company Company { get; set; }

        public virtual Language Language { get; set; }
    }
}
