using SmartVMA.Core.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("amam.Cars")]
    internal partial class Car : IBaseEntity<int>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Car()
        {
            OemBasicServices = new HashSet<OemBasicService>();
            OemMenuLists = new HashSet<OemMenuList>();
            OemParts = new HashSet<OemPart>();
            Vehicles = new HashSet<Vehicle>();
            CarVinShorts = new HashSet<CarVinShort>();
        }

        [Column("CarId")]
        public int Id { get; set; }

        public int VehicleYear { get; set; }

        public int VehicleMakeId { get; set; }

        public int VehicleModelId { get; set; }

        public int VehicleEngineId { get; set; }

        [Required]
        [StringLength(200)]
        public string Description { get; set; }

        public int CarExtId { get; set; }

        [StringLength(50)]
        public string ExtInfoChecksum { get; set; }

        public bool NoOverwrite { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public DateTime TimeChecked { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [StringLength(3)]
        public string VehicleDriveLine { get; set; }

        public virtual VehicleEngine VehicleEngine { get; set; }

        public virtual VehicleMake VehicleMake { get; set; }

        public virtual VehicleModel VehicleModel { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemBasicService> OemBasicServices { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemMenuList> OemMenuLists { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemPart> OemParts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Vehicle> Vehicles { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CarVinShort> CarVinShorts { get; set; }
    }
}
