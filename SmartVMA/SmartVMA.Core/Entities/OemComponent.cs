namespace SmartVMA.Core.Entities
{
    using Contracts;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    [Table("amam.OemComponents")]
    internal partial class OemComponent : IBaseEntity<int>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OemComponent()
        {
            OemBasicServices = new HashSet<OemBasicService>();
            OemMenuLists = new HashSet<OemMenuList>();
            OemMenuLists1 = new HashSet<OemMenuList>();
            OemMenus = new HashSet<OemMenu>();
            OemParts = new HashSet<OemPart>();
        }
        [Column("OemComponentId")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [Required]
        [StringLength(200)]
        public string Description { get; set; }

        public bool IsFluid { get; set; }

        public bool IsEngineOil { get; set; }

        public bool IsLOF { get; set; }

        public bool ToBeIgnored { get; set; }

        public bool? IsShownAsOption { get; set; }

        public int? LaborTimeRuleId { get; set; }

        public int? PartRuleId { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [StringLength(4)]
        public string VehicleTransmissionShort { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [StringLength(5)]
        public string VehicleDrivelineShort { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemBasicService> OemBasicServices { get; set; }

        public virtual OemComponentRule OemComponentRule { get; set; }

        public virtual OemComponentRule OemComponentRule1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemMenuList> OemMenuLists { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemMenuList> OemMenuLists1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemMenu> OemMenus { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemPart> OemParts { get; set; }
    }
}
