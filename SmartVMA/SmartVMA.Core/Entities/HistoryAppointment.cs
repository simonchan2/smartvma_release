namespace SmartVMA.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("dealer.HistoryAppointments")]
    internal partial class HistoryAppointment
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public HistoryAppointment()
        {
            HistoryAppointmentDetails = new HashSet<HistoryAppointmentDetail>();
        }

        [Key]
        public long AppointmentId { get; set; }

        public int DealerId { get; set; }

        public int DealerVehicleId { get; set; }

        public int DealerCustomerId { get; set; }

        public int UserId { get; set; }

        [StringLength(25)]
        public string AdvCode { get; set; }

        [StringLength(50)]
        public string AppointmentDmsId { get; set; }

        [StringLength(50)]
        public string UserDmsId { get; set; }

        [Column(TypeName = "date")]
        public DateTime AppointmentDate { get; set; }

        public DateTime AppointmentTime { get; set; }

        [Required]
        [StringLength(10)]
        public string AppointmentStatus { get; set; }

        [StringLength(5)]
        public string AppointmentType { get; set; }

        [Required]
        [StringLength(5)]
        public string AppUse { get; set; }

        public double? AppointmentDuration { get; set; }

        public DateTime? PromisedTime { get; set; }

        [StringLength(50)]
        public string CustomerPhone { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ServiceDate { get; set; }

        public int? ContactBy { get; set; }

        public DateTime? ContactTime { get; set; }

        public bool Confirmed { get; set; }

        [StringLength(15)]
        public string VehicleLicense { get; set; }

        public int? Mileage { get; set; }

        public bool Waiter { get; set; }

        [StringLength(50)]
        public string RoNumber { get; set; }

        public DateTime? DateRoCreated { get; set; }

        [StringLength(255)]
        public string RoStatus { get; set; }

        [StringLength(1)]
        public string NewRo { get; set; }

        [StringLength(25)]
        public string RoOpenBy { get; set; }

        [StringLength(50)]
        public string InvoiceNumber { get; set; }

        [StringLength(15)]
        public string TagNumber { get; set; }

        [StringLength(15)]
        public string TagColor { get; set; }

        public int PotNum { get; set; }

        [Required]
        [StringLength(5)]
        public string ComeBack { get; set; }

        public short? Fwa { get; set; }

        public short? Fmac { get; set; }

        public short? Fpoi { get; set; }

        public int? Fuel { get; set; }

        public string Comment { get; set; }

        public string AcceptSignature { get; set; }

        public string DeclinedSignature { get; set; }

        [Column(TypeName = "xml")]
        public string WalkAroundInspection { get; set; }

        [StringLength(45)]
        public string TechCode { get; set; }

        [StringLength(15)]
        public string TransportType { get; set; }

        [StringLength(15)]
        public string OrderType { get; set; }

        [StringLength(45)]
        public string ErrorId { get; set; }

        public string ErrorMessage { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? PartitionElement { get; set; }

        public virtual Company Company { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HistoryAppointmentDetail> HistoryAppointmentDetails { get; set; }

        public virtual User User { get; set; }

        public virtual DealerCustomer DealerCustomer { get; set; }

        public virtual DealerVehicle DealerVehicle { get; set; }
    }
}
