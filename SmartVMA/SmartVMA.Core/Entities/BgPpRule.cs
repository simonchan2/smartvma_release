using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("bg.BgPpRules")]
    internal partial class BgPpRule
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BgPpRule()
        {
            BgPpRulesBgProducts = new HashSet<BgPpRulesBgProduct>();
        }

        [Key]
        public int BgPpRuleId { get; set; }

        public int BgProtectionSubPlanId { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual BgProtectionSubPlan BgProtectionSubPlan { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BgPpRulesBgProduct> BgPpRulesBgProducts { get; set; }
    }
}
