using SmartVMA.Core.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    internal partial class DmsUserType : IBaseEntity<int>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DmsUserType()
        {
            DealerDmsUsers = new HashSet<DealerDmsUser>();
        }

        [Column("DmsUserTypeId")]
        public int Id { get; set; }

        [Column("DmsUserType")]
        [Required]
        [StringLength(45)]
        public string DmsUserType1 { get; set; }

        public int Cod { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        public bool Active { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DealerDmsUser> DealerDmsUsers { get; set; }
    }
}
