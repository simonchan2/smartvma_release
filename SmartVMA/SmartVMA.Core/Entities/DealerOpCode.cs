using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("dealer.DealerOpCodes")]
    internal partial class DealerOpCode
    {
        public int DealerOpCodeId { get; set; }

        public int DealerId { get; set; }

        [Required]
        [StringLength(50)]
        public string OpCode { get; set; }

        [Required]
        [StringLength(255)]
        public string OpDescription { get; set; }

        [StringLength(50)]
        public string LaborType { get; set; }

        public double? LabourHour { get; set; }

        [Column(TypeName = "money")]
        public decimal? LaborCost { get; set; }

        [Column(TypeName = "money")]
        public decimal? LaborRate { get; set; }

        [Column(TypeName = "money")]
        public decimal? Price { get; set; }

        [StringLength(50)]
        public string Priority { get; set; }

        [StringLength(2000)]
        public string Concern { get; set; }

        [StringLength(15)]
        public string DispatchCode { get; set; }

        [StringLength(15)]
        public string DispatchTeam { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? PartitionElement { get; set; }

        [StringLength(1)]
        public string ServiceMenuType { get; set; }

        public byte? ServiceMenuLevel { get; set; }

        public int? ServiceMenuInterval { get; set; }

        public int? OpCodeExtId { get; set; }

        public virtual Company Company { get; set; }
    }
}
