using SmartVMA.Core.Contracts;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("dealer.AdditionalServiceParts")]
    internal partial class AdditionalServicePart : IBaseTenantEntity<long>
    {
        [Key]
        [Column("AdditionalServicePartId")]
        public long Id { get; set; }

        public long AdditionalServiceId { get; set; }

        public int? BgProductId { get; set; }

        [Column("DealerId")]
        public int TenantId { get; set; }

        [StringLength(20)]
        public string PartNo { get; set; }

        [Required]
        [StringLength(100)]
        public string PartName { get; set; }

        public double Quantity { get; set; }

        [Required]
        [StringLength(10)]
        public string Unit { get; set; }

        [Column(TypeName = "money")]
        public decimal UnitPrice { get; set; }

        [StringLength(100)]
        public string FluidType { get; set; }

        [StringLength(25)]
        public string FluidViscosity { get; set; }

        public bool IsEngineOil { get; set; }

        public bool IsFluid { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? PartitionElement { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual BgProduct BgProduct { get; set; }

        public virtual Company Company { get; set; }

        public virtual AdditionalService AdditionalService { get; set; }
    }
}
