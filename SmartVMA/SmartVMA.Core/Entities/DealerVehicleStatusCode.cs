using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.Entities
{
    internal partial class DealerVehicleStatusCode
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DealerVehicleStatusCode()
        {
            DealerVehicles = new HashSet<DealerVehicle>();
        }

        public int DealerVehicleStatusCodeId { get; set; }

        [Required]
        [StringLength(50)]
        public string Description { get; set; }

        public string Comment { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DealerVehicle> DealerVehicles { get; set; }
    }
}
