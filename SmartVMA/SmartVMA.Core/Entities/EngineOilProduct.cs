namespace SmartVMA.Core.Entities
{
    using Contracts;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    [Table("amam.EngineOilProducts")]
    internal partial class EngineOilProduct : IBaseTenantEntity<int>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public EngineOilProduct()
        {
            EngineOilORules = new HashSet<EngineOilORule>();
        }
        [Column("EngineOilProductId")]
        public int Id { get; set; }
        [Column("DealerId")]
        public int TenantId { get; set; }

        public byte? MenuLevel { get; set; }
        [Required]
        [StringLength(50)]
        public string PartNumber { get; set; }

        [StringLength(255)]
        public string PartName { get; set; }
        [Required]
        [Column(TypeName = "money")]
        public decimal UnitPrice { get; set; }
        [Required]
        [StringLength(100)]
        public string FluidType { get; set; }
        [Required]
        [StringLength(25)]
        public string FluidViscosity { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? PartitionElement { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EngineOilORule> EngineOilORules { get; set; }

        public virtual Company Company { get; set; }
    }
}
