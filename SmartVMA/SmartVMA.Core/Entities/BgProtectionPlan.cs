using SmartVMA.Core.Contracts;

namespace SmartVMA.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    [Table("bg.BgProtectionPlans")]
    internal partial class BgProtectionPlan : IBaseEntity<int>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BgProtectionPlan()
        {
            BgProtectionPlanDocs = new HashSet<BgProtectionPlanDoc>();
            BgProtectionPlanLevels = new HashSet<BgProtectionPlanLevel>();
            BgProtectionPlanMus = new HashSet<BgProtectionPlanMu>();
            AdditionalServices = new HashSet<AdditionalService>();
            BgProtectionSubPlans = new HashSet<BgProtectionSubPlan>();
            BgProductsProtectionPlans = new HashSet<BgProductsProtectionPlan>();
        }

        [Column("BgProtectionPlanId")]
        public int Id { get; set; }
        
        [Required]
        [StringLength(50)]
        public string ProtectionPlanName { get; set; }
        
        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public string Comment { get; set; }
        
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BgProtectionPlanDoc> BgProtectionPlanDocs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BgProtectionPlanLevel> BgProtectionPlanLevels { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BgProtectionPlanMu> BgProtectionPlanMus { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AdditionalService> AdditionalServices { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BgProtectionSubPlan> BgProtectionSubPlans { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BgProductsProtectionPlan> BgProductsProtectionPlans { get; set; }
    }
}
