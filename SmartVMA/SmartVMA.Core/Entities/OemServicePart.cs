using SmartVMA.Core.Contracts;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("amam.OemServiceParts")] 
    internal partial class OemServicePart : IBaseEntity<int>
    {
        [Column ("OemServicePartId")]
        public int Id { get; set; }

        public int OemBasicServiceId { get; set; }

        public int OemPartId { get; set; }

        public double Quantity { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public bool NoOverwrite { get; set; }

        [ForeignKey("OemBasicServiceId")]
        public virtual OemBasicService OemBasicService { get; set; }

        [ForeignKey("OemPartId")]
        public virtual OemPart OemPart { get; set; }

        public bool IsDeleted { get; set; }
    }
}
