using SmartVMA.Core.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("dealer.AppointmentServiceParts")]
    internal partial class AppointmentServicePart : IBaseTenantEntity<int>
    {
        public double Quantity { get; set; }

        [Key]
        [Column("AppointmentServicePartId")]
        public int Id { get; set; }

        [Column("DealerId")]
        public int TenantId { get; set; }

        public long AppointmentServicesId { get; set; }

        [StringLength(50)]
        public string PartNo { get; set; }

        [StringLength(100)]
        public string PartName { get; set; }

        [StringLength(10)]
        public string Unit { get; set; }

        [Column(TypeName = "money")]
        public decimal UnitPrice { get; set; }

        [StringLength(100)]
        public string FluidType { get; set; }

        [StringLength(25)]
        public string FluidViscosity { get; set; }

        [StringLength(10)]
        public string LaborSkillLevel { get; set; }

        public double? LaborHour { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? PartitionElement { get; set; }

        public int OriginalPartId { get; set; }

        public int PartTypeId { get; set; }

        public virtual Company Company { get; set; }

        public virtual AppointmentService AppointmentService { get; set; }
    }
}
