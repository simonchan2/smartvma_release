using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("bg.BgVideoClipEditions")]
    internal partial class BgVideoClipEdition
    {
        public int BgVideoClipEditionId { get; set; }

        public int BgVideoClipId { get; set; }

        public int LanguageId { get; set; }

        public bool IsActive { get; set; }

        [Required]
        [StringLength(1000)]
        public string VideoUrl { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        public string Comment { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual BgVideoClip BgVideoClip { get; set; }

        public virtual Language Language { get; set; }
    }
}
