using SmartVMA.Core.Contracts;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{

    [Table("vi.DamageIndications")]
    internal partial class DamageIndication :IBaseEntity<long>
    {
        public long Id { get; set; }

        public int TypeId { get; set; }

        [StringLength(1000)]
        public string Comment { get; set; }

        public decimal OffsetLeft { get; set; }

        public decimal OffsetTop { get; set; }

        public int ViewPointId { get; set; }

        public long InspectionId { get; set; }
        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }
    }
}
