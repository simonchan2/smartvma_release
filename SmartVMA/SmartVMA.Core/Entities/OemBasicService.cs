using SmartVMA.Core.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("amam.OemBasicServices")]
    internal partial class OemBasicService : IBaseEntity<int>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OemBasicService()
        {
            OemServiceParts = new HashSet<OemServicePart>();
            OemServices = new HashSet<OemService>();
            OverrideRuleBasicServices = new HashSet<OverrideRuleBasicService>();
            OverrideRuleBasicServicesCrosses = new HashSet<OverrideRuleBasicServicesCross>();
        }

        [Column("OemBasicServiceId")]
        public int Id { get; set; }

        public int CarId { get; set; }

        public int OemComponentId { get; set; }

        public int? OemServiceExceptionId { get; set; }

        [Required]
        [StringLength(50)]
        public string OpAction { get; set; }


        [StringLength(1000)]
        public string OpDescription { get; set; }

        [StringLength(10)]
        public string LaborSkillLevel { get; set; }

        public double LaborHour { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [StringLength(2)]
        public string VehicleTransmission { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public bool? IsIncomplete { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public bool NoOverwrite { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsCreatedByAdmin { get; set; }

        [NotMapped]
        public string MileagesM { get; set; }

        [NotMapped]
        public string MileagesKM { get; set; }

        public virtual Car Car { get; set; }

        public virtual OemComponent OemComponent { get; set; }

        public virtual OemServiceException OemServiceException { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemServicePart> OemServiceParts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemService> OemServices { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OverrideRuleBasicService> OverrideRuleBasicServices { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OverrideRuleBasicServicesCross> OverrideRuleBasicServicesCrosses { get; set; }
    }
}
