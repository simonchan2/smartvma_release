using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    internal partial class DmsEventLogEntry
    {
        [Key]
        [Column(Order = 0)]
        public int EventLogEntriesId { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string PackageName { get; set; }

        [StringLength(50)]
        public string TaskName { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(100)]
        public string DealerName { get; set; }

        [StringLength(50)]
        public string KeyToMatch { get; set; }

        [StringLength(1000)]
        public string ErrorMessage { get; set; }

        [Key]
        [Column(Order = 3)]
        public DateTime TimeCreated { get; set; }

        [Key]
        [Column(Order = 4)]
        public DateTime TimeUpdated { get; set; }
    }
}
