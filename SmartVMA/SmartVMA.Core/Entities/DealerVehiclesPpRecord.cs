using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("dealer.DealerVehiclesPpRecords")]
    internal partial class DealerVehiclesPpRecord
    {
        public int DealerVehiclesPpRecordId { get; set; }

        public int DealerId { get; set; }

        public int DealerVehicleId { get; set; }

        public int BgProtectionSubPlanLevelId { get; set; }

        public int InvoiceId { get; set; }

        [Column(TypeName = "date")]
        public DateTime InvoiceDate { get; set; }

        public int MeasurementUnitId { get; set; }

        public int Mileage { get; set; }

        public int MileageNext { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DateNext { get; set; }

        public bool IsActive { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? PartitionElement { get; set; }

        public virtual BgProtectionSubPlanLevel BgProtectionSubPlanLevel { get; set; }

        public virtual Company Company { get; set; }

        public virtual MeasurementUnit MeasurementUnit { get; set; }

        public virtual DealerVehicle DealerVehicle { get; set; }

        public virtual Invoice Invoice { get; set; }
    }
}
