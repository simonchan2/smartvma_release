using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("cm.MenuSchedules")]
    internal partial class MenuSchedule
    {
        public int MenuScheduleId { get; set; }

        public int DealerId { get; set; }

        [Required]
        [StringLength(50)]
        public string Idpat { get; set; }

        public short Crep { get; set; }

        [Required]
        [StringLength(50)]
        public string Idbrd { get; set; }

        public int Type { get; set; }

        public int Nper { get; set; }

        public double Horas { get; set; }

        public int Per { get; set; }

        [Required]
        [StringLength(50)]
        public string OpCode { get; set; }

        [Required]
        [StringLength(255)]
        public string OpDescription { get; set; }

        [Column(TypeName = "money")]
        public decimal Price { get; set; }

        [Column(TypeName = "money")]
        public decimal Total { get; set; }

        [Required]
        [StringLength(25)]
        public string CommType { get; set; }

        [Column(TypeName = "money")]
        public decimal CommAdv { get; set; }

        [Column(TypeName = "money")]
        public decimal CommMgr { get; set; }

        [Required]
        [StringLength(25)]
        public string Parent { get; set; }

        [Column(TypeName = "money")]
        public decimal LaborAmnt { get; set; }

        [Column(TypeName = "money")]
        public decimal PartsAmnt { get; set; }

        public int FreqChk { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual Company Company { get; set; }
    }
}
