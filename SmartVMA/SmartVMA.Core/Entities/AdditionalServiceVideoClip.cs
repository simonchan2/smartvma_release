using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("dealer.AdditionalServiceVideoClips")]
    internal partial class AdditionalServiceVideoClip
    {
        [Key]
        public int AdditionalServiceVideoClipId { get; set; }

        public long AdditionalServiceId { get; set; }

        public int BgVideoClipId { get; set; }

        public virtual BgVideoClip BgVideoClip { get; set; }

        public virtual AdditionalService AdditionalService { get; set; }
    }
}
