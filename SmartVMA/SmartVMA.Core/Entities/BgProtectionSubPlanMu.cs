using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("bg.BgProtectionSubPlanMus")]
    internal partial class BgProtectionSubPlanMu
    {
        public int BgProtectionSubPlanMuId { get; set; }

        public int BgProtectionSubPlanId { get; set; }

        public int MeasurementUnitId { get; set; }

        public int ServiceInterval { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual MeasurementUnit MeasurementUnit { get; set; }

        public virtual BgProtectionSubPlan BgProtectionSubPlan { get; set; }
    }
}
