namespace SmartVMA.Core.Entities
{
    using Contracts;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    [Table("dbo.FluidViscosity")]
    internal partial class FluidViscosity : IBaseEntity<int>
    {
     
        [Column("FluidViscosityId")]
        public int Id { get; set; }
        
        [Required]
        [StringLength(25)]
        public string Viscosity { get; set; }

        [Required]
        public bool InDealerOilViscosityList { get; set; }
        
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? SortOrder { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

    }
}
