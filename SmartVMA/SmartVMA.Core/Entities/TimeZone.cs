using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    internal partial class TimeZone
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TimeZone()
        {
            Companies = new HashSet<Company>();
            StateTimeZones = new HashSet<StateTimeZone>();
        }

        public int TimeZoneId { get; set; }

        [Column("TimeZone")]
        [Required]
        [StringLength(100)]
        public string TimeZone1 { get; set; }

        public decimal TimeOffset { get; set; }

        [Required]
        [StringLength(400)]
        public string Description { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Company> Companies { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StateTimeZone> StateTimeZones { get; set; }
    }
}
