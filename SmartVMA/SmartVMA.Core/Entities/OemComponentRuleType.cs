using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("amam.OemComponentRuleTypes")]
    internal partial class OemComponentRuleType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OemComponentRuleType()
        {
            OemComponentRules = new HashSet<OemComponentRule>();
        }

        public int OemComponentRuleTypeId { get; set; }

        [Required]
        [StringLength(250)]
        public string Description { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemComponentRule> OemComponentRules { get; set; }
    }
}
