using SmartVMA.Core.Contracts;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    internal partial class CompanySetting : IBaseTenantEntity<int>
    {
        [Key]
        [Column("CompanySettingsId")]
        public int Id { get; set; }

        [Column("CompanyId")]
        public int TenantId { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public string Value { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [ForeignKey("TenantId")]
        public virtual Company Company { get; set; }
    }
}
