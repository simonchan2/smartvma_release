using SmartVMA.Core.Contracts;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("amam.EngineOilORules")]
    internal partial class EngineOilORule : IBaseTenantEntity<int>
    {

        [Column("EngineOilORuleId")]
        public int Id { get; set; }

        public int EngineOilProductId { get; set; }
        [Column("DealerId")]
        public int TenantId { get; set; }
        [Required]
        [StringLength(100)]
        public string FluidType { get; set; }
        [Required]
        [StringLength(25)]
        public string FluidViscosity { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? PartitionElement { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual Company Company { get; set; }

        public virtual EngineOilProduct EngineOilProduct { get; set; }
    }
}
