using SmartVMA.Core.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    internal partial class Language : IBaseEntity<int>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Language()
        {
            BgProtectionPlanDocs = new HashSet<BgProtectionPlanDoc>();
            BgVideoClipEditions = new HashSet<BgVideoClipEdition>();
            Companies = new HashSet<Company>();
            DealerCustomers = new HashSet<DealerCustomer>();
        }

        [Column("LanguageId")]
        public int Id { get; set; }

        [Required]
        [StringLength(10)]
        public string LanguageCode { get; set; }

        [Required]
        [StringLength(255)]
        public string Description { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BgProtectionPlanDoc> BgProtectionPlanDocs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BgVideoClipEdition> BgVideoClipEditions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Company> Companies { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DealerCustomer> DealerCustomers { get; set; }
    }
}
