using SmartVMA.Core.Contracts;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    internal partial class CompanyCompanyGroup : IBaseTenantEntity<int>
    {
        [Column("CompanyCompanyGroupId")]
        public int Id { get; set; }

        public int? CompanyGroupId { get; set; }

        [Column("CompanyId")]
        public int TenantId { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [ForeignKey("TenantId")]
        public virtual Company Company { get; set; }

        public virtual CompanyGroup CompanyGroup { get; set; }
    }
}
