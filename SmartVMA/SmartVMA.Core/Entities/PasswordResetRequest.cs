﻿using SmartVMA.Core.Contracts;
using System;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.Entities
{
    internal partial class PasswordResetRequest : IBaseEntity<long>
    {
        public long Id { get; set; }
        public Guid Token { get; set; }
        public DateTime TimeCreated { get; set; }
        public DateTime TimeUpdated { get; set; }

        [Required]
        [StringLength(200)]
        public string Email { get; set; }
    }
}
