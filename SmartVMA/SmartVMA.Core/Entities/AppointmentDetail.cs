using SmartVMA.Core.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{   

    [Table("dealer.AppointmentDetails")]
    internal partial class AppointmentDetail : IBaseEntity<long>
    {
        [Column("AppointmentDetailId")]
        public long Id { get; set; }

        public int DealerId { get; set; }

        public long AppointmentId { get; set; }

        public int AppointmentLineNo { get; set; }

        [Required]
        [StringLength(5)]
        public string AppointmentLineCode { get; set; }

        [StringLength(50)]
        public string AppointmentDetailType { get; set; }

        [StringLength(10)]
        public string SalesType { get; set; }

        [StringLength(50)]
        public string OpCode { get; set; }

        [StringLength(50)]
        public string OpType { get; set; }

        [StringLength(255)]
        public string OpDescription { get; set; }

        public int? OpStatus { get; set; }

        public DateTime? DateScheduled { get; set; }

        public double? LaborHour { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        public bool InStock { get; set; }

        [Column(TypeName = "money")]
        public decimal? Price { get; set; }

        [StringLength(2000)]
        public string Concern { get; set; }

        [StringLength(50)]
        public string Priority { get; set; }

        [StringLength(15)]
        public string DispatchCode { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? PartitionElement { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public int? AppointmentDetailExtId { get; set; }

        public virtual Company Company { get; set; }

        public virtual Appointment Appointment { get; set; }
    }
}
