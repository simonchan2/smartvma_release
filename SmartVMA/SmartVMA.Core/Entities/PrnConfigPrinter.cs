using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("rprint.PrnConfigPrinters")]
    internal partial class PrnConfigPrinter
    {
        public int PrnConfigPrinterId { get; set; }

        public int PrnConfigurationId { get; set; }

        public int PrnPrinterId { get; set; }

        public int NumberOfCopy { get; set; }

        public int NumberOfODC { get; set; }

        [StringLength(45)]
        public string PrinterTray { get; set; }

        public DateTime TimeUpdated { get; set; }

        public DateTime TimeCreated { get; set; }

        public virtual PrnConfiguration PrnConfiguration { get; set; }

        public virtual PrnPrinter PrnPrinter { get; set; }
    }
}
