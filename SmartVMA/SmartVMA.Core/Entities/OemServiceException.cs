using SmartVMA.Core.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("amam.OemServiceExceptions")]
    internal partial class OemServiceException : IBaseEntity<int>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OemServiceException()
        {
            OemBasicServices = new HashSet<OemBasicService>();
        }
        [Column("OemServiceExceptionId") ]
        public int Id { get; set; }

        [Required]
        public string ExceptionMessage { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemBasicService> OemBasicServices { get; set; }
    }
}
