using SmartVMA.Core.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    internal partial class Company : IBaseEntity<int>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Company()
        {
            EngineOilORules = new HashSet<EngineOilORule>();
            EngineOilProducts = new HashSet<EngineOilProduct>();
            MenuGroups = new HashSet<MenuGroup>();
            MenuNames = new HashSet<MenuName>();
            MenuSchedules = new HashSet<MenuSchedule>();
            AdditionalServiceParts = new HashSet<AdditionalServicePart>();
            AdditionalServices = new HashSet<AdditionalService>();
            AppointmentDetails = new HashSet<AppointmentDetail>();
            AppointmentPresentations = new HashSet<AppointmentPresentation>();
            Appointments = new HashSet<Appointment>();
            AppointmentServices = new HashSet<AppointmentService>();
            Companies1 = new HashSet<Company>();
            CompanyCompanyGroups = new HashSet<CompanyCompanyGroup>();
            CompanyGroups = new HashSet<CompanyGroup>();
            CompanySettings = new HashSet<CompanySetting>();
            DealerCustomers = new HashSet<DealerCustomer>();
            DealerDmsUsers = new HashSet<DealerDmsUser>();
            DealerOpCodes = new HashSet<DealerOpCode>();
            DealerVehicles = new HashSet<DealerVehicle>();
            DealerVehiclesPpRecords = new HashSet<DealerVehiclesPpRecord>();
            Inspections = new HashSet<Inspection>();
            InvoiceDetailParts = new HashSet<InvoiceDetailPart>();
            InvoiceDetails = new HashSet<InvoiceDetail>();
            Invoices = new HashSet<Invoice>();
            OemLofMenuExpandedORules = new HashSet<OemLofMenuExpandedORule>();
            OemLofMenuORules = new HashSet<OemLofMenuORule>();
            OemMenuExpandedORules = new HashSet<OemMenuExpandedORule>();
            OemMenuORules = new HashSet<OemMenuORule>();
            OemServiceExpandedORules = new HashSet<OemServiceExpandedORule>();
            OemServiceORules = new HashSet<OemServiceORule>();
            OverrideRules = new HashSet<OverrideRule>();
            OverrideRuleBasicServicesCrosses = new HashSet<OverrideRuleBasicServicesCross>();
            UserCompanyRoles = new HashSet<UserCompanyRole>();
            AppointmentServiceParts = new HashSet<AppointmentServicePart>();
            AppointmentServices = new HashSet<AppointmentService>();
        }

        [Column("CompanyId")]
        public int Id { get; set; }

        public int? ParentId { get; set; }

        [StringLength(20)]
        public string AccountId { get; set; }

        public int CompanyStatusCodeId { get; set; }

        [Required]
        [StringLength(100)]
        public string CompanyName { get; set; }

        public int CountryId { get; set; }

        public int CountryStateId { get; set; }

        [Required]
        [StringLength(100)]
        public string City { get; set; }

        [Required]
        [StringLength(50)]
        public string Zip { get; set; }

        [Required]
        [StringLength(100)]
        public string AddressLine1 { get; set; }

        [StringLength(100)]
        public string AddressLine2 { get; set; }

        public int TimeZoneId { get; set; }

        public int MeasurementUnitId { get; set; }

        public int? LanguageId { get; set; }

        [Column(TypeName = "xml")]
        public string Configuration { get; set; }

        public string Comment { get; set; }

        public DateTime TimeUpdated { get; set; }

        public DateTime TimeCreated { get; set; }

        public int CompanyTypeId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EngineOilORule> EngineOilORules { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EngineOilProduct> EngineOilProducts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MenuGroup> MenuGroups { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MenuName> MenuNames { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MenuSchedule> MenuSchedules { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AdditionalServicePart> AdditionalServiceParts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AdditionalService> AdditionalServices { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppointmentDetail> AppointmentDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppointmentPresentation> AppointmentPresentations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Appointment> Appointments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppointmentService> AppointmentServices { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Company> Companies1 { get; set; }

        public virtual Company ParentCompany { get; set; }
		public virtual Language Language { get; set; }

        public virtual MeasurementUnit MeasurementUnit { get; set; }

        public virtual CompanyStatusCode CompanyStatusCode { get; set; }

        public virtual Country Country { get; set; }

        public virtual CountryState CountryState { get; set; }

        public virtual TimeZone TimeZone { get; set; }

        public virtual CompanyType CompanyType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CompanyCompanyGroup> CompanyCompanyGroups { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CompanyGroup> CompanyGroups { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CompanySetting> CompanySettings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DealerCustomer> DealerCustomers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DealerDmsUser> DealerDmsUsers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DealerOpCode> DealerOpCodes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DealerVehicle> DealerVehicles { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DealerVehiclesPpRecord> DealerVehiclesPpRecords { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Inspection> Inspections { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvoiceDetailPart> InvoiceDetailParts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvoiceDetail> InvoiceDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Invoice> Invoices { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemLofMenuExpandedORule> OemLofMenuExpandedORules { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemLofMenuORule> OemLofMenuORules { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemMenuExpandedORule> OemMenuExpandedORules { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemMenuORule> OemMenuORules { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemServiceExpandedORule> OemServiceExpandedORules { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemServiceORule> OemServiceORules { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OverrideRuleBasicServicesCross> OverrideRuleBasicServicesCrosses { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OverrideRule> OverrideRules { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserCompanyRole> UserCompanyRoles { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppointmentServicePart> AppointmentServiceParts { get; set; }
    }
}
