using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("bg.BgProtectionPlanMus")]
    internal partial class BgProtectionPlanMu
    {
        public int BgProtectionPlanMuId { get; set; }

        public int BgProtectionPlanId { get; set; }

        public int MeasurementUnitId { get; set; }

        public int? MileageGracePeriod { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual BgProtectionPlan BgProtectionPlan { get; set; }
    }
}
