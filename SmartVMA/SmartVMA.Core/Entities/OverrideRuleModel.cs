using System;

namespace SmartVMA.Core.Entities
{
    internal partial class OverrideRuleModel
    {
        public long Id { get; set; }

        public long OverrideRuleId { get; set; }

        public int VehicleModelId { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual VehicleModel VehicleModel { get; set; }

        public virtual OverrideRule OverrideRule { get; set; }
    }
}
