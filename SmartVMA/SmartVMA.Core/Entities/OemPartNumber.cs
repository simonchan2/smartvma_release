using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SmartVMA.Core.Contracts;

namespace SmartVMA.Core.Entities
{
    [Table("amam.OemPartNumbers")]
    internal class OemPartNumber : IBaseEntity<int>
    {
        [Column("OemPartNumberId")]
        public int Id { get; set; }

        public int OemPartId { get; set; }

        public int CountryId { get; set; }

        [Required]
        [StringLength(50)]
        public string OemPartNo { get; set; }

        public bool NoOverwrite { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual OemPart OemPart { get; set; }

        public bool IsCreatedByAdmin { get; set; }
    }
}
