using SmartVMA.Core.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    internal partial class Role : IBaseEntity<int>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Role()
        {
            RolePermissions = new HashSet<RolePermission>();
            UserCompanyRoles = new HashSet<UserCompanyRole>();
        }

        [Column("RoleId")]
        public int Id { get; set; }

        public int? ParentId { get; set; }

        [Column("Role")]
        [Required]
        [StringLength(100)]
        public string Role1 { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        public DateTime TimeCreated { get; set; }

        [Column(TypeName = "xml")]
        public string Configuration { get; set; }

        public string Comment { get; set; }

        public DateTime TimeUpdated { get; set; }

        public bool IsSystemRole { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RolePermission> RolePermissions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserCompanyRole> UserCompanyRoles { get; set; }
    }
}
