using SmartVMA.Core.Contracts;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("amam.OemMenuMileages")]
    internal partial class OemMenuMileage: IBaseEntity<int>
    {
        [Column("OemMenuMileageId")]
        public int Id { get; set; }

        public int OemMenuId { get; set; }

        public int MeasurementUnitId { get; set; }

        public int Mileage { get; set; }

        public bool NoOverwrite { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [StringLength(4000)]
        public string OemMenuName { get; set; }

        public virtual MeasurementUnit MeasurementUnit { get; set; }

        public virtual OemMenu OemMenu { get; set; }
    }
}
