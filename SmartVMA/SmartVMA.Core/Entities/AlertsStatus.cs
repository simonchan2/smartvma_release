using SmartVMA.Core.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace SmartVMA.Core.Entities
{
    [Table("msg.AlertsStatuses")]
    internal partial class AlertsStatus : IBaseEntity<int>
    {
        public AlertsStatus()
        {
            AlertsUsers = new HashSet<AlertsUser>();
        }
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AlertsUser> AlertsUsers { get; set; }
    }
}
