using SmartVMA.Core.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("dealer.AdditionalServices")]
    internal partial class AdditionalService : IBaseTenantEntity<long>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AdditionalService()
        {
            AdditionalServiceParts = new HashSet<AdditionalServicePart>();
            AdditionalServiceVideoClips = new HashSet<AdditionalServiceVideoClip>();
        }

        [Key]
        [Column("AdditionalServiceId")]
        public long Id { get; set; }

        [Column("DealerId")]
        public int TenantId { get; set; }

        public int? BgProtectionPlanId { get; set; }

        public int? BgProtectionSubPlanId { get; set; }

        [StringLength(50)]
        public string OpCode { get; set; }

        [StringLength(255)]
        public string OpDescription { get; set; }

        public double? LaborHour { get; set; }

        [Column(TypeName = "money")]
        public decimal? LaborRate { get; set; }

        public string Comment { get; set; }

        public bool IsLofService { get; set; }

        public bool CanBeStriked { get; set; }

        public bool IsFixLabor { get; set; }

        public byte? MenuLevel { get; set; }

        [Column(TypeName = "money")]
        public decimal? AdvisorIncentive { get; set; }

        [Column(TypeName = "money")]
        public decimal? TechIncentive { get; set; }

        public int? IntervalStart { get; set; }

        public int? IntervalRepeat { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? PartitionElement { get; set; }

        public virtual BgProtectionPlan BgProtectionPlan { get; set; }

        public virtual BgProtectionSubPlan BgProtectionSubPlan { get; set; }

        public virtual Company Company { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AdditionalServicePart> AdditionalServiceParts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AdditionalServiceVideoClip> AdditionalServiceVideoClips { get; set; }

        public int MeasurementUnitId { get; set; }
        public int? BgProdCategoryId { get; set; }

        public BgProdCategory BgProdCategory { get; set; }
    }
}
