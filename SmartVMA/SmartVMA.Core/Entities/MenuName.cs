using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("cm.MenuNames")]
    internal partial class MenuName
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MenuNameId { get; set; }

        public int DealerId { get; set; }

        [Required]
        [StringLength(50)]
        public string Idbrd { get; set; }

        [Column("MenuName")]
        [Required]
        [StringLength(100)]
        public string MenuName1 { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual Company Company { get; set; }
    }
}
