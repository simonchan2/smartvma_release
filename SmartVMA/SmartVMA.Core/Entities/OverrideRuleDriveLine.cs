using System;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.Entities
{
    internal partial class OverrideRuleDriveLine
    {
        public long Id { get; set; }

        public DateTime TimeUpdated { get; set; }

        public DateTime TimeCreated { get; set; }

        public long OverrideRuleId { get; set; }

        [Required]
        [StringLength(255)]
        public string VehicleDrivelineShort { get; set; }

        public virtual OverrideRule OverrideRule { get; set; }
    }
}
