using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("amam.OemMenuLists")]
    internal partial class OemMenuList
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OemMenuList()
        {
            OemMenus = new HashSet<OemMenu>();
        }

        public int OemMenuListId { get; set; }

        public int CarId { get; set; }

        public int OemComponentId { get; set; }
		
		public int? OemITypes { get; set; }
		
        [StringLength(50)]
        public string ExtInfoChecksum { get; set; }

        public bool NoOverwrite { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual Car Car { get; set; }

        public virtual OemComponent OemComponent { get; set; }

        public virtual OemComponent OemComponent1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemMenu> OemMenus { get; set; }
    }
}
