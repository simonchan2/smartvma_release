using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("amam.OpActions")]
    internal partial class OpAction
    {
        public int OpActionId { get; set; }

        [Column("OpAction")]
        [Required]
        [StringLength(50)]
        public string OpAction1 { get; set; }

        public bool IsRequired { get; set; }

        public string Comment { get; set; }

        public DateTime TimeUpdated { get; set; }

        public DateTime TimeCreated { get; set; }
    }
}
