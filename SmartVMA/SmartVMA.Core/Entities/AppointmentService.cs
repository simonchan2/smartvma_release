using SmartVMA.Core.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("dealer.AppointmentServices")]
    internal partial class AppointmentService : IBaseTenantEntity<long>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AppointmentService()
        {
            AppointmentServiceParts = new HashSet<AppointmentServicePart>();
        }

        [Key]
        [Column("AppointmentServicesId")]
        public long Id { get; set; }

        public long AppointmentPresentationId { get; set; }

        public int? BgProdSubcategoryId { get; set; }

        [Column("DealerId")]
        public int TenantId { get; set; }

        [StringLength(50)]
        public string OpCode { get; set; }

        [StringLength(255)]
        public string OpDescription { get; set; }

        public double? LaborHour { get; set; }

        [Column(TypeName = "money")]
        public decimal? LaborRate { get; set; }

        [Column(TypeName = "money")]
        public decimal? PartsPrice { get; set; }

        public bool IsStriked { get; set; }

        public string Comment { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? PartitionElement { get; set; }

        [Column(TypeName = "money")]
        public decimal? LaborPrice { get; set; }

        [Column(TypeName = "money")]
        public decimal? Price { get; set; }
        
        public long? OrigServiceId { get; set; }

        public int ServiceTypeId { get; set; }

        public bool? IsAddedAfter { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual BgProdSubcategory BgProdSubcategory { get; set; }

        [ForeignKey("TenantId")]
        public virtual Company Company { get; set; }

        public virtual AppointmentPresentation AppointmentPresentation { get; set; }

        [ForeignKey("AppointmentServicesId")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppointmentServicePart> AppointmentServiceParts { get; set; }

        public byte? MenuLevel { get; set; }
    }
}
