using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("bg.BgProtectionPlanDocs")]
    internal partial class BgProtectionPlanDoc
    {
        public int BgProtectionPlanDocId { get; set; }

        public int BgProtectionPlanId { get; set; }

        public int LanguageId { get; set; }

        [Required]
        [StringLength(255)]
        public string Description { get; set; }

        [Required]
        [StringLength(1000)]
        public string Url { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual BgProtectionPlan BgProtectionPlan { get; set; }

        public virtual Language Language { get; set; }
    }
}
