using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using SmartVMA.Core.Contracts;

namespace SmartVMA.Core.Entities
{
    [Table("rprint.PrnSpool")]
    internal partial class PrnSpool : IBaseEntity<int>
    {
        [Required]
        [Column("id")]
        public int Id { get; set; }

        [Required]
        [StringLength(45)]
        public string account { get; set; }

        [Required]
        public int idhist { get; set; }

        [Required]
        [StringLength(45)]
        public string doctype { get; set; }

        [Required]
        [StringLength(255)]
        public string docname { get; set; }

        [Required]
        [StringLength(255)]
        public string prnname { get; set; }

        [Required]
        [StringLength(255)]
        public string prndriver { get; set; }

        [Required]
        public int prncopy { get; set; }

        [Required]
        [StringLength(255)]
        public string prnport { get; set; }

        [Required]
        [StringLength(45)]
        public string prntray { get; set; }

        [Required]
        public DateTime datecreated { get; set; }

        public DateTime? dateprinted { get; set; }

        [Required]
        [StringLength(45)]
        public string status { get; set; }

        [StringLength(255)]
        public string comment { get; set; }

        [StringLength(45)]
        public string assno { get; set; }

        [StringLength(255)]
        public string session { get; set; }

        [Required]
        public DateTime TimeUpdated { get; set; }

        [Required]
        public DateTime TimeCreated { get; set; }
    }
}
