namespace SmartVMA.Core.Entities
{
    using Contracts;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    [Table("bg.BgProdCategories")]
    internal partial class BgProdCategory : IBaseEntity<int>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BgProdCategory()
        {
            BgProdSubcategories = new HashSet<BgProdSubcategory>();
            BgProductsCategories = new HashSet<BgProductsCategory>();
        }

        [Column("BgProdCategoryId")]
        public int  Id { get; set; }

        [Required]
        [StringLength(20)]
        public string VehicleSystem { get; set; }

        [Required]
        [StringLength(20)]
        public string BgType { get; set; }

        [Required]
        [StringLength(100)]
        public string Description { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BgProdSubcategory> BgProdSubcategories { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BgProductsCategory> BgProductsCategories { get; set; }
    }
}
