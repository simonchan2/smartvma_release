using SmartVMA.Core.Contracts;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("amam.OemServices")]
    internal partial class OemService : IBaseEntity<int>
    {
        [Column ("OemServiceId")]
        public int Id { get; set; }

        public int OemMenuId { get; set; }

        public int OemBasicServiceId { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public bool NoOverwrite { get; set; }

        public virtual OemBasicService OemBasicService { get; set; }

        public virtual OemMenu OemMenu { get; set; }
    }
}
