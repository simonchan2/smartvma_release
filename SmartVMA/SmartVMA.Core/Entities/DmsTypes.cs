using SmartVMA.Core.Contracts;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    public partial class DmsTypes : IBaseEntity<int>
    {
        [Key]
        [Column("DmsTypeId")]
        public int Id { get; set; }

        [Required]
        [StringLength(20)]
        public string DmsCode { get; set; }

        [Required]
        [StringLength(100)]
        public string DmsName { get; set; }

        public string Comment { get; set; }

        public bool IsActive { get; set; }

        public int SortOrder { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }
    }
}
