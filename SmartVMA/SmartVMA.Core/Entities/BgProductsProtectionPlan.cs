using SmartVMA.Core.Contracts;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("bg.BgProductsProtectionPlans")]
    internal partial class BgProductsProtectionPlan : IBaseEntity<int>
    {
        [Column("BgProductsProtectionPlanId")]
        public int Id { get; set; }

        public int BgProductId { get; set; }

        public int BgProtectionPlanId { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual BgProduct BgProduct { get; set; }

        public virtual BgProtectionPlan BgProtectionPlan { get; set; }
    }
}
