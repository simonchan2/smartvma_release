using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("dealer.OemMenuExpandedORules")]
    internal partial class OemMenuExpandedORule
    {
        public long OemMenuExpandedORuleId { get; set; }

        public int OemMenuORuleId { get; set; }

        public int DealerId { get; set; }

        public int? VehicleYear { get; set; }

        public int VehicleMakeId { get; set; }

        public int VehicleModelId { get; set; }

        public int VehicleEngineId { get; set; }

        [StringLength(120)]
        public string VehicleTransmission { get; set; }

        [StringLength(50)]
        public string VehicleDriveLine { get; set; }

        [StringLength(50)]
        public string VehicleSteering { get; set; }

        [StringLength(100)]
        public string TextFilter { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? PartitionElement { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual VehicleEngine VehicleEngine { get; set; }

        public virtual VehicleMake VehicleMake { get; set; }

        public virtual VehicleModel VehicleModel { get; set; }

        public virtual Company Company { get; set; }

        public virtual OemMenuORule OemMenuORule { get; set; }
    }
}
