using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("bg.BgProtectionPlanLevels")]
    internal partial class BgProtectionPlanLevel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BgProtectionPlanLevel()
        {
            BgProtectionPlanLevelMus = new HashSet<BgProtectionPlanLevelMu>();
        }

        public int BgProtectionPlanLevelId { get; set; }

        public int BgProtectionPlanId { get; set; }

        public byte PlanLevel { get; set; }

        public int? MaxHourBeforeFirstService { get; set; }

        public int? MinHourBeforeClaim { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BgProtectionPlanLevelMu> BgProtectionPlanLevelMus { get; set; }

        public virtual BgProtectionPlan BgProtectionPlan { get; set; }
    }
}
