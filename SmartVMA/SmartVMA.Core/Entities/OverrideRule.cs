using SmartVMA.Core.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("dealer.OverrideRules")]
    internal partial class OverrideRule : IBaseTenantEntity<long>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OverrideRule()
        {
            OverrideRuleBasicServices = new HashSet<OverrideRuleBasicService>();
            OverrideRuleDriveLines = new HashSet<OverrideRuleDriveLine>();
            OverrideRuleEngines = new HashSet<OverrideRuleEngine>();
            OverrideRuleMakes = new HashSet<OverrideRuleMake>();
            OverrideRuleModels = new HashSet<OverrideRuleModel>();
            OverrideRuleTransmissions = new HashSet<OverrideRuleTransmission>();
            OverrideRuleYears = new HashSet<OverrideRuleYear>();
            OverrideRuleBasicServicesCrosses = new HashSet<OverrideRuleBasicServicesCross>();
            OverrideRuleBasicServicesCrosses1 = new HashSet<OverrideRuleBasicServicesCross>();
            OverrideRuleBasicServicesCrosses2 = new HashSet<OverrideRuleBasicServicesCross>();
            OverrideRuleBasicServicesCrosses3 = new HashSet<OverrideRuleBasicServicesCross>();
            OverrideRuleBasicServicesCrosses4 = new HashSet<OverrideRuleBasicServicesCross>();
        }

        public long Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(50)]
        public string OpCode { get; set; }

        public int? LaborPricePercentage { get; set; }

        [Column(TypeName = "money")]
        public decimal? LaborPriceAmount { get; set; }

        public int? LaborHourPercentage { get; set; }

        [Column(TypeName = "money")]
        public decimal? LaborHourAmount { get; set; }

        public int? PartPricePercentage { get; set; }

        [Column(TypeName = "money")]
        public decimal? PartPriceAmount { get; set; }

        [StringLength(20)]
        public string PartNo { get; set; }

        [Column("DealerId")]
        public int TenantId { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [StringLength(50)]
        public string OpCodeOverride { get; set; }

        [StringLength(20)]
        public string PartNoOverride { get; set; }

        [StringLength(255)]
        public string Keyword { get; set; }

        public virtual Company Company { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OverrideRuleBasicService> OverrideRuleBasicServices { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OverrideRuleDriveLine> OverrideRuleDriveLines { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OverrideRuleEngine> OverrideRuleEngines { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OverrideRuleMake> OverrideRuleMakes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OverrideRuleModel> OverrideRuleModels { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OverrideRuleTransmission> OverrideRuleTransmissions { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OverrideRuleYear> OverrideRuleYears { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OverrideRuleBasicServicesCross> OverrideRuleBasicServicesCrosses { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OverrideRuleBasicServicesCross> OverrideRuleBasicServicesCrosses1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OverrideRuleBasicServicesCross> OverrideRuleBasicServicesCrosses2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OverrideRuleBasicServicesCross> OverrideRuleBasicServicesCrosses3 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OverrideRuleBasicServicesCross> OverrideRuleBasicServicesCrosses4 { get; set; }
    }
}
