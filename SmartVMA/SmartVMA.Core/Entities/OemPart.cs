using SmartVMA.Core.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("amam.OemParts")]
    internal partial class OemPart : IBaseEntity<int>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OemPart()
        {
            OemPartNumbers = new HashSet<OemPartNumber>();
            OemServiceParts = new HashSet<OemServicePart>();
        }


        [Column("OemPartId")]
        public int Id { get; set; }

        public int CarId { get; set; }

        public int? OemComponentId { get; set; }

        [Required]
        [StringLength(100)]
        public string OemPartName { get; set; }

        [StringLength(10)]
        public string Unit { get; set; }

        [Column(TypeName = "money")]
        public decimal UnitPrice { get; set; }

        [StringLength(100)]
        public string FluidType { get; set; }

        [StringLength(25)]
        public string FluidViscosity { get; set; }

        [StringLength(10)]
        public string LaborSkillLevel { get; set; }

        public double LaborHour { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public bool NoOverwrite { get; set; }

        public virtual Car Car { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public bool? IsEngineOil { get; set; }

        [StringLength(25)]
        public string EngineOilType { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsCreatedByAdmin { get; set; }

        public virtual OemComponent OemComponent { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemPartNumber> OemPartNumbers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemServicePart> OemServiceParts { get; set; }
    }
}
