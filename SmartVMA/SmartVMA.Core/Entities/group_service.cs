namespace SmartVMA.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    internal partial class group_service
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }

        public int groupid { get; set; }

        public int serviceid { get; set; }

        public int level { get; set; }

        public double? displayorder { get; set; }

        public short? exception { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }
    }
}
