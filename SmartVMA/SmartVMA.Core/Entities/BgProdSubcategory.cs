namespace SmartVMA.Core.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    

    [Table("bg.BgProdSubcategories")]
    internal partial class BgProdSubcategory
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BgProdSubcategory()
        {
            AppointmentServices = new HashSet<AppointmentService>();
        }

        public int BgProdSubcategoryId { get; set; }

        public int BgProdCategoryId { get; set; }

        [Required]
        [StringLength(50)]
        public string BgSubtype { get; set; }

        [Required]
        [StringLength(100)]
        public string Description { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual BgProdCategory BgProdCategory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppointmentService> AppointmentServices { get; set; }
    }
}
