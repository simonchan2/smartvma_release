using SmartVMA.Core.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{

    [Table("vi.DamageIndicationTypes")]
    internal partial class DamageIndicationType : IBaseEntity<int>
    {


        public int Id { get; set; }

        [StringLength(255)]
        public string Url { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public int Position { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public bool IsComment { get; set; }
    }
}
