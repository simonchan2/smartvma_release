using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartVMA.Core.Entities
{
    [Table("bg.BgProtectionPlanLevelMus")]
    internal partial class BgProtectionPlanLevelMu
    {
        public int BgProtectionPlanLevelMuId { get; set; }

        public int BgProtectionPlanLevelId { get; set; }

        public int MeasurementUnitId { get; set; }

        public int MaxMileageBeforeFirstService { get; set; }

        public int? GraceMileage { get; set; }

        public int? MinMileageOfClaim { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual BgProtectionPlanLevel BgProtectionPlanLevel { get; set; }

        public virtual MeasurementUnit MeasurementUnit { get; set; }
    }
}
