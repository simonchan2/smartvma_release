﻿using SmartVMA.Core.Enums;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.Utils
{
    public class RequiredIfSettingAttribute : RequiredAttribute
    {
        public CompanySettingsNames Setting { get; set; }

        public bool IsRequired()
        {
            var companySettingService = IocManager.Resolve<ICompanyService>();
            var isRequired = companySettingService.GetSettingAsBool(Setting);

            return isRequired;
        }

        public override bool IsValid(object value)
        {
            if (IsRequired())
            {
                return base.IsValid(value);
            }
            return true;
        }
    }
}