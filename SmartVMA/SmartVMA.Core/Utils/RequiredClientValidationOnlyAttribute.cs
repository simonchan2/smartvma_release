﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SmartVMA.Core.Utils
{
    /// <summary>
    /// This attribute will mark the form element as required and will enforce client side validation.
    /// However it won't be validated by the default MVC's Model Binder validation 
    ///     i.e. ModelState will be valid even if the propery doesn't have any value.
    /// 
    /// If the element is hidden then it won't be validated, so it can be used for conditional validation.
    /// IMPORTANT: when using this attribute DON'T forget to validate your model with custom validation logic.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class RequiredClientValidationOnlyAttribute : RequiredAttribute
    {
        public override bool IsValid(object value)
        {
            return true;
        }
    }
}
