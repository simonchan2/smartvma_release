﻿namespace SmartVMA.Core.Enums
{
    public enum OilType : int
    {
        HighMileage,
        Synthetic,
        SyntheticBlend,
        Conventional
    }
}

