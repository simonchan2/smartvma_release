﻿namespace SmartVMA.Core.Enums
{
    public enum PrintDocumentType
    {
        CHKB,
        CHKT,
        MONE,
        MENU,
        WA,
        HIST
    }
}
