﻿namespace SmartVMA.Core.Enums
{
    /// <summary>
    /// This enum represent service table in db.
    /// Right now the services are stored in this tables
    /// </summary>
    public enum AppointmentServicePartType
    {
        OemServicePart = 1,
        AppointmentServicePart = 2
    }
}
