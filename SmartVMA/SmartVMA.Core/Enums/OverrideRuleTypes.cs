﻿namespace SmartVMA.Core.Enums
{
    /// <summary>
    /// This enum represent service table in db.
    /// Right now the services are stored in this tables
    /// </summary>
    public enum OverrideRuleTypes
    {
        LaborHour = 1,
        LaborPrice = 2,
        PartPrice = 3,
        OpCode = 4,
        PartNumber = 5
    }
}
