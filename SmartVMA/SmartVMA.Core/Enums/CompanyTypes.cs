﻿namespace SmartVMA.Core.Enums
{
    public enum CompanyTypes
    {
        Corporation = 1,
        Distributor = 2,
        Dealer = 3
    }
}
