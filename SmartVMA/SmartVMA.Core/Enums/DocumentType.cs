﻿namespace SmartVMA.Core.Enums
{
    public enum DocumentType
    {
        CustomerCopy = 1,
        PartsCopy = 2,
        TechCopy = 3,
        WalkAroundInspection = 4,
        MenuDeclined = 5
    }
}
