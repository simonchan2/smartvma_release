﻿namespace SmartVMA.Core.Enums
{
    public enum UserStatusCodes
    {
        Enabled = 0,
        Disabled = 1
    }
}
