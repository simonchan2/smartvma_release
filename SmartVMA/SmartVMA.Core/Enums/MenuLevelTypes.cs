﻿namespace SmartVMA.Core.Enums
{
    public enum MenuLevelTypes
    {
        FirstLevel = 1,
        SecondLevel = 2,
        ThirdLevel = 3
    }
}
