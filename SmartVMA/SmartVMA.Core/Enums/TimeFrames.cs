﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Core.Enums
{
    public enum TimeFrames : int
    {
        NotApplicable,
        All,
        Today,
        LastWeek,
        LastMonth
    }
}
