﻿namespace SmartVMA.Core.Enums
{
    public enum AlertType :int
    {
        SystemNotification,
        BGAdminNotification
    }
}
