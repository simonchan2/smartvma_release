﻿namespace SmartVMA.Core.Enums
{
    public enum UserRoles
    {
        SystemAdministrator = 1,
        DealerAdmin = 2,
        DistributorAdmin = 3,
        BGExecutive = 4,
        DistributorExecutive = 5,
        Manager = 6,
        MenuAdvisor = 7
    }
}
