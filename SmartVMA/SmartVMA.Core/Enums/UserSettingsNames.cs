﻿namespace SmartVMA.Core.Enums
{
    public enum UserSettingsNames
    {
        LanguageId = 1,
        IsParked = 2,
        UserImageDownloadUrl = 3,
        UserImageFileName = 4,
        UserImageOriginalFileName = 5,
        DefaultTenantId = 6
    }
}
