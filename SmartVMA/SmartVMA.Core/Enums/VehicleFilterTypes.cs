﻿namespace SmartVMA.Core.Enums
{
    public enum VehicleFilterTypes
    {
        Year = 0,
        Make = 1,
        Model = 2,
        Engine = 3,
        Transmission = 4,
        Driveline = 5,
        SecondarySearch = 6
    }
}
