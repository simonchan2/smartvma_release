﻿namespace SmartVMA.Core.Enums
{
    public enum AppointmentPresentationStatus
    {
        V,  //Void
        C,  //Closed
        M,  //Menu Presented
        MC, //Menu Accepted
        MD, //Menu Declined
        MK,  //Menu Parked
        PD, //Parked and Deleted
        DMS,  //DMS Appointment Presentation
    }
}
