﻿namespace SmartVMA.Core.Enums
{
    public enum ILifeColor : int
    {
        Green,
        Yellow,
        Red
    }
}
