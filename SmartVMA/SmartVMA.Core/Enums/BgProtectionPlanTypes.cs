﻿namespace SmartVMA.Core.Enums
{
    /// <summary>
    /// This enum represent BgProtectionPlans table in db.
    /// Right now the plans are stored in this tables
    /// </summary>
    public enum BgProtectionPlanTypes
    {
        LifetimeProtection = 1,
        ForeverDiesel = 2,
        EngineAssurance = 3
    }
}
