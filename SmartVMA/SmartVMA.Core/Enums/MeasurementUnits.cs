﻿namespace SmartVMA.Core.Enums
{
    public enum MeasurementUnits : int
    {
        Imperial = 0,
        Metric = 1
    }
}
