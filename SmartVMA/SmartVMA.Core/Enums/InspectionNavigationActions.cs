﻿namespace SmartVMA.Core.Enums
{
    public enum InspectionNavigationActions
    {
        Services = 1,
        LOF = 2,
        Print = 3,
        ClearAll = 4
    }
}
