﻿namespace SmartVMA.Core.Enums
{
    public enum AppointmentFilterStatus
    {
        All,
        Scheduled,
        Completed
    }
}
