﻿namespace SmartVMA.Core.Enums
{
    /// <summary>
    /// This enum represent service table in db.
    /// Right now the services are stored in this tables
    /// </summary>
    public enum ServiceType
    {
        OemBasicService = 1,
        AdditionalService = 2,
        BgService = 3,
        AdHocAdditionalService = 4, // for this type there isn't any table (currently stored in AppointmentServices (OriginalServiceId nullable))
        //AppointmentServices = 5
    }
}
