﻿namespace SmartVMA.Core.Enums
{
    public enum OilViscosity : int
    {
        Viscosity0W20,
        Viscosity0W30,
        Viscosity5W20,
        Viscosity5W30,
        Viscosity5W40,
        Viscosity10W30,
        Viscosity20W50
    }
}
