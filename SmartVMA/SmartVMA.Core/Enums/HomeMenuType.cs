﻿namespace SmartVMA.Core.Enums
{
    public enum HomeMenuType : int
    {
        ParkedMenu,
        AcceptedMenu
    }
}
