﻿namespace SmartVMA.Core.Enums
{
    public enum DealerVehicleStatusCodes
    {
        Active = 0,
        Inactive = 1
    }
}
