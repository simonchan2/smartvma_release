﻿namespace SmartVMA.Core.Enums
{
    public enum _Languages
    {
        English = 1,
        French = 2,
        Spanish = 3
    }
}
