﻿using System.Collections.Generic;

namespace SmartVMA.Core.Enums
{
    public class DateFormatMap
    {
        public IDictionary<string, string> DateFormatMapDictionary = new Dictionary<string, string> {
            { (DateFormat.DDMMYYYY).ToString(), "dd-MM-yyyy" },
            { (DateFormat.MMDDYYYY).ToString(), "MM-dd-yyyy" },
            { (DateFormat.YYYYMMDD).ToString(), "yyyy-MM-dd" }
        };
   }
}
