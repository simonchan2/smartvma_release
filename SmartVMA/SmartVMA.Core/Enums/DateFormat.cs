﻿using System.Collections.Generic;

namespace SmartVMA.Core.Enums
{
    public enum DateFormat
    {
        DDMMYYYY = 1,
        MMDDYYYY = 2,
        YYYYMMDD = 3
    }
}
