﻿namespace SmartVMA.Core.Enums
{
    public enum PdfAction
    {
        View = 1,
        Save = 2,
        Print = 3,
        Email = 4
    }
}
