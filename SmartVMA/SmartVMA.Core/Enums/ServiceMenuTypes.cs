﻿namespace SmartVMA.Core.Enums
{
    /// <summary>
    /// Type of the service menu
    /// </summary>
    public enum ServiceMenuTypes
    {
        MaintenanceService = 1,
        AdditionalService = 2
    }
}