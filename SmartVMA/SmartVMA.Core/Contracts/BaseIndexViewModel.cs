﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace SmartVMA.Core.Contracts
{
    public abstract class BaseIndexViewModel<T>
    {
        [JsonProperty("data")]
        public IEnumerable<T> Items { get; set; }

        [JsonProperty("recordsTotal")]
        public int ItemsCount { get; set; }


        [JsonProperty("recordsFiltered")]
        public int RecordsFilteredCount { get; set; }

        [JsonProperty("draw")]
        public int Draw { get; set; }
    }
}
