﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace SmartVMA.Core.Contracts
{
    internal interface IRepository<T, K> where T : IBaseEntity<K>
    {
        IEnumerable<T> Find(Expression<Func<T, bool>> expression = null);
        T FirstOrDefault(Expression<Func<T, bool>> expression = null);
       
        void Insert(T entity);
        void Insert(IEnumerable<T> entities);
        void Update(T entity);
        void Delete(T entity);
        void Delete(IEnumerable<T> entities);
        void Delete(K id);


        ServiceResponse CommitChanges();
    }
}
