﻿using System.Collections.Generic;

namespace SmartVMA.Core.Contracts
{
    public class ServiceResponse
    {
        public ServiceResponse()
        {
            Errors = new Dictionary<string, string>();
        }

        public bool IsSuccess { get; set; }

        public Dictionary<string, string> Errors { get; set; }

        public long? SavedItemId { get; set; }

        public bool PreventModalFromClosing { get; set; }

        private string _redirectUrl;
        public string RedirectUrl
        {
            get { return _redirectUrl; }
            set
            {
                _redirectUrl = value;
                PreventModalFromClosing = true;
            }
        }
    }
}