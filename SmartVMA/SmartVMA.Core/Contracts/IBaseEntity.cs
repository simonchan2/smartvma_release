﻿using System;

namespace SmartVMA.Core.Contracts
{
    internal interface IBaseEntity<T>
    {
        T Id { get; set; }
        DateTime TimeCreated { get; set; }
        DateTime TimeUpdated { get; set; }
    }
}