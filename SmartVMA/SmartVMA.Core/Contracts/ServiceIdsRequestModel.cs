﻿using SmartVMA.Core.Enums;

namespace SmartVMA.Core.Contracts
{
    public class ServiceIdsRequestModel
    {
        public int Id { get; set; }

        public ServiceType ServiceTypeId { get; set; }
    }
}
