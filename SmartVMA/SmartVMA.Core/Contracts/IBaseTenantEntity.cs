﻿namespace SmartVMA.Core.Contracts
{
    interface IBaseTenantEntity<T> : IBaseEntity<T>
    {
        int TenantId { get; set; }
    }
}
