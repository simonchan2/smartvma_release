﻿using SmartVMA.Core.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Core.Entities
{
    internal partial class Inspection : IBaseTenantEntity<long>
    {
        public int TenantId
        {
            get
            {
                return DealerId;
            }

            set
            {
                DealerId = value;
            }
        }
    }
}
