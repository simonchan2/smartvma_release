﻿namespace SmartVMA.Core.OutputModels
{
    public class OverrideRuleConflictsServiceDescriptionsModel
    {
        public long RuleId { get; set; }
        public string FullDescription { get; set; }
    }
}
