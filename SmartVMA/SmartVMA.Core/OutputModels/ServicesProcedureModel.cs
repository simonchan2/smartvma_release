﻿using System;

namespace SmartVMA.Core.OutputModels
{
    public class ServicesProcedureModel
    {
        public long ServiceId { get; set; }

        public string OpCode { get; set; }

        public string FullDescription { get; set; }

        public int CarId { get; set; }

        public int? Mileage { get; set; }

        public bool IsFluid { get; set; }

        public bool IsEngineOil { get; set; }

        public bool IsLOF { get; set; }

        public bool CanBeStriked { get; set; }

        public bool IsStrikeOut { get; set; }

        public bool IsDeclined { get; set; }

        public bool IsPreviouslyServed { get; set; }

        public int? BgProtectionPlanId { get; set; }

        public string VideoCode { get; set; }

        public int MenuLevel { get; set; }

        public int ServiceType { get; set; }

        public int ServiceMenuType { get; set; }

        public double LaborHour { get; set; }

        public double LabourRate { get; set; }

        public double PartsPrice { get; set; }

        public double LaborPrice { get; set; }

        public double Price { get; set; }

        public double ShopChargesPartsPrice { get; set; }

        public double ShopChargesLaborPrice { get; set; }

        public double TaxIncludedPartsPrice { get; set; }

        public double TaxIncludedLaborPrice { get; set; }

        public double? GrossPrice { get; set; }

        public bool? IsAddedAfter { get; set; }

        public int? MaxMileageBeforeFirstService { get; set; }

        public int? ServiceInterval { get; set; }

        public string BgProtectionPlanUrl { get; set; }

        public bool? IsAdditionalAddedAfter { get; set; }

        public int? PreviousMileage { get; set; }

        public DateTime? PreviousServedDate { get; set; }
    }
}
