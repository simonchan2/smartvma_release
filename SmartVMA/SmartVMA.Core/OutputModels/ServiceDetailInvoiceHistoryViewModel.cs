﻿namespace SmartVMA.Core.OutputModels
{
    public class ServiceDetailInvoiceHistoryViewModel
    {
        public string OpCode { get; set; }

        public int Mileage { get; set; }
    }
}