﻿using System.Collections.Generic;

namespace SmartVMA.Core.OutputModels
{
    public class OverrideRuleConflictsModel
    {
        public OverrideRuleConflictsModel()
        {
            ServiceDescriptions = new List<OverrideRuleConflictsServiceDescriptionsModel>();
        }
        public long RuleId { get; set; }
        public string RuleName { get; set; }
        public int? LaborHourPercentage { get; set; }
        public decimal? LaborHourAmount { get; set; }
        public int? LaborPricePercentage { get; set; }
        public decimal? LaborPriceAmount { get; set; }
        public int? PartPricePercentage { get; set; }
        public decimal? PartPriceAmount { get; set; }
        public string OpCodeOverride { get; set; }
        public string PartNoOverride { get; set; }
        public int? NewLaborHourPercentage { get; set; }
        public decimal? NewLaborHourAmount { get; set; }
        public int? NewLaborPricePercentage { get; set; }
        public decimal? NewLaborPriceAmount { get; set; }
        public int? NewPartPricePercentage { get; set; }
        public decimal? NewPartPriceAmount { get; set; }
        public string NewOpCodeOverride { get; set; }
        public string NewPartNoOverride { get; set; }
        public int ConflictingServices { get; set; }

        public List<OverrideRuleConflictsServiceDescriptionsModel> ServiceDescriptions { get; set; }
    }
}
