﻿using SmartVMA.Core.InputModels.ViewModels;
using System;
using System.Collections.Generic;
using System.Net.Mail;

namespace SmartVMA.Core.Services
{
    public interface IPdfDocumentService
    {
        PdfDocumentViewModel GePdfDocumentModel(long appointmentPresentationId, int documentType);
        //PdfCustomerCopyViewModel GetPdfCustomerCopyAppointmentPresentation(int appointmentPresentationID);
        void AddPdfDocument(PdfDocumentViewModel model);
        PdfDocumentIndexViewModel GetDocuments(string VIN, string InvoiceNumber, DateTime? startDate, DateTime? endDate);
        void AddPdfDocumentToPrintQueue(int dealerCustomerId, int? userId, string fileName, long appointmentPresentationID, string prnJobTypeCode);

        PdfHeaderViewModel GetPdfHeaderViewModel(int dealerId, long appointmentPresentationId, int? mileage = null);
        PdfHeaderCustomerInfoViewModel GetPdfHeaderCustomerInfoViewModel(int dealerId, long appointmentPresentationId);
        PdfHeaderCustomerInfoViewModel GetPdfHeaderCustomerInfoViewModelFromVin(string vin);
        PartsCopyPdfViewModel GetPartsCopyPdfViewModel(long appointmentPresentationId);
        TechCopyPdfViewModel GetTechCopyPdfViewModel(long appointmentPresentationId);
        CustomerCopyPdfViewModel GetCustomerCopyPdfViewModel(long appointmentPresentationId);
        WalkaroundInspectionPdfViewModel GetWalkaroundInspectionPdfViewModel(long? inspectionId, long? appointmentPresentationId);
        WalkaroundInspectionPdfViewModel GetWalkaroundInspectionPdfViewModel(long? inspectionId, long? appointmentPresentationId, bool hideVehiclePhotos);
        PdfFooterViewModel GetPdfFooterViewModel(long? appointmentPresentationId, DateTime? printedDate);
        PdfHeaderAdvisorViewModel GetPdfHeaderAdvisorViewModel(int userId);
        PdfHeaderDealerViewModel GetPdfHeaderDealerViewModel();

        //bool SendPdfAsEmail(string email, List<Attachment> attachments);
        bool SendPdfAsEmail(string replyto, string messageTo, string messageSubject, string messageBody, List<Attachment> attachments);
    }
}
