﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Core.Services
{
    public interface IRoleService
    {
        RoleIndexViewModel GetIndexViewModel();
        RoleViewModel GetViewModel(int? id = null);
        ServiceResponse Save(RoleViewModel model);
        ServiceResponse Delete(RoleViewModel model);
        RoleIndexViewModel GetPagedItems(PagingRequest request);
        string GetRoleName(int roleId);
    }
}
