﻿using SmartVMA.Core.InputModels.ViewModels;
using System.Collections.Generic;
using SmartVMA.Core.InputModels.ViewModels.Dealer.MenuPresentations;

namespace SmartVMA.Core.Services
{
    public interface ICarService
    {
        List<VehicleYearInfo> GetAllVehicleYears();

        List<VehicleMakeInfo> GetAllVehicleMakes();

        List<VehicleMakeInfo> GetFilteredVehicleMakesByYear(string year);

        List<VehicleModelInfo> GetAllVehicleModels();

        List<VehicleModelInfo> GetFilteredVehicleModels(string years, string makes);

        List<VehicleEngineInfo> GetAllVehicleEngines();

        List<VehicleEngineInfo> GetFilteredVehicleEngines(string years, string makes, string models);

        List<VehicleTransmissionInfo> GetAllVehicleTransmissions(string source);

        List<VehicleTransmissionInfo> GetFilteredVehicleTransmissions(string years, string makes, string models, string engines, string source);

        List<VehicleDriveLineInfo> GetAllVehicleDriveLines(string source);

        List<VehicleDriveLineInfo> GetFilteredVehicleDriveLines(string years, string makes, string models, string engines, string transmissions, string source);

        int? GetCarId(int year, int make, int model, int engine, int transmission, int driveLine);

        VehicleFilterViewModel GetVehicleServiceFilter();

        IEnumerable<int> GetAllDistinctMileages(int measurmentUnitId);
        IEnumerable<int> GetAllDistinctMileages(int carId, int measurmentUnitId);

        OdometerResponseModel FindMenuInterval(OdoMeterViewModel model);
        OdometerResponseModel FindMenuInterval(int carId, int mileage);
        IEnumerable<int> GetAllMileages(int carId, int mileage);

        VehicleFilterResponseViewModel GetVehicleFilter(VehicleFilterViewModel filter);
        bool VinExists(string vin);


    }
}
