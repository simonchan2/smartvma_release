﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using System;
using System.Collections.Generic;

namespace SmartVMA.Core.Services
{
    public interface IAppointmentService
    {
        AppointmentIndexViewModel GetCalendarAppointmentsModel(DateTime startTime, DateTime endTime, string advisorIds, string presentationStatus);
        AppointmentIndexViewModel GetSearchAppointmentsModel(string customerName, string customerNr, string customerPhone, string vin, string vehicleStock, string licensePlate);
        List<AppointmentServiceViewModel> GetAppointmentServiceModel(int dealerCustomerID);
        List<AppointmentServiceViewModel> GetAppointmentServiceModel(string vin);
        AdditionalServiceViewModel GetViewModel(int? id = null);

        List<PdfDocumentViewModel> GetPdfDocuments(DateTime DateTo);
        void DeleteObsoleteAppointments(DateTime DateTo);

        long CreateAppointmentPresentation(long appointmentID, int? userId, string status);

        ServiceResponse Save(AdditionalServiceViewModel model);
    }
}
