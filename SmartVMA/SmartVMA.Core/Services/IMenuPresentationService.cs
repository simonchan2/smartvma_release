﻿using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Core.Services
{
    public interface IMenuPresentationService : IBaseServicePresentationService
    {
        ServiceBasePresentationViewModel GetViewModel(int? carId, int? mileage, int? dealerCustomerId, long? appointmentPresentationId, long? inspectionId, int? vehicleTransmission, int? vehicleDriveline, int? enteredMileage, int? dealerVehicleId, string vin, int? carIdUndetermined);
    }
}
