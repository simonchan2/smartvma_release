﻿using System.Collections.Generic;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Core.Services
{
    public interface IBgVideoClipsService
    {
        IEnumerable<BgVideoCliplsViewModel> GetVideos();

        IEnumerable<BgVideoCliplsViewModel> GetVideos(List<int> bgProducts);
    }
}
