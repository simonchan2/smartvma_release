﻿using SmartVMA.Core.InputModels.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Core.Services
{
    public interface IDealerVehicleService
    {
        int? GetDealerVehicleId(int year, int make, int model, int engine, string transmission, string driveLine);
    }
}
