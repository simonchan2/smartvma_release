﻿using System.Collections.Generic;
using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Core.Services
{
    public interface IInspectionService
    {
        InspectionViewModel GetViewModel(long? inspectionId, long? appointmentId, int? transmissionId, int? driveLineId, int? enteredMileage);
        VehiclePhotoViewModel GetPhoto(int id);
        List<VehiclePhotoViewModel> GetAllPhotos(long inspectionId);
        void ResetInspection(long inspectionId);
        ServiceResponse DeletePhoto(VehiclePhotoViewModel model);
        ServiceResponse AddPhoto(VehiclePhotoViewModel photo);
        ServiceResponse AddSetInspection(InspectionViewModel model);
        DamageIndicationViewModel GetDamageIndication(long id);
        ServiceResponse DeleteDamageIndication(long id);
        ServiceResponse SaveDamageIndication(DamageIndicationViewModel model);
        ServiceResponse LinkInspectionToAppointmentPresentation(long inspectionId, long appointmentPresentationId);
        void SetAppointmentToInspection(long inspectionId, long appointmentPresentationId);
    }
}
