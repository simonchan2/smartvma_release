﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.InputModels.ViewModels;
using System.Collections.Generic;

namespace SmartVMA.Core.Services
{
    public interface IILifeService
    {
        ILifeViewModel GetILifeByVin(string vin);

        ILifeViewModel ButtonInfo(string vin);
    }
}
