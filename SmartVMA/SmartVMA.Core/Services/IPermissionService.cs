﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Core.Services
{
    public interface IPermissionService
    {
        PermissionIndexViewModel GetIndexViewModel(int roleId);
        PermissionViewModel GetViewModel(int id, int roleId);

        /// <summary>
        /// Load all actions from all controllers. If the action doesn't have AllowAnonymousAttribute
        /// then it is saved in db with name in the following format: controllerName.actionName.
        /// If the action have AllowAnonymousAttribute then the permission won't be saved in database.
        /// Also that action(with AllowAnonymousAttribute) can be accessed by anonymous users.
        /// </summary>
        /// <returns></returns>
        ServiceResponse UpdatePermissions();

        /// <summary>
        /// Grants permissions for specific role
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        ServiceResponse GrantPermissionsForRole(PermissionSaveViewModel model);

        ServiceResponse Save(PermissionViewModel model);

        /// <summary>
        /// Checks if the current user has access to the resource with
        /// the following url: /controllerName/actionName
        /// </summary>
        /// <param name="actionName"></param>
        /// <param name="controllerName"></param>
        /// <returns></returns>
        bool IsPermissionGrantedForCurrentUser(string actionName, string controllerName);

        PermissionIndexViewModel GetPagedItems(PagingRequest request, int roleId);
    }
}
