﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.InputModels.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Core.Services
{
    public interface IArticleService
    {
        ArticleViewModel GetArticle(int id);
        ArticleIndexViewModel GetIndexViewModel();
        ArticleViewModel GetViewModel(int? id = null);
        ServiceResponse Save(ArticleViewModel model);
        ServiceResponse Delete(int id);
        ArticleIndexViewModel Search(string key, int[] categoryId);
        ArticleIndexViewModel GetPagedItems(PagingRequest request);
    }
}
