﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.ViewModels;
using System;
using System.Collections.Generic;
using SmartVMA.Core.Enums;
using SmartVMA.Core.OutputModels;

namespace SmartVMA.Core.Services
{
    public interface IAppointmentPresentationService
    {
        AdvisorMenusViewModel GetDefaultAdvisorMenus(DateTime? startDate = null, DateTime? endDate = null, int? timeFrameId = null);

        AppointmentPresentationIndexViewModel ListParkedMenus(DateTime? startDate = null, DateTime? endDate = null, List<long> advisorIds = null, int? timeframeId = null);

        AppointmentPresentationIndexViewModel ListAcceptedMenus(DateTime? startDate = null, DateTime? endDate = null, List<long> advisorIds = null, int? timeframeId = null);

        List<AdvisorInfo> GetAdvisorList();

        long? GetAppointmentPresentationId(int year, int make, int model, int engine, string transmission, string driveLine, int mileage);

        long GetAppointmentPresentationId(long appointmentId);
        ServiceResponse Save(SaveConfirmServiceRequestModel request, bool isLofMenu);
        
        string GetAppointmentPresentationStatus(long appointmentId);
        string GetAdvisorCode(long appointmentId);
        ServiceResponse DeleteParkedMenu(long? appointmentPresentationId);
        AppointmentPresentationViewModel ReopenMenu(long appointmentPresentationId);

        ServiceResponse Create(int customerId, int dealerVehicleId);
        AppoitmentPresentationViewCountsModel IsExistingAppointmentPresentationByDateUserVehicle(long? appointmentPresentationId,DateTime date, int? userId, int? dealerVehicleId, string appointmentPresentationStatus);

        ServicePricesViewModel CalculatePriceCharges(double? partsPrice, double? laborPrice);

    }
}
