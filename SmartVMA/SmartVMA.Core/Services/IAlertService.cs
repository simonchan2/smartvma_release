﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.InputModels.ViewModels;
using System.Collections.Generic;

namespace SmartVMA.Core.Services
{
    public interface IAlertService
    {
        List<AlertViewModel> GetAlerts();
        AlertViewModel GetAlert(int id);
        AlertIndexViewModel GetIndexViewModel();
        AlertViewModel GetViewModel(int? id = null);
        ServiceResponse Save(AlertViewModel model);
        ServiceResponse Delete(int alertId);
        AlertIndexViewModel GetActiveAlertsForCurrentUser();
        AlertIndexViewModel GetPagedItems(PagingRequest request);
    }
}
