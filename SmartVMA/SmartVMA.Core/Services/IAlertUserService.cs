﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Core.Services
{
    public interface IAlertUserService
    {
        ServiceResponse Save(AlertUserViewModel model);
    }
}
