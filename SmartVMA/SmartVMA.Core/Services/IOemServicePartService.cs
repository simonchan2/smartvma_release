﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Core.Services
{
    public interface IOemServicePartService
    {
        ServiceResponse Save(OemServicePartViewModel model);
        OemServicePartViewModel GetOemServicePartById(int id);
    }
}
