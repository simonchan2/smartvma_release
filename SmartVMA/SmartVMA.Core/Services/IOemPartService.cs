﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.ViewModels;
using System.Collections.Generic;

namespace SmartVMA.Core.Services
{
    public interface IOemPartService
    {
        OemPartIndexViewModel GetIndexViewModel();
        OemPartViewModel GetViewModel(int? id = null, int? basicServiceId = null);
        IEnumerable<string> GetDistinctViscosity();

        IEnumerable<string> GetDistinctOilTypes();
        ServiceResponse Save(OemPartViewModel model);
        ServiceResponse Delete(int id);
    }
}
