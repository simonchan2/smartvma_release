﻿using System.Collections.Generic;
using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.InputModels.ViewModels.Admin.CustomServices;

namespace SmartVMA.Core.Services
{
    public interface IAdditionalServiceService
    {
        AdditionalServiceViewModel GetViewModel(int? id = null);

        ServiceResponse Save(AdditionalServiceViewModel model);

        ServiceResponse Save(IEnumerable<CustomServiceViewModel> services);

        ServiceResponse Save(MapCustomServicesToVehiclesViewModel model);

        CustomServiceInitViewModel GetCustomServiceInitViewModel();

        CustomServiceIndexViewModel GetIndexViewModel(CustomServiceMaintenancePagingViewModel request);

        CustomServiceIndexViewModel GetAllServicesIndexViewModel(CustomServiceMaintenancePagingViewModel request);


        ServiceResponse Validate(IEnumerable<CustomServiceViewModel> services);
        ServiceResponse DeleteBulk(string ids);
    }
}
