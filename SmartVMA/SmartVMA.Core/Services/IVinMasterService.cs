﻿using System.Collections.Generic;

namespace SmartVMA.Core.Services
{
    public interface IVinMasterService
    {
        List<string> GetAllVehicleSteerings();
    }
}
