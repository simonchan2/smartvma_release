﻿using SmartVMA.Core.InputModels.ViewModels;
using System.Collections.Generic;

namespace SmartVMA.Core.Services
{
    public interface ICountryService
    {
        IEnumerable<CountryViewModel> GetCountries();

        IEnumerable<StateViewModel> GetStates(int? countryId = null);
    }
}
