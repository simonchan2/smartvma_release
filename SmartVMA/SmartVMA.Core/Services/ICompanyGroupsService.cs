﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Core.Services
{
    public interface ICompanyGroupsService
    {
        CompanyGroupsIndexViewModel GetDealerGroups();
        CompanyGroupViewModel CompanyGroupViewModel(int? id = null);
        ServiceResponse Save(CompanyGroupViewModel model);
        ServiceResponse Delete(CompanyGroupViewModel model);
        CompanyGroupsIndexViewModel GetPagedItems(PagingRequest request);
    }
}
