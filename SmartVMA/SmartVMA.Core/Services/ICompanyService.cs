﻿using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.InputModels.ViewModels;
using System.Collections.Generic;
using SmartVMA.Core.Entities;

namespace SmartVMA.Core.Services
{
    public interface ICompanyService
    {
        CompanyIndexViewModel GetIndexViewModel();

        int? GetCompanyCountryId(int companyId);

        CompanyIndexViewModel GetPagedCompanies(PagingRequest request);

        CompanyIndexViewModel GetDistributors();

        CompanyIndexViewModel GetPagedDistributors(PagingRequest request);

        CompanyIndexViewModel GetDealers(int? distributorId = null);

        CompanyIndexViewModel GetPagedDealers(PagingRequest request, int? distributorId = null);

        CompanyIndexViewModel GetCorporations();

        CompanyIndexViewModel GetCompaniesForUser(string email);

        IEnumerable<CompanyHierarchialViewModel> GetParentCompaniesNames(int? id = null);
        List<SettingViewModel> GetAllSettings();
        int GetSettingAsInteger(CompanySettingsNames setting, int defaultValue = 0);
        bool GetSettingAsBool(CompanySettingsNames setting, bool defaultValue = false);
        string GetSettingAsString(CompanySettingsNames setting);
        decimal GetSettingAsDecimal(CompanySettingsNames setting, decimal defaultValue = 0);
        double GetSettingAsDouble(CompanySettingsNames setting, double defaultValue = 0);

        string GetDateTimeFormat(CompanySettingsNames setting);

        string GetCompanyLogo(CompanySettingsNames setting);
        string GetCompanyLogoPath(CompanySettingsNames setting);

        string GetDefaultCompanySetting(CompanySettingsNames setting);

        IEnumerable<LOFMenu> GetLOFMenuLevels(int id);

        IEnumerable<string> GetMenuButtonsOrder();

        string GetCompanySettingValue(CompanySettingsNames setting);
    }
}
