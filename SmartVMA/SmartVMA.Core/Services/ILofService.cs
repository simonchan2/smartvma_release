﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.ViewModels;
using System.Collections.Generic;

namespace SmartVMA.Core.Services
{
    public interface ILofService : IBaseServicePresentationService
    {
        ServiceBasePresentationViewModel GetViewModel(int? carId, int? mileage, int? dealerCustomerId, long? appointmentPresentationId, long? inspectionId, int? vehicleTransmission, int? vehicleDriveline, int? enteredMileage, int? dealerVehicleId, string vin, int? carIdUndetermined);

        ConfigurationLOFMenuViewModel GetLOFMenuViewModel(int? id = null, int level = 1);

        List<LOFMenuImage> GetLOFImages();

        ServiceResponse SaveLOFMenuImages(int companyID, string imageName, bool isSelected, int selectedLOFMenuLevel);

        List<string> GetLevelImages(string LevelImages);
    }
}
