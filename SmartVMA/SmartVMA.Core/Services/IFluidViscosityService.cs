﻿using SmartVMA.Core.InputModels.ViewModels;
using System.Collections.Generic;

namespace SmartVMA.Core.Services
{
    public interface IFluidViscosityService
    {
        List<FluidViscosityViewModel> GetAllFluidViscosityInDealerOilViscosityList();
    }
}
