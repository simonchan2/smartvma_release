﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Core.Services
{
    public interface IConfigurationSetupService
    {
        ConfigurationProfileSetupViewModel GetProfileSetupViewModel(int? id = null);
        ConfigurationProfileSetupDistributorViewModel GetProfileSetupDistributorViewModel(int? id = null);
        ConfigurationProfileSetupDistributorViewModel GetProfileSetupDefaultDistributorViewModel(int corpId);
        ConfigurationProfileSetupDealersViewModel GetProfileSetupDealersViewModel(int? id = null);
        ConfigurationProfileSetupDealersViewModel GetProfileSetupDefaultDealerViewModel(int distributorId);
        ConfigurationCorpApplicationSettingsViewModel GetCorpApplicationSettings(int id);
        ConfigurationApplicationSettingViewModel GetApplicationSettingsViewModel(int id);
        ConfigurationDocumentSetupViewModel GetDocumentSetupViewModel(int id);
        ConfigurationMenuSetupViewModel GetMenuSetupViewModel(int id);

        ServiceResponse SaveProfileSetup(ConfigurationProfileSetupViewModel model, CompanyTypes companyType);
        ServiceResponse SaveProfileSetup(ConfigurationProfileSetupDealersViewModel model, CompanyTypes companyType);
        ServiceResponse SaveProfileSetup(ConfigurationProfileSetupDistributorViewModel model, CompanyTypes companyType);

        ServiceResponse SaveMenuSetup(ConfigurationMenuSetupViewModel model);
        ServiceResponse SaveDocumentsSetup(ConfigurationDocumentSetupViewModel model);
        ServiceResponse SaveApplicationSettings(ConfigurationApplicationSettingViewModel model);
        ServiceResponse SaveApplicationSettings(ConfigurationCorpApplicationSettingsViewModel model);
      
    }
}
