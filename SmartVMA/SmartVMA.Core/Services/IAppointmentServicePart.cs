﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using System;
using System.Collections.Generic;

namespace SmartVMA.Core.Services
{
    public interface IAppointmentServicePart
    {
        List<AppointmentServiceViewModel> GetAppointmentServicePartModel(int dealerCustomerID);
        AdditionalServiceViewModel GetViewModel(int? id = null);

        ServiceResponse Save(AdditionalServiceViewModel model);
    }
}
