﻿using SmartVMA.Core.InputModels.ViewModels;
using System.Collections.Generic;

namespace SmartVMA.Core.Services
{
    public interface IDMSUserTypeService
    {
        IEnumerable<DMSUserTypesViewModel> GetDmsUserTypes();
    }
}
