﻿using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Core.Services
{
    public interface IUserStatusCodeService
    {
        UserStatusCodeIndexViewModel GetIndexViewModel();
    }
}
