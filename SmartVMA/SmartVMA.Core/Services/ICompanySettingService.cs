﻿using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using System.Collections.Generic;

namespace SmartVMA.Core.Services
{
    public interface ICompanySettingService
    {
        IEnumerable<LanguageViewModel> GetLanguages();
        IEnumerable<DateFormatViewModel> GetDateFormats();
        IEnumerable<MeasurementUnitViewModel> GetMeasurementUnits();

        string GetSetting(CompanySettingsNames setting);
        IEnumerable<LanguageViewModel> GetLanguagesCode();
    }
}
