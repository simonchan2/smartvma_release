﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Core.Services
{
    public interface IArticleCategoryService
    {
        List<ArticleCategoryViewModel> GetArticleCategories();
        ArticleCategoryViewModel GetArticleCategory(int id);
        ArticleCategoryIndexViewModel GetIndexViewModel();
        ArticleCategoryViewModel GetViewModel(int? id = null);
        ServiceResponse Save(ArticleCategoryViewModel model);
        ServiceResponse Delete(int id);
    }
}
