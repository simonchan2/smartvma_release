﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.InputModels.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Core.Services
{
    public interface IEngineOilORuleService
    {
        EngineOilConfigurationIndexViewModel GetIndexViewModel();
        EngineOilConfigurationViewModel GetViewModel(int? id = null);
        ServiceResponse Save(EngineOilConfigurationViewModel model);
        ServiceResponse Delete(int id);
        EngineOilConfigurationIndexViewModel GetAllEngineOilORuleForDealer();
        EngineOilConfigurationIndexViewModel GetPagedItems(PagingRequest request);
        EngineOilConfigurationViewModel GetEngineOilOverride(string oilType, string oilViscosity, int? menuLevel = null);
    }
}
