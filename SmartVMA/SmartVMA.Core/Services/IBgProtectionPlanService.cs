﻿using System.Collections.Generic;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Core.Services
{
    public interface IBgProtectionPlanService
    {
        IEnumerable<BgProtectionPlanViewModel> GetPlans(List<int> productIds);
        IEnumerable<BgProtectionPlanViewModel> GetPlans();
    }
}
