﻿using System.Collections.Generic;
using SmartVMA.Core.Contracts;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Core.Services
{
    public interface IUserService
    {
        LoginResponseViewModel Login(LoginViewModel model);
        UserIndexViewModel GetIndexViewModel(PagingRequest request, int? companyId = null);
        UserViewModel GetViewModel(int? id = null);
        UserViewModel GetViewModel(int companyId, UserRoles userRole);

        ServiceResponse Save(UserViewModel model);
        ServiceResponse Delete(UserViewModel model);


        // password management methods
        ServiceResponse CreateResetPasswordRequest(PasswordForgotViewModel model);
        ServiceResponse ResetPassword(PasswordResetViewModel model);
        ServiceResponse ChangePassword(PasswordChangeViewModel model);
        void LogOut();
        ServiceResponse CreateResetPasswordRequest(int id);

        // profile management
        AccountManagementViewModel GetManageAccountViewModel();
        ServiceResponse Save(AccountManagementViewModel model);
        int GetSettingAsInteger(UserSettingsNames setting, int defaultValue = 0);
        bool GetSettingAsBool(UserSettingsNames setting, bool defaultValue = false);
        string GetSettingAsString(UserSettingsNames setting);
        decimal GetSettingAsDecimal(UserSettingsNames setting, decimal defaultValue = 0);
        double GetSettingAsDouble(UserSettingsNames setting, double defaultValue = 0);
        UserIndexViewModel GetUsersByRole(int companyId, UserRoles userRole);

        string GetUserImagesSettingAsString(UserSettingsNames setting);
        string GetLanguageCode(UserSettingsNames setting);
        List<SettingViewModel> GetAllSettings();
        string Decrypt(string strToEncrypt);
    }
}
