﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Core.Services
{
    public interface IBaseServicePresentationService
    {
        ConfirmServiceViewModel GetConfirmServiceViewModel(ConfirmServiceRequestModel request);

        ServiceResponse Save(SaveConfirmServiceRequestModel request);

        ServiceResponse CheckValidRequest(SaveConfirmServiceRequestModel request);
    }
}
