﻿using System.Collections.Generic;
using SmartVMA.Core.InputModels.ViewModels.Admin;

namespace SmartVMA.Core.Services
{
   public interface IBgSubProtectionPlanService
   {
       IEnumerable<BgProtectionSubPlanViewModel> GetSubPlans(int bgProtectionPlanId);
        IEnumerable<BgProtectionSubPlanViewModel> GetSubPlans();
    }
}
