﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartVMA.Core.InputModels.PageRequests;

namespace SmartVMA.Core.Services
{
    public interface IOverrideRulesService
    {
        ConfigurationSearchByOverridesViewModel GetOverrideRuleById(long? id);
        ConfigurationOverrideRulesViewModel GetOverrideRuleByIdForList(long? id);

        ConfigurationOverrideRulesIndexViewModel GetOverrideRules();
        //List<ConfigurationOverridesSearchResults> SearchOverrides(ConfigurationSearchByOverridesViewModel model,
        //    int pageNumber, int pageSize);

        List<ConfigurationOverridesSearchResults> SearchOverrides(ConfigurationSearchByOverridesViewModel model, int pageNumber, int pageSize);

        ServiceResponse Save(ConfigurationOverrideRulesViewModel model);

        ServiceResponse Delete(long id);
        ServiceResponse DeleteMany(string ruleIds);

        List<ConfigurationOverrideRulesConflictsViewModel> GetConflicts(ConfigurationSearchByOverridesViewModel model, string basicServices, bool areServicesChecked);

        ServiceResponse ApplyOverride(ConfigurationSearchByOverridesViewModel model, string basicServices, bool areServicesChecked, string ruleIdsToDelete);
        //ServiceResponse Create(ConfigurationSearchByOverridesViewModel model);
        //ServiceResponse Delete();
        ConfigurationOverrideRulesIndexViewModel GetPagedItems(PagingRequest request);
    }
}
