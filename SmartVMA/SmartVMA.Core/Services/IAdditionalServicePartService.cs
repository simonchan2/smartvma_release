﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Core.Services
{
    public interface IAdditionalServicePartService
    {
        AdditionalServiceViewModel GetViewModel(int? id = null);

        ServiceResponse Save(AdditionalServiceViewModel model);
    }
}
