﻿
namespace SmartVMA.Core.Services
{
    public interface IOemComponentService
    {
        OemComponentIndexViewModel GetAllOemComponentsShownAsOption();
    }
}
