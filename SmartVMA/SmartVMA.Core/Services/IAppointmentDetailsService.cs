﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using System;
using System.Collections.Generic;

namespace SmartVMA.Core.Services
{
    public interface IAppointmentDetailsService
    {
        List<AppointmentDetailsViewModel> GetAppointmentDetailsList(int appointmentID);
    }
}
