﻿using System.Collections.Generic;
using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Core.Services
{
    public interface IOemBasicServiceService
    {
        OemBasicServiceIndexViewModel GetIndexViewModel();
        OemBasicServiceViewModel GetViewModel(int? id = null);
        ServiceResponse SaveFromDataMaintenance(OemBasicServiceViewModel model);
        ServiceResponse Delete(int id);
        OemBasicServiceIndexViewModel GetPagedItems(PagingRequest request);

       
        OemServiceMaintenanceInitViewModel GetAmamServicesViewModel();
        OemServiceMaintenanceIndexViewModel GetAmamServicesIndexViewModel(OemServiceMaintenancePagingViewModel request);
        ServiceResponse Save(IEnumerable<OemServiceMaintenanceViewModel> items, OemServiceMaintenancePagingViewModel filter);
        IEnumerable<string> GetOpActions();
        ServiceResponse Validate(IEnumerable<OemServiceMaintenanceViewModel> items);
        ServiceResponse DeleteBulk(string ids);
    }
}
