﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Core.Services
{
    public interface IDealerCustomerService
    {
        DealerCustomerViewModel GetViewModel(int? id = null, SaveConfirmServiceRequestModel request = null);
        DealerCustomerViewModel GetViewModel(int id);
        CustomerInfoViewModel GetCustomerInfo(int? dealerCustomerId, int carId, int? transmissionId, int? driveLineId, int? dealerVehicleId);
        CustomerInfoViewModel GetCustomerInfo(long appointmentPresentationId);

        AdvisorSearchIndexViewModel CustomerSearch(string customerName, string customerNumber, string customerPhoneNumber, string vin, string vehicleStock, string licensePlate);
        ServiceResponse Save(DealerCustomerViewModel model);

        string GetVinByCustomerSearchInfo(string customerName, string customerNumber, string customerPhoneNumber, string vehicleStock, string licensePlate);
    }
}
