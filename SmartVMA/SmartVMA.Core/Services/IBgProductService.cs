﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.InputModels.ViewModels;
using System.Collections.Generic;

namespace SmartVMA.Core.Services
{
    public interface IBgProductService 
    {
        List<BgProductCategoryInfo> GetAllBgProductCategories();

        List<BgProductCategoryInfo> GetAllBgProductCategories(List<int> bgProducts);

        BgProductViewModel GetBgProductProtectionPlanBySKUs(int id);

        ServiceResponse SaveBgProduct(BgProductViewModel model);

        BgProductIndexViewModel GetPagedItems(PagingRequest request, string categoryIds);

        ServiceResponse Delete(BgProductViewModel model);

        ServiceResponse PartNumberValidation(BgProductViewModel model);

        List<BgProductViewModel> GetBgProducts();
    }
}
