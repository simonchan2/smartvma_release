﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SmartVMA.Core.Contracts;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Core.Services
{
    public interface IPrnSpoolService
    {
        IEnumerable<PrnSpoolViewModel> GetRemotePrintJobs(string accounts);

        ServiceResponse UpdateRemotePrintJobStatus(int id, string status);

        string GetPdfPath(string account);
    }
}
