﻿using System.Collections.Generic;
using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;

namespace SmartVMA.Core.Repositories
{
    internal interface ICompanySettingRepository : IRepository<CompanySetting, int>
    {
        string GetSettingValue(CompanySettingsNames setting);

        string GetDefaultSettingValue(CompanySettingsNames setting);

        List<CompanySetting> GetAllSettings();
    }
}
