﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.OutputModels;
using System.Collections.Generic;
using SmartVMA.Core.InputModels.PageRequests;

namespace SmartVMA.Core.Repositories
{
    internal interface IOverrideRulesRepository : IRepository<OverrideRule, long>
    {
        ConfigurationSearchByOverridesViewModel GetOverrideRuleById(long? id);
        
        List<ConfigurationOverridesSearchResults> SearchOverrides(ConfigurationSearchByOverridesViewModel model, int pageNumber, int pageSize);
        //List<ConfigurationOverridesSearchResults> SearchOverrides(ConfigurationSearchByOverridesViewModel model,
        //    int pageNumber, int pageSize);
        IEnumerable<OverrideRule> GetOverrideRules();

        List<OverrideRuleConflictsModel> GetConflicts(ConfigurationSearchByOverridesViewModel model, string basicServices, bool areServicesChecked);

        ServiceResponse ApplyOverride(ConfigurationSearchByOverridesViewModel model, string basicServices, bool areServicesChecked, string ruleIdsToDelete);

        ServiceResponse DeleteMany(string ruleIds);
        IEnumerable<OverrideRule> GetPagedResults(PagingRequest request);
        int GetPagedResultsTotal(PagingRequest request);

    }
}
