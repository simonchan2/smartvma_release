﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.PageRequests;
using System.Collections.Generic;

namespace SmartVMA.Core.Repositories
{
    interface IPermissionRepository : IRepository<Permission, int>
    {
        IEnumerable<Permission> GetPermissionsWithRoleInfo();

        bool IsPermissionGranted(string permissionName, int? userId, int? roleId, int? tenantId);
        int GetPagedResultsTotal(PagingRequest request);
        IEnumerable<Permission> GetPagedResults(PagingRequest request);
    }
}
