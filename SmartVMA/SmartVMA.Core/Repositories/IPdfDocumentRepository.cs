﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using System;
using System.Collections.Generic;

namespace SmartVMA.Core.Repositories
{
    internal interface IPdfDocumentRepository : IRepository<PdfDocument, int>
    {
        PdfDocument GetPdfDocument(long appointmentPresentationId, int documentType, int languageId);
        IEnumerable<PdfDocument> GetDocuments(string VIN, string InvoiceNumber, DateTime? startDate, DateTime? endDate);
        void AddPdfDocumentToPrintQueue(int dealerCustomerId, int? userId, string fileName, long appointmentPresentationID, string prnJobTypeCode);

        PdfHeaderViewModel GetPdfHeaderViewModel(int dealerId, long appointmentPresentationId, int? mileage);
        PdfHeaderCustomerInfoViewModel GetPdfHeaderCustomerInfoViewModel(int dealerId, long appointmentPresentationId);
        PdfHeaderCustomerInfoViewModel GetPdfHeaderCustomerInfoViewModelFromVin(string vin);
        PartsCopyPdfViewModel GetPartsCopyPdfViewModel(long appointmentPresentationId);
        TechCopyPdfViewModel GetTechCopyPdfViewModel(long appointmentPresentationId);
        CustomerCopyPdfViewModel GetCustomerCopyPdfViewModel(long appointmentPresentationId);
        PdfFooterViewModel GetPdfFooterViewModel(long? appointmentPresentationId, DateTime? printedDate);
        PdfHeaderAdvisorViewModel GetPdfHeaderAdvisorViewModel(int userId);
        PdfHeaderDealerViewModel GetPdfHeaderDealerViewModel();
    }
}
