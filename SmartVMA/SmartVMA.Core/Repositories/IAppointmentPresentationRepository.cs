﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using System;
using System.Collections.Generic;

namespace SmartVMA.Core.Repositories
{
    interface IAppointmentPresentationRepository : IRepository<AppointmentPresentation, long>
    {
        AppointmentPresentation GetAppointmentPresentationWithVehiclesById(long appointmentPresentationId);
        AppointmentPresentation GetAppointmentPresentationWithServicesById(long appointmentPresentationId);

        IEnumerable<AppointmentPresentation> GetFullAppointmentPresentationList(AppointmentPresentationStatus statusType, DateTime? startDate, DateTime? endDate, DateTime? parkedMenuRetentionDuration, List<long> advisorIds = null, int? timeframeId = null);

        List<AdvisorInfo> GetAdvisorList();

        long? GetAppointmentPresentationId(int year, int make, int model, int engine, string transmission, string driveLine, int mileage);

        AppointmentPresentation GetAppointmentWithServicesAndPartsById(long appointmentPresentationId);
    }
}
