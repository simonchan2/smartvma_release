﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.PageRequests;
using System.Collections.Generic;

namespace SmartVMA.Core.Repositories
{
    internal interface IILifeHistoryRepository : IRepository<ILife_History, int>
    {
        ILife_History GetLastHistory(string contractNumber, string vin);
        List<ILife_History> GetAllHistoryForItem(string contractNumber, string vin);
    }
}

