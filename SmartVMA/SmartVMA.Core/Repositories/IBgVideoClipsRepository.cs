﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;

namespace SmartVMA.Core.Repositories
{
    internal interface IBgVideoClipsRepository : IRepository<BgVideoClip, int>
    {
    }
}
