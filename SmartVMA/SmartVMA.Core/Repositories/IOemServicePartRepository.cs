﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using System.Collections.Generic;


namespace SmartVMA.Core.Repositories
{
    internal interface IOemServicePartRepository : IRepository<OemServicePart, int>
    {
        OemServicePart GetOemServicePartById(int id);
    }
}

