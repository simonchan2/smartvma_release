﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using System.Collections.Generic;

namespace SmartVMA.Core.Repositories
{
    internal interface IVinMasterTransmissionRepository : IRepository<VinMasterTransmission, int>
    {
        string GetVinMasterTransmission(int vinMasterTransmissionId);
    }
}
