﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using System.Collections.Generic;


namespace SmartVMA.Core.Repositories
{
    internal interface IOemComponentRepository : IRepository<OemComponent, int>
    {
        OemComponent GetOemComponentById(int id);

        IEnumerable<OemComponent> GetAllOemComponentsShownAsOption();
    }
}

