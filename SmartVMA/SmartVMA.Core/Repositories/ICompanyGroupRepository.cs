﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.PageRequests;
using System.Collections.Generic;

namespace SmartVMA.Core.Repositories
{
    internal interface ICompanyGroupRepository : IRepository<CompanyGroup, int>
    {
        IEnumerable<CompanyGroup> GetAllDealerGroups();

        void Insert(CompanyGroup companyGroup, IEnumerable<CompanyCompanyGroup> companyCompanyGroups);

        void Update(CompanyGroup companyGroup, IEnumerable<CompanyCompanyGroup> companyCompanyGroups);

        int GetPagedResultsTotal(PagingRequest request);

        IEnumerable<CompanyGroup> GetPagedResults(PagingRequest request);
    }
}
