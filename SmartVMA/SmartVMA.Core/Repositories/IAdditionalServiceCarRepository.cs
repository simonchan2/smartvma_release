﻿using System.Collections.Generic;
using SmartVMA.Core.Entities;
using SmartVMA.Core.Contracts;

namespace SmartVMA.Core.Repositories
{
    internal interface IAdditionalServiceCarRepository : IRepository<AdditionalServicesCar, long>
    {
        bool Save(IEnumerable<int> services, IEnumerable<int> years, IEnumerable<int> makes, IEnumerable<int> models, IEnumerable<int> engines, IEnumerable<int> transmissions, IEnumerable<int> driveLines, int menuLevel);
    }
}
