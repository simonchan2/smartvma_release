﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using System.Collections.Generic;

namespace SmartVMA.Core.Repositories
{
    internal interface IVinMasterRepository : IRepository<VinMaster, int>
    {
        IEnumerable<string> GetAllVehicleSteerings();

        int? GetVinMasterByVinShort(string vinShort);
    }
}
