﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using System.Collections.Generic;

namespace SmartVMA.Core.Repositories
{
    internal interface IBgProdCategoryRepository : IRepository<BgProdCategory, int>
    {
        List<BgProductCategoryInfo> GetAllBgProductCategories();
        List<BgProductCategoryInfo> GetAllBgProductCategories(List<int> bgProducts);
    }
}
