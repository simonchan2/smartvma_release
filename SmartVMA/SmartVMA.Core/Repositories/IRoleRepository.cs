﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.PageRequests;
using System.Collections.Generic;

namespace SmartVMA.Core.Repositories
{
    internal interface IRoleRepository : IRepository<Role, int>
    {
       int GetPagedResultsTotal(PagingRequest request);

        IEnumerable<Role> GetPagedResults(PagingRequest request);
    }
}