﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.InputModels.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Core.Repositories
{
    internal interface IBgProductRepository : IRepository<BgProduct, int>
    {
        BgProductsResultView  GetBgProductProtectionPlanBySKUs(int id);

        IEnumerable<BgProductsResultView> GetPagedResults(PagingRequest request, string categoryIds);
    }
}
