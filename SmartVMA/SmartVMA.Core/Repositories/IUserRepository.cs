﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.PageRequests;
using System.Collections.Generic;

namespace SmartVMA.Core.Repositories
{
    internal interface IUserRepository : IRepository<User, long>
    {
        User GetUserByEmail(string email);
        User GetUserById(long id);
        User GetDefaultUserForCompany(int companyId, UserRoles userRole);
        IEnumerable<User> GetAllUsersByRole(int companyId, UserRoles userRole);
        IEnumerable<User> GetAllUsers(int? companyId = null);
        User GetUserAndSettings(long id);
        void Update(User company, IEnumerable<UserSetting> settings);
        void Insert(User company, IEnumerable<UserSetting> settings);
        int GetPagedResultsTotal(PagingRequest request, int? companyId = null);
        IEnumerable<User> GetPagedResults(PagingRequest request, int? companyId = null);
    }

}
