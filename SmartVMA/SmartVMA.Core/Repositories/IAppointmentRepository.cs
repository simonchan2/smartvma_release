﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using System;
using System.Collections.Generic;

namespace SmartVMA.Core.Repositories
{
    interface IAppointmentRepository : IRepository<Appointment, long>
    {
        List<AppointmentViewModel> GetCalendarAppointments(DateTime startTime, DateTime endTime, string advisorIds, string presentationStatus);
        List<AppointmentViewModel> GetSearchAppointments(string customerName, string customerNr, string customerPhone, string vin, string vehicleStock, string licensePlate);
        List<AppointmentServiceViewModel> GetAppointmentServiceModel(int dealerCustomerID);
        List<AppointmentServiceViewModel> GetAppointmentServiceModel(string vin);
        long CreateAppointmentPresentation(long appointmentID, int? userId, string status);
        List<PdfDocumentViewModel> GetPdfDocuments(DateTime DateTo);
        void DeleteObsoleteAppointments(DateTime DateTo);
        List<VehicleHistoryPlanLevelsViewModel> GetVehicleHistoryPlanLevels();
    }
}
