﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using System.Collections.Generic;

namespace SmartVMA.Core.Repositories
{
    internal interface IArticleArticleCategoryRepository : IRepository<ArticleArticleCategory, int>
    {
        void Insert(Article article, int categoryId);
    }
}
