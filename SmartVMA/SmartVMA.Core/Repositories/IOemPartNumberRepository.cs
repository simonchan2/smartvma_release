﻿using System.Collections.Generic;
using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;

namespace SmartVMA.Core.Repositories
{
    internal interface IOemPartNumberRepository : IRepository<OemPartNumber, int>
    {
        OemPartNumber GetOemPartNumberById(int id);
        List<OemPartNumber> GetOverridenPartNumbersByPartId(int tenantId, int partId);
    }
}
