﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using System;
using System.Collections.Generic;

namespace SmartVMA.Core.Repositories
{
    interface IInspectionRepository : IRepository<Inspection, long>
    {
        InspectionViewModel GetInspectionViewModel(long? id, long? apId);
        InspectionViewModel GetInspectionViewModel(long? id, long? apId, bool hideVehiclePhotos);
        void ResetInspection(long inspectionId);
        ServiceResponse AddSetInspection(Inspection inspection);
        Inspection GetById(long id);

        void SetAppointmentToInspection(long inspectionId, long appointmentPresentationId);
    }
}
