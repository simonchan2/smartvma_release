﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using System.Collections.Generic;

namespace SmartVMA.Core.Repositories
{
    internal interface IEngineOilTypeRepository : IRepository<EngineOilType, int>
    {
        IEnumerable<EngineOilType> GetAllEngineOilTypes();
    }
}

