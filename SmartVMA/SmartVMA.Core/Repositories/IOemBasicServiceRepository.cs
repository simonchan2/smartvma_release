﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.OutputModels;
using System.Collections.Generic;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Core.Repositories
{
    internal interface IOemBasicServiceRepository : IRepository<OemBasicService, int>
    {
        OemBasicService GetOemBasicServiceById(int id);
        IEnumerable<OemBasicService> GetAllOemBasicServicesWithException();
        IEnumerable<OemBasicService> GetServicesForCar(int carId);
        IEnumerable<OemBasicService> GetPagedResults(PagingRequest request);

        IEnumerable<OemBasicService> GetPagedResults(OemServiceMaintenancePagingViewModel request, out int totalCount, out string filteredServiceIds);

        int GetPagedResultsTotal();
        int GetPagedResultsTotal(PagingRequest request);

        ServiceResponse Save(string serviceAsXml, IEnumerable<int> years, IEnumerable<int> makes, IEnumerable<int> models, IEnumerable<int> engines, IEnumerable<int> transmissions, IEnumerable<int> driveLines, int counter);
        
        IEnumerable<string> GetDistinctOpActions();
        ServiceResponse DeleteBulk(string ids);
    }
}
