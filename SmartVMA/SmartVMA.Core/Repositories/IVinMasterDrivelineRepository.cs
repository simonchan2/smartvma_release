﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using System.Collections.Generic;

namespace SmartVMA.Core.Repositories
{
    internal interface IVinMasterDrivelineRepository : IRepository<VinMasterDriveline, int>
    {
        string GetVinMasterDriveline(int vinMasterDrivelineId);
    }
}
