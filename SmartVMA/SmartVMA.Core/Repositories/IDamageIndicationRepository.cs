﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using System;
using System.Collections.Generic;
namespace SmartVMA.Core.Repositories
{
    interface IDamageIndicationRepository : IRepository<DamageIndication, long>
    {
        IEnumerable<DamageIndication> GetDamageIndications(long damageindicationsId);
        ServiceResponse CreateDamageIndications(List<DamageIndication> entities);
        ServiceResponse DeleteDamageIndications(List<DamageIndication> entities);
    }
}