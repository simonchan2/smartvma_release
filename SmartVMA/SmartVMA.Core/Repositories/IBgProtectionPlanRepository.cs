﻿using System.Collections.Generic;
using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;

namespace SmartVMA.Core.Repositories
{
    internal interface IBgProtectionPlanRepository : IRepository<BgProtectionPlan, int>
    {
        IEnumerable<BgProtectionPlan> GetByProducts(List<int> productIds);
    }
}
