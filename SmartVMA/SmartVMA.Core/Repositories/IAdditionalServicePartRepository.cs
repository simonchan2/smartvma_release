﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;

namespace SmartVMA.Core.Repositories
{
    interface IAdditionalServicePartRepository : IRepository<AdditionalServicePart, long>
    {
    }
}