﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.PageRequests;
using System.Collections.Generic;

namespace SmartVMA.Core.Repositories
{
    internal interface IAlertRepository : IRepository<Alert, int>
    {
        IEnumerable<Alert> GetAllActiveAlertsForCurrentUser();
        int GetPagedResultsTotal(PagingRequest request);
        IEnumerable<Alert> GetPagedResults(PagingRequest request);
    }
}

