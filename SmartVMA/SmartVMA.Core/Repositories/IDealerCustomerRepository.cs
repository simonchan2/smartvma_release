﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using System.Collections.Generic;

namespace SmartVMA.Core.Repositories
{
    interface IDealerCustomerRepository : IRepository<DealerCustomer, int>
    {
        CustomerInfoViewModel GetCustomerInfo(long appointmentPresentationId);

        CustomerInfoViewModel GetCustomerInfo(int? dealerCustomerId, int carId, int? transmissionId, int? driveLineId, int? dealerVehicleId);

        CustomerInfoViewModel GetCustomerInfo(int? dealerCustomerId, int? carId, long? appointmentPresentationId, int? transmissionId, int? driveLineId, int? dealerVehicleId);

        List<AdvisorSearchViewModel> CustomerSearch(string customerName, string customerNumber, string customerPhoneNumber, string vin, string vehicleStock, string licensePlate, int parkedMenuRetentionDuration);
        string GetVinByCustomerSearchInfo(string customerName, string customerNumber, string customerPhoneNumber, string vehicleStock, string licensePlate);
    }
}
