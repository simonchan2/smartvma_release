﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.PageRequests;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace SmartVMA.Core.Repositories
{
    internal interface ILanguageRepository : IRepository<Language, int>
    {
        Language GetLanguage(int id);
    }
}
