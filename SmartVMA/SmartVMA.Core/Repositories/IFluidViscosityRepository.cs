﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using System.Collections.Generic;

namespace SmartVMA.Core.Repositories
{
    internal interface IFluidViscosityRepository : IRepository<FluidViscosity, int>
    {
        IEnumerable<FluidViscosity> GetAllFluidViscosityInDealerOilViscosityList();
    }
}

