﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;

namespace SmartVMA.Core.Repositories
{
    internal interface IUserCompanyRoleRepository : IRepository<UserCompanyRole, long>
    {
        void Insert(User user, int roleId, int tenantId);

        bool IsRoleAlreadySet(User user, int roleId, int tenantId);

        UserCompanyRole GetUserRole(User user, int tenantId);

        UserCompanyRole GetRole(long userId, int tenantId);
    }
}
