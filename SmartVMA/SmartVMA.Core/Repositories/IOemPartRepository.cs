﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using System.Collections.Generic;


namespace SmartVMA.Core.Repositories
{
    internal interface IOemPartRepository : IRepository<OemPart, int>
    {
        OemPart GetOemPartById(int id);

        IEnumerable<OemPart> GetAllOemPart();

        IEnumerable<string> GetDistinctViscosity();

        IEnumerable<string> GetDistinctOilTypes();
    }
}

