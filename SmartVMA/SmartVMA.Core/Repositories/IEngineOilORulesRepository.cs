﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.PageRequests;
using System.Collections.Generic;

namespace SmartVMA.Core.Repositories
{
    internal interface IEngineOilORulesRepository : IRepository<EngineOilORule, int>
    {
        IEnumerable<EngineOilORule> GetAllEngineOilORuleForDealer();

        EngineOilORule GetEngineOilORuleById(int id);
        EngineOilORule GetEngineOilORuleByTypeAndViscosity(string oilType, string oilViscosity);
        int GetPagedResultsTotal(PagingRequest request);
        IEnumerable<EngineOilORule> GetPagedResults(PagingRequest request);

        EngineOilORule GetEngineOilOverride(string oilType, string oilViscosity, int? menuLevel = null);
    }
}

