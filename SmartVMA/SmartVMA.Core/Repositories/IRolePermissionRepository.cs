﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using System.Collections.Generic;

namespace SmartVMA.Core.Repositories
{
    internal interface IRolePermissionRepository : IRepository<RolePermission, long>
    {
        IEnumerable<RolePermission> GetRoleWithPermissions(int roleId);
    }
}
