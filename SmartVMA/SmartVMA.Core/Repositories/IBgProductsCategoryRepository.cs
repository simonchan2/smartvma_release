﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Core.Repositories
{
    internal interface IBgProductsCategoryRepository : IRepository<BgProductsCategory, int>
    {
        void Insert(BgProduct bgproduct, int categoryId);

        List<BgProductsCategory> GetBgProductCategoryById(int id);
    }
}
