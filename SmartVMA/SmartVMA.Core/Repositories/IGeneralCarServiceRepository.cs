﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.OutputModels;
using System.Collections.Generic;

namespace SmartVMA.Core.Repositories
{
    internal interface IGeneralCarServiceRepository
    {
        /// <summary>
        ///  Returns all services i.e. 
        ///     Additional services
        ///     OemBasicServices
        ///     BgServices
        /// </summary>
        /// <param name="carId"></param>
        /// <param name="dealerVehicleId"></param>
        /// <param name="mileage"></param>
        /// <param name="isLof"></param>
        /// <param name="serviceIds"></param>
        /// <param name="vehicleTransmission"></param>
        /// <param name="vehicleDriveline"></param>
        /// <returns></returns>
        IEnumerable<ServicesProcedureModel> GetAllServices(int? carId = null, int? dealerVehicleId = null, int? mileage = null, bool? isLof = null, IEnumerable<ServiceIdsRequestModel> serviceIds = null, int? vehicleTransmission = null, int? vehicleDriveline = null, int? dealerCustomerId = null, long? appointmentPresentationId = null);
        IEnumerable<ServiceDetailInvoiceHistoryViewModel> GetHistoryInvoices(List<ServiceDetailViewModel> allServices);

    }
}
