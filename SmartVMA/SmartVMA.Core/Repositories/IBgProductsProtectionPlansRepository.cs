﻿using SmartVMA.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartVMA.Core.Entities;

namespace SmartVMA.Core.Repositories
{
    internal interface IBgProductsProtectionPlansRepository : IRepository<BgProductsProtectionPlan, int>
    {
    }
}
