﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;

namespace SmartVMA.Core.Repositories
{
    internal interface ICompanyCompanyGroupRepository : IRepository<CompanyCompanyGroup, int>
    {
    }
}
