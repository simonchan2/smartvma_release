﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using System;
using System.Collections.Generic;
namespace SmartVMA.Core.Repositories
{
    interface IVehicleRepository : IRepository<Vehicle, int>
    {
        Vehicle GetVehicle(int carId, string vin);
        bool IsExistingVINForOtherCar(int carId, string vin);
        Vehicle GetVehicleForNewCustomer(int carId, string vin);
    }
}