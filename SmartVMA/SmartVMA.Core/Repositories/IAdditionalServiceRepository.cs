﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Core.Repositories
{
    internal interface IAdditionalServiceRepository : IRepository<AdditionalService, long>
    {

        IEnumerable<AdditionalService> GetAllServicesAndParts(CustomServiceMaintenancePagingViewModel request, out int totalCount, out string filteredServiceIds);
        IEnumerable<AdditionalService> GetServicesAndParts(CustomServiceMaintenancePagingViewModel request, out int totalCount, out string filteredServiceIds);
        IEnumerable<AdditionalService> GetServicesAndParts(Expression<Func<AdditionalService, bool>> expression = null);
        int GetItemsCount();
        int GetItemsCount(CustomServiceMaintenancePagingViewModel request);
        IEnumerable<AdditionalService> GetAdditionalService(List<long> services);
        ServiceResponse DeleteBulk(string ids);
    }
}