﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;

namespace SmartVMA.Core.Repositories
{
    internal interface IPrnSpoolRepository : IRepository<PrnSpool, int>
    {
        IEnumerable<PrnSpool> GetRemotePrintJobs(string accounts);

        void UpdateRemotePrintJobStatus(int id, string status);
    }
}
