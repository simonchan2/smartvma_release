﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.PageRequests;
using System.Collections.Generic;

namespace SmartVMA.Core.Repositories
{
    internal interface IILifeRepository : IRepository<ILife, int>
    {
    }
}

