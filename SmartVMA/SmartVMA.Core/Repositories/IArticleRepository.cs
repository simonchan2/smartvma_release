﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.PageRequests;
using System.Collections.Generic;

namespace SmartVMA.Core.Repositories
{
    internal interface IArticleRepository : IRepository<Article, int>
    {
        Article GetArticleById(int id);

        IEnumerable<Article> GetAllArticles();
        IEnumerable<Article> GetArticleByKeyWordAndCategoryId(string key, int[] categoryId);

        int GetPagedResultsTotal(PagingRequest request);
        IEnumerable<Article> GetPagedResults(PagingRequest request);
    }
}
