﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using System;
using System.Collections.Generic;

namespace SmartVMA.Core.Repositories
{
    interface IAppointmentDetailsRepository : IRepository<AppointmentDetail, long>
    {
        IEnumerable<AppointmentDetail> GetAppointmentDetailsList(long appointmentID);
    }
}
