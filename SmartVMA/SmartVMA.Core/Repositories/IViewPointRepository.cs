﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using System;
using System.Collections.Generic;
namespace SmartVMA.Core.Repositories
{
    interface IViewPointRepository : IRepository<ViewPoint, int>
    {
        ViewPoint Get(int Id);
        IEnumerable<ViewPoint> ListAll();
    }
}