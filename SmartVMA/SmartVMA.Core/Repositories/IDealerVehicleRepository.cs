﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;

namespace SmartVMA.Core.Repositories
{
    interface IDealerVehicleRepository : IRepository<DealerVehicle, int>
    {
        int? GetDealerVehicleId(int year, int make, int model, int engine, string transmission, string driveLine);
        DealerVehicle GetDealerVehicle(string vin, int? carId, int? dealerCustomerId, long? appointmentPresentationId);
        int? GetVehicleId(int carId, string vin);
        bool IsExistingVIN(string vin, int? carId);

        int? GetDealerCustomerIdByVIN(string vin);
    }
}
