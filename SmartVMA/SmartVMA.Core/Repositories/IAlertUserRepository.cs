﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;

namespace SmartVMA.Core.Repositories
{
    internal interface IAlertUserRepository : IRepository<AlertsUser, int>
    {
        AlertsUser GetByUserIdAndAlertId(long userId, int alertId);
    }
}
