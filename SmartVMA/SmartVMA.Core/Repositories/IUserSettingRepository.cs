﻿using System.Collections.Generic;
using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;

namespace SmartVMA.Core.Repositories
{
    interface IUserSettingRepository : IRepository<UserSetting, long>
    {
        string GetSettingValue(UserSettingsNames setting);
        List<UserSetting> GetAllSettings();
    }
}
