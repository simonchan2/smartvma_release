﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.PageRequests;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace SmartVMA.Core.Repositories
{
    internal interface ICompanyRepository : IRepository<Company, int>
    {
        Company GetCompanyAndSettings(int id);

        IEnumerable<Company> GetCompanies(Expression<Func<Company, bool>> expression = null);

        IEnumerable<Company> GetParentCompanies(Expression<Func<Company, bool>> expression = null);

        IEnumerable<Company> GetCompaniesForUser(string email);

        Company GetCompanyAndParents(int id);

        void Update(Company company, IEnumerable<CompanySetting> settings);

        void Insert(Company company, IEnumerable<CompanySetting> settings);

        int GetPagedResultsTotal(PagingRequest request, Expression<Func<Company, bool>> expression = null);

        IEnumerable<Company> GetPagedResults(PagingRequest request, Expression<Func<Company, bool>> expression = null);
    }
}
