﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using System.Collections.Generic;
using SmartVMA.Core.OutputModels;

namespace SmartVMA.Core.Repositories
{
    internal interface ICarRepository : IRepository<Car, int>
    {
        List<VehicleYearInfo> GetAllVehicleYears();

        List<VehicleMakeInfo> GetAllVehicleMakes();

        List<VehicleMakeInfo> GetFilteredVehicleMakes(string years);

        List<VehicleModelInfo> GetAllVehicleModels();

        List<VehicleModelInfo> GetFilteredVehicleModels(string years, string makes);

        List<VehicleEngineInfo> GetAllVehicleEngines();

        List<VehicleEngineInfo> GetFilteredVehicleEngines(string years, string makes, string models);

        List<VehicleTransmissionInfo> GetAllVehicleTransmissions(string source);

        List<VehicleTransmissionInfo> GetFilteredVehicleTransmissions(string years, string makes, string models, string engines, string source);

        List<VehicleDriveLineInfo> GetAllVehicleDriveLines(string source);

        List<VehicleDriveLineInfo> GetFilteredVehicleDriveLines(string years, string makes, string models, string engines, string transmissions, string source);

        int? GetCarId(int year, int make, int model, int engine, int transmission, int driveLine);

        IEnumerable<CarProcedureModel> GetCarId(IEnumerable<int> years, IEnumerable<int> makes, IEnumerable<int> models, IEnumerable<int> engines, IEnumerable<int> transmissions, IEnumerable<int> driveLines);

        Car GetById(int id);

        IEnumerable<int> GetAllDistinctMileages(int measurementUnitId);
        IEnumerable<int> GetAllDistinctMileages(int carId, int measurmentUnitId);

        VehicleFilterItemsViewModel[] GetVehicleFilter(VehicleFilterViewModel filter);

        bool VinExists(string vin);
    }
}
