﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using System;

namespace SmartVMA.Core.Repositories
{
    internal interface IPasswordResetRequestRepository : IRepository<PasswordResetRequest, long>
    {
        PasswordResetRequest GetRequestByToken(Guid token);
    }
}
