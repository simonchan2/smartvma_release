﻿using System.Configuration;

namespace SmartVMA.Core
{
    public class StaticSettings
    {
        static StaticSettings()
        {
            PasswordExpirationTimeInHours = int.Parse(ConfigurationManager.AppSettings["PasswordExpirationTimeInHours"]);
            MaximumAllowedLoginAttempts = int.Parse(ConfigurationManager.AppSettings["MaximumAllowedLoginAttempts"]);
            ExpirationLinkDuration = int.Parse(ConfigurationManager.AppSettings["ExpirationLinkDuration"]);
            WarningLoginNumberAttempts = int.Parse(ConfigurationManager.AppSettings["WarningLoginNumberAttempts"]);
            BaseCompanyId = int.Parse(ConfigurationManager.AppSettings["BaseCompanyId"]);
            DefaultApplicationName = ConfigurationManager.AppSettings["DefaultApplicationName"];
            LifeTimeProtectionPlan = ConfigurationManager.AppSettings["LifeTimeProtectionPlan"];
            ForeverDiesel = ConfigurationManager.AppSettings["ForeverDiesel"];
            EngineAssurance = ConfigurationManager.AppSettings["EngineAssurance"];
            Delimiter = ConfigurationManager.AppSettings["Delimiter"];
            PageSizeAdmin = int.Parse(ConfigurationManager.AppSettings["PageSizeAdmin"]);
            DatatableRowId = ConfigurationManager.AppSettings["DatatableRowId"];
            ContentUrl = ConfigurationManager.AppSettings["ContentUrl"];
            SessionPrefixCompanySetting = ConfigurationManager.AppSettings["SessionPrefixCompanySetting"];
            SessionPrefixUserSettings = ConfigurationManager.AppSettings["SessionPrefixUserSettings"];
        }

        public static int PasswordExpirationTimeInHours { get; set; }

        public static int MaximumAllowedLoginAttempts { get; set; }

        public static int ExpirationLinkDuration { get; set; }

        public static int WarningLoginNumberAttempts { get; set; }

        public static int BaseCompanyId { get; set; }

        public static string DefaultApplicationName { get; set; }

        public static string LifeTimeProtectionPlan { get; set; }

        public static string ForeverDiesel { get; set; }

        public static string EngineAssurance { get; set; }

        public static string Delimiter { get; set; }
        public static int PageSizeAdmin { get; set; }
        public static string DatatableRowId { get; set; }
        public static string ContentUrl { get; set; }
        public static string SessionPrefixCompanySetting { get; set; }
        public static string SessionPrefixUserSettings { get; set; }
    }
}
