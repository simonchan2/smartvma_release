﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Infrastructure.Parser
{
    public static class Parser
    {
        public static int ToInt(string value, int defaultValue = 0)
        {
            int val;
            if (value == null || !int.TryParse(value, out val))
            {
                return defaultValue;
            }
            return val;
        }

        public static bool ToBool(string value, bool defaultValue = false)
        {
            bool val;
            if (value == null || !bool.TryParse(value, out val))
            {
                return defaultValue;
            }
            return val;
        }

        public static double ToDouble(string value, double defaultValue = 0)
        {
            double val;
            if (value == null || !double.TryParse(value, out val))
            {
                return defaultValue;
            }
            return val;
        }

        public static decimal ToDecimal(string value, decimal defaultValue = 0)
        {
            decimal val;
            if (value == null || !decimal.TryParse(value, out val))
            {
                return defaultValue;
            }
            return val;
        }

        public static string ToBase64(byte[] value, string dataType)
        {
            string base64 = string.Empty;
            if (value != null)
            {
                base64 = $"data:{dataType};base64,{Convert.ToBase64String(value)}";
            }
            return base64;
        }
    }
}
