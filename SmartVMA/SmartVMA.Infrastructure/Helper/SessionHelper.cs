﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SmartVMA.Infrastructure.Helper
{
    public static class SessionHelper
    {
        public static string GetValue(string name)
        {
            string value = null;
            if (HttpContext.Current.Session[name] != null)
            {
                value = HttpContext.Current.Session[name].ToString();
            }
            return value;
        }
    }
}
