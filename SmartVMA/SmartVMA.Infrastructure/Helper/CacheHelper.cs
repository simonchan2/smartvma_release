﻿using Newtonsoft.Json;

namespace SmartVMA.Infrastructure.Helper
{
    public class CacheHelper
    {
        public static string BuildCacheKey<T>(string keyPrefix, T obj)
        {
            string separator = "_";
            var objectHashCode = JsonConvert.SerializeObject(obj).GetHashCode();
            string key = keyPrefix + separator + objectHashCode;
            return key;
        }
    }
}
