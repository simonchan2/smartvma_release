﻿using System;

namespace SmartVMA.Infrastructure.Extensions
{
    public static class DateTimeExtension
    {
        public static DateTime FirstDayInMonth(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, 1);
        }

        public static DateTime LastDayInMonth(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, 1).AddMonths(1).Subtract(TimeSpan.FromDays(1));
        }

        public static DateTime BeginingOfDay(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 0, 0, 0);
        }

        public static DateTime EndOfDay(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 23, 59, 59);
        }

        public static DateTime FirstDayInLastWeek(this DateTime date)
        {
            return date.AddDays(-(int)date.DayOfWeek - 6);
        }

        public static DateTime LastDayInLastWeek(this DateTime date)
        {
            return date.AddDays(-(int)date.DayOfWeek);
        }
    }
}
