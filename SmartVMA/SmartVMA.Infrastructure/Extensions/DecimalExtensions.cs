﻿using System;

namespace SmartVMA.Infrastructure.Extensions
{
    public static class DecimalExtensions
    {
        public static string Truncate(this decimal? number)
        {
            if (number.HasValue)
            {
                return Math.Truncate(number.Value).ToString();
            }
            return "";
        }
    }
}
