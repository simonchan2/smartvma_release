﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SmartVMA.Infrastructure.Extensions
{
    public static class DictionaryExtension
    {
        public static IDictionary<string, object> ToDictionary(this object data)
        {
            if (data == null)
            {
                return null;
            }

            var publicAttributes = BindingFlags.Public | BindingFlags.Instance;
            var dictionary = new Dictionary<string, object>();

            foreach (PropertyInfo property in data.GetType().GetProperties(publicAttributes).Where(p => p.GetIndexParameters().Length == 0))
            {
                if (property.CanRead)
                {
                    dictionary.Add(property.Name.Replace("_", "-"), property.GetValue(data, null));
                }
            }
            return dictionary;
        }
    }
}
