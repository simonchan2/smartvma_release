﻿using System;

namespace SmartVMA.Infrastructure.Extensions
{
    public static class DoubleExtensions
    {
        public static string Truncate(this double? number)
        {
            if (number.HasValue)
            {
                return Math.Truncate(number.Value).ToString();
            }
            return "";
        }
    }
}
