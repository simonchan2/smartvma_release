﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace SmartVMA.Infrastructure.Extensions
{
    public static class ListExtension
    {
        public static void AddIfNotNull<TValue>(this IList<TValue> list, TValue value)
        {
            if ((object)value != null)
                list.Add(value);
        }
    }
}
