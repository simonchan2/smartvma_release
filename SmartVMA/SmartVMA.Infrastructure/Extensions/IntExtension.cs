﻿namespace SmartVMA.Infrastructure.Extensions
{
    public static class IntExtension
    {
        public static int? TryParseInt(this string value)
        {
            int parsedValue;
            return int.TryParse(value, out parsedValue) ? (int?)parsedValue : null;
        }
    }
}
