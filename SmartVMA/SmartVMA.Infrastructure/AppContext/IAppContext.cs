﻿namespace SmartVMA.Infrastructure.AppContext
{
    public interface IAppContext
    {
        UserIdentity CurrentIdentity { get; }

        string CurrentUserName { get; }

        string CurrentUserEmail { get; }

        string CurrentUserRoleName { get; }

        int CurrentUserRoleId { get; }

        string TempFolderName { get; }

        bool IsUserAuthenticated { get; }

        string MapPath(string path);

        string MapUrl(string url, int? tenantId = null);

        string ImagesFolder { get; }

        string CompanyContentFolder { get; }

        string VehiclePhotosFolder { get; }

        string DocumentsFolder { get; }

        string ReportingServiceUri { get; }

        string ReportingServiceUserName { get; }

        string ReportingServicePwd { get; }

        string ReportingServiceDomain { get; }

        string UserTitle { get; }

        string LOFImagesFolder { get; } 

    }
}
