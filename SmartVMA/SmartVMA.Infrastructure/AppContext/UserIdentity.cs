﻿namespace SmartVMA.Infrastructure.AppContext
{
    public class UserIdentity
    {
        public int? UserId { get; set; }
        public int? RoleId { get; set; }
        public int? TenantId { get; set; }
    }
}
