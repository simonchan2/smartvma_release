﻿namespace SmartVMA.Infrastructure
{
    public class RegisteredTypes
    {
        public const string LocalFileManager = "local.filemanager";
        public const string AzureFileManager = "azure.filemanager";

        public const string XmlSerializationManager = "xml.serializationmanager";
    }
}
