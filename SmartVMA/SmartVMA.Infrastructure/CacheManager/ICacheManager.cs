﻿using System;
using System.Collections.Generic;

namespace SmartVMA.Infrastructure.CacheManager
{
    public interface ICacheManager
    {
        T Get<T>(string key) where T : class;
        void Remove(string key);
        void RemoveRange(IEnumerable<string> keys);
        void Set<T>(string key, T data, long cacheTimeInSeconds = 3600);

        //bool IsSet(string key);

        /// <summary>
        /// Clear all cached items.
        /// </summary>
        void Clear();

        T GetOrSet<T>(string key, Func<T> getItemCallback, long cacheTimeInSeconds = 3600) where T : class;

    }
}
