﻿using System;
using System.Collections.Generic;
using System.Runtime.Caching;
using SmartVMA.Infrastructure.Ioc;

namespace SmartVMA.Infrastructure.CacheManager
{
    [IocBindable(ScopeType = InstanceScopeType.Singleton)]
    public class InMemoryCache : ICacheManager
    {
        public T GetOrSet<T>(string key, Func<T> getItemCallback, long cacheTimeInSeconds = 3600) where T : class
        {
            T item = Get<T>(key);
            if (item == null)
            {
                item = getItemCallback();
                Set(key, item, cacheTimeInSeconds);
            }
            return item;
        }
        public T Get<T>(string key) where T : class
        {
            if (!string.IsNullOrEmpty(key))
            {
                var item = MemoryCache.Default.Get(key) as T;
                return item;
            }
            return null;
        }
        public void Remove(string key)
        {
            if (!string.IsNullOrEmpty(key))
            {
                MemoryCache.Default.Remove(key);
            }
        }
        public void Set<T>(string key, T data, long cacheTimeInSeconds = 3600)
        {
            if (!string.IsNullOrEmpty(key))
            {
                if (data != null)
                {
                    MemoryCache.Default.Add(key, data, DateTime.Now.AddSeconds(cacheTimeInSeconds));
                }
                else
                {
                    MemoryCache.Default.Add(key, string.Empty, DateTime.Now.AddSeconds(cacheTimeInSeconds));
                }
            }
        }

        public void Clear()
        {
            MemoryCache.Default.Dispose();
        }

        public void RemoveRange(IEnumerable<string> keys)
        {
            foreach (var item in keys)
            {
                Remove(item);
            }
        }
    }
}
