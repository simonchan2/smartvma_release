﻿using System;

namespace SmartVMA.Infrastructure.Ioc
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public class IocBindableAttribute : Attribute
    {
        public InstanceScopeType ScopeType { get; set; }
        public string RegistrationName { get; set; }
        public bool RegisterAsSelf { get; set; }
    }
}