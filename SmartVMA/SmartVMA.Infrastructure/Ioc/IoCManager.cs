﻿using Autofac;
using Autofac.Builder;
using Autofac.Configuration;
using Autofac.Integration.Mvc;
using SmartVMA.Infrastructure.Ioc.Configurations;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;

namespace SmartVMA.Infrastructure.Ioc
{
    public class IocManager
    {
        private static Lazy<IContainer> _container = new Lazy<IContainer>(RegisterTypes);

        public static IContainer Container
        {
            get
            {
                return _container.Value;
            }
        }

        public static T Resolve<T>()
        {
            var currentResolver = AutofacDependencyResolver.Current;
            if (currentResolver != null)
            {
                return currentResolver.RequestLifetimeScope.Resolve<T>();
            }
            return Container.Resolve<T>();
        }

        public static T Resolve<T>(string name)
        {
            var currentResolver = AutofacDependencyResolver.Current;
            if (currentResolver != null)
            {
                return currentResolver.RequestLifetimeScope.ResolveNamed<T>(name);
            }
            return Container.ResolveNamed<T>(name);
        }

        public static T Resolve<T>(Dictionary<string, string> param)
        {
            var currentResolver = AutofacDependencyResolver.Current;
            if (currentResolver != null)
            {
                return currentResolver.RequestLifetimeScope.Resolve<T>();
            }
            if (!param.Any())
                return Container.Resolve<T>();

            return Container.Resolve<T>(param.Select(t => new NamedParameter(t.Key, t.Value)));
        }

        public static IContainer RegisterTypes()
        {
            var builder = new ContainerBuilder();
            var types = GetTypes();

            foreach (var type in types)
            {
                var attributes = type.GetCustomAttributes<IocBindableAttribute>();
                foreach (var attribute in attributes)
                {
                    var serviceType = type.GetInterfaces().FirstOrDefault(x => x.IsGenericType == type.IsGenericType) ?? type.BaseType ?? type;
                    if (type.IsGenericType)
                    {
                        var registration = attribute.RegisterAsSelf
                            ? builder.RegisterGeneric(type).AsSelf()
                            : string.IsNullOrEmpty(attribute.RegistrationName)
                                ? builder.RegisterGeneric(type).As(serviceType)
                                : builder.RegisterGeneric(type).Named(attribute.RegistrationName, serviceType);
                        SetScopeType(registration, attribute);
                    }
                    else
                    {
                        var registration = attribute.RegisterAsSelf
                        ? builder.RegisterType(type).AsSelf()
                        : string.IsNullOrEmpty(attribute.RegistrationName)
                            ? builder.RegisterType(type).As(serviceType)
                            : builder.RegisterType(type).Named(attribute.RegistrationName, serviceType);
                        SetScopeType(registration, attribute);
                    }
                }
            }

            // override some registration from config
            builder.RegisterModule(new ConfigurationSettingsReader("autofac"));
            return builder.Build();
        }

        private static IEnumerable<Type> GetTypes()
        {
            var executionDirectory = Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory);
            var registeredAssemblies = ConfigurationManager.GetSection(RegisteredAssembliesSection.SectionName) as RegisteredAssembliesSection;
            var types = registeredAssemblies.Assemblies
                .SelectMany(reg => Assembly.LoadFrom(Path.Combine(executionDirectory, reg.FilePath)).GetTypes())
                .Where(t => t.IsClass && !t.IsAbstract);

            return types;
        }

        private static void SetScopeType<T>(IRegistrationBuilder<object, ReflectionActivatorData, T> registraion, IocBindableAttribute attribute)
        {
            if (attribute.ScopeType == InstanceScopeType.PerRequest)
            {
                registraion.InstancePerRequest();
            }
            else if (attribute.ScopeType == InstanceScopeType.Singleton)
            {
                registraion.SingleInstance();
            }
            else
            {
                registraion.InstancePerDependency();
            }
        }
    }
}