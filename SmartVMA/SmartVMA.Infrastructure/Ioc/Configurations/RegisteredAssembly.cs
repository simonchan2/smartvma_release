﻿using System.Configuration;

namespace SmartVMA.Infrastructure.Ioc.Configurations
{
    public class RegisteredAssembly : ConfigurationElement
    {
        [ConfigurationProperty("fullName", IsKey=true, IsRequired = true)]
        public string FullName
        {
            get { return (string)this["fullName"]; }
            set { this["fullName"] = value; }
        }


        [ConfigurationProperty("filePath", IsRequired = true)]
        public string FilePath
        {
            get { return (string)this["filePath"]; }
            set { this["filePath"] = value; }
        }
    }
}
