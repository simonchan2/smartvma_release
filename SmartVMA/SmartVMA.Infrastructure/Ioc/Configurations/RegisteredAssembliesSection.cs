﻿using System.Configuration;

namespace SmartVMA.Infrastructure.Ioc.Configurations
{
    public class RegisteredAssembliesSection : ConfigurationSection
    {
        public const string SectionName = "registeredAssemblies";

        [ConfigurationProperty("assemblies", IsDefaultCollection = true, IsRequired = true)]
        [ConfigurationCollection(typeof(RegisteredAssembly), AddItemName = "registeredAssembly")]
        public RegisteredAssemblyCollection Assemblies
        {
            get
            {
                return base["assemblies"] as RegisteredAssemblyCollection;
            }
        }
    }
}