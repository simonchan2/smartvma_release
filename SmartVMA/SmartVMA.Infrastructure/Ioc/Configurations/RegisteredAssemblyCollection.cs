﻿using System.Collections.Generic;
using System.Configuration;

namespace SmartVMA.Infrastructure.Ioc.Configurations
{
    public class RegisteredAssemblyCollection : ConfigurationElementCollection, IEnumerable<RegisteredAssembly>
    {
        private List<RegisteredAssembly> _elements = new List<RegisteredAssembly>();
        protected override ConfigurationElement CreateNewElement()
        {
            RegisteredAssembly newElement = new RegisteredAssembly();
            _elements.Add(newElement);
            return newElement;
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
           var configService = (RegisteredAssembly)element;
            return configService.FullName;
        }

        public new IEnumerator<RegisteredAssembly> GetEnumerator()
        {
            return _elements.GetEnumerator();
        }
    }
}
