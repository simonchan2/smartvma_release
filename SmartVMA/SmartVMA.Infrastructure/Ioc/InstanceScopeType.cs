﻿namespace SmartVMA.Infrastructure.Ioc
{
    public enum InstanceScopeType
    {
        Singleton = 1,
        PerRequest = 2,
        PerDependency = 3
    }
}
