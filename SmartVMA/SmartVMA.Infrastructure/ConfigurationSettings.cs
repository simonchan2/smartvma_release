﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Infrastructure
{
    public class ConfigurationSettings
    {
        static ConfigurationSettings()
        {
            CacheKeyVehicleFilter = ConfigurationManager.AppSettings["CacheKeyVehicleFilter"];
            CacheKeyVehicleYears = ConfigurationManager.AppSettings["CacheKeyVehicleFilter"];
            CacheDurationDefault = Convert.ToInt32(ConfigurationManager.AppSettings["CacheDurationDefault"]);
            CacheDurationShort = Convert.ToInt32(ConfigurationManager.AppSettings["CacheDurationShort"]);
            CacheDurationMedium = Convert.ToInt32(ConfigurationManager.AppSettings["CacheDurationMedium"]);
            CacheDurationLong = Convert.ToInt32(ConfigurationManager.AppSettings["CacheDurationLong"]);
        }
        public static string CacheKeyVehicleFilter { get; set; }
        public static string CacheKeyVehicleYears { get; set; }
        public static int CacheDurationDefault { get; set; }
        public static int CacheDurationShort { get; set; }
        public static int CacheDurationMedium { get; set; }
        public static int CacheDurationLong { get; set; }
    }
}
