﻿namespace SmartVMA.Infrastructure.SerializationManager
{
    public interface ISerializationManager
    {
        string Serialize<T>(T entity);
    }
}
