﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;
using SmartVMA.Infrastructure.Ioc;

namespace SmartVMA.Infrastructure.SerializationManager
{
    [IocBindable(RegistrationName = RegisteredTypes.XmlSerializationManager)]
    internal class XmlSerializationManager : ISerializationManager
    {
        public string Serialize<T>(T entity)
        {
            var xsSubmit = new XmlSerializer(typeof(T));
            var sww = new StringWriter();
            using (var writer = XmlWriter.Create(sww))
            {
                xsSubmit.Serialize(writer, entity);
                return sww.ToString(); // Your XML
            }
        }
    }
}
