﻿using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System;
using System.IO;
using Microsoft.WindowsAzure.Storage; // Namespace for Storage Client Library
using Microsoft.WindowsAzure.Storage.File; // Namespace for File storage
using System.Configuration;
using System.Collections.Generic;
using System.Linq;

namespace SmartVMA.Infrastructure.FileManager
{
    [IocBindable(ScopeType = InstanceScopeType.Singleton)]
    [IocBindable(RegistrationName = RegisteredTypes.AzureFileManager, ScopeType = InstanceScopeType.Singleton)]
    internal class AzureFileManager : IFileManager
    {
        private CloudFileShare _share;
        private static readonly string StorageShareName ;
        static AzureFileManager()
        {
            StorageShareName = ConfigurationManager.AppSettings["AzureStorageName"];
        }

        public bool Exists(string src)
        {
            var file = GetFile(src);
            return file.Exists();
        }

        public string MapPath(string src)
        {
            return string.IsNullOrEmpty(src) ? src : src.TrimStart('/', '\\').Replace(@"\","/");
        }

        public bool Copy(string src, string dest)
        {
            try
            {
                var srcFile = GetFile(src);
                var destFile = GetFile(dest);
                destFile.StartCopy(srcFile);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Move(string src, string dest)
        {
            try
            {
                var srcFile = GetFile(src);
                var destFile = GetFile(dest);

                destFile.StartCopy(srcFile);
                Delete(src);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Stream Read(string src)
        {
            try
            {
                var result = new MemoryStream();
                var srcPath = MapPath(src);
                var file = GetFile(srcPath);

                file.DownloadToStream(result);

                result.Position = 0;
                return result;
            }
            catch (Exception ex)
            {
                // ignored
            }
            return null;
        }



        public bool Save(string src, Stream data, bool overwriteExistingFile = false)
        {
            try
            {
                var srcPath = MapPath(src);
                data.Position = 0;
                var file = GetFile(srcPath);
                file.UploadFromStream(data);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void Save(string src, byte[] byteArray, bool overwriteExistingFile = false)
        {
            using (var stream = new MemoryStream(byteArray))
            {
                Save(src, stream);
            }
        }

        public bool Delete(string src)
        {
            try
            {
                var srcPath = MapPath(src);
                var file = GetFile(srcPath);
                file.Delete();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void DeleteEmptyFolders(string rootPath)
        {
            var destPath = MapPath(rootPath);

            foreach (var directory in Directory.GetDirectories(destPath))
            {
                DeleteEmptyFolders(directory);
                if (Directory.GetFiles(directory).Length == 0 &&
                    Directory.GetDirectories(directory).Length == 0)
                {
                    Directory.Delete(directory, false);
                }
            }
        }

        public bool CreateDirectory(string path)
        {
            var share = GetShareInstance();
            if (!share.Exists())
            {
                return false;
            }

            var srcPath = MapPath(path);
            var directoryName = Path.GetDirectoryName(srcPath);
            if (string.IsNullOrEmpty(directoryName))
            {
                return false;
            }

            var directory = share.GetRootDirectoryReference();
            foreach (var dir in directoryName.Split('\\'))
            {
                directory = directory.GetDirectoryReference(dir);
                directory.CreateIfNotExists();
            }
            return true;
        }

        public bool Append(string src, int chunkId, Stream data)
        {
            var srcPath = MapPath(src);
            using (var existingStream = Read(src))
            {
                if (existingStream != null)
                {
                    data.Position = 0;
                    existingStream.Position = existingStream.Length;
                    data.CopyTo(existingStream);
                    return Save(src, existingStream);
                }
            }
            return Save(srcPath, data);
        }

        public List<string> GetAllFilesFromFolder()
        {
            var files = new List<string>();
            var appContext = IocManager.Resolve<IAppContext>();
            //DirectoryInfo dirInfo = new DirectoryInfo(System.AppDomain.CurrentDomain.BaseDirectory + $@"{appContext.LOFImagesFolder}");
            //FileInfo[] LOFFiles = dirInfo.GetFiles();
            //foreach (FileInfo file in LOFFiles)
            //{
            //    //files.Add(appContext.MapUrl($@"{appContext.LOFImagesFolder}/{file.Name}"));
            //    files.Add(file.Name);
            //}

            var share = GetShareInstance();
            if (!share.Exists())
            {
                return null;
            }

            var srcPath = MapPath($@"{appContext.LOFImagesFolder}");
            var directoryName = Path.GetDirectoryName(srcPath);
            var directory = share.GetRootDirectoryReference();

            if (string.IsNullOrEmpty(directoryName))
            {
                return null;
            }

            foreach (var dir in directoryName.Split('\\'))
            {
                directory = directory.GetDirectoryReference(dir);
                directory.CreateIfNotExists();
            }

            foreach (var item in directory.ListFilesAndDirectories())
            {
                var path = item.Uri.LocalPath.Split('/');
                files.Add(path[path.Length-1]);
            }

            return files;
        }


        private CloudFileShare GetShareInstance()
        {
            if (_share != null)
            {
                return _share;
            }

            var storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"]);
            var fileClient = storageAccount.CreateCloudFileClient();
            _share = fileClient.GetShareReference(StorageShareName);
            return _share;
        }

        private CloudFile GetFile(string filePath)
        {
            var share = GetShareInstance();
            if (!share.Exists())
            {
                return null;
            }

            var srcPath = MapPath(filePath);
            var directoryName = Path.GetDirectoryName(srcPath);
            var fileName = Path.GetFileName(srcPath);
            var directory = share.GetRootDirectoryReference();

            if (string.IsNullOrEmpty(directoryName))
            {
                return null;
            }

            foreach (var dir in directoryName.Split('\\'))
            {
                directory = directory.GetDirectoryReference(dir);
                directory.CreateIfNotExists();
            }

            var file = directory.GetFileReference(fileName);
            return file;
        }

        public byte[] ReadStreamToByte(Stream input)
        {
            if (input != null)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    byte[] buffer = new byte[16 * 1024];
                    int read;
                    while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        ms.Write(buffer, 0, read);
                    }
                    return ms.ToArray();
                }
            }
            return null;
        }
    }
}