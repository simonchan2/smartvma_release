﻿using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Extensions;
using SmartVMA.Infrastructure.Ioc;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SmartVMA.Infrastructure.FileManager
{
    [IocBindable(ScopeType = InstanceScopeType.Singleton)]
    [IocBindable(RegistrationName = RegisteredTypes.LocalFileManager, ScopeType = InstanceScopeType.Singleton)]
    internal class LocalFileManager : IFileManager
    {
        public bool Exists(string src)
        {
            return File.Exists(src);
        }

        public string MapPath(string src)
        {
            if (string.IsNullOrEmpty(src) || Path.IsPathRooted(src))
            {
                return src;
            }

            var appContext = IocManager.Resolve<IAppContext>();
            var mappedPath = appContext.MapPath(src);
            return mappedPath;
        }

        public bool Copy(string src, string dest)
        {
            var srcPath = MapPath(src);
            var destPath = MapPath(dest);

            if (!Exists(srcPath) || Exists(destPath))
            {
                return false;
            }

            CreateDirectory(destPath);
            File.Copy(srcPath, destPath);
            return true;
        }

        public bool Move(string src, string dest)
        {
            var srcPath = MapPath(src);
            var destPath = MapPath(dest);

            if (!Exists(srcPath) || Exists(destPath))
            {
                return false;
            }

            CreateDirectory(destPath);
            File.Move(srcPath, destPath);
            return true;
        }

        public Stream Read(string src)
        {
            var srcPath = MapPath(src);
            if (!Exists(srcPath))
            {
                return new MemoryStream();
            }

            var stream = File.OpenRead(srcPath);
            stream.Position = 0;

            return stream;
        }

        public bool Save(string src, Stream data, bool overwriteExistingFile = false)
        {
            var srcPath = MapPath(src);
            if (Exists(srcPath) && overwriteExistingFile == false)
            {
                return false;
            }

            try
            {
                CreateDirectory(srcPath);
                using (var fileStream = File.Create(srcPath))
                {
                    data.Seek(0, SeekOrigin.Begin);
                    data.CopyTo(fileStream);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void Save(string src, byte[] byteArray, bool overwriteExistingFile = false)
        {
            using (var stream = new MemoryStream(byteArray))
            {
                var srcPath = MapPath(src);
                Save(srcPath, stream, overwriteExistingFile);
            }
        }

        public bool Delete(string src)
        {
            var srcPath = MapPath(src);
            if (!Exists(srcPath))
            {
                return false;
            }

            File.Delete(srcPath);
            return true;
        }

        public void DeleteEmptyFolders(string rootPath)
        {
            var destPath = MapPath(rootPath);

            foreach (var directory in Directory.GetDirectories(destPath))
            {
                DeleteEmptyFolders(directory);
                if (Directory.GetFiles(directory).Length == 0 &&
                    Directory.GetDirectories(directory).Length == 0)
                {
                    Directory.Delete(directory, false);
                }
            }
        }

        public bool CreateDirectory(string path)
        {
            var destPath = MapPath(path);
            var directory = IsDirectory(path) ? destPath : Path.GetDirectoryName(destPath);

            if (string.IsNullOrEmpty(directory) || Directory.Exists(directory))
            {
                return false;
            }
            Directory.CreateDirectory(directory);
            return true;
        }

        internal bool IsDirectory(string path)
        {
            var fileInfo = new FileInfo(path);
            return (int)fileInfo.Attributes != -1
                ? fileInfo.Attributes.HasFlag(FileAttributes.Directory)
                : string.IsNullOrWhiteSpace(Path.GetExtension(path));
        }

        public bool Append(string src, int chunkId, Stream data)
        {
            var srcPath = MapPath(src);
            if (!Exists(srcPath))
            {
                return Save(srcPath, data);
            }

            using (var fileStream = new FileStream(srcPath, FileMode.Append, FileAccess.Write))
            {
                var bytes = data.ToByteArray();
                fileStream.Write(bytes, 0, bytes.Length);
            }
            return true;
        }

        public List<string> GetAllFilesFromFolder()
        {
            var appContext = IocManager.Resolve<IAppContext>();
            var dirInfo = new DirectoryInfo(appContext.MapPath(appContext.LOFImagesFolder));
            if (!dirInfo.Exists)
            {
                return new List<string>();
            }

            var files = dirInfo.GetFiles();
            var result = files.Select(x => x.Name).ToList();
            return result;
        }

        public byte[] ReadStreamToByte(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}