﻿using System.Collections.Generic;
using System.IO;

namespace SmartVMA.Infrastructure.FileManager
{
    public interface IFileManager
    {
        bool Exists(string src);
        string MapPath(string src);

        bool Copy(string src, string dest);
        bool Move(string src, string dest);
        bool Delete(string src);
        void DeleteEmptyFolders(string rootPath);

        Stream Read(string src);
        bool Save(string src, Stream data, bool overwriteExistingFile = false);
        void Save(string src, byte[] byteArray, bool overwriteExistingFile = false);
        bool Append(string src, int chunkId, Stream data);

        bool CreateDirectory(string path);

        List<string> GetAllFilesFromFolder();

        byte[] ReadStreamToByte(Stream input);
    }
}
