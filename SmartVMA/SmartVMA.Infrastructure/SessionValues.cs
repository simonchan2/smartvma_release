﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace SmartVMA.Infrastructure
{
    public class SessionValues
    {
        public const string Model = "Model";

        public const string Years = "Years";
        public const string Makes = "Makes";
        public const string Models = "Models";
        public const string EngineTypes = "EngineTypes";
        public const string TransmissionTypes = "TransmissionTypes";
        public const string DriveLines = "DriveLines";
        public const string Steerings = "Steerings";
        public const string SelectedServices = "selectedServicesArray";
        public const string RecordsFiltered = "recordsFiltered";
        public const string RecordsTotal = "recordsTotal";
        public const string AllFilteredServices = "allFilteredServices";

    }
}
