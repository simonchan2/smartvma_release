﻿using SmartVMA.Infrastructure.Ioc;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Mail;

namespace SmartVMA.Infrastructure.Mailer
{
    [IocBindable(ScopeType = InstanceScopeType.Singleton)]
    internal class SimpleSmtpMailManager : IMailManager
    {
        private readonly SmtpClient _smtpServer;
        private readonly string _fromAddress;
        private readonly string _fromAddressDisplayName;

        public SimpleSmtpMailManager()
        {
            _fromAddress = ConfigurationManager.AppSettings["SmtpEmailAddress"];
            _fromAddressDisplayName = ConfigurationManager.AppSettings["SmtpEmailAddressContactName"];

            _smtpServer = new SmtpClient(ConfigurationManager.AppSettings["SmtpServerUrl"]);
            _smtpServer.UseDefaultCredentials = false;
            _smtpServer.Port = int.Parse(ConfigurationManager.AppSettings["SmtpServerPort"]);
            if (!bool.Parse(ConfigurationManager.AppSettings["SmtpAllowAnonymous"]))
            {
                _smtpServer.Credentials = new System.Net.NetworkCredential(_fromAddress,
                    ConfigurationManager.AppSettings["SmtpEmailAddressPassword"]);
            }
            _smtpServer.EnableSsl = bool.Parse(ConfigurationManager.AppSettings["SmtpEnableSsl"]);
        }

        public bool Send(string toAddress, string subject, string messageBody)
        {
            return Send(toAddress, subject, messageBody, null);
        }

        public bool Send(string toAddress, string subject, string messageBody, List<Attachment> attachments)
        {
            var mail = new MailMessage
            {
                From = new MailAddress(_fromAddress, _fromAddressDisplayName),
                Body = messageBody,
                Subject = subject,
                IsBodyHtml = true
            };

            if (attachments != null && attachments.Count > 0)
            {
                foreach (var attachment in attachments)
                {
                    mail.Attachments.Add(attachment);
                }
            }

            mail.To.Add(new MailAddress(toAddress));
            _smtpServer.Send(mail);
            return true;
        }

        public bool Send(string replyto, string toAddress, string subject, string messageBody, List<Attachment> attachments)
        {
            var mail = new MailMessage
            {
                From = new MailAddress(_fromAddress, _fromAddressDisplayName),
                //new MailAddress(fromAddress),
                ReplyToList = {replyto},
                Body = messageBody,
                Subject = subject,
                IsBodyHtml = true
            };

            if (attachments != null && attachments.Count > 0)
            {
                foreach (var attachment in attachments)
                {
                    mail.Attachments.Add(attachment);
                }
            }

            mail.To.Add(new MailAddress(toAddress));
            _smtpServer.Send(mail);
            return true;
        }
    }
}