﻿using System.Collections.Generic;
using System.Net.Mail;

namespace SmartVMA.Infrastructure.Mailer
{
    public interface IMailManager
    {
        bool Send(string toAddress, string subject, string messageBody);
        bool Send(string toAddress, string subject, string messageBody, List<Attachment> attachmentFiles);
        bool Send(string replyto, string toAddress, string subject, string messageBody, List<Attachment> attachmentFiles);
    }
}
