﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Resources;
using System.Linq;

namespace SmartVMA.Services.Validators
{
    internal partial class Validator
    {
        public static ServiceResponse Validate(ConfigurationProfileSetupViewModel model, CompanyTypes companyType)
        {
            var response = new ServiceResponse();
            if (companyType == CompanyTypes.Dealer)
            {
                if (!model.DistributorId.HasValue)
                {
                    response.Errors.Add("DistributorId", Messages.ProfileSetup_DistributorIsRequired);
                }
            }

            if (string.IsNullOrEmpty(model.Name))
            {
                if (companyType == CompanyTypes.Dealer)
                {
                    response.Errors.Add("Name", Messages.ProfileSetup_DealerNameIsRequired);
                }
                else if (companyType == CompanyTypes.Distributor)
                {
                    response.Errors.Add("Name", Messages.ProfileSetup_DistributorNameIsRequired);
                }
            }

            if (companyType == CompanyTypes.Dealer && string.IsNullOrEmpty(model.AccountNumber))
            {
                response.Errors.Add("AccountNumber", Messages.ProfileSetup_AccountNumberIsRequired);
            }

            if (model.DateFormat == 0)
            {
                response.Errors.Add("DateFormat", Messages.ProfileSetup_DateFormatIsRequired);
            }

            response.IsSuccess = !response.Errors.Any();

            return response;
        }

        public static ServiceResponse Validate(ConfigurationProfileSetupDealersViewModel model, CompanyTypes companyType)
        {
            var appContext = IocManager.Resolve<IAppContext>();
            var response = new ServiceResponse();
            if (companyType == CompanyTypes.Dealer)
            {
                if (!model.DistributorId.HasValue && appContext.CurrentUserRoleId == (int)UserRoles.SystemAdministrator)
                {
                    response.Errors.Add("DistributorId", Messages.ProfileSetup_DistributorIsRequired);
                }

                if (model.DateFormat == 0)
                {
                    response.Errors.Add("DateFormat", Messages.ProfileSetup_DateFormatIsRequired);
                }
            }

            if (string.IsNullOrEmpty(model.Name))
            {
                if (companyType == CompanyTypes.Dealer)
                {
                    response.Errors.Add("Name", Messages.ProfileSetup_DealerNameIsRequired);
                }
                //else if (companyType == CompanyTypes.Distributor)
                //{
                //    response.Errors.Add("Name", Messages.ProfileSetup_DistributorNameIsRequired);
                //}
            }

            if (companyType == CompanyTypes.Dealer && string.IsNullOrEmpty(model.AccountNumber) && appContext.CurrentUserRoleId == (int)UserRoles.SystemAdministrator)
            {
                response.Errors.Add("AccountNumber", Messages.ProfileSetup_AccountNumberIsRequired);
            }

            response.IsSuccess = !response.Errors.Any();

            return response;
        }

        public static ServiceResponse Validate(ConfigurationProfileSetupDistributorViewModel model, CompanyTypes companyType)
        {
            var response = new ServiceResponse();

            if (model.DateFormat == 0)
            {
                response.Errors.Add("DateFormat", Messages.ProfileSetup_DateFormatIsRequired);
            }

            if (string.IsNullOrEmpty(model.Name))
            {
                //if (companyType == CompanyTypes.Dealer)
                //{
                //    response.Errors.Add("Name", Messages.ProfileSetup_DealerNameIsRequired);
                //}
                //else
                if (companyType == CompanyTypes.Distributor)
                {
                    response.Errors.Add("Name", Messages.ProfileSetup_DistributorNameIsRequired);
                }
            }

            //if (companyType == CompanyTypes.Dealer && string.IsNullOrEmpty(model.AccountNumber))
            //{
            //    response.Errors.Add("AccountNumber", Messages.ProfileSetup_AccountNumberIsRequired);
            //}

            response.IsSuccess = !response.Errors.Any();

            return response;
        }
    }
}
