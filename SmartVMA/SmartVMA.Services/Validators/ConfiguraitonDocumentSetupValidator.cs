﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Resources;
using System.Linq;

namespace SmartVMA.Services.Validators
{
    internal partial class Validator
    {
        public static ServiceResponse Validate(ConfigurationDocumentSetupViewModel model)
        {
            var response = new ServiceResponse();

            if (model.PrintShopCharges)
            {
                if (!model.ShopChargeAmount.HasValue)
                {
                    response.Errors.Add("ShopChargeAmount", Messages.DocumentSetup_ShopChargeAmountIsRequired);
                }
                else if (model.ShopChargeAmount.Value <= 0 || model.ShopChargeAmount.Value > 100)
                {
                    response.Errors.Add("ShopChargeAmount", Messages.DocumentSetup_ShopChargeAmountMustBePercent);
                }

                if (model.ShopChargeMaximum.HasValue && model.ShopChargeMinimum.HasValue)
                {
                    if (model.ShopChargeMinimum.Value <= 0)
                    {
                        response.Errors.Add("ShopChargeMinimum", Messages.DocumentSetup_ShopChargeMinimumGreaterThanZero);
                    }
                    if (model.ShopChargeMaximum.Value <= model.ShopChargeMinimum.Value)
                    {
                        response.Errors.Add("ShopChargeMaximum", Messages.DocumentSetup_ShopChargeMaximumIsLessThanMin);
                    }
                    else if (model.ShopChargeMaximum.Value <= 0)
                    {
                        response.Errors.Add("ShopChargeMaximum", Messages.DocumentSetup_ShopChargeMaximumGreaterThanZero);
                    }
                }
                else
                {
                    if (!model.ShopChargeMinimum.HasValue)
                    {
                        response.Errors.Add("ShopChargeMinimum", Messages.DocumentSetup_ShopChargeMinimumIsRequired);
                    }

                    if (!model.ShopChargeMaximum.HasValue)
                    {
                        response.Errors.Add("ShopChargeMaximum", Messages.DocumentSetup_ShopChargeMaximumIsRequired);
                    }
                }

                
                if (!(model.ShopChargesApplyToParts || model.ShopChargesApplyToLabor))
                {
                    response.Errors.Add("ShopChargesApplyToParts", Messages.DocumentSetup_TaxRateApplyToPartsIsRequiredAtLeastOne);
                    response.Errors.Add("ShopChargesApplyToLabor", Messages.DocumentSetup_TaxRateApplyToPartsIsRequiredAtLeastOne);
                }
            }

            if ((model.PrintTaxesOnDocuments || model.DisplayTaxesInMenus) && !model.TaxRateAmount.HasValue)
            {
                response.Errors.Add("TaxRateAmount", Messages.DocumentSetup_TaxRateAmountIsRequired);
            }

            if (model.PrintTaxesOnDocuments || model.DisplayTaxesInMenus)
            {
                if (!(model.TaxRateApplyToParts || model.TaxRateApplyToLabor))
                {
                    response.Errors.Add("TaxRateApplyToParts", Messages.DocumentSetup_TaxRateApplyToPartsIsRequiredAtLeastOne);
                    response.Errors.Add("TaxRateApplyToLabor", Messages.DocumentSetup_TaxRateApplyToPartsIsRequiredAtLeastOne);
                }
            }

            response.IsSuccess = !response.Errors.Any();
            return response;
        }
    }
}
