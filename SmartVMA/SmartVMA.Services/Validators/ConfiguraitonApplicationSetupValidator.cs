﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Resources;
using System.Linq;

namespace SmartVMA.Services.Validators
{
    internal partial class Validator
    {
        public static ServiceResponse Validate(ConfigurationApplicationSettingViewModel model)
        {
            var response = new ServiceResponse();

            if (model.IsPlanActive)
            {
                if (string.IsNullOrEmpty(model.PlanProvider))
                {
                    response.Errors.Add("PlanProvider", Messages.ApplicationSetup_PlanProviderIsRequired);
                }
                if (string.IsNullOrEmpty(model.PlanName))
                {
                    response.Errors.Add("PlanName", Messages.ApplicationSetup_PlanNameIsRequired);
                }
            }
            
            if(model.IsRemotePrint)
            {
                if (string.IsNullOrEmpty(model.PrintServer))
                {
                    response.Errors.Add("PrintServer", Messages.ApplicationSetup_PrintServerIsRequired);
                }
                if (string.IsNullOrEmpty(model.PrintPort))
                {
                    response.Errors.Add("PrintPort", Messages.ApplicationSetup_PrintPortIsRequired);
                }
            }
            
            response.IsSuccess = !response.Errors.Any();
            return response;
        }
    }
}
