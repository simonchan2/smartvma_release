﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Resources;
using System;
using System.Linq;

namespace SmartVMA.Services.Validators
{
    internal partial class Validator
    {
        public static ServiceResponse Validate(DealerCustomerViewModel model)
        {
            var response = new ServiceResponse();

            if (model.ConfirmServicesRequest.IsParked != true && model.ConfirmServicesRequest.IsDeclined != true)
            {
                bool isNameFilled = true;
                if (string.IsNullOrEmpty(model.FirstName) || string.IsNullOrEmpty(model.LastName))
                {
                    response.Errors.Add("FirstName", Messages.DealerCustomerFirstNameOrCompanyName);
                    response.Errors.Add("LastName", Messages.DealerCustomerFirstNameOrCompanyName);
                    isNameFilled = false;
                }
                if (!isNameFilled)
                {
                    if (string.IsNullOrEmpty(model.CompanyName))
                    {
                        response.Errors.Add("CompanyName", Messages.DealerCustomerFirstNameOrCompanyName);
                    }
                    else
                    {
                        response.Errors.Clear();
                    }
                }
            }

            if (!String.IsNullOrEmpty(model.VIN))
            {
                int? vinmasterId;

                var _vinMasterRepository = IocManager.Resolve<IVinMasterRepository>();
                var _vehicleRepository = IocManager.Resolve<IVehicleRepository>();

                vinmasterId = _vinMasterRepository.GetVinMasterByVinShort(model.VIN.Substring(0, 8) + model.VIN.Substring(9, 2));
                if (vinmasterId == null)
                {
                    response.Errors.Add("VIN", Messages.InvalidVIN);
                }

                var _dealerVehicleRepository = IocManager.Resolve<IDealerVehicleRepository>();
                if (_dealerVehicleRepository.IsExistingVIN(model.VIN, model.ConfirmServicesRequest.CarId) || _vehicleRepository.IsExistingVINForOtherCar(model.ConfirmServicesRequest.CarId, model.VIN))
                {
                    response.Errors.Add("ErrorMessage", Messages.ExistingVin);
                }
            }


            response.IsSuccess = !response.Errors.Any();
            return response;
        }
    }
}
