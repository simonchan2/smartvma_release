﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Resources;
using System.Linq;

namespace SmartVMA.Services.Validators
{
    internal partial class Validator
    {
        public static ServiceResponse Validate(SaveConfirmServiceRequestModel model)
        {
            var response = new ServiceResponse();

            //company settings [RequiredIfSetting(Setting = CompanySettingsNames.DisplayInvoiceNumber)]
            if (model.IsInvoiceRequired)
            {
                if (!model.IsParked && !model.IsDeclined)
                {
                   
                    if (string.IsNullOrEmpty(model.Invoice))
                    {
                        response.Errors.Add("Invoice", Messages.ConfirmServicesInvoiceRequired);
                    }
                }
            }

            response.IsSuccess = !response.Errors.Any();
            return response;
        }
    }
}
