﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Resources;
using System.Linq;

namespace SmartVMA.Services.Validators
{
    internal partial class Validator
    {
        public static ServiceResponse Validate(ConfigurationMenuSetupViewModel model)
        {
            var response = new ServiceResponse();

            if (model.DefaultLaborRate <= 0)
            {
                response.Errors.Add("DefaultLaborRate", Messages.MenuSetup_DefaultLaborRateGreaterThanZero);
            }

            if (model.ConvertCurrency && !model.ExchangeRate.HasValue)
            {
                response.Errors.Add("ExchangeRate", Messages.MenuSetup_ExchangeRateIsRequired);
            }

            if (model.MenuLevels == 3 && string.IsNullOrEmpty(model.MenuLabelLevel3))
            {
                response.Errors.Add("MenuLabelLevel3", Messages.MenuSetup_MenuLabelLevel3Required);
            }
            if (model.MenuLevels >= 2 && string.IsNullOrEmpty(model.MenuLabelLevel2))
            {
                response.Errors.Add("MenuLabelLevel2", Messages.MenuSetup_MenuLabelLevel2Required);
            }
            if (string.IsNullOrEmpty(model.MenuLabelLevel1))
            {
                response.Errors.Add("MenuLabelLevel1", Messages.MenuSetup_MenuLabelLevel1Required);
            }

            if (model.LOFMenuLevels == 3 && string.IsNullOrEmpty(model.LOFMenuLabelLevel3))
            {
                response.Errors.Add("LOFMenuLabelLevel3", Messages.MenuSetup_LOFMenuLabelLevel3Required);
            }
            if (model.LOFMenuLevels >= 2 && string.IsNullOrEmpty(model.LOFMenuLabelLevel2))
            {
                response.Errors.Add("LOFMenuLabelLevel2", Messages.MenuSetup_LOFMenuLabelLevel2Required);
            }
            if (string.IsNullOrEmpty(model.LOFMenuLabelLevel1))
            {
                response.Errors.Add("LOFMenuLabelLevel1", Messages.MenuSetup_LOFMenuLabelLevel1Required);
            }


            if (model.LOFMenuLevels == 3 && string.IsNullOrEmpty(model.LOFMenuOpCodeLevel3))
            {
                response.Errors.Add("LOFMenuOpCodeLevel3", Messages.MenuSetup_LOFMenuOpCodeLevel3Required);
            }
            if (model.LOFMenuLevels >= 2 && string.IsNullOrEmpty(model.LOFMenuOpCodeLevel2))
            {
                response.Errors.Add("LOFMenuOpCodeLevel2", Messages.MenuSetup_LOFMenuOpCodeLevel2Required);
            }
            if (string.IsNullOrEmpty(model.LOFMenuOpCodeLevel1))
            {
                response.Errors.Add("LOFMenuOpCodeLevel1", Messages.MenuSetup_LOFMenuOpCodeLevel1Required);
            }

            response.IsSuccess = !response.Errors.Any();
            return response;
        }
    }
}
