﻿using System.Linq;
using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Resources;

namespace SmartVMA.Services.Validators
{
    internal partial class Validator
    {
        public static ServiceResponse Validate(UserViewModel model)
        {
            var response = new ServiceResponse();
            if (!model.Tenants.Any())
            {
                response.Errors.Add("Tenants", Messages.EmailAlreadyInUse);
            }
            if (!model.RoleId.HasValue)
            {
                response.Errors.Add("RoleId", Messages.RoleIsRequired);
            }

            // check if user with provided email exists, if such user exists return error message
            if (!model.Id.HasValue)
            {
                var repository = IocManager.Resolve<IUserRepository>();
                var existingUser = repository.GetUserByEmail(model.Email);
                if (existingUser != null)
                {
                    response.Errors.Add("Email", Messages.EmailAlreadyInUse);
                }
            }
            response.IsSuccess = !response.Errors.Any();
            return response;
        }
    }
}
