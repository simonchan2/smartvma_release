﻿using SmartVMA.Infrastructure.Ioc;
using System.Web;
using System.Web.Hosting;
using System.Linq;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Core.Repositories;
using System.Configuration;
using System.IO;
using SmartVMA.Core;

namespace SmartVMA.Services.AppContext
{
    [IocBindable(ScopeType = InstanceScopeType.PerRequest)]
    internal class HttpAppContext : IAppContext
    {
        private readonly string _defaultHostFilePath;

        public HttpAppContext()
        {
            _defaultHostFilePath = ConfigurationManager.AppSettings["DefaultDocumentsPathName"];
        }

        private UserIdentity _currentIdentity;
        public UserIdentity CurrentIdentity
        {
            get
            {
                if (_currentIdentity == null)
                {
                    _currentIdentity = new UserIdentity();
                    if (!HttpContext.Current.User.Identity.IsAuthenticated)
                    {
                        return _currentIdentity;
                    }

                    // "{0}_{1}_{2}"  userId, role, tenant
                    var identities = HttpContext.Current.User.Identity.Name.Split('_').Select(int.Parse).ToArray();
                    if (identities[0] != 0)
                    {
                        _currentIdentity.UserId = identities[0];
                    }

                    if (identities[1] != 0)
                    {
                        _currentIdentity.RoleId = identities[1];
                    }

                    if (identities[2] != 0)
                    {
                        _currentIdentity.TenantId = identities[2];
                    }
                }
                return _currentIdentity;
            }
        }

        private string _currentUserName;
        public string CurrentUserName
        {
            get
            {
                if (!IsUserAuthenticated)
                {
                    return string.Empty;
                }
                if (string.IsNullOrEmpty(_currentUserName))
                {
                    var repository = IocManager.Resolve<IUserRepository>();
                    var user = repository.GetUserById(CurrentIdentity.UserId ?? 0);
                    _currentUserName = $"{user.FirstName} {user.LastName}";
                }
                return _currentUserName;
            }
        }

        private string _currentUserEmail;
        public string CurrentUserEmail
        {
            get
            {
                if (!IsUserAuthenticated)
                {
                    return string.Empty;
                }
                if (!string.IsNullOrEmpty(_currentUserEmail))
                {
                    return _currentUserEmail;
                }

                var repository = IocManager.Resolve<IUserRepository>();
                var user = repository.GetUserById(CurrentIdentity.UserId ?? 0);
                _currentUserEmail = user.Email;
                return _currentUserEmail;
            }
        }

        public string MapPath(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return path;
            }
            return string.IsNullOrEmpty(_defaultHostFilePath) ? HostingEnvironment.MapPath($"/{path}") : Path.Combine(_defaultHostFilePath, path);
        }

        public string MapUrl(string url, int? tenantId = null)
        {
            if (string.IsNullOrEmpty(url))
            {
                return url;
            }
            var request = HttpContext.Current.Request;
            if (url.Contains("Temp/") || url.Contains("UserContent/"))
            {
                if (!string.IsNullOrEmpty(StaticSettings.ContentUrl))
                {
                    return $"{StaticSettings.ContentUrl}{url.TrimStart('/')}";
                }
            }

            if (tenantId.HasValue)
            {
                return $"{request.Url.Scheme}://{request.Url.Authority}/{tenantId ?? CurrentIdentity.UserId}/{url}";
            }
            return $"{request.Url.Scheme}://{request.Url.Authority}/{url}";
        }
        
        public string TempFolderName => "Temp";

        public bool IsUserAuthenticated => HttpContext.Current.User.Identity.IsAuthenticated;

        public string CurrentUserRoleName
        {
            get
            {
                if (!IsUserAuthenticated)
                {
                    return string.Empty;
                }

                var repository = IocManager.Resolve<IRoleRepository>();
                var role = repository.FirstOrDefault(x => x.Id == CurrentIdentity.RoleId);
                return role.Description;
            }
        }

        public int CurrentUserRoleId
        {
            get
            {
                if (!IsUserAuthenticated)
                {
                    return 0;
                }

                var repository = IocManager.Resolve<IRoleRepository>();
                var role = repository.FirstOrDefault(x => x.Id == CurrentIdentity.RoleId);
                return role.Id;
            }
        }

        public string UserTitle
        {
            get
            {
                if (!IsUserAuthenticated)
                {
                    return string.Empty;
                }

                var repository = IocManager.Resolve<IUserRepository>();
                var user = repository.GetUserById(CurrentIdentity.UserId ?? 0);
                return user.Title;
            }
        }

        public string ImagesFolder => "UserContent/Images/";

        public string CompanyContentFolder => "UserContent/Companies/";
        public string VehiclePhotosFolder => "UserContent/VehiclePhotos/";
        public string DocumentsFolder => "UserContent/SmartVMADocuments/";

        public string LOFImagesFolder => "UserContent/LOFImages/";

        public string ReportingServiceUri
        {
            get { return ConfigurationManager.AppSettings["ReportingServiceUri"]; }
        }

        public string ReportingServiceUserName
        {
            get { return ConfigurationManager.AppSettings["ReportingServiceUserName"]; }
        }

        public string ReportingServicePwd
        {
            get { return ConfigurationManager.AppSettings["ReportingServicePwd"]; }
        }

        public string ReportingServiceDomain
        {
            get { return ConfigurationManager.AppSettings["ReportingServiceDomain"]; }
        }  

    }
}
