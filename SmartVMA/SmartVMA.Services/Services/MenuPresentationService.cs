﻿using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.OutputModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Services.Mappers;
using System.Collections.Generic;
using System.Linq;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class MenuPresentationService : BaseServicePresentationService, IMenuPresentationService
    {
        protected override bool IsLofMenu => false;

        public MenuPresentationService(IDealerCustomerRepository dealerCustomerRepository, ICompanyService companyService, IGeneralCarServiceRepository generalServicerRepository, IAppointmentPresentationService appointmentPresentationService, ICarService carService)
            : base(dealerCustomerRepository, companyService, generalServicerRepository, appointmentPresentationService, carService)
        {
        }

        public override ServiceBasePresentationViewModel GetViewModel(int? carId, int? mileage, int? dealerCustomerId, long? appointmentPresentationId, long? inspectionId, int? vehicleTransmission, int? vehicleDriveline, int? enteredMileage, int? dealerVehicleId, string vin, int? carIdUndetermined)
        {
            var model = base.GetViewModel(carId, mileage, dealerCustomerId, appointmentPresentationId, inspectionId, vehicleTransmission, vehicleDriveline, enteredMileage ?? mileage, dealerVehicleId, vin, carIdUndetermined);
            if (AllServices != null)
            {
                var additonalServices = new List<ServiceDetailViewModel>();
                foreach (var item in AllServices.Where(x => x.ServiceMenuType == (int)ServiceMenuTypes.AdditionalService))
                {
                    if (!AllServices.Any(x => x.ServiceId == item.ServiceId && x.ServiceMenuType != (int)ServiceMenuTypes.AdditionalService))
                    {
                        additonalServices.Add(Mapper.Map(item));
                    }
                    else if (item.ServiceMenuType == (int)ServiceMenuTypes.AdditionalService)
                    {
                        item.IsAdditionalAddedAfter = true;
                        additonalServices.Add(Mapper.Map(item));
                    }
                }

                // set additional services
                model.AdditionalServices = new PreviewAdditionalServicesIndexViewModel { Items = additonalServices };
                if (model.AdditionalServices.Items.Any())
                {
                    model.AdditionalServices.Items = model.AdditionalServices.Items.OrderBy(x => x.Description);
                    IEnumerable<ServiceDetailInvoiceHistoryViewModel> historyInvoices = GeneralServiceRepository.GetHistoryInvoices(additonalServices);
                    foreach (var service in additonalServices.Where(c => c.BgProtectionPlanId != null))
                    {
                        GetLppColourForService(model.EnteredMileage, historyInvoices, service, model.IsDmsDealer);
                    }
                }
            }

            return model;
        }

        protected override MenuLevelServicesIndexViewModel GetServices(ServicesProcedureModel[] services)
        {
            var model = new MenuLevelServicesIndexViewModel();
            var menuLevels = CompanyService.GetSettingAsInteger(CompanySettingsNames.MenuLevels);

            model.MenuLabelLevel1 = GetMenuLevelLabel((int)MenuLevelTypes.FirstLevel);
            model.MenuLevel1Services = services.Where(x => x.ServiceMenuType == (int)ServiceMenuTypes.MaintenanceService && x.MenuLevel == (int)MenuLevelTypes.FirstLevel).Select(x => Mapper.Map(x));
            model.MenuLabelLevel1Price = CalculateMenuPrice(model.MenuLevel1Services);

            if (menuLevels >= (int)MenuLevelTypes.SecondLevel)
            {
                // TODO: update GetMaintenanceServices sp to read from actual table
                model.MenuLabelLevel2 = GetMenuLevelLabel((int)MenuLevelTypes.SecondLevel);
                model.MenuLevel2Services = services.Where(x => x.ServiceMenuType == (int)ServiceMenuTypes.MaintenanceService && x.MenuLevel <= (int)MenuLevelTypes.SecondLevel).Select(x => Mapper.Map(x));
                model.MenuLabelLevel2Price = CalculateMenuPrice(model.MenuLevel2Services);

                if (menuLevels == (int)MenuLevelTypes.ThirdLevel)
                {
                    // TODO: update GetMaintenanceServices sp to read from actual table
                    model.MenuLabelLevel3 = GetMenuLevelLabel((int)MenuLevelTypes.ThirdLevel);
                    model.MenuLevel3Services = services.Where(x => x.ServiceMenuType == (int)ServiceMenuTypes.MaintenanceService && x.MenuLevel <= (int)MenuLevelTypes.ThirdLevel).Select(x => Mapper.Map(x));
                    model.MenuLabelLevel3Price = CalculateMenuPrice(model.MenuLevel3Services);
                }
            }
            return model;
        }
    }
}
