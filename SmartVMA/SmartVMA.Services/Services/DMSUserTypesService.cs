﻿using System.Collections.Generic;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Core.Repositories;
using System.Linq;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class DMSUserTypesService : IDMSUserTypeService
    {
        private readonly IDMSUserTypesRepository _dmsUserTypesRepository;

        public DMSUserTypesService(IDMSUserTypesRepository dmsUserTypesRepository)
        {
            _dmsUserTypesRepository = dmsUserTypesRepository;
        }

        public IEnumerable<DMSUserTypesViewModel> GetDmsUserTypes()
        {
            return _dmsUserTypesRepository.Find().Select(x => new DMSUserTypesViewModel { Id = x.Id, Name = x.Description });
        }
    }
}
