﻿using System;
using System.Collections.Generic;
using System.Linq;
using SmartVMA.Core;
using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.InputModels.ViewModels.Admin.CustomServices;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Resources;
using SmartVMA.Services.Mappers;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class AditionalServiceService : IAdditionalServiceService
    {
        private readonly IAdditionalServiceRepository _repository;
        public AditionalServiceService(IAdditionalServiceRepository repository)
        {
            _repository = repository;
        }

        public AdditionalServiceViewModel GetViewModel(int? id = null)
        {
            return new AdditionalServiceViewModel();
        }

        public ServiceResponse Save(AdditionalServiceViewModel model)
        {
            //if (model.Id.HasValue)
            //{
            //    var entity = _repository.FirstOrDefault(x => x.Id == model.Id);
            //    entity = Mapper.Map(model, entity);
            //    _repository.Update(entity);
            //}
            //else
            //{
            //    var entity = Mapper.Map(model);
            //    _repository.Insert(entity);
            //}

            var response = _repository.CommitChanges();
            return response;
        }

        public ServiceResponse Save(IEnumerable<CustomServiceViewModel> services)
        {
            var servicesArray = services.ToArray();
            var additionalServicePartRepository = IocManager.Resolve<IAdditionalServicePartRepository>();
            var updatedServicesIds = servicesArray.Select(m => m.Id).ToArray();
            var existingServices = _repository.GetServicesAndParts(x => updatedServicesIds.Any(m => m == x.Id)).ToArray();
            var companyService = IocManager.Resolve<ICompanyService>();
            var currentMeasurementUnitId = companyService.GetSettingAsInteger(CompanySettingsNames.MeasurementUnit);
            foreach (var item in servicesArray)
            {
                var entity = existingServices.FirstOrDefault(x => x.Id == item.Id) ?? new AdditionalService();
                entity = Mapper.Map(item, entity);

                foreach (var part in item.Parts)
                {
                    if (part.Id.HasValue)
                    {
                        var servicePart = entity.AdditionalServiceParts.FirstOrDefault(x => x.Id == part.Id);
                        servicePart = Mapper.Map(part, servicePart);
                        if (part.IsDeleted)
                        {
                            additionalServicePartRepository.Delete(servicePart);
                        }
                        else
                        {
                            additionalServicePartRepository.Update(servicePart);
                        }
                    }
                    else if (!string.IsNullOrEmpty(part.PartNo))
                    {
                        if (!part.IsDeleted)
                        {
                            var servicePart = Mapper.Map(part);
                            servicePart.AdditionalService = entity;
                            additionalServicePartRepository.Insert(servicePart);
                        }
                    }
                }


                if (item.VideoId.HasValue)
                {
                    var serviceVideo = entity.AdditionalServiceVideoClips.FirstOrDefault();
                    if (serviceVideo == null)
                    {
                        entity.AdditionalServiceVideoClips.Add(new AdditionalServiceVideoClip { BgVideoClipId = item.VideoId.Value });
                    }
                    else
                    {
                        serviceVideo.BgVideoClipId = item.VideoId.Value;
                    }
                }

                if (item.Id.HasValue)
                {
                    _repository.Update(entity);
                }
                else
                {
                    entity.MeasurementUnitId = currentMeasurementUnitId;
                    _repository.Insert(entity);
                }
            }

            var response = _repository.CommitChanges();
            return response;
        }

        public ServiceResponse Save(MapCustomServicesToVehiclesViewModel model)
        {
            var additionalServiceCarRepository = IocManager.Resolve<IAdditionalServiceCarRepository>();
            string[] serviceIds = model.SelectedServices.Replace(StaticSettings.DatatableRowId, "").Split(new[] { StaticSettings.Delimiter }, StringSplitOptions.None);
            var result = additionalServiceCarRepository.Save(
                services: model.Items.Where(x => serviceIds.Contains(x.Id.ToString())).Select(x => x.Id).ToList(),//model.Items.Where(x => x.IsSelected).Select(x => x.Id).ToList(),
                years: model.Years,
                makes: model.Makes,
                models: model.Models,
                engines: model.EngineTypes,
                transmissions: model.TransmissionTypes,
                driveLines: model.DriveLines,
                menuLevel: model.MenuLevel);

            var response = new ServiceResponse { IsSuccess = result };
            return response;
        }

        public CustomServiceInitViewModel GetCustomServiceInitViewModel()
        {
            var model = new CustomServiceInitViewModel
            {
                VehicleFilter = new CustomServicesVehicleFilterViewModel
                {
                    Years = new List<int>(),
                    Makes = new List<int>(),
                    Models = new List<int>(),
                    EngineTypes = new List<int>(),
                    TransmissionTypes = new List<int>(),
                    DriveLines = new List<int>()
                },
                Data = new CustomServiceViewModel { Parts = new List<CustomServicePartViewModel> { new CustomServicePartViewModel() } }
            };
            return model;
        }

        public CustomServiceIndexViewModel GetIndexViewModel(CustomServiceMaintenancePagingViewModel request)
        {
            int itemsCount;
            string filteredServiceIds;
            IEnumerable<CustomServiceViewModel> items;
            if (request.Years == null && request.DriveLines == null && request.Makes == null && request.Models == null && request.TransmissionTypes == null)
            {
                items = _repository.GetAllServicesAndParts(request, out itemsCount, out filteredServiceIds).Select(x => Mapper.Map(x, new CustomServiceViewModel()));
                itemsCount = _repository.GetItemsCount(request);
            }
            else
            {
                items = _repository.GetServicesAndParts(request, out itemsCount, out filteredServiceIds).Select(x => Mapper.Map(x, new CustomServiceViewModel()));
            }

            var model = new CustomServiceIndexViewModel
            {
                Draw = request.Draw + 1,
                ItemsCount = itemsCount,
                RecordsFilteredCount = itemsCount,
                Items = items,
                FilteredServiceIds = filteredServiceIds
            };

            return model;
        }

        public CustomServiceIndexViewModel GetAllServicesIndexViewModel(CustomServiceMaintenancePagingViewModel request)
        {
            int itemsCount;
            string filteredServiceIds;
            var items = _repository.GetAllServicesAndParts(request, out itemsCount, out filteredServiceIds).Select(x => Mapper.Map(x, new CustomServiceViewModel())).ToList();
            var model = new CustomServiceIndexViewModel
            {
                Draw = request.Draw + 1,
                ItemsCount = itemsCount,
                RecordsFilteredCount = itemsCount,
                Items = items,
                FilteredServiceIds = filteredServiceIds
            };
            return model;
        }

        public ServiceResponse DeleteBulk(string ids)
        {
            return _repository.DeleteBulk(ids);
        }

        public ServiceResponse Validate(IEnumerable<CustomServiceViewModel> services)
        {
            var response = new ServiceResponse();
            var counter = 0;
            foreach (var item in services)
            {
                var partsCounter = 0;
                var isRequired = false;
                foreach (var part in item.Parts)
                {
                    if (part.BgProductId.HasValue || part.IsFluid)
                    {
                        isRequired = true;
                    }

                    if (string.IsNullOrEmpty(part.PartNo) && (isRequired || part.Quantity.HasValue || part.UnitPrice.HasValue))
                    {
                        response.Errors.Add($"items[{counter}].parts[{partsCounter}].partNo", Messages.CustomServices_PartNumberRequired);
                    }

                    if (!part.Quantity.HasValue && (isRequired || !string.IsNullOrEmpty(part.PartNo) || part.UnitPrice.HasValue))
                    {
                        response.Errors.Add($"items[{counter}].parts[{partsCounter}].quantity", Messages.CustomServices_QuantityRequired);
                    }

                    if (!part.UnitPrice.HasValue && (isRequired || !string.IsNullOrEmpty(part.PartNo) || part.Quantity.HasValue))
                    {
                        response.Errors.Add($"items[{counter}].parts[{partsCounter}].unitPrice", Messages.CustomServices_UnitPriceRequired);
                    }

                    partsCounter += 1;
                }

                counter += 1;
            }

            response.IsSuccess = !response.Errors.Any();
            return response;
        }
    }
}
