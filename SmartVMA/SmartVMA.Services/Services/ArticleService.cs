﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Services.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class ArticleService : IArticleService
    {
        private readonly IArticleRepository _repository;
        private readonly IArticleArticleCategoryRepository _categoryRepository;
        protected readonly IAppContext _appContext;

        public ArticleService(IArticleRepository repository, IArticleArticleCategoryRepository categoryRepository, IAppContext appContext)
        {
            _repository = repository;
            _categoryRepository = categoryRepository;
            _appContext = appContext;
        }

        public ArticleViewModel GetArticle(int id)
        {
            var article = _repository.FirstOrDefault(x => x.Id == id);
            var model = Mapper.Map(article);

            return model;
        }

        public ArticleIndexViewModel GetIndexViewModel()
        {
            var aticles = new List<ArticleViewModel>();
            aticles.AddRange(_repository.GetAllArticles().Select(x => Mapper.Map(x))); // get the entity from Repository, then Map it to View Model

            var model = new ArticleIndexViewModel();
            model.Items = aticles;
            return model;
        }

        public ArticleViewModel GetViewModel(int? id = null)
        {
            var model = new ArticleViewModel();
            if (!id.HasValue)
            {
                return model;
            }

            var user = _repository.GetArticleById(id.Value);
            model = Mapper.Map(user, model);
            return model;
        }

        public ServiceResponse Save(ArticleViewModel model)
        {
            if (model.Id.HasValue)  // if "model.Id.HasValue" => the item already exist in DB, so just Update it
            {
                var article = Mapper.Map(model, _repository.FirstOrDefault(x => x.Id == model.Id.Value));
                article.ModifyUserId = _appContext.CurrentIdentity.UserId.Value;

                _repository.Update(article);

                foreach (var item in model.CategoryIds) // add or delete categories for this article
                {
                    //if this is new added category, add the relationship in db
                    if (!_categoryRepository.Find().Any(x => x.ArticleCategoryId == item && x.ArticleId == model.Id))
                    {
                        _categoryRepository.Insert(new ArticleArticleCategory { ArticleId = model.Id.Value, ArticleCategoryId = item });
                    }
                    var articleArticleCategories = _categoryRepository.Find().Where(x => x.ArticleId == model.Id.Value);
                    foreach (var cat in _categoryRepository.Find().Where(x => x.ArticleId == model.Id.Value))
                    {
                        //if the category Id is removed from Article.CategoryId list, remove the relationship in DB
                        if (!model.CategoryIds.Contains(cat.ArticleCategoryId))
                        {
                            _categoryRepository.Delete(cat);
                        }
                    }
                }
            }
            else // if the Id is empty, then it's new item, so Insert a new one in the DB
            {
                var article = Mapper.Map(model);
                article.CreatorId = _appContext.CurrentIdentity.UserId.Value;
                article.ModifyUserId = _appContext.CurrentIdentity.UserId.Value;
                article.IsActive = true;
                _repository.Insert(article);

                //TODO: save categories for new added Article
                foreach (var categoryId in model.CategoryIds)
                {
                    _categoryRepository.Insert(article, categoryId);
                }
            }
            

            var response = _repository.CommitChanges(); // always do .CommitChanges() after, so it can save the changes in the DB 
            return response;
        }
       
        public ServiceResponse Delete(int articleId)
        {
            var article = _repository.FirstOrDefault(x => x.Id == articleId);
            article.TimeDeleted = DateTime.Now;
            article.IsActive = false;
            _repository.Update(article);
            var response = _repository.CommitChanges(); // always do .CommitChanges() after, so it can save the changes in the DB 
            return response;
        }

        public ArticleIndexViewModel Search(string key, int[] categoryIds)
        {
            var articlesEntity = _repository.GetArticleByKeyWordAndCategoryId(key, categoryIds);
            foreach (var art in articlesEntity)
            {
                if (art.Description.Length > 100)
                {
                    string description = art.Description.Substring(0, 100);
                    while (!char.IsWhiteSpace(description.Last())) //split on white space, not to cut a word
                    {
                        description = description.Substring(0, description.Length - 1);
                    }
                    art.Description = description + "[..]";
                }
            }
            var articles = new List<ArticleViewModel>();
            articles.AddRange(articlesEntity.Select(x => Mapper.Map(x))); // get the entity from Repository, then Map it to View Model

            var model = new ArticleIndexViewModel();
            model.Items = articles;
            model.ItemsCount = articles.Count();
            return model;
        }

        public ArticleIndexViewModel GetPagedItems(PagingRequest request)
        {
            var entities = _repository.GetPagedResults(request);
            var items = new List<ArticleViewModel>(entities.Select(x => Mapper.Map(x)));
            var model = new ArticleIndexViewModel {Items = items};
            var total = _repository.GetPagedResultsTotal(request);
            model.ItemsCount = total;

            return model;
        }
    }
}
