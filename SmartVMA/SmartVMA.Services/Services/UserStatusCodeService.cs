﻿using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using System.Collections.Generic;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class UserStatusCodeService : IUserStatusCodeService
    {
        private readonly IUserStatusCodeRepository _repository;
        public UserStatusCodeService(IUserStatusCodeRepository repository)
        {
            _repository = repository;
        }

        public UserStatusCodeIndexViewModel GetIndexViewModel()
        {
            var statusCodes = new List<UserStatusCodeViewModel>();
            foreach (var item in _repository.Find())
            {
                statusCodes.Add(new UserStatusCodeViewModel { Id = item.Id, Description = item.Description });
            }
            return new UserStatusCodeIndexViewModel { Items = statusCodes };
        }
    }
}
