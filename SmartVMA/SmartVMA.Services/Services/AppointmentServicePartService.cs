﻿using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using System.Collections.Generic;
using SmartVMA.Services.Mappers;
using System;
using SmartVMA.Core.Contracts;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class AppointmentServicePartService : IAppointmentServicePart
    {
        private readonly IAppointmentRepository _repository;
        //private readonly IAppointmentServiceRepository _appointmentServiceRepository;
        private readonly IAppointmentServicePartRepository _appointmentServicePartRepository;
        public AppointmentServicePartService(IAppointmentRepository repository, IAppointmentServicePartRepository appointmentServicePartRepository)
        {
            _repository = repository;
            _appointmentServicePartRepository = appointmentServicePartRepository;
        }        

       
        public List<AppointmentServiceViewModel> GetAppointmentServicePartModel(int dealerCustomerID)
        {
            var items = _repository.GetAppointmentServiceModel(dealerCustomerID);
            return items;

        }

        public AdditionalServiceViewModel GetViewModel(int? id = null)
        {
            return new AdditionalServiceViewModel();
        }

        public ServiceResponse Save(AdditionalServiceViewModel model)
        {
            //if (model.Id.HasValue)
            //{
            //    var entity = _appointmentServiceRepository.FirstOrDefault(x => x.Id == model.Id);
            //    entity = Mapper.Map(model, entity);
            //    _appointmentServiceRepository.Update(entity);
            //}
            //else
            //{
            //    var entity = Mapper.Map(model);
            //    _appointmentServiceRepository.Insert(entity);
            //}

            //var response = _appointmentServiceRepository.CommitChanges();
            ServiceResponse response  = new ServiceResponse();
            response.IsSuccess = true;
            return response;
        }
    }
}
