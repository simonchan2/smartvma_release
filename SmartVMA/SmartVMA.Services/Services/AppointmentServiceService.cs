﻿using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using System.Collections.Generic;
using SmartVMA.Services.Mappers;
using System;
using System.Linq;
using SmartVMA.Core.Contracts;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class AppointmentServiceService : IAppointmentService
    {
        private readonly IAppointmentRepository _repository;
        private readonly IAppointmentServiceRepository _appointmentServiceRepository;
        public AppointmentServiceService(IAppointmentRepository repository, IAppointmentServiceRepository appointmentServiceRepository)
        {
            _repository = repository;
            _appointmentServiceRepository = appointmentServiceRepository;
        }

        public AppointmentIndexViewModel GetCalendarAppointmentsModel(DateTime startTime, DateTime endTime, string advisorIds, string presentationStatus)
        {
            var appointments = _repository.GetCalendarAppointments(startTime, endTime, advisorIds, presentationStatus);

            var model = new AppointmentIndexViewModel { Items = appointments };
            return model;
        }

        public AppointmentIndexViewModel GetSearchAppointmentsModel(string customerName, string customerNr, string customerPhone, string vin, string vehicleStock, string licensePlate)
        {
            var appointments = _repository.GetSearchAppointments(customerName, customerNr, customerPhone, vin, vehicleStock, licensePlate);

            var model = new AppointmentIndexViewModel { Items = appointments };
            return model;
        }

        public List<AppointmentServiceViewModel> GetAppointmentServiceModel(string vin)
        {
            var items = _repository.GetAppointmentServiceModel(vin);
            if (items != null && items.Count > 0 && items.Any(c => c.BgProtectionPlanId != null && !string.IsNullOrEmpty(c.BgProductCategory)))
            {
                var planLevels = _repository.GetVehicleHistoryPlanLevels();
                foreach (var item in items.Where(c => c.BgProtectionPlanId != null && !string.IsNullOrEmpty(c.BgProductCategory)))
                {
                    var groupedItemsByService = items.Where(c => c.IsStriked == false && c.OpCode == item.OpCode && c.OpDescription == item.OpDescription).OrderBy(c => c.AppointmentTime);
                    var planLevel = planLevels.Where(c => c.BgProtectionPlanId == item.BgProtectionPlanId && c.MaxMileageBeforeFirstService >= groupedItemsByService.Min(d => d.Mileage)).OrderBy(c => c.MaxMileageBeforeFirstService).FirstOrDefault();
                    if(planLevel != null)
                    {
                        item.ProtectionPlanLevel = planLevel.ProtectionPlanLevel;
                    }
                }
            }

            return items;

        }

        public List<AppointmentServiceViewModel> GetAppointmentServiceModel(int dealerCustomerID)
        {
            var items = _repository.GetAppointmentServiceModel(dealerCustomerID);
            return items;

        }

        public AdditionalServiceViewModel GetViewModel(int? id = null)
        {
            return new AdditionalServiceViewModel();
        }        

        public long CreateAppointmentPresentation(long appointmentID, int? userId, string status)
        {
            return _repository.CreateAppointmentPresentation(appointmentID, userId, status);
        }

        public ServiceResponse Save(AdditionalServiceViewModel model)
        {
            //if (model.Id.HasValue)
            //{
            //    var entity = _appointmentServiceRepository.FirstOrDefault(x => x.Id == model.Id);
            //    entity = Mapper.Map(model, entity);
            //    _appointmentServiceRepository.Update(entity);
            //}
            //else
            //{
            //    var entity = Mapper.Map(model);
            //    _appointmentServiceRepository.Insert(entity);
            //}

            //var response = _appointmentServiceRepository.CommitChanges();
            ServiceResponse response = new ServiceResponse();
            response.IsSuccess = true;
            return response;
        }

        public List<PdfDocumentViewModel> GetPdfDocuments(DateTime DateTo)
        {
            return _repository.GetPdfDocuments(DateTo);
        }

        public void DeleteObsoleteAppointments(DateTime DateTo)
        {
            _repository.DeleteObsoleteAppointments(DateTo);
        }
    }
}
