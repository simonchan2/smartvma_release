﻿using System;
using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Services.Mappers;
using System.Collections.Generic;
using System.Linq;
using SmartVMA.Core;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.FileManager;
using SmartVMA.Resources;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class InspectionService : IInspectionService
    {
        private readonly IInspectionRepository _repository;
        private readonly IVehiclePhotoRepository _vprepository;
        private readonly IDamageIndicationRepository _direpository;
        private readonly IDamageIndicationTypeRepository _ditrepository;
        private readonly IViewPointRepository _viewpointrepository;
        private readonly IAppointmentPresentationRepository _appointmentPresentationRepository;

        public InspectionService(IInspectionRepository repository,
            IVehiclePhotoRepository vprepository,
            IDamageIndicationRepository direpository,
            IDamageIndicationTypeRepository ditrepository,
            IViewPointRepository viewpointrepository,
            IAppointmentPresentationRepository appointmentPresentationRepository)
        {
            _repository = repository;
            _vprepository = vprepository;
            _direpository = direpository;
            _ditrepository = ditrepository;
            _viewpointrepository = viewpointrepository;
            _appointmentPresentationRepository = appointmentPresentationRepository;
        }

        public InspectionViewModel GetViewModel(long? inspectionId, long? appointmentId, int? transmissionId, int? driveLineId, int? enteredMileage)
        {
            InspectionViewModel inspection = new InspectionViewModel();
            //Get inspection only if we have one of the arguments
            if (inspectionId != null || appointmentId != null)
            {
                inspection = _repository.GetInspectionViewModel(inspectionId, appointmentId);
            }
            //if there is no inspection for this appointmentId add a new one
            if (inspection.Id == null || inspection.Id == 0)
            {
                Inspection inspectionModel = new Inspection { AppointmentPresentationId = appointmentId };
                _repository.AddSetInspection(inspectionModel);
                inspection = _repository.GetInspectionViewModel(inspectionModel.Id, appointmentId);
            }
            inspection.Transmission = transmissionId;
            inspection.Driveline = driveLineId;
            inspection.EnteredMileage = enteredMileage;
            if(inspection.AppointmentPresentationId != null)
            {
                var app = _appointmentPresentationRepository.FirstOrDefault(c => c.Id == inspection.AppointmentPresentationId);
                inspection.AppointmentPresentationStatus = app.PresentationStatus;
            }
            return inspection;
        }

        public ServiceResponse LinkInspectionToAppointmentPresentation(long inspectionId, long appointmentPresentationId)
        {
            var response = new ServiceResponse();
            try
            {
                Inspection inspection = _repository.GetById(inspectionId);
                inspection.AppointmentPresentationId = appointmentPresentationId;
                _repository.Update(inspection);
            }
            catch (Exception ex)
            {
                response.Errors.Add("AddSetInspection", Messages.InspectionAddSetFailed);
            }
            response.IsSuccess = !response.Errors.Any();
            return response;
        }
        

        public ServiceResponse AddSetInspection(InspectionViewModel model)
        {
            ServiceResponse response = _repository.AddSetInspection(Mapper.Map(model));
            return response;
        }

        public ServiceResponse DeleteDamageIndication(long id)
        {
            _direpository.Delete(id);
            return _direpository.CommitChanges();
        }

        public DamageIndicationViewModel GetDamageIndication(long id)
        {
            var model = Mapper.Map(_direpository.FirstOrDefault(x => x.Id == id));
            return model;
        }

        public ServiceResponse SaveDamageIndication(DamageIndicationViewModel model)
        {
            var damageIndication = new DamageIndication();
            if (model.Id.HasValue)
            {
                damageIndication = Mapper.Map(model, _direpository.FirstOrDefault(x => x.Id == model.Id.Value));
                _direpository.Update(damageIndication);
            }
            else
            {
                damageIndication = Mapper.Map(model);
                _direpository.Insert(damageIndication);
            }

            var response = _direpository.CommitChanges();
            response.SavedItemId = damageIndication.Id;
            return response;
        }

        public void ResetInspection(long inspectionId)
        {
            _repository.ResetInspection(inspectionId);
        }

        public VehiclePhotoViewModel GetPhoto(int id)
        {
            var VehiclePhoto = Mapper.Map(_vprepository.FirstOrDefault(x => x.Id == id));
            return VehiclePhoto;
        }

        public ServiceResponse DeletePhoto(VehiclePhotoViewModel model)
        {
            _vprepository.Delete(Mapper.Map(model));
            var response = _vprepository.CommitChanges();
            return response;
        }

        public List<VehiclePhotoViewModel> GetAllPhotos(long inspectionId)
        {
            List<VehiclePhotoViewModel> vpList = new List<VehiclePhotoViewModel>();
            foreach (VehiclePhoto item in _vprepository.Find(x => x.InspectionId == inspectionId))
            {
                vpList.Add(Mapper.Map(item));
            }
            return vpList;
        }

        public ServiceResponse AddPhoto(VehiclePhotoViewModel photo)
        {
            var fileManager = IocManager.Resolve<IFileManager>();
            var appContext = IocManager.Resolve<IAppContext>();
            var tempFilePath = $@"{appContext.TempFolderName}\{photo.FileName}";
            var vehiclePhotosPath = $@"{appContext.VehiclePhotosFolder}\{appContext.CurrentIdentity.TenantId}\{photo.FileName}";
            fileManager.Move(tempFilePath, vehiclePhotosPath);
            photo.Url = photo.FileName;
            _vprepository.Insert(Mapper.Map(photo));
            var response = _vprepository.CommitChanges();
            return response;
        }

        public void SetAppointmentToInspection(long inspectionId, long appointmentPresentationId)
        {
            _repository.SetAppointmentToInspection(inspectionId, appointmentPresentationId);
        }
    }
}
