﻿using System;
using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Services.Mappers;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class AdditionalServicePartService : IAdditionalServicePartService
    {
        private readonly IAdditionalServicePartRepository _repository;
        public AdditionalServicePartService(IAdditionalServicePartRepository repository)
        {
            _repository = repository;
        }

        public AdditionalServiceViewModel GetViewModel(int? id = null)
        {
            return new AdditionalServiceViewModel();
        }

        public ServiceResponse Save(AdditionalServiceViewModel model)
        {
            //if (model.Id.HasValue)
            //{
            //    var entity = _repository.FirstOrDefault(x => x.Id == model.Id);
            //    entity = Mapper.Map(model, entity);
            //    _repository.Update(entity);
            //}
            //else
            //{
            //    var entity = Mapper.Map(model);
            //    _repository.Insert(entity);
            //}

           var response = _repository.CommitChanges();
           return response;
        }
    }
}
