﻿using System.Collections.Generic;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Core.Repositories;
using System.Linq;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class CountryService : ICountryService
    {
        private readonly ICountryRepository _countryRepository;
        private readonly ICountryStateRepository _countryStateRepository;

        public CountryService(ICountryRepository countryRepository, ICountryStateRepository countryStateRepository)
        {
            _countryRepository = countryRepository;
            _countryStateRepository= countryStateRepository;
        }

        public IEnumerable<CountryViewModel> GetCountries()
        {
            return _countryRepository.Find().Select(x => new CountryViewModel { Id = x.Id, Name = x.CountryName });
        }

        public IEnumerable<StateViewModel> GetStates(int? countryId = null)
        {
            var states = countryId.HasValue ? _countryStateRepository.Find(x => x.CountryId == countryId) : _countryStateRepository.Find();
            return states.Select(x => new StateViewModel { Id = x.Id, Name = x.CountryState1 });
        }
    }
}
