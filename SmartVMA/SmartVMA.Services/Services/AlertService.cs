﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Resources;
using SmartVMA.Services.Mappers;
using System.Collections.Generic;
using System.Linq;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class AlertService : IAlertService
    {
        private readonly IAlertRepository _repository;
        protected readonly IAppContext _appContext;

        public AlertService(IAlertRepository repository, IAppContext appContext)
        {
            _repository = repository;
            _appContext = appContext;
        }

        //not in use for now
        public List<AlertViewModel> GetAlerts()
        {
            var alerts = new List<AlertViewModel>();
            foreach (var item in _repository.Find())
            {
                alerts.Add(Mapper.Map(item));
            }
            return alerts;
        }

        public AlertIndexViewModel GetIndexViewModel()
        {
            var alerts = new List<AlertViewModel>();
            alerts.AddRange(_repository.Find().Select(x => Mapper.Map(x)));

            var model = new AlertIndexViewModel();
            model.Items = alerts;
            return model;
        }

        public AlertViewModel GetViewModel(int? id = null)
        {
            var model = new AlertViewModel();
            if (!id.HasValue)
            {
                return model;
            }

            var role = _repository.FirstOrDefault(x => x.Id == id.Value);
            model = Mapper.Map(role, model);
            return model;
        }
        //get the specific Alert
        public AlertViewModel GetAlert(int id)
        {
            var alert = _repository.FirstOrDefault(x => x.Id == id);
            var model = Mapper.Map(alert);

            return model;
        }

        //the same method is used for New and Edit
        public ServiceResponse Save(AlertViewModel model) 
        {
            if (!model.Title.Contains("script>"))
            {
                if (!model.Description.Contains("script>"))
                {
                    if (model.Id.HasValue) // if "model.Id.HasValue" => the item already exist in DB, so just Update it
                    {
                        var entity = Mapper.Map(model, _repository.FirstOrDefault(x => x.Id == model.Id.Value));
                        _repository.Update(entity);
                    }
                    else // if the Id is empty, then it's new item, so Insert a new one in the DB
                    {
                        var entity = Mapper.Map(model);
                        entity.CreatorUserId = _appContext.CurrentIdentity.UserId;
                        _repository.Insert(entity);
                    }

                    var response = _repository.CommitChanges(); // always do .CommitChanges() after, so it can save the changes in the DB 
                    return response;
                }
                else
                {
                    var response = new ServiceResponse();
                    response.Errors.Add("Description", Messages.InvalidText);
                    return response;
                }
            }
            else
            {
                var response = new ServiceResponse();
                response.Errors.Add("Title", Messages.InvalidText);
                return response;
            }
        }
       
        public ServiceResponse Delete(int alertId)
        {
            var alert = _repository.FirstOrDefault(x => x.Id == alertId); 
            _repository.Delete(alert);
            var response = _repository.CommitChanges(); // always do .CommitChanges() after, so it can save the changes in the DB 
            return response;
        }

        public AlertIndexViewModel GetActiveAlertsForCurrentUser()
        {
            var alerts = new List<AlertViewModel>();
            alerts.AddRange(_repository.GetAllActiveAlertsForCurrentUser().Select(x => Mapper.Map(x)));

            var model = new AlertIndexViewModel();
            model.Items = alerts;
            return model;
        }

        public AlertIndexViewModel GetPagedItems(PagingRequest request)
        {
            var entities = _repository.GetPagedResults(request);
            var items = new List<AlertViewModel>(entities.Select(x => Mapper.Map(x)));
            var model = new AlertIndexViewModel();
            model.Items = items;
            var total = _repository.GetPagedResultsTotal(request);
            model.ItemsCount = total;

            return model;
        }
    }
}

