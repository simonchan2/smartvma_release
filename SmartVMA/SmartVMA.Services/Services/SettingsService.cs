﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using SmartVMA.Core;
using SmartVMA.Core.Enums;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Helper;
using SmartVMA.Infrastructure.Parser;

namespace SmartVMA.Services
{
    public class SettingsService
    {
        public int GetSettingAsInteger(CompanySettingsNames settingName, int defaultValue = 0)
        {
            string value = SessionHelper.GetValue(StaticSettings.SessionPrefixCompanySetting + settingName);
            return Parser.ToInt(value, defaultValue);
        }
        public int GetSettingAsInteger(UserSettingsNames settingName, int defaultValue = 0)
        {
            string value = SessionHelper.GetValue(StaticSettings.SessionPrefixUserSettings + settingName);
            return Parser.ToInt(value, defaultValue);
        }
        public bool GetSettingAsBool(CompanySettingsNames settingName, bool defaultValue = false)
        {
            string value = SessionHelper.GetValue(StaticSettings.SessionPrefixCompanySetting + settingName);
            return Parser.ToBool(value, defaultValue);
        }
        public bool GetSettingAsBool(UserSettingsNames settingName, bool defaultValue = false)
        {
            string value = SessionHelper.GetValue(StaticSettings.SessionPrefixUserSettings + settingName);
            return Parser.ToBool(value, defaultValue);
        }
        public string GetSettingAsString(CompanySettingsNames settingName)
        {
            string value = SessionHelper.GetValue(StaticSettings.SessionPrefixCompanySetting + settingName);
            return value;
        }
        public string GetSettingAsString(UserSettingsNames settingName)
        {
            string value = SessionHelper.GetValue(StaticSettings.SessionPrefixUserSettings + settingName);
            return value;
        }
        public decimal GetSettingAsDecimal(CompanySettingsNames settingName, decimal defaultValue = 0)
        {
            string value = SessionHelper.GetValue(StaticSettings.SessionPrefixCompanySetting + settingName);
            return Parser.ToDecimal(value, defaultValue);
        }
        public decimal GetSettingAsDecimal(UserSettingsNames settingName, decimal defaultValue = 0)
        {
            string value = SessionHelper.GetValue(StaticSettings.SessionPrefixUserSettings + settingName);
            return Parser.ToDecimal(value, defaultValue);
        }
        public double GetSettingAsDouble(CompanySettingsNames settingName, double defaultValue = 0)
        {
            string value = SessionHelper.GetValue(StaticSettings.SessionPrefixCompanySetting + settingName);
            return Parser.ToDouble(value, defaultValue);
        }
        public double GetSettingAsDouble(UserSettingsNames settingName, double defaultValue = 0)
        {
            string value = SessionHelper.GetValue(StaticSettings.SessionPrefixUserSettings + settingName);
            return Parser.ToDouble(value, defaultValue);
        }
    }
}
