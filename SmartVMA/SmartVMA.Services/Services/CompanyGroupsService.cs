﻿using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Services.Mappers;
using System.Collections.Generic;
using System.Linq;
using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using System;
using SmartVMA.Core.InputModels.PageRequests;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class CompanyGroupsService : ICompanyGroupsService
    {
        private readonly ICompanyGroupRepository _repository;
        public CompanyGroupsService(ICompanyGroupRepository repository)
        {
            _repository = repository;
        }

        public CompanyGroupViewModel CompanyGroupViewModel(int? id = null)
        {
            var model = new CompanyGroupViewModel();
            if (id.HasValue)
            {
                var companyGroup = _repository.GetAllDealerGroups().FirstOrDefault(x => x.Id == id);
                model = Mapper.Map(companyGroup, model);
            }
            return model;
        }

        public ServiceResponse Delete(CompanyGroupViewModel model)
        {
            var companyGroup = _repository.FirstOrDefault(x => x.Id == model.Id);
            _repository.Delete(companyGroup);

            var response = _repository.CommitChanges();
            return response;
        }

        public CompanyGroupsIndexViewModel GetDealerGroups()
        {
            var items = new List<CompanyGroupViewModel>(_repository.GetAllDealerGroups().Select(x => Mapper.Map(x)));
            var model = new CompanyGroupsIndexViewModel { Items = items };
            return model;
        }

        public ServiceResponse Save(CompanyGroupViewModel model)
        {
            var dealers = Mapper.Map(model);
            if (model.Id != 0)
            {
                var companyGroup = _repository.GetAllDealerGroups().FirstOrDefault(x => x.Id == model.Id);
                _repository.Update(Mapper.Map(model, companyGroup), dealers);
            }
            else
            {
                _repository.Insert(Mapper.Map(model, new CompanyGroup()), dealers);
            }
            var response = _repository.CommitChanges();
            return response;
        }

        public CompanyGroupsIndexViewModel GetPagedItems(PagingRequest request)
        {
            var entities = _repository.GetPagedResults(request);
            var items = new List<CompanyGroupViewModel>(entities.Select(x => Mapper.Map(x)));
            var model = new CompanyGroupsIndexViewModel();
            model.Items = items;
            var total = _repository.GetPagedResultsTotal(request);
            model.ItemsCount = total;

            return model;
        }
    }
}
