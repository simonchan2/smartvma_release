﻿using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Services.Mappers;
using System.Collections.Generic;
using System.Linq;
using System;
using SmartVMA.Core.InputModels.PageRequests;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Web;
using SmartVMA.Core;
using SmartVMA.Core.Entities;
using SmartVMA.Infrastructure.Comparer;

namespace SmartVMA.Services
{
    [IocBindable(ScopeType = InstanceScopeType.PerRequest)]
    internal class CompanyService : SettingsService, ICompanyService
    {
        private readonly IAppContext _appContext;
        private readonly ICompanyRepository _repository;
        private readonly ICompanySettingRepository _settingRepository;

        public CompanyService(ICompanyRepository repository, IAppContext appContext, ICompanySettingRepository settingRepository)
        {
            _repository = repository;
            _appContext = appContext;
            _settingRepository = settingRepository;
        }

        public CompanyIndexViewModel GetIndexViewModel()
        {
            var model = CreateIndexModel();
            model.Items = new List<CompanyViewModel>(_repository.GetCompanies().Select(x => Mapper.Map(x, new CompanyViewModel())));
            return model;
        }

        public int? GetCompanyCountryId(int companyId)
        {
            var company = _repository.GetCompanyAndSettings(companyId);
            if(company != null)
            {
                return company.CountryId;
            }

            return null;
        }

        public CompanyIndexViewModel GetPagedCompanies(PagingRequest request)
        {
            var model = CreateIndexModel();
            model.Items = new List<CompanyViewModel>(_repository.GetPagedResults(request).Select(x => Mapper.Map(x, new CompanyViewModel(), true)));
            model.ItemsCount = _repository.GetPagedResultsTotal(request);
            return model;
        }

        public CompanyIndexViewModel GetDistributors()
        {
            var model = CreateIndexModel();
            model.Items = new List<CompanyViewModel>(_repository.GetParentCompanies(x => x.CompanyTypeId == (int)CompanyTypes.Distributor).Select(x => Mapper.Map(x, new CompanyViewModel())));
            return model;
        }

        public CompanyIndexViewModel GetPagedDistributors(PagingRequest request)
        {
            Expression<Func<Company, bool>> expression = x => x.CompanyTypeId == (int)CompanyTypes.Distributor;

            var items = _repository.GetPagedResults(request, expression);

            var model = CreateIndexModel();
            model.Items = new List<CompanyViewModel>(items.Select(x => Mapper.Map(x, new CompanyViewModel())));
            model.ItemsCount = _repository.GetPagedResultsTotal(request, expression);

            return model;
        }

        public CompanyIndexViewModel GetDealers(int? distributorId = null)
        {
            var items = _repository.GetCompanies(x => x.CompanyTypeId == (int)CompanyTypes.Dealer);
            if (distributorId.HasValue)
            {
                items = items.Where(x => x.ParentId == distributorId);
            }

            var model = CreateIndexModel();
            model.Items = new List<CompanyViewModel>(items.Select(x => Mapper.Map(x, new CompanyViewModel())));

            return model;
        }

        public CompanyIndexViewModel GetPagedDealers(PagingRequest request, int? distributorId = null)
        {
            Expression<Func<Company, bool>> expression = null;
            if (distributorId.HasValue)
                expression = x => x.CompanyTypeId == (int)CompanyTypes.Dealer && x.ParentId == distributorId;
            else
                expression = x => x.CompanyTypeId == (int)CompanyTypes.Dealer;

            var items = _repository.GetPagedResults(request, expression);

            var model = CreateIndexModel();
            model.Items = new List<CompanyViewModel>(items.Select(x => Mapper.Map(x, new CompanyViewModel())));
            model.ItemsCount = _repository.GetPagedResultsTotal(request, expression);

            return model;
        }

        public CompanyIndexViewModel GetCorporations()
        {
            var model = CreateIndexModel();
            model.Items = new List<CompanyViewModel>(_repository.GetParentCompanies(x => x.CompanyTypeId == (int)CompanyTypes.Corporation).Select(x => Mapper.Map(x, new CompanyViewModel())));
            return model;
        }

        public IEnumerable<CompanyHierarchialViewModel> GetParentCompaniesNames(int? id = null)
        {
            var result = new List<CompanyHierarchialViewModel>();
            if (!id.HasValue)
            {
                return result;
            }

            var counter = 1;
            var company = _repository.GetCompanyAndParents(id.Value);
            var parentCompany = company.ParentCompany;
            result.Add(new CompanyHierarchialViewModel { Name = company.CompanyName, Order = counter++ });
            while (parentCompany != null)
            {
                result.Add(new CompanyHierarchialViewModel { Name = parentCompany.CompanyName, Order = counter++ });
                parentCompany = parentCompany.ParentCompany;
            }

            return result;
        }

        private CompanyIndexViewModel CreateIndexModel()
        {
            var currentIdentity = _appContext.CurrentIdentity;
            var model = new CompanyIndexViewModel
            {
                CurentTenantId = currentIdentity.TenantId ?? 0,
                IsCurrentUserDistributorAdmin = (currentIdentity.RoleId ?? 0) == (int)UserRoles.DistributorAdmin
            };
            return model;
        }

        private CompanyViewModel CreateModel()
        {
            var currentIdentity = _appContext.CurrentIdentity;
            var model = new CompanyViewModel
            {
                CurentTenantId = currentIdentity.TenantId ?? 0,
                IsCurrentUserDistributorAdmin = (currentIdentity.RoleId ?? 0) == (int)UserRoles.DistributorAdmin
            };
            return model;
        }

        public List<SettingViewModel> GetAllSettings()
        {
            List<SettingViewModel> companySettings = new List<SettingViewModel>(_settingRepository.GetAllSettings().Select(x => Mapper.Map(x, new SettingViewModel())));
            return companySettings;
        }

        public string GetDateTimeFormat(CompanySettingsNames setting)
        {
            var result = "dd-MM-yyyy"; // set some default value first
            int dateFormatInt = GetSettingAsInteger(setting); // get the id from DB
            if (dateFormatInt > 0)
            {
                string dateFormatName = Enum.GetName(typeof(DateFormat), dateFormatInt); // get the value form enum
                var dateTimeMap = new DateFormatMap();
                result = dateTimeMap.DateFormatMapDictionary[dateFormatName]; // get the correct date format as string
            }
            return result;
        }

        public CompanyIndexViewModel GetCompaniesForUser(string email)
        {
            var model = CreateIndexModel();
            model.Items = new List<CompanyViewModel>(_repository.GetCompaniesForUser(email).Select(x => Mapper.Map(x, new CompanyViewModel())));
            return model;
        }

        public string GetCompanyLogo(CompanySettingsNames setting)
        {
            var appContext = IocManager.Resolve<IAppContext>();
            string result = string.Empty;
            string settingValue = GetSettingAsString(setting);
            if (!string.IsNullOrEmpty(settingValue))
            {
                result = appContext.MapUrl($@"{appContext.CompanyContentFolder}/{appContext.CurrentIdentity.TenantId}/{settingValue}");
            }
            return result;
        }

        public string GetCompanyLogoPath(CompanySettingsNames setting)
        {
            var appContext = IocManager.Resolve<IAppContext>();
            string result = string.Empty;
            string settingValue = GetSettingAsString(setting);
            if (!string.IsNullOrEmpty(settingValue))
            {
                result = $@"{appContext.CompanyContentFolder}/{appContext.CurrentIdentity.TenantId}/{settingValue}";
            }
            return result;
        }

        public string GetDefaultCompanySetting(CompanySettingsNames setting)
        {
            var settingValue = _settingRepository.GetDefaultSettingValue(setting);
            return settingValue;
        }

        public string GetCompanySettingValue(CompanySettingsNames setting)
        {
            var settingValue = _settingRepository.GetSettingValue(setting);
            return settingValue;
        }

        public IEnumerable<LOFMenu> GetLOFMenuLevels(int id)
        {
            var result = new List<LOFMenu>();
            var repository = IocManager.Resolve<ICompanyRepository>();
            var company = repository.GetCompanyAndSettings(id);

            if (company.CompanySettings.FirstOrDefault(x => x.Name == "LOFMenuLevels") != null)
            {
                var lofMenuLevels = company.CompanySettings.FirstOrDefault(x => x.Name == "LOFMenuLevels").Value;

                if (Convert.ToInt32(lofMenuLevels) > 0)
                {
                    if (company.CompanySettings.FirstOrDefault(x => x.Name == "LOFMenuLabelLevel1") != null)
                    {
                        var lofMenu = new LOFMenu();
                        lofMenu.Name = company.CompanySettings.FirstOrDefault(x => x.Name == "LOFMenuLabelLevel1").Value;
                        lofMenu.Level = 1;
                        result.Add(lofMenu);
                    }

                    if (company.CompanySettings.FirstOrDefault(x => x.Name == "LOFMenuLabelLevel2") != null)
                    {
                        var lofMenu = new LOFMenu();
                        lofMenu.Name = company.CompanySettings.FirstOrDefault(x => x.Name == "LOFMenuLabelLevel2").Value;
                        lofMenu.Level = 2;
                        result.Add(lofMenu);
                    }

                    if (company.CompanySettings.FirstOrDefault(x => x.Name == "LOFMenuLabelLevel3") != null)
                    {
                        var lofMenu = new LOFMenu();
                        lofMenu.Name = company.CompanySettings.FirstOrDefault(x => x.Name == "LOFMenuLabelLevel3").Value;
                        lofMenu.Level = 3;
                        result.Add(lofMenu);
                    }

                }
            }
            return result;
        }

        public IEnumerable<string> GetMenuButtonsOrder()
        {
            var comparer = new AlphaNumericStringComparer();
            var roleId = _appContext.CurrentIdentity.RoleId ?? 0;

            var result = new List<string>
            {
                $"{GetSettingAsString(CompanySettingsNames.ReportingButtonOrder)}Reporting"
            };

            if (roleId == (int)UserRoles.MenuAdvisor || roleId == (int)UserRoles.Manager)
            {
                result.Add($"{GetSettingAsString(CompanySettingsNames.AppointmentsButtonOrder)}Appointments");
                result.Add($"{GetSettingAsString(CompanySettingsNames.DocumentSearchButtonOrder)}PdfDocuments");
            }
            if (roleId == (int)UserRoles.MenuAdvisor)
            {
                result.Add($"{GetSettingAsString(CompanySettingsNames.PlannerButtonOrder)}Planner");
                result.Add($"{GetSettingAsString(CompanySettingsNames.MpiButtonOrder)}MPI");
                result.Add($"{GetSettingAsString(CompanySettingsNames.RoPrepButtonOrder)}RoPrep");
            }
            return result.OrderBy(x => x, comparer).Select(x => Regex.IsMatch(x, @"^\d") ? x.Substring(1) : x);
        }
    }
}
