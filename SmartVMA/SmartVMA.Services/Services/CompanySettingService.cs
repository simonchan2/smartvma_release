﻿using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SmartVMA.Services
{
    [IocBindable(ScopeType = InstanceScopeType.PerRequest)]
    internal class CompanySettingService : ICompanySettingService
    {
        public IEnumerable<LanguageViewModel> GetLanguages()
        {
            //var result = new List<LanguageViewModel>
            //{
            //    new LanguageViewModel { Id = ((int)Languages.English), LanguageName = "English" },
            //    new LanguageViewModel { Id = ((int)Languages.French), LanguageName = "Français" },
            //    new LanguageViewModel { Id = ((int)Languages.Spanish), LanguageName = "Español" }
            //};
            //return result;
            var repository = IocManager.Resolve<ILanguageRepository>();
            return repository.Find().Select(x => new LanguageViewModel { Id = x.Id, LanguageName = x.Description });
        }

        public IEnumerable<DateFormatViewModel> GetDateFormats()
        {
            var result = new List<DateFormatViewModel>();
            var dateTimeMap = new DateFormatMap();
            foreach (DateFormat item in Enum.GetValues(typeof(DateFormat)))
            {
               result.Add(new DateFormatViewModel { Id = (int)item, Format = dateTimeMap.DateFormatMapDictionary[item.ToString()] });
            }            
            return result;
        }

        public IEnumerable<MeasurementUnitViewModel> GetMeasurementUnits()
        {
            var repository = IocManager.Resolve<IMeasurementUnitRepository>();
            return repository.Find().Select(x => new MeasurementUnitViewModel { Id = x.Id, Description = x.Description });
        }

        public string GetSetting(CompanySettingsNames setting)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<LanguageViewModel> GetLanguagesCode()
        {
            var repository = IocManager.Resolve<ILanguageRepository>();
            return repository.Find().Select(x => new LanguageViewModel { Id = x.Id, LanguageCode = x.LanguageCode });
        }
        
    }
}
