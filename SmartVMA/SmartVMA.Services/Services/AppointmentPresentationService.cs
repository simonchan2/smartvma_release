﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.OutputModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Resources;
using SmartVMA.Services.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class AppointmentPresentationService : IAppointmentPresentationService
    {
        private readonly IAppointmentPresentationRepository _repository;
        private readonly IAppContext _appContext;
        private readonly ICompanyService _companyService;

        public AppointmentPresentationService(IAppointmentPresentationRepository repository, IAppContext appContext, ICompanyService companyService)
        {
            _repository = repository;
            _appContext = appContext;
            _companyService = companyService;
        }

        //get default model for the advisor home page menus
        public AdvisorMenusViewModel GetDefaultAdvisorMenus(DateTime? startDate = null, DateTime? endDate = null, int? timeFrameId = null)
        {
            AdvisorMenusViewModel homeModel = new AdvisorMenusViewModel();
            homeModel.ParkedMenu = new ParkedMenu()
            {
                TimeFrameId = timeFrameId.HasValue ? timeFrameId.Value : (int)TimeFrames.All,
                AdvisorIds = new List<long>()
                {
                   _appContext.CurrentIdentity.UserId.Value
                },
                Items = ListParkedMenus(startDate, endDate, advisorIds: new List<long>()
                {
                   _appContext.CurrentIdentity.UserId.Value
                })
            };
            homeModel.AcceptedMenu = new AcceptedMenu()
            {
                TimeFrameId = timeFrameId.HasValue ? timeFrameId.Value : (int)TimeFrames.All,
                AdvisorIds = new List<long>()
                {
                   _appContext.CurrentIdentity.UserId.Value
                },
                Items = ListAcceptedMenus(startDate, endDate, advisorIds: new List<long>()
                {
                   _appContext.CurrentIdentity.UserId.Value
                })
            };
            homeModel.SearchCustomer = new SearchCustomerViewModel();
            homeModel.VehicleLookup = new VehicleFilterViewModel();

            return homeModel;

        }

        //get parked menus filtered by date and advisor list
        public AppointmentPresentationIndexViewModel ListParkedMenus(DateTime? startDate = null, DateTime? endDate = null, List<long> advisorIds = null, int? timeframeId = null)
        {
            AppointmentPresentationIndexViewModel parkedMenus = GetFullAppointmentPresentationList(AppointmentPresentationStatus.MK, startDate, endDate, advisorIds, timeframeId);
            if (parkedMenus.Items != null && parkedMenus.Items.Count() > 0)
            {
                parkedMenus.Items = parkedMenus.Items.OrderBy(c => c.ParkedDate);
            }

            return parkedMenus;
        }

        //get accepted menus filtered by date and advisor list
        public AppointmentPresentationIndexViewModel ListAcceptedMenus(DateTime? startDate = null, DateTime? endDate = null, List<long> advisorIds = null, int? timeframeId = null)
        {
            AppointmentPresentationIndexViewModel acceptedMenus = GetFullAppointmentPresentationList(AppointmentPresentationStatus.MC, startDate, endDate, advisorIds, timeframeId);
            if (acceptedMenus.Items != null && acceptedMenus.Items.Count() > 0)
            {
                acceptedMenus.Items = acceptedMenus.Items.OrderBy(x => x.AcceptedDate);
            }

            return acceptedMenus;
        }

        //get advisor list
        public List<AdvisorInfo> GetAdvisorList()
        {
            return _repository.GetAdvisorList();
        }

        //return appointment presentation ID from the vehicle lookup
        public long? GetAppointmentPresentationId(int year, int make, int model, int engine, string transmission, string driveLine, int mileage)
        {
            return _repository.GetAppointmentPresentationId(year, make, model, engine, transmission, driveLine, mileage);
        }

        public string GetAppointmentPresentationStatus(long appointmentPresentationId)
        {
            var appointmentPresentation = _repository.FirstOrDefault(x => x.Id == appointmentPresentationId);

            return appointmentPresentation != null ? appointmentPresentation.PresentationStatus : null;
        }

        public long GetAppointmentPresentationId(long appointmentId)
        {
            var appointmentPresentation = _repository.FirstOrDefault(x => x.AppointmentId == appointmentId);

            return appointmentPresentation != null ? appointmentPresentation.Id : -1;
        }

        public string GetAdvisorCode(long appointmentPresentationId)
        {
            var appointmentPresentation = _repository.FirstOrDefault(x => x.Id == appointmentPresentationId);

            var advisorCode = string.Empty;

            if (appointmentPresentation != null && appointmentPresentation.AppointmentId.HasValue)
            {
                var _appointmentRepository = IocManager.Resolve<IAppointmentRepository>();
                var appointment = _appointmentRepository.FirstOrDefault(x => x.Id == appointmentPresentation.AppointmentId.Value);
                if (appointment != null)
                    advisorCode = appointment.AdvCode;
            }

            return advisorCode;
        }

        private AppointmentPresentationIndexViewModel GetFullAppointmentPresentationList(AppointmentPresentationStatus statusType, DateTime? startDate, DateTime? endDate, List<long> advisorIds = null, int? timeframeId = null)
        {
            var advisorMenus = new List<AppointmentPresentationViewModel>();
            var _companyRepository = IocManager.Resolve<ICompanyRepository>();
            var commpanySettings = _companyRepository.GetCompanyAndSettings(_appContext.CurrentIdentity.TenantId.Value);
            var parkedMenuRetentionDurationSetting = commpanySettings.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.ParkedMenuRetentionDuration.ToString());
            var parkedMenuRetentionDurationInt = parkedMenuRetentionDurationSetting != null ? Convert.ToInt32(parkedMenuRetentionDurationSetting.Value) : 3;
            DateTime? parkedMenuRetentionDuration = DateTime.Now.AddDays(parkedMenuRetentionDurationInt * -1);
            var appointmentPresentationList = _repository.GetFullAppointmentPresentationList(statusType, startDate, endDate, parkedMenuRetentionDuration, advisorIds, timeframeId);
            if (appointmentPresentationList != null)
            {
                foreach (var item in appointmentPresentationList)
                {
                    AppointmentPresentationViewModel appInfo = Mapper.Map(item, new AppointmentPresentationViewModel());
                    if (appInfo.CustomerInfo != null)
                    {
                        appInfo.CustomerInfo.FullName = !string.IsNullOrEmpty(appInfo.CustomerInfo.FirstName) || !string.IsNullOrEmpty(appInfo.CustomerInfo.LastName) ? string.Format("{0} {1}", appInfo.CustomerInfo.FirstName, appInfo.CustomerInfo.LastName) : appInfo.CustomerInfo.CompanyName;
                        appInfo.CustomerInfo.CarFullName = string.Format("{0} {1} {2}", appInfo.CustomerInfo.Year, appInfo.CustomerInfo.Make, appInfo.CustomerInfo.Model);
                    }
                    advisorMenus.Add(appInfo);
                }
            }

            var model = new AppointmentPresentationIndexViewModel { Items = advisorMenus };
            return model;
        }

        public ServiceResponse Save(SaveConfirmServiceRequestModel request, bool isLofMenu)
        {
            var response = new ServiceResponse();
            if (!request.DealerCustomerId.HasValue && !request.IsParked && !request.IsDeclined)
            {
                response.Errors.Add("DealerCustomerId", Messages.ConfirmServiceDealerCustomerMustBeSet);
                return response;
            }

            // services
            var engineOilORuleService = IocManager.Resolve<IEngineOilORuleService>();

            // repositories
            var dealerVehicleRepository = IocManager.Resolve<IDealerVehicleRepository>();
            var generalServiceRepository = IocManager.Resolve<IGeneralCarServiceRepository>();
            var appointmentServiceRepository = IocManager.Resolve<IAppointmentServiceRepository>();
            var appointmentServicePartRepository = IocManager.Resolve<IAppointmentServicePartRepository>();
            var oemServicePartsRepository = IocManager.Resolve<IOemServicePartRepository>();
            var oemPartsRepository = IocManager.Resolve<IOemPartRepository>();
            var oemPartNumbersRepository = IocManager.Resolve<IOemPartNumberRepository>();
            var additionalServicePartRepository = IocManager.Resolve<IAdditionalServicePartRepository>();
            var _vinmasterTransmissionRepository = IocManager.Resolve<IVinMasterTransmissionRepository>();
            var _vinmasterDrivelineRepository = IocManager.Resolve<IVinMasterDrivelineRepository>();
            var _vehicleRepository = IocManager.Resolve<IVehicleRepository>();

            // variables
            var dealerVehicle = dealerVehicleRepository.GetDealerVehicle(request.VIN, request.CarId, request.DealerCustomerId, request.AppointmentPresentationId);
            var measurementId = _companyService.GetSettingAsInteger(CompanySettingsNames.MeasurementUnit);
            var laborRate = _companyService.GetSettingAsDecimal(CompanySettingsNames.DefaultLaborRate);
            var menuLevel = !isLofMenu ? request.MenuLevel : request.Services != null && request.Services.Any() ? request.Services.Max(x => x.MenuLevel) : 1;
            AppointmentPresentation appointmentPresentation;
            var carRepository = IocManager.Resolve<ICarRepository>();
            var car = carRepository.GetById(request.CarId);
            if (dealerVehicle == null)
            {
                dealerVehicle = Mapper.Map(car, request.DealerCustomerId, request.VIN);

                dealerVehicleRepository.Insert(dealerVehicle);
            }
            Vehicle vehicle = dealerVehicle.Vehicle != null && request.DealerCustomerId != null ? dealerVehicle.Vehicle : !string.IsNullOrEmpty(request.VIN) ? _vehicleRepository.FirstOrDefault(c => c.VIN != null && c.VIN.Trim() == request.VIN.Trim()) : null;
            string vinShort = !string.IsNullOrEmpty(request.VIN) ? request.VIN.Trim().Substring(0, 8) + request.VIN.Trim().Substring(9, 2) : null;
            var carVinShort = !string.IsNullOrEmpty(vinShort) ? car.CarVinShorts.FirstOrDefault(c => c.VinShort != null && c.VinShort == vinShort) : null;
            if (vehicle != null || !string.IsNullOrEmpty(request.VIN))
            {
                vehicle = Mapper.Map(request.CarId, request.VIN, vehicle, vehicle != null ? vehicle.VinMasterId == null ? (carVinShort != null ? carVinShort.VinMasterId : vehicle.VinMasterId) : vehicle.VinMasterId : carVinShort != null ? carVinShort.VinMasterId : (int?)null);
                if (vehicle.Id == 0)
                {
                    _vehicleRepository.Insert(vehicle);
                }
            }
            dealerVehicle.VIN = vehicle != null ? vehicle.VIN : null;
            dealerVehicle.Vehicle = vehicle;

            string originalStatus = string.Empty;
            if (!request.AppointmentPresentationId.HasValue)
            {
                appointmentPresentation = Mapper.Map(request, dealerVehicle, _appContext.CurrentIdentity.UserId, isLofMenu, measurementId, menuLevel);
            }
            else
            {
                appointmentPresentation = _repository.GetAppointmentWithServicesAndPartsById(request.AppointmentPresentationId.Value);
                originalStatus = appointmentPresentation.PresentationStatus;
                if (originalStatus == AppointmentPresentationStatus.MK.ToString() && isLofMenu)
                {
                    appointmentPresentation = Mapper.Map(request, dealerVehicle, _appContext.CurrentIdentity.UserId, isLofMenu, measurementId, menuLevel);
                }
                else
                {
                    appointmentPresentation = Mapper.Map(request, isLofMenu, menuLevel, appointmentPresentation);
                }
            }
            if (dealerVehicle != null)
            {
                if (request.Transmission != 0)
                {
                    string transmission = _vinmasterTransmissionRepository.GetVinMasterTransmission(request.Transmission);
                    dealerVehicle.VehicleTransmission = transmission;
                    dealerVehicle.VinMasterTransmissionId = request.Transmission;
                }
                if (request.Driveline != 0)
                {
                    string driveline = _vinmasterDrivelineRepository.GetVinMasterDriveline(request.Driveline);
                    dealerVehicle.VehicleDriveline = driveline;
                    dealerVehicle.VinMasterDrivelineId = request.Driveline;
                }
                if (dealerVehicle.Vehicle != null && (dealerVehicle.Vehicle.CarIdUndetermined ?? true) == true)
                {
                    dealerVehicle.Vehicle.VehicleStatus = 0;
                }
                if (dealerVehicle.CarId == null || dealerVehicle.CarId != request.CarId)
                {
                    dealerVehicle.CarId = request.CarId;
                }
            }

            // get info for the services
            var services = !isLofMenu && !string.IsNullOrEmpty(originalStatus) && originalStatus == AppointmentPresentationStatus.MK.ToString() ?
                appointmentPresentation.AppointmentServices.ToList() : generalServiceRepository
                             .GetAllServices(request.CarId, mileage: request.Mileage, serviceIds: (appointmentPresentation.PresentationStatus == AppointmentPresentationStatus.MK.ToString() ? request.AllServices : request.Services), isLof: isLofMenu, dealerCustomerId: request.DealerCustomerId, appointmentPresentationId: (!string.IsNullOrEmpty(originalStatus) && originalStatus == AppointmentPresentationStatus.MK.ToString() && isLofMenu ? (long?)null : request.AppointmentPresentationId))
                             .Select(x => Mapper.Map(x, new AppointmentService())).ToList();

            #region remove services and parts from current parked menu
            var serviceToBeRemoved = services.Where(p => !request.Services.Where(c => !c.IsAddedNow).Any(c => c.Id == p.OrigServiceId));

            foreach (var item in serviceToBeRemoved)
            {
                var appointmentService = appointmentPresentation.AppointmentServices.FirstOrDefault(x => x.ServiceTypeId == item.ServiceTypeId && x.OrigServiceId == item.OrigServiceId);
                if (appointmentService != null)
                {
                    List<AppointmentServicePart> allServiceParts = appointmentService.AppointmentServiceParts.ToList();
                    foreach (var servicePart in allServiceParts)
                    {
                        appointmentServicePartRepository.Delete(servicePart);
                    }
                    appointmentServiceRepository.Delete(appointmentService);
                }
            }
            #endregion remove services and parts from current parked menu
            foreach (var item in (appointmentPresentation.PresentationStatus == AppointmentPresentationStatus.MK.ToString() ? request.AllServices.GroupBy(c => c.Id).Select(c => c.FirstOrDefault()) : request.Services.Distinct()))
            {
                AppointmentService appointmentService = null;
                if (!item.IsAddedNow)
                {
                    appointmentService = appointmentPresentation.AppointmentServices
                    .FirstOrDefault(x => x.ServiceTypeId == (int)item.ServiceTypeId && x.OrigServiceId == item.Id);
                    if (appointmentService != null && appointmentService.IsStriked != item.IsStrikeOut)
                    {
                        appointmentService.IsStriked = item.IsStrikeOut;
                    }
                }

                if (appointmentService == null)
                {
                    appointmentService = Mapper.MapAppointment(item, appointmentService);
                    appointmentService.AppointmentPresentation = appointmentPresentation;
                }

                if (appointmentService.Id == 0)
                {
                    appointmentServiceRepository.Insert(appointmentService);
                    if (appointmentService.ServiceTypeId == (int)ServiceType.OemBasicService)
                    {
                        var oemServiceParts = oemServicePartsRepository.Find(x => x.OemBasicServiceId == appointmentService.OrigServiceId && x.IsDeleted == false).ToList();
                        foreach (var servicePart in oemServiceParts)
                        {
                            var part = oemPartsRepository.FirstOrDefault(x => x.Id == servicePart.Id);
                            var partNumbers = oemPartNumbersRepository.GetOverridenPartNumbersByPartId(_appContext.CurrentIdentity.TenantId.Value, servicePart.OemPartId);
                            OemPartNumber partNumber = null;
                            if (partNumbers != null && partNumbers.Count > 0)
                            {
                                if (partNumbers.Count > 1)
                                {
                                    if (_appContext.CurrentIdentity.TenantId.HasValue)
                                    {
                                        var countryId = _companyService.GetCompanyCountryId(_appContext.CurrentIdentity.TenantId.Value);
                                        partNumber = countryId.HasValue ? partNumbers.FirstOrDefault(x => x.CountryId == countryId.Value) : partNumbers.First();
                                    }
                                    else
                                    {
                                        partNumber = partNumbers.First();
                                    }
                                }
                                else
                                {
                                    partNumber = partNumbers.First();
                                }
                            }
                            EngineOilConfigurationViewModel engineOilOverride = null;
                            if (part != null)
                            {
                                servicePart.OemPart = part;
                                engineOilOverride = engineOilORuleService.GetEngineOilOverride(part.FluidType, part.FluidViscosity);
                            }

                            appointmentServicePartRepository.Insert(Mapper.Map(appointmentService, servicePart, new AppointmentServicePart(), engineOilOverride, partNumber));
                        }
                    }
                    else if (appointmentService.ServiceTypeId == (int)ServiceType.AdditionalService)
                    {
                        var additionServiceParts = additionalServicePartRepository.Find(x => x.AdditionalServiceId == appointmentService.OrigServiceId).ToList();
                        foreach (var part in additionServiceParts)
                        {
                            appointmentServicePartRepository.Insert(Mapper.Map(appointmentService, part, new AppointmentServicePart()));
                        }
                    }
                }
                else
                {
                    appointmentServiceRepository.Update(appointmentService);
                }
            }

            appointmentPresentation = UpdateAppointmentPriceCharges(appointmentPresentation);
            if (appointmentPresentation.Id == 0)
            {
                _repository.Insert(appointmentPresentation);
            }
            else
            {
                _repository.Update(appointmentPresentation);
            }
            response = _repository.CommitChanges();
            if (response.IsSuccess)
            {
                foreach (var service in appointmentPresentation.AppointmentServices.Where(c => c.ServiceTypeId == (int)ServiceType.AdHocAdditionalService))
                {
                    service.OrigServiceId = service.Id;
                }
                response = _repository.CommitChanges();
                response.SavedItemId = appointmentPresentation.Id;
            }
            return response;
        }

        public ServiceResponse DeleteParkedMenu(long? appointmentPresentationId)
        {
            if (appointmentPresentationId.HasValue)
            {
                var entity = _repository.Find(x => x.Id == appointmentPresentationId).FirstOrDefault();
                entity.PresentationStatus = AppointmentPresentationStatus.PD.ToString();
                _repository.Update(entity);
                var response = _repository.CommitChanges();
                return response;
            }
            return null;
        }

        //public ServiceResponse ReopenMenu(long appointmentPresentationId)
        //{
        //    var entity = _repository.Find(x => x.Id == appointmentPresentationId).FirstOrDefault();
        //    entity.PresentationStatus = AppointmentPresentationStatus.M.ToString();
        //    _repository.Update(entity);
        //    var response = _repository.CommitChanges();
        //    return response;
        //}

        public AppointmentPresentationViewModel ReopenMenu(long appointmentPresentationId)
        {
            var entity = _repository.GetAppointmentPresentationWithVehiclesById(appointmentPresentationId);
            entity.PresentationStatus = AppointmentPresentationStatus.M.ToString();
            _repository.Update(entity);
            _repository.CommitChanges();
            AppointmentPresentationViewModel appInfo = Mapper.Map(entity, new AppointmentPresentationViewModel());
            if (appInfo.CustomerInfo != null)
            {
                appInfo.CustomerInfo.FullName = !string.IsNullOrEmpty(appInfo.CustomerInfo.FirstName) || !string.IsNullOrEmpty(appInfo.CustomerInfo.LastName) ? string.Format("{0} {1}", appInfo.CustomerInfo.FirstName, appInfo.CustomerInfo.LastName) : appInfo.CustomerInfo.CompanyName;
                appInfo.CustomerInfo.CarFullName = string.Format("{0} {1} {2}", appInfo.CustomerInfo.Year, appInfo.CustomerInfo.Make, appInfo.CustomerInfo.Model);
            }
            return appInfo;
        }

        public ServiceResponse Create(int customerId, int dealerVehicleId)
        {
            AppointmentPresentation app = new AppointmentPresentation();
            app.AdvisorId = (long)_appContext.CurrentIdentity.UserId;
            app.DealerCustomerId = customerId;
            app.DealerVehicleId = dealerVehicleId;
            app.PresentationStatus = AppointmentPresentationStatus.M.ToString();
            app.Mileage = 0;  //TODO
            app.MeasurementUnitId = 0;  //TODO
            app.AppointmentTime = DateTime.Now;
            _repository.Insert(app);
            var response = _repository.CommitChanges();
            response.SavedItemId = app.Id;
            return response;
        }

        public AppoitmentPresentationViewCountsModel IsExistingAppointmentPresentationByDateUserVehicle(long? appointmentPresentationId, DateTime date, int? userId, int? dealerVehicleId, string appointmentPresentationStatus)
        {
            AppoitmentPresentationViewCountsModel model = new AppoitmentPresentationViewCountsModel();

            if (appointmentPresentationId != null)
            {
                if (appointmentPresentationStatus == AppointmentPresentationStatus.MK.ToString())
                {
                    model.NewId = false;
                    model.AppointmentId = (long)appointmentPresentationId;
                }
                else
                {
                    List<AppointmentPresentation> menupresented = _repository.Find(x => x.DealerVehicleId == dealerVehicleId
                                                                                && x.AdvisorId == userId
                                                                                && x.PresentationStatus == AppointmentPresentationStatus.M.ToString()).
                                                                                OrderByDescending(x => x.Id).ToList();
                    if (menupresented.Count() != 0)
                    {
                        if (menupresented[0].TimeCreated.Date <= date.Date)
                        {
                            model.NewId = false;
                            model.AppointmentId = menupresented[0].Id;
                        }
                        else
                        {
                            model.NewId = true;
                        }
                    }
                    else
                    {
                        model.NewId = true;
                    }
                }
            }
            else
            {
                model.NewId = true;
            }
            return model;
        }

        private double CalculateShopChargeAmount(double price, double minCharge, double maxCharge)
        {
            if (price < minCharge)
            {
                return minCharge;
            }
            return price > maxCharge ? maxCharge : price;
        }

        public AppointmentPresentation UpdateAppointmentPriceCharges(AppointmentPresentation appointmentPresentation)
        {
            var partsPrice = (double?)appointmentPresentation.AppointmentServices.Where(x => !x.IsStriked).Sum(x => x.PartsPrice);
            var laborPrice = (double?)appointmentPresentation.AppointmentServices.Where(x => !x.IsStriked).Sum(x => x.LaborPrice);
            var prices = CalculatePriceCharges(partsPrice, laborPrice);

            appointmentPresentation.ShopChargesAmount = prices.ShopChargesAmount;
            appointmentPresentation.TaxChargesAmount = prices.TaxChargesAmount;

            return appointmentPresentation;
        }

        public ServicePricesViewModel CalculatePriceCharges(double? partsPrice, double? laborPrice)
        {
            var model = new ServicePricesViewModel
            {
                //Price = Math.Round((partsPrice ?? 0) + (laborPrice ?? 0), 2),
                LaborPrice = Math.Round(laborPrice ?? 0, 2),
                PartsPrice = Math.Round(partsPrice ?? 0, 2)
            };
            model.Price = Math.Round(model.LaborPrice + model.PartsPrice, 2);

            var includeShopCharges = _companyService.GetSettingAsBool(CompanySettingsNames.PrintShopCharges);
            if (includeShopCharges)
            {
                var applyShopChargesToLabor = _companyService.GetSettingAsBool(CompanySettingsNames.ShopChargesApplyToLabor);
                var applyShopChargesToParts = _companyService.GetSettingAsBool(CompanySettingsNames.ShopChargesApplyToParts);
                var shopChargeAmount = _companyService.GetSettingAsDouble(CompanySettingsNames.ShopChargeAmount);
                var minShopCharges = _companyService.GetSettingAsDouble(CompanySettingsNames.ShopChargeMinimum);
                var maxShopCharges = _companyService.GetSettingAsDouble(CompanySettingsNames.ShopChargeMaximum);

                if (applyShopChargesToLabor && applyShopChargesToParts)
                {
                    model.PartsPriceShopChargeAmount = CalculateShopChargeAmount(model.Price * shopChargeAmount * 0.01, minShopCharges, maxShopCharges);
                    model.PartsPriceShopChargeAmount *= 0.5;
                    model.LaborPriceShopChargeAmount = model.PartsPriceShopChargeAmount;
                }
                else if (applyShopChargesToLabor)
                {
                    model.LaborPriceShopChargeAmount = CalculateShopChargeAmount(model.LaborPrice * shopChargeAmount * 0.01, minShopCharges, maxShopCharges);
                }
                else if (applyShopChargesToParts)
                {
                    model.PartsPriceShopChargeAmount = CalculateShopChargeAmount(model.PartsPrice * shopChargeAmount * 0.01, minShopCharges, maxShopCharges);
                }
            }

            var includeTaxCharges = _companyService.GetSettingAsBool(CompanySettingsNames.DisplayTexesInMenus);
            if (includeTaxCharges)
            {
                var applyTaxChargesToLabor = _companyService.GetSettingAsBool(CompanySettingsNames.TaxRateApplyToLabor);
                var applyTaxChargesToParts = _companyService.GetSettingAsBool(CompanySettingsNames.TaxRateApplyToParts);
                var taxChargeAmount = _companyService.GetSettingAsDouble(CompanySettingsNames.TaxRateAmount);

                if (applyTaxChargesToLabor && applyTaxChargesToParts)
                {
                    model.PartsPriceTaxAmount = (model.Price + model.LaborPriceShopChargeAmount + model.PartsPriceShopChargeAmount) * taxChargeAmount * 0.01;
                    model.PartsPriceTaxAmount *= 0.5;
                    model.LaborPriceTaxAmount = model.PartsPriceTaxAmount;
                }
                else if (applyTaxChargesToLabor)
                {
                    model.LaborPriceTaxAmount = (model.LaborPrice + model.LaborPriceShopChargeAmount) * taxChargeAmount * 0.01;
                }
                else if (applyTaxChargesToParts)
                {
                    model.PartsPriceTaxAmount = (model.PartsPrice + model.PartsPriceShopChargeAmount) * taxChargeAmount * 0.01;
                }
            }

            model.GrossPrice = model.Price
                + model.ShopChargesAmount
                + model.TaxChargesAmount;

            model.GrossPrice = Math.Round(model.GrossPrice, 2);

            return model;
        }
    }
}
