﻿using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Services.Mappers;
using System.Collections.Generic;
using System.Linq;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class FluidViscosityService : IFluidViscosityService
    {
        private readonly IFluidViscosityRepository _repository;

        public FluidViscosityService(IFluidViscosityRepository repository)
        {
            _repository = repository;
        }

        public List<FluidViscosityViewModel> GetAllFluidViscosityInDealerOilViscosityList()
        {
            var fluidViscosity = new List<FluidViscosityViewModel>();
            fluidViscosity.AddRange(_repository.GetAllFluidViscosityInDealerOilViscosityList().Select(x => Mapper.Map(x)));

            return fluidViscosity;
        }
    }
}
