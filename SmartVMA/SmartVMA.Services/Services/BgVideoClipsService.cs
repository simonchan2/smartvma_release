﻿using System.Collections.Generic;
using System.Linq;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Services.Mappers;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class BgVideoClipsService : IBgVideoClipsService
    {
        private readonly IBgVideoClipsRepository _repository;

        public BgVideoClipsService(IBgVideoClipsRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<BgVideoCliplsViewModel> GetVideos()
        {
            var items = _repository.Find().Select(x => Mapper.Map(x));
            return items;
        }

        // TODO: filter by bgProducts
        public IEnumerable<BgVideoCliplsViewModel> GetVideos(List<int> bgProducts)
        {
            var items = _repository.Find().Select(x => Mapper.Map(x));
            return items;
        }
    }
}
