﻿using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Services.Mappers;
using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.OutputModels;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class OverrideRulesService : IOverrideRulesService
    {
        private readonly IOverrideRulesRepository _overrideRulesRepository;

        public OverrideRulesService(IOverrideRulesRepository overrideRulesRepository)
        {
            _overrideRulesRepository = overrideRulesRepository;
        }

        public ConfigurationSearchByOverridesViewModel GetOverrideRuleById(long? id)
        {
            return _overrideRulesRepository.GetOverrideRuleById(id);
        }

        public ConfigurationOverrideRulesViewModel GetOverrideRuleByIdForList(long? id)
        {
            var model = Mapper.Map(_overrideRulesRepository.GetOverrideRules().FirstOrDefault(x=> x.Id == id));
            return model;
        }

        public ConfigurationOverrideRulesIndexViewModel GetOverrideRules()
        {
            var overrideRules = new List<ConfigurationOverrideRulesViewModel>();
            overrideRules.AddRange(_overrideRulesRepository.Find().Select(x => Mapper.Map(x)));

            var model = new ConfigurationOverrideRulesIndexViewModel();
            model.Items = overrideRules;
            return model;
        }

        public List<ConfigurationOverridesSearchResults> SearchOverrides(ConfigurationSearchByOverridesViewModel model, int pageNumber, int pageSize)
        {
            return _overrideRulesRepository.SearchOverrides(model, pageNumber, pageSize);
        }

        public ServiceResponse Save(ConfigurationOverrideRulesViewModel model)
        {
            throw new NotImplementedException();
        }

        public ServiceResponse Delete(long id)
        {
            _overrideRulesRepository.Delete(id);
            var response = _overrideRulesRepository.CommitChanges();
            return response;
        }

        public ServiceResponse DeleteMany(string ruleIds)
        {
            var response = _overrideRulesRepository.DeleteMany(ruleIds);
            return response;
        }

        public List<ConfigurationOverrideRulesConflictsViewModel> GetConflicts(ConfigurationSearchByOverridesViewModel model, string basicServices, bool areServicesChecked)
        {
            List<ConfigurationOverrideRulesConflictsViewModel> result = new List<ConfigurationOverrideRulesConflictsViewModel>();
            List<OverrideRuleConflictsModel> data = _overrideRulesRepository.GetConflicts(model, basicServices, areServicesChecked);
            foreach(var item in data)
            {
                result.Add(Mapper.Map(item));
            }
            return result;
        }

        public ServiceResponse ApplyOverride(ConfigurationSearchByOverridesViewModel model, string basicServices, bool areServicesChecked, string ruleIdsToDelete)
        {
            ServiceResponse response = _overrideRulesRepository.ApplyOverride(model, basicServices, areServicesChecked, ruleIdsToDelete);
            return response;
        }

        public ConfigurationOverrideRulesIndexViewModel GetPagedItems(PagingRequest request)
        {
            var entities = _overrideRulesRepository.GetPagedResults(request);
            var items = new List<ConfigurationOverrideRulesViewModel>(entities.Select(x => Mapper.Map(x)));
            var model = new ConfigurationOverrideRulesIndexViewModel();
            model.Items = items;
            var total = _overrideRulesRepository.GetPagedResultsTotal(request);
            model.ItemsCount = total;

            return model;
        }
    }
}
