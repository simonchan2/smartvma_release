﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Services.Mappers;
using System.Collections.Generic;
using System.Linq;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class OemPartService : IOemPartService
    {
        private readonly IOemPartRepository _repository;
        private readonly IOemServicePartRepository _servicePartRepository;

        public OemPartService(IOemPartRepository repository, IOemServicePartRepository servicePartRepository)
        {
            _repository = repository;
            _servicePartRepository = servicePartRepository;
        }

        public OemPartIndexViewModel GetIndexViewModel()
        {
            var oemParts = new List<OemPartViewModel>();
            oemParts.AddRange(_repository.GetAllOemPart().Select(x => Mapper.Map(x))); // get the entity from Repository, then Map it to View Model

            var model = new OemPartIndexViewModel { Items = oemParts };
            return model;
        }

        public OemPartViewModel GetViewModel(int? id = null, int? basicServiceId = null)
        {
            var model = new OemPartViewModel();
            if (!id.HasValue)
            {
                return model;
            }

            var entity = _repository.GetOemPartById(id.Value);
            model = Mapper.Map(entity, model);
            if (!basicServiceId.HasValue)
            {
                return model;
            }

            var service = _servicePartRepository.Find().FirstOrDefault(x => x.OemBasicServiceId == basicServiceId && x.OemPartId == id && x.IsDeleted == false);
            if (service != null)
            {
                model.Quantity = service.Quantity;
            }
            return model;
        }

        public IEnumerable<string> GetDistinctViscosity()
        {
            return _repository.GetDistinctViscosity();
        }

        public IEnumerable<string> GetDistinctOilTypes()
        {
            return _repository.GetDistinctOilTypes();
        }

        public ServiceResponse Save(OemPartViewModel model)
        {
            var partNumberRepository = IocManager.Resolve<IOemPartNumberRepository>();

            OemPart oemPart;
            if (model.Id.HasValue)
            {
                oemPart = Mapper.Map(model, _repository.FirstOrDefault(x => x.Id == model.Id.Value), true);
                oemPart.NoOverwrite = true;
                _repository.Update(oemPart);
                SavePartNumber(model.OemPartNumberId, oemPart, model.OemPartNo);
            }
            else
            {
                oemPart = Mapper.Map(model);
                oemPart.NoOverwrite = true;
                _repository.Insert(oemPart);
                SavePartNumber(null, oemPart, model.OemPartNo);
            }

            var response = _repository.CommitChanges();
            if (response.IsSuccess)
            {
                response.SavedItemId = oemPart.Id;
            }
            return response;
        }

        public void SavePartNumber(int? OemPartNumberId, OemPart oemPart, string oemPartNo, int countryId = 1, bool noOverwrite = true, bool IsCreatedByAdmin = true)
        {
            var partNumberRepository = IocManager.Resolve<IOemPartNumberRepository>();
            if (oemPart != null && !string.IsNullOrEmpty(oemPartNo))
            {
                var partNumber = new OemPartNumber
                {
                    OemPartNo = oemPartNo,
                    OemPartId = oemPart.Id,
                    OemPart = oemPart,
                    CountryId = countryId,
                    NoOverwrite = noOverwrite
                };
                if (OemPartNumberId.HasValue)
                {
                    var existingPartNumber = partNumberRepository.GetOemPartNumberById(OemPartNumberId.Value);
                    existingPartNumber.OemPartNo = partNumber.OemPartNo;
                    existingPartNumber.OemPartId = partNumber.OemPartId;
                    existingPartNumber.OemPart = partNumber.OemPart;
                    existingPartNumber.CountryId = partNumber.CountryId;
                    existingPartNumber.NoOverwrite = partNumber.NoOverwrite;
                    partNumberRepository.Update(existingPartNumber);
                }
                else
                {
                    partNumber.IsCreatedByAdmin = IsCreatedByAdmin;
                    partNumberRepository.Insert(partNumber);
                }
            }
        }

        public ServiceResponse Delete(int id)
        {
            var oemPart = _repository.FirstOrDefault(x => x.Id == id);
            _repository.Delete(oemPart);
            var response = _repository.CommitChanges();
            return response;
        }
    }
}
