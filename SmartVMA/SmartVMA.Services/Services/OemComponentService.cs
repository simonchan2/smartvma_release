﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Resources;
using SmartVMA.Services.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class OemComponentService : IOemComponentService
    {
        private readonly IOemComponentRepository _repository;
        protected readonly IAppContext _appContext;

        public OemComponentService(IOemComponentRepository repository, IAppContext appContext)
        {
            _repository = repository;
            _appContext = appContext;
        }

        public OemComponentIndexViewModel GetAllOemComponentsShownAsOption()
        {
            var entities = new List<OemComponentViewModel>();
            entities.AddRange(_repository.GetAllOemComponentsShownAsOption().Select(x => Mapper.Map(x))); // get the entity from Repository, then Map it to View Model

            var model = new OemComponentIndexViewModel();
            model.Items = entities;
            return model;
        }        
    }
}
