﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.OutputModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Resources;
using SmartVMA.Services.Mappers;
using SmartVMA.Services.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using SmartVMA.Infrastructure;
using SmartVMA.Infrastructure.CacheManager;
using SmartVMA.Infrastructure.Helper;

namespace SmartVMA.Services
{
    internal abstract class BaseServicePresentationService : IBaseServicePresentationService
    {
        protected readonly ICompanyService CompanyService;
        protected readonly IDealerCustomerRepository DealerCustomerRepository;
        protected readonly IGeneralCarServiceRepository GeneralServiceRepository;
        protected readonly IAppointmentPresentationService AppointmentPresentationService;
        protected readonly ICarService CarService;

        protected ServicesProcedureModel[] AllServices { get; set; }

        protected abstract bool IsLofMenu { get; }
        protected abstract MenuLevelServicesIndexViewModel GetServices(ServicesProcedureModel[] services);

        protected BaseServicePresentationService(IDealerCustomerRepository dealerCustomerRepository, ICompanyService companyService, IGeneralCarServiceRepository generalServicerRepository, IAppointmentPresentationService appointmentPresentationService, ICarService carService)
        {
            DealerCustomerRepository = dealerCustomerRepository;
            CompanyService = companyService;
            GeneralServiceRepository = generalServicerRepository;
            AppointmentPresentationService = appointmentPresentationService;
            CarService = carService;
        }

        public virtual ServiceBasePresentationViewModel GetViewModel(int? carId, int? mileage, int? dealerCustomerId, long? appointmentPresentationId, long? inspectionId, int? vehicleTransmission, int? vehicleDriveline, int? enteredMileage, int? dealerVehicleId, string vin, int? carIdUndetermined)
        {
            var model = new ServiceBasePresentationViewModel
            {
                MenuCarId = carId,
                CarIdUndetermined = carIdUndetermined ?? (int)CarIdUndeterminedStatus.True,
                CustomerInfo = new CustomerInfoForServicesViewModel()
            };

            if(appointmentPresentationId.HasValue && appointmentPresentationId.Value == -1)
            {
                appointmentPresentationId = null;
            }

            var appointmentPresentationRepository = IocManager.Resolve<IAppointmentPresentationRepository>();
            var appointmentPresentation = appointmentPresentationRepository.FirstOrDefault(x => x.Id == (appointmentPresentationId ?? 0));
            if (appointmentPresentationId.HasValue && appointmentPresentation != null && appointmentPresentation.PresentationStatus == AppointmentPresentationStatus.MK.ToString() && (appointmentPresentation.DealerCustomerId == null || appointmentPresentation.DealerCustomerId == 0))
            {
                SetCustomerInfo(carId, null, appointmentPresentationId, ref vehicleTransmission, ref vehicleDriveline, dealerVehicleId, vin, model.CarIdUndetermined, model);
            }
            else
            {
                SetCustomerInfo(carId, dealerCustomerId, appointmentPresentationId, ref vehicleTransmission, ref vehicleDriveline, dealerVehicleId, vin, model.CarIdUndetermined, model);
            }
            if (mileage == null || mileage == 0 && enteredMileage != null)
            {
                mileage = enteredMileage;
            }
            model.InspectionId = inspectionId;
            if (!model.MenuCarId.HasValue && model.CustomerInfo != null && model.CustomerInfo.CustomerInfo != null)
            {
                model.MenuCarId = model.CustomerInfo.CustomerInfo.CarId;
            }
            if (appointmentPresentationId.HasValue)
            {
                model.AppointmentPresentationId = appointmentPresentationId;
                model.AppointmentPresentationStatus = appointmentPresentation != null ? appointmentPresentation.PresentationStatus : null;
                if (appointmentPresentation != null)
                {
                    if (appointmentPresentation.PresentationStatus == AppointmentPresentationStatus.MK.ToString() && !IsLofMenu)
                    {
                        model.Invoice = appointmentPresentation.InvoiceNumber;
                        model.ParkedAppointmentTime = string.Empty;
                        if (appointmentPresentation.ParkedTime.HasValue)
                        {
                            var companyService = IocManager.Resolve<ICompanyService>();
                            model.ParkedAppointmentTime = appointmentPresentation.ParkedTime.Value.ToString(companyService.GetDateTimeFormat(CompanySettingsNames.DateFormat));
                        }
                    }
                    if (!mileage.HasValue && appointmentPresentation.Mileage > 0)
                    {
                        mileage = appointmentPresentation.Mileage;
                    }
                    if (!enteredMileage.HasValue)
                    {
                        enteredMileage = appointmentPresentation.Mileage;
                    }
                }
                
                model.IsValidPresentationMenu = !(model.AppointmentPresentationStatus == AppointmentPresentationStatus.MD.ToString()
                    || model.AppointmentPresentationStatus == AppointmentPresentationStatus.MC.ToString());
            }

            if (mileage.HasValue && carId.HasValue)
            {
                var oemMileageService = IocManager.Resolve<ICarService>();
                var interval = oemMileageService.FindMenuInterval(carId.Value, mileage.Value);
                if (interval.IsSuccess && interval.Model != null)
                {
                    model.MenuMileage = interval.Model.Mileage;
                    if (!enteredMileage.HasValue)
                    {
                        enteredMileage = mileage;
                    }
                    if (model.CustomerInfo != null && model.CustomerInfo.CustomerInfo != null)
                    {
                        model.CustomerInfo.CustomerInfo.Mileage = enteredMileage;
                    }
                    model.EnteredMileage = enteredMileage;
                }
            }

            if (vehicleTransmission != null)
            {
                model.Transmission = vehicleTransmission;
            }
            if (vehicleDriveline != null)
            {
                model.Driveline = vehicleDriveline;
            }

            if (model.AppointmentPresentationStatus == AppointmentPresentationStatus.MK.ToString() && !IsLofMenu)
            {
                model.Services = GetAppointmentPresentationServices(appointmentPresentationId ?? 0, model.MenuCarId, mileage: model.MenuMileage, isLof: IsLofMenu,
                           vehicleTransmission: vehicleTransmission, vehicleDriveline: vehicleDriveline,
                           dealerCustomerId: dealerCustomerId);
            }
            // if there isn't mileage don't load anything, because Odometer pop up will be shown
            else if (IsLofMenu || (!IsLofMenu && model.MenuMileage.HasValue))
            {
                if (IsLofMenu && model.MenuMileage == null && enteredMileage != null)
                {
                    model.MenuMileage = enteredMileage;
                }
                AllServices = GeneralServiceRepository
                    .GetAllServices(model.MenuCarId, mileage: model.MenuMileage, isLof: IsLofMenu,
                            vehicleTransmission: vehicleTransmission, vehicleDriveline: vehicleDriveline,
                            dealerCustomerId: dealerCustomerId, appointmentPresentationId: (!string.IsNullOrEmpty(model.AppointmentPresentationStatus) && model.AppointmentPresentationStatus == AppointmentPresentationStatus.MK.ToString() && IsLofMenu ? null : model.AppointmentPresentationId)).ToArray();
                model.Services = GetServices(AllServices);
            }
            IEnumerable<ServiceDetailInvoiceHistoryViewModel> historyInvoices = null;
            if (model.Services.MenuLevel1Services != null || model.Services.MenuLevel2Services != null || model.Services.MenuLevel3Services != null)
            {
                List<ServiceDetailViewModel> allServices = new List<ServiceDetailViewModel>();
                if (model.Services.MenuLevel1Services != null)
                {
                    allServices = model.Services.MenuLevel1Services.ToList();
                }
                if (model.Services.MenuLevel2Services != null)
                {
                    if (allServices.Count == 0)
                    {
                        allServices = model.Services.MenuLevel2Services.ToList();
                    }
                    else
                    {
                        allServices.Union(model.Services.MenuLevel2Services.Where(c => !allServices.Any(d => d.Id == c.Id)));
                    }
                }
                if (model.Services.MenuLevel3Services != null)
                {
                    if (allServices.Count == 0)
                    {
                        allServices = model.Services.MenuLevel3Services.ToList();
                    }
                    else
                    {
                        allServices.Union(model.Services.MenuLevel3Services.Where(c => allServices.All(d => d.Id != c.Id)));
                    }
                }
                historyInvoices = GeneralServiceRepository.GetHistoryInvoices(allServices);
            }
            model.IsDmsDealer = !string.IsNullOrEmpty(CompanyService.GetCompanySettingValue(CompanySettingsNames.DmsTypeId));
            CalculateLppValues(model.Services, enteredMileage, historyInvoices, model.IsDmsDealer);

            var selectedMenuLevel = model.Services.SelectedMenuLevel;
            if (selectedMenuLevel != 0)
            {
                model.Services.SelectedMenuLevel = selectedMenuLevel;
            }

            //set menu package opcode
            model.Services.MenuPackageOpCodeLevel1 = GetMenuLevelOpCode(model.MenuMileage, (int)MenuLevelTypes.FirstLevel);
            if (model.Services.MenuLevel1Services != null && model.Services.MenuLevel1Services.Any())
            {
                foreach (var service in model.Services.MenuLevel1Services.Where(c => string.IsNullOrEmpty(c.OpCode) && c.ServiceTypeId == (int)ServiceMenuTypes.MaintenanceService))
                {
                    service.OpCode = model.Services.MenuPackageOpCodeLevel1;
                }
            }
            if (model.Services.MenuLevel2Services != null && model.Services.MenuLevel2Services.Any())
            {
                model.Services.MenuPackageOpCodeLevel2 = GetMenuLevelOpCode(model.MenuMileage, (int)MenuLevelTypes.SecondLevel);
                foreach (var service in model.Services.MenuLevel2Services.Where(c => string.IsNullOrEmpty(c.OpCode) && c.ServiceTypeId == (int)ServiceMenuTypes.MaintenanceService))
                {
                    service.OpCode = model.Services.MenuPackageOpCodeLevel2;
                }
            }
            if (model.Services.MenuLevel3Services != null && model.Services.MenuLevel3Services.Any())
            {
                model.Services.MenuPackageOpCodeLevel3 = GetMenuLevelOpCode(model.MenuMileage, (int)MenuLevelTypes.ThirdLevel);
                foreach (var service in model.Services.MenuLevel3Services.Where(c => string.IsNullOrEmpty(c.OpCode) && c.ServiceTypeId == (int)ServiceMenuTypes.MaintenanceService))
                {
                    service.OpCode = model.Services.MenuPackageOpCodeLevel3;
                }
            }

            model.AreConflictsResolved = (model.MenuCarId != null &&
                (model.CarIdUndetermined == (int)CarIdUndeterminedStatus.False ||
                (model.CustomerInfo != null && model.CustomerInfo.VehicleFilterModel != null &&
                model.CustomerInfo.VehicleFilterModel.VehicleFilterResponseModel != null &&
                model.CustomerInfo.VehicleFilterModel.VehicleFilterResponseModel.Years != null &&
                model.CustomerInfo.VehicleFilterModel.VehicleFilterResponseModel.Years.Count() == 1 &&
                model.CustomerInfo.VehicleFilterModel.VehicleFilterResponseModel.Makes != null &&
                model.CustomerInfo.VehicleFilterModel.VehicleFilterResponseModel.Makes.Count() == 1 &&
                model.CustomerInfo.VehicleFilterModel.VehicleFilterResponseModel.Models != null &&
                model.CustomerInfo.VehicleFilterModel.VehicleFilterResponseModel.Models.Count() == 1 &&
                model.CustomerInfo.VehicleFilterModel.VehicleFilterResponseModel.EngineTypes != null &&
                model.CustomerInfo.VehicleFilterModel.VehicleFilterResponseModel.EngineTypes.Count() == 1 &&
                model.CustomerInfo.VehicleFilterModel.VehicleFilterResponseModel.TransmissionTypes != null &&
                model.CustomerInfo.VehicleFilterModel.VehicleFilterResponseModel.TransmissionTypes.Count() == 1 &&
                model.CustomerInfo.VehicleFilterModel.VehicleFilterResponseModel.DriveLines != null &&
                model.CustomerInfo.VehicleFilterModel.VehicleFilterResponseModel.DriveLines.Count() == 1)) &&
                vehicleTransmission != null &&
                vehicleDriveline != null);

            var iLifeService = IocManager.Resolve<IILifeService>();
            ILifeViewModel iLifeModel = model.CustomerInfo != null && model.CustomerInfo.CustomerInfo != null ? iLifeService.ButtonInfo(model.CustomerInfo.CustomerInfo.VIN) : null;
            model.ILifeVisibility = iLifeModel != null ? iLifeModel.IsVisible : false;
            model.ILifeColorClass = iLifeModel != null ? iLifeModel.ColorClass : Labels.ILifeGreyColour;
            model.ILifeButtonName = iLifeModel != null ? iLifeModel.ButtonName : Labels.ILife;

            return model;
        }

        private void CalculateLppValues(MenuLevelServicesIndexViewModel services, int? enteredMileage, IEnumerable<ServiceDetailInvoiceHistoryViewModel> historyInvoices, bool isDmsDealer)
        {
            if (services.MenuLevel1Services != null)
            {
                var menuLevel1Services = services.MenuLevel1Services.ToList();
                foreach (var service in menuLevel1Services.Where(c => c.BgProtectionPlanId != null))
                {
                    GetLppColourForService(enteredMileage, historyInvoices, service, isDmsDealer);
                }
                services.MenuLevel1Services = menuLevel1Services;

            }
            if (services.MenuLevel2Services != null)
            {
                var menuLevel2Services = services.MenuLevel2Services.ToList();
                foreach (var service in menuLevel2Services.Where(c => c.BgProtectionPlanId != null))
                {
                    GetLppColourForService(enteredMileage, historyInvoices, service, isDmsDealer);
                }
                services.MenuLevel2Services = menuLevel2Services;
            }
            if (services.MenuLevel3Services != null)
            {
                var menuLevel3Services = services.MenuLevel3Services.ToList();
                foreach (var service in menuLevel3Services.Where(c => c.BgProtectionPlanId != null))
                {
                    GetLppColourForService(enteredMileage, historyInvoices, service, isDmsDealer);
                }
                services.MenuLevel3Services = menuLevel3Services;
            }
        }

        protected static void GetLppColourForService(int? enteredMileage, IEnumerable<ServiceDetailInvoiceHistoryViewModel> historyInvoices, ServiceDetailViewModel service, bool isDmsDealer)
        {
            if (!isDmsDealer)
            {
                service.LppColour = LPPColour.Grey;
                return;
            }
            bool isServiceEligible = true;
            int checkPointMileage = service.MaxMileageBeforeFirstService ?? 0;
            var serviceHistoryInvoices = historyInvoices == null ? null : historyInvoices.Where(c => c.OpCode == service.OpCode).OrderBy(c => c.Mileage).ToArray();
            if (((serviceHistoryInvoices == null || serviceHistoryInvoices.Count() == 0) && enteredMileage > service.MaxMileageBeforeFirstService) || (serviceHistoryInvoices != null && serviceHistoryInvoices.Count() > 0 && serviceHistoryInvoices.FirstOrDefault().Mileage > service.MaxMileageBeforeFirstService))
            {
                isServiceEligible = false;
            }
            if (isServiceEligible && serviceHistoryInvoices != null)
            {
                for (int i = 0; i <= serviceHistoryInvoices.Count() - 1; i++)
                {
                    if (serviceHistoryInvoices[i].Mileage > checkPointMileage)
                    {
                        isServiceEligible = false;
                    }
                    checkPointMileage += service.ServiceInterval ?? 0;
                }
            }
            if (!isServiceEligible)
            {
                service.LppColour = LPPColour.Red;
            }
            else
            {
                if ((serviceHistoryInvoices == null || serviceHistoryInvoices.Count() == 0) && enteredMileage < service.MaxMileageBeforeFirstService)
                {
                    service.LppColour = LPPColour.Grey;
                }
                else
                {
                    if (checkPointMileage - enteredMileage < 10000)
                    {
                        service.LppColour = LPPColour.Yelow;
                    }
                    else
                    {
                        service.LppColour = LPPColour.Green;
                    }
                }
            }
        }

        private void SetCustomerInfo(int? carId, int? dealerCustomerId, long? appointmentPresentationId, ref int? vehicleTransmission, ref int? vehicleDriveline, int? dealerVehicleId, string vin, int carIdUndetermined, ServiceBasePresentationViewModel model)
        {
            model.CustomerInfo.CustomerInfo = DealerCustomerRepository.GetCustomerInfo(dealerCustomerId, carId, appointmentPresentationId, vehicleTransmission, vehicleDriveline, dealerVehicleId) ?? new CustomerInfoViewModel();
            model.CustomerInfo.VehicleFilterModel = new VehicleFilterViewModel();
            if (!string.IsNullOrEmpty(vin))
            {
                model.CustomerInfo.CustomerInfo.VIN = vin;
                model.CustomerInfo.VehicleFilterModel.VIN = vin;
            }

            if (carIdUndetermined == (int)CarIdUndeterminedStatus.False)
            {
                model.CustomerInfo.VehicleFilterModel.Years = new List<int> { (model.CustomerInfo.CustomerInfo.Year ?? 0) };
                model.CustomerInfo.VehicleFilterModel.Makes = new List<int> { model.CustomerInfo.CustomerInfo.MakeId };
                model.CustomerInfo.VehicleFilterModel.Models = new List<int> { model.CustomerInfo.CustomerInfo.ModelId };
                model.CustomerInfo.VehicleFilterModel.EngineTypes = new List<int> { model.CustomerInfo.CustomerInfo.EngineId };
                if (vehicleTransmission == null && model.CustomerInfo.CustomerInfo.VinMasterTransmissionId != null)
                {
                    vehicleTransmission = model.CustomerInfo.CustomerInfo.VinMasterTransmissionId;
                }
                model.CustomerInfo.VehicleFilterModel.TransmissionTypes = new List<int> { (vehicleTransmission ?? 0) };
                if (vehicleDriveline == null && model.CustomerInfo.CustomerInfo.VinMasterDriveLineId != null)
                {
                    vehicleDriveline = model.CustomerInfo.CustomerInfo.VinMasterDriveLineId;
                }
                model.CustomerInfo.VehicleFilterModel.DriveLines = new List<int> { (vehicleDriveline ?? 0) };
            }
            var cacheManager = IocManager.Resolve<ICacheManager>();
            var key = CacheHelper.BuildCacheKey(ConfigurationSettings.CacheKeyVehicleFilter, model.CustomerInfo.VehicleFilterModel);
            model.CustomerInfo.VehicleFilterModel.VehicleFilterResponseModel = cacheManager.GetOrSet(key, () => CarService.GetVehicleFilter(model.CustomerInfo.VehicleFilterModel));
            var selectedTransmission = model.CustomerInfo.VehicleFilterModel.VehicleFilterResponseModel.TransmissionTypes.Where(c => c.Selected).Count() == 1 ? model.CustomerInfo.VehicleFilterModel.VehicleFilterResponseModel.TransmissionTypes.Where(c => c.Selected).FirstOrDefault() : null;
            if (selectedTransmission != null && vehicleTransmission == null)
            {
                if ((model.CustomerInfo.VehicleFilterModel.TransmissionTypes == null) || (model.CustomerInfo.VehicleFilterModel.TransmissionTypes.FirstOrDefault().ToString() != selectedTransmission.Value))
                {
                    int transmissionValue = Convert.ToInt32(selectedTransmission.Value);
                    model.CustomerInfo.VehicleFilterModel.TransmissionTypes = new List<int> { transmissionValue };
                    vehicleTransmission = transmissionValue;
                }
            }
            var selectedDriveLine = model.CustomerInfo.VehicleFilterModel.VehicleFilterResponseModel.DriveLines.Where(c => c.Selected).Count() == 1 ? model.CustomerInfo.VehicleFilterModel.VehicleFilterResponseModel.DriveLines.Where(c => c.Selected).FirstOrDefault() : null;
            if (selectedDriveLine != null && vehicleDriveline == null)
            {
                if ((model.CustomerInfo.VehicleFilterModel.DriveLines == null) || (model.CustomerInfo.VehicleFilterModel.DriveLines.FirstOrDefault().ToString() != selectedDriveLine.Value))
                {
                    int drivelineValue = Convert.ToInt32(selectedDriveLine.Value);
                    model.CustomerInfo.VehicleFilterModel.DriveLines = new List<int> { drivelineValue };
                    vehicleDriveline = drivelineValue;
                }
            }
        }

        public ConfirmServiceViewModel GetConfirmServiceViewModel(ConfirmServiceRequestModel request)
        {
            var model = Mapper.Map(request);
            string appointmentPresentationStatus = request.AppointmentPresentationId == null ? string.Empty : AppointmentPresentationService.GetAppointmentPresentationStatus(request.AppointmentPresentationId.Value);
            List<ServicesProcedureModel> allServices = null;
            if (!string.IsNullOrEmpty(appointmentPresentationStatus) && appointmentPresentationStatus == AppointmentPresentationStatus.MK.ToString() && !IsLofMenu)
            {
                var appointmentPresentationRepository = IocManager.Resolve<IAppointmentPresentationRepository>();
                var appointmentPresentationWithServices = appointmentPresentationRepository.GetAppointmentPresentationWithServicesById(request.AppointmentPresentationId.Value);
                var test = 
                allServices = appointmentPresentationWithServices.AppointmentServices.Where(c => request.Services.Any(d => d.Id == c.OrigServiceId)).GroupBy(c =>c.OrigServiceId).Select(x => Mapper.Map(x.FirstOrDefault(), new ServicesProcedureModel())).ToList();
            }
            else
            {
                allServices = GeneralServiceRepository
                .GetAllServices(carId: request.CarId, mileage: request.Mileage, serviceIds: request.Services, isLof: IsLofMenu, dealerCustomerId: request.DealerCustomerId, appointmentPresentationId: (!string.IsNullOrEmpty(appointmentPresentationStatus) && appointmentPresentationStatus == AppointmentPresentationStatus.MK.ToString() && IsLofMenu ? (long?)null : request.AppointmentPresentationId)).ToList();
            }

            model.Services = allServices.Select(x => Mapper.Map(x)).ToList();

            if (request.Services != null)
            {
                if (!string.IsNullOrEmpty(appointmentPresentationStatus) && appointmentPresentationStatus == AppointmentPresentationStatus.MK.ToString() && !IsLofMenu)
                {
                    foreach (var item in request.Services.Where(c => c.IsAddedNow == true))
                    {
                        var newAppointmentService = Mapper.Map(item);
                        model.Services.Add(newAppointmentService);
                    }
                }
                else
                {
                    foreach (var item in request.Services.Where(c => c.ServiceTypeId == ServiceType.AdHocAdditionalService))
                    {
                        var newAppointmentService = Mapper.Map(item);
                        model.Services.Add(newAppointmentService);
                    }
                }
            }

            foreach (var item in model.Services)
            {
                foreach (var requestItem in request.Services)
                {
                    if (item.Id == requestItem.Id)
                    {
                        item.Counter = requestItem.Counter;
                        item.IsAddedNow = requestItem.IsAddedNow;
                        if (item.IsAddedAfter != requestItem.IsAddedAfter)
                        {
                            model.Services.Find(i => i.Id == item.Id).IsAddedAfter = requestItem.IsAddedAfter;
                            model.Services.Find(i => i.Id == item.Id).Description = (requestItem.AppDescription == null ? "+ " + item.Description : requestItem.AppDescription);
                        }
                    }
                }
            }

            var menuLevel = !IsLofMenu ? request.MenuLevel : model.Services != null && model.Services.Any() ? model.Services.Max(x => x.MenuLevel) : 1;
            var option = GetMenuLevelLabel(menuLevel);
            model.MenuPackageOpCode = GetMenuLevelOpCode(request.Mileage, menuLevel);
            foreach (var item in model.Services)
            {
                if (IsLofMenu)
                {
                    item.Option = option;
                }
                item.IsStrikeOut = request.Services.First(x => x.Id == item.Id && item.ServiceTypeId == (int)x.ServiceTypeId).IsStrikeOut;
                if ((item.ServiceTypeId == (int)ServiceType.OemBasicService || item.ServiceTypeId == (int)ServiceType.AdHocAdditionalService) && string.IsNullOrEmpty(item.OpCode))
                {
                    item.OpCode = model.MenuPackageOpCode;
                }
            }

            model.TotalLabourHours = model.Services.Where(x => !x.IsStrikeOut).Sum(x => x.LaborHours ?? 0);
            var partsPrice = model.Services.Where(x => !x.IsStrikeOut).Sum(x => Math.Round(x.PartsPrice ?? 0, 2));
            var laborPrice = model.Services.Where(x => !x.IsStrikeOut).Sum(x => Math.Round(x.LaborPrice ?? 0, 2));
            var prices = AppointmentPresentationService.CalculatePriceCharges(partsPrice, laborPrice);

            model.ShopChargesAmount = prices.ShopChargesAmount;
            model.TaxChargesAmount = prices.TaxChargesAmount;
            model.TotalPrice = prices.GrossPrice;
            model.MenuTotalPrice = prices.GrossPrice - prices.TaxChargesAmount - prices.ShopChargesAmount;
            model.IsInvoiceRequired = CompanyService.GetSettingAsBool(CompanySettingsNames.DisplayInvoiceNumber);
            var companySettings = IocManager.Resolve<ICompanyService>();
            model.PrintShopCharges = companySettings.GetSettingAsBool(CompanySettingsNames.PrintShopCharges);
            model.DisplayTaxesInMenus = companySettings.GetSettingAsBool(CompanySettingsNames.DisplayTexesInMenus);

            return model;
        }

        protected MenuLevelServicesIndexViewModel GetAppointmentPresentationServices(long appointmentPresentationId, int? carId = null, int? dealerVehicleId = null, int? mileage = null, bool? isLof = null, IEnumerable<ServiceIdsRequestModel> serviceIds = null, int? vehicleTransmission = null, int? vehicleDriveline = null, int? dealerCustomerId = null)
        {
            var appointmentPresentationRepository = IocManager.Resolve<IAppointmentPresentationRepository>();
            var appointmentPresentationWithServices = appointmentPresentationRepository.GetAppointmentPresentationWithServicesById(appointmentPresentationId);

            AllServices = appointmentPresentationWithServices.AppointmentServices
                .Select(x => Mapper.Map(x, new ServicesProcedureModel())).ToArray();

            var additionalServicesRepository = IocManager.Resolve<IAdditionalServiceRepository>();
            var additionalServices = additionalServicesRepository.GetAdditionalService(AllServices.Select(x => x.ServiceId).ToList());

            foreach (var item in additionalServices)
            {
                AllServices.Where(x => x.ServiceId == item.Id).FirstOrDefault().BgProtectionPlanId = item.BgProtectionPlanId;
                AllServices.Where(x => x.ServiceId == item.Id).FirstOrDefault().BgProtectionPlanUrl = item.BgProtectionPlan != null ? item.BgProtectionPlan.BgProtectionPlanDocs != null ? item.BgProtectionPlan.BgProtectionPlanDocs.FirstOrDefault() != null ? item.BgProtectionPlan.BgProtectionPlanDocs.FirstOrDefault().Url
                     : null : null : null;
                AllServices.Where(x => x.ServiceId == item.Id).FirstOrDefault().ServiceInterval = item.IntervalRepeat;
                AllServices.Where(x => x.ServiceId == item.Id).FirstOrDefault().MaxMileageBeforeFirstService = item.BgProtectionPlan != null ? item.BgProtectionPlan.BgProtectionPlanLevels != null ? item.BgProtectionPlan.BgProtectionPlanLevels.Max(c => c.BgProtectionPlanLevelMus.Where(d => d.MeasurementUnitId == item.MeasurementUnitId).Max(d => d.MaxMileageBeforeFirstService)) : (int?)null : null;
            }

            var model = new MenuLevelServicesIndexViewModel();
            if (AllServices.Any(c => c.MenuLevel == (int)MenuLevelTypes.FirstLevel))
            {
                model.MenuLabelLevel1 = GetMenuLevelLabel((int)MenuLevelTypes.FirstLevel);
                model.MenuLevel1Services = AllServices.Where(x => x.MenuLevel == (int)MenuLevelTypes.FirstLevel).Select(x => Mapper.Map(x));
                model.MenuLabelLevel1Price = model.MenuLevel1Services != null && model.MenuLevel1Services.Any() ? CalculateMenuPrice(model.MenuLevel1Services.GroupBy(c => c.Id).Select(c => c.FirstOrDefault())) : 0;
            }

            if (AllServices.Any(c => c.MenuLevel <= (int)MenuLevelTypes.SecondLevel))
            {
                model.MenuLabelLevel2 = GetMenuLevelLabel((int)MenuLevelTypes.SecondLevel);
                model.MenuLevel2Services = AllServices.Where(x => x.MenuLevel <= (int)MenuLevelTypes.SecondLevel).Select(x => Mapper.Map(x));
                model.MenuLabelLevel2Price = model.MenuLevel2Services != null && model.MenuLevel2Services.Any() ? CalculateMenuPrice(model.MenuLevel2Services.GroupBy(c => c.Id).Select(c => c.FirstOrDefault())) : 0;

                if (AllServices.Any(c => c.MenuLevel <= (int)MenuLevelTypes.ThirdLevel))
                {
                    model.MenuLabelLevel3 = GetMenuLevelLabel((int)MenuLevelTypes.ThirdLevel);
                    model.MenuLevel3Services = AllServices.Where(x => x.MenuLevel <= (int)MenuLevelTypes.ThirdLevel).Select(x => Mapper.Map(x));
                    model.MenuLabelLevel3Price = model.MenuLevel3Services != null && model.MenuLevel3Services.Any() ? CalculateMenuPrice(model.MenuLevel3Services.GroupBy(c => c.Id).Select(c => c.FirstOrDefault())) : 0;
                }
            }
            
            model.SelectedMenuLevel = (byte)appointmentPresentationWithServices.MenuLevel;

            var services = GeneralServiceRepository
            .GetAllServices(carId: carId, mileage: mileage, isLof: IsLofMenu,
                    vehicleTransmission: vehicleTransmission, vehicleDriveline: vehicleDriveline,
                    dealerCustomerId: dealerCustomerId, appointmentPresentationId: (int)appointmentPresentationId).ToArray();
            AllServices = AllServices.Concat(services.Where(x => x.ServiceMenuType == (int)ServiceMenuTypes.AdditionalService)).ToArray();
            foreach (var item in AllServices)
            {
                var serviceItem = services.Where(c => c.ServiceId == item.ServiceId).FirstOrDefault();
                if (serviceItem != null)
                {
                    item.IsPreviouslyServed = serviceItem.IsPreviouslyServed;
                    item.IsDeclined = serviceItem.IsDeclined;
                }
            }

            return model;
        }

        public ServiceResponse Save(SaveConfirmServiceRequestModel request)
        {
            var responseValidator = Validator.Validate(request);
            if (!responseValidator.IsSuccess)
            {
                return responseValidator;
            }

            var response = AppointmentPresentationService.Save(request, IsLofMenu);
            return response;
        }

        protected List<string> GetBgProductImages(int menuLevel)
        {
            var appContext = IocManager.Resolve<IAppContext>();
            var companyRepository = IocManager.Resolve<ICompanyRepository>();
            var lofService = IocManager.Resolve<ILofService>();
            var company = companyRepository.GetCompanyAndSettings((int)appContext.CurrentIdentity.TenantId);
            switch (menuLevel)
            {
                case (int)MenuLevelTypes.FirstLevel:
                    if (company.CompanySettings.FirstOrDefault(x => x.Name == "LOFMenuLevel1Images") != null)
                        return lofService.GetLevelImages(company.CompanySettings.FirstOrDefault(x => x.Name == "LOFMenuLevel1Images").Value);
                    else
                        return null;
                case (int)MenuLevelTypes.SecondLevel:
                    if (company.CompanySettings.FirstOrDefault(x => x.Name == "LOFMenuLevel2Images") != null)
                        return lofService.GetLevelImages(company.CompanySettings.FirstOrDefault(x => x.Name == "LOFMenuLevel2Images").Value);
                    else
                        return null;
                case (int)MenuLevelTypes.ThirdLevel:
                    if (company.CompanySettings.FirstOrDefault(x => x.Name == "LOFMenuLevel3Images") != null)
                        return lofService.GetLevelImages(company.CompanySettings.FirstOrDefault(x => x.Name == "LOFMenuLevel3Images").Value);
                    else
                        return null;
            }

            return null;
        }

        protected string GetMenuLevelLabel(int menuLevel)
        {
            string menuLevellabel = string.Empty;
            switch (menuLevel)
            {
                case (int)MenuLevelTypes.FirstLevel:
                    menuLevellabel = IsLofMenu
                            ? CompanyService.GetSettingAsString(CompanySettingsNames.LOFMenuLabelLevel1)
                            : CompanyService.GetSettingAsString(CompanySettingsNames.MenuLabelLevel1);
                    if (string.IsNullOrEmpty(menuLevellabel))
                    {
                        menuLevellabel = IsLofMenu ? Labels.DefaultLofMenuLevelLabel1 : Labels.DefaultMenuLevelLabel1;
                    }
                    break;
                case (int)MenuLevelTypes.SecondLevel:
                    menuLevellabel = IsLofMenu
                            ? CompanyService.GetSettingAsString(CompanySettingsNames.LOFMenuLabelLevel2)
                            : CompanyService.GetSettingAsString(CompanySettingsNames.MenuLabelLevel2);
                    if (string.IsNullOrEmpty(menuLevellabel))
                    {
                        menuLevellabel = IsLofMenu ? Labels.DefaultLofMenuLevelLabel2 : Labels.DefaultMenuLevelLabel2;
                    }
                    break;
                case (int)MenuLevelTypes.ThirdLevel:
                    menuLevellabel = IsLofMenu
                            ? CompanyService.GetSettingAsString(CompanySettingsNames.LOFMenuLabelLevel3)
                            : CompanyService.GetSettingAsString(CompanySettingsNames.MenuLabelLevel3);
                    if (string.IsNullOrEmpty(menuLevellabel))
                    {
                        menuLevellabel = IsLofMenu ? Labels.DefaultLofMenuLevelLabel3 : Labels.DefaultMenuLevelLabel3;
                    }
                    break;
            }

            return menuLevellabel;
        }

        protected string GetMenuLevelOpCode(int? mileage, int menuLevel)
        {
            string menuLevelOpCode = string.Empty;
            string mileageAsStr = mileage != null ? (mileage < 1000 ? string.Empty : (mileage / 1000).ToString()) : string.Empty;
            switch (menuLevel)
            {
                case (int)MenuLevelTypes.FirstLevel:
                    menuLevelOpCode = IsLofMenu
                            ? CompanyService.GetSettingAsString(CompanySettingsNames.LOFMenuOpCodeLevel1)
                            : mileageAsStr + CompanyService.GetSettingAsString(CompanySettingsNames.MenuSuffixLevel1);
                    break;
                case (int)MenuLevelTypes.SecondLevel:
                    menuLevelOpCode = IsLofMenu
                            ? CompanyService.GetSettingAsString(CompanySettingsNames.LOFMenuOpCodeLevel2)
                            : mileageAsStr + CompanyService.GetSettingAsString(CompanySettingsNames.MenuSuffixLevel2);
                    break;
                case (int)MenuLevelTypes.ThirdLevel:
                    menuLevelOpCode = IsLofMenu
                           ? CompanyService.GetSettingAsString(CompanySettingsNames.LOFMenuOpCodeLevel3)
                            : mileageAsStr + CompanyService.GetSettingAsString(CompanySettingsNames.MenuSuffixLevel3);
                    break;
            }

            return menuLevelOpCode;
        }

        protected double? CalculateMenuPrice(IEnumerable<ServiceDetailViewModel> services)
        {
            return services.Where(x => !x.IsStrikeOut).Sum(x => Math.Round(x.Price ?? 0, 2));
        }

        public ServiceResponse CheckValidRequest(SaveConfirmServiceRequestModel request)
        {
            var responseValidator = Validator.Validate(request);
            if (!responseValidator.IsSuccess)
            {
                return responseValidator;
            }
            return responseValidator;
        }
    }
}
