﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Services.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Services
{
    //not in use for now
    [IocBindable]
    internal class ArticleCategoryService : IArticleCategoryService
    {
        private readonly IArticleCategoryRepository _repository;

        public ArticleCategoryService(IArticleCategoryRepository repository)
        {
            _repository = repository;
        }

        public List<ArticleCategoryViewModel> GetArticleCategories()
        {
            var articleCategories = new List<ArticleCategoryViewModel>();
            foreach (var item in _repository.Find())
            {
                articleCategories.Add(Mapper.Map(item));
            }
            return articleCategories;
        }

        public ArticleCategoryViewModel GetArticleCategory(int id)
        {
            var articleCategory = _repository.FirstOrDefault(x => x.Id == id);
            var model = Mapper.Map(articleCategory);

            return model;
        }

        public ArticleCategoryIndexViewModel GetIndexViewModel()
        {
            var articleCategories = new List<ArticleCategoryViewModel>();
            articleCategories.AddRange(_repository.Find().Select(x => Mapper.Map(x)));

            var model = new ArticleCategoryIndexViewModel();
            model.Items = articleCategories;
            return model;
        }

        public ArticleCategoryViewModel GetViewModel(int? id = null)
        {
            var model = new ArticleCategoryViewModel();
            if (!id.HasValue)
            {
                return model;
            }

            var role = _repository.FirstOrDefault(x => x.Id == id.Value);
            model = Mapper.Map(role, model);
            return model;
        }

        public ServiceResponse Save(ArticleCategoryViewModel model)
        {
            var category = new ArticleCategory();
            if (model.Id.HasValue)
            {
                category = Mapper.Map(model, _repository.FirstOrDefault(x => x.Id == model.Id.Value));
                _repository.Update(category);
            }
            else
            {
                category = Mapper.Map(model);
                _repository.Insert(category);
            }

            var response = _repository.CommitChanges();
            response.PreventModalFromClosing = true;
            if (response.IsSuccess)
            {
                response.SavedItemId = category.Id;
            }
            return response;
        }
       
        public ServiceResponse Delete(int id)
        {
            var articleCategory = _repository.FirstOrDefault(x => x.Id == id);
            _repository.Delete(articleCategory);
            var response = _repository.CommitChanges();
            return response;
        }
    }
}
