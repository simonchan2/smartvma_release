﻿using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using System.Collections.Generic;
using SmartVMA.Services.Mappers;
using System;
using SmartVMA.Core.Entities;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class DealerVehicleService : IDealerVehicleService
    {
        private readonly IDealerVehicleRepository _repository;
        public DealerVehicleService(IDealerVehicleRepository repository)
        {
            _repository = repository;
        }

        public int? GetDealerVehicleId(int year, int make, int model, int engine, string transmission, string driveLine)
        {
            return _repository.GetDealerVehicleId(year, make, model, engine, transmission, driveLine);
        }
    }
}
