﻿using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using System.Collections.Generic;
using System.Linq;
using SmartVMA.Core.Contracts;
using SmartVMA.Core.Repositories;
using SmartVMA.Services.Mappers;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Core.Enums;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class RoleService : IRoleService
    {
        private readonly IRoleRepository _repository;
        public RoleService(IRoleRepository repository)
        {
            _repository = repository;
        }

        public ServiceResponse Delete(RoleViewModel model)
        {
            if (!model.Id.HasValue)
            {
                return new ServiceResponse();
            }
            _repository.Delete(model.Id.Value);
            var response = _repository.CommitChanges();
            return response;
        }

        public RoleIndexViewModel GetIndexViewModel()
        {
            var roles = new List<RoleViewModel>();
            roles.AddRange(_repository.Find().Select(x => Mapper.Map(x)));
            var appContext = IocManager.Resolve<IAppContext>();
            var availableRolesIds = GetAvailableRoles(new List<int>() { appContext.CurrentUserRoleId }, roles);
            var filterRoles = roles.Where(x => availableRolesIds.Contains(x.Id.Value)).ToList();
            var model = new RoleIndexViewModel();
            model.Items = filterRoles;
                      
            return model;
        }

        private List<int> GetAvailableRoles(List<int> currentRoleIds, List<RoleViewModel> allRoles)
        {
            List<int> availableRolesIds = new List<int>();
            availableRolesIds.AddRange(currentRoleIds);
            List<int> updatedCurrentRoleIds = allRoles.Where(x => x.ParentId.HasValue && currentRoleIds.Contains(x.ParentId.Value)).Select(x => x.Id.Value).ToList();
            if (updatedCurrentRoleIds.Count > 0)
            {
                availableRolesIds.AddRange(GetAvailableRoles(updatedCurrentRoleIds, allRoles));
            }
            return availableRolesIds;
        }

        public RoleViewModel GetViewModel(int? id = null)
        {
            var model = new RoleViewModel();
            if (!id.HasValue)
            {
                return model;
            }

            var role = _repository.FirstOrDefault(x => x.Id == id.Value);
            model = Mapper.Map(role, model);
            return model;
        }

        public ServiceResponse Save(RoleViewModel model)
        {
            if (model.Id.HasValue)
            {
                var role = Mapper.Map(model, _repository.FirstOrDefault(x => x.Id == model.Id.Value));
                _repository.Update(role);
            }
            else
            {
                var role = Mapper.Map(model);
                _repository.Insert(role);
            }

            var response = _repository.CommitChanges();
            return response;
        }

        public RoleIndexViewModel GetPagedItems(PagingRequest request)
        {
            var entities = _repository.GetPagedResults(request);
            var items = new List<RoleViewModel>(entities.Select(x => Mapper.Map(x)));
            var model = new RoleIndexViewModel();
            model.Items = items;
            var total = _repository.GetPagedResultsTotal(request);
            model.ItemsCount = total;

            return model;
        }

        public string GetRoleName(int roleId)
        {
            var role = _repository.Find(x => x.Id == roleId).FirstOrDefault();
            return role.Role1;
        }

    }
}
