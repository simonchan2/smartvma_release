﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Core.Enums;
using System.Collections.Generic;
using System.Linq;
using SmartVMA.Services.Mappers;
using SmartVMA.Core.InputModels.ViewModels.Dealer.MenuPresentations;
using System;

namespace SmartVMA.Services
{
    //[IocBindable]
    //internal class OemMenuMileageService : IOemMenuMileageService
    //{
    //    private readonly IOemMenuMileageRepository _repository;
    //    private readonly IAppContext _appContext;
    //    private readonly ICompanyService _companyService;

    //    public OemMenuMileageService(IOemMenuMileageRepository repository, IAppContext appContext, ICompanyService companyService)
    //    {
    //        _repository = repository;
    //        _appContext = appContext;
    //        _companyService = companyService;
    //    }

    //    public OdometerResponseModel FindMenuInterval(OdoMeterViewModel model)
    //    {
    //        return FindMenuInterval(model.Mileage);
    //    }

    //    public OdometerResponseModel FindMenuInterval(int mileage)
    //    {
    //        var measurementUnitId = _companyService.GetSettingAsInteger(CompanySettingsNames.MeasurementUnit);
    //        //var result = _repository.FirstOrDefault(x => x.MeasurementUnitId == measurementUnitId
    //        //    && x.Mileage >= mileage - 5000 && x.Mileage <= mileage + 5000);

    //        var result = _repository.GetAllDistinctMileages(measurementUnitId)
    //            .Aggregate((x, y) => Math.Abs(x - mileage) < Math.Abs(y - mileage) ? x : y);


    //        var response = new OdometerResponseModel() { IsSuccess = true, Model = new OdoMeterViewModel { Mileage = result } };
    //        return response;
    //    }

    //    public IEnumerable<int> GetAllMileages(int mileage)
    //    {
    //        var measurementUnitId = _companyService.GetSettingAsInteger(CompanySettingsNames.MeasurementUnit);
    //        return _repository.GetAllDistinctMileages(measurementUnitId);
    //    }
    //}
}
