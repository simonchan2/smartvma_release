﻿using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Services.Mappers;
using System;
using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Services.Validators;
using SmartVMA.Infrastructure.AppContext;
using System.Linq;
using SmartVMA.Core.Enums;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class DealerCustomerService : IDealerCustomerService
    {
        private readonly IDealerCustomerRepository _repository;
        public DealerCustomerService(IDealerCustomerRepository repository)
        {
            _repository = repository;
        }

        public CustomerInfoViewModel GetCustomerInfo(int? dealerCustomerId, int carId, int? transmissionId, int? driveLineId, int? dealerVehicleId)
        {
            return _repository.GetCustomerInfo(dealerCustomerId, carId, transmissionId, driveLineId, dealerVehicleId);
        }

        public CustomerInfoViewModel GetCustomerInfo(long appointmentPresentationId)
        {
            return _repository.GetCustomerInfo(appointmentPresentationId);
        }

        //return search results from customer search
        public AdvisorSearchIndexViewModel CustomerSearch(string customerName, string customerNumber, string customerPhoneNumber, string vin, string vehicleStock, string licensePlate)
        {
            AdvisorSearchIndexViewModel model = new AdvisorSearchIndexViewModel();
            var appContext = IocManager.Resolve<IAppContext>();
            var _companyRepository = IocManager.Resolve<ICompanyRepository>();
            var commpanySettings = _companyRepository.GetCompanyAndSettings(appContext.CurrentIdentity.TenantId.Value);
            var parkedMenuRetentionDurationSetting = commpanySettings.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.ParkedMenuRetentionDuration.ToString());
            var parkedMenuRetentionDurationInt = parkedMenuRetentionDurationSetting != null ? Convert.ToInt32(parkedMenuRetentionDurationSetting.Value) : 3;
            var items = _repository.CustomerSearch(customerName, customerNumber, customerPhoneNumber, vin, vehicleStock, licensePlate, parkedMenuRetentionDurationInt);
            model.Items = items;
            model.ItemsCount = items.Count;

            return model;
        }

        public DealerCustomerViewModel GetViewModel(int? id = null, SaveConfirmServiceRequestModel request = null)
        {
            var model = new DealerCustomerViewModel { ConfirmServicesRequest = request };
            return model;
        }

        public DealerCustomerViewModel GetViewModel(int id)
        {
            var customer = _repository.FirstOrDefault(x => x.Id == id);
            var model = new DealerCustomerViewModel();
            model = Mapper.Map(customer, model);
            return model;
        }

        public ServiceResponse Save(DealerCustomerViewModel model)
        {
            var responseValidator = Validator.Validate(model);
            if (!responseValidator.IsSuccess)
            {
                return responseValidator;
            }
            DealerCustomer entity = null;

            if (model.Id.HasValue)
            {
                entity = Mapper.Map(model, _repository.FirstOrDefault(x => x.Id == model.Id));
                // TO DO: 1 - In one repo             
                _repository.Update(entity);
            }
            else
            {
                entity = Mapper.Map(model, entity);
                // TO DO: 1 - In one repo             
                _repository.Insert(entity);

            }

            var response = _repository.CommitChanges();

            var _dealerVehicleRepository = IocManager.Resolve<IDealerVehicleRepository>();
            var _vehicleRepository = IocManager.Resolve<IVehicleRepository>();
            var _carRepository = IocManager.Resolve<ICarRepository>();
            var _vinmasterTransmissionRepository = IocManager.Resolve<IVinMasterTransmissionRepository>();
            var _vinmasterDrivelineRepository = IocManager.Resolve<IVinMasterDrivelineRepository>();

            if (response.IsSuccess)
            {
                DealerVehicle dealerVehicle = _dealerVehicleRepository.GetDealerVehicle(model.VIN, model.ConfirmServicesRequest.CarId, model.Id, model.ConfirmServicesRequest.AppointmentPresentationId);
                var car = _carRepository.GetById(model.ConfirmServicesRequest.CarId);
                string transmission = _vinmasterTransmissionRepository.GetVinMasterTransmission(model.ConfirmServicesRequest.Transmission);
                string driveline = _vinmasterDrivelineRepository.GetVinMasterDriveline(model.ConfirmServicesRequest.Driveline);
                dealerVehicle = Mapper.Map(model, dealerVehicle, entity.Id, car, transmission, driveline);

                Vehicle vehicle = dealerVehicle.Vehicle != null ? dealerVehicle.Vehicle : _vehicleRepository.GetVehicleForNewCustomer(model.ConfirmServicesRequest.CarId, model.VIN);
                string vinShort = model.VIN.Trim().Substring(0, 8) + model.VIN.Trim().Substring(9, 2);
                var carVinShort = car.CarVinShorts.FirstOrDefault(c => c.VinShort != null && c.VinShort == vinShort);
                vehicle = Mapper.Map(model, vehicle, vehicle != null ? vehicle.VinMasterId == null ? (carVinShort != null ? carVinShort.VinMasterId : vehicle.VinMasterId) : vehicle.VinMasterId : carVinShort != null ? carVinShort.VinMasterId : (int?)null);
                if (vehicle.Id == 0)
                {
                    _vehicleRepository.Insert(vehicle);
                }
                dealerVehicle.Vehicle = vehicle;
                if (dealerVehicle.Id > 0)
                {
                    _dealerVehicleRepository.Update(dealerVehicle);
                }
                else
                {
                    _dealerVehicleRepository.Insert(dealerVehicle);
                }

                response = _dealerVehicleRepository.CommitChanges();
            }

            response.SavedItemId = entity.Id;
            return response;
        }

        public string GetVinByCustomerSearchInfo(string customerName, string customerNumber, string customerPhoneNumber, string vehicleStock, string licensePlate)
        {
            return _repository.GetVinByCustomerSearchInfo(customerName, customerNumber, customerPhoneNumber, vehicleStock, licensePlate);
        }
    }
}
