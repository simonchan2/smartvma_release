﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.OutputModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.FileManager;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Services.Mappers;
using SmartVMA.Services.Validators;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class LofService : BaseServicePresentationService, ILofService
    {
        protected override bool IsLofMenu => true;

        private readonly ICompanyRepository _companyRepository;

        public LofService(ICompanyService companyService, IGeneralCarServiceRepository generalServicerRepository, IAppointmentPresentationService appointmentPresentationService, IDealerCustomerRepository dealerCustomerRepository, ICompanyRepository companyRepository, ICarService carService)
            : base(dealerCustomerRepository, companyService, generalServicerRepository, appointmentPresentationService, carService)
        {
            _companyRepository = companyRepository;
        }

        protected override MenuLevelServicesIndexViewModel GetServices(ServicesProcedureModel[] services)
        {
            var model = new MenuLevelServicesIndexViewModel();
            var menuLevels = CompanyService.GetSettingAsInteger(CompanySettingsNames.LOFMenuLevels);

            model.MenuLabelLevel1 = GetMenuLevelLabel((int)MenuLevelTypes.FirstLevel);
            model.MenuLevel1Services = services.Where(x => x.MenuLevel == (int)MenuLevelTypes.FirstLevel).Select(x => Mapper.Map(x));
            model.MenuLabelLevel1Price = CalculateMenuPrice(model.MenuLevel1Services);
            model.MenuImagesLevel1 = GetBgProductImages((int)MenuLevelTypes.FirstLevel);
            if (menuLevels >= (int)MenuLevelTypes.SecondLevel && services.Any(x => x.MenuLevel >= (int)MenuLevelTypes.SecondLevel))
            {
                model.MenuLabelLevel2 = GetMenuLevelLabel((int)MenuLevelTypes.SecondLevel);
                model.MenuLevel2Services = services.Where(x => x.MenuLevel <= (int)MenuLevelTypes.SecondLevel).Select(x => Mapper.Map(x));
                model.MenuLabelLevel2Price = CalculateMenuPrice(model.MenuLevel2Services);
                model.MenuImagesLevel2 = GetBgProductImages((int)MenuLevelTypes.SecondLevel);
                if (menuLevels == (int)MenuLevelTypes.ThirdLevel && services.Any(x => x.MenuLevel == (int)MenuLevelTypes.ThirdLevel))
                {
                    model.MenuLabelLevel3 = GetMenuLevelLabel((int)MenuLevelTypes.ThirdLevel);
                    model.MenuLevel3Services = services.Where(x => x.MenuLevel <= (int)MenuLevelTypes.ThirdLevel).Select(x => Mapper.Map(x));
                    model.MenuLabelLevel3Price = CalculateMenuPrice(model.MenuLevel3Services);
                    model.MenuImagesLevel3 = GetBgProductImages((int)MenuLevelTypes.ThirdLevel);
                }
            }
            return model;
        }

        public ConfigurationLOFMenuViewModel GetLOFMenuViewModel(int? id = null, int level = 1)
        {
            var model = new ConfigurationLOFMenuViewModel { LOFMenuSelectedLevel = level };
            var appContext = IocManager.Resolve<IAppContext>();
            if (!id.HasValue)
            {
                id = appContext.CurrentIdentity.TenantId;
            }

            var company = _companyRepository.GetCompanyAndSettings(id.Value);
            return Mapper.Map(company, model);
        }

        public List<LOFMenuImage> GetLOFImages()
        {
            var result = new List<LOFMenuImage>();
            var fileManager = IocManager.Resolve<IFileManager>();
            var appContext = IocManager.Resolve<IAppContext>();

            var listName = new List<string>();
            listName = fileManager.GetAllFilesFromFolder();

            for (int i = 1; i <= 3; i++)
            {
                foreach (string name in listName)
                {
                    var item = new LOFMenuImage();
                    item.ImageName = name;
                    item.ImageUrl = appContext.MapUrl($@"{appContext.LOFImagesFolder}{name}");
                    item.IsSelected = false;
                    item.Id = Guid.NewGuid();
                    item.LOFMenuLevel = i;
                    result.Add(item);
                }
            }

            return result;
        }

        public ServiceResponse SaveLOFMenuImages(int companyID, string imageName, bool isSelected, int selectedLOFMenuLevel)
        {
            var company = _companyRepository.GetCompanyAndSettings(companyID);
            char[] charSeparator = new char[] { ';' };
            var model = GetLOFMenuViewModel(companyID, selectedLOFMenuLevel);
            switch (selectedLOFMenuLevel)
            {
                case 1:
                   //? if (company.CompanySettings.FirstOrDefault(x => x.Name == "LOFMenuLevel1Images") != null)
                    {
                        string[] result;
                        var LOFMenuLevel1Images =model.LOFMenuLevel1Images==null ? "": model.LOFMenuLevel1Images;
                        result = LOFMenuLevel1Images.Split(charSeparator, StringSplitOptions.None);
                        result = result.Where(w => w != "").ToArray();
                        if ((isSelected == true) && (result.Length <= 7))
                        {
                            LOFMenuLevel1Images = LOFMenuLevel1Images == string.Empty ? imageName : (LOFMenuLevel1Images + ";" + imageName);
                            model.LOFMenuLevel1Images = LOFMenuLevel1Images;
                        }
                        else
                        {
                            result = result.Where(w => w != imageName).ToArray();
                            LOFMenuLevel1Images = string.Empty;
                            if (result.Length > 0)
                            {
                                foreach (string item in result)
                                {
                                    LOFMenuLevel1Images = LOFMenuLevel1Images + item + ";";
                                }                                
                            }
                            model.LOFMenuLevel1Images = LOFMenuLevel1Images == null? "" : LOFMenuLevel1Images;
                        }
                        model.LOFMenuLevel2Images = model.LOFMenuLevel2Images == null ? "" : model.LOFMenuLevel2Images;
                        model.LOFMenuLevel3Images = model.LOFMenuLevel3Images == null ? "" : model.LOFMenuLevel3Images;
                    }
                    break;
                case 2:
                  //  if (company.CompanySettings.FirstOrDefault(x => x.Name == "LOFMenuLevel2Images") != null)
                    {
                        string[] result;
                        var LOFMenuLevel2Images = model.LOFMenuLevel2Images==null ? "" : model.LOFMenuLevel2Images;
                        result = LOFMenuLevel2Images.Split(charSeparator, StringSplitOptions.None);
                        result = result.Where(w => w != "").ToArray();
                        if ((isSelected == true) && (result.Length <= 7))
                        {
                            LOFMenuLevel2Images = LOFMenuLevel2Images == string.Empty ? imageName : (LOFMenuLevel2Images + ";" + imageName);
                            model.LOFMenuLevel2Images= LOFMenuLevel2Images;
                        }
                        else
                        {
                            result = result.Where(w => w != imageName).ToArray();
                            LOFMenuLevel2Images = string.Empty;
                            if (result.Length > 0)
                            {
                                foreach (string item in result)
                                {
                                    LOFMenuLevel2Images = LOFMenuLevel2Images + item + ";";
                                }
                            }
                          model.LOFMenuLevel2Images = LOFMenuLevel2Images == null ? "" : LOFMenuLevel2Images;
                        }

                        model.LOFMenuLevel1Images = model.LOFMenuLevel1Images == null ? "" : model.LOFMenuLevel1Images;
                        model.LOFMenuLevel3Images = model.LOFMenuLevel3Images == null ? "" : model.LOFMenuLevel3Images;
                    }
                    break;
                case 3:
                  ///  if (company.CompanySettings.FirstOrDefault(x => x.Name == "LOFMenuLevel3Images") != null)
                    {
                        string[] result;
                        var LOFMenuLevel3Images = model.LOFMenuLevel3Images==null ? "" : model.LOFMenuLevel3Images;
                        result = LOFMenuLevel3Images.Split(charSeparator, StringSplitOptions.None);
                        result = result.Where(w => w != "").ToArray();
                        if ((isSelected == true) && (result.Length <= 7))
                        {
                            LOFMenuLevel3Images = LOFMenuLevel3Images==string.Empty ? imageName : (LOFMenuLevel3Images + ";" + imageName);
                            model.LOFMenuLevel3Images= LOFMenuLevel3Images;
                        }
                        else
                        {
                            result = result.Where(w => w != imageName).ToArray();
                            LOFMenuLevel3Images = string.Empty;
                            if (result.Length > 0)
                            {
                                foreach (string item in result)
                                {
                                    LOFMenuLevel3Images = LOFMenuLevel3Images + item + ";";
                                }                                
                            }
                            model.LOFMenuLevel3Images = LOFMenuLevel3Images == null ? "" : LOFMenuLevel3Images;
                        }
                        model.LOFMenuLevel1Images = model.LOFMenuLevel1Images == null ? "" : model.LOFMenuLevel1Images;
                        model.LOFMenuLevel2Images = model.LOFMenuLevel2Images == null ? "" : model.LOFMenuLevel2Images;
                    }
                    break;

            }
            var settings = Mapper.Map(model);

            _companyRepository.Update(company, settings);
            return _companyRepository.CommitChanges();

        }

        public List<string> GetLevelImages(string LevelImages)
        {
            var images = new List<string>();
            var appContext = IocManager.Resolve<IAppContext>();

            char[] charSeparator = new char[] { ';' };
            string[] result;
            result = LevelImages.Split(charSeparator, StringSplitOptions.None);
            result = result.Where(w => w != "").ToArray();
            foreach (string item in result)
            {
                images.Add(appContext.MapUrl($@"{appContext.LOFImagesFolder}{item}"));                
            }
            return images;
        }
    }
}
