﻿using System.Collections.Generic;
using System.Linq;
using SmartVMA.Core.InputModels.ViewModels.Admin;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class BgSubProtectionPlanService: IBgSubProtectionPlanService
    {
        private readonly IBgSubProtectionPlanRepository _repository;

        public BgSubProtectionPlanService(IBgSubProtectionPlanRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<BgProtectionSubPlanViewModel> GetSubPlans(int bgProtectionPlanId)
        {
            return _repository.Find(x => x.BgProtectionPlanId == bgProtectionPlanId)
                .Select(x => new BgProtectionSubPlanViewModel {Id = x.Id, Description = x.SubPlanName});
        }

        public IEnumerable<BgProtectionSubPlanViewModel> GetSubPlans()
        {
            return _repository.Find()
               .Select(x => new BgProtectionSubPlanViewModel { Id = x.Id, Description = x.SubPlanName });
        }
    }
}
