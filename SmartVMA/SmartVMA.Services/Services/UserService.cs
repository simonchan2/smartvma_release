﻿using SmartVMA.Core.Services;
using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Core.Repositories;
using System.Web.Security;
using System.Collections.Generic;
using SmartVMA.Services.Mappers;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using SmartVMA.Core.Entities;
using SmartVMA.Resources;
using SmartVMA.Infrastructure.Mailer;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Core;
using SmartVMA.Core.Enums;
using SmartVMA.Infrastructure.FileManager;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Services.Validators;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class UserService : SettingsService, IUserService
    {
        private const string strKey = "ee523b07-115b-459d-8fd6-06aaa2d5fc82";
        private readonly IUserRepository _repository;
        private readonly IUserSettingRepository _settingRepository;
        private readonly ILanguageRepository _languageRepository;
        public UserService(IUserRepository repository, IUserSettingRepository settingRepository,ILanguageRepository languageRepository)
        {
            _repository = repository;
            _settingRepository = settingRepository;
            _languageRepository = languageRepository;
        }

        public ServiceResponse Delete(UserViewModel model)
        {
            var user = _repository.FirstOrDefault(x => x.Id == model.Id);
            _repository.Delete(user);
            var response = _repository.CommitChanges();
            return response;
        }

        public List<SettingViewModel> GetAllSettings()
        {
            List<SettingViewModel> companySettings = new List<SettingViewModel>(_settingRepository.GetAllSettings().Select(x => Mapper.Map(x, new SettingViewModel())));
            return companySettings;
        }

        public UserIndexViewModel GetIndexViewModel(PagingRequest request, int? companyId = null)
        {
            var entities = _repository.GetPagedResults(request, companyId);
            var itemsCount = _repository.GetPagedResultsTotal(request, companyId);

            var model = new UserIndexViewModel
            {
                Items = new List<UserViewModel>(entities.Select(x => Mapper.Map(x, getCompanyNameAsRawHtml: true))),
                ItemsCount = itemsCount,
                RecordsFilteredCount = itemsCount,
                Draw = request.Draw + 1
            };
            return model;
        }

        public UserViewModel GetViewModel(int? id = null)
        {
            var model = new UserViewModel();
            if (!id.HasValue)
            {
                return model;
            }

            var user = _repository.GetUserById(id.Value);
            model = Mapper.Map(user, model);
            return model;
        }

        public UserViewModel GetViewModel(int companyId, UserRoles userRole)
        {
            var model = new UserViewModel
            {
                Tenants = new List<int> { companyId },
                RoleId = (int)userRole
            };
            var user = _repository.GetDefaultUserForCompany(companyId, userRole);
            if (user != null)
            {
                model = Mapper.Map(user, model);
            }
            return model;
        }

        public UserIndexViewModel GetUsersByRole(int companyId, UserRoles userRole)
        {
            var model = new UserIndexViewModel();
            var items = _repository.GetAllUsersByRole(companyId, userRole);
            model.Items = items.Select(x => Mapper.Map(x, new UserViewModel()));

            return model;

        }

        public LoginResponseViewModel Login(LoginViewModel model)
        {
            var response = new LoginResponseViewModel();
            var user = _repository.GetUserByEmail(model.Email);
            if (user == null)
            {
                response.Errors.Add("Email", Messages.InvalidUserNameOrPassword);
                return response;
            }

            if (user.UserStatusCodeId == (int)UserStatusCodes.Disabled)
            {
                response.Errors.Add("Disabled", Messages.UserDisabled);
                return response;
            }

            if (Compare(HashPassword(model.Password), user.PasswordHash) && user.LoginAttempts <= StaticSettings.MaximumAllowedLoginAttempts - 1)
            {
                var userRole = user.UserCompanyRoles.Count > 1
                    ? user.UserCompanyRoles.FirstOrDefault(x => x.TenantId == model.TenantId)
                    : user.UserCompanyRoles.FirstOrDefault();
                
                // {0}_{1}_{2}  userId, roleId, tenantId
                string userName;
                if (userRole != null)
                {
                    userName = $"{user.Id}_{userRole.RoleId}_{userRole.TenantId}";
                }
                else
                {
                    response.IsSuccess = true;
                    response.ShowStoreSelectPage = true;
                    response.Model = model;
                    var userSettings = _repository.GetUserAndSettings(user.Id);
                    var defaultTenantId = userSettings.UserSettings.FirstOrDefault(x => x.Name == UserSettingsNames.DefaultTenantId.ToString());
                    if (!string.IsNullOrEmpty(defaultTenantId?.Value))
                    {
                        response.Model.TenantId = int.Parse(defaultTenantId.Value);
                    }
                    return response;
                }
                FormsAuthentication.SetAuthCookie(userName, model.RememberMe);
                response.IsSuccess = true;

                if (user.LoginAttempts > 0)
                {
                    user.LoginAttempts = 0;
                    _repository.Update(user);
                    _repository.CommitChanges();
                }
            }
            else
            {
                if (user.LoginAttempts == StaticSettings.MaximumAllowedLoginAttempts)
                {
                    response.Errors.Add("Password", Messages.AccountLockedOut);
                    return response;
                }

                response.Errors.Add("Password", user.LoginAttempts >= StaticSettings.WarningLoginNumberAttempts
                    ? Messages.WarningAccountWillBeLocked
                    : Messages.InvalidPassword);

                user.LoginAttempts += 1;
                _repository.Update(user);
                _repository.CommitChanges();
            }
            return response;
        }

        public ServiceResponse Save(UserViewModel model)
        {
            var response = Validator.Validate(model);
            if (!response.IsSuccess)
            {
                return response;
            }

            User user;
            var userCompanyRoleRepository = IocManager.Resolve<IUserCompanyRoleRepository>();
            if (model.Id.HasValue)
            {
                user = Mapper.Map(model, _repository.GetUserById(model.Id.Value));
                var deletedCompanyRoles = user.UserCompanyRoles.Where(x => model.Tenants.All(id => id != x.TenantId)).ToArray();
                foreach (var userCompanyRole in deletedCompanyRoles)
                {
                    userCompanyRoleRepository.Delete(userCompanyRole);
                }
                foreach (var tenantId in model.Tenants)
                {
                    var userCompanyRole = userCompanyRoleRepository.GetUserRole(user, tenantId);
                    if (userCompanyRole == null)
                    {
                        UserCompanyRole userRole = new UserCompanyRole();
                        userRole.User = user;
                        userRole.RoleId = model.RoleId ?? (int)UserRoles.Manager;
                        userRole.TenantId = tenantId;
                        userRole.DmsUserNumber = model.UserRoles.Where(x => x.TenantId == tenantId).First().DmsUserNumber;
                        userCompanyRoleRepository.Insert(userRole);
                    }
                    else
                    {
                        userCompanyRole.RoleId = model.RoleId ?? (int)UserRoles.Manager;
                        userCompanyRole.TenantId = tenantId;
                        userCompanyRole.DmsUserNumber = model.UserRoles.Where(x => x.TenantId == tenantId).First().DmsUserNumber;
                        userCompanyRoleRepository.Update(userCompanyRole);
                    }
                }
                _repository.Update(user);
                response = _repository.CommitChanges();
                return response;
            }

            user = Mapper.Map(model);
            user.PasswordHash = HashPassword(Guid.NewGuid().ToString());
            foreach (var tenantId in model.Tenants)
            {
                userCompanyRoleRepository.Insert(user, model.RoleId ?? (int)UserRoles.Manager, tenantId);
            }
            _repository.Insert(user);

            response = _repository.CommitChanges();
            if (response.IsSuccess)
            {
                CreateResetPasswordRequest(user.Email, user);
            }
            return response;
        }

        internal byte[] HashPassword(string password)
        {
            using (var md5 = MD5.Create())
            {
                return md5.ComputeHash(Encoding.UTF8.GetBytes(password));
            }
        }

        internal bool Compare(byte[] password, byte[] dbPassword)
        {
            return password.SequenceEqual(dbPassword);
        }

        public void LogOut()
        {
            FormsAuthentication.SignOut();
        }

        public ServiceResponse CreateResetPasswordRequest(PasswordForgotViewModel model)
        {
            var response = new ServiceResponse();
            var user = _repository.GetUserByEmail(model.Email);
            if (user != null)
            {
                return CreateResetPasswordRequest(model.Email, user);
            }
            response.Errors.Add("Email", Messages.InvalidEmailAddress);
            return response;
        }

        public ServiceResponse ResetPassword(PasswordResetViewModel model)
        {
            var response = new ServiceResponse();
            Guid guidToken;
            if (!Guid.TryParse(model.Token, out guidToken))
            {
                return response;
            }

            var paswordResetRequestRepository = IocManager.Resolve<IPasswordResetRequestRepository>();
            var paswordResetRequest = paswordResetRequestRepository.GetRequestByToken(guidToken);
            if (paswordResetRequest == null)
            {
                response.Errors.Add("Token", "[This url is no longer valid]");
                return response;
            }

            var user = _repository.GetUserByEmail(paswordResetRequest.Email);
            user.PasswordHash = HashPassword(model.Password);
            user.LoginAttempts = 0;

            paswordResetRequestRepository.Delete(paswordResetRequest);
            _repository.Update(user);
            var saveStatus = _repository.CommitChanges();
            return saveStatus.IsSuccess ? Login(new LoginViewModel { Email = user.Email, Password = model.Password }) : response;
        }

        public ServiceResponse ChangePassword(PasswordChangeViewModel model)
        {
            var response = new ServiceResponse();
            var appContext = IocManager.Resolve<IAppContext>();
            var currenttIdentity = appContext.CurrentIdentity;
            if (currenttIdentity.UserId != null)
            {
                var user = _repository.GetUserById(currenttIdentity.UserId.Value);
                if (!Compare(HashPassword(model.OldPassword), user.PasswordHash))
                {
                    response.Errors.Add("OldPassword", Messages.InvalidPassword);
                    return response;
                }
                user.PasswordHash = HashPassword(model.Password);
                _repository.Update(user);
            }

            response = _repository.CommitChanges();
            return response;
        }

        public ServiceResponse CreateResetPasswordRequest(int id)
        {
            var user = _repository.GetUserById(id);
            return CreateResetPasswordRequest(user.Email, user);
        }

        internal ServiceResponse CreateResetPasswordRequest(string email, User user)
        {
            var paswordResetRequestRepository = IocManager.Resolve<IPasswordResetRequestRepository>();
            var passwordResetRequest = new PasswordResetRequest
            {
                Email = email,
                Token = Guid.NewGuid()
            };
            paswordResetRequestRepository.Insert(passwordResetRequest);
            var response = paswordResetRequestRepository.CommitChanges();

            if (!response.IsSuccess)
            {
                return response;
            }            

            var mailer = IocManager.Resolve<IMailManager>();
            var appContext = IocManager.Resolve<IAppContext>();
            var ttl = Encrypt(DateTime.Now.AddMinutes(StaticSettings.ExpirationLinkDuration).Ticks.ToString());
            var url = appContext.MapUrl($"/Admin/Accounts/PasswordReset?t={passwordResetRequest.Token}&ttl={ttl}");
            var mailBody = string.Format(Emails.ResetPasswordEmail, user.FirstName, url);

            mailer.Send(passwordResetRequest.Email, Emails.ResetPasswordSubject, mailBody);
            return response;
        }

        public AccountManagementViewModel GetManageAccountViewModel()
        {
            var appContext = IocManager.Resolve<IAppContext>();
            var currentIdentity = appContext.CurrentIdentity;

            var userSettings = _repository.GetUserAndSettings(currentIdentity.UserId ?? 0);
            var model = Mapper.Map(userSettings, appContext.CurrentUserRoleName, new AccountManagementViewModel());

            return model;
        }

        public ServiceResponse Save(AccountManagementViewModel model)
        {
            var user = _repository.GetUserAndSettings(model.Id);

            if (!string.IsNullOrEmpty(model.OldPassword) && string.IsNullOrEmpty(model.ConfirmPassword))
            {
                var erorResponse = new ServiceResponse();
                erorResponse.Errors.Add("Password", Messages.InvalidPassword);
                return erorResponse;
            }
            if (!string.IsNullOrEmpty(model.ConfirmPassword))
            {
                if (string.IsNullOrEmpty(model.OldPassword) || !Compare(HashPassword(model.OldPassword), user.PasswordHash))
                {
                    var erorResponse = new ServiceResponse();
                    erorResponse.Errors.Add("OldPassword", Messages.InvalidPassword);
                    return erorResponse;
                }
                user.PasswordHash = HashPassword(model.Password);
            }
            var settings = Mapper.Map(model);
            _repository.Update(user, settings);
            var response = _repository.CommitChanges();

            if (response.IsSuccess && !string.IsNullOrEmpty(model.UserImageFileName))
            {
                // move to logo to user images
                var fileManager = IocManager.Resolve<IFileManager>();
                var appContext = IocManager.Resolve<IAppContext>();
                var tempFilePath = $@"{appContext.TempFolderName}\{model.UserImageFileName}";
                var userContentFilePath = $@"{appContext.ImagesFolder}\{model.UserImageFileName}";
                fileManager.Move(tempFilePath, userContentFilePath);
            }
            return response;
        }

        public string GetUserImagesSettingAsString(UserSettingsNames setting)
        {
            var appContext = IocManager.Resolve<IAppContext>();
            string result = string.Empty;
            string settingValue = GetSettingAsString(setting);
            if (!string.IsNullOrEmpty(settingValue))
            {
                result = appContext.MapUrl($@"{appContext.ImagesFolder}{settingValue}");
            }
            return result;
        }

        public string GetLanguageCode(UserSettingsNames setting)
        {
            var appContext = IocManager.Resolve<IAppContext>();
            int result = 0;
            string settingValue = GetSettingAsString(setting);
            if (!string.IsNullOrEmpty(settingValue))
            {
                result = Convert.ToInt32(settingValue);
            }
            Language lang = _languageRepository.GetLanguage(result);
            return (lang != null ? lang.LanguageCode.ToUpper() : string.Empty);
        }

        public static string Encrypt(string strToEncrypt)
        {
            TripleDESCryptoServiceProvider objDESCrypto =
                new TripleDESCryptoServiceProvider();
            MD5CryptoServiceProvider objHashMD5 = new MD5CryptoServiceProvider();
            byte[] byteHash, byteBuff;
            string strTempKey = strKey;
            byteHash = objHashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strTempKey));
            objHashMD5 = null;
            objDESCrypto.Key = byteHash;
            objDESCrypto.Mode = CipherMode.ECB;
            byteBuff = ASCIIEncoding.ASCII.GetBytes(strToEncrypt);
            return Convert.ToBase64String(objDESCrypto.CreateEncryptor().
                TransformFinalBlock(byteBuff, 0, byteBuff.Length));
        }

        public string Decrypt(string strEncrypted)
        {
            TripleDESCryptoServiceProvider objDESCrypto =
                   new TripleDESCryptoServiceProvider();
            MD5CryptoServiceProvider objHashMD5 = new MD5CryptoServiceProvider();
            byte[] byteHash, byteBuff;
            string strTempKey = strKey;
            byteHash = objHashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strTempKey));
            objHashMD5 = null;
            objDESCrypto.Key = byteHash;
            objDESCrypto.Mode = CipherMode.ECB;
            byteBuff = Convert.FromBase64String(strEncrypted);
            string strDecrypted = ASCIIEncoding.ASCII.GetString
            (objDESCrypto.CreateDecryptor().TransformFinalBlock
            (byteBuff, 0, byteBuff.Length));
            objDESCrypto = null;
            return strDecrypted;
        }
    }
}