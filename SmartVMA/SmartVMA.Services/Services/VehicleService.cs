﻿using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using System.Collections.Generic;
using SmartVMA.Services.Mappers;
using System;
using SmartVMA.Core.Entities;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class VehicleService : IVehicleService
    {
        private readonly IVehicleRepository _repository;
        public VehicleService(IVehicleRepository repository)
        {
            _repository = repository;
        }
    }
}
