﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Services.Mappers;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class OemServicePartService : IOemServicePartService
    {
        private readonly IOemServicePartRepository _repository;

        public OemServicePartService(IOemServicePartRepository repository)
        {
            _repository = repository;
        }

        public OemServicePartViewModel GetOemServicePartById(int id)
        {
            return Mapper.Map(_repository.GetOemServicePartById(id));
        }

        public ServiceResponse Save(OemServicePartViewModel model)
        {
            if (model.Id.HasValue) 
            {
                var oemPart = Mapper.Map(model, _repository.FirstOrDefault(x => x.Id == model.Id.Value && x.IsDeleted == false));
                _repository.Update(oemPart);
            }
            else
            {
                var oemPart = Mapper.Map(model);
                _repository.Insert(oemPart);
            }

            var response = _repository.CommitChanges(); 
            return response;
        }
    }
}
