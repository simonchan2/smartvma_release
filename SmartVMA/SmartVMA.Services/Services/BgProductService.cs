﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Resources;
using SmartVMA.Services.Mappers;
using System.Collections.Generic;
using System.Linq;
using SmartVMA.Infrastructure.Comparer;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class BgProductService : IBgProductService
    {

        private readonly IBgProdCategoryRepository _repository;

        private readonly IBgProductsProtectionPlansRepository _repositoryProtectionPlans;
        private readonly IBgProductsCategoryRepository _repositoryProductsCategory;
        private readonly IAppContext _appContext;
        private readonly IBgProductRepository _repositoryBgProducts;
        public BgProductService(IBgProdCategoryRepository repository, IBgProductsProtectionPlansRepository repositoryProtectionPlans, IBgProductRepository repositoryBgProducts, IBgProductsCategoryRepository repositoryProductsCategory, IAppContext appContext)
        {
            _repository = repository;
            _appContext = appContext;
            _repositoryProtectionPlans = repositoryProtectionPlans;
            _repositoryBgProducts = repositoryBgProducts;
            _repositoryProductsCategory = repositoryProductsCategory;
        }

        public List<BgProductCategoryInfo> GetAllBgProductCategories()
        {
            return _repository.GetAllBgProductCategories();
        }

        public List<BgProductCategoryInfo> GetAllBgProductCategories(List<int> bgProducts)
        {
            return _repository.GetAllBgProductCategories(bgProducts);
        }

        public BgProductViewModel GetBgProductProtectionPlanBySKUs(int id)
        {
            var model = new BgProductViewModel();
            return Mapper.Map(_repositoryBgProducts.GetBgProductProtectionPlanBySKUs(id), model);
        }

        public ServiceResponse SaveBgProduct(BgProductViewModel model)
        {
            ServiceResponse response = new ServiceResponse();
            response.IsSuccess = true;

            var selectedBgProtectionPlans = new List<int>();
            if (model.isCheckedLifetimeProtection) selectedBgProtectionPlans.Add(1);
            if (model.isCheckedForeverDiesel) selectedBgProtectionPlans.Add(2);
            if (model.isCheckedEngineAssurance) selectedBgProtectionPlans.Add(3);

            if (model.BgProductId != 0)
            {
                var bgprojectProtectionPlans = _repositoryProtectionPlans.Find().Where(x => x.BgProductId == model.BgProductId);

                if (_repositoryProtectionPlans.Find().Where(x => x.BgProductId == model.BgProductId).FirstOrDefault() == null)
                {
                    var product = Mapper.Map(model, _repositoryBgProducts.FirstOrDefault(x => x.Id == model.BgProductId));
                    _repositoryBgProducts.Update(product);

                    if (model.SelectCategories != null)
                    {
                        foreach (var catitem in model.SelectCategories) // add or delete categories for this bgproduct
                        {
                            //if this is new added category, add the relationship in db
                            if (!_repositoryProductsCategory.Find().Any(x => x.BgProdCategoryId == catitem && x.BgProductId == model.BgProductId))
                            {
                                _repositoryProductsCategory.Insert(new BgProductsCategory { BgProductId = model.BgProductId, BgProdCategoryId = catitem });
                            }
                            var bgProductCategories = _repositoryProductsCategory.Find().Where(x => x.BgProductId == model.BgProductId);
                            foreach (var cat in _repositoryProductsCategory.Find().Where(x => x.BgProductId == model.BgProductId))
                            {
                                //if the category Id is removed from BgProduct.SelectedCategories list, remove the relationship in DB
                                if (!model.SelectCategories.Contains(cat.BgProdCategoryId))
                                {
                                    _repositoryProductsCategory.Delete(cat);
                                }
                            }
                        }
                    }
                    response = _repositoryBgProducts.CommitChanges();
                }


                //Insert new items in BgProjectProtectionPlans (ProjectPlanId/ ProductId)
                foreach (var item in selectedBgProtectionPlans)
                {
                    if (!_repositoryProtectionPlans.Find().Any(x => x.BgProductId == model.BgProductId && x.BgProtectionPlanId == item))
                    {
                        _repositoryProtectionPlans.Insert(new BgProductsProtectionPlan { BgProductId = model.BgProductId, BgProtectionPlanId = item });

                        var product = Mapper.Map(model, _repositoryBgProducts.FirstOrDefault(x => x.Id == model.BgProductId));
                        _repositoryBgProducts.Update(product);
                        if (model.SelectCategories != null)
                        {
                            foreach (var catitem in model.SelectCategories) // add or delete categories for this bgproduct
                            {
                                //if this is new added category, add the relationship in db
                                if (!_repositoryProductsCategory.Find().Any(x => x.BgProdCategoryId == catitem && x.BgProductId == model.BgProductId))
                                {
                                    _repositoryProductsCategory.Insert(new BgProductsCategory { BgProductId = model.BgProductId, BgProdCategoryId = catitem });
                                }
                                var bgProductCategories = _repositoryProductsCategory.Find().Where(x => x.BgProductId == model.BgProductId);
                                foreach (var cat in _repositoryProductsCategory.Find().Where(x => x.BgProductId == model.BgProductId))
                                {
                                    //if the category Id is removed from BgProduct.SelectedCategories list, remove the relationship in DB
                                    if (!model.SelectCategories.Contains(cat.BgProdCategoryId))
                                    {
                                        _repositoryProductsCategory.Delete(cat);
                                    }
                                }
                            }
                        }
                    }
                }

                //Update already existing items in BgProjectProtectionPlans
                foreach (var item in bgprojectProtectionPlans)
                {
                    if ((item.BgProductId == model.BgProductId) && selectedBgProtectionPlans.Contains(item.BgProtectionPlanId))
                    {
                        //update already existing
                        var product = Mapper.Map(model, _repositoryBgProducts.FirstOrDefault(x => x.Id == model.BgProductId));
                        _repositoryBgProducts.Update(product);

                        if (model.SelectCategories != null)
                        {
                            foreach (var catitem in model.SelectCategories) // add or delete categories for this bgproduct
                            {
                                //if this is new added category, add the relationship in db
                                if (!_repositoryProductsCategory.Find().Any(x => x.BgProdCategoryId == catitem && x.BgProductId == model.BgProductId))
                                {
                                    _repositoryProductsCategory.Insert(new BgProductsCategory { BgProductId = model.BgProductId, BgProdCategoryId = catitem });
                                }
                                var bgProductCategories = _repositoryProductsCategory.Find().Where(x => x.BgProductId == model.BgProductId);
                                foreach (var cat in _repositoryProductsCategory.Find().Where(x => x.BgProductId == model.BgProductId))
                                {
                                    //if the category Id is removed from BgProduct.SelectedCategories list, remove the relationship in DB
                                    if (!model.SelectCategories.Contains(cat.BgProdCategoryId))
                                    {
                                        _repositoryProductsCategory.Delete(cat);
                                    }
                                }
                            }
                        }
                    }
                }

                //Delete - if the ProtectionPlanId is removed from BgProductProtectionPlans, remove the relationship in DB
                foreach (var projectPlan in _repositoryProtectionPlans.Find().Where(x => x.BgProductId == model.BgProductId))
                {
                    //if ProtectionPlanId is removed from BgProductProtectionPlans, remove the relationship in DB
                    if (!selectedBgProtectionPlans.Contains(projectPlan.BgProtectionPlanId))
                    {
                        _repositoryProtectionPlans.Delete(projectPlan);
                    }
                }

                response = _repositoryProtectionPlans.CommitChanges();
            }
            else
            {

                var product = new BgProduct();
                var bgProduct = Mapper.Map(model, product);
                _repositoryBgProducts.Insert(bgProduct);
                response = _repositoryBgProducts.CommitChanges();
                if (model.SelectCategories != null)
                {
                    foreach (var catId in model.SelectCategories)
                    {
                        var bgproduccat = new BgProductsCategory();
                        _repositoryProductsCategory.Insert(Mapper.Map(bgProduct, catId));
                    }
                    response = _repositoryProductsCategory.CommitChanges();
                }

                if (selectedBgProtectionPlans.Count != 0)
                {
                    foreach (var item in selectedBgProtectionPlans)
                    {
                        model.BgProductId = bgProduct.Id;
                        var bgProductsProtectionPlans = Mapper.Map(model, item);
                        _repositoryProtectionPlans.Insert(bgProductsProtectionPlans);

                    }
                    response = _repositoryProtectionPlans.CommitChanges();
                }
            }

            return response;
        }

        public BgProductIndexViewModel GetPagedItems(PagingRequest request, string categoryIds)
        {
            var entities = _repositoryBgProducts.GetPagedResults(request, categoryIds);
            var items = new List<BgProductViewModel>(entities.Select(x => Mapper.Map(x)));
            var model = new BgProductIndexViewModel { Items = items };
            return model;
        }

        public ServiceResponse Delete(BgProductViewModel model)
        {
            ServiceResponse response = new ServiceResponse();
            response.IsSuccess = true;

            var selectedBgProtectionPlans = new List<int>();
            if (model.isCheckedLifetimeProtection) selectedBgProtectionPlans.Add(1);
            if (model.isCheckedForeverDiesel) selectedBgProtectionPlans.Add(2);
            if (model.isCheckedEngineAssurance) selectedBgProtectionPlans.Add(3);

            //Delete - if the ProtectionPlanId is removed from BgProductProtectionPlans, remove the relationship in DB
            foreach (var projectPlan in _repositoryProtectionPlans.Find().Where(x => x.BgProductId == model.Id))
            {
                //if ProtectionPlanId is removed from BgProductProtectionPlans, remove the relationship in DB
                if (selectedBgProtectionPlans.Contains(projectPlan.BgProtectionPlanId))
                {
                    _repositoryProtectionPlans.Delete(projectPlan);
                }
            }
            response = _repositoryProtectionPlans.CommitChanges();

            var bgProduct = _repositoryBgProducts.FirstOrDefault(x => x.Id == model.Id);
            foreach (var prodcat in _repositoryProductsCategory.Find().Where(x => x.BgProductId == model.Id))
            {
                _repositoryProductsCategory.Delete(prodcat);
            }
            _repositoryBgProducts.Delete(bgProduct);
            response = _repositoryBgProducts.CommitChanges();

            return response;
        }

        public ServiceResponse PartNumberValidation(BgProductViewModel model)
        {
            var partnumber = _repositoryBgProducts.FirstOrDefault(x => x.PartNumber == model.PartNumber && x.Id != model.BgProductId);
            var response = new ServiceResponse();
            if ((partnumber != null) || (model.SelectCategories == null) || (model.isCheckedEngineAssurance == false && model.isCheckedForeverDiesel == false && model.isCheckedLifetimeProtection == false))
            {
                response.IsSuccess = false;
                if (partnumber != null)
                    response.Errors.Add("PartNumber", Messages.BgPartNumberMustBeUniq);
                if (model.isCheckedEngineAssurance == false && model.isCheckedForeverDiesel == false && model.isCheckedLifetimeProtection == false)
                    response.Errors.Add("isCheckedEngineAssurance", Messages.ProtectionPlanSelectedValidation);
            }
            else
            {
                response.IsSuccess = true;
            }

            return response;
        }

        public List<BgProductViewModel> GetBgProducts()
        {
            var comparer = new AlphaNumericStringComparer();
            return _repositoryBgProducts.Find().Select(x => Mapper.Map(x, new BgProductViewModel())).OrderBy(x => x.PartNumber, comparer).ToList();
        }
    }
}
