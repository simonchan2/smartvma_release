﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Services.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SmartVMA.Services
{
    [IocBindable(ScopeType = InstanceScopeType.PerRequest)]
    internal class PermissionService : IPermissionService
    {
        private readonly IPermissionRepository _repository;
        private readonly IRolePermissionRepository _rolePermissionsRepository;
        private readonly IAppContext _appContext;
        public PermissionService(IPermissionRepository repository, IRolePermissionRepository rolePermissionsRepository, IAppContext appContext)
        {
            _repository = repository;
            _rolePermissionsRepository = rolePermissionsRepository;
            _appContext = appContext;
        }

        public ServiceResponse GrantPermissionsForRole(PermissionSaveViewModel model)
        {
            var grantedRolePermissions = new List<RolePermission>(_rolePermissionsRepository.GetRoleWithPermissions(model.RoleId));

            // adds relation for the newly granted permissions
            var allPermissions = new List<Permission>(_repository.Find().ToList());
            foreach (var item in model.PermissionNames)
            {
                if (!grantedRolePermissions.Any(x => x.Permission.Name == item))
                {
                    var permission = allPermissions.FirstOrDefault(x => x.Name == item);
                    var rolePermission = new RolePermission
                    {
                        RoleId = model.RoleId,
                        PermissionId = permission.Id
                    };

                    _rolePermissionsRepository.Insert(rolePermission);
                }
            }

            // removes the relation between the role and the permission for denied permissions
            var deniedPermissionForRole = grantedRolePermissions.Where(r => model.PermissionNames.All(x => x != r.Permission.Name));
            if (deniedPermissionForRole.Any())
            {
                _rolePermissionsRepository.Delete(deniedPermissionForRole);
            }

            var response = _rolePermissionsRepository.CommitChanges();
            return response;
        }

        public PermissionIndexViewModel GetIndexViewModel(int roleId)
        {
            var items = new List<PermissionViewModel>();
            foreach (var item in _repository.GetPermissionsWithRoleInfo())
            {
                items.Add(Mapper.Map(item, roleId));
            }

            var roleRepository = IocManager.Resolve<IRoleRepository>();
            var role = roleRepository.FirstOrDefault(x => x.Id == roleId);

            var model = new PermissionIndexViewModel
            {
                RoleId = roleId,
                RoleName = role.Role1,
                Items = items.OrderBy(x => x.Name)
            };
            return model;
        }

        public ServiceResponse UpdatePermissions()
        {
            var permissions = _repository.Find();
            foreach (var item in GetAllControlActions())
            {
                if (!permissions.Any(x => x.Name == item))
                {
                    var permission = new Permission { Name = item };
                    _repository.Insert(permission);
                }
            }

            var response = _repository.CommitChanges();
            return response;
        }

        private IEnumerable<string> GetAllControlActions()
        {
            var result = new List<string>();

            var controllers = AppDomain.CurrentDomain.GetAssemblies()
                .Where(x => x.FullName.Contains("Web"))
                .SelectMany(x => x.GetTypes())
                .Where(x => x.Name.EndsWith("Controller") && !x.IsAbstract);

            foreach (var item in controllers)
            {
                var entityName = item.Name.Replace("Controller", "");

                //Added change for methods ending with PdfFile even they are void!
                foreach (var action in item.GetMethods().Where(x => x.IsPublic && (x.ReturnType.Name.Contains("Result") || x.Name.EndsWith("PdfFile") || x.Name == "GetUploadImage" || x.Name == "CreateAppointmentPresentation")))
                {
                    if (!action.GetCustomAttributes().Any(x => x.GetType().Name.Contains("AllowAnonymous")))
                    {
                        var actionNameAttr = action.GetCustomAttributes().FirstOrDefault(x => x.GetType().Name.Contains("ActionName"));
                        if (actionNameAttr != null)
                        {
                            var actionName = actionNameAttr.GetType().GetProperty("Name").GetValue(actionNameAttr);
                            result.Add(string.Format("{0}.{1}", entityName, actionName));
                        }
                        else
                        {
                            result.Add(string.Format("{0}.{1}", entityName, action.Name));
                        }
                    }
                }
            }
            return result.Distinct();
        }

        public PermissionViewModel GetViewModel(int id, int roleId)
        {
            var permission = _repository.GetPermissionsWithRoleInfo().FirstOrDefault(x => x.Id == id);
            var model = Mapper.Map(permission, roleId);
            return model;
        }

        public ServiceResponse Save(PermissionViewModel model)
        {
            var permission = _repository.GetPermissionsWithRoleInfo().FirstOrDefault(x => x.Id == model.Id);
            _repository.Update(Mapper.Map(model, permission));
            var response = _repository.CommitChanges();

            return response;
        }

        public bool IsPermissionGrantedForCurrentUser(string actionName, string controllerName)
        {
            var currentIdentity = _appContext.CurrentIdentity;
            if (controllerName == "Errors" || actionName == "LogOut" || currentIdentity.RoleId == (int)UserRoles.SystemAdministrator)
            {
                return true;
            }

            var permissionName = string.Format("{0}.{1}", controllerName, actionName);
            var result = _repository.IsPermissionGranted(permissionName, currentIdentity.UserId, currentIdentity.RoleId, currentIdentity.TenantId);
            return result;
        }

        public PermissionIndexViewModel GetPagedItems(PagingRequest request, int roleId)
        {
            var entities = _repository.GetPagedResults(request);
            var items = new List<PermissionViewModel>(entities.Select(x => Mapper.Map(x, roleId)));
            var model = new PermissionIndexViewModel();
            model.Items = items;
            var total = _repository.GetPagedResultsTotal(request);
            model.ItemsCount = total;

            return model;
        }
    }
}
