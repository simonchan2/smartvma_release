﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Contracts;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class PrnSpoolService : IPrnSpoolService
    {
        private readonly IPrnSpoolRepository _prnSpoolRepository;

        public PrnSpoolService(IPrnSpoolRepository prnSpoolRepository)
        {
            _prnSpoolRepository = prnSpoolRepository;
        }

        public string GetPdfPath(string account)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<PrnSpoolViewModel> GetRemotePrintJobs(string accounts)
        {
            IEnumerable<PrnSpool> printJobs = _prnSpoolRepository.GetRemotePrintJobs(accounts);
            List<PrnSpoolViewModel> result = (from pj in printJobs
                                              select new PrnSpoolViewModel
                                              {
                                                  Id=pj.Id, 
                                                  account = pj.account,
                                                  idhist = pj.idhist,
                                                  doctype = pj.doctype,
                                                  docname = pj.docname,
                                                  prnname = pj.prnname,
                                                  prndriver = pj.prndriver,
                                                  prncopy = pj.prncopy,
                                                  prnport = pj.prnport,
                                                  prntray = pj.prntray,
                                                  datecreated = pj.datecreated,
                                                  dateprinted = pj.dateprinted,
                                                  status = pj.status,
                                                  comment = pj.comment,
                                                  assno = pj.assno,
                                                  session = pj.session,
                                                  TimeCreated = pj.TimeCreated,
                                                  TimeUpdated = pj.TimeUpdated
                                              }).ToList<PrnSpoolViewModel>();

            return result;
        }

        public ServiceResponse UpdateRemotePrintJobStatus(int id, string status)
        {
            var response = new ServiceResponse();

            try
            {
                _prnSpoolRepository.UpdateRemotePrintJobStatus(id, status);
                response.IsSuccess = true;
            }
            catch (Exception e)
            {
                response.Errors.Add("ErrorMessage", e.ToString());
            }
            return response;
        }
    }
}
