﻿using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Services.Mappers;
using System.Collections.Generic;
using System.Linq;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class EngineOilTypeService : IEngineOilTypeService
    {
        private readonly IEngineOilTypeRepository _repository;

        public EngineOilTypeService(IEngineOilTypeRepository repository)
        {
            _repository = repository;
        }

        public List<EngineOilTypeViewModel> GetAllEngineOilTypes()
        {
            var engineOilTypes = new List<EngineOilTypeViewModel>();
            engineOilTypes.AddRange(_repository.GetAllEngineOilTypes().Select(x => Mapper.Map(x)));

            return engineOilTypes;
        }
    }
}
