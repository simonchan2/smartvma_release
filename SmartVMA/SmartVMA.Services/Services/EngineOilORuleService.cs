﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Resources;
using SmartVMA.Services.Mappers;
using System.Collections.Generic;
using System.Linq;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class EngineOilORuleService : IEngineOilORuleService
    {
        private readonly IEngineOilORulesRepository _repository;
        private readonly IEngineOilProductRepository _productRepository;

        public EngineOilORuleService(IEngineOilORulesRepository repository, IEngineOilProductRepository productRepository)
        {
            _repository = repository;
            _productRepository = productRepository;
        }

        public EngineOilConfigurationIndexViewModel GetIndexViewModel()
        {
            var engineOil = new List<EngineOilConfigurationViewModel>();
            engineOil.AddRange(_repository.Find().Select(x => Mapper.Map(x)));

            var model = new EngineOilConfigurationIndexViewModel
            {
                Items = engineOil
            };
            return model;
        }

        public EngineOilConfigurationViewModel GetViewModel(int? id = null)
        {
            var model = new EngineOilConfigurationViewModel();
            if (!id.HasValue)
            {
                return model;
            }
            var engineOil = _repository.GetEngineOilORuleById(id.Value);
            model = Mapper.Map(engineOil, model);
            return model;
        }

        private ServiceResponse SameRuleExistsError()
        {
            var response = new ServiceResponse();
            response.Errors.Add(Labels.Error_OEMOilViscosity, Messages.SameRuleAlreadyExist);
            response.Errors.Add(Labels.Error_OEMOilType, Messages.SameRuleAlreadyExist);
            return response;
        }

        public ServiceResponse Save(EngineOilConfigurationViewModel model)
        {
            var sameExistingRule = _repository.GetEngineOilORuleByTypeAndViscosity(model.OEMOilType, model.OEMOilViscosity);
            if (model.Id.HasValue)
            {
                if (sameExistingRule == null || sameExistingRule.Id == model.Id)
                {

                    var engineOilProduct = Mapper.Map(model, _productRepository.FirstOrDefault(x => x.Id == model.EngineOilProductId));
                    _productRepository.Update(engineOilProduct);
                    _productRepository.CommitChanges();
                    var entity = Mapper.Map(model, _repository.FirstOrDefault(x => x.Id == model.Id.Value));
                    _repository.Update(entity);
                }
                else
                {
                    return SameRuleExistsError();
                }
            }
            else
            {
                var entity = Mapper.Map(model, new EngineOilORule());
                if (sameExistingRule == null)
                {
                    var engineOilProduct = Mapper.Map(model, new EngineOilProduct());
                    _productRepository.Insert(engineOilProduct);
                    var productResponce = _productRepository.CommitChanges();
                    if (productResponce.IsSuccess)
                    {
                        entity.EngineOilProductId = engineOilProduct.Id;
                    }

                    _repository.Insert(entity);
                }
                else
                {
                    return SameRuleExistsError();
                }
            }
            var response = _repository.CommitChanges();
            return response;
        }

        public ServiceResponse Delete(int id)
        {
            var engineOil = _repository.FirstOrDefault(x => x.Id == id);

            _repository.Delete(engineOil);
            var response = _repository.CommitChanges();
            if (engineOil != null && response.IsSuccess)
            {
                var engineOilProduct = _productRepository.Find(x => x.Id == engineOil.EngineOilProductId);
                _productRepository.Delete(engineOilProduct);
            }
            return response;
        }

        public EngineOilConfigurationIndexViewModel GetAllEngineOilORuleForDealer()
        {
            var items = new List<EngineOilConfigurationViewModel>();
            var engineOilRules = _repository.GetAllEngineOilORuleForDealer();
            items.AddRange(engineOilRules.Select(x => Mapper.Map(x)));

            var model = new EngineOilConfigurationIndexViewModel
            {
                Items = items
            };
            return model;
        }

        public EngineOilConfigurationIndexViewModel GetPagedItems(PagingRequest request)
        {
            var entities = _repository.GetPagedResults(request);
            var items = new List<EngineOilConfigurationViewModel>(entities.Select(x => Mapper.Map(x)));
            var model = new EngineOilConfigurationIndexViewModel
            {
                Items = items
            };
            var total = _repository.GetPagedResultsTotal(request);
            model.ItemsCount = total;

            return model;
        }


        public EngineOilConfigurationViewModel GetEngineOilOverride(string oilType, string oilViscosity, int? menuLevel = null)
        {
            var entity = _repository.GetEngineOilOverride(oilType, oilViscosity, menuLevel);
            if (entity != null)
            {
                return Mapper.Map(entity, new EngineOilConfigurationViewModel());
            }

            return null;
        }
    }
}

