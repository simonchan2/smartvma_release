﻿using SmartVMA.Core.Services;
using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Core.Repositories;
using SmartVMA.Services.Mappers;
using SmartVMA.Infrastructure.FileManager;
using SmartVMA.Core.Enums;
using SmartVMA.Core.Entities;
using SmartVMA.Infrastructure.AppContext;
using System;
using SmartVMA.Resources;
using System.Linq;
using SmartVMA.Services.Validators;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class ConfigurationSetupService : IConfigurationSetupService
    {
        private readonly ICompanyRepository _companyRepository;
        public ConfigurationSetupService(ICompanyRepository companyRepository)
        {
            _companyRepository = companyRepository;
        }

        public ConfigurationApplicationSettingViewModel GetApplicationSettingsViewModel(int id)
        {
            var model = new ConfigurationApplicationSettingViewModel();
            var company = _companyRepository.GetCompanyAndSettings(id);
            model = Mapper.Map(company, model);
            return model;
        }

        public ConfigurationCorpApplicationSettingsViewModel GetCorpApplicationSettings(int id)
        {
            var model = new ConfigurationCorpApplicationSettingsViewModel
            {
                DataRefreshFrequencyInDays = 30,
                SessionExpiration = 180
            };

            var company = _companyRepository.GetCompanyAndSettings(id);
            model = Mapper.Map(company, model);
            return model;
        }

        public ConfigurationDocumentSetupViewModel GetDocumentSetupViewModel(int id)
        {
            var model = new ConfigurationDocumentSetupViewModel();
            var company = _companyRepository.GetCompanyAndSettings(id);
            model = Mapper.Map(company, model);
            return model;
        }

        public ConfigurationMenuSetupViewModel GetMenuSetupViewModel(int id)
        {
            var model = new ConfigurationMenuSetupViewModel();
            var company = _companyRepository.GetCompanyAndSettings(id);
            model = Mapper.Map(company, model);
            return model;
        }

        public ConfigurationProfileSetupViewModel GetProfileSetupViewModel(int? id = null)
        {
            var model = new ConfigurationProfileSetupViewModel();
            if (id.HasValue)
            {
                var company = _companyRepository.GetCompanyAndSettings(id.Value);
                return Mapper.Map(company, model);
            }
            return model;
        }

        public ConfigurationProfileSetupDistributorViewModel GetProfileSetupDistributorViewModel(int? id = null)
        {
            var model = new ConfigurationProfileSetupDistributorViewModel();
            if (id.HasValue)
            {
                var company = _companyRepository.GetCompanyAndSettings(id.Value);
                return Mapper.Map(company, model);
            }
            return model;
        }

        public ConfigurationProfileSetupDealersViewModel GetProfileSetupDealersViewModel(int? id = null)
        {
            var model = new ConfigurationProfileSetupDealersViewModel();
            if (id.HasValue)
            {
                var company = _companyRepository.GetCompanyAndSettings(id.Value);
                return Mapper.Map(company, model);
            }
            return model;
        }

        public ConfigurationProfileSetupDistributorViewModel GetProfileSetupDefaultDistributorViewModel(int corpId)
        {
            var model = new ConfigurationProfileSetupDistributorViewModel();
            var corporation = _companyRepository.GetCompanyAndSettings(corpId);
            return Mapper.MapDefaultValues(corporation, model);
        }

        public ConfigurationProfileSetupDealersViewModel GetProfileSetupDefaultDealerViewModel(int distributorId)
        {
            var model = new ConfigurationProfileSetupDealersViewModel();
            var distributor = _companyRepository.GetCompanyAndSettings(distributorId);
            return Mapper.MapDefaultValues(distributor, model);
        }


        public ServiceResponse SaveApplicationSettings(ConfigurationCorpApplicationSettingsViewModel model)
        {
            var settings = Mapper.Map(model);
            var company = _companyRepository.GetCompanyAndSettings(model.Id);
            _companyRepository.Update(company, settings);

            var response = _companyRepository.CommitChanges();
            return response;
        }

        public ServiceResponse SaveApplicationSettings(ConfigurationApplicationSettingViewModel model)
        {
            var response = Validator.Validate(model);
            if (!response.IsSuccess)
            {
                return response;
            }
            var settings = Mapper.Map(model);
            var company = _companyRepository.GetCompanyAndSettings(model.Id);
            _companyRepository.Update(company, settings);

            response = _companyRepository.CommitChanges();
            return response;
        }

        public ServiceResponse SaveDocumentsSetup(ConfigurationDocumentSetupViewModel model)
        {
            var response = Validator.Validate(model);
            if (!response.IsSuccess)
            {
                return response;
            }

            var settings = Mapper.Map(model);
            var company = _companyRepository.GetCompanyAndSettings(model.Id);
            _companyRepository.Update(company, settings);

            response = _companyRepository.CommitChanges();
            return response;
        }

        public ServiceResponse SaveMenuSetup(ConfigurationMenuSetupViewModel model)
        {
            var response = Validator.Validate(model);
            if (!response.IsSuccess)
            {
                return response;
            }

            var settings = Mapper.Map(model);
            var company = _companyRepository.GetCompanyAndSettings(model.Id);
            _companyRepository.Update(company, settings);

            response = _companyRepository.CommitChanges();
            return response;
        }

        public ServiceResponse SaveProfileSetup(ConfigurationProfileSetupViewModel model, CompanyTypes companyType)
        {
            var name = model.GetType().Name;
            var validationResponse = Validator.Validate(model, companyType);
            if (!validationResponse.IsSuccess)
            {
                return validationResponse;
            }

            Company company;
            var settings = Mapper.Map(model);
            if (model.Id.HasValue)
            {
                company = Mapper.Map(model, _companyRepository.GetCompanyAndSettings(model.Id.Value));
                _companyRepository.Update(company, settings);
            }
            else
            {
                company = Mapper.Map(model, null);
                company.CompanyTypeId = (int)companyType;

                // TODO: this is temp, waiting for answer what to do about the timezone
                company.TimeZoneId = 1;
                _companyRepository.Insert(company, settings);
            }

            var response = _companyRepository.CommitChanges();
            if (response.IsSuccess)
            {
                response.SavedItemId = company.Id;
                SaveCompanyProfileImage(model.LogoFileName, company.Id);
            }

            return response;
        }

        public ServiceResponse SaveProfileSetup(ConfigurationProfileSetupDealersViewModel model, CompanyTypes companyType)
        {
            var validationResponse = Validator.Validate(model, companyType);
            if (!validationResponse.IsSuccess)
            {
                return validationResponse;
            }

            Company company;
            var settings = Mapper.Map(model);
            if (model.Id.HasValue)
            {
                company = Mapper.Map(model, _companyRepository.GetCompanyAndSettings(model.Id.Value));
                _companyRepository.Update(company, settings);
            }
            else
            {
                company = Mapper.Map(model, null);
                company.CompanyTypeId = (int)companyType;

                // TODO: this is temp, waiting for answer what to do about the timezone
                company.TimeZoneId = 1;
                _companyRepository.Insert(company, settings);
            }

            var response = _companyRepository.CommitChanges();
            if (response.IsSuccess)
            {
                response.SavedItemId = company.Id;
                SaveCompanyProfileImage(model.LogoFileName, company.Id);
            }

            return response;
        }

        public ServiceResponse SaveProfileSetup(ConfigurationProfileSetupDistributorViewModel model, CompanyTypes companyType)
        {
            var name = model.GetType().Name;
            var validationResponse = Validator.Validate(model, companyType);
            if (!validationResponse.IsSuccess)
            {
                return validationResponse;
            }

            Company company;
            var settings = Mapper.Map(model);
            if (model.Id.HasValue)
            {
                company = Mapper.Map(model, _companyRepository.GetCompanyAndSettings(model.Id.Value));
                _companyRepository.Update(company, settings);
            }
            else
            {
                company = Mapper.Map(model, null);
                company.CompanyTypeId = (int)companyType;

                // TODO: this is temp, waiting for answer what to do about the timezone
                company.TimeZoneId = 1;
                _companyRepository.Insert(company, settings);
            }

            var response = _companyRepository.CommitChanges();
            if (response.IsSuccess)
            {
                response.SavedItemId = company.Id;
                SaveCompanyProfileImage(model.LogoFileName, company.Id);
            }

            return response;
        }

        private static void SaveCompanyProfileImage(string logoFileName, int companyId)
        {
            if (string.IsNullOrEmpty(logoFileName))
            {
                return;
            }
            var appContext = IocManager.Resolve<IAppContext>();
            var tempFilePath = $@"{appContext.TempFolderName}\{logoFileName}";
            var userContentFilePath = $@"{appContext.CompanyContentFolder}\{companyId}\{logoFileName}";

            var fileManager = IocManager.Resolve<IFileManager>();
            fileManager.Move(tempFilePath, userContentFilePath);
        }
    }
}
