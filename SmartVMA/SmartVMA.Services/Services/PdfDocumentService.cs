﻿using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Services.Mappers;
using System.Collections.Generic;
using SmartVMA.Infrastructure.Mailer;
using System.Net.Mail;
using System;
using System.Configuration;
using System.Net;
using System.Drawing;
using System.IO;
using SmartVMA.Infrastructure.FileManager;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class PdfDocumentService : IPdfDocumentService
    {
        private readonly IPdfDocumentRepository _repository;
        private readonly IInspectionRepository _inspectionRepository;
        public PdfDocumentService(IPdfDocumentRepository repository, IInspectionRepository inspectionRepository)
        {
            _repository = repository;
            _inspectionRepository = inspectionRepository;
        }

        public PdfDocumentViewModel GePdfDocumentModel(long appointmentPresentationId, int documentType)
        {
            var companySettings = IocManager.Resolve<ICompanyService>();
            var languageId = companySettings.GetSettingAsInteger(CompanySettingsNames.Language);            

            var pdfDocument = _repository.GetPdfDocument(appointmentPresentationId, documentType, languageId);

            if (pdfDocument != null)
            {
                var model = Mapper.Map(pdfDocument);
                return model;
            }
            else
            {
                return null;
            }
        }

        public void AddPdfDocument(PdfDocumentViewModel model)
        {
            _repository.Insert(Mapper.Map(model, null));
            _repository.CommitChanges();
        }

        public PdfDocumentIndexViewModel GetDocuments(string VIN, string InvoiceNumber, DateTime? startDate, DateTime? endDate)
        {
            var documentsEntity = _repository.GetDocuments(VIN, InvoiceNumber, startDate, endDate);
            var documents = new List<PdfDocumentViewModel>();

            foreach (var document in documentsEntity)
            {
                documents.Add(Mapper.Map(document));
            }

            var model = new PdfDocumentIndexViewModel();
            model.Items = documents;
            return model;
        }

        public void AddPdfDocumentToPrintQueue(int dealerCustomerId, int? userId, string fileName, long appointmentPresentationID, string prnJobTypeCode)
        {
            _repository.AddPdfDocumentToPrintQueue(dealerCustomerId, userId, fileName, appointmentPresentationID, prnJobTypeCode);

            // To ping the remote printer server and start the remote printing
            pingRemotePrintServer(dealerCustomerId);
        }

        public PdfHeaderViewModel GetPdfHeaderViewModel(int dealerId, long appointmentPresentationId, int? mileage = null)
        {
            return _repository.GetPdfHeaderViewModel(dealerId, appointmentPresentationId, mileage);
        }

        public PdfHeaderCustomerInfoViewModel GetPdfHeaderCustomerInfoViewModel(int dealerId, long appointmentPresentationId)
        {
            return _repository.GetPdfHeaderCustomerInfoViewModel(dealerId, appointmentPresentationId);
        }

        public PdfHeaderCustomerInfoViewModel GetPdfHeaderCustomerInfoViewModelFromVin(string vin)
        {
            return _repository.GetPdfHeaderCustomerInfoViewModelFromVin(vin);
        }

        public PartsCopyPdfViewModel GetPartsCopyPdfViewModel(long appointmentPresentationId)
        {
            var appointmentPresentationRepository = IocManager.Resolve<IAppointmentPresentationRepository>();
            var appointment = appointmentPresentationRepository.GetAppointmentWithServicesAndPartsById(appointmentPresentationId);

            var model = Mapper.Map(appointment, new PartsCopyPdfViewModel());

            return model;
        }

        public TechCopyPdfViewModel GetTechCopyPdfViewModel(long appointmentPresentationId)
        {
            return _repository.GetTechCopyPdfViewModel(appointmentPresentationId);
        }

        public CustomerCopyPdfViewModel GetCustomerCopyPdfViewModel(long appointmentPresentationId)
        {
            var appointmentRepository = IocManager.Resolve<IAppointmentPresentationRepository>();
            var appointment = appointmentRepository.GetAppointmentPresentationWithServicesById(appointmentPresentationId);

            var model = Mapper.Map(appointment, new CustomerCopyPdfViewModel());
            return model;
        }

        public WalkaroundInspectionPdfViewModel GetWalkaroundInspectionPdfViewModel(long? inspectionId, long? appointmentPresentationId)
        {
            WalkaroundInspectionPdfViewModel model = new WalkaroundInspectionPdfViewModel();
            model.WalkaroundInspection = _inspectionRepository.GetInspectionViewModel(inspectionId, appointmentPresentationId);

            if (model.WalkaroundInspection.VehiclePhotos.Count > 0)
            {
                var fileManager = IocManager.Resolve<IFileManager>();
                for (int i = 0; i < model.WalkaroundInspection.VehiclePhotos.Count; i++)
                {
                    var photoStream = fileManager.Read(model.WalkaroundInspection.VehiclePhotos[i].ContentPath);

                    Image img = Image.FromStream(photoStream);

                    var fileExtension = Path.GetExtension(model.WalkaroundInspection.VehiclePhotos[i].FileName);
                    model.WalkaroundInspection.VehiclePhotos[i].FileContentType = $"image/{fileExtension.TrimStart('.')}";

                    model.WalkaroundInspection.VehiclePhotos[i].IsPortrait = img.Height / (double)img.Width > 1.366;

                    photoStream.Position = 0;
                    model.WalkaroundInspection.VehiclePhotos[i].FileByteArray = fileManager.ReadStreamToByte(photoStream);
                }
            }

            return model;
        }

        public WalkaroundInspectionPdfViewModel GetWalkaroundInspectionPdfViewModel(long? inspectionId, long? appointmentPresentationId, bool hideVehiclePhotos)
        {
            if(hideVehiclePhotos)
            {
                WalkaroundInspectionPdfViewModel model = new WalkaroundInspectionPdfViewModel();
                model.WalkaroundInspection = _inspectionRepository.GetInspectionViewModel(inspectionId, appointmentPresentationId, hideVehiclePhotos);

                return model;
            }
            else
            {
                return GetWalkaroundInspectionPdfViewModel(inspectionId, appointmentPresentationId);
            }
            
        }

        public PdfFooterViewModel GetPdfFooterViewModel(long? appointmentPresentationId, DateTime? printedDate)
        {
            return _repository.GetPdfFooterViewModel(appointmentPresentationId, printedDate);
        }

        public PdfHeaderAdvisorViewModel GetPdfHeaderAdvisorViewModel(int userId)
        {
            return _repository.GetPdfHeaderAdvisorViewModel(userId);
        }

        public PdfHeaderDealerViewModel GetPdfHeaderDealerViewModel()
        {
            return _repository.GetPdfHeaderDealerViewModel();
        }        

        public bool SendPdfAsEmail(string replyto, string messageTo, string messageSubject, string messageBody, List<Attachment> attachments)
        {
            var mailer = IocManager.Resolve<IMailManager>();            
            return mailer.Send(replyto, messageTo, messageSubject, messageBody, attachments);
        }

        private void pingRemotePrintServer(int dealerId)
        {
            try
            {
                var _repository = IocManager.Resolve<ICompanyRepository>();
                var dealer = _repository.FirstOrDefault(x => x.Id == dealerId);
                string remotePrintServer = ConfigurationManager.AppSettings["RemotePrintServer"].ToString();
                string remotePrintServerPort = ConfigurationManager.AppSettings["RemotePrintServerPort"].ToString();
                string url = string.Format(ConfigurationManager.AppSettings["RemotePrintUrl"].ToString(), remotePrintServer, remotePrintServerPort, dealer.AccountId.ToLower());

                using (WebClient client = new WebClient())
                {
                    client.Headers["User-Agent"] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36";

                    // just to ping the remote print server, no need to store the web request result
                    byte[] result = client.DownloadData(url);
                }
            }
            catch (System.Exception e)
            {
                throw e;
            }
        }
    }
}
