﻿using System.Collections.Generic;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Core.Repositories;
using System.Linq;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class DMSTypesService : IDMSTypesService
    {
        private readonly IDMSTypesRepository _dmsTypesRepository;

        public DMSTypesService(IDMSTypesRepository dmsTypesRepository)
        {
            _dmsTypesRepository = dmsTypesRepository;
        }

        public IEnumerable<DMSTypesViewModel> GetDmsTypes()
        {
            return _dmsTypesRepository.Find().Select(x => new DMSTypesViewModel { Id = x.Id, Name = x.DmsCode }).OrderBy(x => x.SortOrder);
        }
    }
}
