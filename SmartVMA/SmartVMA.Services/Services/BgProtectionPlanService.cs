﻿using System.Collections.Generic;
using System.Linq;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class BgProtectionPlanService : IBgProtectionPlanService
    {
        private readonly IBgProtectionPlanRepository _repository;

        public BgProtectionPlanService(IBgProtectionPlanRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<BgProtectionPlanViewModel> GetPlans(List<int> productIds)
        {
            return _repository.GetByProducts(productIds)
                .Select(x => new BgProtectionPlanViewModel() { Id = x.Id, Description = x.ProtectionPlanName });
        }

        public IEnumerable<BgProtectionPlanViewModel> GetPlans()
        {
            return _repository.Find()
                .Select(x => new BgProtectionPlanViewModel() { Id = x.Id, Description = x.ProtectionPlanName });
        }
    }
}
