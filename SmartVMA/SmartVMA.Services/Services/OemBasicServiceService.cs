﻿using System;
using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Services.Mappers;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using SmartVMA.Core;
using SmartVMA.Core.Entities;
using SmartVMA.Infrastructure;
using SmartVMA.Infrastructure.SerializationManager;
using SmartVMA.Resources;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class OemBasicServiceService : IOemBasicServiceService
    {
        private readonly IOemBasicServiceRepository _repository;
        private readonly ICompanyService _companyService;

        public OemBasicServiceService(IOemBasicServiceRepository repository, ICompanyService companyService)
        {
            _repository = repository;
            _companyService = companyService;
        }

        public OemBasicServiceIndexViewModel GetIndexViewModel()
        {
            var oemBasicServices = new List<OemBasicServiceViewModel>();
            oemBasicServices.AddRange(_repository.GetAllOemBasicServicesWithException().Select(x => Mapper.Map(x))); // get the entity from Repository, then Map it to View Model

            var model = new OemBasicServiceIndexViewModel();
            model.Items = oemBasicServices;
            return model;
        }
        public OemBasicServiceViewModel GetViewModel(int? id = null)
        {
            var model = new OemBasicServiceViewModel();
            if (!id.HasValue)
            {
                return model;
            }

            var role = _repository.GetOemBasicServiceById(id.Value);
            model = Mapper.Map(role, model);
            return model;
        }
        public ServiceResponse SaveFromDataMaintenance(OemBasicServiceViewModel model)
        {
            if (model.Id.HasValue)
            {
                var entity = Mapper.Map(model, _repository.FirstOrDefault(x => x.Id == model.Id.Value), true);
                entity.OemServiceExceptionId = null;
                entity.NoOverwrite = true;
                _repository.Update(entity);
            }
            else
            {
                //TODO: check if there's a need of this in DataMaintenance, because for now there we only edit services with parameter "IsIncomplete = true"
                var role = Mapper.Map(model);
                _repository.Insert(role);
            }

            var response = _repository.CommitChanges();
            return response;
        }
        public ServiceResponse Delete(int id)
        {
            var article = _repository.FirstOrDefault(x => x.Id == id);
            _repository.Delete(article);
            var response = _repository.CommitChanges();
            return response;
        }

        public ServiceResponse DeleteBulk(string ids)
        {
            return _repository.DeleteBulk(ids);
        }

        public OemBasicServiceIndexViewModel GetPagedItems(PagingRequest request)
        {
            var entities = _repository.GetPagedResults(request);
            var items = new List<OemBasicServiceViewModel>(entities.Select(x => Mapper.Map(x)));
            var model = new OemBasicServiceIndexViewModel();
            model.Items = items;
            var total = _repository.GetPagedResultsTotal(request);
            model.ItemsCount = total;

            return model;
        }

        public OemServiceMaintenanceInitViewModel GetAmamServicesViewModel()
        {
            var carService = IocManager.Resolve<ICarService>();
            var model = new OemServiceMaintenanceInitViewModel
            {
                VehicleFilter = carService.GetVehicleServiceFilter(),
                Data = new OemServiceMaintenanceViewModel { Parts = new List<PartItemViewModel> { new PartItemViewModel() } }
            };
            return model;
        }

        public OemServiceMaintenanceIndexViewModel GetAmamServicesIndexViewModel(OemServiceMaintenancePagingViewModel request)
        {
            int itemsCount;
            string filteredServiceIds;
            var services = _repository.GetPagedResults(request, out itemsCount, out filteredServiceIds).Select(x => Mapper.Map(x, new OemServiceMaintenanceViewModel())).ToArray();

            //itemsCount = _repository.GetPagedResultsTotal();
            var model = new OemServiceMaintenanceIndexViewModel
            {
                Draw = request.Draw + 1,
                Items = services,
                FilteredServiceIds = filteredServiceIds,
                ItemsCount = itemsCount,
                RecordsFilteredCount = itemsCount
            };

            return model;
        }

        public ServiceResponse Save(IEnumerable<OemServiceMaintenanceViewModel> items, OemServiceMaintenancePagingViewModel filter)
        {
            var itemsAsArray = items.ToArray();
            var response = new ServiceResponse();
            var serializationManager = IocManager.Resolve<ISerializationManager>(RegisteredTypes.XmlSerializationManager);
            var counter = 0;
            foreach (var service in itemsAsArray)
            {
                service.LaborTime = service.LaborTime ?? 0;
                var serviceAsXml = serializationManager.Serialize(service);
                var saveResponse = _repository.Save(
                    serviceAsXml,
                    filter.Years,
                    filter.Makes,
                    filter.Models,
                    filter.EngineTypes,
                    filter.TransmissionTypes,
                    filter.DriveLines,
                    counter++);

                if (!saveResponse.IsSuccess)
                {
                    foreach (var error in saveResponse.Errors)
                    {
                        response.Errors.Add(error.Key,error.Value);
                    }
                }
            }

            response.IsSuccess = !response.Errors.Any();
            return response;
        }

        public IEnumerable<string> GetOpActions()
        {
            return _repository.GetDistinctOpActions();
        }

        public ServiceResponse Validate(IEnumerable<OemServiceMaintenanceViewModel> items)
        {
            var response = new ServiceResponse();
            var counter = 0;
            foreach (var item in items)
            {
                var partsCounter = 0;
                var isRequired = false;
                foreach (var part in item.Parts)
                {
                    if (part.Id.HasValue || part.IsFluid)
                    {
                        isRequired = true;
                    }

                    if (!part.Quantity.HasValue && (isRequired || !string.IsNullOrEmpty(part.PartNumber) || part.Price.HasValue))
                    {
                        response.Errors.Add($"items[{counter}].parts[{partsCounter}].quantity", Messages.CustomServices_QuantityRequired);
                    }

                    if (!part.Price.HasValue && (isRequired || !string.IsNullOrEmpty(part.PartNumber) || part.Quantity.HasValue))
                    {
                        response.Errors.Add($"items[{counter}].parts[{partsCounter}].price", Messages.CustomServices_UnitPriceRequired);
                    }

                    partsCounter += 1;
                }

                counter += 1;
            }

            response.IsSuccess = !response.Errors.Any();
            return response;
        }
    }
}
