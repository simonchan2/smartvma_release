﻿using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Services.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SmartVMA.Core.InputModels.ViewModels.Dealer.MenuPresentations;
using SmartVMA.Resources;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class CarService : ICarService
    {
        private readonly ICarRepository _repository;
        private readonly ICompanyService _companyService;
        private readonly IAppContext _appContext;

        public CarService(ICarRepository repository, IAppContext appContext, ICompanyService companyService)
        {
            _repository = repository;
            _appContext = appContext;
            _companyService = companyService;
        }

        //get all distinct years from Car table
        public List<VehicleYearInfo> GetAllVehicleYears()
        {
            return _repository.GetAllVehicleYears();
        }

        //get all vehicle makes
        public List<VehicleMakeInfo> GetAllVehicleMakes()
        {
            return _repository.GetAllVehicleMakes();
        }

        //get all vehicle makes filtered by year
        public List<VehicleMakeInfo> GetFilteredVehicleMakesByYear(string year)
        {
            return _repository.GetFilteredVehicleMakes(year);
        }

        //get all vehicle models
        public List<VehicleModelInfo> GetAllVehicleModels()
        {
            return _repository.GetAllVehicleModels();
        }

        //get all vehicle models filtered by year and make
        public List<VehicleModelInfo> GetFilteredVehicleModels(string years, string makes)
        {
            return _repository.GetFilteredVehicleModels(years, makes);
        }

        //get all vehicle engines
        public List<VehicleEngineInfo> GetAllVehicleEngines()
        {
            return _repository.GetAllVehicleEngines();
        }

        //get all vehicle engines filtered by year, make and model
        public List<VehicleEngineInfo> GetFilteredVehicleEngines(string years, string makes, string models)
        {
            return _repository.GetFilteredVehicleEngines(years, makes, models);
        }

        //get all distinct vehicle transmissions
        public List<VehicleTransmissionInfo> GetAllVehicleTransmissions(string source)
        {
            return _repository.GetAllVehicleTransmissions(source);
        }

        //get vehicle transmissions filtered by year, make, model and engine
        public List<VehicleTransmissionInfo> GetFilteredVehicleTransmissions(string years, string makes, string models, string engines, string source)
        {
            return _repository.GetFilteredVehicleTransmissions(years, makes, models, engines, source);
        }

        //get all distinct vehicle drive lines
        public List<VehicleDriveLineInfo> GetAllVehicleDriveLines(string source)
        {
            return _repository.GetAllVehicleDriveLines(source);
        }

        //get vehicle drive lines filtered by year, make, model, engine and transmission
        public List<VehicleDriveLineInfo> GetFilteredVehicleDriveLines(string years, string makes, string models, string engines, string transmissions, string source)
        {
            return _repository.GetFilteredVehicleDriveLines(years, makes, models, engines, transmissions, source);
        }

        public int? GetCarId(int year, int make, int model, int engine, int transmission, int driveLine)
        {
            return _repository.GetCarId(year, make, model, engine, transmission, driveLine);
        }

        public VehicleFilterViewModel GetVehicleServiceFilter()
        {
            var model = new VehicleFilterViewModel
            {
                Years = new List<int>(),
                Makes = new List<int>(),
                Models = new List<int>(),
                EngineTypes = new List<int>(),
                TransmissionTypes = new List<int>(),
                DriveLines = new List<int>()
            };
            return model;
        }

        public IEnumerable<int> GetAllDistinctMileages(int measurementUnitId)
        {
            return _repository.GetAllDistinctMileages(measurementUnitId);
        }

        public IEnumerable<int> GetAllDistinctMileages(int carId, int measurmentUnitId)
        {
            return _repository.GetAllDistinctMileages(carId, measurmentUnitId);
        }

        public OdometerResponseModel FindMenuInterval(OdoMeterViewModel model)
        {
            OdometerResponseModel response = FindMenuInterval(model.CarId, (int)model.Mileage);
            response.Model.EnteredMileage = model.EnteredMileage;
            return response;
        }

        public OdometerResponseModel FindMenuInterval(int carId, int mileage)
        {
            var measurementUnitId = _companyService.GetSettingAsInteger(CompanySettingsNames.MeasurementUnit);
            var allResult = _repository.GetAllDistinctMileages(carId, measurementUnitId);
            var result = allResult.Count() > 0 ?
                allResult.Aggregate((x, y) => Math.Abs(x - mileage) < Math.Abs(y - mileage) ? x : y) : 0;


            var response = new OdometerResponseModel()
            {
                IsSuccess = true,
                Model = new OdoMeterViewModel { Mileage = result, CarId = carId }
            };
            return response;
        }

        public IEnumerable<int> GetAllMileages(int carId, int mileage)
        {
            var measurementUnitId = _companyService.GetSettingAsInteger(CompanySettingsNames.MeasurementUnit);
            return _repository.GetAllDistinctMileages(carId, measurementUnitId);
        }

        private IEnumerable<SelectListItem> SetUpFilter(IEnumerable<int> filterList, VehicleFilterItemsViewModel[] items, VehicleFilterTypes listType, bool preserveNA = false)
        {
            string NAText = preserveNA ? Labels.NotApplicable : Labels.All;
            var groups = items.Where(x => x.EntityType == (int)listType)
                    .Select(x => x.Text == Labels.NotApplicable || string.IsNullOrEmpty(x.Text) ? NAText.Substring(0, 1) : x.Text[0].ToString())
                    .Distinct()
                    .Select(x => new SelectListGroup { Name = x });
            if (items.Count(z => z.EntityType == (int)listType) == 1)
            {
                filterList = items.Where(z => z.EntityType == (int)listType).Select(c => c.Id.Value);
            }
            IEnumerable<SelectListItem> list = items.Where(x => x.EntityType == (int)listType)
                    .Select(x => new SelectListItem
                    {
                        Text = x.Text == Labels.NotApplicable || string.IsNullOrEmpty(x.Text) ? NAText : x.Text,
                        Value = x.Id.HasValue ? x.Id.ToString() : x.Text,
                        Selected = filterList != null && filterList.Any(y => y == x.Id),
                        Group = groups.FirstOrDefault(group => group.Name == (x.Text == Labels.NotApplicable || string.IsNullOrEmpty(x.Text) ? NAText : x.Text)[0].ToString())
                    }).OrderBy(c => c.Group.Name).ThenBy(c => c.Text).ToList();

            return list;
        }
        public VehicleFilterResponseViewModel GetVehicleFilter(VehicleFilterViewModel filter)
        {
            var items = _repository.GetVehicleFilter(filter);
            var response = new VehicleFilterResponseViewModel();
            if (items.Any(c => c.EntityType == (int)VehicleFilterTypes.SecondarySearch))
            {
                response.IsFromSecondarySearch = true;
            }
            response.Years = SetUpFilter(filter.Years, items, VehicleFilterTypes.Year, filter.PreserveNA);
            if (response.Years.Count() == 0 && string.IsNullOrEmpty(filter.VIN))
            {
                response.Years = null;
            }
            response.Makes = SetUpFilter(filter.Makes, items, VehicleFilterTypes.Make, filter.PreserveNA);
            response.Models = SetUpFilter(filter.Models, items, VehicleFilterTypes.Model, filter.PreserveNA);
            response.EngineTypes = SetUpFilter(filter.EngineTypes, items, VehicleFilterTypes.Engine, filter.PreserveNA);
            response.TransmissionTypes = SetUpFilter(filter.TransmissionTypes, items, VehicleFilterTypes.Transmission, filter.PreserveNA);
            response.DriveLines = SetUpFilter(filter.DriveLines, items, VehicleFilterTypes.Driveline, filter.PreserveNA);

            return response;
        }

        public bool VinExists(string vin)
        {
            return _repository.VinExists(vin);
        }
    }
}