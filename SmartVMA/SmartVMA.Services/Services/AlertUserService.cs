﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Services.Mappers;
using System.Collections.Generic;
using System.Linq;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class AlertUserService : IAlertUserService
    {
        private readonly IAlertUserRepository _repository;

        public AlertUserService(IAlertUserRepository repository)
        {
            _repository = repository;
        }

        public ServiceResponse Save(AlertUserViewModel model) 
        {
            var relationship = _repository.GetByUserIdAndAlertId(model.UserId, model.AlertId);
            if (relationship != null) // that relationship exis, just updat with new status
            {
                var entity = Mapper.Map(model, relationship);
                _repository.Update(entity);
            }            
            else // insert a new row in AlertsUsers table for specific Alert.Id and CurrentUserId with selected StatusId
            {
                var entity = Mapper.Map(model);
                _repository.Insert(entity);
            }

            var response = _repository.CommitChanges(); 
            return response;
        }
    }
}

