﻿using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using System.Collections.Generic;
using SmartVMA.Services.Mappers;
using System;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class AppointmentDetailsService : IAppointmentDetailsService
    {
        private readonly IAppointmentDetailsRepository _repository;
        public AppointmentDetailsService(IAppointmentDetailsRepository repository)
        {
            _repository = repository;
        }

        public List<AppointmentDetailsViewModel> GetAppointmentDetailsList(int appointmentID)
        {
            List<AppointmentDetailsViewModel> appointmentDetails = new List<AppointmentDetailsViewModel>();
            foreach (var item in _repository.GetAppointmentDetailsList(appointmentID))
            {
                appointmentDetails.Add(Mapper.Map(item, new AppointmentDetailsViewModel()));
            }

           return appointmentDetails;

        }
    }
}
