﻿using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Services.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class VinMasterService : IVinMasterService
    {
        private readonly IVinMasterRepository _repository;
        private readonly IAppContext _appContext;
        public VinMasterService(IVinMasterRepository repository, IAppContext appContext)
        {
            _repository = repository;
            _appContext = appContext;
        }

        //get all distinct vehicle steerings
        public List<string> GetAllVehicleSteerings()
        {
            return _repository.GetAllVehicleSteerings().ToList();
        }
    }
}
