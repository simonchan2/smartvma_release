﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Resources;
using SmartVMA.Services.Mappers;
using System.Collections.Generic;
using System.Linq;

namespace SmartVMA.Services
{
    [IocBindable]
    internal class ILifeService : IILifeService
    {
        private readonly IILifeRepository _repository;
        private readonly IILifeHistoryRepository _repositoryHistory;
        protected readonly IAppContext _appContext;
        protected readonly IDealerVehicleRepository _dealerVehicleRepository;
        protected readonly IDealerCustomerRepository _dealerCustomerRepository;

        public ILifeService(IILifeRepository repository, IILifeHistoryRepository repositoryHistory, IAppContext appContext, IDealerVehicleRepository dealerVehicleRepository, IDealerCustomerRepository dealerCustomerRepository)
        {
            _repository = repository;
            _repositoryHistory = repositoryHistory;
            _appContext = appContext;
            _dealerVehicleRepository = dealerVehicleRepository;
            _dealerCustomerRepository = dealerCustomerRepository;
        }

        public ILifeViewModel GetILifeByVin(string vin)
        {
            var model = new ILifeViewModel();
            ILife entity = _repository.FirstOrDefault(x => x.VIN == vin);
            if (entity != null)
            {
                var dealerCustomerId = _dealerVehicleRepository.GetDealerCustomerIdByVIN(entity.VIN);
                var dealerCustomer = dealerCustomerId != null ? _dealerCustomerRepository.FirstOrDefault(x => x.Id == dealerCustomerId) : null;
                List<ILife_History> historyList = _repositoryHistory.GetAllHistoryForItem(entity.Contract_Number, vin);
                model = Mapper.Map(entity, historyList, dealerCustomer);
            }
            //get color and message
            GetILifeButtonColour(vin, model, entity);

            return model;
        }

        private void GetILifeButtonColour(string vin, ILifeViewModel model = null, ILife entity = null)
        {
            if (model == null)
            {
                model = new ILifeViewModel();
            }
            if (entity == null && vin != null)
            {
                entity = _repository.FirstOrDefault(x => x.VIN == vin);
            }

            model.ColorClass = Labels.ILifeGreyColour;
            if (entity != null)
            {
                int? dealerCustomerId = _dealerVehicleRepository.GetDealerCustomerIdByVIN(entity.VIN);
                var dealerCustomer = dealerCustomerId != null && dealerCustomerId != 0 ? _dealerCustomerRepository.FirstOrDefault(x => x.Id == dealerCustomerId) : null;
                if (entity.Active == true)
                {
                    if (dealerCustomer != null && (entity.First_Name ?? string.Empty).ToLower() == (dealerCustomer.FirstName ?? string.Empty).ToLower() && (entity.Last_Name ?? string.Empty).ToLower() == (dealerCustomer.LastName ?? string.Empty).ToLower() && (entity.EligibilityMessage ?? string.Empty).ToLower() == Messages.ILifeEligibilityMessage.ToLower())
                    {
                        model.ColorClass = Labels.ILifeGreenColour;
                    }
                    else if (dealerCustomer != null && ((entity.First_Name ?? string.Empty).ToLower() != (dealerCustomer.FirstName ?? string.Empty).ToLower() || (entity.Last_Name ?? string.Empty).ToLower() != (dealerCustomer.LastName ?? string.Empty).ToLower()))
                    {
                        model.ColorClass = Labels.ILifeYellowColour;
                        model.Message = Messages.PleaseConfirmNameOnContract;
                    }
                    else if ((entity.EligibilityMessage ?? string.Empty).ToLower() != Messages.ILifeEligibilityMessage.ToLower())
                    {
                        model.ColorClass = Labels.ILifeYellowColour;
                        model.Message = Messages.PleaseConfirmMileageConditions;
                    }
                }
                else
                {
                    model.ColorClass = Labels.ILifeRedColour;
                    model.Message = Messages.CustomerIsIneligible;
                }
            }
        }

        public ILifeViewModel ButtonInfo(string vin)
        {
            var model = new ILifeViewModel();
            var companyService = IocManager.Resolve<ICompanyService>();

            bool iLifeVisibility = companyService.GetSettingAsBool(Core.Enums.CompanySettingsNames.IsPlanActive);
            string iLifeText = companyService.GetSettingAsString(Core.Enums.CompanySettingsNames.PlanName);

            model.IsVisible = iLifeVisibility;
            model.ButtonName = iLifeText;
            GetILifeButtonColour(vin, model);

            return model;
        }

    }
}

