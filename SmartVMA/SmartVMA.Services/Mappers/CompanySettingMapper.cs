﻿using SmartVMA.Core;
using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Resources;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static IEnumerable<CompanySetting> Map(ConfigurationProfileSetupViewModel src)
        {
            var settings = new List<CompanySetting>();

            settings.Add(new CompanySetting { Name = CompanySettingsNames.Language.ToString(), Value = src.Language.ToString() });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.DateFormat.ToString(), Value = src.DateFormat.ToString() });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.MeasurementUnit.ToString(), Value = src.MeasurementUnitId.ToString() });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.Web.ToString(), Value = src.Web });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.CompanyLogo.ToString(), Value = src.LogoFileName });
            //settings.Add(new CompanySetting { Name = CompanySettingsNames.AccountNumber.ToString(), Value = src.AccountNumber });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.PhoneNumber.ToString(), Value = src.PhoneNumber });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.PhoneNumber2.ToString(), Value = src.PhoneNumber2 });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.FaxNumber.ToString(), Value = src.FaxNumber });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.Email.ToString(), Value = src.Email });

            return settings;
        }

        public static IEnumerable<CompanySetting> Map(ConfigurationProfileSetupDealersViewModel src)
        {
            var settings = new List<CompanySetting>();

            settings.Add(new CompanySetting { Name = CompanySettingsNames.Language.ToString(), Value = src.Language.ToString() });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.DateFormat.ToString(), Value = src.DateFormat.ToString() });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.MeasurementUnit.ToString(), Value = src.MeasurementUnitId.ToString() });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.Web.ToString(), Value = src.Web });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.CompanyLogo.ToString(), Value = src.LogoFileName });
            //settings.Add(new CompanySetting { Name = CompanySettingsNames.AccountNumber.ToString(), Value = src.AccountNumber });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.PhoneNumber.ToString(), Value = src.PhoneNumber });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.PhoneNumber2.ToString(), Value = src.PhoneNumber2 });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.FaxNumber.ToString(), Value = src.FaxNumber });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.Email.ToString(), Value = src.Email });

            return settings;
        }

        public static IEnumerable<CompanySetting> Map(ConfigurationProfileSetupDistributorViewModel src)
        {
            var settings = new List<CompanySetting>();

            settings.Add(new CompanySetting { Name = CompanySettingsNames.Language.ToString(), Value = src.Language.ToString() });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.DateFormat.ToString(), Value = src.DateFormat.ToString() });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.MeasurementUnit.ToString(), Value = src.MeasurementUnitId.ToString() });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.Web.ToString(), Value = src.Web });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.CompanyLogo.ToString(), Value = src.LogoFileName });
            //settings.Add(new CompanySetting { Name = CompanySettingsNames.AccountNumber.ToString(), Value = src.AccountNumber });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.PhoneNumber.ToString(), Value = src.PhoneNumber });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.PhoneNumber2.ToString(), Value = src.PhoneNumber2 });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.FaxNumber.ToString(), Value = src.FaxNumber });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.Email.ToString(), Value = src.Email });

            return settings;
        }

        public static IEnumerable<CompanySetting> Map(ConfigurationMenuSetupViewModel src)
        {
            var settings = new List<CompanySetting>();

            settings.Add(new CompanySetting { Name = CompanySettingsNames.UseOEMMenus.ToString(), Value = src.UseOEMMenus.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.DefaultLaborRate.ToString(), Value = src.DefaultLaborRate.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.ConvertCurrency.ToString(), Value = src.ConvertCurrency.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.ExchangeRate.ToString(), Value = src.ExchangeRate.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.DisplayServicesMenuContent.ToString(), Value = src.DisplayServicesMenuContent.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.RollAdditionalServiceIntoMainMenu.ToString(), Value = src.RollAdditionalServiceIntoMainMenu.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.MenuLevels.ToString(), Value = src.MenuLevels.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.MenuLabelLevel1.ToString(), Value = src.MenuLabelLevel1, TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.MenuLabelLevel2.ToString(), Value = src.MenuLabelLevel2, TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.MenuLabelLevel3.ToString(), Value = src.MenuLabelLevel3, TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.MenuPrefixLevel1.ToString(), Value = src.MenuPrefixLevel1, TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.MenuPrefixLevel2.ToString(), Value = src.MenuPrefixLevel2, TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.MenuPrefixLevel3.ToString(), Value = src.MenuPrefixLevel3, TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.MenuSuffixLevel1.ToString(), Value = src.MenuSuffixLevel1, TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.MenuSuffixLevel2.ToString(), Value = src.MenuSuffixLevel2, TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.MenuSuffixLevel3.ToString(), Value = src.MenuSuffixLevel3, TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.LOFMenuLevels.ToString(), Value = src.LOFMenuLevels.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.LOFMenuLabelLevel1.ToString(), Value = src.LOFMenuLabelLevel1, TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.LOFMenuLabelLevel2.ToString(), Value = src.LOFMenuLabelLevel2, TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.LOFMenuLabelLevel3.ToString(), Value = src.LOFMenuLabelLevel3, TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.LOFMenuOpCodeLevel1.ToString(), Value = src.LOFMenuOpCodeLevel1, TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.LOFMenuOpCodeLevel2.ToString(), Value = src.LOFMenuOpCodeLevel2, TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.LOFMenuOpCodeLevel3.ToString(), Value = src.LOFMenuOpCodeLevel3, TenantId = src.Id });

            return settings;
        }

        public static IEnumerable<CompanySetting> Map(ConfigurationDocumentSetupViewModel src)
        {
            var settings = new List<CompanySetting>();

            settings.Add(new CompanySetting { Name = CompanySettingsNames.InvoiceNoteText.ToString(), Value = src.InvoiceNoteText.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.DisclaimerText.ToString(), Value = src.DisclaimerText.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.DisplayInvoiceNumber.ToString(), Value = src.DisplayInvoiceNumber.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.PrintAdvisorInformation.ToString(), Value = src.PrintAdvisorInformation.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.PrintShopCharges.ToString(), Value = src.PrintShopCharges.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.ShopChargeAmount.ToString(), Value = src.ShopChargeAmount.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.ShopChargesApplyToParts.ToString(), Value = src.ShopChargesApplyToParts.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.ShopChargesApplyToLabor.ToString(), Value = src.ShopChargesApplyToLabor.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.ShopChargeMinimum.ToString(), Value = src.ShopChargeMinimum.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.ShopChargeMaximum.ToString(), Value = src.ShopChargeMaximum.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.PrintTaxesOnDocuments.ToString(), Value = src.PrintTaxesOnDocuments.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.DisplayTexesInMenus.ToString(), Value = src.DisplayTaxesInMenus.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.TaxRateAmount.ToString(), Value = src.TaxRateAmount.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.TaxRateApplyToParts.ToString(), Value = src.TaxRateApplyToParts.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.TaxRateApplyToLabor.ToString(), Value = src.TaxRateApplyToLabor.ToString(), TenantId = src.Id });

            return settings;
        }

        public static IEnumerable<CompanySetting> Map(ConfigurationApplicationSettingViewModel src)
        {
            var settings = new List<CompanySetting>();

            settings.Add(new CompanySetting { Name = CompanySettingsNames.SessionExpiration.ToString(), Value = src.SessionExpiration.ToString(), TenantId = src.Id });
            if (src.DmsTypeId.HasValue)
            {
                if (src.DmsTypeId == -1)
                {
                    settings.Add(new CompanySetting { Name = CompanySettingsNames.DmsTypeId.ToString(), Value = null, TenantId = src.Id });
                }
                else
                {
                    settings.Add(new CompanySetting { Name = CompanySettingsNames.DmsTypeId.ToString(), Value = src.DmsTypeId.ToString(), TenantId = src.Id });
                }

            }
            settings.Add(new CompanySetting { Name = CompanySettingsNames.ParkedMenuRetentionDuration.ToString(), Value = src.ParkedMenuRetentionDuration.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.AdvisorsHaveDeleteParkedMenuPermission.ToString(), Value = src.AdvisorsHaveDeleteParkedMenuPermission.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.FlagMenuAsModifiedForAnyChange.ToString(), Value = src.FlagMenuAsModifiedForAnyChange.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.AverageMilesPerYear.ToString(), Value = src.AverageMilesPerYear.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.IsPlanActive.ToString(), Value = src.IsPlanActive.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.PlanProvider.ToString(), Value = src.PlanProvider, TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.PlanName.ToString(), Value = src.PlanName, TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.IsRemotePrint.ToString(), Value = src.IsRemotePrint.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.PrintServer.ToString(), Value = src.PrintServer, TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.PrintPort.ToString(), Value = src.PrintPort, TenantId = src.Id });

            settings.Add(new CompanySetting { Name = CompanySettingsNames.AppointmentsButtonOrder.ToString(), Value = src.AppointmentsButtonOrder.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.PlannerButtonOrder.ToString(), Value = src.PlannerButtonOrder.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.DocumentSearchButtonOrder.ToString(), Value = src.DocumentSearchButtonOrder.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.MpiButtonOrder.ToString(), Value = src.MpiButtonOrder.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.RoPrepButtonOrder.ToString(), Value = src.RoPrepButtonOrder.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.ReportingButtonOrder.ToString(), Value = src.ReportingButtonOrder.ToString(), TenantId = src.Id });

            return settings;
        }

        public static IEnumerable<CompanySetting> Map(ConfigurationCorpApplicationSettingsViewModel src)
        {
            var settings = new List<CompanySetting>();

            settings.Add(new CompanySetting { Name = CompanySettingsNames.ApplicationName.ToString(), Value = StaticSettings.DefaultApplicationName, TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.DataRefreshFrequencyInDays.ToString(), Value = src.DataRefreshFrequencyInDays.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.SessionExpiration.ToString(), Value = src.SessionExpiration.ToString(), TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.SupportLink.ToString(), Value = src.SupportLink, TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.SupportLinkDisplayText.ToString(), Value = src.SupportLinkDisplayText, TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.PrivacyPolicyLink.ToString(), Value = src.PrivacyPolicyLink, TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.PrivacyPolicyLinkDisplayText.ToString(), Value = src.PrivacyPolicyLinkDisplayText, TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.SmartVmaLink.ToString(), Value = src.SmartVmaLink, TenantId = src.Id });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.SmartVmaLinkDisplayText.ToString(), Value = src.SmartVmaLinkDisplayText, TenantId = src.Id });

            return settings;
        }

        public static ConfigurationProfileSetupViewModel Map(IEnumerable<CompanySetting> src, ConfigurationProfileSetupViewModel dest)
        {
            var settings = new List<CompanySetting>();

            var languageSetting = src.FirstOrDefault(x => x.Name == CompanySettingsNames.Language.ToString());
            if (languageSetting != null && !string.IsNullOrEmpty(languageSetting.Value))
            {
                dest.Language = int.Parse(languageSetting.Value);
            }

            var webSetting = src.FirstOrDefault(x => x.Name == CompanySettingsNames.Web.ToString());
            if (webSetting != null)
            {
                dest.Web = webSetting.Value;
            }

            var dateFormatSetting = src.FirstOrDefault(x => x.Name == CompanySettingsNames.DateFormat.ToString());
            if (!string.IsNullOrEmpty(dateFormatSetting?.Value))
            {
                dest.DateFormat = int.Parse(dateFormatSetting.Value);
            }

            var measurementUnitSetting = src.FirstOrDefault(x => x.Name == CompanySettingsNames.MeasurementUnit.ToString());
            if (dateFormatSetting != null && measurementUnitSetting != null && !string.IsNullOrEmpty(dateFormatSetting.Value))
            {
                dest.MeasurementUnitId = int.Parse(measurementUnitSetting.Value);
            }

            var companyLogoSetting = src.FirstOrDefault(x => x.Name == CompanySettingsNames.CompanyLogo.ToString());
            if (companyLogoSetting != null)
            {
                dest.LogoFileName = companyLogoSetting.Value;
                if (!string.IsNullOrEmpty(dest.LogoFileName))
                {
                    var appContext = IocManager.Resolve<IAppContext>();
                    dest.LogoDownloadUrl = appContext.MapUrl($@"{appContext.CompanyContentFolder}/{companyLogoSetting.TenantId}/{dest.LogoFileName}");
                }
            }

            //var accountNumberSetting = src.FirstOrDefault(x => x.Name == CompanySettingsNames.AccountNumber.ToString());
            //if (accountNumberSetting != null)
            //{
            //    dest.AccountNumber = accountNumberSetting.Value;
            //}

            var phoneNumberSetting = src.FirstOrDefault(x => x.Name == CompanySettingsNames.PhoneNumber.ToString());
            if (phoneNumberSetting != null)
            {
                dest.PhoneNumber = phoneNumberSetting.Value;
            }

            var phoneNumber2Setting = src.FirstOrDefault(x => x.Name == CompanySettingsNames.PhoneNumber2.ToString());
            if (phoneNumber2Setting != null)
            {
                dest.PhoneNumber2 = phoneNumber2Setting.Value;
            }

            var faxNumberSetting = src.FirstOrDefault(x => x.Name == CompanySettingsNames.FaxNumber.ToString());
            if (faxNumberSetting != null)
            {
                dest.FaxNumber = faxNumberSetting.Value;
            }

            var emailSetting = src.FirstOrDefault(x => x.Name == CompanySettingsNames.Email.ToString());
            if (emailSetting != null)
            {
                dest.Email = emailSetting.Value;
            }

            dest.ApplicationName = StaticSettings.DefaultApplicationName;
            return dest;
        }

        public static ConfigurationProfileSetupDistributorViewModel Map(IEnumerable<CompanySetting> src, ConfigurationProfileSetupDistributorViewModel dest)
        {
            var settings = new List<CompanySetting>();

            var languageSetting = src.FirstOrDefault(x => x.Name == CompanySettingsNames.Language.ToString());
            if (languageSetting != null && !string.IsNullOrEmpty(languageSetting.Value))
            {
                dest.Language = int.Parse(languageSetting.Value);
            }

            var webSetting = src.FirstOrDefault(x => x.Name == CompanySettingsNames.Web.ToString());
            if (webSetting != null)
            {
                dest.Web = webSetting.Value;
            }

            var dateFormatSetting = src.FirstOrDefault(x => x.Name == CompanySettingsNames.DateFormat.ToString());
            if (!string.IsNullOrEmpty(dateFormatSetting?.Value))
            {
                dest.DateFormat = int.Parse(dateFormatSetting.Value);
            }

            var measurementUnitSetting = src.FirstOrDefault(x => x.Name == CompanySettingsNames.MeasurementUnit.ToString());
            if (dateFormatSetting != null && measurementUnitSetting != null && !string.IsNullOrEmpty(dateFormatSetting.Value))
            {
                dest.MeasurementUnitId = int.Parse(measurementUnitSetting.Value);
            }

            var companyLogoSetting = src.FirstOrDefault(x => x.Name == CompanySettingsNames.CompanyLogo.ToString());
            if (companyLogoSetting != null)
            {
                dest.LogoFileName = companyLogoSetting.Value;
                if (!string.IsNullOrEmpty(dest.LogoFileName))
                {
                    var appContext = IocManager.Resolve<IAppContext>();
                    dest.LogoDownloadUrl = appContext.MapUrl($@"{appContext.CompanyContentFolder}/{companyLogoSetting.TenantId}/{dest.LogoFileName}");
                }
            }

            //var accountNumberSetting = src.FirstOrDefault(x => x.Name == CompanySettingsNames.AccountNumber.ToString());
            //if (accountNumberSetting != null)
            //{
            //    dest.AccountNumber = accountNumberSetting.Value;
            //}

            var phoneNumberSetting = src.FirstOrDefault(x => x.Name == CompanySettingsNames.PhoneNumber.ToString());
            if (phoneNumberSetting != null)
            {
                dest.PhoneNumber = phoneNumberSetting.Value;
            }

            var phoneNumber2Setting = src.FirstOrDefault(x => x.Name == CompanySettingsNames.PhoneNumber2.ToString());
            if (phoneNumber2Setting != null)
            {
                dest.PhoneNumber2 = phoneNumber2Setting.Value;
            }

            var faxNumberSetting = src.FirstOrDefault(x => x.Name == CompanySettingsNames.FaxNumber.ToString());
            if (faxNumberSetting != null)
            {
                dest.FaxNumber = faxNumberSetting.Value;
            }

            var emailSetting = src.FirstOrDefault(x => x.Name == CompanySettingsNames.Email.ToString());
            if (emailSetting != null)
            {
                dest.Email = emailSetting.Value;
            }

            dest.ApplicationName = StaticSettings.DefaultApplicationName;

            return dest;
        }

        public static ConfigurationProfileSetupDealersViewModel Map(IEnumerable<CompanySetting> src, ConfigurationProfileSetupDealersViewModel dest)
        {
            var languageSetting = src.FirstOrDefault(x => x.Name == CompanySettingsNames.Language.ToString());
            if (!string.IsNullOrEmpty(languageSetting?.Value))
            {
                dest.Language = int.Parse(languageSetting.Value);
            }

            var webSetting = src.FirstOrDefault(x => x.Name == CompanySettingsNames.Web.ToString());
            if (webSetting != null)
            {
                dest.Web = webSetting.Value;
            }

            var dateFormatSetting = src.FirstOrDefault(x => x.Name == CompanySettingsNames.DateFormat.ToString());
            if (!string.IsNullOrEmpty(dateFormatSetting?.Value))
            {
                dest.DateFormat = int.Parse(dateFormatSetting.Value);
            }

            var measurementUnitSetting = src.FirstOrDefault(x => x.Name == CompanySettingsNames.MeasurementUnit.ToString());
            if (dateFormatSetting != null && measurementUnitSetting != null && !string.IsNullOrEmpty(dateFormatSetting.Value))
            {
                dest.MeasurementUnitId = int.Parse(measurementUnitSetting.Value);
            }

            var companyLogoSetting = src.FirstOrDefault(x => x.Name == CompanySettingsNames.CompanyLogo.ToString());
            if (companyLogoSetting != null)
            {
                dest.LogoFileName = companyLogoSetting.Value;
                if (!string.IsNullOrEmpty(dest.LogoFileName))
                {
                    var appContext = IocManager.Resolve<IAppContext>();
                    dest.LogoDownloadUrl = appContext.MapUrl($@"{appContext.CompanyContentFolder}/{companyLogoSetting.TenantId}/{dest.LogoFileName}");
                }
            }

            //var accountNumberSetting = src.FirstOrDefault(x => x.Name == CompanySettingsNames.AccountNumber.ToString());
            //if (accountNumberSetting != null)
            //{
            //    dest.AccountNumber = accountNumberSetting.Value;
            //}

            var phoneNumberSetting = src.FirstOrDefault(x => x.Name == CompanySettingsNames.PhoneNumber.ToString());
            if (phoneNumberSetting != null)
            {
                dest.PhoneNumber = phoneNumberSetting.Value;
            }

            var phoneNumber2Setting = src.FirstOrDefault(x => x.Name == CompanySettingsNames.PhoneNumber2.ToString());
            if (phoneNumber2Setting != null)
            {
                dest.PhoneNumber2 = phoneNumber2Setting.Value;
            }

            var faxNumberSetting = src.FirstOrDefault(x => x.Name == CompanySettingsNames.FaxNumber.ToString());
            if (faxNumberSetting != null)
            {
                dest.FaxNumber = faxNumberSetting.Value;
            }

            var emailSetting = src.FirstOrDefault(x => x.Name == CompanySettingsNames.Email.ToString());
            if (emailSetting != null)
            {
                dest.Email = emailSetting.Value;
            }

            dest.ApplicationName = StaticSettings.DefaultApplicationName;
            return dest;
        }

        public static ConfigurationMenuSetupViewModel Map(Company src, ConfigurationMenuSetupViewModel dest)
        {
            if (dest == null)
            {
                dest = new ConfigurationMenuSetupViewModel();
            }

            dest.Id = src.Id;

            var useOemMenusSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.UseOEMMenus.ToString());
            if (!string.IsNullOrEmpty(useOemMenusSetting?.Value))
            {
                dest.UseOEMMenus = bool.Parse(useOemMenusSetting.Value);
            }
            else
            {
                dest.UseOEMMenus = true;
            }

            var defaultLaborRateSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.DefaultLaborRate.ToString());
            if (!string.IsNullOrEmpty(defaultLaborRateSetting?.Value))
            {
                dest.DefaultLaborRate = decimal.Parse(defaultLaborRateSetting.Value);
            }
            else
            {
                dest.DefaultLaborRate = 100;
            }

            var convertCurrencySetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.ConvertCurrency.ToString());
            if (!string.IsNullOrEmpty(convertCurrencySetting?.Value))
            {
                dest.ConvertCurrency = bool.Parse(convertCurrencySetting.Value);
            }
            else
            {
                dest.ConvertCurrency = false;
            }
            var exchangeRateSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.ExchangeRate.ToString());
            if (!string.IsNullOrEmpty(exchangeRateSetting?.Value))
            {
                dest.ExchangeRate = decimal.Parse(exchangeRateSetting.Value);
            }

            var displayServicesMenuContentSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.DisplayServicesMenuContent.ToString());
            if (!string.IsNullOrEmpty(displayServicesMenuContentSetting?.Value))
            {
                dest.DisplayServicesMenuContent = bool.Parse(displayServicesMenuContentSetting.Value);
            }
            else
            {
                dest.DisplayServicesMenuContent = false;
            }

            var rollAdditionalServiceIntoMainMenuSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.RollAdditionalServiceIntoMainMenu.ToString());
            if (!string.IsNullOrEmpty(rollAdditionalServiceIntoMainMenuSetting?.Value))
            {
                dest.RollAdditionalServiceIntoMainMenu = bool.Parse(rollAdditionalServiceIntoMainMenuSetting.Value);
            }
            else
            {
                dest.RollAdditionalServiceIntoMainMenu = false;
            }

            var menuLevelsSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.MenuLevels.ToString());
            if (!string.IsNullOrEmpty(menuLevelsSetting?.Value))
            {
                dest.MenuLevels = int.Parse(menuLevelsSetting.Value);
            }
            else
            {
                dest.MenuLevels = 3;
            }

            var menuLabelLevel1Setting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.MenuLabelLevel1.ToString());
            dest.MenuLabelLevel1 = menuLabelLevel1Setting != null ? menuLabelLevel1Setting.Value : Labels.MenuLabelLevel1;

            var menuLabelLevel2Setting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.MenuLabelLevel2.ToString());
            dest.MenuLabelLevel2 = menuLabelLevel2Setting != null ? menuLabelLevel2Setting.Value : Labels.MenuLabelLevel2;

            var menuLabelLevel3Setting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.MenuLabelLevel3.ToString());
            dest.MenuLabelLevel3 = menuLabelLevel3Setting != null ? menuLabelLevel3Setting.Value : Labels.MenuLabelLevel3;

            var menuPrefixLevel1Setting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.MenuPrefixLevel1.ToString());
            if (menuPrefixLevel1Setting != null)
            {
                dest.MenuPrefixLevel1 = menuPrefixLevel1Setting.Value;
            }

            var menuPrefixLevel2Setting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.MenuPrefixLevel2.ToString());
            if (menuPrefixLevel2Setting != null)
            {
                dest.MenuPrefixLevel2 = menuPrefixLevel2Setting.Value;
            }

            var menuPrefixLevel3Setting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.MenuPrefixLevel3.ToString());
            if (menuPrefixLevel3Setting != null)
            {
                dest.MenuPrefixLevel3 = menuPrefixLevel3Setting.Value;
            }

            var menuSuffixLevel1Setting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.MenuSuffixLevel1.ToString());
            if (menuSuffixLevel1Setting != null)
            {
                dest.MenuSuffixLevel1 = menuSuffixLevel1Setting.Value;
            }

            var menuSuffixLevel2Setting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.MenuSuffixLevel2.ToString());
            if (menuSuffixLevel2Setting != null)
            {
                dest.MenuSuffixLevel2 = menuSuffixLevel2Setting.Value;
            }

            var menuSuffixLevel3Setting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.MenuSuffixLevel3.ToString());
            if (menuSuffixLevel3Setting != null)
            {
                dest.MenuSuffixLevel3 = menuSuffixLevel3Setting.Value;
            }

            var lofMenuLevelsSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.LOFMenuLevels.ToString());
            if (lofMenuLevelsSetting != null && !string.IsNullOrEmpty(lofMenuLevelsSetting.Value))
            {
                dest.LOFMenuLevels = int.Parse(lofMenuLevelsSetting.Value);
            }
            else
            {
                dest.LOFMenuLevels = 3;
            }

            var lofMenuLabelLevel1Setting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.LOFMenuLabelLevel1.ToString());
            dest.LOFMenuLabelLevel1 = lofMenuLabelLevel1Setting != null ? lofMenuLabelLevel1Setting.Value : Labels.LOFMenuLabel1;

            var lofMenuLabelLevel2Setting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.LOFMenuLabelLevel2.ToString());
            dest.LOFMenuLabelLevel2 = lofMenuLabelLevel2Setting != null ? lofMenuLabelLevel2Setting.Value : Labels.LOFMenuLabel2;

            var lofMenuLabelLevel3Setting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.LOFMenuLabelLevel3.ToString());
            dest.LOFMenuLabelLevel3 = lofMenuLabelLevel3Setting != null ? lofMenuLabelLevel3Setting.Value : Labels.LOFMenuLabel3;

            var lofMenuOpCodeLevel1Setting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.LOFMenuOpCodeLevel1.ToString());
            if (lofMenuOpCodeLevel1Setting != null)
            {
                dest.LOFMenuOpCodeLevel1 = lofMenuOpCodeLevel1Setting.Value;
            }

            var lofMenuOpCodeLevel2Setting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.LOFMenuOpCodeLevel2.ToString());
            if (lofMenuOpCodeLevel2Setting != null)
            {
                dest.LOFMenuOpCodeLevel2 = lofMenuOpCodeLevel2Setting.Value;
            }

            var lofMenuOpCodeLevel3Setting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.LOFMenuOpCodeLevel3.ToString());
            if (lofMenuOpCodeLevel3Setting != null)
            {
                dest.LOFMenuOpCodeLevel3 = lofMenuOpCodeLevel3Setting.Value;
            }

            return dest;
        }

        public static ConfigurationDocumentSetupViewModel Map(Company src, ConfigurationDocumentSetupViewModel dest)
        {
            if (dest == null)
            {
                dest = new ConfigurationDocumentSetupViewModel();
            }

            dest.Id = src.Id;

            var invoiceNoteTextSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.InvoiceNoteText.ToString());
            dest.InvoiceNoteText = invoiceNoteTextSetting != null ? invoiceNoteTextSetting.Value : Labels.InvoiceNoteTextSetting;

            var disclaimerTextSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.DisclaimerText.ToString());
            dest.DisclaimerText = disclaimerTextSetting != null ? disclaimerTextSetting.Value : Labels.DisclaimerTextSetting;

            var displayInvoiceNumberSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.DisplayInvoiceNumber.ToString());
            if (!string.IsNullOrEmpty(displayInvoiceNumberSetting?.Value))
            {
                dest.DisplayInvoiceNumber = bool.Parse(displayInvoiceNumberSetting.Value);
            }
            else
            {
                dest.DisplayInvoiceNumber = false;
            }

            var printAdvisorInformationSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.PrintAdvisorInformation.ToString());
            if (!string.IsNullOrEmpty(printAdvisorInformationSetting?.Value))
            {
                dest.PrintAdvisorInformation = bool.Parse(printAdvisorInformationSetting.Value);
            }
            else
            {
                dest.PrintAdvisorInformation = false;
            }

            var printShopChargesSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.PrintShopCharges.ToString());
            if (!string.IsNullOrEmpty(printShopChargesSetting?.Value))
            {
                dest.PrintShopCharges = bool.Parse(printShopChargesSetting.Value);
            }
            else
            {
                dest.PrintShopCharges = true;
            }

            var shopChargeAmountSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.ShopChargeAmount.ToString());
            if (!string.IsNullOrEmpty(shopChargeAmountSetting?.Value))
            {
                dest.ShopChargeAmount = decimal.Parse(shopChargeAmountSetting.Value);
            }
            else
            {
                dest.ShopChargeAmount = 10;
            }

            var shopChargesApplyToPartsSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.ShopChargesApplyToParts.ToString());
            if (!string.IsNullOrEmpty(shopChargesApplyToPartsSetting?.Value))
            {
                dest.ShopChargesApplyToParts = bool.Parse(shopChargesApplyToPartsSetting.Value);
            }
            else
            {
                dest.ShopChargesApplyToParts = true;
            }

            var shopChargesApplyToLaborSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.ShopChargesApplyToLabor.ToString());
            if (!string.IsNullOrEmpty(shopChargesApplyToLaborSetting?.Value))
            {
                dest.ShopChargesApplyToLabor = bool.Parse(shopChargesApplyToLaborSetting.Value);
            }
            else
            {
                dest.ShopChargesApplyToLabor = true;
            }

            var shopChargeMinimumSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.ShopChargeMinimum.ToString());
            if (!string.IsNullOrEmpty(shopChargeMinimumSetting?.Value))
            {
                dest.ShopChargeMinimum = decimal.Parse(shopChargeMinimumSetting.Value);
            }
            else
            {
                dest.ShopChargeMinimum = 1;
            }

            var shopChargeMaximumSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.ShopChargeMaximum.ToString());
            if (!string.IsNullOrEmpty(shopChargeMaximumSetting?.Value))
            {
                dest.ShopChargeMaximum = decimal.Parse(shopChargeMaximumSetting.Value);
            }
            else
            {
                dest.ShopChargeMaximum = 25;
            }

            var printTaxesOnDocumentsSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.PrintTaxesOnDocuments.ToString());
            if (!string.IsNullOrEmpty(printTaxesOnDocumentsSetting?.Value))
            {
                dest.PrintTaxesOnDocuments = bool.Parse(printTaxesOnDocumentsSetting.Value);
            }
            else
            {
                dest.PrintTaxesOnDocuments = false;
            }

            var displayTexesInMenusSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.DisplayTexesInMenus.ToString());
            if (!string.IsNullOrEmpty(displayTexesInMenusSetting?.Value))
            {
                dest.DisplayTaxesInMenus = bool.Parse(displayTexesInMenusSetting.Value);
            }
            else
            {
                dest.DisplayTaxesInMenus = false;
            }

            var taxRateAmountSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.TaxRateAmount.ToString());
            if (!string.IsNullOrEmpty(taxRateAmountSetting?.Value))
            {
                dest.TaxRateAmount = decimal.Parse(taxRateAmountSetting.Value);
            }

            var taxRateApplyToPartsSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.TaxRateApplyToParts.ToString());
            if (!string.IsNullOrEmpty(taxRateApplyToPartsSetting?.Value))
            {
                dest.TaxRateApplyToParts = bool.Parse(taxRateApplyToPartsSetting.Value);
            }
            else
            {
                dest.TaxRateApplyToParts = true;
            }

            var taxRateApplyToLaborSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.TaxRateApplyToLabor.ToString());
            if (!string.IsNullOrEmpty(taxRateApplyToLaborSetting?.Value))
            {
                dest.TaxRateApplyToLabor = bool.Parse(taxRateApplyToLaborSetting.Value);
            }
            else
            {
                dest.TaxRateApplyToLabor = true;
            }

            return dest;
        }

        public static ConfigurationApplicationSettingViewModel Map(Company src, ConfigurationApplicationSettingViewModel dest)
        {
            if (dest == null)
            {
                dest = new ConfigurationApplicationSettingViewModel();
            }

            dest.Id = src.Id;

            var sessionExpirationSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.SessionExpiration.ToString());
            if (sessionExpirationSetting != null && !string.IsNullOrEmpty(sessionExpirationSetting.Value))
            {
                dest.SessionExpiration = int.Parse(sessionExpirationSetting.Value);
            }
            else
            {
                var parentSessionExpirationSetting = src.ParentCompany != null ? src.ParentCompany.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.SessionExpiration.ToString()) : null;
                dest.SessionExpiration = parentSessionExpirationSetting != null ? int.Parse(parentSessionExpirationSetting.Value) : 0;
            }

            var dmsTypeIdSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.DmsTypeId.ToString());
            if (dmsTypeIdSetting != null)
            {
                if (string.IsNullOrEmpty(dmsTypeIdSetting.Value))
                {
                    dest.DmsTypeId = -1;
                }
                else
                {
                    dest.DmsTypeId = int.Parse(dmsTypeIdSetting.Value);
                }
            }

            var parkedMenuRetentionDurationSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.ParkedMenuRetentionDuration.ToString());
            if (parkedMenuRetentionDurationSetting != null && !string.IsNullOrEmpty(parkedMenuRetentionDurationSetting.Value))
            {
                dest.ParkedMenuRetentionDuration = int.Parse(parkedMenuRetentionDurationSetting.Value);
            }
            else
            {
                dest.ParkedMenuRetentionDuration = 3; // set on 3 days as default value
            }

            var advisorsHaveDeleteParkedMenuPermissionSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.AdvisorsHaveDeleteParkedMenuPermission.ToString());
            if (advisorsHaveDeleteParkedMenuPermissionSetting != null && !string.IsNullOrEmpty(advisorsHaveDeleteParkedMenuPermissionSetting.Value))
            {
                dest.AdvisorsHaveDeleteParkedMenuPermission = bool.Parse(advisorsHaveDeleteParkedMenuPermissionSetting.Value);
            }
            else
            {
                dest.AdvisorsHaveDeleteParkedMenuPermission = true;
            }

            var flagMenuAsModifiedForAnyChangeSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.FlagMenuAsModifiedForAnyChange.ToString());
            if (flagMenuAsModifiedForAnyChangeSetting != null && !string.IsNullOrEmpty(flagMenuAsModifiedForAnyChangeSetting.Value))
            {
                dest.FlagMenuAsModifiedForAnyChange = bool.Parse(flagMenuAsModifiedForAnyChangeSetting.Value);
            }
            else
            {
                dest.FlagMenuAsModifiedForAnyChange = true;
            }

            var averageMilesPerYearSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.AverageMilesPerYear.ToString());
            if (averageMilesPerYearSetting != null && !string.IsNullOrEmpty(averageMilesPerYearSetting.Value))
            {
                dest.AverageMilesPerYear = int.Parse(averageMilesPerYearSetting.Value);
            }
            else
            {
                dest.AverageMilesPerYear = 10000;
            }

            var isPlanActiveSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.IsPlanActive.ToString());
            if (isPlanActiveSetting != null && !string.IsNullOrEmpty(isPlanActiveSetting.Value))
            {
                dest.IsPlanActive = bool.Parse(isPlanActiveSetting.Value);
            }
            else
            {
                dest.IsPlanActive = false;
            }

            var planProviderSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.PlanProvider.ToString());
            if (planProviderSetting != null)
            {
                dest.PlanProvider = planProviderSetting.Value;
            }

            var planNameSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.PlanName.ToString());
            if (planNameSetting != null)
            {
                dest.PlanName = planNameSetting.Value;
            }

            var isRemotePrintSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.IsRemotePrint.ToString());
            if (isRemotePrintSetting != null && !string.IsNullOrEmpty(isRemotePrintSetting.Value))
            {
                dest.IsRemotePrint = bool.Parse(isRemotePrintSetting.Value);
            }
            else
            {
                dest.IsRemotePrint = false;
            }

            var printServerSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.PrintServer.ToString());
            if (printServerSetting != null)
            {
                dest.PrintServer = printServerSetting.Value;
            }
            else
            {
                dest.PrintServer = Labels.PrintServerSetting;
            }

            var printPortSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.PrintPort.ToString());
            if (printPortSetting != null)
            {
                dest.PrintPort = printPortSetting.Value;
            }
            else
            {
                dest.PrintPort = Labels.PrintPortSetting;
            }



            var homeButtonOrderSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.HomeButtonOrder.ToString());
            if (homeButtonOrderSetting != null && !string.IsNullOrEmpty(homeButtonOrderSetting.Value))
            {
                dest.HomeButtonOrder = int.Parse(homeButtonOrderSetting.Value);
            }
            else
            {
                dest.HomeButtonOrder = 1;
            }

            var appointmentsButtonOrderSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.AppointmentsButtonOrder.ToString());
            if (appointmentsButtonOrderSetting != null && !string.IsNullOrEmpty(appointmentsButtonOrderSetting.Value))
            {
                dest.AppointmentsButtonOrder = int.Parse(appointmentsButtonOrderSetting.Value);
            }
            else
            {
                dest.AppointmentsButtonOrder = 2;
            }

            var documentSearchButtonOrderSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.DocumentSearchButtonOrder.ToString());
            if (documentSearchButtonOrderSetting != null && !string.IsNullOrEmpty(documentSearchButtonOrderSetting.Value))
            {
                dest.DocumentSearchButtonOrder = int.Parse(documentSearchButtonOrderSetting.Value);
            }
            else
            {
                dest.DocumentSearchButtonOrder = 3;
            }

            var reportingButtonOrderSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.ReportingButtonOrder.ToString());
            if (reportingButtonOrderSetting != null && !string.IsNullOrEmpty(reportingButtonOrderSetting.Value))
            {
                dest.ReportingButtonOrder = int.Parse(reportingButtonOrderSetting.Value);
            }
            else
            {
                dest.ReportingButtonOrder = 4;
            }

            var plannerButtonOrderSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.PlannerButtonOrder.ToString());
            if (plannerButtonOrderSetting != null && !string.IsNullOrEmpty(plannerButtonOrderSetting.Value))
            {
                dest.PlannerButtonOrder = int.Parse(plannerButtonOrderSetting.Value);
            }
            else
            {
                dest.PlannerButtonOrder = 5;
            }

            var mpiButtonOrderSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.MpiButtonOrder.ToString());
            if (mpiButtonOrderSetting != null && !string.IsNullOrEmpty(mpiButtonOrderSetting.Value))
            {
                dest.MpiButtonOrder = int.Parse(mpiButtonOrderSetting.Value);
            }
            else
            {
                dest.MpiButtonOrder = 6;
            }

            var roPrepButtonOrderSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.RoPrepButtonOrder.ToString());
            if (roPrepButtonOrderSetting != null && !string.IsNullOrEmpty(roPrepButtonOrderSetting.Value))
            {
                dest.RoPrepButtonOrder = int.Parse(roPrepButtonOrderSetting.Value);
            }
            else
            {
                dest.RoPrepButtonOrder = 7;
            }

            return dest;
        }

        public static ConfigurationCorpApplicationSettingsViewModel Map(Company src, ConfigurationCorpApplicationSettingsViewModel dest)
        {
            var dataRefreshFrequencyInDaysSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.DataRefreshFrequencyInDays.ToString());
            if (dataRefreshFrequencyInDaysSetting != null && !string.IsNullOrEmpty(dataRefreshFrequencyInDaysSetting.Value))
            {
                dest.DataRefreshFrequencyInDays = int.Parse(dataRefreshFrequencyInDaysSetting.Value);
            }

            var sessionExpiryInMinutesSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.SessionExpiration.ToString());
            if (sessionExpiryInMinutesSetting != null && !string.IsNullOrEmpty(sessionExpiryInMinutesSetting.Value))
            {
                dest.SessionExpiration = int.Parse(sessionExpiryInMinutesSetting.Value);
            }

            var supportLinkSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.SupportLink.ToString());
            if (supportLinkSetting != null)
            {
                dest.SupportLink = supportLinkSetting.Value;
            }

            var supportLinkDisplayTextSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.SupportLinkDisplayText.ToString());
            if (supportLinkDisplayTextSetting != null)
            {
                dest.SupportLinkDisplayText = supportLinkDisplayTextSetting.Value;
            }

            var privacyPolicyLinkSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.PrivacyPolicyLink.ToString());
            if (privacyPolicyLinkSetting != null)
            {
                dest.PrivacyPolicyLink = privacyPolicyLinkSetting.Value;
            }

            var privacyPolicyLinkDisplayTextSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.PrivacyPolicyLinkDisplayText.ToString());
            if (privacyPolicyLinkDisplayTextSetting != null)
            {
                dest.PrivacyPolicyLinkDisplayText = privacyPolicyLinkDisplayTextSetting.Value;
            }

            var smartVmaLinkSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.SmartVmaLink.ToString());
            if (smartVmaLinkSetting != null)
            {
                dest.SmartVmaLink = smartVmaLinkSetting.Value;
            }

            var smartVmaLinkDisplayTextSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.SmartVmaLinkDisplayText.ToString());
            if (smartVmaLinkDisplayTextSetting != null)
            {
                dest.SmartVmaLinkDisplayText = smartVmaLinkDisplayTextSetting.Value;
            }

            var applicationNameSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.ApplicationName.ToString());
            if (applicationNameSetting != null)
            {
                dest.ApplicationName = applicationNameSetting.Value;
            }
            else
            {
                dest.ApplicationName = StaticSettings.DefaultApplicationName;
            }
            return dest;
        }

        public static ConfigurationLOFMenuViewModel Map(Company src, ConfigurationLOFMenuViewModel dest)
        {
            if (dest == null)
            {
                dest = new ConfigurationLOFMenuViewModel();
            }

            dest.Id = src.Id;

            char[] charSeparator = new char[] { ';' };
            var service = IocManager.Resolve<ILofService>();
            dest.Images = service.GetLOFImages();

            var lofMenuLevel1ImagesSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.LOFMenuLevel1Images.ToString());
            if (lofMenuLevel1ImagesSetting != null)
            {
                dest.LOFMenuLevel1Images = lofMenuLevel1ImagesSetting.Value;
                string[] result;
                result = lofMenuLevel1ImagesSetting.Value.Split(charSeparator, StringSplitOptions.None);
                result = result.Where(w => w != "").ToArray();
                foreach (var item in result)
                {
                    dest.Images.Where(x => x.LOFMenuLevel == 1 && x.ImageName == item).Select(x => { x.IsSelected = true; return x; }).ToList();
                }
            }

            var lofMenuLevel2ImagesSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.LOFMenuLevel2Images.ToString());
            if (lofMenuLevel2ImagesSetting != null)
            {
                dest.LOFMenuLevel2Images = lofMenuLevel2ImagesSetting.Value;
                string[] result;
                result = lofMenuLevel2ImagesSetting.Value.Split(charSeparator, StringSplitOptions.None);
                result = result.Where(w => w != "").ToArray();
                foreach (var item in result)
                {
                    dest.Images.Where(x => x.LOFMenuLevel == 2 && x.ImageName == item).Select(x => { x.IsSelected = true; return x; }).ToList();
                }
            }

            var lofMenuLevel3ImagesSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.LOFMenuLevel3Images.ToString());
            if (lofMenuLevel3ImagesSetting != null)
            {
                dest.LOFMenuLevel3Images = lofMenuLevel3ImagesSetting.Value;
                string[] result;
                result = lofMenuLevel3ImagesSetting.Value.Split(charSeparator, StringSplitOptions.None);
                result = result.Where(w => w != "").ToArray();
                foreach (var item in result)
                {
                    dest.Images.Where(x => x.LOFMenuLevel == 3 && x.ImageName == item).Select(x => { x.IsSelected = true; return x; }).ToList();
                }
            }

            return dest;
        }

        public static IEnumerable<CompanySetting> Map(ConfigurationLOFMenuViewModel src)
        {
            var settings = new List<CompanySetting>();

            settings.Add(new CompanySetting { Name = CompanySettingsNames.LOFMenuLevel1Images.ToString(), Value = src.LOFMenuLevel1Images.ToString() });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.LOFMenuLevel2Images.ToString(), Value = src.LOFMenuLevel2Images.ToString() });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.LOFMenuLevel3Images.ToString(), Value = src.LOFMenuLevel3Images.ToString() });
            settings.Add(new CompanySetting { Name = CompanySettingsNames.LOFMenuSelectedLevel.ToString(), Value = src.LOFMenuSelectedLevel.ToString() });


            return settings;
        }

    }
}
