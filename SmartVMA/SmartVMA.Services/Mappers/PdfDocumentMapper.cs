﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static PdfDocumentViewModel Map(PdfDocument src, PdfDocumentViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new PdfDocumentViewModel();
            }

            dest.PdfDocumentId = src.Id;

            dest.AppointmentPresentationId = src.AppointmentPresentationId;
            dest.DealerCustomerId = src.DealerCustomerId;
            dest.DealerId = src.TenantId;

            dest.Name = src.Name;
            dest.FilePathName = src.FilePathName;
            dest.VIN = src.VIN;
            dest.InvoiceNumber = src.InvoiceNumber;
            dest.DocumentType = src.DocumentType;

            return dest;
        }

        public static PdfDocument Map(PdfDocumentViewModel src, PdfDocument dest = null)
        {
            if (dest == null)
            {
                dest = new PdfDocument();
            }            

            dest.AppointmentPresentationId = src.AppointmentPresentationId;
            dest.DealerCustomerId = src.DealerCustomerId;
            dest.TenantId = src.DealerId;

            dest.Name = src.Name;
            dest.FilePathName = src.FilePathName;
            dest.VIN = src.VIN;
            dest.InvoiceNumber = src.InvoiceNumber;
            dest.DocumentType = src.DocumentType;

            dest.LanguageId = src.LanguageId;

            return dest;
        }

    }
}
