﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static OemServicePartViewModel Map(OemServicePart src, OemServicePartViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new OemServicePartViewModel();
            }

            dest.Id = src.Id;
            dest.OemBasicServiceId = src.OemBasicServiceId;
            dest.OemPartId = src.OemPartId;
            dest.Quantity = src.Quantity;
            dest.TimeCreated = src.TimeCreated;
            dest.TimeUpdated = src.TimeUpdated;
            

            return dest;
        }

        public static OemServicePart Map(OemServicePartViewModel src, OemServicePart dest = null)
        {
            if (dest == null)
            {
                dest = new OemServicePart();
            }

            dest.OemBasicServiceId = src.OemBasicServiceId;
            dest.OemPartId = src.OemPartId;
            dest.Quantity = src.Quantity;
            dest.TimeCreated = src.TimeCreated;
            dest.TimeUpdated = src.TimeUpdated;

            return dest;
        }
    }
}
