﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Resources;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static ILifeViewModel Map(ILife src, List<ILife_History> historyList, DealerCustomer dCustomer, ILifeViewModel dest = null)
        {
            var companyService = IocManager.Resolve<ICompanyService>();
            if (dest == null)
            {
                dest = new ILifeViewModel();
            }

            dest.Id = src.Id;
            dest.Active = src.Active ? "Y" : "N";
            dest.SalesDate = src.Contract_Date;
            dest.RecordID = src.Record_ID; 
            dest.Store = src.Store; 
            dest.Prefix = src.Prefix;
            dest.DealerNo = src.Deal_No; 
            dest.SalePrice = src.Sale_Price; 
            dest.STDEXE = src.STD_EXE; 
            dest.PastDue = src.Past_Due ? "Y" : "N"; 
            dest.LeaseTermDate = src.Lease_Term_Date; 
            dest.ContractNumber = src.Contract_Number;
            dest.CancelDate = src.Cancel_Date; 
            dest.StockNum = src.Stock_Num; 
            dest.Surcharge = src.Surcharge; 
            dest.Type = src.GDS; 
            dest.FirstName = src.First_Name;
            dest.MiddleInitial = src.Middle_Initial;
            dest.LastName = src.Last_Name;
            dest.ContractHolder2 = src.Contract_Holder_2;
            dest.Address = src.Address;
            dest.City = src.City;
            dest.State = src.State;
            dest.Zip = src.Zip;
            dest.SoldAsNewUsed = src.Sold_As__New_Used_;
            dest.VIN = src.VIN;
            dest.VinLast8 = src.Vin_Last_8;
            dest.Make = src.Make;
            dest.Model = src.Model;
            dest.Year = src.Model_Year;
            dest.EligibilityMessage = src.EligibilityMessage;
            dest.Prefix = src.Prefix;
            dest.ContractNumber = src.Contract_Number;
            dest.RONum = src.LastRONum;
            dest.Company = src.CoName;
            dest.Mileage = src.Mileage;
            dest.RoDate = src.LastClaimDate;

            if (historyList.Count != 0)
            {
                foreach (var item in historyList)
                {
                    var historyItem = new ILifeHistoryViewModel();
                    historyItem.Prefix = item.Prefix;
                    historyItem.ContractNumber = item.Contract_Number;
                    historyItem.RONum = item.LastRONum;
                    historyItem.Company = item.CoName;
                    historyItem.Name = item.Last_Name + ", " + item.First_Name;
                    historyItem.Mileage = item.Mileage;
                    historyItem.RoDate = item.LastClaimDate;

                    dest.HistoryItems.Add(historyItem);

                }
            }
                

            return dest;
        }

    }
}
