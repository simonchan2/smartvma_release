﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static EngineOilConfigurationViewModel Map(EngineOilORule src, EngineOilConfigurationViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new EngineOilConfigurationViewModel();
            }
            dest.Id = src.Id;
            dest.OEMOilType = src.FluidType;
            dest.OEMOilViscosity = src.FluidViscosity;
            dest.EngineOilProductId = src.EngineOilProductId;
            dest.OilType = src.EngineOilProduct.FluidType;
            dest.OilViscosity = src.EngineOilProduct.FluidViscosity;
            dest.OilTypeInt = GetOilEnumInt(src.EngineOilProduct.FluidType, typeof(OilType));
            dest.PartName = src.EngineOilProduct.PartName;
            dest.PartNumber = src.EngineOilProduct.PartNumber;
            dest.UnitPrice = src.EngineOilProduct.UnitPrice;
            var appContext = IocManager.Resolve<IAppContext>();
            dest.CompanyId = appContext.CurrentIdentity.TenantId;

            return dest;
        }

        public static EngineOilORule Map(EngineOilConfigurationViewModel src, EngineOilORule dest = null)
        {
            if (dest == null)
            {
                dest = new EngineOilORule();
            }
            dest.FluidType = src.OEMOilType;
            dest.FluidViscosity = src.OEMOilViscosity; 
            dest.EngineOilProductId = src.EngineOilProductId;

            return dest;
        }
    }
}
