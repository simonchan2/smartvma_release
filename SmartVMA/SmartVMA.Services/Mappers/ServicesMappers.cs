﻿using System;
using SmartVMA.Core;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.OutputModels;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Core.Services;
using System.Linq;
using System.Threading;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static ServiceDetailViewModel Map(ServicesProcedureModel src, ServiceDetailViewModel dest = null)
        {
            var companyService = IocManager.Resolve<ICompanyService>();
            if (dest == null)
            {
                dest = new ServiceDetailViewModel();
            }

            dest.Id = src.ServiceId;
            dest.OpCode = src.OpCode;
            dest.CarId = src.CarId;
            dest.Description = src.FullDescription;

            dest.LaborHours = src.LaborHour;
            dest.LaborPrice = src.LaborPrice;
            dest.LaborRate = src.LabourRate;

            dest.IsFluid = src.IsFluid;
            dest.IsEngineOil = src.IsEngineOil;
            dest.IsLOF = src.IsLOF;
            dest.Mileage = src.Mileage;
            dest.IsStrikeOut = src.IsStrikeOut;
            dest.CanBeStriked = src.CanBeStriked;

            dest.IsDeclined = src.IsDeclined;
            dest.IsPreviouslyServed = src.IsPreviouslyServed;

            dest.PartsPrice = src.PartsPrice;
            dest.Price = src.Price;
            dest.MenuLevel = src.MenuLevel;
            dest.VideoCode = src.VideoCode;
            dest.ServiceTypeId = src.ServiceType;
            dest.BgProtectionPlanId = src.BgProtectionPlanId;
            dest.BgProtectionPlanLink = src.BgProtectionPlanId != null && src.BgProtectionPlanUrl == null ? SetBgProtectionPlanLink(src.BgProtectionPlanId) : src.BgProtectionPlanUrl;

            if(!string.IsNullOrEmpty(dest.BgProtectionPlanLink))
            {
                var queryStringParam = "en";
                if (Thread.CurrentThread.CurrentCulture.ToString() == "fr-FR")
                    queryStringParam = "fr";
                else if (Thread.CurrentThread.CurrentCulture.ToString() == "es-ES")
                    queryStringParam = "es";

                dest.BgProtectionPlanLink += "?lang=" + queryStringParam;
            }

            dest.ShopChargesPartsPrice = src.ShopChargesPartsPrice;
            dest.ShopChargesLaborPrice = src.ShopChargesLaborPrice;
            dest.TaxIncludedPartsPrice = src.TaxIncludedPartsPrice;
            dest.TaxIncludedLaborPrice = src.TaxIncludedLaborPrice;

            dest.GrossPrice = src.GrossPrice;
            dest.IsAddedAfter = src.IsAddedAfter ?? false;
            dest.IsAdditionalAddedAfter = src.IsAdditionalAddedAfter ?? false;

            dest.MaxMileageBeforeFirstService = src.MaxMileageBeforeFirstService;
            dest.ServiceInterval = src.ServiceInterval;

            dest.PreviousMileage = src.PreviousMileage.HasValue ? src.PreviousMileage.Value.ToString() : string.Empty;
            dest.PreviousServedDate = src.PreviousServedDate.HasValue ? src.PreviousServedDate.Value.ToString(companyService.GetDateTimeFormat(CompanySettingsNames.DateFormat)) : string.Empty;
            dest.MenuLevel = src.MenuLevel;

            return dest;
        }

        public static Core.Entities.AppointmentService Map(ServicesProcedureModel src, Core.Entities.AppointmentService dest)
        {
            if (dest == null)
            {
                dest = new Core.Entities.AppointmentService();
            }

            dest.OpCode = src.OpCode;
            dest.OpDescription = src.FullDescription;
            dest.IsStriked = src.IsStrikeOut;
            dest.LaborHour = src.LaborHour;
            dest.LaborRate = (decimal)src.LabourRate;
            dest.LaborPrice = (decimal?)src.LaborPrice;
            dest.PartsPrice = (decimal)src.PartsPrice;
            dest.Price = (decimal?)src.Price;

            // TODO: where on the screen the comment is entered
            dest.Comment = "COMMENT_1";
            //dest.PartitionElement = src.PartitionElement;
            //dest.BgProdSubcategoryId = src.BgProdSubcategoryId;

            dest.OrigServiceId = src.ServiceId;
            dest.ServiceTypeId = src.ServiceType;
            dest.MenuLevel = (byte)src.MenuLevel;

            return dest;
        }

        public static ServicesProcedureModel Map(Core.Entities.AppointmentService src, ServicesProcedureModel dest)
        {
            if (dest == null)
            {
                dest = new ServicesProcedureModel();
            }

            dest.ServiceId = src.OrigServiceId ?? 0;
            dest.OpCode = src.OpCode;
            dest.FullDescription = src.OpDescription;
            dest.IsStrikeOut = src.IsStriked;
            dest.LaborHour = src.LaborHour ?? 0;
            dest.LabourRate = (double)(src.LaborRate ?? 0);
            dest.LaborPrice = (double)(src.LaborPrice ?? 0);
            dest.PartsPrice = (double)(src.PartsPrice ?? 0);
            dest.Price = (double)(src.Price ?? 0);
            dest.ServiceType = src.ServiceTypeId;
            dest.MenuLevel = src.MenuLevel ?? 1;
            dest.IsAddedAfter = src.IsAddedAfter ?? false;
            dest.CanBeStriked = (src.IsAddedAfter == true ? false: true); // TODO hardcoded because at this moment we don't have column in query

            return dest;
        }

        public static ConfirmServiceViewModel Map(ConfirmServiceRequestModel src, ConfirmServiceViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new ConfirmServiceViewModel();
            }
            dest.TotalPrice = 0;
            dest.ShopChargesAmount = 0;
            dest.TaxChargesAmount = 0;
            dest.TotalLabourHours = 0;
            dest.MenuTotalPrice = 0;
            dest.Mileage = src.Mileage;
            dest.CarId = src.CarId;
            dest.DealerCustomerId = src.DealerCustomerId;
            dest.AppointmentPresentationId = src.AppointmentPresentationId;
            dest.VIN = src.VIN;
            dest.Invoice = src.Invoice;
            dest.Transmission = src.Transmission;
            dest.Driveline = src.Driveline;
            dest.MenuLevel = src.MenuLevel;
            dest.EnteredMileage = src.EnteredMileage;
            dest.MenuPackageOpCode = src.MenuPackageOpCode;
            dest.Transmission = src.Transmission;
            dest.Driveline = src.Driveline;
            dest.InspectionId = src.InspectionId;
            dest.IsLof = src.IsLof;
            dest.AllServices = src.AllServices != null ? src.AllServices.Select(x => Mapper.Map(x)).ToList() : null;

            return dest;
        }

        public static ServiceDetailViewModel Map(ConfirmServiceRequestItemsModel src, ServiceDetailViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new ServiceDetailViewModel();
            }
            dest.Id = src.Id;
            dest.IsStrikeOut = src.IsStrikeOut;
            dest.MenuLevel = src.MenuLevel;
            dest.ServiceTypeId = (int)src.ServiceTypeId;
            dest.Description = src.AppDescription;
            dest.LaborHours = src.AppLaborHours;
            dest.Price = src.AppPrice;
            dest.LaborPrice = src.LaborPrice;
            dest.IsAddedAfter = src.IsAddedAfter;
            dest.OpCode = src.OpCode;
            dest.PartsPrice = src.PartsPrice;
            dest.LaborRate = src.LaborRate;
            dest.Counter = src.Counter;

            return dest;
        }

        public static Core.Entities.AppointmentService MapAppointment(ConfirmServiceRequestItemsModel src, Core.Entities.AppointmentService dest = null)
        {
            return MapAppointment(src, src.LaborRate, dest);
        }

        public static Core.Entities.AppointmentService MapAppointment(ConfirmServiceRequestItemsModel src, double laborRate, Core.Entities.AppointmentService dest = null)
        {
            if (dest == null)
            {
                dest = new Core.Entities.AppointmentService();
            }
            dest.OpCode = src.OpCode;
            dest.OpDescription = src.AppDescription.Replace("\r\n", string.Empty).Trim();
            dest.IsStriked = src.IsStrikeOut;
            dest.LaborHour = src.AppLaborHours;
            dest.LaborRate = (decimal)laborRate;

            dest.PartsPrice = (decimal)src.PartsPrice;
            dest.LaborPrice = (decimal?)src.LaborPrice;
            dest.Price = (decimal?)src.AppPrice;

            // TODO: this needs to be decided
            //if (src.ServiceTypeId == ServiceType.AdHocAdditionalService)
            //{
            //    dest.PartsPrice = (decimal)src.AppPrice;
            //}

            // TODO: where on the screen the comment is entered
            dest.Comment = "COMMENT_1";
            //dest.PartitionElement = src.PartitionElement;
            //dest.BgProdSubcategoryId = src.BgProdSubcategoryId;

            dest.OrigServiceId = src.ServiceTypeId == ServiceType.AdHocAdditionalService ? (long?)null : src.Id;
            dest.ServiceTypeId = (int)src.ServiceTypeId;
            dest.IsAddedAfter = src.IsAddedAfter;
            dest.MenuLevel = (byte)src.MenuLevel;

            return dest;
        }

        private static string SetBgProtectionPlanLink(int? bgProtectionPlanId)
        {
            if (bgProtectionPlanId != null)
            {
                switch (bgProtectionPlanId)
                {
                    case (int)BgProtectionPlanTypes.LifetimeProtection:
                        return StaticSettings.LifeTimeProtectionPlan;
                    case (int)BgProtectionPlanTypes.ForeverDiesel:
                        return StaticSettings.ForeverDiesel;
                    case (int)BgProtectionPlanTypes.EngineAssurance:
                        return StaticSettings.EngineAssurance;
                }
            }

            return string.Empty;
        }
    }
}
