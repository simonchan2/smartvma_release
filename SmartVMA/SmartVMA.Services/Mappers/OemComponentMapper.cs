﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static OemComponentViewModel Map(OemComponent src, OemComponentViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new OemComponentViewModel();
            }
            dest.Id = src.Id;
            dest.Description = src.Description;
            dest.IsEngineOil = src.IsEngineOil;
            dest.IsFluid = src.IsFluid;
            dest.IsLOF = src.IsLOF;
            dest.IsShownAsOption = src.IsShownAsOption;
            dest.LaborTimeRuleId = src.LaborTimeRuleId;
            dest.PartRuleId = src.PartRuleId;
            dest.TimeCreated = src.TimeCreated;
            dest.TimeUpdated = src.TimeUpdated;
            dest.ToBeIgnored = src.ToBeIgnored;           

            return dest;
        }

        public static OemComponent Map(OemComponentViewModel src, OemComponent dest = null)
        {
            if (dest == null)
            {
                dest = new OemComponent();
            }
            dest.Description = src.Description;
            dest.IsEngineOil = src.IsEngineOil;
            dest.IsFluid = src.IsFluid;
            dest.IsLOF = src.IsLOF;
            dest.IsShownAsOption = src.IsShownAsOption;
            dest.LaborTimeRuleId = src.LaborTimeRuleId;
            dest.PartRuleId = src.PartRuleId;
            dest.TimeCreated = src.TimeCreated;
            dest.TimeUpdated = src.TimeUpdated;
            dest.ToBeIgnored = src.ToBeIgnored;

            return dest;
        }
    }
}
