﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using System.Linq;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static PermissionViewModel Map(Permission src, int roleId, PermissionViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new PermissionViewModel();
            }

            dest.Id = src.Id;
            dest.Name = src.Name;
            dest.Description = src.Description;
            dest.Displayname = src.Displayname;
            dest.IsGranted = src.RolePermissions.Any(x => x.RoleId == roleId);
            dest.TimeUpdated = src.TimeUpdated;

            dest.RoleId = roleId;

            return dest;
        }

        public static Permission Map(PermissionViewModel src, Permission dest)
        {
            dest.Description = src.Description;
            dest.Displayname = src.Displayname;
            return dest;
        }
    }
}
