﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static SettingViewModel Map(CompanySetting src, SettingViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new SettingViewModel();
            }
            dest.Name = src.Name;
            dest.Value = src.Value;
            return dest;
        }

        public static SettingViewModel Map(UserSetting src, SettingViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new SettingViewModel();
            }
            dest.Name = src.Name;
            dest.Value = src.Value;
            return dest;
        }
    }
}
