﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static ArticleCategoryViewModel Map(ArticleCategory src, ArticleCategoryViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new ArticleCategoryViewModel();
            }

            dest.Id = src.Id;
            dest.Name = src.Name;

            return dest;
        }

        public static ArticleCategory Map(ArticleCategoryViewModel src, ArticleCategory dest = null)
        {
            if (dest == null)
            {
                dest = new ArticleCategory();
            }
            
            dest.Name = src.Name;

            return dest;
        }

    }
}
