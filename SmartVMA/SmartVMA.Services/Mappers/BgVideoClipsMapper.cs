﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static BgVideoCliplsViewModel Map(BgVideoClip src, BgVideoCliplsViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new BgVideoCliplsViewModel();
            }
            dest.Id = src.Id;
            dest.Description = src.Description;
            return dest;
        }
    }
}
