﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using System.Data;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static AppointmentDetailsViewModel Map(AppointmentDetail src, AppointmentDetailsViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new AppointmentDetailsViewModel();
            }

            dest.OpDescription = src.OpDescription;
            dest.OpType = src.OpType;
            dest.OpCode = src.OpCode;
            dest.InStock = src.InStock;

            return dest;
        }
    }
}
