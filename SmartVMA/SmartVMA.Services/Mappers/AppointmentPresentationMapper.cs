﻿using System;
using System.Linq;
using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static AppointmentPresentationViewModel Map(AppointmentPresentation src, AppointmentPresentationViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new AppointmentPresentationViewModel();
            }
            var appContext = IocManager.Resolve<IAppContext>();
            var _companyRepository = IocManager.Resolve<ICompanyRepository>();
            var commpanySettings = _companyRepository.GetCompanyAndSettings(appContext.CurrentIdentity.TenantId.Value);
            var advisorsHaveDeleteParkedMenuPermission = commpanySettings.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.AdvisorsHaveDeleteParkedMenuPermission.ToString());

            var userCompanyRoleRepository = IocManager.Resolve<IUserCompanyRoleRepository>();
            var userCompanyRole = userCompanyRoleRepository.GetRole(src.AdvisorId.Value, appContext.CurrentIdentity.TenantId.Value);
            dest.AdvisorCode = userCompanyRole != null ? userCompanyRole.DmsUserNumber : string.Empty;            

            dest.AdvisorId = src.AdvisorId;

            dest.AdvisorInfo = src.User != null ? string.Format("{0} {1}, {2}", src.User.FirstName, src.User.LastName, dest.AdvisorCode) : string.Empty;
            
            dest.IsCurrentUserOwner = appContext.CurrentIdentity.UserId == src.AdvisorId;
            dest.AdvisorsHaveDeleteParkedMenuPermission = advisorsHaveDeleteParkedMenuPermission != null ? advisorsHaveDeleteParkedMenuPermission.Value.Trim() == "True" : false;
            dest.CustomerInfo = new CustomerInfoViewModel
            {
                Id = src.DealerCustomer != null ? (int?)src.DealerCustomer.Id : null,
                FirstName = src.DealerCustomer != null ? src.DealerCustomer.FirstName : string.Empty,
                LastName = src.DealerCustomer != null ? src.DealerCustomer.LastName : string.Empty,
                Email = src.DealerCustomer != null ? src.DealerCustomer.Email : string.Empty,
                CellPhone = src.DealerCustomer != null ? src.DealerCustomer.CellPhone : string.Empty,
                HomePhone = src.DealerCustomer != null ? src.DealerCustomer.HomePhone : string.Empty,
                WorkPhone = src.DealerCustomer != null ? src.DealerCustomer.WorkPhone : string.Empty,
                Address = src.DealerCustomer != null ? src.DealerCustomer.AddressLine1 : string.Empty,
                City = src.DealerCustomer != null ? src.DealerCustomer.City : string.Empty,
                Zip = src.DealerCustomer != null ? src.DealerCustomer.Zip : string.Empty,
                State = src.DealerCustomer != null ? src.DealerCustomer.CountryState != null ? src.DealerCustomer.CountryState.CountryStateShort : string.Empty : string.Empty,
                Country = src.DealerCustomer != null ? src.DealerCustomer.Country != null ? src.DealerCustomer.Country.Country1 : string.Empty : string.Empty,
                DriveLine = src.DealerVehicle != null ? src.DealerVehicle.VehicleDriveline : string.Empty,
                Engine = src.DealerVehicle != null ? src.DealerVehicle.VehicleEngine : string.Empty,
                Make = src.DealerVehicle != null ? src.DealerVehicle.VehicleMake : string.Empty,
                Model = src.DealerVehicle != null ? src.DealerVehicle.VehicleModel : string.Empty,
                Transmission = src.DealerVehicle != null ? src.DealerVehicle.VehicleTransmission : string.Empty,
                Mileage = src.Mileage,
                VIN = src.DealerVehicle != null ? src.DealerVehicle.VIN : string.Empty,
                Year = src.DealerVehicle != null ? src.DealerVehicle.VehicleYear : null,
                CompanyName = src.DealerCustomer != null ? src.DealerCustomer.Company : string.Empty
            };
            dest.Vin = src.DealerVehicle != null ? src.DealerVehicle.VIN : string.Empty;
            dest.OpCode = src.MenuOpCode;
            dest.ParkedDate = src.ParkedTime;
            dest.AcceptedDate = src.DateRoCreated;
            dest.RoNumber = src.RoNumber;
            dest.DateRoCreated = src.DateRoCreated;
            dest.Mileage = src.Mileage;
            dest.AppointmentID = src.AppointmentId;
            dest.AppointmentPresentationID = src.Id;
            dest.DealerID = src.TenantId;
            dest.InvoiceNumber = src.InvoiceNumber;
            dest.DealerCustomerID = src.DealerCustomerId;
            if (src.DealerVehicle?.Vehicle != null)
            {
                dest.CarId = src.DealerVehicle.Vehicle.CarId;
                dest.CarIdUndetermined = (src.DealerVehicle.Vehicle.CarIdUndetermined ?? true) ? (int)CarIdUndeterminedStatus.True : (int)CarIdUndeterminedStatus.False;
            }
            if (src.DealerVehicle?.CarId != null || src.DealerCustomer == null)
            {
                dest.CarId = src.DealerVehicle.CarId;
                dest.CarIdUndetermined = (int)CarIdUndeterminedStatus.False;
            }

            dest.PresentationStatus = src.PresentationStatus;
            dest.DealerVehicleId = src.DealerVehicleId;
            var iLifeService = IocManager.Resolve<IILifeService>();
            ILifeViewModel iLifeModel = src.DealerVehicle != null ? iLifeService.ButtonInfo(src.DealerVehicle.VIN) : null;
            dest.ILifeVisibility = iLifeModel != null ? iLifeModel.IsVisible : false;
            dest.ILifeColorClass = iLifeModel != null ? iLifeModel.ColorClass : Resources.Labels.ILifeGreyColour;
            dest.ILifeButtonName = iLifeModel != null ? iLifeModel.ButtonName : Resources.Labels.ILife;

            return dest;
        }

        public static AppointmentPresentation Map(SaveConfirmServiceRequestModel request, DealerVehicle dealerVehicle, int? advisorId, bool isLofMenu, int measurementUnitId, int menuLevel)
        {
            var dest = new AppointmentPresentation
            {
                AdvisorId = advisorId ?? 0,
                DealerCustomerId = request.DealerCustomerId ?? 0,
                DealerVehicle = dealerVehicle,
                Mileage = request.EnteredMileage ?? 0,
                MeasurementUnitId = measurementUnitId,
                MenuLevel = (byte)menuLevel,
                DateRoCreated = DateTime.UtcNow,
                AppointmentTime = DateTime.UtcNow,
                IsLofMenu = isLofMenu,
                InvoiceNumber = request.Invoice,
                ShopChargesAmount = request.ShopChargesAmount,
                TaxChargesAmount = request.TaxChargesAmount,
                MenuOpCode = request.MenuPackageOpCode,
                MenuMileage = request.Mileage ?? 0
            };

            if (request.IsAccepted)
            {
                dest.PresentationStatus = AppointmentPresentationStatus.MC.ToString();
            }
            else if (request.IsParked)
            {
                dest.PresentationStatus = AppointmentPresentationStatus.MK.ToString();
                dest.ParkedTime = DateTime.UtcNow;
            }
            else if (request.IsDeclined)
            {
                dest.PresentationStatus = AppointmentPresentationStatus.MD.ToString();
            }
            return dest;
        }

        public static AppointmentPresentation Map(SaveConfirmServiceRequestModel request, bool isLof, int menuLevel, AppointmentPresentation dest)
        {
            if (request.IsAccepted)
            {
                dest.PresentationStatus = AppointmentPresentationStatus.MC.ToString();
            }
            else if (request.IsParked)
            {
                dest.PresentationStatus = AppointmentPresentationStatus.MK.ToString();
                dest.ParkedTime = DateTime.UtcNow;
            }
            else if (request.IsDeclined)
            {
                dest.PresentationStatus = AppointmentPresentationStatus.MD.ToString();
            }

            dest.DateRoCreated = DateTime.UtcNow;
            dest.InvoiceNumber = request.Invoice;
            dest.Mileage = request.EnteredMileage ?? 0;
            dest.IsLofMenu = isLof;
            dest.MenuLevel = (byte)menuLevel;
            dest.MenuOpCode = request.MenuPackageOpCode;
            dest.MenuMileage = request.Mileage ?? 0;
            if(!dest.DealerCustomerId.HasValue || dest.DealerCustomerId.Value == 0)
            {
                dest.DealerCustomerId = request.DealerCustomerId;
            }
            return dest;
        }

        public static CustomerCopyPdfViewModel Map(AppointmentPresentation src, CustomerCopyPdfViewModel dest)
        {
            if (dest == null)
            {
                dest = new CustomerCopyPdfViewModel();
            }

            foreach (var item in src.AppointmentServices)
            {
                var service = new CustomerCopyPdfService
                {
                    ServiceDescription = item.OpDescription,
                    ServicePrice = item.Price.HasValue ? (double)item.Price : 0,
                    IsAdditional = item.IsAddedAfter.HasValue ? item.IsAddedAfter.Value : false,
                    IsDeclined = item.IsStriked,
                    LaborPrice = item.LaborPrice.HasValue ? (double)item.LaborPrice : 0,
                    PartsPrice = item.PartsPrice.HasValue ? (double)item.PartsPrice : 0
                };
                dest.Services.Add(service);
            }

            dest.TotalShopChargesAmount = src.ShopChargesAmount.HasValue ? src.ShopChargesAmount.Value : 0;
            dest.TotalTaxChargesAmount = src.TaxChargesAmount.HasValue ? src.TaxChargesAmount.Value : 0;

            return dest;
        }


        public static PartsCopyPdfViewModel Map(AppointmentPresentation src, PartsCopyPdfViewModel dest)
        {
            if (dest == null)
            {
                dest = new PartsCopyPdfViewModel();
            }

            dest.TotalERL = 0;
            dest.ModifiedTotalERL = 0;

            foreach (var service in src.AppointmentServices)
            {
                var serviceItem = new PartsCopyPdfService
                {
                    OpCode = service.OpCode,
                    OpDescription = service.OpDescription,
                    ServiceLaborHour = service.LaborHour ?? 0,
                    LaborPrice = service.LaborPrice ?? 0,
                    PartsPrice = service.PartsPrice ?? 0,
                    Price = service.Price ?? 0,
                    IsService = true,
                    IsDeclined = service.IsStriked,
                    IsAdditional = service.IsAddedAfter.HasValue ? service.IsAddedAfter.Value : false
                };

                if (service.AppointmentServiceParts.Count > 0)
                {
                    bool isFirstItem = true;
                    foreach (var part in service.AppointmentServiceParts)
                    {
                        if (isFirstItem)
                        {
                            serviceItem.PartName = part.PartNo;
                            serviceItem.Quantity = part.Quantity;
                            dest.Services.Add(serviceItem);
                            isFirstItem = false;
                        }
                        else
                        {
                            var partItem = new PartsCopyPdfService
                            {
                                PartName = part.PartNo,
                                Quantity = part.Quantity,
                                IsService = false,
                                IsDeclined = service.IsStriked,
                                IsAdditional = service.IsAddedAfter.HasValue ? service.IsAddedAfter.Value : false
                            };

                            dest.Services.Add(partItem);
                        }
                    }
                }
                else
                {
                    dest.Services.Add(serviceItem);
                }
            }

            dest.TotalHours = dest.Services.Where(x => x.IsService && !x.IsAdditional).Select(x => x.ServiceLaborHour).Sum();
            dest.ModfiedTotalHours = dest.Services.Where(x => x.IsService && !x.IsDeclined).Select(x => x.ServiceLaborHour).Sum();
            dest.TotalLabor = dest.Services.Where(x => x.IsService && !x.IsAdditional).Select(x => x.LaborPrice).Sum();
            dest.ModifiedTotalLabor = dest.Services.Where(x => x.IsService && !x.IsDeclined).Select(x => x.LaborPrice).Sum();
            dest.TotalParts = dest.Services.Where(x => x.IsService && !x.IsAdditional).Select(x => x.PartsPrice).Sum();
            dest.ModifiedTotalParts = dest.Services.Where(x => x.IsService && !x.IsDeclined).Select(x => x.PartsPrice).Sum();
            dest.Total = dest.Services.Where(x => x.IsService && !x.IsAdditional).Select(x => x.Price).Sum();
            dest.ModifiedTotal = dest.Services.Where(x => x.IsService && !x.IsDeclined).Select(x => x.Price).Sum();

            dest.TotalQTY = dest.Services.Where(x => !x.IsAdditional).Select(x => x.Quantity).Sum();
            dest.ModifiedTotalQTY = dest.Services.Where(x => !x.IsDeclined).Select(x => x.Quantity).Sum();

            if (dest.TotalHours != 0)
            {
                dest.TotalERL = (double)dest.TotalLabor / dest.TotalHours;
            }
            if (dest.ModfiedTotalHours != 0)
            {
                dest.ModifiedTotalERL = (double)dest.ModifiedTotalLabor / dest.ModfiedTotalHours;
            }

            return dest;
        }
    }
}
