﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static CompanyGroupViewModel Map(CompanyGroup src, CompanyGroupViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new CompanyGroupViewModel();
            }

            dest.Id = src.Id;
            dest.Name = src.Name;
            dest.Description = src.Description;
            dest.DistributorId = src.TenantId;

            if (src.Company != null)
            {
                dest.Distributor = src.Company.CompanyName;
            }

            if (src.CompanyCompanyGroups != null)
            {
                dest.DealerIds = src.CompanyCompanyGroups.Select(x => x.TenantId).ToList();
            }
            return dest;
        }

        public static CompanyGroup Map(CompanyGroupViewModel src, CompanyGroup dest)
        {
            if (dest == null)
            {
                dest = new CompanyGroup();
            }

            dest.Name = src.Name;
            dest.Description = src.Description;
            dest.TenantId = src.DistributorId;

            return dest;
        }

        public static IEnumerable<CompanyCompanyGroup> Map(CompanyGroupViewModel src)
        {
            var result = new List<CompanyCompanyGroup>();
            foreach (var item in src.DealerIds)
            {
                result.Add(new CompanyCompanyGroup { CompanyGroupId = src.Id, TenantId = item });
            }
            return result;
        }
    }
}
