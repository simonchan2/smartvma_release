﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using System.Collections.Generic;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static ViewPointViewModel Map(ViewPoint src, ViewPointViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new ViewPointViewModel();
            }
            dest.Id = src.Id;
            dest.Name = src.Name;
            dest.Url = src.Url;
            dest.Position = src.Position;
            return dest;
        }
    }
}