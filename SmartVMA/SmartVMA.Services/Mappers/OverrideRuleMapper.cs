﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.OutputModels;
using SmartVMA.Infrastructure.Extensions;
using SmartVMA.Resources;
using System;
using System.Collections.Generic;
using System.Text;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static ConfigurationOverrideRulesViewModel Map(OverrideRule src, ConfigurationOverrideRulesViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new ConfigurationOverrideRulesViewModel();
            }

            dest.Id = src.Id;
            dest.RuleName = src.Name;
            dest.LaborHour = src.LaborHourAmount != null ? string.Format("hrs {0:n2}", src.LaborHourAmount) : (src.LaborHourPercentage != null ? string.Format("{0} % ", src.LaborHourPercentage) : string.Empty);
            dest.LaborPrice = src.LaborPriceAmount != null ? string.Format("$ {0:n2}", src.LaborPriceAmount) : (src.LaborPricePercentage != null ? string.Format("{0} % ", src.LaborPricePercentage) : string.Empty);
            dest.PartPrice = src.PartPriceAmount != null ? string.Format("$ {0:n2}", src.PartPriceAmount) : (src.PartPricePercentage != null ? string.Format("{0} % ", src.PartPricePercentage) : string.Empty);
            dest.OpCode = src.OpCodeOverride;
            dest.PartNumber = string.IsNullOrEmpty(src.PartNoOverride) ? string.Empty : src.PartNoOverride;

            return dest;
        }

        public static ConfigurationOverrideRulesConflictsViewModel Map(OverrideRuleConflictsModel src, ConfigurationOverrideRulesConflictsViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new ConfigurationOverrideRulesConflictsViewModel();
            }

            dest.RuleId = src.RuleId;
            dest.RuleName = src.RuleName;
            dest.ConflictingServices = src.ConflictingServices;
            dest.ConflictingTypes = Mapper.Map(src, new List<ConfigurationOverrideRulesConflictsByTypeViewModel>());
            foreach (var description in src.ServiceDescriptions)
            {
                dest.ServiceDescriptions.Add(description.FullDescription);
            }

            return dest;
        }

        private static List<ConfigurationOverrideRulesConflictsByTypeViewModel> Map(OverrideRuleConflictsModel src, List<ConfigurationOverrideRulesConflictsByTypeViewModel> dest = null)
        {
            if (dest == null)
            {
                dest = new List<ConfigurationOverrideRulesConflictsByTypeViewModel>();
            }
            dest.AddIfNotNull(CreateType(OverrideRuleTypes.LaborHour, src.LaborHourAmount, src.NewLaborHourAmount, src.LaborHourPercentage, src.NewLaborHourPercentage));
            dest.AddIfNotNull(CreateType(OverrideRuleTypes.LaborPrice, src.LaborPriceAmount, src.NewLaborPriceAmount, src.LaborPricePercentage, src.NewLaborPricePercentage));
            dest.AddIfNotNull(CreateType(OverrideRuleTypes.PartPrice, src.PartPriceAmount, src.NewPartPriceAmount, src.PartPricePercentage, src.NewPartPricePercentage));
            dest.AddIfNotNull(CreateType(OverrideRuleTypes.OpCode, null, null, null, null, src.OpCodeOverride, src.NewOpCodeOverride));
            dest.AddIfNotNull(CreateType(OverrideRuleTypes.PartNumber, null, null, null, null, src.PartNoOverride, src.NewPartNoOverride));

            return dest;
        }

        private static ConfigurationOverrideRulesConflictsByTypeViewModel CreateType(OverrideRuleTypes ruleType, decimal? existingAmount, decimal? newAmount, int? existingPercentage, int? newPercentage, string existingInputValue = null, string newInputValue = null)
        {
            if (existingAmount != null || existingPercentage != null || existingInputValue != null)
            {
                ConfigurationOverrideRulesConflictsByTypeViewModel conflicType = new ConfigurationOverrideRulesConflictsByTypeViewModel();
                conflicType.OverrideType = Labels.ResourceManager.GetString(ruleType.ToString());
                conflicType = SetValues(conflicType, false, existingAmount, existingPercentage, existingInputValue);
                conflicType = SetValues(conflicType, true, newAmount, newPercentage, newInputValue);
                return conflicType;
            }
            return null;            
        }

        private static ConfigurationOverrideRulesConflictsByTypeViewModel SetValues(ConfigurationOverrideRulesConflictsByTypeViewModel conflicType, bool isNewValue, decimal? amount, int? percentage, string intupValue = null)
        {
            if(conflicType == null)
            {
                return new ConfigurationOverrideRulesConflictsByTypeViewModel();
            }
            string currencySymbol = conflicType.OverrideType == Labels.ResourceManager.GetString(OverrideRuleTypes.LaborHour.ToString()) ? "hrs" : "$";
            string percentSymbol = "%";
            string emptySpace = " ";
            string val = string.Empty;
            if (amount != null)
            {
                val = currencySymbol + emptySpace + String.Format("{0:0.00}", amount);
                if (isNewValue)
                {
                    conflicType.NewValue = val;
                }
                else
                {
                    conflicType.ExistingValue = val;
                }
            }
            else if (percentage != null)
            {
                val = percentage + emptySpace + percentSymbol;
                if (isNewValue)
                {
                    conflicType.NewValue = val;
                }
                else
                {
                    conflicType.ExistingValue = val;
                }
            }
            else if (intupValue != null)
            {
                if (isNewValue)
                {
                    conflicType.NewValue = intupValue;
                }
                else
                {
                    conflicType.ExistingValue = intupValue;
                }
            }
            return conflicType;
        }
    }
}
