﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using System.Collections.Generic;
using System.Data;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static AppointmentViewModel Map(Appointment src, AppointmentViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new AppointmentViewModel();
            }
            

            dest.AppointmentDetailsList = new List<AppointmentDetailsViewModel>();
            foreach(var item in src.AppointmentDetails)
            {
                dest.AppointmentDetailsList.Add(Map(item, new AppointmentDetailsViewModel()));
            }

            dest.Mileage = src.Mileage.HasValue ? src.Mileage.Value : 0;
            dest.InvoiceNumber = src.InvoiceNumber;
            dest.AppointmentTime = src.AppointmentTime;
            dest.VIN = src.VIN;
            dest.DealerCustomerId = src.DealerCustomerId;

            return dest;
        }
    }
}
