﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static DamageIndicationTypeViewModel Map(DamageIndicationType src, DamageIndicationTypeViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new DamageIndicationTypeViewModel();
            }
            dest.Id = src.Id;
            dest.Url = src.Url;
            dest.Name = src.Name;
            dest.Position = src.Position;
            dest.IsComment = src.IsComment;
            return dest;
        }
    }
}