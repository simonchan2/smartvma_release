﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static ArticleViewModel Map(Article src, ArticleViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new ArticleViewModel();
            }

            dest.Id = src.Id;
            dest.Title = src.Title;
            dest.Description = src.Description;
            foreach (var item in src.ArticleArticleCategories)
            {
                dest.CategoryIds.Add(item.ArticleCategory.Id);
            }
            foreach (var item in src.ArticleArticleCategories)
            {
                dest.CategoryNames.Add(item.ArticleCategory.Name);
            }
            dest.IsActive = src.IsActive;
            dest.TimeDeleted = src.TimeDeleted;

            return dest;
        }

        public static Article Map(ArticleViewModel src, Article dest = null)
        {
            if (dest == null)
            {
                dest = new Article();
            }
            
            dest.Title = src.Title;
            dest.Description = src.Description;
            dest.IsActive = src.IsActive;
            dest.TimeDeleted = src.TimeDeleted;
            
            return dest;
        }

    }
}
