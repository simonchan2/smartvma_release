﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static FluidViscosityViewModel Map(FluidViscosity src, FluidViscosityViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new FluidViscosityViewModel();
            }

            dest.Id = src.Id;
            dest.Viscosity = src.Viscosity;
            dest.InDealerOilViscosityList = src.InDealerOilViscosityList;

            return dest;
        }
    }
}
