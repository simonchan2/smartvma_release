﻿using SmartVMA.Core;
using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static UserViewModel Map(User src, UserViewModel dest = null, bool getCompanyNameAsRawHtml = false)
        {
            if (dest == null)
            {
                dest = new UserViewModel();
            }

            dest.Id = src.Id;
            dest.UserStatusCodeId = src.UserStatusCodeId;
            dest.Email = src.Email;
            dest.LastName = src.LastName;
            dest.FirstName = src.FirstName;
            dest.MiddleName = src.MiddleName;
            dest.FullName = $"{dest.FirstName} {dest.LastName}";
            dest.Title = src.Title;
            dest.PhoneNumber = src.PhoneNumber;
            dest.IsLocked = src.LoginAttempts >= StaticSettings.MaximumAllowedLoginAttempts;
            var appContext = IocManager.Resolve<IAppContext>();
            var companyRole = src.UserCompanyRoles.FirstOrDefault();
            if (companyRole != null)
            {
                dest.Tenants = src.UserCompanyRoles.Select(x => x.TenantId).ToList();
                dest.RoleId = companyRole.RoleId;
                if (companyRole.Company != null)
                {
                    var companies = src.UserCompanyRoles.Select(x => x.Company).Select(item => GetCompanyName(item, appContext.CurrentIdentity.RoleId == (int)UserRoles.SystemAdministrator ? true : (appContext.CurrentIdentity.TenantId == item.Id))).ToList();
                    dest.CompanyName = string.Join(", ", companies);
                }
                if (companyRole.Role != null)
                {
                    dest.RoleName = companyRole.Role.Description;
                }
            }
            foreach(var company in src.UserCompanyRoles)
            {
                var userRole = new UserViewModel.UserCompanyRoles();
                userRole.CompanyName = company.Company.CompanyName;
                userRole.TenantId = company.TenantId;
                userRole.DmsUserNumber = company.DmsUserNumber;
                dest.UserRoles.Add(userRole);
                
            }
            var currentTenantId = appContext.CurrentIdentity.TenantId;

            if (src.UserCompanyRoles.Where(x => x.TenantId == currentTenantId).Count() > 0)
            {
                var currentCompanyRole = src.UserCompanyRoles.Where(x => x.TenantId == currentTenantId).First();
                dest.AdvisorCode = currentCompanyRole.DmsUserNumber;
            }
            //dest.AdvisorCode = src.AdvisorCode;
            return dest;
        }

        public static User Map(UserViewModel src, User dest = null)
        {
            if (dest == null)
            {
                dest = new User();
            }

            dest.UserStatusCodeId = src.UserStatusCodeId;
            dest.Login = src.Email;
            dest.Email = src.Email;
            dest.LastName = src.LastName;
            dest.FirstName = src.FirstName;
            dest.MiddleName = src.MiddleName;
            dest.Title = src.Title;
            dest.PhoneNumber = src.PhoneNumber;
            dest.LoginAttempts = src.IsLocked ? StaticSettings.MaximumAllowedLoginAttempts : 0;
            dest.Configuration = "";
            dest.Comment = "";
            dest.AdvisorCode = src.AdvisorCode;

            return dest;
        }

        public static AccountManagementViewModel Map(User src, string roleName, AccountManagementViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new AccountManagementViewModel();
            }

            dest.Id = src.Id;
            dest.Email = src.Email;
            dest.UserName = $"{src.FirstName} {src.LastName}";
            dest.RoleName = roleName;

            var languageSetting = src.UserSettings.FirstOrDefault(x => x.Name == UserSettingsNames.LanguageId.ToString());
            if (!string.IsNullOrEmpty(languageSetting?.Value))
            {
                dest.LanguageId = int.Parse(languageSetting.Value);
            }

            var isParkedSetting = src.UserSettings.FirstOrDefault(x => x.Name == UserSettingsNames.IsParked.ToString());
            dest.IsParked = string.IsNullOrEmpty(isParkedSetting?.Value) || bool.Parse(isParkedSetting.Value);

            var userImageDownloadUrl = src.UserSettings.FirstOrDefault(x => x.Name == UserSettingsNames.UserImageDownloadUrl.ToString());
            if (!string.IsNullOrEmpty(userImageDownloadUrl?.Value))
            {
                dest.UserImageDownloadUrl = userImageDownloadUrl.Value;
            }

            var appContext = IocManager.Resolve<IAppContext>();

            var userImageFileName = src.UserSettings.FirstOrDefault(x => x.Name == UserSettingsNames.UserImageFileName.ToString());
            if (!string.IsNullOrEmpty(userImageFileName?.Value))
            {
                dest.UserImageOriginalFileName = userImageFileName.Value;
                dest.UserImageDownloadUrl = appContext.MapUrl($@"{appContext.ImagesFolder}{userImageFileName.Value}");
            }

            var defaultTenantId = src.UserSettings.FirstOrDefault(x => x.Name == UserSettingsNames.DefaultTenantId.ToString());
            if (!string.IsNullOrEmpty(defaultTenantId?.Value))
            {
                dest.TenantId = int.Parse(defaultTenantId.Value);
            }
            else
            {
                dest.TenantId = appContext.CurrentIdentity.TenantId ?? 0;
            }

            return dest;
        }

        public static IEnumerable<UserSetting> Map(AccountManagementViewModel src)
        {
            var settings = new List<UserSetting>();

            settings.Add(new UserSetting { Name = UserSettingsNames.IsParked.ToString(), Value = src.IsParked.ToString() });
            settings.Add(new UserSetting { Name = UserSettingsNames.LanguageId.ToString(), Value = src.LanguageId.ToString() });
            settings.Add(new UserSetting { Name = UserSettingsNames.DefaultTenantId.ToString(), Value = src.TenantId.ToString() });
            if (!string.IsNullOrEmpty(src.UserImageFileName))
            {
                settings.Add(new UserSetting { Name = UserSettingsNames.UserImageFileName.ToString(), Value = src.UserImageFileName });
                settings.Add(new UserSetting { Name = UserSettingsNames.UserImageOriginalFileName.ToString(), Value = src.UserImageOriginalFileName });
            }

            return settings;
        }
    }
}