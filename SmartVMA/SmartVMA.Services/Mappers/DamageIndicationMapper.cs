﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using System;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static DamageIndicationViewModel Map(DamageIndication src, DamageIndicationViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new DamageIndicationViewModel();
            }
            dest.Id = src.Id;
            dest.TypeId = src.TypeId;
            dest.Comment = src.Comment;
            dest.OffsetLeft = src.OffsetLeft;
            dest.OffsetTop = src.OffsetTop;
            dest.ViewPointId = src.ViewPointId;
            dest.InspectionId = src.InspectionId;
            return dest;
        }

        public static DamageIndication Map(DamageIndicationViewModel src, DamageIndication dest = null)
        {
            if (dest == null)
            {
                dest = new DamageIndication();
            }
            dest.TypeId = src.TypeId;
            dest.Comment = src.Comment;
            dest.OffsetLeft = src.OffsetLeft;
            dest.OffsetTop = src.OffsetTop;
            dest.ViewPointId = src.ViewPointId;
            dest.InspectionId = src.InspectionId;
            return dest;
        }
    }
}