﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Resources;
using System;
using System.Collections;
using System.Globalization;
using System.Linq;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static OemPartViewModel Map(OemPart src, OemPartViewModel dest = null, double quantity = 0)
        {
            if (dest == null)
            {
                dest = new OemPartViewModel();
            }

            dest.Id = src.Id;
            dest.CarId = src.CarId;
            dest.FluidType = src.FluidType;
            dest.FluidViscosity = src.FluidViscosity;
            dest.LaborHour = src.LaborHour;
            dest.LaborSkillLevel = src.LaborSkillLevel;
            dest.OemComponentId = src.OemComponentId;
            dest.OemPartName = src.OemPartName;
            var hasPartNumber = src.OemPartNumbers.Where(x => x.CountryId == 1).Count() > 0;
            dest.OemPartNo = hasPartNumber ? src.OemPartNumbers.Where(x => x.CountryId == 1).Select(x => x.OemPartNo).FirstOrDefault() : string.Empty;
            dest.OemPartNumberId = hasPartNumber ? src.OemPartNumbers.Where(x => x.CountryId == 1).Select(x => x.Id).FirstOrDefault() : (int?)null;
            dest.OemServicePartId = src.OemServiceParts.Count() > 0 ? src.OemServiceParts.Select(x => x.Id).FirstOrDefault() : (int?)null;
            dest.TimeCreated = src.TimeCreated;
            dest.TimeUpdated = src.TimeUpdated;
            dest.Unit = src.Unit;
            dest.UnitPrice = src.UnitPrice;
            dest.Quantity = quantity;
            dest.EngineOilType = src.EngineOilType;

            return dest;
        }

        public static OemPart Map(OemPartViewModel src, OemPart dest = null)
        {
            if (dest == null)
            {
                dest = new OemPart();
            }

            dest.CarId = src.CarId.Value;
            dest.FluidType = src.FluidType;
            dest.FluidViscosity = src.FluidViscosity;
            dest.LaborHour = src.LaborHour;
            dest.LaborSkillLevel = src.LaborSkillLevel;
            dest.OemComponentId = src.OemComponentId;
            dest.OemPartName = src.OemPartName;
            dest.TimeCreated = src.TimeCreated;
            dest.TimeUpdated = src.TimeUpdated;
            dest.Unit = src.Unit;
            dest.UnitPrice = src.UnitPrice;

            return dest;
        }
        public static OemPart Map(OemPartViewModel src, OemPart dest, bool fromBasicServiceView = true)
        {
            if (dest == null)
            {
                dest = new OemPart();
            }
            if (fromBasicServiceView)
            {
                dest.CarId = src.CarId ?? 0;
                dest.UnitPrice = src.UnitPrice;
                dest.FluidType = src.FluidType;
                dest.FluidViscosity = src.FluidViscosity;
                dest.OemPartName = src.OemPartName;
                dest.EngineOilType = src.EngineOilType;
            }

            return dest;
        }

        public static int? GetOilEnumInt(string name, Type t)
        {
            int? result = null;
            if (name != null)
            {
                var resxDictionary = Labels.ResourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true).OfType<DictionaryEntry>().ToArray();
                var fluidTypeName = resxDictionary.FirstOrDefault(e => e.Value.ToString() == name);
                if (fluidTypeName.Key != null)
                {
                    var fluidTypeInt = (int)Enum.Parse(t, fluidTypeName.Key.ToString());
                    result = fluidTypeInt;
                }
            }
            return result;
        }
    }
}
