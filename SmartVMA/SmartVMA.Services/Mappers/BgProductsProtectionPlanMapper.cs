﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using System;
using System.Collections.Generic;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    { 

        public static BgProductsProtectionPlan Map(BgProductViewModel src, int ProtectionPlanId, BgProductsProtectionPlan dest = null)
        {
            if (dest == null)
            {
                dest = new BgProductsProtectionPlan();
            }

            dest.Id = src.BgProductId;
            dest.BgProductId = src.BgProductId;
            dest.BgProtectionPlanId = ProtectionPlanId;

            return dest;
        }

        public static BgProductViewModel Map(BgProductsResultView src, BgProductViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new BgProductViewModel();
            }

            dest.Id = src.ProductId;
            dest.BgProductId = src.ProductId;            
            dest.PartNumber = src.BGSKU;
            dest.ProductDescription = src.ProductDescription;
            dest.SelectCategories = src.SelectedCategoryIDs;
            dest.SelectedCategoryNames = src.SelectedCategoriesNames;
            dest.LifetimeProtection = !String.IsNullOrEmpty(src.LifetimeProtection) ? "<div class= protection-plan><span class='icon-prev-serv'></span></div>" : "";
            dest.ForeverDiesel = !String.IsNullOrEmpty(src.ForeverDiesel) ? "<div class= protection-plan><span class='icon-prev-serv'></span></div>" : ""; 
            dest.EngineAssurance = !String.IsNullOrEmpty(src.EngineAssurance) ? "<div class= protection-plan><span class='icon-prev-serv'></span></div>" : ""; ;
            dest.isCheckedEngineAssurance = src.EngineAssurance == "" ? false : true;
            dest.isCheckedForeverDiesel = src.ForeverDiesel == "" ? false : true;
            dest.isCheckedLifetimeProtection = src.LifetimeProtection == "" ? false : true;
            dest.SelectedProjectPlanIDs = src.dicSelectedProjectPlanIDs;
            dest.RecordCount = src.RecordCount;
            return dest;
        }


        public static BgProductsCategory Map(BgProductViewModel src, BgProductsCategory dest)
        {
            if (dest == null)
            {
                dest = new BgProductsCategory();
            }

            dest.BgProductId = src.BgProductId;
            dest.BgProdCategoryId = src.CategoryId;

            return dest;
        }

        public static BgProduct Map(BgProductViewModel src, BgProduct dest)
        {
            if (dest == null)
            {
                dest = new BgProduct();
            }
            dest.Id = src.BgProductId;
            dest.PartNumber = src.PartNumber;
            dest.Description = src.ProductDescription;          
            return dest;
        }

        public static BgProductViewModel Map(BgProduct src, BgProductViewModel dest )
        {
            if (dest == null)
            {
                dest = new BgProductViewModel();
            }

            dest.Id = src.Id;
            dest.BgProductId = src.Id;
            dest.PartNumber = src.PartNumber;
            dest.ProductDescription = src.Description;
            return dest;
        }

        public static BgProductsCategory Map(BgProduct src, int CategoryId, BgProductsCategory dest = null)
        {
            if (dest == null)
            {
                dest = new BgProductsCategory();
            }

            dest.BgProductId = src.Id;
            dest.BgProdCategoryId = CategoryId;
            return dest;
        }

    }
}
