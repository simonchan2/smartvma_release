﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static AlertUserViewModel Map(AlertsUser src, AlertUserViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new AlertUserViewModel();
            }

            dest.Id = src.Id;
            dest.AlertId = src.AlertId;
            dest.UserId = src.UserId;
            dest.StatusId = src.StatusId;
            
            return dest;
        }

        public static AlertsUser Map(AlertUserViewModel src, AlertsUser dest = null)
        {
            if (dest == null)
            {
                dest = new AlertsUser();
            }
            
            dest.AlertId = src.AlertId;
            dest.UserId = src.UserId;
            dest.StatusId = src.StatusId;

            return dest;
        }
    }
}
