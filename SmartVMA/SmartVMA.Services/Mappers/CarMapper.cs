﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using System.Linq;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        // TODO: fill missing mapped fields
        public static DealerVehicle Map(Car src, int? dealerCustomerId, string vin, DealerVehicle dest = null, string transmission = null, string driveline = null)
        {
            if (dest == null)
            {
                dest = new DealerVehicle();
            }

            dest.DealerCustomerId = dealerCustomerId;
            dest.DealerVehicleStatusCodeId = (int)DealerVehicleStatusCodes.Active;
            dest.VIN = vin;
            dest.VehicleYear = src.VehicleYear;
            dest.VehicleMake = src.VehicleMake.VehicleMake1;
            dest.VehicleModel = src.VehicleModel.VehicleModel1;
            dest.VehicleEngine = src.VehicleEngine.VehicleEngine1;
            var vehicle = src.Vehicles.FirstOrDefault(x => x.VIN == vin);
            dest.Vehicle = vehicle;

            dest.VehicleDriveline = driveline;
            dest.VehicleTransmission = transmission;

            var carVinShort = src.CarVinShorts.FirstOrDefault(x => x.VIN == vin);
            if (carVinShort != null)
            {
                if (carVinShort.VinMaster != null)
                {
                    if (string.IsNullOrEmpty(dest.VehicleDriveline))
                    {
                        dest.VehicleDriveline = carVinShort.VinMaster.VehicleDriveLine;
                    }
                    if (string.IsNullOrEmpty(dest.VehicleTransmission))
                    {
                        dest.VehicleTransmission = carVinShort.VinMaster.VehicleTransmission;
                    }
                    dest.VehicleSteering = carVinShort.VinMaster.VehicleSteering;
                }
            }
            dest.CarId = src.Id;

            //VehicleDmsId
            //StockNo
            //VehicleColor
            //VehicleLicense
            //DeliveryDate
            //Options
            //Comment
            //VehicleExtId
            return dest;
        }

    }
}
