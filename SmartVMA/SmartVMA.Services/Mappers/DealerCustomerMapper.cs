﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static DealerCustomer Map(DealerCustomerViewModel src, DealerCustomer dest = null)
        {
            if (dest == null)
            {
                dest = new DealerCustomer();
            }

            dest.FirstName = src.FirstName;
            dest.LastName = src.LastName;
            dest.Company = src.CompanyName;
            dest.AddressLine1 = src.Address;
            dest.HomePhone = src.HomePhone;
            dest.WorkPhone = src.WorkPhone;
            dest.CellPhone = src.MobilePhone;
            dest.Email = src.Email;
            dest.LanguageId = 1;//(int)Languages.English;

            dest.FullName = string.Format("{0} {1}", dest.FirstName, dest.LastName);

            return dest;
        }

        public static DealerCustomerViewModel Map(DealerCustomer src, DealerCustomerViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new DealerCustomerViewModel();
            }

            dest.FirstName = src.FirstName;
            dest.LastName = src.LastName;
            dest.CompanyName = src.Company;
            dest.Address = src.AddressLine1;
            dest.HomePhone = src.HomePhone;
            dest.WorkPhone = src.WorkPhone;
            dest.MobilePhone = src.CellPhone;
            dest.Email = src.Email;

            return dest;
        }

        public static DealerVehicle Map(DealerCustomerViewModel src, DealerVehicle dest = null, int? dealerCustomerId = null, Car car = null, string transmission = "All", string driveline = "All")
        {
            if (dest == null)
            {
                dest = new DealerVehicle();
            }
            dest.DealerCustomerId = dealerCustomerId;
            dest.VIN = src.VIN;

            if (car != null)
            {
                dest.VehicleYear = car.VehicleYear;
                dest.VehicleMake = car.VehicleMake.VehicleMake1;
                dest.VehicleModel = car.VehicleModel.VehicleModel1;
                dest.VehicleEngine = car.VehicleEngine.VehicleEngine1;
            }
            dest.VehicleTransmission = transmission;
            dest.VehicleDriveline = driveline;
            dest.VinMasterTransmissionId = src.ConfirmServicesRequest.Transmission;
            dest.VinMasterDrivelineId = src.ConfirmServicesRequest.Driveline;
            dest.CarId = car.Id;

            return dest;
        }

        public static Vehicle Map(DealerCustomerViewModel src, Vehicle dest = null, int? vinMasterId = null)
        {
            if (dest == null)
            {
                dest = new Vehicle();
            }
            dest.VinMasterId = vinMasterId;
            dest.CarId = src.ConfirmServicesRequest.CarId;
            dest.VIN = src.VIN;
            dest.VinShort = src.VIN.Substring(0, 8) + src.VIN.Substring(9, 2);

            return dest;
        }
    }
}
