﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Enums;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static AppointmentServicePart Map(AppointmentService service, OemServicePart src, AppointmentServicePart dest = null, EngineOilConfigurationViewModel engineOilOverride = null, OemPartNumber partNumber = null)
        {
            if (dest == null)
            {
                dest = new AppointmentServicePart();
            }
            dest.AppointmentServicesId = service.Id;
            dest.OriginalPartId = src.Id;
            dest.Quantity = src.Quantity;
            dest.TenantId = service.TenantId;          
            dest.AppointmentService = service;

            if (src.OemPart != null)
            {
                dest.Unit = src.OemPart.Unit;
                dest.UnitPrice = src.OemPart.UnitPrice;
                dest.PartName = src.OemPart.OemPartName;
                dest.FluidType = src.OemPart.FluidType;
                dest.FluidViscosity = src.OemPart.FluidViscosity;
                dest.LaborSkillLevel = src.OemPart.LaborSkillLevel;
                dest.LaborHour = src.OemPart.LaborHour;
            }
            if (engineOilOverride != null)
            {
                if (engineOilOverride.UnitPrice.HasValue) dest.UnitPrice = engineOilOverride.UnitPrice.Value;
                dest.PartName = engineOilOverride.PartName;
                dest.PartNo = engineOilOverride.PartNumber;
                dest.FluidType = engineOilOverride.OilType;
                dest.FluidType = engineOilOverride.OilViscosity;

            }
            if(partNumber != null)
            {
                dest.PartNo = partNumber.OemPartNo;
            }
            dest.PartTypeId = (int)AppointmentServicePartType.OemServicePart;
            return dest;
        }

        public static AppointmentServicePart Map(AppointmentService service, AdditionalServicePart src, AppointmentServicePart dest = null)
        {
            if (dest == null)
            {
                dest = new AppointmentServicePart();
            }

            dest.AppointmentServicesId = service.Id;
            dest.OriginalPartId = (int)src.Id;
            dest.Quantity = src.Quantity;
            dest.TenantId = service.TenantId;
            dest.UnitPrice = src.UnitPrice;
            dest.AppointmentService = service;
            
            dest.Unit = src.Unit;
            dest.PartNo = src.PartNo;
            dest.PartName = src.PartName;
            dest.FluidType = src.FluidType;
            dest.FluidViscosity = src.FluidViscosity;
            //dest.LaborSkillLevel = null;
            //dest.LaborHour = null;
            dest.PartTypeId = (int)AppointmentServicePartType.AppointmentServicePart;

            return dest;
        }
    }
}
