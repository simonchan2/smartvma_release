﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static VehiclePhotoViewModel Map(VehiclePhoto src, VehiclePhotoViewModel dest = null)
        {
            var appContext = IocManager.Resolve<IAppContext>();
            if (dest == null)
            {
                dest = new VehiclePhotoViewModel();
            }
            dest.Id = src.Id;
            dest.InspectionId = src.InspectionId;
            //dest.Url = src.Url;
            dest.FileName = src.FileName;
            dest.Url = appContext.MapUrl($@"{appContext.VehiclePhotosFolder}/{appContext.CurrentIdentity.TenantId}/{dest.FileName}");
            //if (!string.IsNullOrEmpty(dest.FileName))
            //{
            //    var appContext = IocManager.Resolve<IAppContext>();
            //    dest.LogoDownloadUrl = appContext.MapUrl(dest.FileName, companyLogoSetting.TenantId);
            //}
            return dest;
        }

        public static VehiclePhoto Map(VehiclePhotoViewModel src, VehiclePhoto dest = null)
        {
            if (dest == null)
            {
                dest = new VehiclePhoto();
            }
            dest.Id = src.Id;
            dest.InspectionId = src.InspectionId;
            dest.FileName = src.FileName;
            return dest;
        }
    }
}
