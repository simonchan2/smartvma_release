﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static OemMenuMileageViewModel Map(OemMenuMileage src, OemMenuMileageViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new OemMenuMileageViewModel();
            }

            dest.Id = src.Id;
            dest.Mileage = src.Mileage;
            dest.OemMenuName = src.OemMenuName;

            return dest;
        }

        //public static OdoMeterViewModel Map(OemMenuMileage src, OdoMeterViewModel dest = null)
        //{
        //    if (dest == null)
        //    {
        //        dest = new OdoMeterViewModel();
        //    }
        
        //    dest.Mileage = src.Mileage;
        //    return dest;
        //}
    }
}
