﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static RoleViewModel Map(Role src, RoleViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new RoleViewModel();
            }

            dest.Id = src.Id;
            dest.ParentId = src.ParentId;
            dest.Role = src.Role1;
            dest.Description = src.Description;
            dest.Comment = src.Comment;
            dest.Configuration = src.Configuration;
            dest.IsSystemRole = src.IsSystemRole;
            dest.IsDeletable = !src.IsSystemRole;

            return dest;
        }

        public static Role Map(RoleViewModel src, Role dest = null)
        {
            if (dest == null)
            {
                dest = new Role();
            }

            dest.Role1 = src.Role;
            dest.Description = src.Description;
            dest.Comment = src.Comment;
            dest.Configuration = src.Configuration;
            return dest;
        }
    }
}
