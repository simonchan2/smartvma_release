﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static CustomerInfoViewModel Map(Vehicle src, CustomerInfoViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new CustomerInfoViewModel();
            }

            dest.CarId = src.Id;
            dest.Year = src.Car != null ? src.Car.VehicleYear : src.VinMaster != null ? src.VinMaster.VehicleYear : 0;
            dest.Make = src.Car != null ? src.Car.VehicleMake != null ? src.Car.VehicleMake.VehicleMake1 : string.Empty : string.Empty;
            dest.Model = src.Car != null ? src.Car.VehicleModel != null ? src.Car.VehicleModel.VehicleModel1 : string.Empty : string.Empty;
            dest.Engine = src.Car != null ? src.Car.VehicleEngine != null ? src.Car.VehicleEngine.VehicleEngine1 : string.Empty : string.Empty;
            dest.Transmission = src.VinMaster != null ? src.VinMaster.VehicleTransmission : string.Empty;
            dest.DriveLine = src.VinMaster != null ? src.VinMaster.VehicleDriveLine : string.Empty;
            dest.VIN = src.VIN;

            return dest;
        }

        public static Vehicle Map(int carId, string vin, Vehicle dest = null, int? vinMasterId = null)
        {
            if (dest == null)
            {
                dest = new Vehicle();
            }

            dest.VinMasterId = vinMasterId;
            dest.CarId = carId;
            dest.VIN = vin;
            dest.VinShort = !string.IsNullOrEmpty(vin) ? vin.Substring(0, 8) + vin.Substring(9, 2) : null;

            return dest;
        }
    }
}
