﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static EngineOilTypeViewModel Map(EngineOilType src, EngineOilTypeViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new EngineOilTypeViewModel();
            }

            dest.Id = src.Id;
            dest.OilType = src.OilType;
            dest.SortOrder = src.SortOrder;

            return dest;
        }
    }
}
