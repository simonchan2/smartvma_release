﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Resources;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {       
        public static EngineOilProduct Map(EngineOilConfigurationViewModel src, EngineOilProduct dest = null)
        {
            if (dest == null)
            {
                dest = new EngineOilProduct();
            }
            dest.Id = src.EngineOilProductId;
            dest.FluidType = src.OilType;
            dest.FluidViscosity = src.OilViscosity;
            dest.PartName = src.PartName;
            dest.PartNumber = src.PartNumber;
            if (src.UnitPrice != null) dest.UnitPrice = src.UnitPrice.Value;

            return dest;
        }

        public static EngineOilConfigurationViewModel Map(EngineOilProduct src, EngineOilConfigurationViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new EngineOilConfigurationViewModel();
            }
            dest.EngineOilProductId = src.Id;
            dest.OilType = src.FluidType;
            dest.OilViscosity = src.FluidViscosity;
            dest.PartNumber = src.PartNumber;
            dest.UnitPrice = src.UnitPrice;

            return dest;
        }
    }
}
