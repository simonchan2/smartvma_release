﻿using System;
using System.Linq;
using SmartVMA.Core;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static CustomServiceViewModel Map(AdditionalService src, CustomServiceViewModel dest)
        {
            if (dest == null)
            {
                dest = new CustomServiceViewModel();
            }

            dest.Id = src.Id;
            dest.RowId = StaticSettings.DatatableRowId + src.Id;
            dest.BgProtectionPlanId = src.BgProtectionPlanId;

            if (src.BgProtectionPlan != null)
            {
                dest.BgProtectionPlanName = src.BgProtectionPlan.ProtectionPlanName;
            }

            if (src.BgProtectionSubPlan != null)
            {
                dest.BgProtectionSubPlanName = src.BgProtectionSubPlan.SubPlanName;
            }

            dest.BgProtectionSubPlanId = src.BgProtectionSubPlanId;
            dest.CategoryId = src.BgProdCategoryId;
            if (src.BgProdCategory != null)
            {
                dest.BgProdCategoryName = src.BgProdCategory.Description;
            }

            dest.OpCode = src.OpCode;
            dest.OpDescription = src.OpDescription;
            dest.LaborHour = src.LaborHour;
            dest.LaborRate = src.LaborRate;
            dest.Comment = src.Comment;
            dest.IsLofService = src.IsLofService;
            dest.CanBeStriked = src.CanBeStriked;
            dest.IsFixLabor = src.IsFixLabor;
            //     dest.MenuLevel = src.MenuLevel;
            dest.AdvisorIncentive = src.AdvisorIncentive;
            dest.TechIncentive = src.TechIncentive;
            dest.IntervalStart = src.IntervalStart;
            dest.IntervalRepeat = src.IntervalRepeat;

            if (src.AdditionalServiceVideoClips != null)
            {
                dest.VideoId = src.AdditionalServiceVideoClips.Select(x => x.BgVideoClipId).FirstOrDefault();
            }

            if (src.AdditionalServiceParts == null)
            {
                return dest;
            }

            foreach (var item in src.AdditionalServiceParts)
            {
                var part = new CustomServicePartViewModel
                {
                    Id = item.Id,
                    BgProductId = item.BgProductId,
                    IsFluid = item.IsFluid,
                    Quantity = item.Quantity,
                    UnitPrice = item.UnitPrice,
                    PartNo = item.PartNo
                };

                if (item.BgProduct != null)
                {
                    part.BgProductName = item.BgProduct.Description;
                }

                dest.Parts.Add(part);
            }

            return dest;
        }


        public static AdditionalService Map(CustomServiceViewModel src, AdditionalService dest = null)
        {
            if (dest == null)
            {
                dest = new AdditionalService();
            }

            dest.Id = src.Id ?? 0;
            dest.OpCode = src.OpCode;
            dest.OpDescription = src.OpDescription;
            dest.LaborHour = src.LaborHour;
            dest.LaborRate = src.LaborRate;
            dest.Comment = src.Comment;
            dest.IsLofService = src.IsLofService;
            dest.CanBeStriked = src.CanBeStriked;
            dest.IsFixLabor = src.IsFixLabor;
            dest.MenuLevel = 1;
            dest.AdvisorIncentive = src.AdvisorIncentive;
            dest.TechIncentive = src.TechIncentive;
            dest.IntervalStart = src.IntervalStart;
            dest.IntervalRepeat = src.IntervalRepeat;

            dest.BgProtectionPlanId = src.BgProtectionPlanId;
            dest.BgProtectionSubPlanId = src.BgProtectionSubPlanId;
            dest.BgProdCategoryId = src.CategoryId;
            return dest;
        }

        public static AdditionalServicePart Map(CustomServicePartViewModel src, AdditionalServicePart dest = null)
        {
            if (dest == null)
            {
                dest = new AdditionalServicePart();
            }

            dest.Id = src.Id ?? 0;
            dest.IsFluid = src.IsFluid;
            dest.Quantity = src.Quantity ?? 0;
            dest.UnitPrice = src.UnitPrice ?? 0;
            dest.PartNo = src.PartNo;

            dest.BgProductId = src.BgProductId;

            // TODO
            dest.Unit = "TEST UNIT";
            dest.PartName = "TEST PART NAME";

            return dest;
        }
    }
}
