﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static InspectionViewModel Map(Inspection src, InspectionViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new InspectionViewModel();
            }

            dest.Id = src.Id;
            dest.LFRimScratch = src.LFRimScratch;
            dest.LRRimScratch = src.LRRimScratch;
            dest.RFRimScratch = src.RFRimScratch;
            dest.RRRimScratch = src.RRRimScratch;
            dest.LFTireTypeId = src.LFTireTypeId;
            dest.LRTireTypeId = src.LRTireTypeId;
            dest.RFTireTypeId = src.RFTireTypeId;
            dest.RRTireTypeId = src.RRTireTypeId;
            dest.Fuel = src.Fuel;
            dest.AppointmentPresentationId = src.AppointmentPresentationId;
            //dest.TenantId = src.TenantId;

            return dest;
        }

        public static Inspection Map(InspectionViewModel src, Inspection dest = null)
        {
            if (dest == null)
            {
                dest = new Inspection();
            }

            dest.Id = src.Id ?? 0;
            dest.LFRimScratch = src.LFRimScratch;
            dest.LRRimScratch = src.LRRimScratch;
            dest.RFRimScratch = src.RFRimScratch;
            dest.RRRimScratch = src.RRRimScratch;
            dest.LFTireTypeId = src.LFTireTypeId;
            dest.LRTireTypeId = src.LRTireTypeId;
            dest.RFTireTypeId = src.RFTireTypeId;
            dest.RRTireTypeId = src.RRTireTypeId;
            dest.Fuel = src.Fuel;
            dest.AppointmentPresentationId = src.AppointmentPresentationId;
            //dest.TenantId = 14;

            return dest;
        }
    }
}
