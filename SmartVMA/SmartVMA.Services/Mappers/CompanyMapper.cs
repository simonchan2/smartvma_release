﻿using SmartVMA.Core;
using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System.Collections.Generic;
using System.Linq;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static CompanyViewModel Map(Company src, CompanyViewModel dest = null, bool getCompanyNameAsRawHtml = false)
        {
            if (dest == null)
            {
                dest = new CompanyViewModel();
            }
            dest.Id = src.Id;
            dest.CompanyTypeId = src.CompanyTypeId;
            dest.Name = GetCompanyName(src, getCompanyNameAsRawHtml);

            // TODO: add general method for getting enum display values
            dest.CompanyType = ((CompanyTypes)src.CompanyTypeId).ToString();

            if (src.ParentCompany != null)
            {
                dest.Parent = GetCompanyName(src.ParentCompany, getCompanyNameAsRawHtml);
                if (dest.CompanyTypeId == (int)CompanyTypes.Dealer)
                {
                    dest.CompanyGroups = string.Join(", ", src.ParentCompany.CompanyGroups
                        .SelectMany(x => x.CompanyCompanyGroups)
                        .Where(x => x.TenantId == src.Id)
                        .Select(x => x.CompanyGroup.Name));
                }
            }
            return dest;
        }

        public static Company Map(ConfigurationProfileSetupViewModel src, Company dest)
        {
            if (dest == null)
            {
                dest = new Company();
            }

            dest.ParentId = src.DistributorId;
            dest.CompanyName = string.IsNullOrEmpty(src.Name) ? StaticSettings.DefaultApplicationName : src.Name;
            dest.CountryId = src.CountryId;
            dest.CountryStateId = src.StateId ?? 0;
            dest.City = src.City;
            dest.Zip = src.ZipCode;
            dest.AddressLine1 = src.Address;
            dest.AddressLine2 = src.Address2;
            dest.LanguageId = src.Language;
            dest.AccountId = src.AccountNumber;

            return dest;
        }

        public static Company Map(ConfigurationProfileSetupDealersViewModel src, Company dest)
        {
            if (dest == null)
            {
                dest = new Company();
            }

            dest.ParentId = src.DistributorId;
            dest.CompanyName = string.IsNullOrEmpty(src.Name) ? StaticSettings.DefaultApplicationName : src.Name;
            dest.CountryId = src.CountryId;
            dest.CountryStateId = src.StateId ?? 0;
            dest.City = src.City;
            dest.Zip = src.ZipCode;
            dest.AddressLine1 = src.Address;
            dest.AddressLine2 = src.Address2;
            dest.LanguageId = src.Language;

            if (!string.IsNullOrEmpty(src.AccountNumber))
            {
                dest.AccountId = src.AccountNumber;
            }



            return dest;
        }

        public static Company Map(ConfigurationProfileSetupDistributorViewModel src, Company dest)
        {
            if (dest == null)
            {
                dest = new Company();
            }

            dest.ParentId = src.DistributorId;
            dest.CompanyName = string.IsNullOrEmpty(src.Name) ? StaticSettings.DefaultApplicationName : src.Name;
            dest.CountryId = src.CountryId;
            dest.CountryStateId = src.StateId ?? 0;
            dest.City = src.City;
            dest.Zip = src.ZipCode;
            dest.AddressLine1 = src.Address;
            dest.AddressLine2 = src.Address2;
            dest.LanguageId = src.Language;

            if (!string.IsNullOrEmpty(src.AccountNumber))
            {
                dest.AccountId = src.AccountNumber;
            }

            return dest;
        }


        public static ConfigurationProfileSetupViewModel Map(Company src, ConfigurationProfileSetupViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new ConfigurationProfileSetupViewModel();
            }

            dest.AccountNumber = src.AccountId;
            dest.DistributorId = src.ParentId;
            dest.Name = src.CompanyName;
            dest.CountryId = src.CountryId;
            dest.StateId = src.CountryStateId;
            dest.City = src.City;
            dest.ZipCode = src.Zip;
            dest.Address = src.AddressLine1;
            dest.Address2 = src.AddressLine2;
            if (src.LanguageId.HasValue)
            {
                dest.Language = src.LanguageId.Value;
            }
            // Map company settings
            dest = Map(src.CompanySettings, dest);
            return dest;
        }

        public static ConfigurationProfileSetupDistributorViewModel Map(Company src, ConfigurationProfileSetupDistributorViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new ConfigurationProfileSetupDistributorViewModel();
            }

            dest.DistributorId = src.ParentId;
            dest.DistributorName = src.ParentCompany != null ? src.ParentCompany.CompanyName : string.Empty;
            dest.Name = src.CompanyName;
            dest.CountryId = src.CountryId;
            dest.StateId = src.CountryStateId;
            dest.City = src.City;
            dest.ZipCode = src.Zip;
            dest.Address = src.AddressLine1;
            dest.Address2 = src.AddressLine2;
            if (src.LanguageId.HasValue)
            {
                dest.Language = src.LanguageId.Value;
            }
            // Map company settings
            dest = Map(src.CompanySettings, dest);
            return dest;
        }

        public static ConfigurationProfileSetupDistributorViewModel MapDefaultValues(Company src, ConfigurationProfileSetupDistributorViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new ConfigurationProfileSetupDistributorViewModel();
            }

            dest.DistributorId = src.Id;
            dest.DistributorName = src.CompanyName;
            dest.CountryId = src.CountryId;
            dest.StateId = src.CountryStateId;
            dest.City = src.City;
            dest.ZipCode = src.Zip;
            dest.Address = src.AddressLine1;
            if (src.LanguageId.HasValue)
            {
                dest.Language = src.LanguageId.Value;
            }
            // Map corp company settings
            var settings = new List<CompanySetting>();

            var languageSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.Language.ToString());
            if (languageSetting != null && !string.IsNullOrEmpty(languageSetting.Value))
            {
                dest.Language = int.Parse(languageSetting.Value);
            }

            var dateFormatSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.DateFormat.ToString());
            if (!string.IsNullOrEmpty(dateFormatSetting?.Value))
            {
                dest.DateFormat = int.Parse(dateFormatSetting.Value);
            }

            var measurementUnitSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.MeasurementUnit.ToString());
            if (dateFormatSetting != null && measurementUnitSetting != null && !string.IsNullOrEmpty(dateFormatSetting.Value))
            {
                dest.MeasurementUnitId = int.Parse(measurementUnitSetting.Value);
            }

            var phoneNumberSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.PhoneNumber.ToString());
            if (phoneNumberSetting != null)
            {
                dest.PhoneNumber = phoneNumberSetting.Value;
            }

            var sessionExpirationSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.SessionExpiration.ToString());
            dest.SessionExpiration = sessionExpirationSetting != null ? int.Parse(sessionExpirationSetting.Value) : 0;

            return dest;
        }

        public static ConfigurationProfileSetupDealersViewModel Map(Company src, ConfigurationProfileSetupDealersViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new ConfigurationProfileSetupDealersViewModel();
            }

            dest.DistributorId = src.ParentId;
            dest.Name = src.CompanyName;
            dest.CountryId = src.CountryId;
            dest.StateId = src.CountryStateId;
            dest.City = src.City;
            dest.ZipCode = src.Zip;
            dest.Address = src.AddressLine1;
            dest.Address2 = src.AddressLine2;
            if (src.LanguageId.HasValue)
            {
                dest.Language = src.LanguageId.Value;
            }
            dest.AccountNumber = src.AccountId;

            // Map company settings
            dest = Map(src.CompanySettings, dest);
            return dest;
        }

        public static ConfigurationProfileSetupDealersViewModel MapDefaultValues(Company src, ConfigurationProfileSetupDealersViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new ConfigurationProfileSetupDealersViewModel();
            }

            dest.DistributorId = src.Id;
            dest.Name = src.CompanyName;
            dest.CountryId = src.CountryId;
            dest.StateId = src.CountryStateId;
            dest.City = src.City;
            dest.ZipCode = src.Zip;
            dest.Address = src.AddressLine1;
            if (src.LanguageId.HasValue)
            {
                dest.Language = src.LanguageId.Value;
            }
            // Map corp company settings
            var settings = new List<CompanySetting>();

            var languageSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.Language.ToString());
            if (languageSetting != null && !string.IsNullOrEmpty(languageSetting.Value))
            {
                dest.Language = int.Parse(languageSetting.Value);
            }

            var dateFormatSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.DateFormat.ToString());
            if (!string.IsNullOrEmpty(dateFormatSetting?.Value))
            {
                dest.DateFormat = int.Parse(dateFormatSetting.Value);
            }

            var measurementUnitSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.MeasurementUnit.ToString());
            if (dateFormatSetting != null && measurementUnitSetting != null && !string.IsNullOrEmpty(dateFormatSetting.Value))
            {
                dest.MeasurementUnitId = int.Parse(measurementUnitSetting.Value);
            }

            var phoneNumberSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.PhoneNumber.ToString());
            if (phoneNumberSetting != null)
            {
                dest.PhoneNumber = phoneNumberSetting.Value;
            }

            dest.ApplicationName = StaticSettings.DefaultApplicationName;
            var companyLogoSetting = src.CompanySettings.FirstOrDefault(x => x.Name == CompanySettingsNames.CompanyLogo.ToString());
            if (companyLogoSetting != null)
            {
                dest.LogoFileName = companyLogoSetting.Value;
                if (!string.IsNullOrEmpty(dest.LogoFileName))
                {
                    var appContext = IocManager.Resolve<IAppContext>();
                    dest.LogoDownloadUrl = appContext.MapUrl($@"{appContext.CompanyContentFolder}/{companyLogoSetting.TenantId}/{dest.LogoFileName}");
                }
            }

            return dest;
        }

        private static string GetCompanyName(Company src, bool allowHtmlMarkup)
        {
            if (src == null)
            {
                return string.Empty;
            }
            if (!allowHtmlMarkup)
            {
                return src.CompanyName;
            }

            if (src.CompanyTypeId == (int)CompanyTypes.Dealer)
            {
                return $"<a href=\"/Admin/Dealers/ProfileSetup/{src.Id}\" class=\"btn btn-link btn-company-name\">{src.CompanyName}</a>";
            }
            if (src.CompanyTypeId == (int)CompanyTypes.Distributor)
            {
                return $"<a href=\"/Admin/Distributors/ProfileSetup/{src.Id}\" class=\"btn btn-link btn-company-name\">{src.CompanyName}</a>";
            }
            return $"<a href=\"/Admin/Corporations/ProfileSetup/{src.Id}\" class=\"btn btn-link btn-company-name\">{src.CompanyName}</a>";
        }
    }
}
