﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static AppointmentService Map(AdditionalServiceViewModel src, AppointmentService dest =null )
        {
            if (dest == null)
            {
                dest = new AppointmentService();
            }

            dest.OpDescription = src.Description;
            dest.LaborHour = src.LaborHours;
            dest.Price = (decimal?) src.Price;

            return dest;
        }
    }
}
