﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Resources;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static AlertViewModel Map(Alert src, AlertViewModel dest = null)
        {
            var companyService = IocManager.Resolve<ICompanyService>();
            if (dest == null)
            {
                dest = new AlertViewModel();
            }

            dest.Id = src.Id;
            dest.Title = src.Title;
            dest.Description = src.Description;
            dest.CreatorUserId = src.CreatorUserId;
            
            if (src.CreatorUserId.HasValue) // the Alert is created by some user(Admin)
                dest.TypeName = Labels.BGAdminNotification;
            else // the Alert is not created by user, it's System Notification
                dest.TypeName = Labels.SystemNotification;
            var dateTimeFormat = companyService.GetDateTimeFormat(CompanySettingsNames.DateFormat);
            if (src.StartDate.HasValue)
            {
                dest.StartDate = src.StartDate;
                dest.StartDateForPresentation = src.StartDate.Value.ToString(dateTimeFormat) +"<br />" + src.StartDate.Value.ToString("h:mm tt");
            }
            if (src.EndDate.HasValue)
            {
                dest.EndDate = src.EndDate;
                dest.EndDateForPresentation = src.EndDate.Value.ToString(dateTimeFormat) + "<br />" + src.EndDate.Value.ToString("h:mm tt");
            }
            return dest;
        }

        public static Alert Map(AlertViewModel src, Alert dest = null)
        {
            if (dest == null)
            {
                dest = new Alert();
            }

            dest.Title = src.Title;
            dest.Description = src.Description;
            dest.CreatorUserId = src.CreatorUserId;
            dest.EndDate = src.EndDate;
            dest.StartDate = src.StartDate;

            return dest;
        }
    }
}
