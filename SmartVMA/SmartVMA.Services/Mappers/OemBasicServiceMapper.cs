﻿using HtmlAgilityPack;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using SmartVMA.Core;

namespace SmartVMA.Services.Mappers
{
    internal partial class Mapper
    {
        public static OemBasicServiceViewModel Map(OemBasicService src, OemBasicServiceViewModel dest = null)
        {
            if (dest == null)
            {
                dest = new OemBasicServiceViewModel();
            }

            dest.Id = src.Id;
            dest.CarId = src.Car.CarExtId;
            dest.CarIdFK = src.Car.Id;
            dest.IsIncomplete = src.IsIncomplete;
            dest.LaborHour = src.LaborHour;
            dest.LaborSkillLevel = src.LaborSkillLevel;
            dest.OemComponentId = src.OemComponentId;
            dest.OemServiceExceptionId = src.OemServiceExceptionId;
            dest.OpAction = src.OpAction;
            dest.ServiceDescription = src.OpDescription;
            dest.TimeCreated = src.TimeCreated;
            dest.TimeUpdated = src.TimeUpdated;
            dest.VehicleTransmission = src.VehicleTransmission;
            dest.VehicleDescription = src.Car.Description;

            if (ConfigurationManager.AppSettings["DataMaitenanceAllDataUrl"] != null)
            {
                HtmlDocument document = new HtmlDocument();
                string htmlString = src.OemServiceException != null ? src.OemServiceException.ExceptionMessage : String.Empty;
                document.LoadHtml(htmlString);
                HtmlNodeCollection collection = document.DocumentNode.SelectNodes("//a");
                var links = document.DocumentNode.SelectNodes("//a[@href]");
                if (links != null)
                {
                    foreach (HtmlNode link in links)
                    {
                        if (!string.IsNullOrEmpty(link.Attributes["href"].Value))
                        {
                            HtmlAttribute att = link.Attributes["href"];
                            if (att.Value.Contains(ConfigurationManager.AppSettings["DataMaitenanceAllDataTextToBeReplaced"]))
                            {
                                att.Value = ConfigurationManager.AppSettings["DataMaitenanceAllDataUrl"] + link.Attributes["href"].Value.Replace(ConfigurationManager.AppSettings["DataMaitenanceAllDataTextToBeReplaced"], "");
                            }
                        }
                    }
                }
                dest.ContentFromAllData = document.DocumentNode.InnerHtml;
            }
            else
            {
                dest.ContentFromAllData = src.OemServiceException != null ? src.OemServiceException.ExceptionMessage : String.Empty;
            }
            foreach (var item in src.OemServiceParts)
            {
                dest.PartsIds.Add(item.OemPartId);
                dest.OemParts.Add(Map(item.OemPart, quantity: item.Quantity));
            }

            return dest;
        }

        public static OemBasicService Map(OemBasicServiceViewModel src, OemBasicService dest = null)
        {
            if (dest == null)
            {
                dest = new OemBasicService();
            }

            dest.CarId = src.CarId;
            dest.IsIncomplete = src.IsIncomplete;
            dest.LaborHour = Convert.ToDouble(src.LaborHour);
            dest.LaborSkillLevel = src.LaborSkillLevel;
            dest.OemComponentId = src.OemComponentId;
            dest.OemServiceExceptionId = src.OemServiceExceptionId;
            //dest.OpAction = src.OpAction;
            dest.OpDescription = src.ServiceDescription;
            dest.TimeCreated = src.TimeCreated;
            dest.TimeUpdated = src.TimeUpdated;
            dest.VehicleTransmission = src.VehicleTransmission;

            return dest;
        }

        public static OemBasicService Map(OemBasicServiceViewModel src, OemBasicService dest, bool fromDataMaintenance)
        {
            if (fromDataMaintenance)
            {
                dest.LaborHour = Convert.ToDouble(src.LaborHour);
            }

            return dest;
        }

        public static OemServiceMaintenanceViewModel Map(OemBasicService src, OemServiceMaintenanceViewModel dest)
        {
            dest.Id = src.Id;
            dest.RowId = StaticSettings.DatatableRowId + src.Id;
            dest.OpCode = src.OpAction;
            dest.OpAction = src.OpAction;
            dest.Description = src.OpDescription;
            dest.LaborTime = src.LaborHour;
            dest.OemComponentId = src.OemComponentId;
            dest.IsLof = src.OemComponent.IsLOF;

            dest.Parts = src.OemServiceParts.Select(item => new PartItemViewModel
            {
                Id = item.OemPartId,
                PartNumber = (item.OemPart != null && item.OemPart.OemPartNumbers.Count() > 0) ? item.OemPart.OemPartNumbers.FirstOrDefault().OemPartNo : string.Empty,
                Quantity = item.Quantity,
                Price = item.OemPart.UnitPrice,
                IsFluid = item.OemPart.OemComponent.IsFluid
            }).ToList();

            if (!string.IsNullOrEmpty(src.MileagesKM))
            {
                dest.MileagesKm = src.MileagesKM.Split(',').Where(x => !string.IsNullOrEmpty(x)).Select(int.Parse).ToList();
            }

            if (!string.IsNullOrEmpty(src.MileagesM))
            {
                dest.MileagesM = src.MileagesM.Split(',').Where(x => !string.IsNullOrEmpty(x)).Select(int.Parse).ToList();
            }
            return dest;
        }


        public static OemBasicService Map(OemServiceMaintenanceViewModel src, int? carId = null, OemBasicService dest = null)
        {
            if (dest == null)
            {
                dest = new OemBasicService
                {
                    TimeCreated = DateTime.UtcNow
                };
            }
            dest.TimeUpdated = DateTime.UtcNow;
            dest.OpDescription = src.Description;
            dest.LaborHour = src.LaborTime ?? 0;
            dest.OemComponentId = src.OemComponentId;
            dest.NoOverwrite = true;

            if (carId.HasValue)
            {
                dest.CarId = carId.Value;
            }

            foreach (var item in src.Parts)
            {
                var part = (item.Id.HasValue ? dest.OemServiceParts.FirstOrDefault(x => x.Id == item.Id) : new OemServicePart { TimeCreated = DateTime.UtcNow }) ?? new OemServicePart { TimeCreated = DateTime.UtcNow };
                part.Quantity = item.Quantity ?? 0;
                part.TimeUpdated = DateTime.UtcNow;

                if (part.OemPart == null)
                {
                    part.OemPart = new OemPart
                    {
                        OemComponentId = src.OemComponentId,
                        CarId = dest.CarId,
                        TimeCreated = DateTime.UtcNow,
                        TimeUpdated = DateTime.UtcNow
                    };
                }
                part.OemPart.OemPartName = item.PartNumber;
                part.OemPart.UnitPrice = item.Price ?? 0;
                part.OemBasicServiceId = dest.Id;

                if (!item.Id.HasValue)
                {
                    dest.OemServiceParts.Add(part);
                }
            }

            return dest;
        }
    }
}