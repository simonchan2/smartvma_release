﻿namespace SmartVMA.Web
{
    public class LayoutPaths
    {
        public const string Default = "~/Views/Shared/_Layout.cshtml";

        public const string AnonymousForm = "~/Views/Shared/_AnonymousFormLayout.cshtml";

        public const string Anonymous= "~/Views/Shared/_AnonymousLayout.cshtml";

        public const string Modal = "~/Views/Shared/_ModalLayout.cshtml";

        public const string VideoModal = "~/Views/Shared/_VideoModalLayout.cshtml";

        public const string LargeModal = "~/Views/Shared/_LargeModalLayout.cshtml";

        public const string UserModal = "~/Views/Shared/_ModalLayoutUser.cshtml";

        public const string DeleteModal = "~/Views/Shared/_DeleteModalLayout.cshtml";

        public const string IndexLayout = "_IndexLayout.cshtml";

        public const string EditLayout = "_EditLayout.cshtml";

        public const string Pdf = "~/Views/Shared/_PdfLayout.cshtml";

        public static string GetIndexLayoutPath(string entityName = "Shared")
        {
            return string.Format("~/Views/{0}/_IndexLayout.cshtml", entityName);
        }
    }
}