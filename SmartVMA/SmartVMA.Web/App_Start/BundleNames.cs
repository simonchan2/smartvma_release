﻿namespace SmartVMA.Web
{
    public class BundleNames
    {
        public const string General = "~/bundles/general";
        public const string Tinymce = "~/bundles/tinymce";
        public const string Bootstrap = "~/bundles/bootstrapwidgets";
        public const string DataTables = "~/bundles/datatables";
        public const string Uploader = "~/bundles/uploader";
        public const string Appointments = "~/bundles/appointments";
        public const string Carousel = "~/bundles/carousel";
        public const string Inspection = "~/bundles/inspection";
        public const string DataMaintenance = "~/bundles/dataMaintenance";
        public const string GeneralStyles = "~/bundles/generalstyle";
        public const string ControlStyles = "~/bundles/controlsstyle";
        public const string DataTablesStyles = "~/Content/DataTables/css";
        public const string UploaderStyles= "~/bundles/uploaderstyles";
        public const string CarouselStyles = "~/bundles/carouselstyles";
        public const string AppointmentsStyles = "~/bundles/appointmentsstyles";
        public const string OverrideRules = "~/bundles/overrideRules";
        public const string AlertScripts = "~/bundles/alertScripts";

        public const string DefaultLayoutScripts = "~/bundles/defaultLayoutScripts";
        public const string AnonymousLayoutScripts = "~/bundles/anonymousLayoutScripts";
        public const string DefaultLayoutStyles = "~/bundles/defaultLayoutStyles";
        public const string PasswordForgot = "~/bundles/passwordForgot";
    }
}