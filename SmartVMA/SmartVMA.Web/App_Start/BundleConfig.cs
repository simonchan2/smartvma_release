﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Optimization;

namespace SmartVMA.Web
{
    public static class BundleConfig
    {
        private static void AddScriptBundles(BundleCollection bundles)
        {
            ScriptBundle general = new ScriptBundle(BundleNames.General);
            general.Include("~/scripts/jquery-2.2.3.js");
            general.Include("~/scripts/moment.js");
            general.Include("~/scripts/jquery.validate.js");
            general.Include("~/scripts/jquery.validate.unobtrusive.js");
            general.Include("~/scripts/jquery.unobtrusive-ajax.js");
            general.Include("~/scripts/custom/general.js");
            general.Include("~/scripts/custom/dataTables.js");
            general.Include("~/scripts/modernizr-2.6.2.js");
            bundles.Add(general);

            ScriptBundle tinyMce = new ScriptBundle(BundleNames.Tinymce);
            tinyMce.Include("~/scripts/tinymce/tinymce.js");
            tinyMce.Include("~/scripts/tinymce/themes/modern/theme.js");
            tinyMce.Include("~/scripts/tinymce/plugins/advlist/plugin.js");
            tinyMce.Include("~/scripts/tinymce/plugins/lists/plugin.js");
            tinyMce.Include("~/scripts/tinymce/plugins/autolink/plugin.js");
            tinyMce.Include("~/scripts/tinymce/plugins/image/plugin.js");
            tinyMce.Include("~/scripts/tinymce/plugins/link/plugin.js");
            tinyMce.Include("~/scripts/tinymce/plugins/print/plugin.js");
            tinyMce.Include("~/scripts/tinymce/plugins/preview/plugin.js");
            tinyMce.Include("~/scripts/tinymce/plugins/charmap/plugin.js");
            tinyMce.Include("~/scripts/tinymce/plugins/searchreplace/plugin.js");
            tinyMce.Include("~/scripts/tinymce/plugins/hr/plugin.js");
            tinyMce.Include("~/scripts/tinymce/plugins/anchor/plugin.js");
            tinyMce.Include("~/scripts/tinymce/plugins/pagebreak/plugin.js");
            tinyMce.Include("~/scripts/tinymce/plugins/wordcount/plugin.js");
            tinyMce.Include("~/scripts/tinymce/plugins/visualchars/plugin.js");
            tinyMce.Include("~/scripts/tinymce/plugins/fullscreen/plugin.js");
            tinyMce.Include("~/scripts/tinymce/plugins/visualblocks/plugin.js");
            tinyMce.Include("~/scripts/tinymce/plugins/code/plugin.js");
            tinyMce.Include("~/scripts/tinymce/plugins/insertdatetime/plugin.js");
            tinyMce.Include("~/scripts/tinymce/plugins/nonbreaking/plugin.js");
            tinyMce.Include("~/scripts/tinymce/plugins/media/plugin.js");
            tinyMce.Include("~/scripts/tinymce/plugins/save/plugin.js");
            tinyMce.Include("~/scripts/tinymce/plugins/table/plugin.js");
            tinyMce.Include("~/scripts/tinymce/plugins/contextmenu/plugin.js");
            tinyMce.Include("~/scripts/tinymce/plugins/directionality/plugin.js");
            tinyMce.Include("~/scripts/tinymce/plugins/template/plugin.js");
            tinyMce.Include("~/scripts/tinymce/plugins/textcolor/plugin.js");
            tinyMce.Include("~/scripts/tinymce/plugins/textpattern/plugin.js");
            tinyMce.Include("~/scripts/tinymce/plugins/paste/plugin.js");
            tinyMce.Include("~/scripts/tinymce/plugins/colorpicker/plugin.js");
            tinyMce.Include("~/scripts/tinymce/plugins/imagetools/plugin.js");
            bundles.Add(tinyMce);

            ScriptBundle bootstrap = new ScriptBundle(BundleNames.Bootstrap);
            bootstrap.Include("~/scripts/bootstrap.js");
            bootstrap.Include("~/scripts/bootstrap-multiselect.js");
            bootstrap.Include("~/scripts/bootstrap-datetimepicker.js");
            bootstrap.Include("~/scripts/jquery.mCustomScrollbar.concat.min.js");
            bootstrap.Include("~/scripts/daterangepicker.js");
            bundles.Add(bootstrap);

            ScriptBundle datatables = new ScriptBundle(BundleNames.DataTables);
            datatables.Include("~/scripts/DataTables/jquery.dataTables.js");
            datatables.Include("~/scripts/DataTables/dataTables.select.js");
            bundles.Add(datatables);

            ScriptBundle uploader = new ScriptBundle(BundleNames.Uploader);
            uploader.Include("~/scripts/uploader/jquery.ui.widget.js");
            uploader.Include("~/scripts/uploader/jquery.fileupload.js");
            uploader.Include("~/scripts/custom/uploader.js");
            bundles.Add(uploader);

            ScriptBundle appointments = new ScriptBundle(BundleNames.Appointments);
            appointments.Include("~/scripts/fullcalendar.js");
            appointments.Include("~/scripts/custom/appointments.js");
            bundles.Add(appointments);

            ScriptBundle carousel = new ScriptBundle(BundleNames.Carousel);
            appointments.Include("~/scripts/Slick/slick.js");
            appointments.Include("~/scripts/custom/carousel.js");
            bundles.Add(carousel);

            ScriptBundle ispection = new ScriptBundle(BundleNames.Inspection);
            ispection.Include("~/Scripts/custom/inspection.js");
            bundles.Add(ispection);

            ScriptBundle dataMaintanance = new ScriptBundle(BundleNames.DataMaintenance);
            dataMaintanance.Include("~/Scripts/custom/dataMaintenance.js");
            bundles.Add(dataMaintanance);

            ScriptBundle overrideRules = new ScriptBundle(BundleNames.OverrideRules);
            overrideRules.Include("~/Scripts/custom/overrideRules.js");
            bundles.Add(overrideRules);

            ScriptBundle alerts = new ScriptBundle(BundleNames.AlertScripts);
            alerts.Include("~/scripts/custom/alert.js");
            bundles.Add(alerts);

            ScriptBundle passwordForgot = new ScriptBundle(BundleNames.PasswordForgot);
            passwordForgot.Include("~/scripts/custom/passwordForgot.js");
            bundles.Add(passwordForgot);

            AddCompiledScriptBundles(bundles);
        }

        private static void AddCompiledScriptBundles(BundleCollection bundles)
        {
            BundleResolver resolver = new BundleResolver(bundles);
            List<string> defaultLayoutScriptPaths = new List<string>();
            defaultLayoutScriptPaths.AddRange(resolver.GetBundleContents(BundleNames.General).ToList());
            defaultLayoutScriptPaths.AddRange(resolver.GetBundleContents(BundleNames.Tinymce).ToList());
            defaultLayoutScriptPaths.AddRange(resolver.GetBundleContents(BundleNames.Bootstrap).ToList());
            defaultLayoutScriptPaths.AddRange(resolver.GetBundleContents(BundleNames.DataTables).ToList());
            defaultLayoutScriptPaths.AddRange(resolver.GetBundleContents(BundleNames.Uploader).ToList());
            defaultLayoutScriptPaths.AddRange(resolver.GetBundleContents(BundleNames.Appointments).ToList());
            defaultLayoutScriptPaths.AddRange(resolver.GetBundleContents(BundleNames.Carousel).ToList());
            defaultLayoutScriptPaths.AddRange(resolver.GetBundleContents(BundleNames.DataMaintenance).ToList());
            ScriptBundle defaultLayout = new ScriptBundle(BundleNames.DefaultLayoutScripts);
            foreach (string path in defaultLayoutScriptPaths)
            {
                defaultLayout.Include(path);
            }
            bundles.Add(defaultLayout);

            List<string> anonymousLayoutScriptPaths = new List<string>();
            anonymousLayoutScriptPaths.AddRange(resolver.GetBundleContents(BundleNames.General).ToList());
            anonymousLayoutScriptPaths.AddRange(resolver.GetBundleContents(BundleNames.Tinymce).ToList());
            anonymousLayoutScriptPaths.AddRange(resolver.GetBundleContents(BundleNames.Bootstrap).ToList());            
            ScriptBundle anonymousLayout = new ScriptBundle(BundleNames.AnonymousLayoutScripts);
            foreach (string path in anonymousLayoutScriptPaths)
            {
                anonymousLayout.Include(path);
            }
            bundles.Add(anonymousLayout);
        }

        private static void AddStyleBundles(BundleCollection bundles)
        {
            StyleBundle general = new StyleBundle(BundleNames.GeneralStyles);
            general.Include("~/Content/bootstrap-theme.css", new CssRewriteUrlTransform());
            general.Include("~/Content/icons.css", new CssRewriteUrlTransform());
            general.Include("~/Content/tmp2.css", new CssRewriteUrlTransform());
            general.Include("~/Content/tmp.css", new CssRewriteUrlTransform());
            general.Include("~/Content/tmp-kris.css", new CssRewriteUrlTransform());
            general.Include("~/Content/Site.css", new CssRewriteUrlTransform());
            general.Include("~/scripts/tinymce/skins/lightgray/skin.min.css", new CssRewriteUrlTransform());
            //general.Include("~/scripts/tinymce/skins/lightgray/content.min.css", new CssRewriteUrlTransform());
            bundles.Add(general);

            StyleBundle control = new StyleBundle(BundleNames.ControlStyles);
            control.Include("~/Content/bootstrap.min.css", new CssRewriteUrlTransform());
            control.Include("~/Content/bootstrap-multiselect.css", new CssRewriteUrlTransform());
            control.Include("~/Content/bootstrap-datepicker.css", new CssRewriteUrlTransform());
            control.Include("~/Content/jquery.mCustomScrollbar.css", new CssRewriteUrlTransform());
            control.Include("~/Content/daterangepicker.css", new CssRewriteUrlTransform());
            bundles.Add(control);

            StyleBundle datatables = new StyleBundle(BundleNames.DataTablesStyles);
            datatables.Include("~/Content/DataTables/css/jquery.dataTables.min.css");
            datatables.Include("~/Content/DataTables/css/select.dataTables.min.css");
            bundles.Add(datatables);

            StyleBundle uploader = new StyleBundle(BundleNames.UploaderStyles);
            uploader.Include("~/Content/uploader/jquery.fileupload.css", new CssRewriteUrlTransform());
            uploader.Include("~/Content/uploader/jquery.fileupload-ui.css", new CssRewriteUrlTransform());
            bundles.Add(uploader);

            StyleBundle carousel = new StyleBundle(BundleNames.CarouselStyles);
            carousel.Include("~/Content/Slick/slick.css", new CssRewriteUrlTransform());
            carousel.Include("~/Content/Slick/slick-theme.css", new CssRewriteUrlTransform());
            carousel.Include("~/Content/_custom/carousel.css", new CssRewriteUrlTransform());
            bundles.Add(carousel);

            StyleBundle appointments = new StyleBundle(BundleNames.AppointmentsStyles);
            appointments.Include("~/Content/fullcalendar.min.css", new CssRewriteUrlTransform());
            bundles.Add(appointments);

            AddCompiledStyleBundles(bundles);
        }

        private static void AddCompiledStyleBundles(BundleCollection bundles)
        {
            BundleResolver resolver = new BundleResolver(bundles);
            List<string> defaultLayoutStylePaths = new List<string>();
            defaultLayoutStylePaths.AddRange(resolver.GetBundleContents(BundleNames.ControlStyles).ToList());
            defaultLayoutStylePaths.AddRange(resolver.GetBundleContents(BundleNames.DataTablesStyles).ToList());
            defaultLayoutStylePaths.AddRange(resolver.GetBundleContents(BundleNames.UploaderStyles).ToList());
            defaultLayoutStylePaths.AddRange(resolver.GetBundleContents(BundleNames.CarouselStyles).ToList());
            defaultLayoutStylePaths.AddRange(resolver.GetBundleContents(BundleNames.AppointmentsStyles).ToList());
            defaultLayoutStylePaths.AddRange(resolver.GetBundleContents(BundleNames.GeneralStyles).ToList());
            StyleBundle defaultLayout = new StyleBundle(BundleNames.DefaultLayoutStyles);
            foreach (string path in defaultLayoutStylePaths)
            {
                defaultLayout.Include(path, new CssRewriteUrlTransform());
            }
            bundles.Add(defaultLayout);
        }

        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Clear();
            bundles.ResetAll();

            AddScriptBundles(bundles);
            AddStyleBundles(bundles);

#if DEBUG
            BundleTable.EnableOptimizations = false;
#else
            BundleTable.EnableOptimizations = true;
#endif
        }
    }
}