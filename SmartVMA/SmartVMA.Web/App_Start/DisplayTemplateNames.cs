﻿namespace SmartVMA.Web
{
    public class DisplayTemplateNames
    {
        public const string Boolean = "Boolean";

        public const string Currency = "Currency";

        public const string TextWithLabel = "TextWithLabel";

        public const string IconForBoolean = "IconForBoolean";

        public const string IconForBooleanPNG = "IconForBooleanPNG";

        public const string DivRowText = "DivRowText";

        public const string RowText = "RowText";

        public const string InputAndButton = "InputAndButton";

        public const string DateTime = "DateTime";
    }
}