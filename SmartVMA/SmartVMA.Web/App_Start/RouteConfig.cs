﻿using System.Web.Mvc;
using System.Web.Routing;

namespace SmartVMA.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // BotDetect requests must not be routed
            routes.IgnoreRoute("{*botdetect}", new { botdetect = @"(.*)BotDetectCaptcha\.ashx" });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "AppointmentPresentation", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "SmartVMA.Web.Areas.Dealer.Controllers" }
            ).DataTokens.Add("area", "Dealer");

            routes.MapRoute(
                "404-PageNotFound",
                "{*url}",
                new { controller = "Error", action = "PageNotFound" }
            );
        }
    }
}
