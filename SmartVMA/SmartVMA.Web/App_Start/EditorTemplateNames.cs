﻿namespace SmartVMA.Web
{
    public class EditorTemplateNames
    {
        public const string Uploader = "Uploader";
        public const string UploaderVehiclePhotos = "UploaderVehiclePhotos";
        public const string Dropdown = "Dropdown";
        public const string TableDropdown = "TableDropdown";
        public const string Tags = "Tags";
        public const string Checkbox = "Checkbox";
        public const string CustomCheckbox = "CustomCheckbox";
        public const string TableCheckbox = "TableBoolean";
        public const string RadioButton = "RadioButton";
        public const string RadioButtonQuestion = "RadioButtonQuestion";
        public const string RadioInputGroup = "RadioInputGroup";
        public const string TextArea = "TextArea";
        public const string TableTextArea = "TableTextArea";
        public const string MultiSelect = "MultiSelect";
        public const string MultiSelectLong = "MultiSelectLong";
        public const string MultiSelectString = "MultiSelectString";
        public const string Password = "Password";
        public const string TextBox = "String";
        public const string TableTextBox = "TableString";
        public const string Captcha = "Captcha";
        public const string TextBoxWithInputGroup = "TextBoxInputGroup";
        public const string RichTextEditor = "RichTextEditor";
        public const string DateRange = "DateRange";
        public const string RadioButtonGroup = "RadioButtonGroup";
        public const string Currency = "Currency";
        public const string TableBoolean = "TableBoolean";
        public const string TableDecimal = "TableDecimal";
        public const string TableInteger = "TableInteger";
        public const string TableDouble = "TableDouble";
        public const string TableMultiselect = "TableMultiselect";
    }
}