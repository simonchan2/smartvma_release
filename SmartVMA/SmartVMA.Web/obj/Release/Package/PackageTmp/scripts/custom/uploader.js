﻿smartvma.uploader = {
    init: function () {
        $('.file-upload-wrapper:not(.active)').each(function (i, element) {
            var $wrapper = $(element);
            var $element = $wrapper.find('input.fileUpload');
            var options = generalOptions.uploader;

            switch ($element.data('allowed-types')) {
                case 'video':
                    options.acceptFileTypes = /(\.|\/)(mp4|webm|3gpp|x-m4v)$/i;
                    break;
                case 'zip':
                    options.acceptFileTypes = /(\.|\/)(zip|x-zip-compressed|x-compressed|x-zip)$/i;
                    break;
                case 'image':
                    options.acceptFileTypes = /(\.|\/)(gif|jpe?g|png)$/i;
                    break;
            }

            $element.fileupload(options)
                .on('fileuploadadd', function (e, data) {
                    var acceptFileTypes = options.acceptFileTypes;
                    if (acceptFileTypes && data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
                        $wrapper.find('.upload-error-message').text('Not an accepted file type');
                        return false;
                    }
                    data.formData = { fileGuid: '', chunkId: 0 };
                    data.submit();
                })
                .on('fileuploadprogressall', function (e, data) {
                    $wrapper.find('.progress').show();
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $wrapper.find('.progress-bar').css('width', progress + '%');
                })
                .on('fileuploadchunkdone', function (e, response) {
                    response.formData.fileGuid = response.result.fileGuid;
                    response.formData.chunkId = response.result.chunkId;
                })
                .on('fileuploaddone', function (e, data) {
                    if ($wrapper.data('uploader-type') == 'Content') {
                        var isImage = data.files[0].type.toLowerCase().indexOf('image') >= 0;
                        $('label[for=Duration]').closest('div.form-group').toggleClass('hidden', !isImage);
                        $('.file-upload-wrapper[data-uploader-type="Thumbnail"]').toggle(!isImage);
                    }

                    $wrapper.find('.progress-bar').css('width', 0);
                    $wrapper.find('.progress').hide();
                    $wrapper.find('input.file-guid').val(data.result.fileGuid);
                    $wrapper.find('input.file-name').val(data.result.tempFileName);
                    $wrapper.find('input.orig-file-name').val(data.result.fileName);
                    $wrapper.find('.filename-container').html(data.result.fileName);

                    var $thumbnail = $wrapper.find('.thumbnail img');
                    if ($thumbnail) {
                        $thumbnail.attr('src', data.result.thumbnail);                        
                        $thumbnail.removeClass('hidden');
                    }
                });
            $wrapper.addClass('active');
        });
    }
};
