﻿smartvma.dataTable = {
    init: function () {
        smartvma.dataTable.selectedServices = smartvma.dataTable.selectedServices || [];
        smartvma.dataTable.allServiceIds = smartvma.dataTable.allServiceIds || [];
        $(".dataTableGrid:not(.dataTable)")
            .each(function (i, element) {
                smartvma.dataTable.self = this;
                var $table = $(element);
                var options = generalOptions.datatable;

                var url = $table.data("url");
                if (typeof (url) !== "undefined") {

                } else {
                    options.paging = false;
                }
                smartvma.dataTable.initOptions($table, element, options);
                $table.dataTable(options);
                if ($table.hasClass("table-form")) {
                    smartvma.dataTable.registerFormEvents();
                }
                var $columnHeaders = $(".hideColumn");
                if ($columnHeaders) {
                    $columnHeaders.hide();
                }
                $("div.dataTables_filter input").addClass("form-control");
            });
    },
    initOptions: function ($table, element, options) {
        if (!$table) {
            return options;
        }
        options.columnDefs = options.columnDefs || [];
        // options.dom = 'Bfrtip';
        options.rowId = "rowId";
        options.dom = 'Bfr<"custom-datatable-scroll"t><"custom-datatable-footer"ip>';
        options.processing = true;
        options.bAutoWidth = false;
        options.rowCallback = function (row, data) {
            setTimeout(function () {
                if ($.inArray(data.rowId, smartvma.dataTable.selectedServices) !== -1) {
                    $(row).find("td:first input[type=checkbox]").prop("checked", true);
                }
            },
                300);
        };
        var orderColumnIndex = $table.find('thead tr:last th:not([data-orderable="false"])').index() < 0
            ? 0
            : $table.find('thead tr:last th:not([data-orderable="false"])').index();
        options.order = [[orderColumnIndex, "asc"]];
        var enableSearch = $table.data("enable-search");
        if (enableSearch !== undefined) {
            options.searching = smartvma.controls.parseBool(enableSearch);
        }
        smartvma.dataTable.defaultMode = $table.data("default-mode");
        if ($(element).hasClass("select")) {
            options.columnDefs = [{ orderable: false, className: "select-checkbox", targets: 0 }];
            options.select = { style: "multi", selector: "td:first-child" }
        }
        // trim long company list in /Admin/Users
        var trim = $table.data("trimlonglist");
        if (typeof (trim) !== "undefined") {
            var t = trim.split(",");
            options.columnDefs.push(
                {
                    targets: parseInt(t[0]),
                    render: function (data) {
                        if (null != data) {
                            var d = data.split(", ");
                            var l = parseInt(t[1]);
                            if (d.length > l) {
                                var trimmed = d.slice(0, l).join(", ");
                                return ('<span title="' + $(data).text() + '">' + trimmed + "...<span>");
                            } else return data;
                        } else return "";
                    }
                }
            );
        }
        var url = $table.data("url");
        if (typeof (url) !== "undefined") {
            smartvma.dataTable.populateDatatable($table, url, options);
        } else {
            options.paging = false;
        }
        return options;
    },
    initTableHeaders: function ($table, options) {
        $table.find("thead tr:last th")
            .each(function (i, header) {
                var $header = $(header);
                var isColumnVisible = $header.data("visible");
                isColumnVisible = (isColumnVisible === undefined);

                options.columns.push({
                    name: $header.attr("name"),
                    data: $header.attr("name"),
                    orderable: smartvma.controls.parseBool($header.attr("data-orderable") === undefined
                        ? true
                        : $header.attr("data-orderable")),
                    searching: smartvma.controls.parseBool($header.attr("data-searching") || true),
                    width: $header.width() + "px",
                    visible: isColumnVisible
                });
            });
    },
    initAjax: function ($table, url, options) {
        url = smartvma.controls.mapUrl(url);
        options.serverSide = true;
        options.ajax = {
            url: url,
            method: "POST",
            error: smartvma.callbacks.handleAjaxError
        };

        if ($table.hasClass("table-form")) {
            options.ajax.data = function (data) {
                return $.param(data) + "&" + $table.parents("form").serialize();
            }
        }

        options.ajax.dataSrc = function (json) {
            if (json.filteredServiceIds) {
                smartvma.dataTable.allServiceIds = json.filteredServiceIds.split(",");
            }
            return json.data;
        }

        $table.on("preXhr.dt",
            function (e, settings, data) {
                var val = data.search.value;
                if (val) {
                    data.search.value = val.replace(/[<>]/g, "");
                    $("#" + settings.sTableId + "_filter input").val(data.search.value);
                }
            });
    },
    initActionButtons: function ($table, oSettings) {
        var actionsColumn = $(smartvma.dataTable.self).find("[data-actions]");
        if (actionsColumn.length) {
            var actions = actionsColumn.data("actions").buttons;
            var defaultBtnHref = "";
            $.each(oSettings.aoData,
                function (i, item) {
                    var actionColumn = $table.find("tbody tr").eq(item.idx).find("td:last");
                    $.each(actions,
                        function (j, btn) {
                            defaultBtnHref = btn.href;
                            btn.href = btn.href.interpolate(item._aData);
                            if (btn.visibility.interpolate(item._aData) === "true") {
                                $("<a>", btn).appendTo(actionColumn);
                            }
                            btn.href = defaultBtnHref;
                        });
                });
        }
    },
    initCheckboxes: function ($table, oSettings) {
        $.each(oSettings.aoData,
            function (i, item) {
                checkboxes = $(item.nTr).find(".custom-checkbox");
                $.each(checkboxes,
                    function (j, btn) {
                        var defaultName = btn.name;
                        var defaultValue = btn.value;
                        btn.name = btn.name.interpolate(item._aData);
                        btn.value = btn.value.interpolate(item._aData);
                        var dataValue = false;
                        var sourcename = btn.dataset.sourcename;
                        var prefixname = btn.dataset.prefixname;
                        var prefixindex = btn.dataset.prefixindex;
                        if (prefixname != undefined && prefixname != '' && item._aData[prefixname].length > 0) {
                                dataValue = item._aData[prefixname][prefixindex][sourcename];
                        }
                        else {
                            dataValue = item._aData[sourcename];
                        }
                        if (dataValue) {
                            $(btn).prop("checked", true);
                        }
                        else {
                            $(btn).prop("checked", false);
                        }
                    });
            });
    },
    initInlineTemplates: function (oSettings) {
        if (!oSettings.aoData.length) {
            return false;
        }
        var templates = $(oSettings.nTHead).find('tr[data-role="template"] th');
        if (!templates.length) {
            return false;
        }
        var t = {
            columns: oSettings.aoColumns,
            items: oSettings.aoData,
            columnsCount: oSettings.aoColumns.length
        };


        var setTableCell = function (cellIndex, tableCell) {
            var $tableCell = $(tableCell);
            var value = $tableCell.text();
            var rowIndex = Math.floor(cellIndex / t.columnsCount);
            var columnIndex = cellIndex % t.columnsCount;

            $tableCell.html($(templates[columnIndex]).clone().html());

            var cellValues = [value];
            if (t.columns[columnIndex].name.indexOf("[") >= 0) {
                cellValues = value.split(",");
            }

            var setHtmlForArrayItems = function (idx, value) {
                var $wrapper = $tableCell.find(".input-wrapper:first").clone();
                var $input = $wrapper.find("input:first:not(.id-field), textarea, select");

                var inputName = "items[" +
                    rowIndex +
                    "]." +
                    t.columns[columnIndex].name.replace(",", idx);
                $input.attr("id", inputName.replace(/[\[\]\.]+/g, "_"))
                    .attr("name", inputName)
                    .attr("data-prev-val", value);

                $wrapper.find("span.field-validation-valid")
                    .attr("data-valmsg-for", $input.attr("id"));
                $tableCell.find(".edit-view").append($wrapper);

                if ($input.is("select")) {
                    $input.removeClass("initialized");
                    $tableCell.find(".input-container").append($wrapper);
                    if ($input.hasClass("singleoption")) {
                        $input.val(value);
                        $tableCell.find(".read-view").append('<span class="table-line" title="' + $input.find("option:selected").text() + '">' + $input.find("option:selected").text() + "</span>");
                    } else {
                        if (value) {
                            $input.val(value.split(","));
                        }

                        $input.find("option:selected").each(function (j, option) {
                            $tableCell.find(".read-view").append('<span class="table-line" title="' + $(option).text() + '">' + $(option).text() + "</span>");
                        });
                    }
                } else if ($input.is(":checkbox")) {
                    if (smartvma.controls.parseBool(value)) {
                        $input.prop("checked", true);
                    }
                    $input.siblings("label").attr("for", inputName.replace(/[\[\]\.]+/g, "_"));
                    $input.siblings('[type="hidden"]').attr("name", inputName);

                    var $readModeInputWrapper = $input.parents(".input-container").clone();
                    $readModeInputWrapper.find("input:not(.not-disabable)").attr("disabled", "disabled");
                    var updateAttributesFunc = function (j, attr) {
                        if (attr) {
                            return "read_mode_" + attr;
                        }
                        return "";
                    };

                    $readModeInputWrapper.find("input").attr("name", updateAttributesFunc);
                    $readModeInputWrapper.find("input,label").attr("id", updateAttributesFunc);
                    $readModeInputWrapper.find("label").attr("for", updateAttributesFunc);

                    $readModeInputWrapper.find('span,input[type="hidden"]').remove();
                    $tableCell.find(".read-view").append($readModeInputWrapper.html().replace(/^(?=\n)$|^\s*|\s*$|\n\n+/gm, "").trim());
                } else {
                    $tableCell.find(".read-view").append('<span class="table-line" title="' + value + '">' + value + "</span>");
                    if ($input.is("textarea")) {
                        $input.text(value);
                    } else {
                        $input.val(value);
                    }
                }

                var $idInput = $tableCell.find("input.id-field:first").clone();
                if ($input.length > 0 && $idInput.length > 0) {
                    var idInputName = $input.attr("name").replace(/(\.)[^\.]+$/, ".id");
                    $idInput.attr("name", idInputName)
                        .attr("id", idInputName.replace(/[\[\]\.]+/g, "_"));
                    try {
                        $idInput.val(eval("t." + $idInput.attr("name").replace(/\]/, "]._aData")));
                    } catch (e) {
                    }
                    $tableCell.append($idInput);
                }
            };
            $.each(cellValues, setHtmlForArrayItems);
            $tableCell.find(".input-wrapper:first,input.id-field:first").remove();
        };
        $(oSettings.nTBody).find("tr td").each(setTableCell);
        return true;
    },
    hideColumns: function (oSettings) {
        var hideColumn = $(".hideColumn");
        if (hideColumn.length) {
            var index = $(hideColumn[0]).index();
            $(".dataTable tbody tr td:nth-child(" + index + ")").hide();
        }
    },
    initDrawCallback: function ($table, options) {
        options.fnDrawCallback = function (oSettings) {
            smartvma.dataTable.initActionButtons($table, oSettings);
            smartvma.dataTable.initInlineTemplates(oSettings);
            smartvma.dataTable.initCheckboxes($table, oSettings);
            smartvma.dataTable.hideColumns(oSettings);
            smartvma.multiselect.init();
            $table.find('[id$="bgProductId"]').trigger("change");
            smartvma.controls.hideLoader();
        }
    },
    populateDatatable: function ($table, url, options) {
        options.columns = [];
        smartvma.dataTable.initTableHeaders($table, options);
        smartvma.dataTable.initAjax($table, url, options);
        smartvma.dataTable.initDrawCallback($table, options);

    },
    updateSelectAll: function () {
        $("#selectAllDtRows").prop("checked", smartvma.dataTable.selectedServices.length === smartvma.dataTable.allServiceIds.length);
    },
    markAsChanged: function ($element) {
        $element.parents("tr").addClass("hasChanged");
    },
    uncheckAndDisableRowSelectors: function () {
        var $rowSelectors = $("tbody .custom-checkbox.is-row-selector");
        var $selectAllSelector = $("#selectAllDtRows");
        if ($rowSelectors.length) {
            $rowSelectors.prop("checked", false);
            smartvma.dataTable.selectedServices = [];
            smartvma.dataTable.updateSelectAll();
            if ($selectAllSelector.length) {
                $("#selectAllDtRows").attr("disabled", "disabled");
            }
        }
    },
    disableOrderableMode: function () {

        //var oTable = $('.dataTableGrid').dataTable();

        //// get settings object after table is initialized
        //var oSettings = oTable.fnSettings();
        //oSettings.ordering = false;
        //oSettings.paging = true;

        //$('.dataTableGrid').DataTable().destroy();
        //smartvma.datatable.initOptions($('.dataTableGrid'), '.dataTableGrid', oSettings);
        //$('.dataTableGrid').dataTable(oSettings);

        //$(".dataTableGrid").each(function (i, element) {
        //    smartvma.dataTable.self = this;
        //    var $table = $(element);
        //    var dataTable = $table.DataTable();
        //    dataTable.destroy();
        //    var options = generalOptions.datatable;
        //    var url = $table.data("url");
        //    if (typeof (url) !== "undefined") {

        //    } else {
        //        options.paging = false;
        //    }
        //    smartvma.dataTable.initOptions($table, element, options);
        //    options.columnDefs = [{ orderable: false, targets: 0 }];
        //    $(dataTable).dataTable(options);
        //    if ($table.hasClass("table-form")) {
        //        smartvma.dataTable.registerFormEvents();
        //    }
        //    var $columnHeaders = $(".hideColumn");
        //    if ($columnHeaders) {
        //        $columnHeaders.hide();
        //    }
        //    $("div.dataTables_filter input").addClass("form-control");
        //});
    },
    enableRowSelectors: function () {
        var $selectAllSelector = $("#selectAllDtRows");
        if ($selectAllSelector.length) {
            $("#selectAllDtRows").removeAttr("disabled");
        }
    },
    registerFormEvents: function () {
        var $tableWrapper = $(".custom-datatable-holder");
        var $table = $tableWrapper.find("table:last");
        var updateButtonStatus = function (isEditMode) {
            if (isEditMode) {
                $tableWrapper.find(".edit-table, .delete-table").addClass("custom-disabled");
                $tableWrapper.find(".save-table, .cancel-table").removeClass("custom-disabled");
            } else {
                $tableWrapper.find(".cancel-table, .save-table").addClass("custom-disabled");
                $tableWrapper.find(".edit-table, .delete-table").removeClass("custom-disabled");
            }
        };
        var incrementLastIndex = function (v) {
            return v.replace(/[0-9]+(?!.*[0-9])/, function (match) {
                return parseInt(match, 10) + 1;
            });
        };


        $(document).on("click", ".table-search", function (event) {
            event.preventDefault();
            $(".dataTable").DataTable().page("first");
            var elem = $(this).attr("data-validateelements");
            if (!elem || $(elem).valid()) {
                smartvma.controls.showLoader();
                $table.dataTable()._fnDraw();
                return true;
            }
            return false;
        });

        $(document).on("click", ".table-reset", function (event) {
            event.preventDefault();
            smartvma.controls.showLoader();
            window.location.reload();
        });

        /* EDIT */
        $tableWrapper.on("click", ".edit-table:not(.custom-disabled)", function () {
            $table.addClass("edit-mode");
            $tableWrapper.addClass("edit-mode-wrapper");
            updateButtonStatus(true);
            smartvma.dataTable.uncheckAndDisableRowSelectors();
            smartvma.dataTable.disableOrderableMode();
        });

        /* CANCEL */
        $tableWrapper.on("click", ".cancel-table:not(.custom-disabled)", function (e) {
            e.stopPropagation();
            $table.removeClass("edit-mode");
            $tableWrapper.removeClass("edit-mode-wrapper");
            $table.find("tr").removeClass("hasChanged");
            $table.find(".new-row,.new-cell-input").remove();
            $table.find(".marked-for-deletion").removeClass("marked-for-deletion");
            //$('table tr:not(.hasChanged):not(.new-row)').find('input, textarea, select').removeAttr('disabled');

            updateButtonStatus(false);

            $table.find('.edit-view input:not(.custom-checkbox[type="hidden"]):not(.id-field), .edit-view textarea, .edit-view select').each(function (idx, input) {
                var $input = $(input);
                var previousValue = $input.data("prev-val");
                if ($input.is(":checkbox")) {
                    $input.prop("checked", smartvma.controls.parseBool(previousValue));
                } else {
                    $input.val(previousValue);
                }
            });
            smartvma.dataTable.enableRowSelectors();

        });

        /* CHECK ROW */
        $tableWrapper.on("click", "tbody .custom-checkbox.is-row-selector", function () {
            var id = $(this).parents("tr").attr("id"); //this.id;
            if (id) {
                var index = $.inArray(id, smartvma.dataTable.selectedServices);
                if (index === -1) {
                    smartvma.dataTable.selectedServices.push(id);
                } else {
                    smartvma.dataTable.selectedServices.splice(index, 1);
                }
                smartvma.dataTable.updateSelectAll();
            }
        });


        $tableWrapper.on("change", "#selectAllDtRows", function () {
            var rowIdPrefix = "RowId_";
            var isChecked = $(this).is(":checked");
            if (isChecked) {
                if (smartvma.dataTable.allServiceIds) {
                    for (var i = 0; i < smartvma.dataTable.allServiceIds.length; i++) {
                        smartvma.dataTable.selectedServices.push(rowIdPrefix + smartvma.dataTable.allServiceIds[i]);
                    }
                    $table.find("tr").each(function (j, row) {
                        var $input = $(row).find('td:first input[type="checkbox"]');
                        $input.prop("checked", true);
                    });
                }
            }
            else {
                smartvma.dataTable.selectedServices = [];
                $table.find("tr").each(function (k, row) {
                    var $input = $(row).find('td:first input[type="checkbox"]');
                    $input.prop("checked", false);
                });
            }
        });

        /* DELETE */
        $tableWrapper.on("click", ".delete-table:not(.custom-disabled)", function (e) {
            if (smartvma.dataTable.selectedServices && smartvma.dataTable.selectedServices.length && controllerName) {
                smartvma.modals.open("/Admin/" + controllerName + "/DeletePrompt");
            }
        });
        /* SAVE */
        $tableWrapper.on("click", ".save-table:not(.custom-disabled)", function (e) {
            e.stopPropagation();
            if (!$table.find("tbody tr.hasChanged").length
                && !$table.find("tbody tr.new-row").length) {
                e.preventDefault();
                $(".no-items-selected").removeClass("hidden");
                return;
            }
            // set not updated items as disabled
            $("table tr:not(.hasChanged):not(.new-row)").find("input:not(.not-disabable), textarea:not(.not-disabable), select:not(.not-disabable)").attr("disabled", true);

            var updateInputNames = function ($rows) {
                $rows.each(function (rowIndex, row) {
                    $(row).find("input, textarea, select, label").each(function (i, input) {

                        var $input = $(input);
                        var elementId = $input.attr("id");
                        if (elementId) {
                            $input.attr("id", elementId.replace(/\d+/, rowIndex));

                        }
                        var elementName = $input.attr("name");
                        if (elementName) {
                            $input.attr("name", elementName.replace(/\d+/, rowIndex));
                            $input.parents(".input-wrapper").find("[data-valmsg-for]").attr("data-valmsg-for", elementName.replace(/\d+/, rowIndex));
                        }
                        var elementFor = $input.attr("for");
                        if (elementFor) {
                            $input.attr("for", elementFor.replace(/\d+/, rowIndex));
                        }
                        $input.removeAttr("disabled");
                    });
                });
            };
            updateInputNames($table.find("tbody tr.hasChanged,tbody tr.new-row"));
            $(".selected-items").val(smartvma.dataTable.selectedServices);
            var $form = $table.parents("form");
            $form.removeData("validator").removeData("unobtrusiveValidator").removeData("previousValue");
            $.validator.unobtrusive.parse($form);
            if (!$form.valid()) {
                updateInputNames($table.find("tbody tr"));
                return;
            }
            smartvma.controls.showLoader();
            $.ajax({
                url: $form.attr("action"),
                method: $form.attr("method"),
                data: $form.serialize(),
                success: function (response) {
                    $(".save-errors-container").html("");
                    if (response.isSuccess) {
                        smartvma.dataTable.selectedServices = [];
                        smartvma.dataTable.updateSelectAll();
                        $table.dataTable()._fnDraw();
                        if (smartvma.dataTable.defaultMode !== "edit") {
                            $table.removeClass("edit-mode");
                            $tableWrapper.removeClass("edit-mode-wrapper");
                            updateButtonStatus(false);
                        }
                        $(".no-items-selected").hide();
                        smartvma.dataTable.enableRowSelectors();
                    } else {
                        $.each(response.errors, function (key, val) {
                            var $errorLabel = $("td div.edit-view span[data-valmsg-for]").filter(function () {
                                return $(this).attr("data-valmsg-for").toLowerCase() === key.toLowerCase();
                            });
                            if ($errorLabel.length) {
                                $errorLabel.addClass("field-validation-error").removeClass("field-validation-valid");
                                $errorLabel.html("<span>" + val + "</span>");
                            } else {
                                $(".save-errors-container").append('<span class="field-validation-error">' + val + "</span>");
                            }
                        });
                        smartvma.controls.hideLoader();
                    }
                    $("table tr").find(".edit-view input, .edit-view textarea, .edit-view select").removeAttr("disabled");
                }
            });

        });

        /* ADD */
        $tableWrapper.on("click", ".add-table", function (e) {
            e.stopPropagation();

            var rowIndex = 0;
            if ($("table .dataTables_empty").length) {
                $("table .dataTables_empty").remove();
            } else {
                rowIndex = $table.find("tbody tr").length;
            }

            var $tableRow = $table.find('tr[data-role="template"]:last').clone();
            $tableRow.find("th").replaceWith(function () {
                return $("<td />", { html: $(this).html() });
            });

            $tableRow.find("td").each(function (idx, el) {
                var inputName = "items[" + rowIndex + "]." + $table.find("thead tr:last th").eq(idx).attr("name").replace(",", 0);

                var $input = $(el).find("input, select, textarea");
                $input.attr("id", inputName.replace(/[\[\]\.]+/g, "_")).attr("name", inputName);
                $(el).find("label").attr("for", inputName.replace(/[\[\]\.]+/g, "_"));
                $(el).find('input[type="hidden"].custom-checkbox').removeAttr("id");
                $(el).find('input[type="hidden"].id-field').remove();
                $(el).find(".field-validation-valid").attr("data-valmsg-for", inputName);
            });

            $tableRow.removeAttr("data-role");
            //} else {
            //    $tableRow = $table.find('tbody tr:first').clone();
            //}

            $tableRow.toggleClass("odd").toggleClass("even").addClass("new-row");
            $tableRow.find("input:not(.custom-checkbox), textarea, select").val("");
            $tableRow.find(".read-view").text("");

            $tableRow.find("td").each(function (i, tableCell) {
                if ($(tableCell).find(".input-wrapper").length > 1) {
                    $(tableCell).find(".input-wrapper:not(:first)").remove();
                    $(tableCell).find("input.id-field:not(:first)").remove();
                }

                $(tableCell).find(":checked").prop("checked", false);
            });

            $tableRow.find("select").each(function (idx, el) {
                var $el = $(el);
                $el.parents(".input-wrapper").append($el);
                $el.siblings(".hide-native-select").remove();
                $el.removeClass("initialized");
            });
            $tableRow.find("input, textarea, select").removeAttr("disabled");
            $table.find("tbody").prepend($tableRow);
            $table.addClass("edit-mode");
            $tableWrapper.addClass("edit-mode-wrapper")
            updateButtonStatus(true);
            smartvma.multiselect.init();
            $table.find('tr.new-row [id$="bgProductId"]').trigger("change");
            smartvma.dataTable.uncheckAndDisableRowSelectors();
            smartvma.dataTable.disableOrderableMode();

            return false;
        });

        $tableWrapper.on("change", "input:not(.not-trackable), textarea:not(.not-trackable), select:not(.not-trackable)", function () {
            smartvma.dataTable.markAsChanged($(this));
        });

        $tableWrapper.on("click", "a.add-cell-part", function () {
            var cellCount = $(this).data("sibling-cell-count") || 1;
            var $tableCell = $(this).parents("td");
            for (var i = 0; i < cellCount; i++) {
                var $wrapper = $tableCell.find(".input-wrapper:last").clone();
                $wrapper.children().removeClass("marked-for-deletion");
                $wrapper.addClass("new-cell-input");
                var $input = $wrapper.find("input, select, textarea");
                $input.attr("id", incrementLastIndex($input.attr("id")))
                    .attr("name", incrementLastIndex($input.attr("name")));

                if ($input.is(":checkbox")) {
                    $input.prop("checked", false);
                    $input.siblings("label").attr("for", $input.attr("id"));
                    $input.siblings('[type="hidden"]').attr("name", $input.attr("name")).val("false");
                } else {
                    $input.val("");
                }
                if ($input.is("select")) {
                    var appendTo = $tableCell.find(".edit-view");
                    $input.removeClass("initialized");
                    $('<div class="input-container"><div class="input-wrapper">' + $input.prop("outerHTML") + "</div></div>").appendTo(appendTo);
                    smartvma.multiselect.init();

                }
                else {
                    $tableCell.find(".edit-view").append($wrapper);
                }

                $tableCell = $tableCell.next("td");
                console.log($tableCell.attr("id"));
            }
        });

        $tableWrapper.on("click", "a.delete-cell-part", function () {
            var $parent = $(this).parent(".delete-cell-part-wrapper");
            var $psiblings = $parent.siblings("input, select, textarea");
            var deletionClass = "marked-for-deletion";
            var isMarkedForDeletion = !$(this).hasClass(deletionClass);

            if ($psiblings.length) {
                var fullId = $psiblings.attr("id");
                var idSelector = fullId.substring(0, fullId.lastIndexOf("_"));
                smartvma.dataTable.markAsChanged($(this));
                var $elementsForUpdate = $("input[id^='" + idSelector + "'][type!='hidden']");
                var $isDeletedElement = $("input[id^='" + idSelector + "_isDeleted" + "']");
                if (isMarkedForDeletion) {
                    $elementsForUpdate.addClass(deletionClass);
                    $(this).addClass(deletionClass);
                } else {
                    $elementsForUpdate.removeClass(deletionClass);
                    $(this).removeClass(deletionClass);
                }
                $isDeletedElement.prop("checked", isMarkedForDeletion);
            }
        });

        $tableWrapper.on("change", '[id$="bgProductId"]', function () {
            var $protectionPlanList = $(this).parents("tr").find('[id$="bgProtectionPlanId"]');
            var $productCategoriesList = $(this).parents("tr").find('[id$="categoryId"]');
            var $videosList = $(this).parents("tr").find('[id$="videoId"]');
            var bgProductIds = [];

            $(this).parents("td").find('[id$="bgProductId"]').each(function (i, el) {
                var val = $(el).val();
                if (val) {
                    bgProductIds.push(val);
                }
            });

            if (bgProductIds.length === 0) {
                smartvma.multiselect.destroyData($protectionPlanList, true);
                smartvma.multiselect.destroyData($productCategoriesList, true);
                smartvma.multiselect.destroyData($videosList, true);
                return;
            }

            var data = {
                bgProducts: bgProductIds,
                rowIndex: $protectionPlanList.attr("id").replace(/[^0-9]+/ig, ""),
                currentCategoryId: $productCategoriesList.val(),
                currentProtectionPlanId: $protectionPlanList.val(),
                currentVideoId: $videosList.val()
            };

            $.ajax({
                url: "/Dropdown/GetBgProductsDependentItems",
                type: "POST",
                data: data,
                success: function (response) {
                    for (var key in response) {
                        if (response.hasOwnProperty(key)) {
                            var $element = $(key);
                            smartvma.multiselect.resetData($element, response[key]);
                            $element.parents("td").find(".read-view").html($element.find("option:selected").text());
                        }
                    }
                }
            });
        });


        $(document).on("click", ".paginate_button", function () {
            if ($(this).hasClass("disabled") || $(this).hasClass("current")) {
                return;
            }

            if ($table.hasClass("page-prompt") && $table.hasClass("edit-mode")) {
                if ($table.find("tr.hasChanged").length > 0
                    || $table.find("tr.new-row").length > 0
                    || ($table.find('[id$="isSelected"]').length > 0
                        && $table.find('[id$="isSelected"]:checked').length > 0)) {
                    smartvma.modals.open("/Admin/OemServices/PageChangePrompt");
                }
                else {
                    smartvma.controls.showLoader();
                }
            }
            else {
                smartvma.controls.showLoader();
            }
        });

        $(document).on("click", ".table-prompt-save", function () {
            smartvma.modals.close();
            $(".save-table").trigger("click");
        });

        $(document).on("click", ".table-prompt-cancel", function () {
            smartvma.modals.close();
            $(".cancel-table").trigger("click");
        });

        $(document).on("click", ".table-delete-prompt-delete", function () {
            smartvma.modals.close();
            if (smartvma.dataTable.selectedServices && smartvma.dataTable.selectedServices.length) {
                $(".selected-items").val(smartvma.dataTable.selectedServices);
                var $form = $table.parents("form");
                smartvma.controls.showLoader();

                $.ajax({
                    url: $form.attr("action"),
                    method: $form.attr("method"),
                    data: $form.serialize(),
                    success: function (response) {
                        $(".save-errors-container").html("");
                        if (response.isSuccess) {
                            smartvma.dataTable.selectedServices = [];
                            smartvma.dataTable.updateSelectAll();
                            $table.dataTable()._fnDraw();
                            if (smartvma.dataTable.defaultMode !== "edit") {
                                $table.removeClass("edit-mode");
                                $tableWrapper.removeClass("edit-mode-wrapper");
                                updateButtonStatus(false);
                            }
                            $(".no-items-selected").hide();

                        } else {
                            $.each(response.errors, function (key, val) {
                                $(".save-errors-container").append('<span class="field-validation-error">' + val + "</span>");
                            });
                            smartvma.controls.hideLoader();
                        }
                        $("table tr").find(".edit-view input, .edit-view textarea, .edit-view select").removeAttr("disabled");
                    }
                });
            }
        });

    }
};