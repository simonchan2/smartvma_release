﻿smartvma.carousel = {
    init: function () {
        $('.carousel').slick({
            dots: true,
            dotsClass: 'slick-nav',
            arrows: true,
            adaptiveHeight: true,

            customPaging: function (slider, i) {
                var img = slider.$slides.find('img');

                var text = img[i].alt;
                return '<button type="button" data-role="none" role="button" aria-required="false" tabindex="0">' + text + '</button>';
            }
        });
    }
}