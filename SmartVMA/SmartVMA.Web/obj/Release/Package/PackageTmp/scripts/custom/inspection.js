﻿$(function () {
    smartvma.inspection.init();
});

smartvma.inspection = {
    selectedDITypeId: 0,
    isComment: false,
    newIndications: [],
    init: function () {
        this.attachEvents();
        this.drawAll(damageIndications);
    },

    attachEvents: function () {
        var that = this;
        $(document).on("click", ".damage-indication-type", function () {
            selectedDITypeId = $(this).data("id");
            isComment = ($(this).data("iscomment") == "True");
            return false;
        });

        $(document).on("click", ".view-point", function (e) {
            that.addDamageIndication($(this), e);
        });

        $(document).on("click", ".damage-indication", function () {
            that.openEditModal($(this).data("id"));
        });

        $(document).on("fileuploaddone", "input.fileUpload", function (e, data) {
            $.ajax({
                url: '/Dealer/Inspection/SavePhoto?fileName=' + data.result.tempFileName + '&inspectionId=' + inspectionId,
                type: 'GET',
                success: function (data) {
                    $("#vehiclePhotos").html(data);
                    console.log(data);
                }
            });
        });

        $(document).on("click", ".icon-clear-all", function (e) {
            $('.view-point-container.carImg a').remove();
            $('.custom-radio').prop('checked', false);
            $('.custom-checkbox').prop('checked', false);
            $.ajax({
                url: '/Dealer/Inspection/ResetInspection?inspectionId=' + inspectionId,
                type: 'POST'
            });
        });
        
    },

    openEditModal: function(elemId){
        smartvma.modals.open(editDIUrl + "?id=" + elemId, "Edit");
    },

    addDamageIndication: function (viewpoint, e) {
        var damageIndication = {};
        var offset = viewpoint.offset();
        var width = viewpoint.width();
        var height = viewpoint.height();
        var index;
        damageIndication.TypeId = selectedDITypeId;
        damageIndication.Comment = null;
        damageIndication.OffsetLeft = parseInt(this.calcOffset(offset.left, width, e.pageX), 10);
        damageIndication.OffsetTop = parseInt(this.calcOffset(offset.top, height, e.pageY), 10);
        damageIndication.ViewPointId = viewpoint.data("id");
        damageIndication.InspectionId = inspectionId;
        this.createDamageIndication(damageIndication);
    },

    deleteDamageIndication: function (id) {
        var that = this;
        $.ajax({
            url: deleteDIUrl,
            method: "POST",
            data: {id: id},
            success: function (response) {
                if (response.isSuccess) {
                    that.erase(id);
                    smartvma.modals.close();
                }
                else {
                    console.log(response);
                    //TO-DO handle Errors
                }
            }
        });
    },


    erase: function (id) {
        $(".damage-indication[data-id='" + id + "']").remove();
    },

    draw: function (indication, index) {
        var percent100 = 100;
        var viewpoint = $(".view-point[data-id='" + indication.ViewPointId + "']");
        var type = $(".damage-indication-type[data-id='" + indication.TypeId + "']");
        var top = this.calcOffset(indication.OffsetTop, $(type).height() / $(viewpoint).height());
        var left = this.calcOffset(indication.OffsetLeft, $(type).width() / $(viewpoint).width());
        var path = type.data("url");

        console.log(type);
        var icon = type.data('icon');
        var $element = '<a class="damage-indication ' + icon + '" data-id="' + indication.Id + '" data-toggle="modal" data-target="#jsModal" data-index="' + index + '" src="' + path + '" style="position: absolute; top: ' + top + '%; left: ' + left + '%;"></a>'
        $(viewpoint).parent().append($element);
    },

    drawAll: function (damageIndications) {
        for (var i = 0; i < damageIndications.length; i++) {
            this.draw(damageIndications[i], i);
        }
    },

    createDamageIndication: function (damageIndication) {
        var that = this;
        var index = damageIndications.push(damageIndication) - 1;
        $.ajax({
            url: addDIUrl,
            method: "POST",
            data: damageIndication,
            success: function (response) {
                if (response.isSuccess && response.savedItemId) {
                    damageIndication.Id = response.savedItemId;
                    that.draw(damageIndication, index);
                    if ($(".damage-indication-type[data-id='" + damageIndication.TypeId + "']").data("iscomment") === "True") {
                        that.openEditModal(damageIndication.Id);
                    }
                }
                else
                {
                    console.log(response);
                    //TO-DO handle Errors
                }
            }
        });
    },

    calcOffset: function (offset, dimension, pageCoord) {
        var percent100 = 100;
        var offsetPercentage;
        if (pageCoord) {
            offsetPercentage = parseInt(((pageCoord - offset) / dimension) * percent100 * percent100, 10) / percent100;
        }
        else {
            offsetPercentage = offset - (dimension * percent100 / 2);
        }
        return offsetPercentage;
    },

    deletePhoto: function (id, inspectionId) {
    var that = this;
    $.ajax({
        url: deletePhotoUrl,
        method: "POST",
        data: { id: id, inspectionId: inspectionId },
        success: function (response) {
            if (response.isSuccess) {
                smartvma.modals.close();
                $.ajax({
                    url: '/Dealer/Inspection/GetVehiclePhotos?inspectionId=' + inspectionId,
                    type: 'GET',
                    success: function (data) {
                        $("#vehiclePhotos").html(data);
                    }
                });
            }
            else {
                console.log(response);
                //TO-DO handle Errors               
            }
        }
    });
    },

    printPhoto: function (imageUrl) {
        var win = window.open(imageUrl, '_blank');
        win.focus();
        setTimeout(function () { win.print(); }, 3000);
    },
}