﻿$(document).ready(function () {
    var selectedImgs = $('.lof-table-menu .custom-checkbox:checked').length;
    if (selectedImgs >= 7) {
        $('.lof-table-menu .custom-checkbox:not(:checked)').attr('disabled', true);
    }
    else {
        $('.lof-table-menu .custom-checkbox:not(:checked)').attr('disabled', false);
    }
})