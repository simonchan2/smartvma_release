﻿// general options
var generalOptions = {
    modalOptions: {
        backdrop: 'static'
    },
    multiselect: {
        enableFullValueFiltering: true,
        filterBehavior: 'text',
        includeSelectAllOption: true,
        allSelectedText: 'All selected',
        selectAllText: 'Select all',
        onInitialized: function () {
            var $container = $(this)[0].$container;
            if ($container.parents('.inline-label').length > 0) {
                var label = $container.closest('.input-container').children('label').text();
                var $multiselect = $(this)[0];
                $multiselect.$button.prepend('<span class="label-text">' + label + '</span>');
            }
        },
        onDropdownShow: function () {
            var $multiselect = $(this)[0];
            smartvma.multiselect.makeSearchBoxFixed($multiselect);
        },
        onDropdownShown: function () {
            if ('$filter' in $(this)[0]) {
                $(this)[0].$filter[0].children[0].children[1].focus();
            }

        }
    },
    datatable: {
        processing: true, // for show progress bar
        language: {
            search: '_INPUT_',
            searchPlaceholder: 'Search...'
        }
    },
    uploader: {
        url: '/Files/Upload',
        dataType: 'json',
        formData: { fileGuid: '', chunkId: 0 },
        maxChunkSize: 5000000, // 5MB
        autoUpload: true,
        maxFileSize: 999000
    },
    datePicker: function () {
        return { format: '' };
    },
    dateRange: function () {
        return {
            autoUpdateInput: true,
            autoApply: true,
            locale: { format: '' }
        };
    },
    singleselect: {
        enableFullValueFiltering: true,
        filterBehavior: 'text',
        includeSelectAllOption: false,
        numberDisplayed: 1,
        onInitialized: function () {
            var $container = $(this)[0].$container;
            if ($container.parents('.inline-label').length > 0) {
                var label = $container.closest('.input-container').children('label').text();
                var $multiselect = $(this)[0];
                $multiselect.$button.prepend('<span class="label-text">' + label + '</span>');
            }
            if ($(this)[0].$container.parents('.input-clear').length > 0) {
                var closeBtn = $($(this)[0].$container[0]).append('<span class="close-dropdown"><b class="caret"></b></span>');
                closeBtn.on('click', function () { });
            }
        },
        onDropdownShow: function () {
            var $multiselect = $(this)[0];
            smartvma.multiselect.makeSearchBoxFixed($multiselect);
        },
        onDropdownShown: function () {
            if ('$filter' in $(this)[0]) {
                $(this)[0].$filter[0].children[0].children[1].focus();

            }
        },
        onChange: function (option, checked) {
            var $element = $('#' + option.parents('select').attr('id'));
            $element.multiselect('deselectAll', false);
            if (checked === true) {
                $element.multiselect('select', option.val());
            }

            if ($element.data('container')) {
                $(document).click();//this resolves the problem when choosing from single select and dropdown-menu is not closing
            } else {
                this.$container.removeClass('open');
            }
        }
    },
    currentDateFormat: function () {
        return $('#currentCultureFormat').val();
    }
};
// general options - end

$(function () {
    smartvma.controls.hideLoader();
    $(document).on('keypress', function (e) {
        if (e.which === buttonKeys.EnterKey && $(".default-submit-button").length > 0) {
            e.preventDefault(); //stops default action: submitting form
            $(this).blur();
            $('.default-submit-button').focus().click();
        }
    });

    $('.menu-tabs').on('click', '.menu-opcode .btn-toggle', function () {
        $(this).toggleClass('collapsed');
        $(this).closest('.menu-item-row').find('.extra-customer-info').toggle();
    });

    smartvma.controls.init();
    initPage();
    $(window).resize(initPage);

    if ($('#maintenanceServicesContainer').length > 0 && !$('#MenuMileage').val()) {
        var carId = $('#MenuCarId').val();
        smartvma.modals.open('/Dealer/MenuPresentation/OdoMeter' + '?carId=' + carId);
    }
    if ($('#MenuMileage').val() != null && $('#MenuMileage').val() != "0" && $('#MenuMileage').val() != "") {
        smartvma.multiselect.select($('#CustomerInfo_Mileage'), $('#MenuMileage').val(), true);
    }
    if ($('#MenuMileage').val() != null && $('#MenuMileage').val() != "0" && $('#MenuMileage').val() != "" && $('#EnteredMileage').val() != null && $('#EnteredMileage').val() != "0" && $('#EnteredMileage').val() != "") {
        var maxMileage = 0;
        $('#CustomerInfo_Mileage option').each(function (i, item) {
            var optionValue = parseInt(item.value);
            if (maxMileage < optionValue) {
                maxMileage = optionValue;
            }
        });
        if ($('#EnteredMileage').val() > maxMileage && maxMileage > 0 && $('#MenuMileage').val() == maxMileage) {
            smartvma.modals.open('/Dealer/MenuPresentation/ValidateMaxMileage');
        }
    }
    if ($('#SelectedMenuLevel').val()) {
        switch ($('#SelectedMenuLevel').val()) {
            case "1": { $('#level1Btn').trigger('click'); } break;
            case "2": { $('#level2Btn').trigger('click'); } break;
            case "3": { $('#level3Btn').trigger('click'); } break;
        }
    }
    $('body').on('click', '.options-menu', function () {
        if ($(this).offset().top - $(window).scrollTop() < 200) {
            $(this).closest('.options-menu').removeClass('dropup').addClass('dropdown');
        } else {
            $(this).closest('.options-menu').removeClass('dropdown').addClass('dropup');
        }
    });

    var isOemServiceAvailable = false;
    $('#level3Services tr td #ServiceTypeId').each(function (i, tRow) {
        if ($(tRow).val() == "1") { isOemServiceAvailable = true; }
    });
    var isOemServiceAvailable1 = false;
    $('#level2Services tr td #ServiceTypeId').each(function (i, tRow) {
        if ($(tRow).val() == "1") { isOemServiceAvailable1 = true; }
    });
    var isOemServiceAvailable2 = false;
    $('#level1Services tr td #ServiceTypeId').each(function (i, tRow) {
        if ($(tRow).val() == "1") { isOemServiceAvailable2 = true; }
    });
    var x = readCookie('tempTranslateLanguage');
    if (!x) {
        document.cookie = "googtrans" + "=" + "/en/" + $('#languagecode').text().toLowerCase() + "; path=/";
    }
    smartvma.callbacks.refreshServices();
});


// methods
var buttonKeys = { "EnterKey": 13 };
var smartvma = {};
smartvma.getParameterByName = function (name, url) {
    if (!url) {
        url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
},
smartvma.controls = {
    init: function () {
        smartvma.multiselect.init();
        smartvma.dataTable.init();
        if (smartvma.uploader) {
            smartvma.uploader.init();
        }
        if (smartvma.textEditor && window.tinymce) {
            smartvma.textEditor.init();
        }
        if (smartvma.carousel) {
            smartvma.carousel.init();
        }
        //smartvma.copyToClipboardButton.init();
        //TODO: after testing, put this in function
        $('.hide-input-group').addClass('invisible');
    },
    refreshContainer: function (selector, dataUrl) {
        $(selector.split(',')).each(function (idx, el) {
            var $element = $(el);
            if (!$element.length) {
                return false;
            }
            var customOnStartFunction = $element.data('custom-onstart-function');
            if (customOnStartFunction) {
                smartvma.controls.executeFunctionByName(customOnStartFunction, window, $element);
            }

            var url = dataUrl || $element.attr('data-url');
            if (!url) {
                return false;
            }

            var options = {
                url: smartvma.controls.mapUrl(url),
                method: $element.data('request-type') || 'GET',
                success: function (data) {
                    if (typeof (data) !== 'object') {
                        $element.html(data);
                    }
                },
                complete: function (data) {
                    var customOnCompleteFunction = $element.data('custom-oncomplete-function');
                    if (customOnCompleteFunction) {
                        smartvma.controls.executeFunctionByName(customOnCompleteFunction, window, $element, data.responseJSON);
                    }
                    smartvma.controls.hideLoader();
                    smartvma.controls.init();
                }
            };

            var requestDataFunctionName = $element.data('request-data-function');
            if (requestDataFunctionName) {
                options.data = smartvma.controls.executeFunctionByName(requestDataFunctionName);
            }
            smartvma.controls.showLoader();
            $.ajax(options);
            return true;
        });
    },
    executeFunctionByName: function (functionName, context /*, args */) {
        if (!functionName) {
            smartvma.controls.hideLoader();
            return false;
        }
        if (!context) {
            context = window;
        }

        var args = [].slice.call(arguments).splice(2);
        var namespaces = functionName.split('.');
        var func = namespaces.pop();
        for (var i = 0; i < namespaces.length; i++) {
            context = context[namespaces[i]];
        }
        return context[func].apply(context, args);
    },
    mapUrl: function (str) {
        if (typeof (str) !== 'undefined') {
            $.each(str.split(/[\/|=|?|&]/), function (i, param) {
                if (param.startsWith('[')) {
                    var value = $(param.substring(1, param.length - 1)).val();
                    str = str.replace(param, value != null ? value : '');
                }
            });
            return str;
        }
        return null;
    },
    parseBool: function (obj) {
        if (typeof obj === 'string') {
            return obj.toLowerCase() === 'true';
        }
        if (typeof obj === 'boolean') {
            return obj;
        }
        return false;
    },
    parseDouble: function (str) {
        if (!str) {
            return 0;
        }
        var x = readCookie('tempTranslateLanguage');
        if (x != null) {

            var lng = x.split("/")[x.split("/").length - 1];

            if (lng == 'es' || lng == 'fr') {
                str = str.replace('$', '').replace(/[^0-9\,-]/g, '').replace(',', '.');
            }
            else {
                str = str.replace('$', '').replace(/[^0-9\.-]/g, '');
            }
        }
        else {
            str = str.replace('$', '').replace(/[^0-9\.-]/g, '');
        }

        return parseFloat(str);
    },
    parseInt: function (str) {
        if (!str) {
            return 0;
        }
        return parseInt(str.replace(/\D/g, ''));
    },
    showLoader: function () {
        if (!$('div.loading').length) {
            $('<div class="loading"><div class="loader"></div></div>').appendTo('body');
        }
    },
    hideLoader: function () {
        $('div.loading').remove();
    }
};

smartvma.multiselect = {
    init: function () {
        $('.multiselectlist:not(.initialized)').each(function (i, element) {
            var $element = $(element);
            var options = $element.is('.singleoption') ? generalOptions.singleselect : generalOptions.multiselect;
            options.enableFiltering = !$element.data('hide-search');
            options.enableCaseInsensitiveFiltering = !$element.data('hide-search');

            var fullValueFiltering = $element.data('disable-full-value-filtering');
            if (fullValueFiltering !== undefined) {
                options.enableFullValueFiltering = !smartvma.controls.parseBool(fullValueFiltering);
            }
            else {
                options.enableFullValueFiltering = true;
            }

            var refreshContainerAfterChange = $element.data('container');
            var refreshElementsAfterChange = $element.data('refresh-element-data');
            if (refreshContainerAfterChange || refreshElementsAfterChange) {
                options.onDropdownHide = function () {
                    var currentValue = $element.val();
                    var prevValue = $element.attr('data-prev-val');
                    if ((!currentValue && !prevValue) || (currentValue && prevValue === currentValue.toString())) {
                        return;
                    }

                    $element.attr('data-prev-val', $element.val());
                    if (refreshContainerAfterChange) {
                        var onChangeFunctionName = $element.data('on-change-function');
                        if (onChangeFunctionName) {
                            smartvma.controls.executeFunctionByName(onChangeFunctionName, window, $(this));
                        }
                        smartvma.controls.refreshContainer(refreshContainerAfterChange);
                    }

                    if (refreshElementsAfterChange) {
                        var ajaxRequests = [];
                        $($element.data('refresh-element-data')).each(function () {
                            var $refreshElement = $(this);
                            var noRefresh = $refreshElement.data('norefresh-ondropdownhide');
                            if (!noRefresh) {
                                var ajaxOption = {
                                    elementId: '#' + $refreshElement.attr('id'),
                                    url: smartvma.controls.mapUrl($refreshElement.data('remote-source-url')),
                                    method: $refreshElement.data('request-type') || 'GET'
                                };

                                var requestDataFunctionName = $refreshElement.data('request-data-function');
                                if (requestDataFunctionName) {
                                    ajaxOption.data = smartvma.controls.executeFunctionByName(requestDataFunctionName);
                                }
                                ajaxRequests.push(ajaxOption);
                            }
                        });

                        $.each(ajaxRequests, function (idx, option) {
                            option.success = function (response) {
                                smartvma.multiselect.resetData($(option.elementId), response);
                            };
                            $.ajax(option);
                        });
                    }
                }
            }
            $element.multiselect(options);
            $element.addClass('initialized');
            if (!$element.find('option').length) {
                $element.multiselect('disable');
            }
        });

    },
    destroy: function ($element, $elementContainer) {
        if ($element && $element.is('.multiselectlist.initialized')) {
            $element.multiselect('destroy');
        }
        if ($elementContainer) {
            $elementContainer.find('.multiselectlist.initialized').multiselect('destroy');
        }
    },
    destroyData: function ($element, forceDataDestroy) {
        if ($element.length && $element.is('.multiselectlist.initialized')) {
            if (forceDataDestroy || ($element.data('remote-source-url') && !$element.hasClass('noreset'))) {
                smartvma.multiselect.resetData($element, []);
            } else {
                smartvma.multiselect.deselectAll($element);
            }
        }
    },
    rebuild: function ($element) {
        if ($element.length && $element.is('.multiselectlist.initialized')) {
            $element.multiselect('rebuild');
        }
    },
    enable: function ($element) {
        if ($element.length && $element.is('.multiselectlist.initialized')) {
            $element.multiselect('enable');

        }
    },
    disable: function ($element) {
        if ($element.length && $element.is('.multiselectlist.initialized')) {
            $element.multiselect('disable');
        }
    },
    deselect: function ($element, value) {
        if ($element.length && $element.is('.multiselectlist.initialized')) {
            $element.multiselect('deselect', value);
        }
    },
    deselectAll: function ($element) {
        if ($element.length && $element.is('.multiselectlist.initialized')) {
            $element.multiselect('deselectAll', false);
            $element.multiselect('updateButtonText');
        }
    },
    select: function ($element, value, isInitalChange) {
        if ($element.length && $element.is('.multiselectlist.initialized')) {
            if ($element.hasClass('singleoption')) {
                $element.multiselect('deselectAll', false);
            }
            $element.multiselect('select', value, true);
            var refreshContainerAfterChange = $element.data('container');
            if (refreshContainerAfterChange && !isInitalChange) {
                smartvma.controls.refreshContainer(refreshContainerAfterChange);
            }
        }
    },
    selectAll: function ($element) {
        if ($element.length && $element.is('.multiselectlist.initialized')) {
            $element.multiselect('selectAll', false);
            $element.multiselect('updateButtonText');
        }
    },
    makeSearchBoxFixed: function ($element) {
        var $multiselect = $($element.$ul[0]);
        var $multiselectLi = $multiselect.find('li:not(:first-child)');
        var $multiselectWrapper = $multiselect.find(".multiselect-options-wrapper");
        var $searchBox = $($element.$select[0]);
        var shouldBefixedClass = "should-be-fixed";
        var shouldBeFixed = $searchBox.hasClass(shouldBefixedClass);
        if (shouldBeFixed && $multiselect.length > 0 && $multiselectLi.length > 0 && $element.$filter && $element.$filter.length > 0 && $multiselectWrapper.length === 0) {
            $multiselectLi.wrapAll('<div class="multiselect-options-wrapper"></div>');
        }
    },
    resetData: function ($element, response) {
        smartvma.controls.hideLoader();
        if ($element.length && $element.is('.multiselectlist.initialized')) {
            var parseResponse = function (jsonResponse) {
                if (typeof jsonResponse !== 'object' || jsonResponse == null) {
                    return null;
                }
                var resultData = [];
                if (jsonResponse.length) {
                    var dataWithGroups = {};
                    $.each(jsonResponse, function (i, element) {
                        var item = {
                            label: element.text,
                            value: element.value,
                            selected: element.selected,
                            disabled: element.disabled
                        };
                        var group = element.group;
                        if (group) {
                            var groupKey = group.name;
                            if (!dataWithGroups[groupKey]) {
                                dataWithGroups[groupKey] = [];
                            }
                            dataWithGroups[groupKey].push(item);
                        } else {
                            resultData.push(item);
                        }
                    });
                    if (resultData.length === 0) {
                        $.each(dataWithGroups, function (key, value) {
                            var groupElement = { label: key, children: value };
                            resultData.push(groupElement);
                        });
                    }
                }
                return resultData;
            };
            var setData = function ($el, parsedData) {
                if (!$el.length || !parsedData) {
                    return;
                }

                $el.multiselect('dataprovider', parsedData);
                if (!$el.find('option').length) {
                    $el.multiselect('disable');
                }

                $el.attr('data-prev-val', $el.val());
            };

            if ($.isArray(response)) {
                setData($element, parseResponse(response));
            } else {
                $.each(response, function (key, val) {
                    setData($(key), parseResponse(val));
                });
            }
        }
    }
};

smartvma.textEditor = {
    init: function () {

        tinymce.init({
            selector: '.richtexteditor',
            statusbar: false,
            menubar: '',
            automatic_uploads: true,
            theme: 'modern',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'template paste textcolor colorpicker textpattern imagetools'
            ],
            toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor',
            image_advtab: true,
            file_browser_callback: function (fieldName, url, type) {
                if (type === 'image') { $('#my_form input').click(); }
            },
            setup: function (editor) {
                editor.on('change', function () { editor.save(); });
            }
        });
        /* fix inser link and insert focus problem */
        $(document).on('focusin', function (e) {
            if ($(e.target).closest('.mce-window').length) {
                e.stopImmediatePropagation();
            }
        });
    },
    destroyAll: function () {
        tinymce.EditorManager.editors = [];
    }
};

smartvma.modals = {
    openFlag: true,
    open: function (url, title, requestData, requestType) {
        smartvma.textEditor.destroyAll();
        var options = {
            url: url,
            method: requestType || 'GET',
            success: function (response) {

                if (smartvma.modals.openFlag) {
                    smartvma.modals.openFlag = false;
                    var $contents = $(response);
                    var options = generalOptions.modalOptions;
                    $contents.modal(options);

                    $contents.on('shown.bs.modal', function () {
                        $contents.find('.modal-title').html(title);
                        smartvma.controls.init();
                        $('form > div:first input:text:visible', this).focus();

                        // uncomment if multiple popups
                        //smartvma.modals.smartvma = true;
                    });

                    $contents.on('hidden.bs.modal', function () {
                        smartvma.textEditor.destroyAll();
                        $(this).remove();
                        smartvma.modals.openFlag = true;
                    });
                }
            }
        };

        if (requestData) {
            if (options.method.toLowerCase() === 'get') {
                options.data = decodeURIComponent($.param(requestData, true));
            } else {
                options.data = requestData;
            }
        }


        $.ajax(options);
    },

    close: function () {
        $('.modal').modal('toggle');
    },

    refreshLastModal: function (html) {
        var $modal = $('.modal:last .modal-body');
        if (!$modal.length) {
            return;
        }

        // destroy all controls in the current modal
        smartvma.multiselect.destroy(null, $modal);

        // set new html
        var $html = $(html);
        if ($html.hasClass('modal')) {
            $modal.parents('.modal-dialog').html($html.find('.modal-content'));
        } else {
            $modal.html($html);
        }

        // initialize new controls
        smartvma.controls.init();
    }
};

//smartvma.copyToClipboardButton = {
//    init: function () {
//        if (typeof (Clipboard) === 'function') {
//            var clipboard = new Clipboard('.copy-to-clipboard');
//        }
//    }
//};

smartvma.callbacks = {
    filterParkedMenus: function () {
        console.log('filterParkedMenus');
        var obj = {
            StartDate: $('#ParkedMenu_StartDate').val(),
            EndDate: $('#ParkedMenu_EndDate').val(),
            TimeFrameId: $('#ParkedMenu_TimeFrameId').val(),
            AdvisorIds: $('#ParkedMenu_AdvisorIds').val()
        };
        return obj;
    },
    filterAcceptedMenus: function () {
        var obj = {
            StartDate: $('#AcceptedMenu_StartDate').val(),
            EndDate: $('#AcceptedMenu_EndDate').val(),
            TimeFrameId: $('#AcceptedMenu_TimeFrameId').val(),
            AdvisorIds: $('#AcceptedMenu_AdvisorIds').val()
        };
        return obj;
    },
    resetParkedMenuTimeFrame: function () {
        if ($('.daterangepicker:visible').length) {
            smartvma.multiselect.deselect($('#ParkedMenu_TimeFrameId'), $('#ParkedMenu_TimeFrameId').val());
            smartvma.multiselect.select($('#ParkedMenu_TimeFrameId'), 0);
            $("#ParkedMenu_TimeFrameId").attr('data-prev-val', 0);
        }
    },
    resetParkedMenuCalendar: function () {
        var timeframeId = $("#ParkedMenu_TimeFrameId").val().join();

        var d = new Date();
        var dateFormat = generalOptions.currentDateFormat();
        switch (timeframeId) {
            case "0":
                $('.parkedMenuStartDate').html("--");
                $('.parkedMenuEndDate').html("--");
                $('#ParkedMenu_StartDate').val('');
                $('#ParkedMenu_EndDate').val('');

                break;
            case "1":
                $('.parkedMenuStartDate').html("--");
                $('.parkedMenuEndDate').html("--");
                $('#ParkedMenu_StartDate').val('');
                $('#ParkedMenu_EndDate').val('');

                break;
            case "2":
                var startDate = moment(d).format(dateFormat);
                var startMoment = new moment(d);

                $('.parkedMenuStartDate').html(startDate);
                $('.parkedMenuEndDate').html(startDate);
                $('#ParkedMenu_StartDate').val(startDate);
                $('#ParkedMenu_EndDate').val(startDate);

                $("#parked .date-select.icon-calendar").click();
                $('#ParkedMenu_StartDate_ParkedMenu_StartDate_DateRange').data('daterangepicker').startDate = startMoment;
                $('#ParkedMenu_StartDate_ParkedMenu_StartDate_DateRange').data('daterangepicker').endDate = startMoment;
                $("#parked .date-select.icon-calendar").removeClass('isSelected');
                $('.daterangepicker').hide();

                break;
            case "3":
                var start = new Date().setDate(d.getDate() - d.getDay() - 6);
                var end = new Date().setDate(d.getDate() - d.getDay());
                var startMoment = new moment(start);
                var endMoment = new moment(end);

                $('.parkedMenuStartDate').html(moment(start).format(dateFormat));
                $('.parkedMenuEndDate').html(moment(end).format(dateFormat));
                $('#ParkedMenu_StartDate').val(moment(start).format(dateFormat));
                $('#ParkedMenu_EndDate').val(moment(end).format(dateFormat));

                $("#parked .date-select.icon-calendar").click();
                $('#ParkedMenu_StartDate_ParkedMenu_StartDate_DateRange').data('daterangepicker').startDate = startMoment;
                $('#ParkedMenu_StartDate_ParkedMenu_StartDate_DateRange').data('daterangepicker').endDate = endMoment;
                $("#parked .date-select.icon-calendar").removeClass('isSelected');
                $('.daterangepicker').hide();

                break;
            case "4":
                var start = new Date(d.getFullYear(), d.getMonth() - 1, 1);
                var end = new Date(d.getFullYear(), d.getMonth(), 0);
                var startMoment = new moment(start);
                var endMoment = new moment(end);

                $('.parkedMenuStartDate').html(moment(start).format(dateFormat));
                $('.parkedMenuEndDate').html(moment(end).format(dateFormat));
                $('#ParkedMenu_StartDate').val(moment(start).format(dateFormat));
                $('#ParkedMenu_EndDate').val(moment(end).format(dateFormat));

                $("#parked .date-select.icon-calendar").click();
                $('#ParkedMenu_StartDate_ParkedMenu_StartDate_DateRange').data('daterangepicker').startDate = startMoment;
                $('#ParkedMenu_StartDate_ParkedMenu_StartDate_DateRange').data('daterangepicker').endDate = endMoment;
                $("#parked .date-select.icon-calendar").removeClass('isSelected');
                $('.daterangepicker').hide();

                break;
        }

        console.log('resetParkedMenuCalendar');
    },
    resetAcceptedMenuTimeFrame: function () {
        if ($('.daterangepicker:visible').length) {
            smartvma.multiselect.deselect($('#AcceptedMenu_TimeFrameId'), $('#AcceptedMenu_TimeFrameId').val());
            smartvma.multiselect.select($('#AcceptedMenu_TimeFrameId'), 0);
            $("#AcceptedMenu_TimeFrameId").attr('data-prev-val', 0);
        }
    },
    resetAcceptedMenuCalendar: function () {
        var timeframeId = $("#AcceptedMenu_TimeFrameId").val().join();

        var d = new Date();
        var dateFormat = generalOptions.currentDateFormat();
        switch (timeframeId) {
            case "0":
                $('.acceptedMenuStartDate').html("--");
                $('.acceptedMenuEndDate').html("--");
                $('#AcceptedMenu_StartDate').val('');
                $('#AcceptedMenu_EndDate').val('');

                break;
            case "1":
                $('.acceptedMenuStartDate').html("--");
                $('.acceptedMenuEndDate').html("--");
                $('#AcceptedMenu_StartDate').val('');
                $('#AcceptedMenu_EndDate').val('');

                break;
            case "2":
                var startDate = moment(d).format(dateFormat);
                var startMoment = new moment(d);

                $('.acceptedMenuStartDate').html(startDate);
                $('.acceptedMenuEndDate').html(startDate);
                $('#AcceptedMenu_StartDate').val(startDate);
                $('#AcceptedMenu_EndDate').val(startDate);

                $("#accepted .date-select.icon-calendar").click();
                $('#AcceptedMenu_StartDate_AcceptedMenu_StartDate_DateRange').data('daterangepicker').startDate = startMoment;
                $('#AcceptedMenu_StartDate_AcceptedMenu_StartDate_DateRange').data('daterangepicker').endDate = startMoment;
                $("#accepted .date-select.icon-calendar").removeClass('isSelected');
                $('.daterangepicker').hide();

                break;
            case "3":
                var start = new Date().setDate(d.getDate() - d.getDay() - 6);
                var end = new Date().setDate(d.getDate() - d.getDay());
                var startMoment = new moment(start);
                var endMoment = new moment(end);

                $('.acceptedMenuStartDate').html(moment(start).format(dateFormat));
                $('.acceptedMenuEndDate').html(moment(end).format(dateFormat));
                $('#AcceptedMenu_StartDate').val(moment(start).format(dateFormat));
                $('#AcceptedMenu_EndDate').val(moment(end).format(dateFormat));

                $("#accepted .date-select.icon-calendar").click();
                $('#AcceptedMenu_StartDate_AcceptedMenu_StartDate_DateRange').data('daterangepicker').startDate = startMoment;
                $('#AcceptedMenu_StartDate_AcceptedMenu_StartDate_DateRange').data('daterangepicker').endDate = endMoment;
                $("#accepted .date-select.icon-calendar").removeClass('isSelected');
                $('.daterangepicker').hide();

                break;
            case "4":
                var start = new Date(d.getFullYear(), d.getMonth() - 1, 1);
                var end = new Date(d.getFullYear(), d.getMonth(), 0);
                var startMoment = new moment(start);
                var endMoment = new moment(end);

                $('.acceptedMenuStartDate').html(moment(start).format(dateFormat));
                $('.acceptedMenuEndDate').html(moment(end).format(dateFormat));
                $('#AcceptedMenu_StartDate').val(moment(start).format(dateFormat));
                $('#AcceptedMenu_EndDate').val(moment(end).format(dateFormat));

                $("#accepted .date-select.icon-calendar").click();
                $('#AcceptedMenu_StartDate_AcceptedMenu_StartDate_DateRange').data('daterangepicker').startDate = startMoment;
                $('#AcceptedMenu_StartDate_AcceptedMenu_StartDate_DateRange').data('daterangepicker').endDate = endMoment;
                $("#accepted .date-select.icon-calendar").removeClass('isSelected');
                $('.daterangepicker').hide();

                break;
        }
    },
    saveArticleCategory: function () {
        $('#createCategoryArea,#openArticleCategoryForm').toggle();
        $('#createCategoryArea #Name').val('');
        $.ajax({
            url: '/ArticleCategory/GetAllCategories',
            method: 'GET',
            success: function (response) {
                smartvma.multiselect.resetData($('#CategoryIds'), response);
            }
        });
    },
    menuSetupRefreshLabels: function ($element) {
        var selectedValue = $element.val();
        if (selectedValue == '1') {
            $('#MenuLabelLevel2,#MenuLabelLevel3,#MenuPrefixLevel2,#MenuPrefixLevel3,#MenuSuffixLevel2,#MenuSuffixLevel3').val('');
        } else if (selectedValue == '2') {
            $('#MenuLabelLevel3,#MenuPrefixLevel3,#MenuSuffixLevel3').val('');
            $('#MenuLabelLevel2').val('Recommended');
        }
        else if (selectedValue == '3') {
            $('#MenuLabelLevel2').val('Recommended');
            $('#MenuLabelLevel3').val('Performance');
        }
        $('#MenuLabelLevel2,#MenuSuffixLevel2,#MenuPrefixLevel2').parents('.form-group').toggle(selectedValue != '1');
        $('#MenuPrefixLevel3,#MenuSuffixLevel3,#MenuLabelLevel3').parents('.form-group').toggle(selectedValue == '3');
    },
    menuSetupRefreshLofLabels: function ($element) {
        var selectedValue = $element.val();
        if (selectedValue == '1') {
            $('#LOFMenuLabelLevel2,#LOFMenuOpCodeLevel2,#LOFMenuLabelLevel3,#LOFMenuOpCodeLevel3').val('');
        } else if (selectedValue == '2') {
            $('#LOFMenuLabelLevel3,#LOFMenuOpCodeLevel3').val('');
            $('#LOFMenuLabelLevel2').val('Better');
        }
        else if (selectedValue == '3') {
            $('#LOFMenuLabelLevel2').val('Better');
            $('#LOFMenuLabelLevel3').val('Best');
        }
        $('#LOFMenuLabelLevel2,#LOFMenuOpCodeLevel2').parents('.form-group').toggle(selectedValue != '1');
        $('#LOFMenuLabelLevel3,#LOFMenuOpCodeLevel3').parents('.form-group').toggle(selectedValue == '3');
    },
    userManagementHideResetPasswordButton: function ($element, response) {
        $element.toggle(!response.isSuccess);
    },
    applicationSettingsRefreshButtonsOrder: function () {
        // TODO: refresh order buttons 
    },
    getSelectedServicesIds: function ($btn) {
        var obj = [];
        var counter = 0;

        obj.push({ name: 'AppointmentPresentationId', value: $('#AppointmentPresentationId').val() });
        obj.push({ name: 'DealerCustomerId', value: $('#CustomerInfo_CustomerInfo_Id').val() });
        obj.push({ name: 'CarId', value: $('#MenuCarId').val() });
        obj.push({ name: 'Mileage', value: $('#MenuMileage').val() });
        obj.push({ name: 'EnteredMileage', value: $('#EnteredMileage').val() });
        obj.push({ name: 'Invoice', value: $('#Invoice').val() });
        obj.push({ name: 'Transmission', value: $('#Transmission').val() });
        obj.push({ name: 'Driveline', value: $('#Driveline').val() });
        obj.push({ name: 'VIN', value: $('#VIN').val() });
        if ($btn.data()["requestLofMenu"]) {
            obj.push({ name: 'IsLof', value: true });
        }
        else {
            obj.push({ name: 'IsLof', value: false });
        }

        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            if (hash[0] == "inspectionId") {
                obj.push({ name: 'InspectionId', value: hash[1] });
                break;
            }
        }
        var closestMaintenanceContainer = $btn.closest('.maintenance-service-container');
        if (closestMaintenanceContainer) {
            var menulevel = closestMaintenanceContainer.data('level');
            if (menulevel) {
                obj.push({
                    name: 'MenuLevel', value: menulevel
                });
            }
            else {
                $('#maintenanceServicesContainer .iconradio').each(function (i, element) {
                    if (element.className.indexOf('active') !== -1) {
                        obj.push({ name: 'MenuLevel', value: element.id.substring(5, 6) });
                    }
                });
            }
        }

        if ($('.services #MenuLevel').val()) {
            obj.push({ name: 'MenuLevel', value: $('.services #MenuLevel').val() });
        }

        var $parentContainers = $btn.parents('.maintenance-service-container').find('tr');
        if (!$parentContainers.length) {
            $parentContainers = $btn.parents('.lof-service-container').find('div.services');
        }
        var $allServices = $btn.parents('.tab-content').find('.maintenance-service-container');
        $allServices.each(function (i, el) {
            var $container = $(el);
            var $serviceContainers = $container.find('tr');
            $serviceContainers.each(function (i, el) {
                var $row = $(el);
                var id = $('#Id', $row).val();
                if (id) {
                    obj.push({ name: 'AllServices[' + counter + '].Id', value: id });
                    obj.push({ name: 'AllServices[' + counter + '].IsStrikeOut', value: $('#IsStrikeOut', $row).val() || false });
                    obj.push({ name: 'AllServices[' + counter + '].IsAddedAfter', value: $('#IsAddedAfter', $row).val() });
                    obj.push({ name: 'AllServices[' + counter + '].ServiceTypeId', value: $('#ServiceTypeId', $row).val() });
                    obj.push({ name: 'AllServices[' + counter + '].MenuLevel', value: $('#MenuLevel', $row).val() });
                    obj.push({ name: 'AllServices[' + counter + '].IsAddedNow', value: $('#IsAddedNow', $row).val() != undefined ? $('#IsAddedNow', $row).val() : false });
                    obj.push({ name: 'AllServices[' + counter + '].AppDescription', value: $('#DescriptionId', $row).text() });
                    obj.push({ name: 'AllServices[' + counter + '].AppPrice', value: smartvma.controls.parseDouble($('#AppPrice', $row).val()) });
                    obj.push({ name: 'AllServices[' + counter + '].AppLaborHours', value: $('#LaborHours', $row).val() });
                    obj.push({ name: 'AllServices[' + counter + '].Counter', value: counter });
                    obj.push({ name: 'AllServices[' + counter + '].OpCode', value: $('#OpCode', $row).val() });
                    obj.push({ name: 'AllServices[' + counter + '].PartsPrice', value: $('#PartsPrice', $row).val() });
                    obj.push({ name: 'AllServices[' + counter + '].LaborPrice', value: $('#LaborPrice', $row).val() });
                    obj.push({ name: 'AllServices[' + counter + '].LaborRate', value: $('#LaborRate', $row).val() });
                    counter += 1;
                }
            });
        });

        counter = 0;
        var ids = [];
        $parentContainers.each(function (i, el) {
            var $row = $(el);
            var id = $('#Id', $row).val();

            if (id && $.inArray(id, ids) == -1) {
                ids.push(id);
                obj.push({ name: 'services[' + counter + '].Id', value: id });
                obj.push({
                    name: 'services[' + counter + '].IsStrikeOut',
                    value: $('#IsStrikeOut', $row).val() || false
                });
                obj.push({ name: 'services[' + counter + '].IsAddedAfter', value: $('#IsAddedAfter', $row).val() });
                obj.push({ name: 'services[' + counter + '].ServiceTypeId', value: $('#ServiceTypeId', $row).val() });
                obj.push({ name: 'services[' + counter + '].MenuLevel', value: $('#MenuLevel', $row).val() });
                obj.push({ name: 'services[' + counter + '].IsAddedNow', value: $('#IsAddedNow', $row).val() != undefined ? $('#IsAddedNow', $row).val() : false });
                if ($('#IsAddedNow', $row).val() == 'true') {
                    obj.push({
                        name: 'services[' + counter + '].AppDescription',
                        value: $('#DescriptionId', $row).text()
                    });
                    obj.push({ name: 'services[' + counter + '].AppPrice', value: smartvma.controls.parseDouble($('#AppPrice', $row).val()) });
                }
                obj.push({ name: 'services[' + counter + '].AppLaborHours', value: $('#LaborHours', $row).val() });
                obj.push({ name: 'services[' + counter + '].Counter', value: counter });
                obj.push({ name: 'services[' + counter + '].OpCode', value: $('#OpCode', $row).val() });
                obj.push({ name: 'services[' + counter + '].PartsPrice', value: $('#PartsPrice', $row).val() });
                obj.push({ name: 'services[' + counter + '].LaborPrice', value: $('#LaborPrice', $row).val() });
                obj.push({ name: 'services[' + counter + '].LaborRate', value: $('#LaborRate', $row).val() });
                counter += 1;
            }
        });
        return obj;
    },
    storeActiveTab: function (activetabId) {
        var dataToSend = {
            activeTabId: activetabId
        };
        $.ajax({
            url: '/Dealer/AppointmentPresentation/StoreActiveTab',
            data: dataToSend,
            method: 'POST'
        });
    },
    refreshImageUrl: function ($btn) {
        //$.ajax({
        //    url: '/Admin/Accounts/GetUserImage',
        //    type: 'GET',
        //    //data: { id: id },
        //    success: function (partialView) {
        //        //var $thumbnail = $('.thumbnail img');
        //        //if ($thumbnail) {
        //        //    //$thumbnail.attr('src', data.result.thumbnail);
        //        //    $thumbnail.attr('src', '/Admin/Accounts/GetUserImage');
        //        //    $thumbnail.removeClass('hidden');
        //        //}
        //    }
        //});
    },
    validateStrike: function ($btn) {
        smartvma.modals.close();
        $('#maintenanceServicesContainer tr[data-service-id="' + $btn.attr('serviceId') + '"]')
            .each(function (i, tRow) {
                var $tRow = $(tRow);
                var price;
                if ($('a', $tRow).hasClass('strikethrough-text')) {
                    price = smartvma.controls.parseDouble($tRow.find('.price-container').text());
                    $('#IsStrikeOut', $tRow).val(false);
                    $('a', $tRow).removeClass('strikethrough-text');
                } else {
                    price = smartvma.controls.parseDouble($tRow.find('.price-container').text()) * (-1);
                    $('#IsStrikeOut', $tRow).val(true);
                    $('a', $tRow).addClass('strikethrough-text');
                }
                var $priceContainer = $('#' +
                    $tRow.parents('.maintenance-service-container').attr('id').replace('Services', 'Btn'));
                smartvma.menuPresentation.updateMenuPrice(price, $priceContainer);
            });
    },
    getVehicleTypesFilterData: function (filterBy) {
        smartvma.controls.showLoader();
        var elementId = $(filterBy).attr('id');
        switch (elementId) {
            case 'Years':
                smartvma.multiselect.destroyData($('#Makes'));
            case 'Makes':
                smartvma.multiselect.destroyData($('#Models'));
            case 'Models':
                smartvma.multiselect.destroyData($('#EngineTypes'));
            case 'EngineTypes':
                smartvma.multiselect.destroyData($('#TransmissionTypes'));
            case 'TransmissionTypes':
                smartvma.multiselect.destroyData($('#DriveLines'));
        }

        var data = {
            Years: $('#Years').val(),
            Makes: $('#Makes').val(),
            Models: $('#Models').val(),
            EngineTypes: $('#EngineTypes').val(),
            TransmissionTypes: $('#TransmissionTypes').val(),
            DriveLines: $('#DriveLines').val(),
            PreserveNA: $('#PreserveNA').val(),
            VIN: $('#VIN').val()
        };
        return data;
    },
    disableIfSelected: function (filterBy) {
        $('#CarIdUndetermined').val(1);
        var years = $('#Years').val();
        if (years != null) {
            smartvma.multiselect.disable($('#Years'));
            $('#alertYears').addClass('hidden');
        }
        else {
            $('#alertYears').removeClass('hidden');
        }
        var makes = $('#Makes').val();
        if (makes != null) {
            smartvma.multiselect.disable($('#Makes'));
            $('#alertMakes').addClass('hidden');
        }
        else {
            $('#alertMakes').removeClass('hidden');
        }
        var models = $('#Models').val();
        if (models != null) {
            smartvma.multiselect.disable($('#Models'));
            $('#alertModels').addClass('hidden');
        }
        else {
            $('#alertModels').removeClass('hidden');
        }
        var engineTypes = $('#EngineTypes').val();
        if (engineTypes != null) {
            smartvma.multiselect.disable($('#EngineTypes'));
            $('#alertEngines').addClass('hidden');
        }
        else {
            $('#alertEngines').removeClass('hidden');
        }
        var transmissionTypes = $('#TransmissionTypes').val();
        if (transmissionTypes != null) {
            smartvma.multiselect.disable($('#TransmissionTypes'));
            $('#alertTransmissions').addClass('hidden');
        }
        else {
            $('#alertTransmissions').removeClass('hidden');
        }
        var driveLines = $('#DriveLines').val();
        if (driveLines != null) {
            smartvma.multiselect.disable($('#DriveLines'));
            $('#alertDrivelines').addClass('hidden');
        }
        else {
            $('#alertDrivelines').removeClass('hidden');
        }

        switch ($(filterBy).attr('id')) {
            case 'Years':
                smartvma.multiselect.enable($('#Years'));
            case 'Makes':
                smartvma.multiselect.enable($('#Makes'));
            case 'Models':
                smartvma.multiselect.enable($('#Models'));
            case 'EngineTypes':
                smartvma.multiselect.enable($('#EngineTypes'));
            case 'TransmissionTypes':
                smartvma.multiselect.enable($('#TransmissionTypes'));
            case 'DriveLines':
                smartvma.multiselect.enable($('#DriveLines'));
        }
        if (years != null && makes != null && models != null && engineTypes != null && transmissionTypes != null && driveLines != null) {
            $('#refreshServices').removeClass('custom-disabled');
            $('#Transmission').val(transmissionTypes);
            $('#Driveline').val(driveLines);
            $.ajax({
                url: '/Dealer/MenuPresentation/GetCarIdByFilters',
                type: 'POST',
                data: { year: years[0], make: makes[0], model: models[0], engine: engineTypes[0], transmission: transmissionTypes[0], driveline: driveLines[0] },
                success: function (data) {
                    $('#MenuCarId').val(data.carId);
                    $('#CarIdUndetermined').val(0);
                    var inspectionId = $('#InspectionId').val();
                    if (inspectionId == null || inspectionId == '') {
                        $('#InspectionId').val(0);
                    }
                    var lofUrl = '/Dealer/LOF?mileage=[#CustomerInfo_Mileage]&customerId=[#CustomerInfo_CustomerInfo_Id]&carId=[#MenuCarId]&appointmentPresentationId=[#AppointmentPresentationId]&inspectionId=[#InspectionId]&transmission=[#Transmission]&driveLine=[#Driveline]&enteredMileage=[#EnteredMileage]&dealerVehicleId=[#CustomerInfo_CustomerInfo_DealerVehicleId]&vin=[#CustomerInfo_CustomerInfo_VIN]&CarIdUndetermined=[#CarIdUndetermined]';
                    var inspectionUrl = '/Dealer/Inspection?mileage=[#CustomerInfo_Mileage]&customerId=[#CustomerInfo_CustomerInfo_Id]&carId=[#MenuCarId]&appointmentPresentationId=[#AppointmentPresentationId]&inspectionId=[#InspectionId]&transmission=[#Transmission]&driveLine=[#Driveline]&enteredMileage=[#EnteredMileage]&dealerVehicleId=[#CustomerInfo_CustomerInfo_DealerVehicleId]&vin=[#CustomerInfo_CustomerInfo_VIN]&CarIdUndetermined=[#CarIdUndetermined]';
                    var menuPresentationUrl = '/Dealer/MenuPresentation?mileage=[#CustomerInfo_Mileage]&customerId=[#CustomerInfo_CustomerInfo_Id]&carId=[#MenuCarId]&appointmentPresentationId=[#AppointmentPresentationId]&inspectionId=[#InspectionId]&transmission=[#Transmission]&driveLine=[#Driveline]&enteredMileage=[#EnteredMileage]&dealerVehicleId=[#CustomerInfo_CustomerInfo_DealerVehicleId]&vin=[#CustomerInfo_CustomerInfo_VIN]&CarIdUndetermined=[#CarIdUndetermined]';
                    $('#Lof').attr('href', smartvma.controls.mapUrl(lofUrl));
                    $('#InspectionBtn').attr('href', smartvma.controls.mapUrl(inspectionUrl));
                    $('#MenuPresentation').attr('href', smartvma.controls.mapUrl(menuPresentationUrl));
                }
            });
        }
        else {
            $('#refreshServices').addClass('custom-disabled');
        }
    },
    hideAlertIconForVehicleInfo: function (filterBy) {
        var vinSearch = $('#VIN').val();
        if (vinSearch && vinSearch != null && vinSearch != '') {
            var years = $('#Years').val();
            if (years != null) {
                $('#alertYears').addClass('hidden');
            }
            else {
                $('#alertYears').removeClass('hidden');
            }
            var makes = $('#Makes').val();
            if (makes != null) {
                $('#alertMakes').addClass('hidden');
            }
            else {
                $('#alertMakes').removeClass('hidden');
            }
            var models = $('#Models').val();
            if (models != null) {
                $('#alertModels').addClass('hidden');
            }
            else {
                $('#alertModels').removeClass('hidden');
            }
            var engineTypes = $('#EngineTypes').val();
            if (engineTypes != null) {
                $('#alertEngines').addClass('hidden');
            }
            else {
                $('#alertEngines').removeClass('hidden');
            }
            var transmissionTypes = $('#TransmissionTypes').val();
            if (transmissionTypes != null) {
                $('#alertTransmissions').addClass('hidden');
            }
            else {
                $('#alertTransmissions').removeClass('hidden');
            }
            var driveLines = $('#DriveLines').val();
            if (driveLines != null) {
                $('#alertDrivelines').addClass('hidden');
            }
            else {
                $('#alertDrivelines').removeClass('hidden');
            }

            var secondarySearch = $('#VehicleFilterResponseModel_IsFromSecondarySearch').val();
            if (secondarySearch && secondarySearch == 'True') {
                if (years != null && makes != null && models != null && engineTypes != null && transmissionTypes != null && driveLines != null) {
                    $('#msgForSecondarySearch').removeClass('hidden');
                }
                else {
                    $('#msgForSecondarySearch').addClass('hidden');
                }
            }
        }
    },
    reInitVehicleYears: function () {
        $.ajax({
            url: "/Dealer/AppointmentPresentation/SecondarySearchVehicleLookup?secondarySearchVin=",
            method: 'POST',
            success: function (data) {
                var htmlContainer = "#searchByVinVehicleLookup";
                if (htmlContainer) {
                    $(htmlContainer).html(data);
                    smartvma.controls.init();
                }
            }
        });
    },
    refreshLayout: function () {
        window.location.reload();
    },
    lofMenuLevelsImageSelected: function ($element) {
        var selectedImgs = $('.lof-table-menu .custom-checkbox:checked').length;
        if (selectedImgs >= 7) {
            $('.lof-table-menu .custom-checkbox:not(:checked)').attr('disabled', true);
            $('.messagePopUp').click();
        }
        else {
            $('.lof-table-menu .custom-checkbox:not(:checked)').attr('disabled', false);
        }

        var id = $element[0].id;
        var imageName = $element[0].defaultValue;
        var isChecked = $element[0].checked;
        var selectedLevel = $('#LOFMenuSelectedLevel option:selected').val();
        $.ajax({
            url: '/Admin/LOFMenu/LOFMenuImageSelected',
            dataType: 'html',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({
                imageID: id,
                imageName: imageName,
                isSelected: isChecked,
                selectedLOFMenuLevel: selectedLevel
            })
        });
    },
    getBgProductsProtectionPlansFilterData: function ($element) {
        var id = $element[0].id;
        // slected category ids as one string in -> Admin/BGProducts/Index/--string of selected id separated by ,

    },
    setTextMessage: function (label, cssClass) {
        if ($(label).length) {
            $(label).html($('#Message').val()).addClass(cssClass);
        }
    },
    handleAjaxError: function (e, request) {
        if (request.status === 308) {
            location.href = '/Admin/Accounts/Login';
        } else {
            console.error(e, request);
        }
    },

    refreshServices: function () {
        if ($('.maintenance-service-table tbody>tr:last').length === 0) {
            $('.newadditionalservice').addClass('disabled');
        } else {
            $('.newadditionalservice').removeClass('disabled');
        }
        initTooltips();

    },
    updateDefaultDistributorProfileSetupValues: function ($element) {
        var selectedValue = $element.val();
        var distributorExists = $('#Id').val() != "";
        if (!distributorExists && selectedValue != null) {
            $.ajax({
                url: "/Admin/Distributors/UpdateDefaultDistributorValues?corpId=" + selectedValue,
                method: 'POST',
                success: function (data) {
                    var htmlContainer = "#distributorProfileSetup";
                    if (htmlContainer) {
                        $(htmlContainer).html(data);
                        smartvma.controls.init();
                    }
                }
            });
        }
    },
    updateDefaultDealerProfileSetupValues: function ($element) {
        var selectedValue = $element.val();
        var dealerExists = $('#Id').val() != "";
        if (!dealerExists && selectedValue != null) {
            $.ajax({
                url: "/Admin/Dealers/UpdateDefaultDealerValues?distributorId=" + selectedValue,
                method: 'POST',
                success: function (data) {
                    var htmlContainer = "#dealerProfileSetup";
                    if (htmlContainer) {
                        $(htmlContainer).html(data);
                        smartvma.controls.init();
                    }
                }
            });
        }
    },
    refreshAdvisorCodes: function () {
        if ($('#Tenants :selected').length == 0) {
            $('.user-role-wrapper').html('')
        }
        else {
            var roleCount = $(".user-role-wrapper .user-role").length;
            var companyIds = [];
            $('#Tenants :selected').each(function () {
                var companyId = $(this).val();
                var companyName = $(this).text();
                companyIds.push(companyId);
                if ($(".user-role-wrapper #" + companyId).length == 0) {
                    var html = addAdvisorCodeForCompanyDiv(companyId, companyName, roleCount);
                    $(".user-role-wrapper").append(html);
                    roleCount++;
                }
            });
            var nr = 0;
            var hasDeleted = false;
            $('.user-role-wrapper .user-role').each(function () {
                var currentId = $(this).attr('id');
                if ($.inArray(currentId, companyIds) == -1) {
                    $(this).remove();
                    hasDeleted = true;
                }
                else if (hasDeleted) {
                    changeAdvisorCodeCompanyProperties(nr);
                }
                nr++;
            });
        }
    }
};
// methods - end

// menu presentation
smartvma.menuPresentation = {
    updateMenuPrice: function (price, $parentScope) {
        $('.menu-price', $parentScope || $('#maintenanceServicesContainer')).each(function (i, el) {
            var $el = $(el);
            var oldPrice = smartvma.controls.parseDouble($el.text());
            var newPrice = price + oldPrice;
            $el.text(newPrice.formatMoney());
        });
    }
};

$(document).on('click', '.add-additional-maintenance-service', function () {
    var $row = $(this).parents('tr');
    var $newRow = $row.clone();
    $('#IsAddedAfter', $newRow).val(true);
    $('#IsAddedNow', $newRow).val(true);
    $newRow.find('a.service-btn').removeClass('add-additional-maintenance-service').addClass('remove-additional-maintenance-service').prepend('+ ');
    $('#MenuLevel', $newRow).val(1);
    $newRow.appendTo('#level1Services table tbody');
    var $newRowLevel2 = $newRow.clone();
    $newRowLevel2.appendTo('#level2Services table tbody');
    var $newRowLevel3 = $newRow.clone();
    $newRowLevel3.appendTo('#level3Services table tbody');
    var $newRowLevel2Preview = $newRow.clone();
    $newRowLevel2Preview.addClass('hidden');
    $newRowLevel2Preview.appendTo('#level2ServicesPreview table tbody');
    var $newRowLevel3Preview = $newRow.clone();
    $newRowLevel3Preview.addClass('hidden');
    $newRowLevel3Preview.appendTo('#level3ServicesPreview table tbody');

    $row.find('a.service-btn').toggleClass('add-additional-maintenance-service').toggleClass('disabled');



    $('.maintenance-service-container table').removeClass('hidden');
    $('.maintenance-service-container .no-serviceItems').addClass('hidden');

    var price = smartvma.controls.parseDouble($('#PriceId', $row).text());
    smartvma.menuPresentation.updateMenuPrice(price);
});

$(document).on('click', '.remove-additional-maintenance-service', function () {
    var $row = $(this).parents('tr');
    var serviceId = $row.data('service-id');
    var serviceTypeId = $('#ServiceTypeId', $row).val();
    if ($('#additionalServicesContainer tbody').find('[data-service-id="' + serviceId + '"]').length == 0) {
        if (serviceTypeId != "4") {
            $row.appendTo('#additionalServicesContainer table tbody');
            $row.find('.service-btn').text($row.find('.service-btn').text().replace('+ ', ''));
            $('#additionalServicesContainer tbody').find('[data-service-id="' + serviceId + '"] td  a').removeClass('remove-additional-maintenance-service');
            $('#additionalServicesContainer tbody').find('[data-service-id="' + serviceId + '"] td  a').toggleClass('add-additional-maintenance-service');
        }
    }
    else {
        if ($('#additionalServicesContainer tbody').find('[data-service-id="' + serviceId + '"] td  a').hasClass('add-additional-maintenance-service')) {
            $('#additionalServicesContainer tbody').find('[data-service-id="' + serviceId + '"] td  a').toggleClass('disabled');
        }
        else {
            $('#additionalServicesContainer tbody').find('[data-service-id="' + serviceId + '"] td  a').toggleClass('add-additional-maintenance-service').toggleClass('disabled');
        }
    }

    $('#additionalServicesContainer tbody #BgProtectionPlanId').removeClass("add-additional-maintenance-service disabled");

    $('.maintenance-service-container table:first tbody tr[data-service-id="' + serviceId + '"]').each(function (i, rowEl) {
        var $rowEl = $(rowEl);
        var price = smartvma.controls.parseDouble($('#PriceId', $rowEl).text()) * (-1);
        smartvma.menuPresentation.updateMenuPrice(price);
    });

    $('.maintenance-service-container table tbody tr[data-service-id="' + serviceId + '"]').each(function (i, rowEl) {
        var $rowEl = $(rowEl);
        if ($rowEl.siblings().length === 0) {
            $rowEl.parents('table').addClass('hidden');
            $rowEl.parents('.maintenance-service-container').find('.no-serviceItems').removeClass('hidden');
        }
        $rowEl.remove();
    });
});

$(document).on('click', '.strikeout-maintenance-service.can-be-striked', function () {
    var $row = $(this).parents('tr');
    var serviceId = $('#Id', $row).val();

    if ($row.find('.iconlpp').length !== 0 && $row.find('.strikethrough-text').length == 0) {
        smartvma.modals.open('/Dealer/MenuPresentation/ValidateStrike' + '?serviceId=' + serviceId);
    }

    $('#maintenanceServicesContainer tr[data-service-id="' + serviceId + '"]').each(function (i, tRow) {
        var $tRow = $(tRow);
        var price = 0;
        if ($('a', $tRow).hasClass('strikethrough-text')) {
            price = smartvma.controls.parseDouble($('#PriceId', $tRow).text());
            $('#IsStrikeOut', $tRow).val(false);
            $('a', $tRow).removeClass('strikethrough-text');
        }
        else {
            if ($tRow.find('.iconlpp').length !== 0) {
                //smartvma.modals.open('/Dealer/MenuPresentation/ValidateStrike' + '?serviceId=' + serviceId);
            }
            else {
                price = smartvma.controls.parseDouble($('#PriceId', $tRow).text()) * (-1);
                $('#IsStrikeOut', $tRow).val(true);
                $('a', $tRow).addClass('strikethrough-text');
            }
        }
        var $priceContainer = $('#' + $tRow.parents('.maintenance-service-container').attr('id').replace('Services', 'Btn'));
        smartvma.menuPresentation.updateMenuPrice(price, $priceContainer);
    });

});

$(document).on('click', '.add-addhoc-maintenance-service', function () {
    if ($('.newAdditionalService #Description').val().length !== 0 && $('#Price').val().length !== 0 && $('.modal-form-row #LaborHours').val().length !== 0) {
        $('.no-serviceitems').remove();

        $('table.table-condensed').each(function (i, item) {
            $(item).removeClass('hidden');
        });

        var $row = $('.maintenance-service-table tbody>tr:last');//.prev('tr');
        var $newRow = $row.clone();

        var id = ($('[id="ServiceTypeId"][value="4"]:last').parents('tr').data('service-id') + 1 || 1000000);
        $newRow.addClass('appointmentServices');
        $newRow.attr('data-service-id', id);
        $('#ServiceTypeId', $newRow).val(4);//4 means AdHocAdditionalService 
        $newRow.find('.service-btn').text('+ ' + $('.newAdditionalService #Description').val());

        var price = smartvma.controls.parseDouble($('#Price').val());
        $('#Description', $newRow).val($('.newAdditionalService #Description').val());
        $('#AppPrice', $newRow).val(price);
        $('#LaborHours', $newRow).val($('.modal-content #LaborHours').val());
        $('#Id', $newRow).val(id);
        $('#IsAddedAfter', $newRow).val(true);
        $('#IsAddedNow', $newRow).val(true);
        $('#PartsPrice', $newRow).val(0);
        $('#LaborPrice', $newRow).val(price);
        $('#OpCode', $newRow).val('');

        $newRow.find('.service-btn').removeClass('add-additional-maintenance-service strikeout-text strikethrough-text');
        $newRow.find('.service-btn').removeClass('disabled');
        $newRow.find('.service-btn').addClass('remove-additional-maintenance-service');
        $newRow.find('input#IsStrikeOut').val('false');
        $newRow.find('.price-container').text(price.formatMoney());
        $newRow.find('a#BgProtectionPlanId').remove();
        $newRow.find('.icon-prev-decline').remove();
        $newRow.find('.icon-prev-serv').remove();

        smartvma.menuPresentation.updateMenuPrice(price);

        $('#MenuLevel', $newRow).val(1);
        $newRow.appendTo('#level1Services table tbody');
        var $newRowLevel2 = $newRow.clone();
        $newRowLevel2.appendTo('#level2Services table tbody');
        var $newRowLevel3 = $newRow.clone();
        $newRowLevel3.appendTo('#level3Services table tbody');
        var $newRowLevel2Preview = $newRow.clone();
        $newRowLevel2Preview.addClass('hidden');
        $newRowLevel2Preview.appendTo('#level2ServicesPreview table tbody');
        $newRowLevel3Preview = $newRow.clone();
        $newRowLevel3Preview.addClass('hidden');
        $newRowLevel3Preview.appendTo('#level3ServicesPreview table tbody');
    }
});

smartvma.callbacks.setMenuMileage = function ($form, data) {
    if (data.isSuccess) {
        $('#MenuMileage').val(data.model.mileage);
        $('#EnteredMileage').val(data.model.enteredMileage);
        smartvma.multiselect.select($('#CustomerInfo_Mileage'), data.model.mileage);
        if ($('#MenuMileage').val() != null && $('#MenuMileage').val() != "0" && $('#MenuMileage').val() != "" && $('#EnteredMileage').val() != null && $('#EnteredMileage').val() != "0" && $('#EnteredMileage').val() != "") {
            var maxMileage = 0;
            $('#CustomerInfo_Mileage option').each(function (i, item) {
                var optionValue = parseInt(item.value);
                if (maxMileage < optionValue) {
                    maxMileage = optionValue;
                }
            });
            if ($('#EnteredMileage').val() > maxMileage && maxMileage > 0 && $('#MenuMileage').val() == maxMileage) {
                smartvma.modals.open('/Dealer/MenuPresentation/ValidateMaxMileage');
            }
        }

    }
};
smartvma.callbacks.updateCustomerMenuMileage = function () {
    var enteredMileage = $("#EnteredMileage");
    var mileageAmmount = $("#lblMileageAmmount");
    if (enteredMileage.val() !== mileageAmmount.text()) {
        mileageAmmount.text(enteredMileage.val());
    }
};
smartvma.callbacks.updateMenuMileage = function () {
    smartvma.callbacks.updateCustomerMenuMileage();
    var mileage = $('#CustomerInfo_Mileage').val();
    $('#MenuMileage').val(mileage);
    if (mileage) {
        var inspectionId = $('#InspectionId').val();
        if (inspectionId == null || inspectionId == '') {
            $('#InspectionId').val(0);
        }
        var lofUrl = '/Dealer/LOF?mileage=[#CustomerInfo_Mileage]&customerId=[#CustomerInfo_CustomerInfo_Id]&carId=[#MenuCarId]&appointmentPresentationId=[#AppointmentPresentationId]&inspectionId=[#InspectionId]&transmission=[#Transmission]&driveLine=[#Driveline]&enteredMileage=[#EnteredMileage]&dealerVehicleId=[#CustomerInfo_CustomerInfo_DealerVehicleId]&vin=[#CustomerInfo_CustomerInfo_VIN]&CarIdUndetermined=[#CarIdUndetermined]';
        var inspectionUrl = '/Dealer/Inspection?mileage=[#CustomerInfo_Mileage]&customerId=[#CustomerInfo_CustomerInfo_Id]&carId=[#MenuCarId]&appointmentPresentationId=[#AppointmentPresentationId]&inspectionId=[#InspectionId]&transmission=[#Transmission]&driveLine=[#Driveline]&enteredMileage=[#EnteredMileage]&dealerVehicleId=[#CustomerInfo_CustomerInfo_DealerVehicleId]&vin=[#CustomerInfo_CustomerInfo_VIN]&CarIdUndetermined=[#CarIdUndetermined]';
        var menuPresentationUrl = '/Dealer/MenuPresentation?mileage=[#CustomerInfo_Mileage]&customerId=[#CustomerInfo_CustomerInfo_Id]&carId=[#MenuCarId]&appointmentPresentationId=[#AppointmentPresentationId]&inspectionId=[#InspectionId]&transmission=[#Transmission]&driveLine=[#Driveline]&enteredMileage=[#EnteredMileage]&dealerVehicleId=[#CustomerInfo_CustomerInfo_DealerVehicleId]&vin=[#CustomerInfo_CustomerInfo_VIN]&CarIdUndetermined=[#CarIdUndetermined]';
        $('#Lof').attr('href', smartvma.controls.mapUrl(lofUrl));
        $('#InspectionBtn').attr('href', smartvma.controls.mapUrl(inspectionUrl));
        $('#MenuPresentation').attr('href', smartvma.controls.mapUrl(menuPresentationUrl));
        $('.icon-mileage').next('span').html($('#EnteredMileage').val());
    }

    smartvma.callbacks.refreshServices();

};

// menu presentation - end

// modals
$(document).on('click', '.btn-modal', function (event) {
    event.preventDefault();
    var $btn = $(this);
    if ($btn.hasClass('custom-disabled')) {
        return;
    }
    var requestData = null;
    var requestDataFunctionName = $btn.data('request-data-function');
    if (requestDataFunctionName) {
        requestData = smartvma.controls.executeFunctionByName('smartvma.callbacks.' + requestDataFunctionName, window, $btn);
    }
    if (null != $btn.data('modal-title')) {
        smartvma.modals.open($btn.attr('href') || $btn.data('url'), $btn.data('modal-title'), requestData, $btn.data('request-type'));
    }
    else {
        smartvma.modals.open($btn.attr('href') || $btn.data('url'), $btn.attr('title'), requestData, $btn.data('request-type'));
    }
});
// modals - end

// form submition via ajax
$(document).on('click', '.btn-modal-submit', function () {
    var $element = $(this);
    var $form = $element.parents('.modal-content').find('form:first');
    $form.trigger('submit', { button: $element });
});

$(document).on('click', '.showloader', function () {
    //smartvma.controls.showLoader();
    //var $element = $(this);
    //var $form = $element.parents('.modal-content').find('form:first');
    //$form.trigger('submit', { button: $element });
});

$(document).on('submit', '.modal-content form,form.ajax-form', function (event, extraParameters) {
    event.preventDefault();
    smartvma.controls.showLoader();
    var $form = $(this);

    var url = $form.attr('action');
    var formData = $form.serialize();
    if (extraParameters && extraParameters.button) {
        var $submitButton = $(extraParameters.button);
        var actionName = $submitButton.data('action');
        if (actionName) {
            url += '/' + actionName;
        }

        var btnName = $submitButton.attr('name');
        if (btnName) {
            formData += '&' + btnName + '=' + $submitButton.attr('value');
        }
    }


    $form.removeData('validator').removeData('unobtrusiveValidator').removeData('previousValue');
    $.validator.unobtrusive.parse($form);
    if ($form.valid()) {
        smartvma.controls.showLoader();
        $.ajax({
            url: url,
            method: $form.attr('method'),
            data: formData,
            success: function (data) {
                if (typeof (data) === 'object') {
                    if (data.isSuccess) {
                        if (!data.preventModalFromClosing) {
                            $('.modal:last').modal('hide');
                            smartvma.controls.refreshContainer($form.data('reload-on-success') || '#contents');
                        }
                        smartvma.controls.executeFunctionByName($form.data('on-success-func'), window, $form, data);
                        if (data.redirectUrl) {
                            window.location.href = data.redirectUrl;
                        }
                    }
                    else {
                        if (data.redirectUrl) {
                            smartvma.controls.showLoader();
                            $.ajax({
                                url: data.redirectUrl,
                                method: $form.attr('method'),
                                success: function (data) {
                                    var htmlContainer = $form.data('no-data-handling-container');
                                    if (htmlContainer) {
                                        $(htmlContainer).html(data);
                                        smartvma.controls.init();
                                        var resetContainer = $form.data('container');
                                        if (resetContainer) {
                                            $(resetContainer).html(null);
                                        }
                                    }
                                    smartvma.controls.hideLoader();
                                }
                            });
                        }
                    }
                    // TODO: add code for error handling, if the response is json
                } else {
                    var htmlContainer = $form.data('container');
                    if (htmlContainer) {
                        $(htmlContainer).html(data);
                        smartvma.controls.init();
                        var resetNoDataContainer = $form.data('no-data-handling-container');
                        if (resetNoDataContainer) {
                            var resetButton = $form.data('reset-button');
                            if (resetButton) {
                                $(resetButton).click();
                            }
                        }
                    } else {
                        smartvma.modals.refreshLastModal(data);
                    }
                    smartvma.controls.hideLoader();
                }
            }
        });
    }
    else {
        smartvma.controls.hideLoader();
    }
});
// form submition via ajax - end

// dom manipulations

$(document).on('click', '[data-toggle]', function (event) {
    if (!$(this).is('button[type="submit"], input[type="submit"]')) {
        event.preventDefault();
    }
    $($(this).data('toggle')).toggle();
});

$(document).on('change', '[data-refresh-element-data].singleoption', function () {
    var ajaxRequests = [];
    $($(this).data('refresh-element-data')).each(function () {
        var $refreshElement = $(this);
        var ajaxOption = {
            elementId: '#' + $refreshElement.attr('id'),
            url: smartvma.controls.mapUrl($refreshElement.data('remote-source-url')),
            method: $refreshElement.data('request-type') || 'GET'
        };

        var requestDataFunctionName = $refreshElement.data('request-data-function');
        if (requestDataFunctionName) {
            ajaxOption.data = smartvma.controls.executeFunctionByName(requestDataFunctionName, window, $(this));
        }
        ajaxRequests.push(ajaxOption);
    });

    $.each(ajaxRequests, function (idx, option) {
        option.success = function (response) {
            smartvma.multiselect.resetData($(option.elementId), response);
            var resetCompleteFunctionName = $(option.elementId).data('reset-complete-data-function');
            if (resetCompleteFunctionName) {
                smartvma.controls.executeFunctionByName(resetCompleteFunctionName, window, $(option.elementId));
            }
        };
        $.ajax(option);
    });
});

$(document).on('change', '[data-on-change-function]', function () {
    var functionName = $(this).data('on-change-function');
    smartvma.controls.executeFunctionByName(functionName, window, $(this));
});

$(document).on('click', '.btn-refresh-container', function () {
    smartvma.controls.refreshContainer($(this).data('container'));
});

$(document).on('click', 'input:radio.toggle-element-state', function () {
    var $element = $(this);
    var selector = $element.data('toggle-element-state');
    var dependentElementsSelector = $element.data('dependent-toggle-element');
    var isDependentElementChecked = true;

    if (dependentElementsSelector) {
        $(dependentElementsSelector).each(function (i, el) {
            if (!smartvma.controls.parseBool($(el).val())) {
                isDependentElementChecked = false;
                return;
            }
        });

        if (isDependentElementChecked || smartvma.controls.parseBool($element.val())) {
            $(selector).removeAttr('disabled');
        } else {
            $(selector).attr('disabled', 'disabled');
        }
    } else {
        if (smartvma.controls.parseBool($element.val())) {
            $(selector).removeAttr('disabled');
        } else {
            $(selector).attr('disabled', 'disabled');
        }
    }
});


$(document).on('click', '.toggle-submenu', function (e) {
    e.stopPropagation();
    var $element = $(this);

    var $windowW = $(window).width();
    //console.log('$windowW' + $windowW);
    //console.log("matchmedia " + window.matchMedia('(max-width: 1279px)').matches)
    var $li = $element.closest('li');
    var $target = $li.find('.submenu');

    if ($target.hasClass('open')) {
        $target.slideUp(function () {
            $target.removeClass('open').removeAttr('style');
        });
    } else {
        //  if ($windowW <= 1279) {
        if (window.matchMedia('(max-width: 1279px)').matches) {

            $('body').addClass('toggled');
        } else {
            $('body').removeClass('toggled');
        }
        $target.slideDown(function () {
            $target.addClass('open').removeAttr('style');
        });
    }
    return false;
});

$(document).on('click', '.menu-toggle', function (e) {
    e.stopPropagation();
    var $windowW = $(window).width();
    var $wrap = $('body');
    var $nav = $(this).closest('.sidebar-nav');
    var $target = $nav.find('.submenu');

    if ($wrap.hasClass('toggled')) {
        // if ($target.hasClass('open') && $windowW < 1280) 
        if ($target.hasClass('open') && window.matchMedia('(max-width: 1279px)').matches) {

            $target.removeClass('open').removeAttr('style');
        }
        // if ($windowW >= 1280) {
        if (window.matchMedia('(min-width: 1280px)').matches) {
            $nav.data('desktopOpen', false);

        } else {
            $nav.data('mobileOpen', false);

        }
        $wrap.removeClass('toggled');
    } else {

        //if ($target.hasClass('open') && $windowW >= 1280) {
        if ($target.hasClass('open') && window.matchMedia('(min-width: 1280px)').matches) {

            $target.removeClass('open').removeAttr('style');
        }
        // if ($windowW >= 1280) {
        if (window.matchMedia('(min-width: 1280px)').matches) {
            $nav.data('desktopOpen', true);

        } else {
            $nav.data('mobileOpen', true);

        }
        $wrap.addClass('toggled');
    }
    return false;
});

$(document).on('click', '[data-toggle-class]', function (event) {
    event.preventDefault();
    var $element = $(this);
    var classes = $element.data('toggle-class');
    var toggleClass = function ($el, clasStr) {
        $.each(clasStr.split(','), function (i, el) {
            $el.toggleClass(el);
        });
    };

    if (typeof (classes) === 'string') {
        toggleClass($element, classes);
        return;
    }

    $.each(classes, function (key, value) {
        toggleClass($(key), value);
    });
});


$(document).on('click', '.reset-searchResults', function (e) {
    e.stopPropagation();
    $('#searchResults, #searchAppointmentResults').fadeOut(function () {
        $(this).empty().removeAttr('style');
    });
    return false;
});

$(document).on('click', '[data-add-class]', function () {
    var $element = $(this);
    var classes = $element.data('add-class');
    var addClass = function ($el, clasStr) {
        $.each(clasStr.split(','), function (i, el) {
            $el.addClass(el);
        });
    };
    if (typeof (classes) === 'string') {
        addClass($element, classes);
        return;
    }
    $.each(classes, function (key, value) {
        addClass($(key), value);
    });
});

$(document).on('click', '[data-remove-class]', function () {
    var $element = $(this);
    var classes = $element.data('remove-class');
    var removeClass = function ($el, clasStr) {
        $.each(clasStr.split(','), function (i, el) {
            $el.removeClass(el);
        });
    };
    if (typeof (classes) === 'string') {
        removeClass($element, classes);
        return;
    }
    $.each(classes, function (key, value) {
        removeClass($(key), value);
    });
});

$(document).on('click', '.form-reset-btn', function () {
    var $form = $(this).parents('form');
    var requestDataFunctionName = $(this).data('on-reset-function');
    if (requestDataFunctionName) {
        smartvma.controls.executeFunctionByName('smartvma.callbacks.' + requestDataFunctionName);
    }
    var reInitSelectAll = $(this).hasClass('reInit-selectAll');
    $form.find('select').each(function (i, el) {
        smartvma.multiselect.destroyData($(el));
        if (reInitSelectAll) {
            smartvma.multiselect.selectAll($(el));
        }
    });

    $form.find('input.form-control,textarea').val('');
    $form.find('input:checked').removeAttr('checked');
    $form.find('.input-validation-error').removeClass('input-validation-error');
    $form.find('.field-validation-error').addClass('field-validation-valid');
    $form.find('.field-validation-error').removeClass('field-validation-error');
    $form.find('.validation-summary-errors').addClass('validation-summary-valid');
    $form.find('.validation-summary-errors').removeClass('validation-summary-errors');
});

$(document).on('click', '.custom-disabled', function (e) {
    e.preventDefault();
    e.stopPropagation();
    return;
});

$(document).on('click', '.nav-tabs', function (e) {
    e.preventDefault();
    var $tabs = $(this);
    var activeTabId = $tabs.find('li.active').data('active');

    var requestDataFunctionName = $tabs.data('request-data-function');
    if (requestDataFunctionName) {
        smartvma.controls.executeFunctionByName(requestDataFunctionName, window, activeTabId);
    }
});

$(document).on('click', '.validation-clear', function () {
    var fieldWrapper = $(this).closest('.form-group, .input-container, .edit-view');
    fieldWrapper.find('input').val('').removeClass('input-validation-error');
    fieldWrapper.find('.field-validation-error').removeClass('field-validation-error').addClass('validation-summary-valid').text('');
});

$(document).on('keypress', '.float-number', function (event) {
    var $this = $(this);
    if (event.which === 45 && ($this[0].selectionStart > 0 || !$this.hasClass('allow-negative'))) {
        event.preventDefault();
        return;
    }
    if ((event.which !== 46 || $this.val().indexOf('.') !== -1) &&
       (((event.which < 48 && event.which !== 45) || event.which > 57) &&
       (event.which !== 0 && event.which !== 8))) {
        event.preventDefault();
        return;
    }

    var text = $(this).val();
    if (text.length >= 16 && parseFloat(text) > Number.MAX_SAFE_INTEGER - 1) {
        event.preventDefault();
        return;
    }

    if ((event.which === 46) && (text.indexOf('.') === -1)) {
        setTimeout(function () {
            if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
            }
        }, 1);
    }

    if ((text.indexOf('.') !== -1) && (text.substring(text.indexOf('.')).length > 2) &&
        (event.which !== 0 && event.which !== 8) && ($(this)[0].selectionStart >= text.length - 2)) {
        event.preventDefault();
    }
});

$(document).on('paste', '.float-number', function () {
    var $element = $(this);
    setTimeout(function () {
        var number = parseFloat($element.val());
        if (/Invalid|NaN/.test(number) || (number < 0 && !$element.hasClass('allow-negative')) || number > Number.MAX_SAFE_INTEGER - 1) {
            $element.val('');
        } else {
            $element.val(number.toFixed(2));
        }
    }, 100);
});

$(document).on('click', '.languageli', function (e) {
    e.preventDefault();
    translateTo($(this).text());
});

// dom manipulations - end

/* Date time */
$(document).on('focus', '.datepicker:not(.init)', function () {
    var $element = $(this);
    var options = generalOptions.datePicker();
    options.format = generalOptions.currentDateFormat();
    var minRangeSelector = $element.data('min-range');
    var maxRangeSelector = $element.data('max-range');

    if (minRangeSelector) {
        var $minRangeElement = $(minRangeSelector);
        if (!$minRangeElement.hasClass('init')) {
            var minElementOptions = generalOptions.datePicker();
            minElementOptions.format = generalOptions.currentDateFormat();
            minElementOptions.useCurrent = false;
            $minRangeElement.datetimepicker(minElementOptions).addClass('init');
        }

        $element.on('dp.change', function (e) {
            $minRangeElement.data('DateTimePicker').maxDate(e.date);
            $element.trigger('change');
        });
        $minRangeElement.on('dp.change', function (e) {
            $element.data('DateTimePicker').minDate(e.date);
            $minRangeElement.trigger('change');
        });
        options.useCurrent = false;
    } else if (maxRangeSelector) {
        var $maxRangeElement = $(maxRangeSelector);
        if (!$maxRangeElement.hasClass('init')) {
            var maxElementOptions = generalOptions.datePicker();
            maxElementOptions.format = generalOptions.currentDateFormat();
            maxElementOptions.useCurrent = false;
            $maxRangeElement.datetimepicker(maxElementOptions).addClass('init');
        }

        $element.on('dp.change', function (e) {
            $maxRangeElement.data('DateTimePicker').minDate(e.date);
            $element.trigger('change');
        });
        $maxRangeElement.on('dp.change', function (e) {
            $element.data('DateTimePicker').maxDate(e.date);
            $maxRangeElement.trigger('change');
        });
        options.useCurrent = false;
    }
    $element.datetimepicker(options).addClass('init');
});

$(document).on('dp.change', '.datepicker', function () {
    var requestDataFunctionName = $(this).data('on-change-function');
    if (requestDataFunctionName) {
        smartvma.controls.executeFunctionByName(requestDataFunctionName);
    }

    var container = $(this).data('container');
    if (container) {
        smartvma.controls.refreshContainer(container);
    }
});

$(document).on('click', '.icon-calendar', function (e) {
    e.preventDefault(); // the important thing I think
    e.stopPropagation();

    var $input = $(this).parents('.form-group').find('.daterange');
    if ($input.hasClass('initialized')) {
        $input.data('daterangepicker').show();
    } else {
        $input.initRange();
    }
});

$(document).on('click focus', '.daterange:not(.initialized)', function (e) {
    e.stopPropagation();
    $(this).initRange();
});

$(document).on('click', '#searchBtn', function (e) {
    e.preventDefault();
    var descriptionValue = $('#Description').val();
    var ids = $('#CategoryIds').val();
    $.ajax({
        url: '/Admin/Article/SearchedItems',
        dataType: 'html',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify({
            key: descriptionValue,
            categoryIds: ids
        }),
        success: function (partialViewResult) {
            $('#searchItemsResults').html(partialViewResult);
            renderArticleSearchPaging();
        }
    });
});

$(document).on('click', '.pricebtn', function (e) {
    e.preventDefault();
    var dataToSend = {
        isVisible: $(this).hasClass('active')
    };
    $.ajax({
        url: '/Dealer/AppointmentPresentation/StoreIsPriceVisible',
        data: dataToSend,
        method: 'POST'
    });
});

$(document).on('hide.bs.modal', '.modal', function () {
    $('.daterangepicker').hide();
});

//set the status of seen alerts on closing the alert modal
$(document).on('click', '[data-alerts-set-status]', function () {
    var $closeBtn = $(this);
    var parent = $closeBtn.parents('.modal-content');
    if (parent.length) {
        var alertColection = parent.find('#alertColection');
        var singleAlerts = alertColection.find('.singleAlert');
        if (singleAlerts.length) {
            var ids = [];
            singleAlerts.each(function (i, el) {
                ids[i] = $(el).attr('id');
            });
            $.ajax({
                url: '/Admin/Alert/SetAlertStatus',
                dataType: 'html',
                type: 'POST',
                data: {
                    alertIds: ids,
                    statusId: '2'
                },
                success: function () {
                    smartvma.modals.close();
                }
            });
        }
    }
});

$.fn.initRange = function () {
    var $element = $(this);
    var options = generalOptions.dateRange();
    var minDate = $element.data('min-range');

    options.locale.format = generalOptions.currentDateFormat();
    if (minDate) {
        options.minDate = minDate;
    }
    var maxDate = $element.data('max-range');
    if (maxDate) {
        options.maxDate = maxDate;
    }
    var timePicker = $element.data('time-picker');
    if (timePicker) {

        options.timePicker = true;
        options.timePicker24Hour = true;
        options.locale.format += ' HH:mm';
    }

    var endRangeElement = $element.data('end-range-element');
    if (endRangeElement) {
        var $icon = $element.closest('.filters-container').find('.date-select');
        $icon.addClass('isSelected');
        $element.on('change',
            function () {
                var rangeData = $(this).data('daterangepicker');
                if (typeof rangeData === 'object' && rangeData != null) {
                    var startDate = rangeData.startDate.format(options.locale.format);
                    var endDate = rangeData.endDate.format(options.locale.format);
                    $($element.data('start-range-element')).val(startDate);
                    $($element.data('end-range-element')).val(endDate);

                    //set From and To dates in a string fields
                    var fromDateField = $element.attr('from-date-field');
                    if (fromDateField.length) {
                        $('.' + fromDateField).html(startDate);
                    }

                    var toDateField = $element.attr('to-date-field');
                    if (toDateField.length) {
                        $('.' + toDateField).html(endDate);
                    }

                    $element.on('show.daterangepicker', function () { $icon.addClass('isSelected'); });
                    $element.on('hide.daterangepicker', function () { $icon.removeClass('isSelected'); });
                    $($element.siblings('.date-range-display').find('.startDateString')).val(startDate);
                    $($element.siblings('.date-range-display').find('.endDateString')).val(endDate);
                }
            });
    } else {
        options.singleDatePicker = true;
        var parentEl = $element.closest('.modal-dialog');
        options.parentEl = parentEl;
    }

    if (Date.parse($element.val()) == 'Invalid Date') {
        $element.val('');
    }

    $element.daterangepicker(options);
    $element.addClass('initialized');
    $element.data('daterangepicker').show();
};
/* Date time - end */
$(document).on("click", ".filter-reset",
    function () {
        event.preventDefault();
        smartvma.controls.showLoader();
        var href = $(this).attr("data-href");
        $.ajax({
            method: "GET",
            url: href
        })
        .success(function (data) {
            $('.vehicle-filters').html(data);
            smartvma.multiselect.init();
            smartvma.controls.hideLoader();
        });

    });
/* 'AZ' buttons sorting*/
$(document).on('click', '.sort-table-az', function () {

    var $sortingButton = $(this);
    var order = 'asc';
    if (typeof ($sortingButton.data('sorting-order')) !== 'undefined') {
        order = $sortingButton.data('sorting-order');
    }
    var $table = $sortingButton.parents('table');

    smartvma.table.sorting($table, order);
    $sortingButton.closest('.sorting').addClass('sorted');
});

smartvma.table = {
    sorting: function (table, order) {
        var asc = order === 'asc',
               tbody = table.find('tbody');

        tbody.find('tr').sort(function (a, b) {
            if (asc) {
                return $('td:first', a).text().localeCompare($('td:first', b).text());
            } else {
                return $('td:first', b).text().localeCompare($('td:first', a).text());
            }
        }).appendTo(tbody);
    }
};


/* JS Prototype Methods, overrides */
if (!Array.prototype.filter) {
    Array.prototype.filter = function (fun) {
        var len = this.length >>> 0;
        if (typeof fun != 'function')
            throw new TypeError();

        var res = [];
        var thisp = arguments[1];
        for (var i = 0; i < len; i++) {
            if (i in this) {
                var val = this[i]; // in case fun mutates this
                if (fun.call(thisp, val, i, this))
                    res.push(val);
            }
        }
        return res;
    };
};
if (!String.prototype.replaceAll) {
    String.prototype.replaceAll = function (search, replacement) {
        var target = this;
        return target.split(search).join(replacement);
    };
}

String.prototype.interpolate = function (o) {
    return this.replace(/{([^{}]*)}/g,
        function (a, b) {
            var r = o[b];
            return typeof r === 'string' || typeof r === 'number' || typeof r === 'boolean' ? r : a;
        }
    );
};

Number.prototype.formatMoney = function (places, symbol, thousand, decimal) {
    places = !isNaN(places = Math.abs(places)) ? places : 2;
    symbol = symbol !== undefined ? symbol : '$ ';
    thousand = thousand || ',';
    decimal = decimal || '.';

    var x = readCookie('tempTranslateLanguage');
    if (x != null) {

        var lng = x.split("/")[x.split("/").length - 1];

        if (lng == 'es') {
            thousand = '.';
            decimal = ',';
        }
        else if (lng == 'fr') {
            thousand = ' ';
            decimal = ',';
        }
    }

    var number = this,
        negative = number < 0 ? '-' : '',
        i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + '',
        j = (j = i.length) > 3 ? j % 3 : 0;
    return symbol + negative + (j ? i.substr(0, j) + thousand : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : '');
};

if (!String.prototype.startsWith) {
    String.prototype.startsWith = function (searchString, position) {
        position = position || 0;
        return this.indexOf(searchString, position) === position;
    };
};

/* DO NOT USE .trigger('click') this is solution but skip this if possible!!! */
// how to use $('#bar').simulateClick('click');
$.fn.simulateClick = function () {
    return this.each(function () {
        if ('createEvent' in document) {
            var doc = this.ownerDocument,
                evt = doc.createEvent('MouseEvents');
            evt.initMouseEvent('click', true, true, doc.defaultView, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
            this.dispatchEvent(evt);
        } else {
            this.click(); // IE Boss!
        }
    });
};

$.fn.serializeObject = function (makeArrays, delimiter) {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (makeArrays) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            }
            else {
                o[this.name] += delimiter + this.value;
            }

        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

Date.parse = function (str) {
    if (typeof (str) !== 'string') {
        return null;
    }

    var dateFormat = generalOptions.currentDateFormat();
    var values = str.replace(/\D/g, ' ').split(' ');
    if (values.length < 3) {
        return null;
    }
    if (values.length > 3) {
        dateFormat += ' HH:mm';
    }
    return moment(str, dateFormat).toDate();
};

$.validator.setDefaults({ ignore: ':disabled:not(.forcevalidation), :hidden:not(.multiselectlist)' });
$.validator.methods.date = function (value, element) {
    return this.optional(element) || !/Invalid|NaN/.test(Date.parse(value).toString());
};
$.validator.prototype.elements = function () {
    var validator = this,
        rulesCache = {};
    // Select all valid inputs inside the form (no submit or reset buttons)
    return $(this.currentForm)
        .find('input, select, textarea, [contenteditable]').not(':submit, :reset, :image').not(this.settings.ignore)
            .filter(function () {
                var name = this.name || $(this).attr('name'); // For contenteditable
                if (!name && validator.settings.debug && window.console) {
                    console.error('%o has no name assigned', this);
                }
                // Set form expando on contenteditable
                if (this.hasAttribute('contenteditable')) {
                    this.form = $(this).closest('form')[0];
                }
                // Select only the first element for each name, and only those with rules specified
                if (name in rulesCache || !validator.objectLength($(this).rules())) {
                    return false;
                }
                rulesCache[name] = true;
                return true;
            });
};


$(document).ajaxError(
        smartvma.callbacks.handleAjaxError
);


$(document).ajaxStart(function (e, request, errorThrown, exception) {
    //smartvma.controls.showLoader();
});
$(document).ajaxComplete(function (event, xhr, settings) {
    //smartvma.controls.hideLoader();
    //console.log('global ajaxComplete status: ' + xhr.status);
    var url = '/Admin/Accounts/Login';

    if (xhr.responseJSON && xhr.responseJSON.authorized === false) {
        //console.log('global ajaxComplete redirecting to: ' + url)
        location.href = url;
    }
    if (xhr.status == 401 || xhr.status == 403 || xhr.status == 308) {
        if (xhr.responseText) {
            var isResponseJson = false;
            try {
                var response = JSON.parse(xhr.responseText);
                isResponseJson = true;
            }
            catch (e) {
            }

            if (isResponseJson && response.controller) {
                return;
            }
        }
        //console.log('global ajaxComplete redirecting to: ' + url)
        location.href = url;
    }
});
/* End JS Prototype Methods, overrides */
function initTooltips() {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="tooltip"]').on('click', function () { $(this).tooltip('toggle'); });
    $('[data-toggle="tooltip"]').on('mouseenter', function () { $(this).tooltip('show'); });
    $('[data-toggle="tooltip"]').on('mouseleave', function () { $(this).tooltip('hide'); });
}
function initPage() {
    /* fix jump between resoloution */
    var $windowW = $(window).width();
    var $wrap = $('body');
    var $nav = $('.sidebar-nav');
    var $target = $nav.find('.submenu');
    initTooltips();
    $('body').addClass('noAnimation');
    //if ($windowW < 1280) {
    if (window.matchMedia('(max-width: 1279px)').matches) {
        if ($nav.data('mobileOpen')) {
            $wrap.addClass('toggled');
        } else {
            $wrap.removeClass('toggled');
        }
        if (!$wrap.hasClass('toggled')) {
            if ($target.hasClass('open')) {
                $target.removeClass('open').removeAttr('style');
            }
        }
    } else {
        if ($nav.data('desktopOpen')) {
            $wrap.addClass('toggled');
        } else {
            $wrap.removeClass('toggled');
        }
        if ($wrap.hasClass('toggled')) {
            if ($target.hasClass('open')) {
                $target.removeClass('open').removeAttr('style');
            }
        }
    }

    setTimeout(function () { $('body').removeClass('noAnimation'); }, 600);
};
$('body').on('click', '.menu-actions .dropdown-menu li.menu-actions-item, .print-actions li', function (event) {
    event.stopPropagation();
});
/* detech touch srean, getEvent return touchen or click */
var isTouch = {
    event: 'click',
    init: function () {
        this.event = (this.checkTouch()) ? 'touchend' : 'click';
        return this;
    },
    checkTouch: function () {
        return 'ontouchstart' in window || 'msmaxtouchpoints' in window.navigator;
    },
    getEvent: function () {
        return this.event;
    }
};

function translateTo(language) {
    document.cookie = "googtrans" + "=" + "/en/" + language + "; path=/";
    document.cookie = "tempTranslateLanguage" + "=" + "/en/" + language + "; path=/";
    window.location.reload();
}
function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

/*Password validation*/
$("#Password").on('input propertychange paste', function () {
    if ($(this).val() == '') {
        $("span.weakPassword").addClass("hidden");
        $("span.strongPassword").addClass("hidden");
    }
    else {
        var passReg = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,50}$");
        if (passReg.test($(this).val())) {
            $("#passwordContainer .input-container").addClass("strongPassword");
            $("#passwordContainer .input-container").removeClass("weakPassword");
            $("span.weakPassword").addClass("hidden");
            $("span.strongPassword").removeClass("hidden");
        }
        else {
            $("#passwordContainer .input-container").removeClass("strongPassword");
            $("#passwordContainer .input-container").addClass("weakPassword");
            $("span.weakPassword").removeClass("hidden");
            $("span.strongPassword").addClass("hidden");
        }
    }
});

function addAdvisorCodeForCompanyDiv(companyId, companyName, elementNr) {
    var html = '<div class="user-role" id="' + companyId + '">';
    html += companyName;
    html += '   <div class="form-group">';
    html += '       <label class="col-sm-4 control-label" for="UserRoles_' + elementNr + '__DmsUserNumber">Advisor Code:</label>';
    html += '       <div class="col-sm-8">';
    html += '           <input class="form-control" id="UserRoles_' + elementNr + '__DmsUserNumber" name="UserRoles[' + elementNr + '].DmsUserNumber" placeholder="" type="text" value="">';
    html += '           <span class="field-validation-valid" data-valmsg-for="UserRoles[' + elementNr + '].DmsUserNumber" data-valmsg-replace="true"></span>';
    html += '           <span class="validation-clear"></span>';
    html += '       </div>';
    html += '   </div>';
    html += '   <input data-val="true" data-val-number="The field TenantId must be a number." data-val-required="The TenantId field is required." id="UserRoles_' + elementNr + '__TenantId" name="UserRoles[' + elementNr + '].TenantId" type="hidden" value="' + companyId + '">';
    html += '</div>';
    return html;
}

function changeAdvisorCodeCompanyProperties(elementNr) {
    var currentSelector = 'UserRoles_' + elementNr + '__DmsUserNumber';
    var nextSelector = 'UserRoles_' + (elementNr - 1) + '__DmsUserNumber';
    $("label[for='" + currentSelector + "']").attr("for", nextSelector);
    var currentNameProperty = 'UserRoles[' + elementNr + '].DmsUserNumber';
    var nextNameProperty = 'UserRoles[' + (elementNr - 1) + '].DmsUserNumber';
    $("#" + currentSelector).attr("name", nextNameProperty);
    $("#" + currentSelector).attr("id", nextSelector);
    $("span[data-valmsg-for='" + currentNameProperty + "']").attr("data-valmsg-for", nextNameProperty);

    var currentTenantId = 'UserRoles_' + elementNr + '__TenantId';
    var nextTenantId = 'UserRoles_' + (elementNr - 1) + '__TenantId';
    var currentTenantName = 'UserRoles[' + elementNr + '].TenantId';
    var nextTenantName = 'UserRoles[' + (elementNr - 1) + '].TenantId';
    $("#" + currentTenantId).attr("name", nextTenantName);
    $("#" + currentTenantId).attr("id", nextTenantId);
}

function copyToClipboard(){
    var copyTextArea = $('.copytextarea');
    copyTextArea.select();
    document.execCommand("copy");
}
 
var currentPage = 1;

function renderArticleSearchPaging() {
    var cnt = $(".results-count").html();

    var info = "Showing 1 to 10 of " + cnt + " entries";
    if (cnt < 10)
        info = "Showing 1 to " + cnt + " of " + cnt + " entries";
    $(".dataTables_info").html(info);

    var html = "";

    for (var i = 0; i < cnt / 10; i++) {
        if (i == 0) {
            html += '<a class="paginate_button current" onclick="goToPage(' + (i + 1) + ')" id="paginate_button' + (i + 1) + '">' + (i + 1) + '</a>';
        }
        else {
            html += '<a class="paginate_button" onclick="goToPage(' + (i + 1) + ')" id="paginate_button' + (i + 1) + '">' + (i + 1) + '</a>';
        }
    }

    $("#paginationPages").html(html);

    managePrevNextButtons();
}

function goToPrevPage() {
    currentPage--;    
    goToPage(currentPage);
}

function goToNextPage() {
    currentPage++;
    goToPage(currentPage);
}

function goToPage(pageNr) {
    currentPage = pageNr;
    
    var cnt = $(".results-count").html();
    for (var i = 0; i < cnt / 10; i++) {
        $("#page" + (i + 1)).addClass("hidden");
    }

    $("#page" + (currentPage)).removeClass("hidden");

    var info = "Showing " + ((pageNr - 1) * 10 + 1) + " to " + (pageNr * 10) + " of " + cnt + " entries";
    if (cnt < pageNr * 10)
        info = "Showing " + ((pageNr - 1) * 10 + 1) + " to " + cnt + " of " + cnt + " entries";
    $(".dataTables_info").html(info);

    $(".paginate_button").removeClass("current");

    $("#paginate_button" + (currentPage)).addClass("current");

    managePrevNextButtons();
}

function managePrevNextButtons() {
    if (currentPage == 1) {
        $(".paginate_button.previous").attr('onclick', '');
        $(".paginate_button.previous").addClass('disabled');
    }
    else {
        $(".paginate_button.previous").attr('onclick', 'goToPrevPage(); return false;');
        $(".paginate_button.previous").removeClass('disabled');
    }

    var cnt = $(".results-count").html();
    if (currentPage > cnt / 10) {
        $(".paginate_button.next").attr('onclick', '');
        $(".paginate_button.next").addClass('disabled');
    }
    else {
        $(".paginate_button.next").attr('onclick', 'goToNextPage(); return false;');
        $(".paginate_button.next").removeClass('disabled');
    }
}