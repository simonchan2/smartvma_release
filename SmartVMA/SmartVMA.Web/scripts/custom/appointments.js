﻿var selectedCalendarView = 'agendaDay';
var startTime = new Date();
var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var presentationStatus = '';
var advisorIds = '';

$(document).ready(function () {

    if (window.location.pathname.toLowerCase().indexOf("appointmentpresentation") > -1) {
        openCustomerCopyPdf();
    }
    

    $('#calendarAppointment').fullCalendar({
        views: {
            customWeek: {
                type: 'agenda',
                duration: { days: 1 },
                buttonText: 'week'
            }
        },
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,customWeek,agendaDay'
        },
        defaultView: selectedCalendarView,
        firstDay: 1,
        allDaySlot: false,
        selectable: false,
        editable: false,
        eventLimit: false,
        aspectRatio: 2,
        eventAfterAllRender: function (view) { setDefaultScrollPosition(); }
    });

    advisorIds = $("#advisorIdsValue").val();
    presentationStatus = $("#appointmentFilterStatusValue").val();

    $("#AdvisorIds").change(function () {
        getSelectedAdvisorIDs($(this).val());
    });

    $("#AppointmentFilterStatus").change(function () {
        getSelectedPresentationStatus($(this).val());
    });

    // TODO: check this. I think this makes unnecessary request if there isn't this check: $('#calendarAppointment').length
    if ($('#calendarAppointment').length) {
        filterCalendarAppointments();
    }
    // setDefaultScrollPosition();
});

//scroll to current pc time
function setDefaultScrollPosition() {
    var totalHours = 24;
    var totalMinutesInHour = 60;
    var date = new Date();
    var startHour = 6;
    if (moment().format('YYYY-MM-DD') ==
    $('#calendarAppointment').fullCalendar('getDate').format('YYYY-MM-DD')) {
        startHour = date.getHours();
        if (date.getMinutes() >= totalMinutesInHour / 2) {
            startHour += 1 / 2;
        }
    }


    var scrolloffset = $(".fc-slats table").width() / totalHours * startHour;

    //  $(".fc-slats").scrollLeft($(".fc-slats table").width() / (totalHours / startHour));

    $(".fc-slats").mCustomScrollbar({
        theme: 'dark-3',
        scrollButtons: { enable: false },
        axis: "x",
        mouseWheel: { enable: false },
        advanced: { autoExpandHorizontalScroll: true },
        setLeft: scrolloffset + 'px',
        callbacks: {
            onInit: function () {
                cloneScroll($(this));
            },
            whileScrolling: function () {
                var offset = this.mcs.draggerLeft + 'px';
                var scrollid = $(this).find('.mCSB_scrollTools').attr('id');
                var $scroll2 = $('#' + scrollid + '2');
                var scrollbar2 = $scroll2.find('.mCSB_dragger')[0];
                scrollbar2.style.left = offset;
            },
            onUpdate: function () {
                cloneScroll($(this));
            }
        }

    })
}
function cloneScroll(el) {
    var scrollid = el.find('.mCSB_scrollTools').attr('id');
    var $scroll = $('#' + scrollid);
    var $scroll2 = $('#' + scrollid + '2');
    $scroll2.remove();
    $scroll.clone(true).attr('id', scrollid + '2').css({ 'top': '47px' }).appendTo(el);

}
function toggleAppointmentDetails(event, appointmentId, appointmentPresentationId) {
    $(".additionalAppointmentDetails" + appointmentId + "-" + appointmentPresentationId).toggle();
    event.preventDefault();
    $(event.target).toggleClass("glyphicon-triangle-top glyphicon-triangle-bottom");

    if (appointmentPresentationId == -1) {
        $.ajax({
            url: "/Dealer/Appointments/CreateAppointmentPresentation?appointmentId=" + appointmentId,
            type: 'GET',
            success: function (data) {
                debugger;
            },
        });
    }
}

function filterCalendarAppointments() {
    var ey = startTime.getFullYear();
    var em = startTime.getMonth() + 1;
    var ed = startTime.getDate();
    startTimeISO = ey + '-' + (em > 9 ? em : '0' + em) + '-' + (ed > 9 ? ed : '0' + ed);

    setCurrentDateLabel();

    jQuery.ajax({
        url: '/Dealer/Appointments/GetCalendarAppointments',
        type: 'GET',
        dataType: 'json',
        async: false,
        data: { startTimeISO: startTimeISO, calendarView: selectedCalendarView, advisorIds: advisorIds, presentationStatus: presentationStatus },
        success: function (data) {
            fillCalendarAppointments(data)

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    });
}

function getSelectedPresentationStatus(val) {

    presentationStatus = val;
    $("#AppointmentFilterStatus > option").each(function () {
        if ($(this).val() == val) {
            //presentationStatus = $(this).text();
            presentationStatus = $(this).val();
            return false;
        }
    });

    filterCalendarAppointments();
}

function getSelectedAdvisorIDs(advisorId) {
    if (advisorId === null) {
        advisorIds = '';
    }
    else {
        advisorIds = ',' + advisorId + ',';
    }

    filterCalendarAppointments();
}

function setCurrentDateLabel() {
    var res = "";

    if (selectedCalendarView == 'agendaDay') {
        res = startTime.getDate() > 9 ? startTime.getDate() : "0" + startTime.getDate();
        res += " " + monthNames[startTime.getMonth()];
    }
    else if (selectedCalendarView == 'customWeek') {
        var tmpDate = new Date(startTime);
        var diff = (tmpDate.getDay() + 6) % 7;
        $(".calendarWeekday").removeClass("weekdaySelected");
        $('.calendarWeekday').eq(diff).addClass("weekdaySelected");
        tmpDate.setDate(tmpDate.getDate() - diff);
        res = tmpDate.getDate() > 9 ? tmpDate.getDate() : "0" + tmpDate.getDate();
        res += " " + monthNames[tmpDate.getMonth()];
        tmpDate.setDate(tmpDate.getDate() + 6);
        res += " - ";
        res += tmpDate.getDate() > 9 ? tmpDate.getDate() : "0" + tmpDate.getDate();
        res += " " + monthNames[tmpDate.getMonth()];
    }
    else {
        res = monthNames[startTime.getMonth()];
    }

    res += ", " + startTime.getFullYear();

    $("#calendarCurrentDate").html(res);
}

function fillCalendarAppointments(data) {
    $('#calendarAppointment').fullCalendar('removeEvents');
    $('.dailyCountMV').remove();
    $('.customerInfo').remove();
    $('#calendarAppointment').fullCalendar('addEventSource', data.data);
    $('#calendarAppointment').fullCalendar('refetchEvents');
}

function getAppointmentHTML(event) {
    var appointmentHTML = "<div class='customerInfo' id='aid" + event.AppointmentId + 'apid' + event.AppointmentPresentationId + "'>";
    appointmentHTML += "<div><div class=\"customer-info-name\"><a href='#' onclick='toggleAppointmentDetails(event," + event.AppointmentId + "," + event.AppointmentPresentationId + ")' class=\"glyphicon glyphicon-triangle-bottom\"></a>" + ((event.FirstName === null) ? "" : event.FirstName) + " " + ((event.LastName === null) ? "" : event.LastName) + "</div>";
    appointmentHTML += "<div class=\"customer-info-mobile\">Mobile #: " + ((event.CellPhone === null) ? "" : event.CellPhone) + "</div>";

    appointmentHTML += "<div class='additionalAppointmentDetails" + event.AppointmentId + "-" + event.AppointmentPresentationId + "' style='display:none;'>Home #: " + ((event.HomePhone === null) ? "" : event.HomePhone) + "<br/>Work #: " + ((event.WorkPhone === null) ? "" : event.WorkPhone) + "<br/>License plate: " + ((event.VehicleLicense === null) ? "" : event.VehicleLicense) + "</div>";

    appointmentHTML += "<div class=\"customer-info-vehicle\">" + ((event.Year === null) ? "" : event.Year) + " " + ((event.Make === null) ? "" : event.Make) + " " + ((event.Model === null) ? "" : event.Model) + "</div>";

    appointmentHTML += "<div class='additionalAppointmentDetails" + event.AppointmentId + "-" + event.AppointmentPresentationId + "' style='display:none;'>VIN: " + ((event.VIN === null) ? "" : event.VIN) +
        "<br/>Engine: " + ((event.Engine === null) ? "" : event.Engine) +
        "<br/>Drive Line: " + ((event.DriveLine === null) ? "" : event.DriveLine) +
        "<br/>Transmission: " + ((event.Transmission === null) ? "" : event.Transmission) + "</div>";

    var appointmentStatus = "";
    var presentationStatus = "";
    if (!(event.AppointmentStatus === null)) {
        switch (event.AppointmentStatus) {
            case "D":
                appointmentStatus = "Open on DMS";
                break;
            case "DMS":
                appointmentStatus = "Open on DMS";
                presentationStatus = "Scheduled";
                break;
            default:
                appointmentStatus = "N/A";
                presentationStatus = "N/A";
                break;
        }
    }

    if (!(event.PresentationStatus === null)) {
        switch (event.PresentationStatus) {
            case "MC":
                presentationStatus = "Completed";
                appointmentStatus = "Accepted";
                break;
            case "MD":
                presentationStatus = "Completed";
                appointmentStatus = "Declined";
                break;
            case "MK":
                presentationStatus = "Completed";
                appointmentStatus = "Parked";
                break;
            case "M":
                presentationStatus = "Scheduled";
                appointmentStatus = "Presented";
                break;
            case "PD":
                presentationStatus = "Completed";
                appointmentStatus = "Parked Declined";
                break;
        }
    }


    appointmentHTML += "<div class=\"customer-info-status\"><span class=\"customer-appointment-status\">" + appointmentStatus + "</span> | <span class=\"customer-presentation-status\">" + presentationStatus + "</span></div>";

    appointmentHTML += "<div class=\"customer-info-advisor\">Advisor: " + ((event.AdvisorName === null) ? "" : event.AdvisorName) + " " + ((event.AdvisorFamily === null) ? "" : event.AdvisorFamily) + "</div><div class=\"options-btn\">";

    if (event.PresentationStatus == 'MC') {
        appointmentHTML += "<div class=\"menu-actions icon-menu\">";
        appointmentHTML += "<div class=\"dropup options-menu\">";
        appointmentHTML += "<a href=\"#\" class=\"btn dropdown-primary btn-primary\" data-toggle=\"dropdown\">";
        appointmentHTML += "<span>Options</span>";
        appointmentHTML += "</a>";
        var res = getAcceptedOptions(event.AppointmentPresentationId, event.DealerCustomerId, event.DealerId);
        appointmentHTML += res;
        appointmentHTML += "</div>";
        appointmentHTML += "</div>";
    }
    else {
        appointmentHTML += "<div class=\"menu-actions icon-menu\">";
        appointmentHTML += "<div class=\"dropup options-menu\">";
        appointmentHTML += "<a href=\"#\" class=\"btn dropdown-primary btn-primary\" data-toggle=\"dropdown\">";
        appointmentHTML += "<span>Options</span>";
        appointmentHTML += "</a>";
        var res = getParkedOptions(event.AppointmentPresentationId, event.DealerId, event.DealerCustomerId, event.AdvisorId, event.Mileage, event.CarId, event.DealerVehicleId, event.PresentationStatus, event.VIN, event.CarIdUndetermined, event.AppointmentTime, event.AppointmentId);
        appointmentHTML += res;
        appointmentHTML += "</div>";
        appointmentHTML += "</div>";
    }

    appointmentHTML += "</div>";
    appointmentHTML += "</div>";

    return appointmentHTML;
}

function getParkedOptions(appointmentPresentationId, dealerId, dealerCustomerId, advisorId, mileage, carId, dealerVehicleId, presentationStatus, vin, carIdUndetermined, appointmentDate, appointmentId) {
    var res = "";
    $.ajax({
        url: "/Dealer/Appointments/ParkedOptions?appointmentPresentationId=" + appointmentPresentationId + "&dealerId=" + dealerId + "&dealerCustomerId=" + dealerCustomerId + "&advisorId=" + advisorId + "&mileage=" + mileage + "&carId=" + carId + "&dealerVehicleId=" + dealerVehicleId + "&presentationStatus=" + presentationStatus + "&vin=" + vin + "&CarIdUndetermined=" + carIdUndetermined + "&appointmentDate=" + appointmentDate + "&appointmentId=" + appointmentId,
        contentType: 'application/html; charset=utf-8',
        type: 'GET',
        dataType: 'html',
        success: function (data) {
            res = data;
        },
        async: false
    });
    return res;
}

function getAcceptedOptions(appointmentPresentationId, dealerCustomerId, dealerId) {
    var res = "";
    $.ajax({
        url: "/Dealer/Appointments/AcceptedOptions?appointmentPresentationId=" + appointmentPresentationId + "&dealerCustomerId=" + dealerCustomerId + "&dealerId=" + dealerId,
        contentType: 'application/html; charset=utf-8',
        type: 'GET',
        dataType: 'html',
        success: function (data) {
            res = data;
        },
        async: false
    });
    return res;
}

function changeDate(date, setDailyView) {
    var offset = new Date(date).getTimezoneOffset();
    startTime = new Date(date + offset * 60000);
    filterCalendarAppointments();

    if (setDailyView) {
        selectedCalendarView = 'agendaDay';
        $(".fc-agendaDay-button").click();

        $("#linkCurrentDay").html("Today");

        $("#filterCalendarDaily").addClass("active custom-disabled");
        $("#filterCalendarWeekly").removeClass("active custom-disabled");
        $("#filterCalendarMonthly").removeClass("active custom-disabled");
    }

    $('#calendarAppointment').fullCalendar('gotoDate', date);
    $(".calendarWeekday").removeClass("weekdaySelected");
    $("#" + date).parent().addClass("weekdaySelected");

    setCurrentDateLabel();
}

function changeCalendarState(calendarView, object) {
    if (!($(object).hasClass('custom-disabled'))) {
        filterCalendarTimeframe(calendarView);
        //    setDefaultScrollPosition();
    }
}

function filterCalendarTimeframe(calendarView) {
    selectedCalendarView = calendarView;
    if (selectedCalendarView == 'agendaDay') {
        filterCalendarAppointments();

        $(".fc-agendaDay-button").click();

        $("#filterCalendarDaily").addClass("active custom-disabled");
        $("#filterCalendarWeekly").removeClass("active custom-disabled");
        $("#filterCalendarMonthly").removeClass("active custom-disabled");

        $("#linkCurrentDay").html("Today");
    }
    else if (selectedCalendarView == 'customWeek') {

        $(".fc-customWeek-button").click();

        $("#filterCalendarDaily").removeClass("active custom-disabled");
        $("#filterCalendarWeekly").addClass("active custom-disabled");
        $("#filterCalendarMonthly").removeClass("active custom-disabled");

        var tmpDate = new Date(startTime);
        var diff = (tmpDate.getDay() + 6) % 7;

        $(".calendarWeekday a").eq(diff).click();


        $("#linkCurrentDay").html("Current week");
    }
    else {
        filterCalendarAppointments();

        $(".fc-month-button").click();

        $("#filterCalendarDaily").removeClass("active custom-disabled");
        $("#filterCalendarWeekly").removeClass("active custom-disabled");
        $("#filterCalendarMonthly").addClass("active custom-disabled");

        $("#linkCurrentDay").html("Current month");
    }
}

function filterCalendarNext() {
    if (selectedCalendarView == 'agendaDay') {
        startTime.setDate(startTime.getDate() + 1);
    }
    else if (selectedCalendarView == 'customWeek') {
        var tmpDate = new Date(startTime);
        var diff = (tmpDate.getDay() + 6) % 7;
        tmpDate.setDate(tmpDate.getDate() - diff);
        startTime.setDate(tmpDate.getDate() + 7);
    }
    else {
        startTime.setDate(1);
        startTime.setMonth(startTime.getMonth() + 1);
    }

    filterCalendarAppointments();
    $('#calendarAppointment').fullCalendar('next');
    if (selectedCalendarView == 'customWeek') {
        $(".calendarWeekday a").first().click();
    }
}

function filterCalendarPrev() {
    if (selectedCalendarView == 'agendaDay') {
        startTime.setDate(startTime.getDate() - 1);
    }
    else if (selectedCalendarView == 'customWeek') {
        var tmpDate = new Date(startTime);
        var diff = (tmpDate.getDay() + 6) % 7;
        tmpDate.setDate(tmpDate.getDate() - diff);
        startTime.setDate(tmpDate.getDate() - 7);
    }
    else {
        startTime.setDate(1);
        startTime.setMonth(startTime.getMonth() - 1);
    }

    filterCalendarAppointments();
    $('#calendarAppointment').fullCalendar('prev');
    if (selectedCalendarView == 'customWeek') {
        $(".calendarWeekday a").first().click();
    }
}

function filterCalendarToday() {
    startTime = new Date();
    filterCalendarAppointments();
    $('#calendarAppointment').fullCalendar('today');

    if (selectedCalendarView == 'customWeek') {
        var diff = (startTime.getDay() + 6) % 7;

        $('.calendarWeekday').eq(diff).addClass("weekdaySelected");
    }
}

function openCustomerCopyPdf() {
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    var dealerId = 0;
    var appointmentPresentationId = 0;
    var documentType = 0;
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        if (hash[0] == "dealerId") {
            dealerId = hash[1];
        }
        if (hash[0] == "appointmentPresentationId") {
            appointmentPresentationId = hash[1];
        }
        if (hash[0] == "documentType") {
            documentType = hash[1];
        }        
    }

    if (dealerId > 0 && appointmentPresentationId > 0 && documentType > 0) {
        //Action View
        var pdfAction = 1;
        managePDF(dealerId, appointmentPresentationId, documentType, pdfAction);
    }

}

function managePDF(dealerId, appointmentPresentationId, documentType, pdfAction) {
    window.open("/Dealer/PdfDocuments/ManagePdfFile?dealerId=" + dealerId + '&appointmentPresentationId=' + appointmentPresentationId + '&documentType=' + documentType + '&pdfAction=' + pdfAction, '_blank');
}

$(function () {
    $('.appointments-filters h3 .switch').click(function (ev) {
        ev.preventDefault();
        $('.appointments-filters').toggleClass('open');
    })
});

function getMenuPresentationPrintOptions() {
    var selectedMenuLevel = 1;
    var selectedMenuID = $(".iconradio.active").attr('id');

    switch (selectedMenuID) {
        case "level3Btn":
            selectedMenuLevel = 3;
            break;
        case "level2Btn":
            selectedMenuLevel = 2;
            break;
        case "level1Btn":
            selectedMenuLevel = 1;
            break;
    }

    var carId = $("#MenuCarId").val();
    var mileage = $("#MenuMileage").val();
    var customerId = $("#CustomerInfo_Id").val();
    var appointmentPresentationId = $("#AppointmentPresentationId").val();
    var inspectionId = $("#InspectionId").val();
    var transmission = $("#Transmission").val();
    var driveline = $("#Driveline").val();

    $.ajax({
        url: "/Dealer/MenuPresentation/MenuPresentationPrint/?printSingleMenu=true&includePrices=true&selectedMenuLevel=" + selectedMenuLevel +
        "&carId=" + carId + "&mileage=" + mileage + "&customerId=" + customerId + "&appointmentPresentationId=" + appointmentPresentationId +
        "&inspectionId=" + inspectionId + "&transmission=" + transmission + "&driveline=" + driveline,
        contentType: 'application/html; charset=utf-8',
        type: 'GET',
        dataType: 'html',
        success: function (data) {
            $("#menuPresentationPrintOptions").html(data);
        },
        async: false
    });
}

function printMenuPresentation(printSingleMenu) {

    var appointmentPresentationId = $("#AppointmentPresentationId").val() != "" ? $("#AppointmentPresentationId").val() : -1;

    var parkedAppointmentTime = $("#ParkedAppointmentTime").val();

    var selectedMenuLevel = $("#SelectedMenuLevel").val();
    var includePrices = $("#IncludePrices").is(':checked');

    var menuLevel1Price = $("#level1Btn .menu-price").html();
    var menuLevel1Description = $("#level1Btn .menu-title").html();
    var menuLevel2Price = $("#level2Btn .menu-price").html();
    var menuLevel2Description = $("#level2Btn .menu-title").html();
    var menuLevel3Price = $("#level3Btn .menu-price").html();
    var menuLevel3Description = $("#level3Btn .menu-title").html();

    var services = new Array();

    if (!printSingleMenu || selectedMenuLevel == 1) {
        $("#level1Services .maintenance-service-table tbody tr").each(function () {
            var serviceId = $(this).find(".service-name #Id").val();
            var isDeclined = $(this).find(".service-name #IsStrikeOut").val();
            var isAdditional = $(this).find(".service-name #DescriptionId").hasClass("remove-additional-maintenance-service");
            var serviceMenuLevel = 1;
            var serviceDescription = $(this).find(".service-name #Description").val();
            var servicePrice = $(this).find(".service-name #AppPrice").val();
            var partsPrice = $(this).find(".service-name #PartsPrice").val();
            var laborPrice = $(this).find(".service-name #LaborPrice").val();
            var serviceLabourHours = $(this).find(".service-name #LaborHours").val();

            services.push({
                serviceId: serviceId, isDeclined: isDeclined, ServiceIsStrikeOut: isDeclined, isAdditional: isAdditional, serviceMenuLevel: serviceMenuLevel, serviceDescription: serviceDescription,
                servicePrice: servicePrice, partsPrice: partsPrice, laborPrice: laborPrice, serviceLabourHours: serviceLabourHours
            });
        });
    }

    if (!printSingleMenu || selectedMenuLevel == 2) {
        $("#level2Services .maintenance-service-table tbody tr").each(function () {
            var serviceId = $(this).find(".service-name #Id").val();
            var isDeclined = $(this).find(".service-name #IsStrikeOut").val();
            var isAdditional = $(this).find(".service-name #DescriptionId").hasClass("remove-additional-maintenance-service");
            var serviceMenuLevel = 2;
            var serviceDescription = $(this).find(".service-name #Description").val();
            var servicePrice = $(this).find(".service-name #AppPrice").val();
            var partsPrice = $(this).find(".service-name #PartsPrice").val();
            var laborPrice = $(this).find(".service-name #LaborPrice").val();
            var serviceLabourHours = $(this).find(".service-name #LaborHours").val();

            services.push({
                serviceId: serviceId, isDeclined: isDeclined, ServiceIsStrikeOut: isDeclined, isAdditional: isAdditional, serviceMenuLevel: serviceMenuLevel, serviceDescription: serviceDescription,
                servicePrice: servicePrice, partsPrice: partsPrice, laborPrice: laborPrice, serviceLabourHours: serviceLabourHours
            });
        });
    }

    if (!printSingleMenu || selectedMenuLevel == 3) {
        $("#level3Services .maintenance-service-table tbody tr").each(function () {
            var serviceId = $(this).find(".service-name #Id").val();
            var isDeclined = $(this).find(".service-name #IsStrikeOut").val();
            var isAdditional = $(this).find(".service-name #DescriptionId").hasClass("remove-additional-maintenance-service");
            var serviceMenuLevel = 3;
            var serviceDescription = $(this).find(".service-name #Description").val();
            var servicePrice = $(this).find(".service-name #AppPrice").val();
            var partsPrice = $(this).find(".service-name #PartsPrice").val();
            var laborPrice = $(this).find(".service-name #LaborPrice").val();
            var serviceLabourHours = $(this).find(".service-name #LaborHours").val();

            services.push({
                serviceId: serviceId, isDeclined: isDeclined, ServiceIsStrikeOut: isDeclined, isAdditional: isAdditional, serviceMenuLevel: serviceMenuLevel, serviceDescription: serviceDescription,
                servicePrice: servicePrice, partsPrice: partsPrice, laborPrice: laborPrice, serviceLabourHours: serviceLabourHours
            });
        });
    }

    var customerName = $("#CustomerInfo_CustomerInfo_FullName").val();
    var vehicleMake = $("#Makes").find(":selected").text();
    var vehicleYear = $("#Years").find(":selected").text();
    var vehicleModel = $("#Models").find(":selected").text();
    var vehicleEngine = $("#EngineTypes").find(":selected").text();
    var vIN = $("#CustomerInfo_CustomerInfo_VIN").val();
    var odometerReading = $("#EnteredMileage").val();
    var odometer = $("#CustomerInfo_Mileage").next().find(".multiselect-selected-text").text();
    var title = $(".customer-info-mileage .multiselect-selected-text").text();

    var customerInfo = { CustomerName: customerName, VehicleMake: vehicleMake, VehicleYear: vehicleYear, VehicleModel: vehicleModel, VIN: vIN, OdometerReading: odometerReading, Odometer: odometer, VehicleEngine: vehicleEngine }

    var header = { CustomerInfo: customerInfo };
    var pdfWindow = window.open('/Home/PageLoader');
/*

  var pdfWindows = {}  
    var pdfWindowName='pdf' + Date.now();
    console.log(pdfWindowName);
    pdfWindows[pdfWindowName] = window.open('/Home/PageLoader');
    pdfWindows[pdfWindowName].window.location.href = '/Home/PageLoader';
    console.log(pdfWindows[pdfWindowName]);
*/

    //google translate adds some html tags, so we remove them here
    menuLevel1Price = menuLevel1Price.replace(/<\/?[^>]+(>|$)/g, "");
    menuLevel2Price = menuLevel2Price.replace(/<\/?[^>]+(>|$)/g, "");
    menuLevel3Price = menuLevel3Price.replace(/<\/?[^>]+(>|$)/g, "");
    menuLevel1Description = menuLevel1Description.replace(/<\/?[^>]+(>|$)/g, "");
    menuLevel2Description = menuLevel2Description.replace(/<\/?[^>]+(>|$)/g, "");
    menuLevel3Description = menuLevel3Description.replace(/<\/?[^>]+(>|$)/g, "");

    if (printSingleMenu) {
        var customerCopyPdf = {
            IncludePrices: includePrices, ParkedAppointmentTime: parkedAppointmentTime,
            MenuLevel1Price: menuLevel1Price, MenuLevel2Price: menuLevel2Price, MenuLevel3Price: menuLevel3Price,
            MenuLevel1Description: menuLevel1Description, MenuLevel2Description: menuLevel2Description, MenuLevel3Description: menuLevel3Description,
            Services: services, AppointmentPresentationId: appointmentPresentationId,
            Header: header, Title: title, SelectedMenuLevel: selectedMenuLevel
        };
        $.ajax({
            url: "/Dealer/MenuPresentation/ViewMenuPresentationPrintSinglePdfFile",
            type: "POST",
            data: { customerCopyPdf: customerCopyPdf },
            success: function (data) {
                //  window.open(data, '_blank');
                pdfWindow.window.location.href = data;
            }
        });
    }
    else {
        var menuPresentationPrintModel = {
            IncludePrices: includePrices, ParkedAppointmentTime: parkedAppointmentTime,
            MenuLevel1Price: menuLevel1Price, MenuLevel2Price: menuLevel2Price, MenuLevel3Price: menuLevel3Price,
            MenuLevel1Description: menuLevel1Description, MenuLevel2Description: menuLevel2Description, MenuLevel3Description: menuLevel3Description,
            PrintServices: services, AppointmentPresentationId: appointmentPresentationId,
            Header: header
        };

        $.ajax({
            url: "/Dealer/MenuPresentation/ViewMenuPresentationPrintPdfFile",
            type: "POST",
            data: { menuPresentationPrintModel: menuPresentationPrintModel },
            success: function (data) {
                //  window.open(data, '_blank');
                pdfWindow.window.location.href = data;
            }
        });
    }
}

function printVehicleInspection(inspectionId, appointmentPresentationID, dealerId) {
    var lFRimScratch = $("#LFRimScratch").is(':checked');
    var lRRimScratch = $("#LRRimScratch").is(':checked');
    var rFRimScratch = $("#RFRimScratch").is(':checked');
    var rRRimScratch = $("#RRRimScratch").is(':checked');
    var lFTireTypeId = -1;
    if ($("#LFTireTypeId_0").is(':checked')) {
        lFTireTypeId = 0;
    }
    if ($("#LFTireTypeId_1").is(':checked')) {
        lFTireTypeId = 1;
    }
    if ($("#LFTireTypeId_2").is(':checked')) {
        lFTireTypeId = 2;
    }
    var lRTireTypeId = -1;
    if ($("#LRTireTypeId_0").is(':checked')) {
        lRTireTypeId = 0;
    }
    if ($("#LRTireTypeId_1").is(':checked')) {
        lRTireTypeId = 1;
    }
    if ($("#LRTireTypeId_2").is(':checked')) {
        lRTireTypeId = 2;
    }
    var rFTireTypeId = -1;
    if ($("#RFTireTypeId_0").is(':checked')) {
        rFTireTypeId = 0;
    }
    if ($("#RFTireTypeId_1").is(':checked')) {
        rFTireTypeId = 1;
    }
    if ($("#RFTireTypeId_2").is(':checked')) {
        rFTireTypeId = 2;
    }
    var rRTireTypeId = -1;
    if ($("#RRTireTypeId_0").is(':checked')) {
        rRTireTypeId = 0;
    }
    if ($("#RRTireTypeId_1").is(':checked')) {
        rRTireTypeId = 1;
    }
    if ($("#RRTireTypeId_2").is(':checked')) {
        rRTireTypeId = 2;
    }
    var enteredMileage = $("#EnteredMileage").val();
    var model = $("#CustomerInfo_Model").val();
    var make = $("#CustomerInfo_Make").val();
    var year = $("#CustomerInfo_Year").val();
    var pdfWindow = window.open('/Home/PageLoader');
    $.ajax({
        url: "/Dealer/Inspection/ViewVehicleInspectionPrintPdfFile",
        type: "POST",
        data: {
            inspectionId: inspectionId, appointmentPresentationID: appointmentPresentationID, dealerId: dealerId,
            enteredMileage: enteredMileage, model: model, make: make, year: year,
            lFRimScratch: lFRimScratch, lRRimScratch: lRRimScratch, rFRimScratch: rFRimScratch, rRRimScratch: rRRimScratch,
            lFTireTypeId: lFTireTypeId, lRTireTypeId: lRTireTypeId, rFTireTypeId: rFTireTypeId, rRTireTypeId: rRTireTypeId
        },
        success: function (data) {
            // window.open(data, '_blank');
            pdfWindow.window.location.href = data;
        }
    });
}

function printVehicleHistory(vin, isDeclined) {
    var pdfWindow = window.open('/Home/PageLoader');
    $.ajax({
        url: "/Dealer/VehicleMaintenanceHistory/ViewAppointmentInfoPdfFile",
        type: "POST",
        data: { vin: vin, isDeclined: isDeclined },
        success: function (data) {
            // window.open(data, '_blank');
            pdfWindow.window.location.href = data;
        }
    });
}

function printILife(vin) {
    var pdfWindow = window.open('/Home/PageLoader');
    $.ajax({
        url: "/Dealer/ILife/ViewILifePdfFile",
        type: "POST",
        data: { vin: vin },
        success: function (data) {
            pdfWindow.window.location.href = data;
        }
    });
}