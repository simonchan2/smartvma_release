﻿$(document).ready(function () {
    var isFromLogin = $(':hidden[name=isFromLogin]').val();
    if (smartvma.controls.parseBool(isFromLogin)) {
        smartvma.modals.open('/Admin/Alert/ShowAlertsOnLongIn', 'Alerts', '');
    }
});

$(document).on('click', '.setAlertStatus', function () {
    var $this = $(this);
    var statusId = $this.attr('status-id');
    var alertId = $this.attr('alert-id');

    //set the alert status after user click
    $.ajax({
        type: 'POST',
        url: '/Admin/Alert/SetAlertStatus',
        data: {
            alertId: alertId,
            statusId: statusId
        },
        success: function (result) {
            if (result.isSuccess) {
                var mainDiv = $('#alertMainDiv_' + alertId);
                if (mainDiv.length) {
                    mainDiv.hide();
                    if ($('#alertColection .singleAlert:visible').length === 0) {
                        smartvma.modals.close();
                    }
                }
            }
        }
    });
});

function onSuccessDeleteParkedMenu(data) {
    $('#parkedMenuContent').html(data);
}