﻿//$('#addServicePart').click(function (e) {
//    e.preventDefault();
//    $.ajax({
//        type: 'POST',
//        dataType: 'html',
//        url: '/Admin/DataMaintenance/OemPart',
//        success: function (result) {
//            var test = $('.partsTotal');
//            $('#partsTotal').append(result);
//            smartvma.controls.init();
//        }
//    })
//});

$(".selectPartType").click(function () {
    var partType = $(this).val();
    var name = $(this).attr('name').split("OemPartTypeId");
    var fluidType = $("select[name='" + name[0] + "FluidTypeInt']");
    var fluidViscosity = $("select[name='" + name[0] + "FluidViscosityInt']");

    if (partType == 0) {
        if (!fluidType.is(":disabled")) 
            smartvma.multiselect.disable(fluidType);
        if (!fluidViscosity.is(":disabled"))
            smartvma.multiselect.disable(fluidViscosity);
       
    }
    else {
        if (fluidType.is(":disabled")) 
            smartvma.multiselect.enable(fluidType);
        if (fluidViscosity.is(":disabled"))
            smartvma.multiselect.enable(fluidViscosity);
        
    }

})