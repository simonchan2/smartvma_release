﻿$(function () {
    smartvma.overrideRules.init();
});
smartvma.overrideRules = (function () {

    var dtCreated = false;
    var selectedServices = [];
    var allServices = [];
    var selectAllChbox = $("#selectAllDtRows");
    var radioButtonPrefix = "rb_";
    var checkboSuffix = "Checked";
    function init() {
        $("#RuleName").val($("#RuleName").attr("value"));
        $("#RuleName").removeAttr("value");
        initValidation();
        attachEvents();
        createDatatable();
        //initialSearchOverrides(loadCheckboxData);
        //initRadioButtons($(".select-override-types input[type='radio']"), true);
    };
    function attachEvents() {
        $("#btnSearchOverrides")
            .on("click",
                function () {
                    if (validateSearchFilter()) {
                        //initialSearchOverrides(loadCheckboxData);
                        createDatatable();
                    }
                });
        $("#overrideRuleSearch")
            .on("change",
                "[type=checkbox], #Keyword, #OpCode, #PartNumber",
                function () {
                    destroyDatatable();
                });

        $('#overrideBasicServices tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selectedServices);
            if (index === -1) {
                selectedServices.push(id);
            } else {
                selectedServices.splice(index, 1);
            }
            updateSelectAll();
            $(this).toggleClass('selected');
        });

        $("#selectAllDtRows").on('change', function () {
            applyChange(this.checked);
        });

        $("input[type='radio']")
            .on("change", function () {
                resetTexboxes($(".select-override-types input[type='text'][name^='" + this.name + "']"));
                enableTextbox($("#" + this.id.replace(radioButtonPrefix, "")));
            });
        $(".select-override-types input[type='checkbox']")
        .on("change", function () {
            var tbName = this.name.replace(checkboSuffix, "");
            var $rbHandle = $(".select-override-types input[type='radio'][name^='" + tbName + "']");
            var $txtHandle = $(".select-override-types input[type='text'][name^='" + tbName + "']");
            if (!this.checked) {
                resetTexboxes($txtHandle);
                if (tbName === "PartNumber") {
                    $txtHandle = $(".select-override-types input[type='text'][name^='Original" + tbName + "']");
                    resetTexboxes($txtHandle);
                    $txtHandle = $(".select-override-types input[type='text'][name^='Override" + tbName + "']");
                    resetTexboxes($txtHandle);
                }
            }
            else {
                if (tbName === "OverrideOpcode") {
                    enableTextbox($txtHandle);
                }
                if (tbName === "PartNumber") {
                    $txtHandle = $(".select-override-types input[type='text'][name^='Original" + tbName + "']");
                    enableTextbox($txtHandle);
                    $txtHandle = $(".select-override-types input[type='text'][name^='Override" + tbName + "']");
                    enableTextbox($txtHandle);
                }
            }
            initRadioButtons($rbHandle, !this.checked);
        });
        $("#btnSave").on("click",
                function () {
                    $("#selectedServicesArray").val(selectedServices);
                    if (selectedServices.length > 0) {
                        if ($('form').valid()) {
                            smartvma.controls.showLoader();
                            var $btn = $(this);
                            smartvma.modals.open($btn.data('url'), $btn.attr('title'), $('form').serialize(), 'POST');
                        }
                        $("#selectedServicesValidation > span").addClass("hidden");
                    }
                    else {
                        $("#selectedServicesValidation > span").removeClass("hidden");
                    }
                });
    };
    function initValidation() {
        $("form").validate();
    };

    function validateSearchFilter() {
        return $('#Years, #Makes, #Models, #EngineTypes, #DriveLines, #TransmissionTypes').valid();
    };
    function resetTexboxes(obj) {
        obj.val("").attr("disabled", "disabled");
    };
    function enableTextbox(obj) {
        obj.removeAttr("disabled");
    };
    function initRadioButtons(obj, disable) {
        obj.removeAttr("checked");
        if (disable) {
            obj.attr("disabled", "disabled");
        }
        else {
            obj.removeAttr("disabled");
        }

    };

    function reinitCheckboxes(row, data) {
        if ($.inArray(data.rowId, selectedServices) !== -1) {
            $(row).addClass('selected');
        }
    };
    function createDatatable() {
        destroyDatatable();
        $("#overrideBasicServices").show();
        $('#overrideBasicServices')
            .on('xhr.dt', function (e, settings, json, xhr) {
                //console.log(e, settings, json, xhr);
            })
            .DataTable({
                "processing": true,
                "serverSide": true,
                "ordering": false,
                "searching": false,
                "lengthChange": false,
                rowId: 'rowId',
                ajax: {
                    data: $("form").serializeObject(false, ","),
                    url: "/Admin/OverrideRules/SearchOverrideRules",
                    type: "post",
                    error: smartvma.callbacks.handleAjaxError,
                    dataSrc: function (json) {
                        if (json && json.data) {
                            var partInfoArr = "";
                            var separator = "<br/>";
                            for (var j = 0; j < json.data.length; j++) {
                                json.data[j].partNumbers = "";
                                json.data[j].partPrices = "";
                                json.data[j].partQuantitys = "";
                                json.data[j].partTypes = "";

                                if (json.data[j].partInfo) {
                                    partInfoArr = json.data[j].partInfo.slice();
                                    for (var i = 0; i < partInfoArr.length; i++) {
                                        json.data[j].partNumbers += partInfoArr[i].partNumber + separator;
                                        json.data[j].partPrices += partInfoArr[i].partPrice + separator;
                                        json.data[j].partQuantitys += partInfoArr[i].partQuantity + separator;
                                        json.data[j].partTypes += partInfoArr[i].partType + separator;
                                    }
                                }
                                if (json.data[j].isLof) {
                                    json.data[j].isLof = "<span class='icon-confirm'></span>";
                                } else {
                                    json.data[j].isLof = "";
                                }
                            }
                            if (json.draw == 1) {
                                loadCheckboxData(json);
                            }
                            return json.data;
                        }

                    }
                },
                "rowCallback": function (row, data) {
                    reinitCheckboxes(row, data);
                },
                "columns": [
                    {
                        "data": null, "defaultContent": ""
                    },
                    {
                        "data": "id"
                    },
                    {
                        "data": "opcode"
                    },
                    {
                        "data": "description"
                    },
                    {
                        "data": "laborPrice"
                    },
                    {
                        "data": "laborTime"
                    },
                    {
                        "data": "partNumbers"
                    },
                    {
                        "data": "partPrices"
                    },
                    {
                        "data": "partQuantitys"
                    },
                    {
                        "data": "partTypes"
                    },
                    {
                        "data": "isLof"
                    }
                ],
                columnDefs: [
                { "width": "100px", "targets": 6 },
                {
                    orderable: false,
                    className: 'select-checkbox',
                    targets: 0
                }
                ]
                //"deferLoading": data.recordsTotal
            });
        dtCreated = true;
    };
    function destroyDatatable() {
        if (dtCreated) {
            $('#overrideBasicServices').DataTable().destroy();
            $("#overrideBasicServices").hide();
            dtCreated = false;
        }
    };
    /*function initialSearchOverrides(successFunc) {
        var formDataStr = $("form").serialize();
        $.ajax({
            type: "POST",
            url: "/Admin/OverrideRules/SearchOverrideRules",
            data: formDataStr,
            success: successFunc
        });
    };*/
    function applyChange(checkAll) {
        if (checkAll) {
            selectedServices = allServices.slice();
        } else {
            selectedServices = [];
        }
        $('#overrideBasicServices').dataTable().fnDraw();
    };
    function loadCheckboxData(data) {
        selectedServices = data.selectedServices;
        allServices = data.allServices;
        //createDatatable();
        updateSelectAll();
    };

    function updateSelectAll() {
        selectAllChbox.prop("checked", selectedServices.length === allServices.length);
    }

    function getSelectedServices() {
        return selectedServices;
    }

    return {
        getSelectedServices: getSelectedServices,
        init: init
    };

}());