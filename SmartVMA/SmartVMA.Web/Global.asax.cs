﻿using Autofac.Integration.Mvc;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Utilities;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Elmah;
using SmartVMA.Core.Utils;
using System.Threading;
using SmartVMA.Infrastructure.CacheManager;
using SmartVMA.Infrastructure.ViewEngines;

namespace SmartVMA.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            DependencyResolver.SetResolver(new AutofacDependencyResolver(IocManager.Container));

            // register custom model metadata provider and custom validation attributes
            ModelMetadataProviders.Current = new CustomModelMetadataProvider();
            DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(RequiredIfSettingAttribute), typeof(RequiredAttributeAdapter));
            DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(RequiredClientValidationOnlyAttribute), typeof(RequiredAttributeAdapter));

            ModelBinders.Binders.Add(typeof(DateTime), new CustomDateTimeModelBinder());
            ModelBinders.Binders.Add(typeof(DateTime?), new CustomDateTimeModelBinder());

            // create missing permissions
            var permissionService = IocManager.Resolve<IPermissionService>();
            var response = permissionService.UpdatePermissions();

            // Removing all the view engines
            ViewEngines.Engines.Clear();
            var ve = new CsRazorViewEngine();
            //Set custom view location cache for better performance
            ve.ViewLocationCache = new TwoLevelViewCache(ve.ViewLocationCache);
            //Add Razor Engine (which we are using)
            ViewEngines.Engines.Add(ve);
        }


        private void Application_Error(object sender, EventArgs e)
        {
            var lastException = Server.GetLastError();
            ErrorLog.GetDefault(HttpContext.Current).Log(new Error(lastException));
        }

        protected void Application_BeginRequest(Object source, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();

            HttpApplication application = (HttpApplication)source;
            HttpContext context = application.Context;

            string culture = "en-US";

            if(context.Request.Cookies["tempTranslateLanguage"] != null)
            {
                var language = context.Request.Cookies["tempTranslateLanguage"].Value.Split('/')[2];

                if(language == "fr")
                {
                    culture = "fr-FR";
                }
                else if (language == "es")
                {
                    culture = "es-ES";
                }

                Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(culture);
                Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

            }
            else if (context.Request.UserLanguages != null && Request.UserLanguages.Length > 0)
            {
                try
                {
                    culture = Request.UserLanguages[0];
                    Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(culture);
                    Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
                }
                catch (Exception ex)
                {
                    Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(culture);
                    Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
                }
            }
            else
            {
                Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(culture);
                Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
            }
        }
    }
}
