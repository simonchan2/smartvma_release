﻿using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System.Web.Mvc;
using Microsoft.Reporting.WebForms;
using SmartVMA.Web.Areas.Dealer.Models;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web;
using System.Configuration;

namespace SmartVMA.Web.Areas.Dealer.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class ReportingController : BaseServicePresentationController
    {
        private string reportingServiceUri;
        private string reportingServiceUserName;
        private string reportingServicePwd;
        private string reportingServiceDomain;
        public ReportingController(IAppContext appContext, ILofService service, IPdfDocumentService pdfService) : base(appContext, service, pdfService)
        {

            reportingServiceUri = appContext.ReportingServiceUri;
            reportingServiceUserName = appContext.ReportingServiceUserName;
            reportingServicePwd = appContext.ReportingServicePwd;
            reportingServiceDomain = appContext.ReportingServiceDomain;
        }

        public ActionResult Index()
        {
           
            return View();
        }

        public ActionResult GetReportViewer(string reportPath)
        {

            string userName = FormsAuthentication.Decrypt(HttpContext.Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name; //You have the UserName!

            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Remote;

            reportViewer.ServerReport.ReportPath = "/" + reportPath;
            reportViewer.ServerReport.ReportServerUrl = new System.Uri(reportingServiceUri);

            reportViewer.SizeToReportContent = false;
            reportViewer.Width = Unit.Pixel(1000);
            reportViewer.Height = Unit.Pixel(800);
            reportViewer.AsyncRendering = true;
            reportViewer.KeepSessionAlive = false;

            reportViewer.ServerReport.ReportServerCredentials = new ReportServerCredentials(reportingServiceUserName, reportingServicePwd, reportingServiceDomain);


            ReportParameter reportParameter = new ReportParameter("UserRoleCompany", userName);
            reportParameter.Visible = false;
            reportViewer.ServerReport.SetParameters(reportParameter);

            ViewBag.ReportViewer = reportViewer;

            return View();
        }
    }

}
