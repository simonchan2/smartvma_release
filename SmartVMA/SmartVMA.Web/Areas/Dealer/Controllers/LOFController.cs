﻿using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System.Web.Mvc;

namespace SmartVMA.Web.Areas.Dealer.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class LOFController : BaseServicePresentationController
    {
        private readonly ILofService _service;

        public LOFController(IAppContext appContext, ILofService service, IPdfDocumentService pdfService) : base(appContext, service, pdfService)
        {
            _service = service;
        }

        // GET: Dealer/LOF
        public ActionResult Index(int? carId, int? mileage, int? customerId, long? appointmentPresentationId, long? inspectionId, int? transmission, int? driveline, int? enteredMileage, int? dealerVehicleId, string vin, int? carIdUndetermined)
        {
            var model = _service.GetViewModel(carId, mileage, customerId, appointmentPresentationId, inspectionId, transmission, driveline, enteredMileage, dealerVehicleId, vin, carIdUndetermined);           
            return View("Index", model);
        }

        [HttpPost]
        public ActionResult CreateCustomer(DealerCustomerViewModel model)
        {
            if (ModelState.IsValid)
            {
                var dealerCustomerService = IocManager.Resolve<IDealerCustomerService>();
                var response = dealerCustomerService.Save(model);
                if (response.IsSuccess)
                {
                    var request = Map(model, response.SavedItemId, new SaveConfirmServiceRequestModel());
                    request.IsLof = true;
                    return SaveServiceConfirmation(request);
                }
                MapErrors(response.Errors);
            }
            return View("CreateCustomer", model);
        }

        private ConfirmServiceRequestModel Map(DealerCustomerViewModel src, long? dealerCustomerId, ConfirmServiceRequestModel dest)
        {
            dest.Services = src.ConfirmServicesRequest.Services;
            dest.CarId = src.ConfirmServicesRequest.CarId;
            dest.Mileage = src.ConfirmServicesRequest.Mileage;
            dest.EnteredMileage = src.ConfirmServicesRequest.EnteredMileage;
            dest.DealerCustomerId = (int?)dealerCustomerId;
            dest.AppointmentPresentationId = src.ConfirmServicesRequest.AppointmentPresentationId;
            dest.VIN = src.VIN;
            dest.Invoice = src.ConfirmServicesRequest.Invoice;
            dest.MenuLevel = src.ConfirmServicesRequest.MenuLevel;
            dest.Transmission = src.ConfirmServicesRequest.Transmission;
            dest.Driveline = src.ConfirmServicesRequest.Driveline;
            dest.MenuPackageOpCode = src.ConfirmServicesRequest.MenuPackageOpCode;

            return dest;
        }
        private SaveConfirmServiceRequestModel Map(DealerCustomerViewModel src, long? dealerCustomerId, SaveConfirmServiceRequestModel dest)
        {
            dest.Services = src.ConfirmServicesRequest.Services;
            dest.CarId = src.ConfirmServicesRequest.CarId;
            dest.Mileage = src.ConfirmServicesRequest.Mileage;
            dest.EnteredMileage = src.ConfirmServicesRequest.EnteredMileage;
            dest.DealerCustomerId = (int?)dealerCustomerId;
            dest.AppointmentPresentationId = src.ConfirmServicesRequest.AppointmentPresentationId;
            dest.VIN = src.VIN;
            dest.Invoice = src.ConfirmServicesRequest.Invoice;
            dest.IsInvoiceRequired = src.ConfirmServicesRequest.IsInvoiceRequired;
            dest.TaxChargesAmount = src.ConfirmServicesRequest.TaxChargesAmount;
            dest.ShopChargesAmount = src.ConfirmServicesRequest.ShopChargesAmount;
            dest.IsParked = src.ConfirmServicesRequest.IsParked;
            dest.IsDeclined = src.ConfirmServicesRequest.IsDeclined;
            dest.IsAccepted = src.ConfirmServicesRequest.IsAccepted;
            dest.MenuPackageOpCode = src.ConfirmServicesRequest.MenuPackageOpCode;
            dest.MenuLevel = src.ConfirmServicesRequest.MenuLevel;
            dest.Transmission = src.ConfirmServicesRequest.Transmission;
            dest.Driveline = src.ConfirmServicesRequest.Driveline;
            dest.InspectionId = src.ConfirmServicesRequest.InspectionId;

            return dest;
        }
    }
}