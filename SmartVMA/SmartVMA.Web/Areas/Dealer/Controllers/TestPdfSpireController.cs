﻿using System;
using System.Web.Mvc;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;
using SmartVMA.Core.Enums;
using Spire.Pdf;
using System.IO;
using SmartVMA.Infrastructure.FileManager;
using System.Threading;
using System.Drawing;
using Spire.Pdf.Graphics;

using SmartVMA.Web.Utilities;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Resources;
using Spire.Pdf.HtmlConverter;
using System.Net;

namespace SmartVMA.Web.Areas.Dealer.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class TestPdfSpireController : BaseController
    {
        private readonly IPdfDocumentService _service;

        public TestPdfSpireController(IAppContext appContext, IPdfDocumentService service) : base(appContext)
        {
            _service = service;
        }

        [AllowAnonymous]
        public ActionResult Index(long inspectionId, long appointmentPresentationID, int dealerId)
        {
            var filePath = string.Format("{0}\\{1}", dealerId, appointmentPresentationID);
            var fileName = string.Format("WC_TestPdfSpire_{0}.pdf", DateTime.Now.ToString("yyyyMMddHHmm"));

            var appContext = IocManager.Resolve<IAppContext>();
            var pdfFilePath = $@"{appContext.DocumentsFolder}\{filePath}\{fileName}";
            var pdfFilePathUrl = pdfFilePath.Replace("\\", "/");
            PdfDocument doc = new PdfDocument();

            var url = string.Format("{0}://{1}{2}/Dealer/PdfDocuments/VehicleInspectionPdf?inspectionId={3}&appointmentPresentationID={4}&dealerId={5}", Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port,
                inspectionId, appointmentPresentationID, dealerId);

            bool pageBreak = true;

            //this.RenderView(viewName, model);

            url = VehicleInspectionPdfAsString(inspectionId, appointmentPresentationID, dealerId);

            PdfHtmlLayoutFormat htmlLayoutFormat = new PdfHtmlLayoutFormat();
            //webBrowser load html whether Waiting
            htmlLayoutFormat.IsWaiting = false;
            //page setting
            PdfPageSettings setting = new PdfPageSettings();
            setting.Size = PdfPageSize.Letter;

            //string output = this.RenderView(viewName, model);

            //PdfPageSettings settings = new PdfPageSettings()

            //doc.LoadFromHTML(url, true, true, pageBreak);

            Thread thread = new Thread(() =>
            {
                //doc.LoadFromHTML(url, true, true, pageBreak);
                doc.LoadFromHTML(url, pageBreak, setting, htmlLayoutFormat);
            });
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            thread.Join();

            Stream stream = new MemoryStream();
            doc.SaveToStream(stream);
            stream.Position = 0;

            //PdfUnitConvertor unitCvtr = new PdfUnitConvertor();
            //PdfMargins margin = new PdfMargins();
            //margin.Top = unitCvtr.ConvertUnits(2.54f, PdfGraphicsUnit.Centimeter, PdfGraphicsUnit.Point);
            //margin.Bottom = margin.Top;
            //margin.Left = unitCvtr.ConvertUnits(4.17f, PdfGraphicsUnit.Centimeter, PdfGraphicsUnit.Point);
            //margin.Right = margin.Left;
            //SetDocumentTemplate(doc, PdfPageSize.Letter, margin);            

            doc.Close();

            var fileManager = IocManager.Resolve<IFileManager>();
            var byteArray = fileManager.ReadStreamToByte(stream);
            fileManager.Save(pdfFilePath, byteArray, true);

            ViewBag.Msg = string.Format("Export was successful, file could be found here: {0}://{1}{2}/Files/Download?path={3}", Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port, pdfFilePathUrl);
            return View();
        }

        [AllowAnonymous]
        public ActionResult PdfFromVehicleStringHtml(long inspectionId, long appointmentPresentationID, int dealerId)
        {
            var filePath = string.Format("{0}\\{1}", dealerId, appointmentPresentationID);
            var fileName = string.Format("WC_TestPdfSpire_String_{0}.pdf", DateTime.Now.ToString("yyyyMMddHHmm"));

            var appContext = IocManager.Resolve<IAppContext>();
            var pdfFilePath = $@"{appContext.DocumentsFolder}\{filePath}\{fileName}";
            var pdfFilePathUrl = pdfFilePath.Replace("\\", "/");
            PdfDocument doc = new PdfDocument();

            var url = VehicleInspectionPdfAsString(inspectionId, appointmentPresentationID, dealerId);
            bool pageBreak = true;            

            PdfHtmlLayoutFormat htmlLayoutFormat = new PdfHtmlLayoutFormat();
            //webBrowser load html whether Waiting
            htmlLayoutFormat.IsWaiting = false;
            //page setting
            PdfPageSettings setting = new PdfPageSettings();
            setting.Size = PdfPageSize.Letter;

            Thread thread = new Thread(() =>
            {
                doc.LoadFromHTML(url, pageBreak, setting, htmlLayoutFormat);
            });
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            thread.Join();

            Stream stream = new MemoryStream();
            doc.SaveToStream(stream);
            stream.Position = 0;    

            doc.Close();

            var fileManager = IocManager.Resolve<IFileManager>();
            var byteArray = fileManager.ReadStreamToByte(stream);
            fileManager.Save(pdfFilePath, byteArray, true);

            ViewBag.Msg = string.Format("Export was successful, file could be found here: {0}://{1}{2}/Files/Download?path={3}", Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port, pdfFilePathUrl);
            return View();
        }

        [AllowAnonymous]
        public ActionResult PdfFromVehicleHtml(long inspectionId, long appointmentPresentationID, int dealerId)
        {
            var filePath = string.Format("{0}\\{1}", dealerId, appointmentPresentationID);
            var fileName = string.Format("WC_TestPdfSpire_{0}.pdf", DateTime.Now.ToString("yyyyMMddHHmm"));

            var appContext = IocManager.Resolve<IAppContext>();
            var pdfFilePath = $@"{appContext.DocumentsFolder}\{filePath}\{fileName}";
            var pdfFilePathUrl = pdfFilePath.Replace("\\", "/");
            PdfDocument doc = new PdfDocument();

            var url = string.Format("{0}://{1}{2}/Dealer/PdfDocuments/VehicleInspectionPdf?inspectionId={3}&appointmentPresentationID={4}&dealerId={5}", Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port,
                inspectionId, appointmentPresentationID, dealerId);

            bool pageBreak = true;

            Thread thread = new Thread(() =>
            {
                doc.LoadFromHTML(url, true, true, pageBreak);
            });
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            thread.Join();

            Stream stream = new MemoryStream();
            doc.SaveToStream(stream);
            stream.Position = 0;    

            doc.Close();

            var fileManager = IocManager.Resolve<IFileManager>();
            var byteArray = fileManager.ReadStreamToByte(stream);
            fileManager.Save(pdfFilePath, byteArray, true);

            ViewBag.Msg = string.Format("Export was successful, file could be found here: {0}://{1}{2}/Files/Download?path={3}", Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port, pdfFilePathUrl);
            return View();
        }

        [AllowAnonymous]
        public ActionResult PdfFromStaticStringHtml()
        {
            var filePath = string.Format("1089");
            var fileName = string.Format("StaticSite_TestPdfSpire_String_{0}.pdf", DateTime.Now.ToString("yyyyMMddHHmm"));

            var appContext = IocManager.Resolve<IAppContext>();
            var pdfFilePath = $@"{appContext.DocumentsFolder}\{filePath}\{fileName}";
            var pdfFilePathUrl = pdfFilePath.Replace("\\", "/");
            PdfDocument doc = new PdfDocument();

            var url = "https://www.bgprod.com/";

            WebClient wc = new WebClient();
            var urlString = wc.DownloadString(url);

            bool pageBreak = true;

            PdfHtmlLayoutFormat htmlLayoutFormat = new PdfHtmlLayoutFormat();
            //webBrowser load html whether Waiting
            htmlLayoutFormat.IsWaiting = false;
            //page setting
            PdfPageSettings setting = new PdfPageSettings();
            setting.Size = PdfPageSize.Letter;

            Thread thread = new Thread(() =>
            {
                doc.LoadFromHTML(urlString, pageBreak, setting, htmlLayoutFormat);
            });
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            thread.Join();

            Stream stream = new MemoryStream();
            doc.SaveToStream(stream);
            stream.Position = 0;

            doc.Close();

            var fileManager = IocManager.Resolve<IFileManager>();
            var byteArray = fileManager.ReadStreamToByte(stream);
            fileManager.Save(pdfFilePath, byteArray, true);

            ViewBag.Msg = string.Format("Export was successful, file could be found here: {0}://{1}{2}/Files/Download?path={3}", Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port, pdfFilePathUrl);
            return View();
        }

        [AllowAnonymous]
        public ActionResult PdfFromStaticHtml()
        {
            var filePath = "1089";
            var fileName = string.Format("StaticSite_TestPdfSpire_{0}.pdf", DateTime.Now.ToString("yyyyMMddHHmm"));

            var appContext = IocManager.Resolve<IAppContext>();
            var pdfFilePath = $@"{appContext.DocumentsFolder}\{filePath}\{fileName}";
            var pdfFilePathUrl = pdfFilePath.Replace("\\", "/");
            PdfDocument doc = new PdfDocument();

            var url = "https://www.bgprod.com/";

            bool pageBreak = true;

            Thread thread = new Thread(() =>
            {
                doc.LoadFromHTML(url, true, true, pageBreak);
            });
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            thread.Join();

            Stream stream = new MemoryStream();
            doc.SaveToStream(stream);
            stream.Position = 0;

            doc.Close();

            var fileManager = IocManager.Resolve<IFileManager>();
            var byteArray = fileManager.ReadStreamToByte(stream);
            fileManager.Save(pdfFilePath, byteArray, true);

            ViewBag.Msg = string.Format("Export was successful, file could be found here: {0}://{1}{2}/Files/Download?path={3}", Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port, pdfFilePathUrl);
            return View();
        }

        //[AllowAnonymous]
        //public ActionResult Index(long inspectionId, long appointmentPresentationID, int dealerId)
        //{
        //    string output = this.RenderView(viewName, model);
        //    PdfDocument pdf = PdfGenerator.GeneratePdf(output, PageSize.A4);
        //    byte[] byteArray;
        //    using (MemoryStream ms = new MemoryStream())
        //    {
        //        pdf.Save(ms, false);
        //        byteArray = new byte[ms.Length];
        //        ms.Seek(0, SeekOrigin.Begin);
        //        ms.Flush();
        //        ms.Read(byteArray, 0, (int)ms.Length);
        //    }
        //    var appContext = IocManager.Resolve<IAppContext>();
        //    var pdfFilePath = $@"{appContext.DocumentsFolder}\{filePath}\{fileName}";
        //    var fileManager = IocManager.Resolve<IFileManager>();
        //    fileManager.Save(pdfFilePath, byteArray);

        //    var filePath = string.Format("{0}\\{1}", dealerId, appointmentPresentationID);
        //    var fileName = string.Format("WC_TestPdfSpire_{0}.pdf", DateTime.Now.ToString("yyyyMMddHHmm"));

        //    var appContext = IocManager.Resolve<IAppContext>();
        //    var pdfFilePath = $@"{appContext.DocumentsFolder}\{filePath}\{fileName}";
        //    var pdfFilePathUrl = pdfFilePath.Replace("\\", "/");
        //    PdfDocument doc = new PdfDocument();

        //    var url = string.Format("{0}://{1}{2}/Dealer/PdfDocuments/VehicleInspectionPdf?inspectionId={3}&appointmentPresentationID={4}&dealerId={5}", Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port,
        //        inspectionId, appointmentPresentationID, dealerId);

        //    bool pageBreak = true;

        //    //PdfPageSettings settings = new PdfPageSettings()

        //    Thread thread = new Thread(() =>
        //    {
        //        doc.LoadFromHTML(url, true, true, pageBreak);
        //    });
        //    thread.SetApartmentState(ApartmentState.STA);
        //    thread.Start();
        //    thread.Join();

        //    Stream stream = new MemoryStream();
        //    doc.SaveToStream(stream);
        //    stream.Position = 0;        

        //    doc.Close();

        //    var fileManager = IocManager.Resolve<IFileManager>();
        //    var byteArray = fileManager.ReadStreamToByte(stream);
        //    fileManager.Save(pdfFilePath, byteArray, true);

        //    ViewBag.Msg = string.Format("Export was successful, file could be found here: {0}://{1}{2}/Files/Download?path={3}", Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port, pdfFilePathUrl);
        //    return View();
        //}

        [AllowAnonymous]
        public ActionResult PlainHtmltPdf()
        {
            var filePath = "1089";
            var fileName = string.Format("TestEmptyPdfSpire_{0}.pdf", DateTime.Now.ToString("yyyyMMddHHmm"));

            var appContext = IocManager.Resolve<IAppContext>();
            var pdfFilePath = $@"{appContext.DocumentsFolder}\{filePath}\{fileName}";
            var pdfFilePathUrl = pdfFilePath.Replace("\\", "/");
            PdfDocument doc = new PdfDocument();
            string text = "Plain text PDF testing";
            PdfSection section = doc.Sections.Add();
            PdfPageBase page = section.Pages.Add();
            PdfFont font = new PdfFont(PdfFontFamily.Helvetica, 11);
            PdfStringFormat format = new PdfStringFormat();
            format.LineSpacing = 20f;
            PdfBrush brush = PdfBrushes.Green;
            PdfTextWidget textWidget = new PdfTextWidget(text, font, brush);
            float y = 0;
            PdfTextLayout textLayout = new PdfTextLayout();
            textLayout.Break = PdfLayoutBreakType.FitPage;
            textLayout.Layout = PdfLayoutType.Paginate;
            RectangleF bounds = new RectangleF(new PointF(0, y), page.Canvas.ClientSize);
            textWidget.StringFormat = format;
            textWidget.Draw(page, bounds, textLayout);

            Stream stream = new MemoryStream();
            doc.SaveToStream(stream);
            stream.Position = 0;         

            doc.Close();

            var fileManager = IocManager.Resolve<IFileManager>();
            var byteArray = fileManager.ReadStreamToByte(stream);
            fileManager.Save(pdfFilePath, byteArray, true);

            ViewBag.Msg = string.Format("Export was successful, file could be found here: {0}://{1}{2}/Files/Download?path={3}", Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port, pdfFilePathUrl);
            return View();
        }

        [AllowAnonymous]
        public string VehicleInspectionPdfAsString(long? inspectionId, long? appointmentPresentationID, int dealerId)
        {
            //inspectionId = 222;
            //appointmentPresentationID = 590;
            //dealerId = 1089;
            if (inspectionId.HasValue && inspectionId.Value == -1)
            {
                inspectionId = null;
            }
            if (appointmentPresentationID.HasValue && appointmentPresentationID.Value == -1)
            {
                appointmentPresentationID = null;
            }

            var companySettings = IocManager.Resolve<ICompanyService>();
            var appPresentation = IocManager.Resolve<IAppointmentPresentationService>();
            var pdfDocumentService = IocManager.Resolve<IPdfDocumentService>();
            var fileManager = IocManager.Resolve<IFileManager>();

            string measurementUnit = companySettings.GetSettingAsInteger(CompanySettingsNames.MeasurementUnit) == (int)MeasurementUnits.Imperial ? Labels.Miles : Labels.km;

            PdfHeaderViewModel header = new PdfHeaderViewModel();

            header.CustomerInfo = pdfDocumentService.GetPdfHeaderCustomerInfoViewModel(dealerId, appointmentPresentationID.HasValue ? appointmentPresentationID.Value : -1);
            if (header.CustomerInfo != null)
            {
                header.CustomerInfo.DateString = header.CustomerInfo.Date.ToString(companySettings.GetDateTimeFormat(CompanySettingsNames.DateFormat));
            }
            else
            {
                header.CustomerInfo = new PdfHeaderCustomerInfoViewModel();
            }

            var printAdvisorInformation = companySettings.GetSettingAsBool(CompanySettingsNames.PrintAdvisorInformation);
            if (printAdvisorInformation && AppContext.CurrentIdentity.UserId.HasValue)
            {
                header.ShowAdvisor = true;
                header.Advisor = pdfDocumentService.GetPdfHeaderAdvisorViewModel(AppContext.CurrentIdentity.UserId.Value);
            }

            header.ShowDealer = true;
            header.Dealer = pdfDocumentService.GetPdfHeaderDealerViewModel();
            //header.Dealer.LogoImg = companySettings.GetCompanyLogoPath(CompanySettingsNames.CompanyLogo);
            var logoPath = Request.Url.Host.IndexOf("localhost") > -1 ? "UserContent/Companies//1089/75a8ebc4-0578-4a6b-81b7-4025c8c0bb52.png" : "UserContent/Companies//1089/4e2e02fd-a59f-4d04-8290-dfc17f16e264.png";
            //var stream = fileManager.Read(header.Dealer.LogoImg);
            var stream = fileManager.Read(logoPath);
            header.Dealer.LogoImgByteArray = fileManager.ReadStreamToByte(stream);
            header.CustomerInfo.OdometerReading = string.Format("{0} {1}", header.CustomerInfo.OdometerReading, measurementUnit);

            var footer = pdfDocumentService.GetPdfFooterViewModel(appointmentPresentationID.HasValue ? appointmentPresentationID.Value : -1, header.CustomerInfo.Date == DateTime.MinValue ? DateTime.Now : header.CustomerInfo.Date);

            var modelWA = pdfDocumentService.GetWalkaroundInspectionPdfViewModel(inspectionId, appointmentPresentationID, true);

            modelWA.Header = header;

            modelWA.Footer = footer;

            return this.RenderView("PdfWalkaroundInspection", modelWA);
        }
    }
}