﻿using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;
using System.Web.Mvc;
using SmartVMA.Infrastructure.AppContext;
using System;
using System.Collections.Generic;
using Rotativa;
using SmartVMA.Core.Enums;
using SmartVMA.Infrastructure.FileManager;

namespace SmartVMA.Web.Areas.Dealer.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class VehicleMaintenanceHistoryController : BaseController
    {
        private IAppointmentService _appointmentService;
        public VehicleMaintenanceHistoryController(IAppContext appContext, IAppointmentService appointmentService) : base(appContext)
        {
            _appointmentService = appointmentService;
        }

        // GET: Appointment/Appointment
        public ActionResult Index(string vin)
        {
            List<AppointmentServiceViewModel> model = _appointmentService.GetAppointmentServiceModel(vin);
            ViewBag.VIN = vin;

            return View(model);
        }

        public string ViewAppointmentInfoPdfFile(string vin, bool isDeclined)
        {
            var companySettings = IocManager.Resolve<ICompanyService>();
            var pdfDocumentService = IocManager.Resolve<IPdfDocumentService>();
            var fileManager = IocManager.Resolve<IFileManager>();
            var appPresentation = IocManager.Resolve<IAppointmentPresentationService>();

            PdfHeaderViewModel header = new PdfHeaderViewModel();

            header.CustomerInfo = pdfDocumentService.GetPdfHeaderCustomerInfoViewModelFromVin(vin);
            header.CustomerInfo.ShowVehicleHistoryCustomerInfo = true;

            var printAdvisorInformation = companySettings.GetSettingAsBool(CompanySettingsNames.PrintAdvisorInformation);
            if (printAdvisorInformation && AppContext.CurrentIdentity.UserId.HasValue)
            {
                header.ShowAdvisor = true;
                header.Advisor = pdfDocumentService.GetPdfHeaderAdvisorViewModel(AppContext.CurrentIdentity.UserId.Value);
            }

            header.ShowDealer = true;
            header.Dealer = pdfDocumentService.GetPdfHeaderDealerViewModel();
            header.Dealer.LogoImg = companySettings.GetCompanyLogoPath(CompanySettingsNames.CompanyLogo);
            var stream = fileManager.Read(header.Dealer.LogoImg);
            header.Dealer.LogoImgByteArray = fileManager.ReadStreamToByte(stream);

            var footer = pdfDocumentService.GetPdfFooterViewModel(-1, DateTime.Now);

            ViewBag.LogoImg = companySettings.GetCompanyLogo(CompanySettingsNames.CompanyLogo);
            VehicleHistoryPdfViewModel model = new VehicleHistoryPdfViewModel();
            model.Header = header;
            model.Services = _appointmentService.GetAppointmentServiceModel(vin);
            model.Footer = footer;

            var view = isDeclined ? "PDFAppointmentInfoDeclined" : "PDFAppointmentInfo";

            var pdf = new ViewAsPdf(view, model);

            string headerPath = string.Format("{0}://{1}{2}/Dealer/PdfDocuments/PdfEmptyHeader",
                    Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port);
            string footerPath = string.Format("{0}://{1}{2}/Dealer/PdfDocuments/PdfFooter?printed={3}&form={4}",
                    Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port, footer.Printed, footer.From);

            string customSwitches = string.Format("--header-html {0} --footer-html {1}", headerPath, footerPath);
            pdf.CustomSwitches = customSwitches;
            var filePath = string.Format("VehicleHistory");
            var fileName = string.Format("VehicleHistory_{0}.pdf", DateTime.Now.ToString("yyyyMMddHHmm"));
            var url = AppContext.MapUrl($"/UserContent/SmartVMADocuments/{filePath}/{fileName}");

            ExportPdfFile(pdf, filePath, fileName);

            if (companySettings.GetSettingAsBool(CompanySettingsNames.IsRemotePrint) && AppContext.CurrentIdentity.TenantId.HasValue)
            {
                string prnJobTypeCode = PrintDocumentType.HIST.ToString();
                pdfDocumentService.AddPdfDocumentToPrintQueue(AppContext.CurrentIdentity.TenantId.Value, AppContext.CurrentIdentity.UserId, url, 0, prnJobTypeCode);
            }

            return url;
        }
    }
}