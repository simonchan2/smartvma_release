﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PdfSharp;
using PdfSharp.Pdf;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;
using SmartVMA.Web.Utilities;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using SmartVMA.Core.Enums;
using SmartVMA.Infrastructure.FileManager;
using PdfSharp.Drawing;
using Elmah;
using System.IO;
using Rotativa;

namespace SmartVMA.Web.Areas.Dealer.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class TestPdfSharpController : BaseController
    {
        private readonly IPdfDocumentService _service;
        
        public TestPdfSharpController(IAppContext appContext, IPdfDocumentService service) : base(appContext)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            string msg = string.Empty;
                        
            try
            {
                var appointmentPresentationID = 590;
                var dealerId = 1089;
                var _pdfDocumentService = IocManager.Resolve<IPdfDocumentService>();
                var model = _pdfDocumentService.GetCustomerCopyPdfViewModel(appointmentPresentationID);

                model.Header = _pdfDocumentService.GetPdfHeaderViewModel(dealerId, appointmentPresentationID);
                var companyService = IocManager.Resolve<ICompanyService>();
                model.Header.Dealer.LogoImg = companyService.GetCompanyLogo(CompanySettingsNames.CompanyLogo);

                model.Footer = _pdfDocumentService.GetPdfFooterViewModel(appointmentPresentationID, null);

                //var pdf = new ViewAsPdf("PdfCustomerCopy", model);

                //var filePath = string.Format("{0}\\{1}", dealerId, appointmentPresentationID);
                //var fileName = string.Format("CC_VIN{0}_Invoice{1}.pdf", model.Header.CustomerInfo.VIN, model.Header.CustomerInfo.InvoiceNumberText);
                ////ExportPdfFile(pdf, filePath, fileName);

                ////pdf.
                //var appContext = IocManager.Resolve<IAppContext>();
                //var pdfFilePath = $@"{appContext.DocumentsFolder}\{filePath}\{fileName}";
                //var byteArray = pdf.BuildPdf(ControllerContext);
                //var fileManager = IocManager.Resolve<IFileManager>();
                //fileManager.Save(pdfFilePath, byteArray, true);

                //msg = string.Format("Export was successful, file could be found here: {0}", pdfFilePath);

                //ViewBag.Msg = msg;
                return View(model);
            }
            catch (Exception ex)
            {
                msg = ex.Message;

                ViewBag.Msg = msg;
                return View();
            }

           
        }
        // GET: Dealer/TestPdfSharp
        //public ActionResult Index()
        //{
        //    //var model = new TestPdfSharpViewModel();
        //    //var pdfDocumentService = IocManager.Resolve<IPdfDocumentService>();
        //    //var appointmentPresentationID = 200;
        //    //var dealerId = 1089;
        //    //var model = pdfDocumentService.GetCustomerCopyPdfViewModel(appointmentPresentationID);
        //    //model.Header = pdfDocumentService.GetPdfHeaderViewModel(dealerId, appointmentPresentationID);
        //    //var companyService = IocManager.Resolve<ICompanyService>();
        //    //model.Header.LogoImg = companyService.GetCompanyLogo(CompanySettingsNames.CompanyLogo);

        //    //model.Footer = pdfDocumentService.GetPdfFooterViewModel(dealerId, appointmentPresentationID);
        //    //string output = this.RenderView("Index", model);

        //    //PdfDocument pdf = PdfGenerator.GeneratePdf(output, PageSize.A4);
        //    //string fileName = string.Format("{0}_TestPdfSharpCustomerCopy.pdf", DateTime.Now.ToString("yyyyMMdd"));
        //    //string folderName = AppDomain.CurrentDomain.BaseDirectory + "UserContent//SmartVMADocuments//";
        //    //string filePath = folderName + fileName;
        //    ////pdf.CanSave()

        //    PdfDocument pdf = PdfGenerator.GeneratePdf("Empty Pdf", PageSize.A4);

        //    //PdfDocument pdf = new PdfDocument(); //PdfGenerator.GeneratePdf("Empty Pdf", PageSize.A4);


        //    //var config = new PdfGenerateConfig();
        //    //config.PageSize = PageSize.A4;
        //    //config.SetMargins(20);


        //    //AddPdfPages(pdf, "Empty Pdf", config);

        //    //pdf.PageCount

        //    //var error = string.Format("Before adding page: PageCount: {0}, ToString: {1}, Title: {2}, Subject: {3}, Keywords: {4},  FullPath: {5}, FileSize: {6}, IsReadOnly: {7}, Version: {8}",
        //    //    pdf.PageCount, pdf.ToString(), pdf.Info.Title, pdf.Info.Subject, pdf.Info.Keywords, pdf.FullPath, pdf.FileSize, pdf.IsReadOnly, pdf.Version);

        //    //Exception ex = new Exception(error);

        //    //ErrorLog.GetDefault(System.Web.HttpContext.Current).Log(new Error(ex));

        //    //var page = pdf.AddPage();

        //    //error = string.Format("After adding page: PageCount: {0}, ToString: {1}, Title: {2}, Subject: {3}, Keywords: {4},  FullPath: {5}, FileSize: {6}, IsReadOnly: {7}, Version: {8}",
        //    //    pdf.PageCount, pdf.ToString(), pdf.Info.Title, pdf.Info.Subject, pdf.Info.Keywords, pdf.FullPath, pdf.FileSize, pdf.IsReadOnly, pdf.Version);

        //    //ex = new Exception(error);

        //    //ErrorLog.GetDefault(System.Web.HttpContext.Current).Log(new Error(ex));
        //    //var gfx = XGraphics.FromPdfPage(page);


        //    //var font = new XFont("Times New Roman", 16);


        //    //gfx.DrawString("Empty Pdf 2 page", font, XBrushes.Black,
        //    //    new XPoint(XUnit.FromMillimeter(10),
        //    //               XUnit.FromMillimeter(10)),
        //    //               XStringFormats.TopLeft);
        //    //PdfPage page = new PdfPage();
        //    //PdfGenerator.GeneratePdf

        //    //pdf.Pages.Add();

        //    var appContext = IocManager.Resolve<IAppContext>();
        //    string folderName = "UserContent\\Images";// appContext.TempFolderName;
        //    string fileName = string.Format("{0}_TestPdfSharpCustomerCopy.pdf", DateTime.Now.ToString("yyyyMMdd"));
        //    string filePath = string.Format("{0}\\{1}", folderName, fileName);
        //    filePath = appContext.MapPath(filePath);

        //    if (!Directory.Exists(filePath))
        //    {
        //        Directory.CreateDirectory(filePath);
        //    }

        //    pdf.Save(filePath);
        //    //string fileName = string.Format("{0}_TestPdfSharpCustomerCopy.pdf", DateTime.Now.ToString("yyyyMMdd"));
        //    //string filePath = "1089";

        //    //var appContext = IocManager.Resolve<IAppContext>();
        //    //var pdfFilePath = $@"{appContext.DocumentsFolder}\{filePath}\{fileName}";
        //    //var fileManager = IocManager.Resolve<IFileManager>();

        //    //var mappedPath = appContext.MapPath(pdfFilePath);

        //    ////var fileManager = IocManager.Resolve<IFileManager>();
        //    ////fileManager.Save
        //    //pdf.Save(mappedPath);
        //    return View();

        //    //pdf.Save(filePath);
        //    //return View(model);

        //    //var appContext = IocManager.Resolve<IAppContext>();
        //    ////var pdfFilePath = $@"{appContext.DocumentsFolder}\{filePath}\{fileName}";
        //    //var fileManager = IocManager.Resolve<IFileManager>();
        //    //var stream = fileManager.Read(filePath);
        //    //var bytes = new byte[stream.Length];
        //    //stream.Read(bytes, 0, bytes.Length);
        //    //string contentType = MimeMapping.GetMimeMapping(filePath);

        //    //Response.Clear();

        //    //Response.ContentType = contentType;
        //    //Response.AddHeader("Accept-Header", stream.Length.ToString());
        //    //Response.AddHeader("Content-Disposition", string.Format("Inline; filename={0}", fileName));
        //    //Response.AddHeader("Content-Length", stream.Length.ToString());
        //    //Response.BinaryWrite(bytes);

        //    //Response.End();
        //}

        /*public static void AddPdfPages(PdfDocument document, string html, PdfGenerateConfig config)
        {
            XSize orgPageSize;
            // get the size of each page to layout the HTML in
            if (config.PageSize != PageSize.Undefined)
                orgPageSize = PageSizeConverter.ToSize(config.PageSize);
            else
                orgPageSize = config.ManualPageSize;

            if (config.PageOrientation == PageOrientation.Landscape)
            {
                // invert pagesize for landscape
                orgPageSize = new XSize(orgPageSize.Height, orgPageSize.Width);
            }

            var pageSize = new XSize(orgPageSize.Width - config.MarginLeft - config.MarginRight, orgPageSize.Height - config.MarginTop - config.MarginBottom);

            var error = string.Format("Page size: orgPageSizeHeight: {0}, orgPageSizeWidth: {1}, orgPageSizeIsEmpty: {2}, pageSizeHeight: {3}, pageSizeWidth: {4},  pageSizeIsEmpty: {5}",
                orgPageSize.Height, orgPageSize.Width, orgPageSize.IsEmpty, pageSize.Height, pageSize.Width, pageSize.IsEmpty);
            Exception ex = new Exception(error);
            ErrorLog.GetDefault(System.Web.HttpContext.Current).Log(new Error(ex));

            if (!string.IsNullOrEmpty(html))
            {
                using (var container = new HtmlContainer())
                {
                    container.Location = new XPoint(config.MarginLeft, config.MarginTop);
                    container.MaxSize = new XSize(pageSize.Width, 0);
                    container.SetHtml(html);

                    error = string.Format("Container info: containerActualSizeHeight: {0}, containerActualSizeWidth: {1}, containerActualSizeIsEmpty: {2}, containerGetHtml: {3}, html: {4}",
                container.ActualSize.Height, container.ActualSize.Width, container.ActualSize.IsEmpty, container.GetHtml(TheArtOfDev.HtmlRenderer.Core.Entities.HtmlGenerationStyle.None), html);
                    ex = new Exception(error);
                    ErrorLog.GetDefault(System.Web.HttpContext.Current).Log(new Error(ex));

                    // layout the HTML with the page width restriction to know how many pages are required
                    //using (var measure = XGraphics.CreateMeasureContext(pageSize, XGraphicsUnit.Point, XPageDirection.Downwards))
                    //{
                    //    container.PerformLayout(measure);
                    //    //measure.w
                    //}

                    // while there is un-rendered HTML, create another PDF page and render with proper offset for the next page
                    double scrollOffset = 0;

                    error = string.Format("scrollOffset: {0}, -container.ActualSize.Height: {1}, scrollOffset > -container.ActualSize.Height: {2}",
               scrollOffset, -container.ActualSize.Height, scrollOffset > -container.ActualSize.Height);
                    ex = new Exception(error);
                    ErrorLog.GetDefault(System.Web.HttpContext.Current).Log(new Error(ex));

                    while (scrollOffset >= -container.ActualSize.Height)
                    {
                        var page = document.AddPage();
                        page.Height = orgPageSize.Height;
                        page.Width = orgPageSize.Width;

                        error = string.Format("Page info: pageHeight: {0}, pageWidth: {1}, scrollOffset: {2}",
                page.Height, page.Width, scrollOffset);
                        ex = new Exception(error);
                        ErrorLog.GetDefault(System.Web.HttpContext.Current).Log(new Error(ex));

                        using (var g = XGraphics.FromPdfPage(page))
                        {
                            g.IntersectClip(new XRect(0, 0, page.Width, page.Height));

                            container.ScrollOffset = new XPoint(0, scrollOffset);
                            container.PerformPaint(g);                            
                        }
                        scrollOffset -= pageSize.Height;
                    }
                }
            }
        }*/
    }
}