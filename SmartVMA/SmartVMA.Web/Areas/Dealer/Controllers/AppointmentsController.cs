﻿using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;
using System.Web.Mvc;
using SmartVMA.Infrastructure.AppContext;
using System;
using Rotativa;
using System.Configuration;
using System.Web;
using System.IO;
using System.Collections.Generic;
using SmartVMA.Core.Enums;

namespace SmartVMA.Web.Areas.Dealer.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class AppointmentsController : BaseController
    {
        private IAppointmentService _appointmentService;
        public AppointmentsController(IAppContext appContext, IAppointmentService appointmentService) : base(appContext)
        {
            _appointmentService = appointmentService;
        }

        // GET: Appointment/Appointment
        public ActionResult Index()
        {
            var appContext = IocManager.Resolve<IAppContext>();
            var advisorIds = new List<long>();
            advisorIds.Add(appContext.CurrentIdentity.UserId.Value);
            ViewBag.AdvisorIds = advisorIds;
            ViewBag.AppointmentFilterStatus = (int)AppointmentFilterStatus.All;
            return View();
        }

        public JsonResult GetCalendarAppointments(string startTimeISO, string calendarView, string advisorIds, string presentationStatus)
        {
            DateTime startTime = DateTime.Parse(startTimeISO);
            DateTime endTime;
            if (calendarView == "agendaDay" || calendarView == "customWeek")
            {
                startTime = new DateTime(startTime.Year, startTime.Month, startTime.Day, 0, 0, 0, 0);
                endTime = startTime.AddDays(1).AddMilliseconds(-1);
            }
            else if (calendarView == "customWeek")
            {
                startTime = startTime.AddDays(-(int)startTime.DayOfWeek + (int)DayOfWeek.Monday);
                startTime = new DateTime(startTime.Year, startTime.Month, startTime.Day, 0, 0, 0, 0);
                endTime = startTime.AddDays(7).AddMilliseconds(-1);
            }
            else
            {
                startTime = new DateTime(startTime.Year, startTime.Month, 1, 0, 0, 0, 0);
                endTime = startTime.AddMonths(1).AddMilliseconds(-1);
            }

            var model = _appointmentService.GetCalendarAppointmentsModel(startTime, endTime, advisorIds, presentationStatus);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSearchAppointments(string customerName, string customerNr, string customerPhone, string vin, string vehicleStock, string licensePlate)
        {
            var model = _appointmentService.GetSearchAppointmentsModel(customerName, customerNr, customerPhone, vin, vehicleStock, licensePlate);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSearchAppointments(SearchCustomerViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView("_SearchAppointmentsResults", new AppointmentIndexViewModel());
            }

            AppointmentIndexViewModel searchAppointmentsResults = _appointmentService.GetSearchAppointmentsModel((model.CustomerName != null ? model.CustomerName.Trim() : model.CustomerName),
                                                                                                                 (model.CustomerNumber != null ? model.CustomerNumber.Trim() : model.CustomerNumber),
                                                                                                                 (model.CustomerPhone != null ? model.CustomerPhone.Trim() : model.CustomerPhone),
                                                                                                                 (model.Vin != null ? model.Vin.Trim() : model.Vin),
                                                                                                                 (model.VehicleStockNumber != null ? model.VehicleStockNumber.Trim() : model.VehicleStockNumber),
                                                                                                                 (model.LicensePlate != null ? model.LicensePlate.Trim() : model.LicensePlate));
            return PartialView("_SearchAppointmentsResults", searchAppointmentsResults);
        }

        public void CreateAppointmentPresentation(long appointmentID)
        {
            _appointmentService.CreateAppointmentPresentation(appointmentID, AppContext.CurrentIdentity.UserId, AppointmentPresentationStatus.DMS.ToString());
        }

        public ActionResult AcceptedOptions(long appointmentPresentationID, int dealerCustomerID, int dealerID)
        {
            OptionsMenu model = new OptionsMenu(appointmentPresentationID, dealerCustomerID, dealerID, null);
            return PartialView("_AcceptedOptions", model);
        }

        public ActionResult ParkedOptions(int appointmentPresentationID, int dealerID, int dealerCustomerID, long? advisorId, int mileage, int? carId, int? dealerVehicleId, string presentationStatus, string vin, string appointmentDate, long? appointmentId)
        {
            OptionsMenu model = new OptionsMenu(appointmentPresentationID, dealerCustomerID, dealerID, mileage, carId, presentationStatus, dealerVehicleId, vin, null, null, null, advisorId, null, 1, null, appointmentId);
            if (string.IsNullOrEmpty(appointmentDate))
            {
                model.AppointmentDate = null;
            }
            else
            {
                model.AppointmentDate = DateTime.Parse(appointmentDate);
            }
            return PartialView("_ParkedOptions", model);
        }
    }
}