﻿using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;
using SmartVMA.Infrastructure.AppContext;
using System;
using System.Web;
using SmartVMA.Core.Enums;
using SmartVMA.Infrastructure.FileManager;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Net.Mail;
using SmartVMA.Resources;

namespace SmartVMA.Web.Areas.Dealer.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    
    public class PdfDocumentsController : BaseController
    {
        private IPdfDocumentService _pdfDocumentService;
        public PdfDocumentsController(IAppContext appContext, IPdfDocumentService pdfDocumentService) : base(appContext)
        {
            _pdfDocumentService = pdfDocumentService;
        }

        public ActionResult Index()
        {
            SearchDocumentsViewModel model = new SearchDocumentsViewModel();
            var companyService = IocManager.Resolve<ICompanyService>();
            var defaultDate = DateTime.Now.Date;
            var defaultDateString = defaultDate.ToString(companyService.GetDateTimeFormat(CompanySettingsNames.DateFormat));
            model.StartDateString = defaultDateString;
            model.EndDateString = defaultDateString;
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult GetDocumentSearch(SearchDocumentsViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView("_SearchDocumentsResults", new PdfDocumentIndexViewModel());
            }

            if (!model.StartDate.HasValue && !model.EndDate.HasValue)
            {
                var defaultDate = DateTime.Now.Date;
                model.StartDate = defaultDate;
                model.EndDate = defaultDate;
            }
            PdfDocumentIndexViewModel searchDocumentsResults = _pdfDocumentService.GetDocuments(model.VIN, model.InvoiceNumber, model.StartDate, model.EndDate);
            return PartialView("_SearchDocumentsResults", searchDocumentsResults);
        }

        [AllowAnonymous]
        public ActionResult ManagePdfFile(long appointmentPresentationID, int documentType, int pdfAction)
        {
            PdfDocumentViewModel pdfDocument = _pdfDocumentService.GePdfDocumentModel(appointmentPresentationID, documentType);

            if (pdfDocument == null)
            {
                return null;
            }

            if (pdfAction == (int)PdfAction.View || pdfAction == (int)PdfAction.Save)
            {
                ViewPdfFile(pdfDocument.FilePathName, pdfDocument.Name, pdfAction);
            }
            else if (pdfAction == (int)PdfAction.Print)
            {
                PrintPdfFile(pdfDocument.FilePathName, pdfDocument.Name, appointmentPresentationID, documentType);
            }

            return null;
        }

        public ActionResult EmailPdfFile(int dealerId, int dealerCustomerID, long appointmentPresentationID, int documentType)
        {
            PdfDocumentEmailModel model = new PdfDocumentEmailModel();
            var pdfDocument = _pdfDocumentService.GePdfDocumentModel(appointmentPresentationID, documentType);
            model.FilePath = pdfDocument.FilePathName;
            model.FileName = pdfDocument.Name;
            model.DealerId = dealerId;
            model.DealerCustomerId = dealerCustomerID;
            model.AppointmentPresentationId = appointmentPresentationID;
            model.DocumentType = documentType;
            model.MessageFrom = AppContext.CurrentUserEmail;

            if(documentType == (int)DocumentType.CustomerCopy || documentType == (int)DocumentType.WalkAroundInspection)
            {
                var dealerCustomerManager = IocManager.Resolve<IDealerCustomerService>();
                var dealerCustomer = dealerCustomerManager.GetViewModel(dealerCustomerID);
                model.MessageTo = dealerCustomer.Email;
            }

            switch(documentType)
            {
                case (int)DocumentType.CustomerCopy:
                    ViewBag.EmailPdfTitle = string.Format(Labels.EmailPdfTitle, Labels.CustomerCopy);
                    break;
                case (int)DocumentType.PartsCopy:
                    ViewBag.EmailPdfTitle = string.Format(Labels.EmailPdfTitle, Labels.PartsCopy);
                    break;
                case (int)DocumentType.TechCopy:
                    ViewBag.EmailPdfTitle = string.Format(Labels.EmailPdfTitle, Labels.TechCopy);
                    break;
                case (int)DocumentType.WalkAroundInspection:
                    ViewBag.EmailPdfTitle = string.Format(Labels.EmailPdfTitle, Labels.WalkaroundInspection);
                    break;
                default:
                    ViewBag.EmailPdfTitle = string.Format(Labels.EmailPdfTitle, Labels.PDF);
                    break;
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult EmailPdfFile(PdfDocumentEmailModel model)
        {
            try
            {
                var pdfFilePath = $@"{AppContext.DocumentsFolder}\{model.FilePath}\{model.FileName}";
                var fileManager = IocManager.Resolve<IFileManager>();
                var stream = fileManager.Read(pdfFilePath);
                var contentType = MimeMapping.GetMimeMapping(pdfFilePath);
                var attachments = new List<Attachment> { new Attachment(stream, model.FileName, contentType) };

                model.MessageBody = string.IsNullOrEmpty(model.MessageBody) ? model.MessageBody : model.MessageBody.Replace(Environment.NewLine, "<br />");

                ViewBag.MessageResult = _pdfDocumentService.SendPdfAsEmail(model.MessageFrom, model.MessageTo, model.MessageSubject, model.MessageBody, attachments)
                    ? Messages.PdfEmailSuccess : Messages.PdfEmailFail;
            }
            catch (Exception ex)
            {
                ViewBag.MessageResult = Messages.PdfEmailFail + " " +  ex.Message;
            }

            return View(model);
        }

        [AllowAnonymous]
        public void ViewPdfFile(string filePath, string fileName, int pdfAction)
        {
            var appContext = IocManager.Resolve<IAppContext>();
            var pdfFilePath = $@"{appContext.DocumentsFolder}\{filePath}\{fileName}";
            var fileManager = IocManager.Resolve<IFileManager>();
            var stream = fileManager.Read(pdfFilePath);
            var bytes = new byte[stream.Length];
            stream.Read(bytes, 0, bytes.Length);
            string contentType = MimeMapping.GetMimeMapping(pdfFilePath);

            Response.Clear();

            Response.ContentType = contentType;
            Response.AddHeader("Accept-Header", stream.Length.ToString());
            Response.AddHeader("Content-Disposition", String.Format("{0}; filename={1}", pdfAction == (int)PdfAction.View ? "Inline" : "Attachment", fileName));
            Response.AddHeader("Content-Length", stream.Length.ToString());
            Response.BinaryWrite(bytes);

            Response.End();
        }

        [AllowAnonymous]
        public void PrintPdfFile(string filePath, string fileName, long appointmentPresentationID, int documentType)
        {
            var companySettings = IocManager.Resolve<ICompanyService>();
            if (companySettings.GetSettingAsBool(CompanySettingsNames.IsRemotePrint) && AppContext.CurrentIdentity.TenantId.HasValue)
            {
                string prnJobTypeCode;

                switch ((DocumentType)documentType)
                {
                    case DocumentType.CustomerCopy:
                        prnJobTypeCode = PrintDocumentType.MONE.ToString();
                        break;
                    case DocumentType.PartsCopy:
                        prnJobTypeCode = PrintDocumentType.CHKB.ToString();
                        break;
                    case DocumentType.TechCopy:
                        prnJobTypeCode = PrintDocumentType.CHKT.ToString();
                        break;
                    case DocumentType.WalkAroundInspection:
                        prnJobTypeCode = PrintDocumentType.WA.ToString();
                        break;
                    default:
                        prnJobTypeCode = string.Empty;
                        break;
                }

                string url = string.Format("{0}://{1}{2}/Dealer/PdfDocuments/ViewPdfFile?filePath={3}&fileName={4}&pdfAction={5}", Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port, filePath, fileName, ((int)PdfAction.View));

                _pdfDocumentService.AddPdfDocumentToPrintQueue(AppContext.CurrentIdentity.TenantId.Value, AppContext.CurrentIdentity.UserId, url, appointmentPresentationID, prnJobTypeCode);
            }
            ViewPdfFile(filePath, fileName, ((int)PdfAction.View));
        }

        [AllowAnonymous]
        public ActionResult PdfFooter()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult PdfEmptyHeader()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult VehicleInspectionPdf(long? inspectionId, long? appointmentPresentationID, int dealerId)
        {
            //inspectionId = 222;
            //appointmentPresentationID = 590;
            //dealerId = 1089;
            if (inspectionId.HasValue && inspectionId.Value == -1)
            {
                inspectionId = null;
            }
            if (appointmentPresentationID.HasValue && appointmentPresentationID.Value == -1)
            {
                appointmentPresentationID = null;
            }

            var companySettings = IocManager.Resolve<ICompanyService>();
            var appPresentation = IocManager.Resolve<IAppointmentPresentationService>();
            var pdfDocumentService = IocManager.Resolve<IPdfDocumentService>();
            var fileManager = IocManager.Resolve<IFileManager>();

            string measurementUnit = companySettings.GetSettingAsInteger(CompanySettingsNames.MeasurementUnit) == (int)MeasurementUnits.Imperial ? Labels.Miles : Labels.km;

            PdfHeaderViewModel header = new PdfHeaderViewModel();

            //TODO check why TenantId is NULL when getting this page from Evo...
            //header.CustomerInfo = pdfDocumentService.GetPdfHeaderCustomerInfoViewModel(dealerId, appointmentPresentationID.HasValue ? appointmentPresentationID.Value : -1);
            if (header.CustomerInfo != null)
            {
                header.CustomerInfo.DateString = header.CustomerInfo.Date.ToString(companySettings.GetDateTimeFormat(CompanySettingsNames.DateFormat));
            }
            else
            {
                header.CustomerInfo = new PdfHeaderCustomerInfoViewModel();
                header.CustomerInfo.Date = DateTime.Now;
                header.CustomerInfo.DateString = header.CustomerInfo.Date.ToString(companySettings.GetDateTimeFormat(CompanySettingsNames.DateFormat));
                header.CustomerInfo.VehicleMake = "PORSCHE";
                header.CustomerInfo.VehicleYear = "2011";
                header.CustomerInfo.VehicleModel = "911";
                header.CustomerInfo.CustomerName = "LILY DANESHGAR";
                header.CustomerInfo.VIN = "WP0CB29988S776056";
                header.CustomerInfo.OdometerReading = "15000 Miles";
            }

            var printAdvisorInformation = companySettings.GetSettingAsBool(CompanySettingsNames.PrintAdvisorInformation);
            if (printAdvisorInformation && AppContext.CurrentIdentity.UserId.HasValue)
            {
                header.ShowAdvisor = true;
                header.Advisor = pdfDocumentService.GetPdfHeaderAdvisorViewModel(AppContext.CurrentIdentity.UserId.Value);
            }

            header.ShowDealer = true;
            header.Dealer = pdfDocumentService.GetPdfHeaderDealerViewModel();
            //header.Dealer.LogoImg = companySettings.GetCompanyLogoPath(CompanySettingsNames.CompanyLogo);
            var logoPath = Request.Url.Host.IndexOf("localhost") > -1 ? "UserContent/Companies//1089/75a8ebc4-0578-4a6b-81b7-4025c8c0bb52.png" : "UserContent/Companies//1089/4e2e02fd-a59f-4d04-8290-dfc17f16e264.png";
            //var stream = fileManager.Read(header.Dealer.LogoImg);
            var stream = fileManager.Read(logoPath);
            header.Dealer.LogoImgByteArray = fileManager.ReadStreamToByte(stream);
            header.CustomerInfo.OdometerReading = string.Format("{0} {1}", header.CustomerInfo.OdometerReading, measurementUnit);

            var footer = pdfDocumentService.GetPdfFooterViewModel(appointmentPresentationID.HasValue ? appointmentPresentationID.Value : -1, header.CustomerInfo.Date == DateTime.MinValue ? DateTime.Now : header.CustomerInfo.Date);

            var modelWA = pdfDocumentService.GetWalkaroundInspectionPdfViewModel(inspectionId, appointmentPresentationID, true);

            modelWA.Header = header;

            modelWA.Footer = footer;

            return View("PdfWalkaroundInspection", modelWA);
        }
    }
}