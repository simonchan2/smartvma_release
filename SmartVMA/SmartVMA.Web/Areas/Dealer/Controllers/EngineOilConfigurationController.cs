﻿using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;
using System.Web.Mvc;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Core.InputModels.PageRequests;
using System.Linq;
using SmartVMA.Core.Contracts;

namespace SmartVMA.Web.Areas.Dealer.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class EngineOilConfigurationController : BaseController
    {
        private readonly IEngineOilORuleService _service;
        private readonly IAppContext _appContext;
        public EngineOilConfigurationController(IAppContext appContext, IEngineOilORuleService service) : base(appContext)
        {
            _service = service;
            _appContext = appContext;
        }

        public ActionResult Index()
        {
            var model = _service.GetAllEngineOilORuleForDealer();
            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var model = _service.GetViewModel();
            model.CompanyId = _appContext.CurrentIdentity.TenantId;
            return View("Edit", model);
        }
        [HttpPost]
        public ActionResult Create(EngineOilConfigurationViewModel model)
        {
            if (ModelState.IsValid)
            {
                var response = _service.Save(model);
                if (response.IsSuccess)
                {
                    return Json(response);
                }
                MapErrors(response.Errors);
            }
            return View("Edit", model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var model = _service.GetViewModel(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(EngineOilConfigurationViewModel model)
        {

            if (ModelState.IsValid)
            {
                var response = _service.Save(model);
                if (response.IsSuccess)
                {
                    return Json(response);
                }
                MapErrors(response.Errors);
            }
            return View("Edit", model);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var model = new EngineOilConfigurationViewModel();
            _service.GetViewModel(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(EngineOilConfigurationViewModel model)
        {
            var response = new ServiceResponse();
            if (model.Id.HasValue)
            {
                response = _service.Delete(model.Id.Value);
            }
            return Json(response);
        }

        [HttpPost]
        public ActionResult LoadData(PagingRequest request)
        {
            var draw = Request.Form.GetValues("draw");
            var model = _service.GetPagedItems(request);

            return Json(new { draw = draw.FirstOrDefault() ?? "1", recordsFiltered = model.ItemsCount, recordsTotal = model.ItemsCount, data = model.Items }, JsonRequestBehavior.AllowGet);
        }
    }
}