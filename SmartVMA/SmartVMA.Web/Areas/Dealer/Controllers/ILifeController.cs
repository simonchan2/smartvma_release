﻿using Rotativa;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.FileManager;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;
using System;
using System.Web.Mvc;

namespace SmartVMA.Web.Areas.Dealer.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class ILifeController : BaseController
    {
        private readonly IILifeService _service;

        public ILifeController(IAppContext appContext, IILifeService service) : base(appContext)
        {
            _service = service;
        }

        public ActionResult Index(string vin )
        {
            var model = _service.GetILifeByVin(vin);
            return View(model);
        }

        public string ViewILifePdfFile(string vin)
        {
            var companySettings = IocManager.Resolve<ICompanyService>();
            var pdfDocumentService = IocManager.Resolve<IPdfDocumentService>();
            var fileManager = IocManager.Resolve<IFileManager>();
            var appPresentation = IocManager.Resolve<IAppointmentPresentationService>();

            PdfHeaderViewModel header = new PdfHeaderViewModel();

            header.CustomerInfo = pdfDocumentService.GetPdfHeaderCustomerInfoViewModelFromVin(vin);

            var printAdvisorInformation = companySettings.GetSettingAsBool(CompanySettingsNames.PrintAdvisorInformation);
            if (printAdvisorInformation && AppContext.CurrentIdentity.UserId.HasValue)
            {
                header.ShowAdvisor = true;
                header.Advisor = pdfDocumentService.GetPdfHeaderAdvisorViewModel(AppContext.CurrentIdentity.UserId.Value);
            }

            header.ShowDealer = true;
            header.Dealer = pdfDocumentService.GetPdfHeaderDealerViewModel();
            header.Dealer.LogoImg = companySettings.GetCompanyLogoPath(CompanySettingsNames.CompanyLogo);
            var stream = fileManager.Read(header.Dealer.LogoImg);
            header.Dealer.LogoImgByteArray = fileManager.ReadStreamToByte(stream);

            var footer = pdfDocumentService.GetPdfFooterViewModel(-1, DateTime.Now);

            ViewBag.LogoImg = companySettings.GetCompanyLogo(CompanySettingsNames.CompanyLogo);
            ILifePdfViewModel model = new ILifePdfViewModel();
            model.Header = header;
            model.ILifeModel = _service.GetILifeByVin(vin);
            model.Footer = footer;

            var pdf = new ViewAsPdf("ViewILifePdfFile", model);

            string headerPath = string.Format("{0}://{1}{2}/Dealer/PdfDocuments/PdfEmptyHeader",
                    Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port);
            string footerPath = string.Format("{0}://{1}{2}/Dealer/PdfDocuments/PdfFooter?printed={3}&form={4}",
                    Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port, footer.Printed, footer.From);

            string customSwitches = string.Format("--header-html {0} --footer-html {1}", headerPath, footerPath);
            pdf.CustomSwitches = customSwitches;
            var filePath = string.Format("ILife");
            var fileName = string.Format("ILife_{0}.pdf", DateTime.Now.ToString("yyyyMMddHHmm"));
            var url = AppContext.MapUrl($"/UserContent/SmartVMADocuments/{filePath}/{fileName}");

            ExportPdfFile(pdf, filePath, fileName);            

            return url;
        }
    }
}