﻿using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;
using System;
using System.Web.Mvc;
using SmartVMA.Infrastructure.Extensions;
using System.Collections.Generic;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Web.Utilities;
using System.Linq;
using System.Web.Routing;
using SmartVMA.Core.Contracts;
using SmartVMA.Resources;
using System.Web.Security;
using System.Web;
using SmartVMA.Core;

namespace SmartVMA.Web.Areas.Dealer.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class AppointmentPresentationController : BaseController
    {
        private readonly IAppointmentPresentationService _service;
        private readonly IDealerCustomerService _dealerCustomerService;
        private readonly ICarService _carService;
        private readonly ICompanyService _companyService;
        private readonly IUserService _userService;
        private readonly IAppointmentService _appointmentService;


        public AppointmentPresentationController(IAppContext appContext, IAppointmentPresentationService service, IDealerCustomerService dealerCustomerService, ICarService carService, ICompanyService companyService, IUserService userService, IAppointmentService appointmentService) : base(appContext)
        {
            _service = service;
            _dealerCustomerService = dealerCustomerService;
            _carService = carService;
            _companyService = companyService;
            _userService = userService;
            _appointmentService = appointmentService;
        }

        // GET: Dealer/AppointmentPresentation
        public ActionResult Index(bool isFromLogin = false)
        {
            ICompanyService companyService = IocManager.Resolve<ICompanyService>();
            AdvisorMenusViewModel defaultModel = CreateHomeModel(isFromLogin);

            //Populate company settings in session
            var companySettings = _companyService.GetAllSettings();
            foreach (var setting in companySettings)
            {
                if (!string.IsNullOrEmpty(setting.Name))
                {
                    Session[StaticSettings.SessionPrefixCompanySetting + setting.Name] = setting.Value;
                }
            }
            //Populate user settings in session
            var userSettings = _userService.GetAllSettings();
            foreach (var setting in userSettings)
            {
                if (!string.IsNullOrEmpty(setting.Name))
                {
                    Session[StaticSettings.SessionPrefixUserSettings + setting.Name] = setting.Value;
                }
            }

            string userName;
            userName = $"{AppContext.CurrentIdentity.UserId}_{AppContext.CurrentIdentity.RoleId}_{AppContext.CurrentIdentity.TenantId}";
            int sessionExpiration = (companyService.GetSettingAsInteger(Core.Enums.CompanySettingsNames.SessionExpiration) == 0 ? 5 : companyService.GetSettingAsInteger(Core.Enums.CompanySettingsNames.SessionExpiration));
            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
                 userName,
                 DateTime.Now,
                 DateTime.Now.AddMinutes(sessionExpiration),
                 true,//isPersistent,
                 "",//userData,
                 FormsAuthentication.FormsCookiePath);
            // Encrypt the ticket.
            string encTicket = FormsAuthentication.Encrypt(ticket);
            // Create the cookie.
            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
            cookie.HttpOnly = true;
            Response.Cookies.Add(cookie);
            return View("Index", defaultModel);
        }

        public ActionResult PopulateVehicleLookupSearch(string vin, int? customerId, int? dealerVehicleId, int? appointmentPresentationId, string appointmentPresentationStatus)
        {
            AdvisorMenusViewModel defaultModel = CreateHomeModel(vin: vin, customerId: customerId, dealerVehicleId: dealerVehicleId);
            return View("Index", defaultModel);
        }

        private AdvisorMenusViewModel CreateHomeModel(bool isFromLogin = false, string vin = null, int? customerId = null, int? dealerVehicleId = null, int? appointmentPresentationId = null, string appointmentPresentationStatus = null)
        {
            ICompanyService companyService = IocManager.Resolve<ICompanyService>();
            var timeFrameId = (int)TimeFrames.Today;
            var defaultDate = DateTime.Now.Date;
            var defaultDateString = defaultDate.ToString(companyService.GetDateTimeFormat(CompanySettingsNames.DateFormat));
            AdvisorMenusViewModel defaultModel = _service.GetDefaultAdvisorMenus(defaultDate, defaultDate, timeFrameId);
            if (!string.IsNullOrEmpty(vin))
            {
                defaultModel.VehicleLookup = GetVehicleFilterResults(vin, defaultModel.VehicleLookup);
                defaultModel.VehicleLookup.CustomerId = customerId;
                defaultModel.VehicleLookup.DealerVehicleId = dealerVehicleId;
                defaultModel.VehicleLookup.AppointmentPresentationId = appointmentPresentationId;
                defaultModel.VehicleLookup.AppointmentPresentationStatus = appointmentPresentationStatus;
            }
            if (isFromLogin)
            {
                var alertService = IocManager.Resolve<IAlertService>();
                var numberOfAlerts = alertService.GetActiveAlertsForCurrentUser();
                isFromLogin = numberOfAlerts.Items.Count() != 0 ? true : false;
                Session.Remove("IsParkedMenuVisible");
                Session.Remove("IsPriceVisible");
            }

            ViewBag.IsFromLogin = isFromLogin;
            if (Session["IsParkedMenuVisible"] != null)
            {
                defaultModel.IsParkedMenuVisible = (bool)Session["IsParkedMenuVisible"];
            }
            else
            {
                var userSettings = IocManager.Resolve<IUserService>();
                defaultModel.IsParkedMenuVisible = userSettings.GetSettingAsBool(UserSettingsNames.IsParked, true);
                Session["IsParkedMenuVisible"] = defaultModel.IsParkedMenuVisible;
            }
            defaultModel.ParkedMenu.StartDateString = defaultDateString;
            defaultModel.ParkedMenu.EndDateString = defaultDateString;
            defaultModel.AcceptedMenu.StartDateString = defaultDateString;
            defaultModel.AcceptedMenu.EndDateString = defaultDateString;
            return defaultModel;
        }

        [HttpPost]
        public void StoreActiveTab(int activeTabId)
        {
            switch (activeTabId)
            {
                case (int)HomeMenuType.ParkedMenu:
                    Session["IsParkedMenuVisible"] = true;
                    return;
                case (int)HomeMenuType.AcceptedMenu:
                    Session["IsParkedMenuVisible"] = false;
                    return;
            }
            return;
        }

        [HttpPost]
        public ActionResult CustomerSearch(SearchCustomerViewModel model)
        {
            AdvisorSearchIndexViewModel searchResults = new AdvisorSearchIndexViewModel();
            bool haveSearchParametars = true;
            string errorMsg = string.Empty;
            if (string.IsNullOrEmpty(model.CustomerName) && string.IsNullOrEmpty(model.CustomerNumber) && string.IsNullOrEmpty(model.CustomerPhone) && string.IsNullOrEmpty(model.Vin)
                && string.IsNullOrEmpty(model.VehicleStockNumber) && string.IsNullOrEmpty(model.LicensePlate))
            {
                haveSearchParametars = false;
                errorMsg = Messages.NoSearchParametars;
            }
            else
            {
                if (ModelState.IsValid)
                {
                    searchResults = _dealerCustomerService.CustomerSearch((model.CustomerName != null ? model.CustomerName.Trim() : model.CustomerName),
                                                                           (model.CustomerNumber != null ? model.CustomerNumber.Trim() : model.CustomerNumber),
                                                                           (model.CustomerPhone != null ? model.CustomerPhone.Trim() : model.CustomerPhone),
                                                                           (model.Vin != null ? model.Vin.Trim() : model.Vin),
                                                                           (model.VehicleStockNumber != null ? model.VehicleStockNumber.Trim() : model.VehicleStockNumber),
                                                                           (model.LicensePlate != null ? model.LicensePlate.Trim() : model.LicensePlate));

                    if (searchResults == null || searchResults.ItemsCount == 0)
                    {
                        //do secondary search by VIN for vehicle
                        if (string.IsNullOrEmpty(model.Vin))
                        {
                            model.Vin = _dealerCustomerService.GetVinByCustomerSearchInfo((model.CustomerName != null ? model.CustomerName.Trim() : model.CustomerName),
                                                                          (model.CustomerNumber != null ? model.CustomerNumber.Trim() : model.CustomerNumber),
                                                                          (model.CustomerPhone != null ? model.CustomerPhone.Trim() : model.CustomerPhone),
                                                                          (model.VehicleStockNumber != null ? model.VehicleStockNumber.Trim() : model.VehicleStockNumber),
                                                                          (model.LicensePlate != null ? model.LicensePlate.Trim() : model.LicensePlate));
                        }
                        if (!string.IsNullOrEmpty((model.Vin ?? string.Empty).Trim()))
                        {
                            bool vinExist = _carService.VinExists((model.Vin ?? string.Empty).Trim());
                            if (vinExist)
                            {
                                return Json(new ServiceResponse { IsSuccess = false, RedirectUrl = string.Format("/Dealer/AppointmentPresentation/SecondarySearchVehicleLookup?secondarySearchVin={0}", model.Vin) });
                            }
                            else
                            {
                                haveSearchParametars = false;
                                errorMsg = Messages.VINNotExists;
                            }
                        }
                    }
                    else if (searchResults != null && searchResults.ItemsCount == 1)
                    {
                        var oneResult = searchResults.Items.FirstOrDefault();
                        if (oneResult.CarId == null)
                        {
                            //do secondary search by VIN for vehicle
                            if (!string.IsNullOrEmpty((oneResult.Vin ?? string.Empty).Trim()))
                            {
                                bool vinExist = _carService.VinExists((oneResult.Vin ?? string.Empty).Trim());
                                if (vinExist)
                                {
                                    return Json(new ServiceResponse { IsSuccess = false, RedirectUrl = string.Format("/Dealer/AppointmentPresentation/SecondarySearchVehicleLookup?secondarySearchVin={0}&secondarySearchCustomerId={1}&secondarySearchDealerVehicleId={2}", oneResult.Vin, oneResult.CustomerId, oneResult.DealerVehicleId) });
                                }
                                else
                                {
                                    haveSearchParametars = false;
                                    errorMsg = Messages.VINNotExists;
                                }
                            }
                            else
                            {
                                searchResults = new AdvisorSearchIndexViewModel();
                                searchResults.Items = new List<AdvisorSearchViewModel>();
                            }
                        }
                        else
                        {
                            return Json(new ServiceResponse
                            {
                                IsSuccess = true,
                                RedirectUrl = Url.Action("NavigateToMenuPresentation", "AppointmentPresentation",
                                new
                                {
                                    Area = "Dealer",
                                    customerId = oneResult.CustomerId,
                                    carId = oneResult.CarId,
                                    appointmentPresentationStatus = oneResult.AppointmentPresentationStatus,
                                    appointmentPresentationId = oneResult.AppointmentPresentationId,
                                    dealerVehicleId = oneResult.DealerVehicleId,
                                    CarIdUndetermined = (oneResult.CarIdUndetermined ?? true) ? (int)CarIdUndeterminedStatus.True : (int)CarIdUndeterminedStatus.False,
                                    vin = (oneResult.Vin ?? string.Empty).Trim()
                                })
                            });
                        }
                    }
                }
            }

            return haveSearchParametars ? GetIndexView(searchResults, "SearchResults") : GetSearchErrorView(errorMsg, "NoSearchParametars");
        }

        public ActionResult FilterParkedMenus(DateTime? startDate, DateTime? endDate, int timeFrameId, List<long> advisorIds)
        {
            AppointmentPresentationIndexViewModel parkedMenu = GetParkedMenu(startDate, endDate, timeFrameId, advisorIds);
            var viewModel = new ParkedMenu();
            viewModel.Items = parkedMenu;
            viewModel.StartDate = startDate;
            viewModel.EndDate = endDate;
            viewModel.TimeFrameId = timeFrameId;
            var iLifeService = IocManager.Resolve<IILifeService>();
            return PartialView("_ParkedMenus", viewModel);
        }

        [ActionName("SortParkedMenus")]
        [AcceptParameter(Name = "sort", Value = "SortParkedMenusByNameAsc")]
        public ActionResult SortParkedMenusByNameAsc(AdvisorMenusViewModel model)
        {
            AppointmentPresentationIndexViewModel parkedMenu = GetParkedMenu(model.ParkedMenu.StartDate, model.ParkedMenu.EndDate, model.ParkedMenu.TimeFrameId, model.ParkedMenu.AdvisorIds);
            if (parkedMenu.Items != null && parkedMenu.Items.Count() > 0)
            {
                parkedMenu.Items = parkedMenu.Items.Where(c => c.CustomerInfo != null).OrderBy(c => c.CustomerInfo.LastName);
            }
            var viewModel = model.ParkedMenu;
            viewModel.Items = parkedMenu;

            return PartialView("_ParkedMenus", viewModel);
        }

        [ActionName("SortParkedMenus")]
        [AcceptParameter(Name = "sort", Value = "SortParkedMenusByNameDesc")]
        public ActionResult SortParkedMenusByNameDesc(AdvisorMenusViewModel model)
        {
            AppointmentPresentationIndexViewModel parkedMenu = GetParkedMenu(model.ParkedMenu.StartDate, model.ParkedMenu.EndDate, model.ParkedMenu.TimeFrameId, model.ParkedMenu.AdvisorIds);
            if (parkedMenu.Items != null && parkedMenu.Items.Count() > 0)
            {
                parkedMenu.Items = parkedMenu.Items.Where(c => c.CustomerInfo != null).OrderByDescending(c => c.CustomerInfo.LastName);
            }
            var viewModel = model.ParkedMenu;
            viewModel.Items = parkedMenu;
            return PartialView("_ParkedMenus", viewModel);
        }

        [ActionName("SortParkedMenus")]
        [AcceptParameter(Name = "sort", Value = "SortParkedMenusByDateAsc")]
        public ActionResult SortParkedMenusByDateAsc(AdvisorMenusViewModel model)
        {
            AppointmentPresentationIndexViewModel parkedMenu = GetParkedMenu(model.ParkedMenu.StartDate, model.ParkedMenu.EndDate, model.ParkedMenu.TimeFrameId, model.ParkedMenu.AdvisorIds);
            if (parkedMenu.Items != null && parkedMenu.Items.Count() > 0)
            {
                parkedMenu.Items = parkedMenu.Items.OrderBy(c => c.ParkedDate);
            }
            var viewModel = model.ParkedMenu;
            viewModel.Items = parkedMenu;
            return PartialView("_ParkedMenus", viewModel);
        }

        [ActionName("SortParkedMenus")]
        [AcceptParameter(Name = "sort", Value = "SortParkedMenusByDateDesc")]
        public ActionResult SortParkedMenusByDateDesc(AdvisorMenusViewModel model)
        {
            AppointmentPresentationIndexViewModel parkedMenu = GetParkedMenu(model.ParkedMenu.StartDate, model.ParkedMenu.EndDate, model.ParkedMenu.TimeFrameId, model.ParkedMenu.AdvisorIds);
            if (parkedMenu.Items != null && parkedMenu.Items.Count() > 0)
            {
                parkedMenu.Items = parkedMenu.Items.OrderByDescending(c => c.ParkedDate);
            }
            var viewModel = model.ParkedMenu;
            viewModel.Items = parkedMenu;
            return PartialView("_ParkedMenus", viewModel);
        }

        [HttpGet]
        public ActionResult DeleteParkedMenu(long? appointmentPresentationId, DateTime? startDate, DateTime? endDate, int? timeFrameId)
        {
            var viewModel = new OptionsMenu(appointmentPresentationId.Value, 0, 0, null, null, null, null, null, startDate, endDate, timeFrameId);
            var test = ModelState.IsValid;
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Delete(long? appointmentPresentationId, DateTime? startDate, DateTime? endDate, int? timeFrameId, List<long> advisorIds)
        {
            ServiceResponse response = _service.DeleteParkedMenu(appointmentPresentationId);
            return Json(response);
        }

        [HttpGet]
        public ActionResult ReopenMenu(long appointmentPresentationId)
        {
            AppointmentPresentationViewModel model = _service.ReopenMenu(appointmentPresentationId);
            return CreateRedirectUrl(model.DealerCustomerID, model.CarId.HasValue ? model.CarId.Value : -1, AppointmentPresentationStatus.M.ToString(), model.AppointmentPresentationID, model.DealerVehicleId, (model.Vin ?? string.Empty).Trim(), model.CarIdUndetermined > 0 ? (int)CarIdUndeterminedStatus.True : (int)CarIdUndeterminedStatus.False);
        }

        public ActionResult FilterAcceptedMenus(DateTime? startDate, DateTime? endDate, int timeFrameId, List<long> advisorIds)
        {
            AppointmentPresentationIndexViewModel aceptedMenu = GetAcceptedMenu(startDate, endDate, timeFrameId, advisorIds);
            return PartialView("_AcceptedMenus", aceptedMenu);
        }

        [HttpPost]
        public ActionResult VehicleLookup(VehicleFilterViewModel viewModel)
        {
            if (viewModel != null && viewModel.Years != null)
            {
                var carId = _carService.GetCarId(viewModel.Years.FirstOrDefault(), viewModel.Makes.FirstOrDefault(), viewModel.Models.FirstOrDefault(), viewModel.EngineTypes.FirstOrDefault(), viewModel.TransmissionTypes.FirstOrDefault(), viewModel.DriveLines.FirstOrDefault());
                if (carId != null)
                {
                    return CreateRedirectUrl(viewModel.CustomerId, carId.Value, viewModel.AppointmentPresentationStatus, viewModel.AppointmentPresentationId, viewModel.DealerVehicleId, viewModel.VIN, (int)CarIdUndeterminedStatus.False, viewModel.Mileage, viewModel.TransmissionTypes.FirstOrDefault(), viewModel.DriveLines.FirstOrDefault(), viewModel.IsLofMethod);
                }
                else
                {
                    return RedirectToAction("Index", "AppointmentPresentation");
                }
            }
            else
            {
                return RedirectToAction("Index", "AppointmentPresentation");
            }
        }

        [HttpPost]
        public ActionResult SecondarySearchVehicleLookup(string secondarySearchVin, int? secondarySearchCustomerId, int? secondarySearchDealerVehicleId, int? secondarySearchAppId, string secondarySearchAppStatus)
        {
            VehicleFilterViewModel filter = new VehicleFilterViewModel();
            if (!string.IsNullOrEmpty(secondarySearchVin))
            {
                filter = GetVehicleFilterResults(secondarySearchVin, filter);
                filter.CustomerId = secondarySearchCustomerId;
                filter.DealerVehicleId = secondarySearchDealerVehicleId;
                filter.AppointmentPresentationId = secondarySearchAppId;
                filter.AppointmentPresentationStatus = secondarySearchAppStatus;
            }
            return PartialView("_VehicleLookup", filter);
        }

        private VehicleFilterViewModel GetVehicleFilterResults(string vin, VehicleFilterViewModel filter)
        {
            filter.VIN = vin;
            filter.VehicleFilterResponseModel = _carService.GetVehicleFilter(filter);
            if (filter.VehicleFilterResponseModel != null
                && (filter.VehicleFilterResponseModel.Years == null || filter.VehicleFilterResponseModel.Years.Count() == 0)
                && (filter.VehicleFilterResponseModel.Makes == null || filter.VehicleFilterResponseModel.Makes.Count() == 0)
                && (filter.VehicleFilterResponseModel.Models == null || filter.VehicleFilterResponseModel.Models.Count() == 0)
                && (filter.VehicleFilterResponseModel.EngineTypes == null || filter.VehicleFilterResponseModel.EngineTypes.Count() == 0)
                && (filter.VehicleFilterResponseModel.TransmissionTypes == null || filter.VehicleFilterResponseModel.TransmissionTypes.Count() == 0)
                && (filter.VehicleFilterResponseModel.DriveLines == null || filter.VehicleFilterResponseModel.DriveLines.Count() == 0))
            {
                filter = new VehicleFilterViewModel();
            }

            return filter;
        }

        private AppointmentPresentationIndexViewModel GetParkedMenu(DateTime? startDate, DateTime? endDate, int timeFrameId, List<long> advisorIds)
        {
            if (startDate == null && endDate == null)
            {
                SetAdvisorMenuFilterDates(timeFrameId, out startDate, out endDate);
            }

            AppointmentPresentationIndexViewModel parkedMenu = _service.ListParkedMenus(startDate, endDate, advisorIds, timeFrameId);
            return parkedMenu;
        }

        private AppointmentPresentationIndexViewModel GetAcceptedMenu(DateTime? startDate, DateTime? endDate, int timeFrameId, List<long> advisorIds)
        {
            if (startDate == null && endDate == null)
            {
                SetAdvisorMenuFilterDates(timeFrameId, out startDate, out endDate);
            }

            AppointmentPresentationIndexViewModel aceptedMenu = _service.ListAcceptedMenus(startDate, endDate, advisorIds, timeFrameId);
            return aceptedMenu;
        }

        private static void SetAdvisorMenuFilterDates(int timeFrameId, out DateTime? startDate, out DateTime? endDate)
        {
            startDate = null;
            endDate = null;
            switch (timeFrameId)
            {
                case ((int)TimeFrames.Today):
                    startDate = DateTime.Now.BeginingOfDay();
                    endDate = DateTime.Now.EndOfDay();
                    break;
                case ((int)TimeFrames.LastMonth):
                    startDate = DateTime.Now.AddMonths(-1).FirstDayInMonth();
                    endDate = DateTime.Now.AddMonths(-1).LastDayInMonth();
                    break;
                case ((int)TimeFrames.LastWeek):
                    startDate = DateTime.Now.FirstDayInLastWeek();
                    endDate = DateTime.Now.LastDayInLastWeek();
                    break;
                default:
                    break;
            }
        }

        [HttpPost]
        public void StoreIsPriceVisible(bool isVisible)
        {
            Session["IsPriceVisible"] = isVisible;
        }

        public ActionResult NavigateToMenuPresentation(int? customerId, int carId, string appointmentPresentationStatus, long? appointmentPresentationId, int? dealerVehicleId, string vin, int carIdUndetermined)
        {
            return CreateRedirectUrl(customerId, carId, appointmentPresentationStatus, appointmentPresentationId, dealerVehicleId, vin, carIdUndetermined);
        }

        public ActionResult NavigateToMenuPresentationFromAppointment(int? customerId, int carId, int? dealerVehicleId, string vin, int carIdUndetermined, long appointmentId, string controllerName, int mileage)
        {
            long appointmentPresentationId = _appointmentService.CreateAppointmentPresentation(appointmentId, AppContext.CurrentIdentity.UserId, AppointmentPresentationStatus.M.ToString());
            string appointmentPresentationStatus = AppointmentPresentationStatus.M.ToString();
                        
            return RedirectToAction("Index", controllerName, new
            {
                customerId = customerId,
                carId = carId,
                appointmentPresentationId = appointmentPresentationId,
                dealerVehicleId = dealerVehicleId,
                vin = vin,
                CarIdUndetermined = carIdUndetermined,
                Mileage = mileage
            });
        }

        private ActionResult CreateRedirectUrl(int? customerId, int carId, string appointmentPresentationStatus, long? appointmentPresentationId, int? dealerVehicleId, string vin, int carIdUndetermined, string mileage = null, int? transmission = null, int? driveLine = null, bool isLOF = false)
        {
            var appointmentPresentationService = IocManager.Resolve<IAppointmentPresentationService>();
            if (customerId != null)
            {
                if (appointmentPresentationId == null && dealerVehicleId.HasValue)
                {
                    var response = appointmentPresentationService.Create(customerId.Value, dealerVehicleId.Value);
                    appointmentPresentationId = response.SavedItemId;
                }
                else if(appointmentPresentationId != null && !string.IsNullOrEmpty(appointmentPresentationStatus))
                {
                    var appointment = appointmentPresentationService.IsExistingAppointmentPresentationByDateUserVehicle(
                                                    appointmentPresentationId, DateTime.Now, AppContext.CurrentIdentity.UserId, dealerVehicleId, appointmentPresentationStatus);

                    if (!appointment.NewId)
                    {
                        appointmentPresentationId = appointment.AppointmentId;
                    }
                    else
                    {
                        var response = appointmentPresentationService.Create(customerId.Value, (int)dealerVehicleId);
                        appointmentPresentationId = response.SavedItemId;
                    }
                }
            }

            return RedirectToAction("Index", isLOF ? "LOF" : "MenuPresentation", new
            {
                customerId = customerId,
                carId = carId,
                appointmentPresentationId = appointmentPresentationId,
                dealerVehicleId = dealerVehicleId,
                vin = vin,
                CarIdUndetermined = carIdUndetermined,
                mileage = mileage,
                transmission = transmission,
                driveline = driveLine
            });
        }
    }
}