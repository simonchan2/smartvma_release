﻿using System;
using System.Web.Mvc;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;
using SmartVMA.Infrastructure.FileManager;

namespace SmartVMA.Web.Areas.Dealer.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class TestEvoPdfController : BaseController
    {
        private readonly IPdfDocumentService _service;

        public TestEvoPdfController(IAppContext appContext, IPdfDocumentService service) : base(appContext)
        {
            _service = service;
        }

        [AllowAnonymous]
        public ActionResult Index(long inspectionId, long appointmentPresentationID, int dealerId)
        {
            var filePath = string.Format("{0}\\{1}", dealerId, appointmentPresentationID);
            var fileName = string.Format("WA_TestEvoPdf_{0}.pdf", DateTime.Now.ToString("yyyyMMddHHmm"));

            var appContext = IocManager.Resolve<IAppContext>();
            var pdfFilePath = $@"{appContext.DocumentsFolder}\{filePath}\{fileName}";
            var pdfFilePathUrl = pdfFilePath.Replace("\\", "/");

            var url = string.Format("{0}://{1}{2}/Dealer/PdfDocuments/VehicleInspectionPdf?inspectionId={3}&appointmentPresentationID={4}&dealerId={5}", Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port,
                inspectionId, appointmentPresentationID, dealerId);

            EvoPdf.HtmlToPdfConverter htmlToPdfConverter = new EvoPdf.HtmlToPdfConverter();
            var byteArray = htmlToPdfConverter.ConvertUrl(url);
            
            var fileManager = IocManager.Resolve<IFileManager>();
            fileManager.Save(pdfFilePath, byteArray, true);

            ViewBag.Msg = string.Format("Export was successful, file could be found here: {0}://{1}{2}/Files/Download?path={3}", Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port, pdfFilePathUrl);
            return View();
        }

        [AllowAnonymous]
        public void EvoInline(long inspectionId, long appointmentPresentationID, int dealerId)
        {
            var fileName = string.Format("WA_TestEvoPdf_{0}.pdf", DateTime.Now.ToString("yyyyMMddHHmm"));

            var url = string.Format("{0}://{1}{2}/Dealer/PdfDocuments/VehicleInspectionPdf?inspectionId={3}&appointmentPresentationID={4}&dealerId={5}", Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port,
                inspectionId, appointmentPresentationID, dealerId);

            EvoPdf.HtmlToPdfConverter htmlToPdfConverter = new EvoPdf.HtmlToPdfConverter();
            var byteArray = htmlToPdfConverter.ConvertUrl(url);

            // Send the PDF as response to browser

            // Set response content type
            Response.AddHeader("Content-Type", "application/pdf");

            // Instruct the browser to open the PDF file as an attachment or inline
            Response.AddHeader("Content-Disposition", String.Format("inline; filename={0}; size={1}",
                fileName, byteArray.Length.ToString()));

            // Write the PDF document buffer to HTTP response
            Response.BinaryWrite(byteArray);

            // End the HTTP response and stop the current page processing
            Response.End();
        }

        [AllowAnonymous]
        public ActionResult Azure(long inspectionId, long appointmentPresentationID, int dealerId)
        {
            var filePath = string.Format("{0}\\{1}", dealerId, appointmentPresentationID);
            var fileName = string.Format("WA_TestEvoPdf_{0}.pdf", DateTime.Now.ToString("yyyyMMddHHmm"));

            var appContext = IocManager.Resolve<IAppContext>();
            var pdfFilePath = $@"{appContext.DocumentsFolder}\{filePath}\{fileName}";
            var pdfFilePathUrl = pdfFilePath.Replace("\\", "/");

            var url = string.Format("{0}://{1}{2}/Dealer/PdfDocuments/VehicleInspectionPdf?inspectionId={3}&appointmentPresentationID={4}&dealerId={5}", Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port,
                inspectionId, appointmentPresentationID, dealerId);

            EvoPdf.HtmlToPdfClient.HtmlToPdfConverter htmlToPdfConverter = new EvoPdf.HtmlToPdfClient.HtmlToPdfConverter();
            var byteArray = htmlToPdfConverter.ConvertUrl(url);

            var fileManager = IocManager.Resolve<IFileManager>();
            fileManager.Save(pdfFilePath, byteArray, true);

            ViewBag.Msg = string.Format("Export was successful, file could be found here: {0}://{1}{2}/Files/Download?path={3}", Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port, pdfFilePathUrl);
            return View();
        }

        [AllowAnonymous]
        public ActionResult AzureByIp(long inspectionId, long appointmentPresentationID, int dealerId)
        {
            var filePath = string.Format("{0}\\{1}", dealerId, appointmentPresentationID);
            var fileName = string.Format("WA_TestEvoPdf_{0}.pdf", DateTime.Now.ToString("yyyyMMddHHmm"));

            var appContext = IocManager.Resolve<IAppContext>();
            var pdfFilePath = $@"{appContext.DocumentsFolder}\{filePath}\{fileName}";
            var pdfFilePathUrl = pdfFilePath.Replace("\\", "/");

            var url = string.Format("{0}://{1}{2}/Dealer/PdfDocuments/VehicleInspectionPdf?inspectionId={3}&appointmentPresentationID={4}&dealerId={5}", Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port,
                inspectionId, appointmentPresentationID, dealerId);

            EvoPdf.HtmlToPdfClient.HtmlToPdfConverter htmlToPdfConverter = new EvoPdf.HtmlToPdfClient.HtmlToPdfConverter("13.89.56.116", 40001);
            htmlToPdfConverter.ServicePassword = "bgpr0ducts";
            var byteArray = htmlToPdfConverter.ConvertUrl(url);

            var fileManager = IocManager.Resolve<IFileManager>();
            fileManager.Save(pdfFilePath, byteArray, true);            

            ViewBag.Msg = string.Format("Export was successful, file could be found here: {0}://{1}{2}/Files/Download?path={3}", Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port, pdfFilePathUrl);
            return View();
        }

        [AllowAnonymous]
        public void AzureByIpInline(long inspectionId, long appointmentPresentationID, int dealerId)
        {
            var fileName = string.Format("WA_TestEvoPdf_{0}.pdf", DateTime.Now.ToString("yyyyMMddHHmm"));            

            var url = string.Format("{0}://{1}{2}/Dealer/PdfDocuments/VehicleInspectionPdf?inspectionId={3}&appointmentPresentationID={4}&dealerId={5}", Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port,
                inspectionId, appointmentPresentationID, dealerId);

            EvoPdf.HtmlToPdfClient.HtmlToPdfConverter htmlToPdfConverter = new EvoPdf.HtmlToPdfClient.HtmlToPdfConverter("13.89.56.116", 40001);
            htmlToPdfConverter.ServicePassword = "bgpr0ducts";
            var byteArray = htmlToPdfConverter.ConvertUrl(url);

            // Send the PDF as response to browser

            // Set response content type
            Response.AddHeader("Content-Type", "application/pdf");

            // Instruct the browser to open the PDF file as an attachment or inline
            Response.AddHeader("Content-Disposition", String.Format("inline; filename={0}; size={1}",
                fileName, byteArray.Length.ToString()));

            // Write the PDF document buffer to HTTP response
            Response.BinaryWrite(byteArray);

            // End the HTTP response and stop the current page processing
            Response.End();
        }
    }
}