﻿using Rotativa;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Infrastructure.FileManager;
using System;
using System.Web.Mvc;
using System.Linq;
using SmartVMA.Resources;

namespace SmartVMA.Web.Areas.Dealer.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class MenuPresentationController : BaseServicePresentationController
    {
        private readonly IMenuPresentationService _service;
        public MenuPresentationController(IAppContext appContext, IMenuPresentationService service, IPdfDocumentService pdfService) : base(appContext, service, pdfService)
        {
            _service = service;
        }

        // GET: Dealer/MenuPresentation
        public ActionResult Index(int? carId, int? mileage, int? customerId, long? appointmentPresentationId, long? inspectionId, int? transmission, int? driveline, int? enteredMileage, int? dealerVehicleId, string vin, int? carIdUndetermined)
        {
            ServiceBasePresentationViewModel model = new ServiceBasePresentationViewModel();
            if (ModelState.IsValid)
            {
                model = _service.GetViewModel(carId, mileage, customerId, appointmentPresentationId, inspectionId, transmission, driveline, enteredMileage, dealerVehicleId, vin, carIdUndetermined);
                if (Session["IsPriceVisible"] != null)
                {
                    model.isVisiblePrice = (bool)Session["IsPriceVisible"];
                }
            }
           
            return View(model);
        }

        public ActionResult Video(string code)
        {
            return View("Video", model: code);
        }

        public ActionResult Odometer(int carId)
        {
            var model = new OdoMeterViewModel { CarId = carId };
            return View(model);
        }

        [HttpPost]
        public ActionResult Odometer(OdoMeterViewModel model)
        {
            var carService = IocManager.Resolve<ICarService>();
            var response = carService.FindMenuInterval(model);
            response.Model.EnteredMileage = model.Mileage;
            return Json(response);
        }

        public ActionResult ValidateMaxMileage()
        {
            return View();
        }

        public ActionResult ValidateStrike(int? serviceId)
        {
            ValidateStrikeViewModel model = new ValidateStrikeViewModel();
            model.ServiceId = serviceId;
            return View("ValidateStrike", model);
        }

        [HttpPost]
        public ActionResult ValidateStrike(ValidateStrikeViewModel model)
        {
            return Json(model);
        }

        [HttpPost]
        public ActionResult CreateCustomer(DealerCustomerViewModel model)
        {
            if (ModelState.IsValid)
            {
                var dealerCustomerService = IocManager.Resolve<IDealerCustomerService>();
                var response = dealerCustomerService.Save(model);
                if (response.IsSuccess)
                {
                    //var request = Map(model, response.SavedItemId, new ConfirmServiceRequestModel());
                    //return ConfirmService(request);

                    var request = Map(model, response.SavedItemId, new SaveConfirmServiceRequestModel());
                    request.IsLof = false;
                    return SaveServiceConfirmation(request);
                }
                MapErrors(response.Errors);
            }
            return View("CreateCustomer", model);
        }

        private SaveConfirmServiceRequestModel Map(DealerCustomerViewModel src, long? dealerCustomerId, SaveConfirmServiceRequestModel dest)
        {
            dest.Services = src.ConfirmServicesRequest.Services;
            dest.CarId = src.ConfirmServicesRequest.CarId;
            dest.Mileage = src.ConfirmServicesRequest.Mileage;
            dest.EnteredMileage = src.ConfirmServicesRequest.EnteredMileage;
            dest.DealerCustomerId = (int?)dealerCustomerId;
            dest.AppointmentPresentationId = src.ConfirmServicesRequest.AppointmentPresentationId;
            dest.VIN = src.VIN;
            dest.Invoice = src.ConfirmServicesRequest.Invoice;
            dest.IsInvoiceRequired = src.ConfirmServicesRequest.IsInvoiceRequired;
            dest.TaxChargesAmount = src.ConfirmServicesRequest.TaxChargesAmount;
            dest.ShopChargesAmount = src.ConfirmServicesRequest.ShopChargesAmount;
            dest.IsParked = src.ConfirmServicesRequest.IsParked;
            dest.IsDeclined = src.ConfirmServicesRequest.IsDeclined;
            dest.IsAccepted = src.ConfirmServicesRequest.IsAccepted;
            dest.MenuPackageOpCode = src.ConfirmServicesRequest.MenuPackageOpCode;
            dest.MenuLevel = src.ConfirmServicesRequest.MenuLevel;
            dest.Transmission = src.ConfirmServicesRequest.Transmission;
            dest.Driveline = src.ConfirmServicesRequest.Driveline;
            dest.InspectionId = src.ConfirmServicesRequest.InspectionId;

            return dest;
        }

        public ActionResult MenuPresentationPrint(bool printSingleMenu, bool includePrices, int selectedMenuLevel, int? carId, int? mileage, int? customerId, long? appointmentPresentationId, long? inspectionId, int? transmission, int? driveline)
        {
            MenuPresentationPrintOptions model = new MenuPresentationPrintOptions(printSingleMenu, includePrices, selectedMenuLevel, carId, mileage, customerId, appointmentPresentationId, inspectionId, transmission, driveline);
            return PartialView("_MenuPresentationPrintOptions", model);
        }        

        [HttpPost]
        public string ViewMenuPresentationPrintPdfFile(MenuPresentationPrintModel menuPresentationPrintModel)
        {
            var companySettings = IocManager.Resolve<ICompanyService>();
            var appPresentation = IocManager.Resolve<IAppointmentPresentationService>();
            var pdfDocumentService = IocManager.Resolve<IPdfDocumentService>();
            var fileManager = IocManager.Resolve<IFileManager>();
            string measurementUnit = companySettings.GetSettingAsInteger(CompanySettingsNames.MeasurementUnit) == (int)MeasurementUnits.Imperial ? Labels.Miles : Labels.km;
            
            menuPresentationPrintModel.Header.CustomerInfo.OdometerReading = string.Format("{0} {1}", menuPresentationPrintModel.Header.CustomerInfo.OdometerReading, measurementUnit);

            menuPresentationPrintModel.Header.ShowInvoiceNumber = false;
            menuPresentationPrintModel.ShowShopCharges = companySettings.GetSettingAsBool(CompanySettingsNames.PrintShopCharges);
            menuPresentationPrintModel.ShowTaxes = companySettings.GetSettingAsBool(CompanySettingsNames.PrintTaxesOnDocuments);

            var printAdvisorInformation = companySettings.GetSettingAsBool(CompanySettingsNames.PrintAdvisorInformation);
            if (printAdvisorInformation && AppContext.CurrentIdentity.UserId.HasValue)
            {
                menuPresentationPrintModel.Header.ShowAdvisor = true;
                menuPresentationPrintModel.Header.Advisor = pdfDocumentService.GetPdfHeaderAdvisorViewModel(AppContext.CurrentIdentity.UserId.Value);
            }

            menuPresentationPrintModel.Header.ShowDealer = true;
            menuPresentationPrintModel.Header.Dealer = pdfDocumentService.GetPdfHeaderDealerViewModel();
            menuPresentationPrintModel.Header.Dealer.LogoImg = companySettings.GetCompanyLogoPath(CompanySettingsNames.CompanyLogo);
            var stream = fileManager.Read(menuPresentationPrintModel.Header.Dealer.LogoImg);
            menuPresentationPrintModel.Header.Dealer.LogoImgByteArray = fileManager.ReadStreamToByte(stream);

            menuPresentationPrintModel.Header.CustomerInfo.DateString = DateTime.Now.Date.ToString(companySettings.GetDateTimeFormat(CompanySettingsNames.DateFormat));

            menuPresentationPrintModel.Header.CustomerInfo.ShowEngine = true;

            var menu1LaborPrice = menuPresentationPrintModel.PrintServices != null ? menuPresentationPrintModel.PrintServices.Where(x => x.ServiceMenuLevel == 1 && !x.ServiceIsStrikeOut).Sum(x => x.LaborPrice) : 0;
            var menu1PartsPrice = menuPresentationPrintModel.PrintServices != null ? menuPresentationPrintModel.PrintServices.Where(x => x.ServiceMenuLevel == 1 && !x.ServiceIsStrikeOut).Sum(x => x.PartsPrice) : 0;
            menuPresentationPrintModel.MenuLevel1Price = menuPresentationPrintModel.PrintServices != null ? menuPresentationPrintModel.PrintServices.Where(x => x.ServiceMenuLevel == 1 && !x.ServiceIsStrikeOut).Sum(x => x.ServicePrice) : 0;
            var prices = appPresentation.CalculatePriceCharges(menu1PartsPrice, menu1LaborPrice);
            menuPresentationPrintModel.MenuLevel1ShopCharges = prices.ShopChargesAmount;
            menuPresentationPrintModel.MenuLevel1TaxCharges = prices.TaxChargesAmount;
            menuPresentationPrintModel.MenuLevel1TotalPrice = menuPresentationPrintModel.MenuLevel1Price + menuPresentationPrintModel.MenuLevel1ShopCharges + menuPresentationPrintModel.MenuLevel1TaxCharges;

            var menu2LaborPrice = menuPresentationPrintModel.PrintServices != null ? menuPresentationPrintModel.PrintServices.Where(x => x.ServiceMenuLevel == 2 && !x.ServiceIsStrikeOut).Sum(x => x.LaborPrice) : 0;
            var menu2PartsPrice = menuPresentationPrintModel.PrintServices != null ? menuPresentationPrintModel.PrintServices.Where(x => x.ServiceMenuLevel == 2 && !x.ServiceIsStrikeOut).Sum(x => x.PartsPrice) : 0;
            menuPresentationPrintModel.MenuLevel2Price = menuPresentationPrintModel.PrintServices != null ? menuPresentationPrintModel.PrintServices.Where(x => x.ServiceMenuLevel == 2 && !x.ServiceIsStrikeOut).Sum(x => x.ServicePrice) : 0;
            prices = appPresentation.CalculatePriceCharges(menu2PartsPrice, menu2LaborPrice);
            menuPresentationPrintModel.MenuLevel2ShopCharges = prices.ShopChargesAmount;
            menuPresentationPrintModel.MenuLevel2TaxCharges = prices.TaxChargesAmount;
            menuPresentationPrintModel.MenuLevel2TotalPrice = menuPresentationPrintModel.MenuLevel2Price + menuPresentationPrintModel.MenuLevel2ShopCharges + menuPresentationPrintModel.MenuLevel2TaxCharges;

            var menu3LaborPrice = menuPresentationPrintModel.PrintServices != null ? menuPresentationPrintModel.PrintServices.Where(x => x.ServiceMenuLevel == 3 && !x.ServiceIsStrikeOut).Sum(x => x.LaborPrice) : 0;
            var menu3PartsPrice = menuPresentationPrintModel.PrintServices != null ? menuPresentationPrintModel.PrintServices.Where(x => x.ServiceMenuLevel == 3 && !x.ServiceIsStrikeOut).Sum(x => x.PartsPrice) : 0;
            menuPresentationPrintModel.MenuLevel3Price = menuPresentationPrintModel.PrintServices != null ? menuPresentationPrintModel.PrintServices.Where(x => x.ServiceMenuLevel == 3 && !x.ServiceIsStrikeOut).Sum(x => x.ServicePrice) : 0;
            prices = appPresentation.CalculatePriceCharges(menu3PartsPrice, menu3LaborPrice);
            menuPresentationPrintModel.MenuLevel3ShopCharges = prices.ShopChargesAmount;
            menuPresentationPrintModel.MenuLevel3TaxCharges = prices.TaxChargesAmount;
            menuPresentationPrintModel.MenuLevel3TotalPrice = menuPresentationPrintModel.MenuLevel3Price + menuPresentationPrintModel.MenuLevel3ShopCharges + menuPresentationPrintModel.MenuLevel3TaxCharges;
            
            menuPresentationPrintModel.Footer = pdfDocumentService.GetPdfFooterViewModel(menuPresentationPrintModel.AppointmentPresentationId, null);

            var pdf = new ViewAsPdf("PrintMenuPresentationPdf", menuPresentationPrintModel);
            string headerPath = string.Format("{0}://{1}{2}/Dealer/PdfDocuments/PdfEmptyHeader",
                    Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port);
            string footerPath = string.Format("{0}://{1}{2}/Dealer/PdfDocuments/PdfFooter?printed={3}&form={4}",
                    Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port, menuPresentationPrintModel.Footer.Printed, menuPresentationPrintModel.Footer.From);

            string customSwitches = string.Format("--header-html {0} --footer-html {1}", headerPath, footerPath);
            pdf.CustomSwitches = customSwitches;
            pdf.PageOrientation = Rotativa.Options.Orientation.Landscape;
            var filePath = string.Format("MenuPresentations");
            var fileName = string.Format("MenuPresentation_{0}.pdf", DateTime.Now.ToString("yyyyMMddHHmm"));

            var url = AppContext.MapUrl($"/UserContent/SmartVMADocuments/{filePath}/{fileName}");
                
            ExportPdfFile(pdf, filePath, fileName);

            if (companySettings.GetSettingAsBool(CompanySettingsNames.IsRemotePrint) && AppContext.CurrentIdentity.TenantId.HasValue)
            {
                string prnJobTypeCode = PrintDocumentType.MENU.ToString();

                pdfDocumentService.AddPdfDocumentToPrintQueue(AppContext.CurrentIdentity.TenantId.Value, AppContext.CurrentIdentity.UserId, url, 0, prnJobTypeCode);
            }

            return url;
        }

        [HttpPost]
        public string ViewMenuPresentationPrintSinglePdfFile(CustomerCopyPdfViewModel customerCopyPdf)
        {
            var companySettings = IocManager.Resolve<ICompanyService>();
            var appPresentation = IocManager.Resolve<IAppointmentPresentationService>();
            var pdfDocumentService = IocManager.Resolve<IPdfDocumentService>();
            var fileManager = IocManager.Resolve<IFileManager>();
            string measurementUnit = companySettings.GetSettingAsInteger(CompanySettingsNames.MeasurementUnit) == (int)MeasurementUnits.Imperial ? Labels.Miles : Labels.km;
            customerCopyPdf.Header.CustomerInfo.OdometerReading = string.Format("{0} {1}", customerCopyPdf.Header.CustomerInfo.OdometerReading, measurementUnit);

            customerCopyPdf.Header.ShowInvoiceNumber = false;
            customerCopyPdf.ShowShopCharges = companySettings.GetSettingAsBool(CompanySettingsNames.PrintShopCharges);
            customerCopyPdf.ShowTaxes = companySettings.GetSettingAsBool(CompanySettingsNames.PrintTaxesOnDocuments);

            var opCode = customerCopyPdf.Title;

            string suffix = "";
            string performancePlan = "";

            if (customerCopyPdf.SelectedMenuLevel == 1)
            {
                suffix = companySettings.GetSettingAsString(CompanySettingsNames.MenuSuffixLevel1);
                performancePlan = companySettings.GetSettingAsString(CompanySettingsNames.MenuLabelLevel1);
            }
            if (customerCopyPdf.SelectedMenuLevel == 2)
            {
                suffix = companySettings.GetSettingAsString(CompanySettingsNames.MenuSuffixLevel2);
                performancePlan = companySettings.GetSettingAsString(CompanySettingsNames.MenuLabelLevel2);
            }
            if (customerCopyPdf.SelectedMenuLevel == 3)
            {
                suffix = companySettings.GetSettingAsString(CompanySettingsNames.MenuSuffixLevel3);
                performancePlan = companySettings.GetSettingAsString(CompanySettingsNames.MenuLabelLevel3);
            }

            var modified = (customerCopyPdf.Services.Where(x => x.IsAdditional).Count() > 0 || customerCopyPdf.Services.Where(x => x.IsDeclined).Count() > 0);

            customerCopyPdf.Title = string.Format("Recommended Maintenance {0}{1}{2}", modified ? "- Modified " : "", opCode.Substring(0, 3), suffix);
            customerCopyPdf.SubTitle = string.Format("{0} {1} Interval Scheduled Maintenance Service - {2} Plan", opCode, measurementUnit, performancePlan);

            var printAdvisorInformation = companySettings.GetSettingAsBool(CompanySettingsNames.PrintAdvisorInformation);
            if (printAdvisorInformation && AppContext.CurrentIdentity.UserId.HasValue)
            {
                customerCopyPdf.Header.ShowAdvisor = true;
                customerCopyPdf.Header.Advisor = pdfDocumentService.GetPdfHeaderAdvisorViewModel(AppContext.CurrentIdentity.UserId.Value);
            }

            customerCopyPdf.Header.ShowDealer = true;
            customerCopyPdf.Header.Dealer = pdfDocumentService.GetPdfHeaderDealerViewModel();
            customerCopyPdf.Header.Dealer.LogoImg = companySettings.GetCompanyLogoPath(CompanySettingsNames.CompanyLogo);
            var stream = fileManager.Read(customerCopyPdf.Header.Dealer.LogoImg);
            customerCopyPdf.Header.Dealer.LogoImgByteArray = fileManager.ReadStreamToByte(stream);

            customerCopyPdf.Header.CustomerInfo.DateString = DateTime.Now.Date.ToString(companySettings.GetDateTimeFormat(CompanySettingsNames.DateFormat));

            customerCopyPdf.TotalPrice = customerCopyPdf.Services.Where(x => !x.IsDeclined).Sum(x => x.ServicePrice);
            var partsPrice = customerCopyPdf.Services.Where(x => !x.IsDeclined).Sum(x => x.PartsPrice);
            var laborPrice = customerCopyPdf.Services.Where(x => !x.IsDeclined).Sum(x => x.LaborPrice);
            var prices = appPresentation.CalculatePriceCharges(partsPrice, laborPrice);
            customerCopyPdf.TotalShopChargesAmount = prices.ShopChargesAmount;
            customerCopyPdf.TotalTaxChargesAmount = prices.TaxChargesAmount;

            customerCopyPdf.GrandTotalPrice = customerCopyPdf.TotalPrice + customerCopyPdf.TotalShopChargesAmount + customerCopyPdf.TotalTaxChargesAmount;
            
            customerCopyPdf.Disclaimer = Labels.Disclaimer;
            customerCopyPdf.MenuStatus = Labels.AcceptedCapsLock;

            customerCopyPdf.Footer = pdfDocumentService.GetPdfFooterViewModel(customerCopyPdf.AppointmentPresentationId, null);

            var pdf = new ViewAsPdf("PdfCustomerCopy", customerCopyPdf);

            string headerPath = string.Format("{0}://{1}{2}/Dealer/PdfDocuments/PdfEmptyHeader",
                    Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port);
            string footerPath = string.Format("{0}://{1}{2}/Dealer/PdfDocuments/PdfFooter?printed={3}&form={4}",
                    Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port, customerCopyPdf.Footer.Printed, customerCopyPdf.Footer.From);

            string customSwitches = $"--header-html {headerPath} --footer-html {footerPath}";
            pdf.CustomSwitches = customSwitches;
            var filePath = string.Format("MenuPresentations");
            var fileName = string.Format("MenuPresentationSingle_{0}.pdf", DateTime.Now.ToString("yyyyMMddHHmm"));

            var url = AppContext.MapUrl($"/UserContent/SmartVMADocuments/{filePath}/{fileName}");
            
            ExportPdfFile(pdf, filePath, fileName);

            if (companySettings.GetSettingAsBool(CompanySettingsNames.IsRemotePrint) && AppContext.CurrentIdentity.TenantId.HasValue)
            {
                string prnJobTypeCode = PrintDocumentType.MENU.ToString();

                pdfDocumentService.AddPdfDocumentToPrintQueue(AppContext.CurrentIdentity.TenantId.Value, AppContext.CurrentIdentity.UserId, url, 0, prnJobTypeCode);
            }

            return url;
        }
    }
}