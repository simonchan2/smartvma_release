﻿using System;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;
using System.Web.Mvc;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Contracts;
using SmartVMA.Core.Enums;
using SmartVMA.Infrastructure.FileManager;
using SmartVMA.Resources;
using Rotativa;


namespace SmartVMA.Web.Areas.Dealer.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class InspectionController : BaseController
    {
        private readonly IInspectionService _service;
        private readonly IDealerCustomerService _dealerCustomerService;


        public InspectionController(IAppContext appContext, IInspectionService service, IDealerCustomerService dealerCustomerService) : base(appContext)
        {
            _service = service;
            _dealerCustomerService = dealerCustomerService;
        }

        [HttpGet]
        public ActionResult Index(int carId, long? inspectionId = null, long? appointmentPresentationId = null, int? customerId = null, int? mileage = null, int? transmission = null, int? driveline = null, int? enteredMileage = null, int? dealerVehicleId = null, string vin = null, int carIdUndetermined = 1)
        {
            InspectionViewModel model = _service.GetViewModel(inspectionId, appointmentPresentationId, transmission, driveline, enteredMileage);
            var customerInfo = _dealerCustomerService.GetCustomerInfo(customerId, carId, transmission, driveline, dealerVehicleId);
            if (customerInfo != null)
            {
                customerInfo.Mileage = enteredMileage;
                if (!string.IsNullOrEmpty(vin))
                {
                    customerInfo.VIN = vin;
                }
            }
            model.CustomerInfo = customerInfo ?? new CustomerInfoViewModel();
            model.CarIdUndetermined = carIdUndetermined;
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(InspectionViewModel model, string action, int carId, long? inspectionId = null, int? appointmentPresentationId = null, int? customerId = null, int? mileage = null, string transmission = null, string driveline = null, int? enteredMileage = null, int? dealerVehicleId = null, string vin = null, int carIdUndetermined = 1)
        {
            if (ModelState.IsValid)
            {
                var response = _service.AddSetInspection(model);
                if (response.IsSuccess)
                {
                    switch ((InspectionNavigationActions)Enum.Parse(typeof(InspectionNavigationActions), action, true))
                    {
                        case InspectionNavigationActions.LOF:
                            return RedirectToAction("Index", "LOF", new { carId, inspectionId, appointmentPresentationId, customerId, mileage, transmission, driveline, enteredMileage, dealerVehicleId, vin, carIdUndetermined });
                        case InspectionNavigationActions.Services:
                            return RedirectToAction("Index", "MenuPresentation", new { carId, inspectionId, appointmentPresentationId, customerId, mileage, transmission, driveline, enteredMileage, dealerVehicleId, vin, carIdUndetermined });
                    }
                }
                MapErrors(response.Errors);
            }
            return View(model);
        }

        [HttpPost]
        public void ResetInspection(long inspectionId)
        {
            _service.ResetInspection(inspectionId);
        }
        public ActionResult GetPhoto(int id)
        {
            var model = _service.GetPhoto(id);
            return View(model);
        }

        public ActionResult GetVehiclePhotos(long inspectionId)
        {
            return PartialView("_VehiclePhotos", _service.GetAllPhotos(inspectionId));
        }

        public ActionResult SavePhoto(string fileName, long inspectionId)
        {
            VehiclePhotoViewModel photo = new VehiclePhotoViewModel
            {
                FileName = fileName,
                InspectionId = inspectionId
            };
            var response = _service.AddPhoto(photo);
            MapErrors(response.Errors);
            return PartialView("_VehiclePhotos", _service.GetAllPhotos(inspectionId));
        }


        [HttpPost]
        public JsonResult AddDamageIndication(DamageIndicationViewModel model)
        {
            if (model != null)
            {
                var response = _service.SaveDamageIndication(model);
                return Json(response);
            }
            return Json(new { IsSuccess = "false" });
        }

        [HttpPost]
        public JsonResult DeleteDamageIndication(long id)
        {
            ServiceResponse response = new ServiceResponse();
            if (ModelState.IsValid)
            {
                response = _service.DeleteDamageIndication(id);
                return Json(response);
            }
            return Json(response);
        }

        [HttpPost]
        public ActionResult EditDamageIndication(DamageIndicationViewModel model)
        {
            if (ModelState.IsValid)
            {
                var response = _service.SaveDamageIndication(model);
                return Json(response);
            }
            return View(model);
        }

        public ActionResult EditDamageIndication(long id)
        {
            var model = _service.GetDamageIndication(id);
            return View(model);
        }


        public ActionResult DeletePhoto(VehiclePhotoViewModel model)
        {
            ServiceResponse response = new ServiceResponse();
            if (ModelState.IsValid)
            {
                response = _service.DeletePhoto(model);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public string ViewVehicleInspectionPrintPdfFile(long? inspectionId, long? appointmentPresentationID, int dealerId, 
            string enteredMileage, string model, string make, string year,
            bool lFRimScratch, bool lRRimScratch, bool rFRimScratch, bool rRRimScratch, int? lFTireTypeId, int? lRTireTypeId, int? rFTireTypeId, int? rRTireTypeId)
        {
            if(inspectionId.HasValue && inspectionId.Value == -1)
            {
                inspectionId = null;
            }
            if (appointmentPresentationID.HasValue && appointmentPresentationID.Value == -1)
            {
                appointmentPresentationID = null;
            }

            var companySettings = IocManager.Resolve<ICompanyService>();
            var appPresentation = IocManager.Resolve<IAppointmentPresentationService>();
            var pdfDocumentService = IocManager.Resolve<IPdfDocumentService>();
            var fileManager = IocManager.Resolve<IFileManager>();
            string measurementUnit = companySettings.GetSettingAsInteger(CompanySettingsNames.MeasurementUnit) == (int)MeasurementUnits.Imperial ? Labels.Miles : Labels.km;
            
            PdfHeaderViewModel header = new PdfHeaderViewModel();

            header.CustomerInfo = pdfDocumentService.GetPdfHeaderCustomerInfoViewModel(dealerId, appointmentPresentationID.HasValue ? appointmentPresentationID.Value : -1);
            if (header.CustomerInfo == null)
            {
                header.CustomerInfo = new PdfHeaderCustomerInfoViewModel();
                header.CustomerInfo.VehicleMake = make;
                header.CustomerInfo.VehicleModel = model;
                header.CustomerInfo.VehicleYear = year;
            }

            header.CustomerInfo.DateString = header.CustomerInfo.Date == DateTime.MinValue ? DateTime.Now.ToString(companySettings.GetDateTimeFormat(CompanySettingsNames.DateFormat)) : header.CustomerInfo.Date.ToString(companySettings.GetDateTimeFormat(CompanySettingsNames.DateFormat));


            header.CustomerInfo.OdometerReading = string.Format("{0} {1}", enteredMileage, measurementUnit);

            var printAdvisorInformation = companySettings.GetSettingAsBool(CompanySettingsNames.PrintAdvisorInformation);
            if (printAdvisorInformation && AppContext.CurrentIdentity.UserId.HasValue)
            {
                header.ShowAdvisor = true;
                header.Advisor = pdfDocumentService.GetPdfHeaderAdvisorViewModel(AppContext.CurrentIdentity.UserId.Value);
            }

            header.ShowDealer = true;
            header.Dealer = pdfDocumentService.GetPdfHeaderDealerViewModel();
            header.Dealer.LogoImg = companySettings.GetCompanyLogoPath(CompanySettingsNames.CompanyLogo);
            var stream = fileManager.Read(header.Dealer.LogoImg);
            header.Dealer.LogoImgByteArray = fileManager.ReadStreamToByte(stream);
            
            var footer = pdfDocumentService.GetPdfFooterViewModel(appointmentPresentationID.HasValue ? appointmentPresentationID.Value : -1, header.CustomerInfo.Date == DateTime.MinValue ? DateTime.Now : header.CustomerInfo.Date);

            var modelWA = pdfDocumentService.GetWalkaroundInspectionPdfViewModel(inspectionId, appointmentPresentationID);

            modelWA.WalkaroundInspection.LFRimScratch = lFRimScratch;
            modelWA.WalkaroundInspection.LRRimScratch = lRRimScratch;
            modelWA.WalkaroundInspection.RFRimScratch = rFRimScratch;
            modelWA.WalkaroundInspection.RRRimScratch = rRRimScratch;            

            modelWA.WalkaroundInspection.LFTireTypeId = lFTireTypeId == -1 ? null : lFTireTypeId;
            modelWA.WalkaroundInspection.LRTireTypeId = lRTireTypeId == -1 ? null : lRTireTypeId;
            modelWA.WalkaroundInspection.RFTireTypeId = rFTireTypeId == -1 ? null : rFTireTypeId;
            modelWA.WalkaroundInspection.RRTireTypeId = rRTireTypeId == -1 ? null : rRTireTypeId;

            modelWA.Header = header;

            modelWA.Footer = footer;
            
            var pdf = new ViewAsPdf("PdfWalkaroundInspection", modelWA);

            string headerPath = string.Format("{0}://{1}{2}/Dealer/PdfDocuments/PdfEmptyHeader",
                    Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port);
            string footerPath = string.Format("{0}://{1}{2}/Dealer/PdfDocuments/PdfFooter?printed={3}&form={4}",
                    Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port, footer.Printed, footer.From);

            string customSwitches = string.Format("--header-html {0} --footer-html {1}", headerPath, footerPath);
            pdf.CustomSwitches = customSwitches;
            var filePath = string.Format("MenuPresentations");
            var fileName = string.Format("VehicleInspection_{0}.pdf", DateTime.Now.ToString("yyyyMMddHHmm"));
            var url = AppContext.MapUrl($"/UserContent/SmartVMADocuments/{filePath}/{fileName}");            

            ExportPdfFile(pdf, filePath, fileName);

            if (companySettings.GetSettingAsBool(CompanySettingsNames.IsRemotePrint) && AppContext.CurrentIdentity.TenantId.HasValue)
            {
                string prnJobTypeCode = PrintDocumentType.MENU.ToString();

                pdfDocumentService.AddPdfDocumentToPrintQueue(AppContext.CurrentIdentity.TenantId.Value, AppContext.CurrentIdentity.UserId, url, 0, prnJobTypeCode);
            }

            return url;
        }

        //[AllowAnonymous]
        //public ActionResult PrintPhoto(int id)
        //{
        //    var model = _service.GetPhoto(id);
        //    //TO-DO Print Photo
        //    return View(model);
        //}

        //[AllowAnonymous]
        //public ActionResult Save(int id)
        //{
        //    var model = _service.GetPhoto(id);
        //    //TO-DO Print Photo
        //    return View(model);
        //}
    }
}