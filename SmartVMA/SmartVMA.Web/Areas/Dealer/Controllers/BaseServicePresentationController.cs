﻿using System;
using System.IO;
using Rotativa;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.FileManager;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;
using System.Web.Mvc;
using PdfSharp;
using PdfSharp.Pdf;
using SmartVMA.Web.Utilities;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using SmartVMA.Resources;
using System.Linq;
using System.Globalization;

namespace SmartVMA.Web.Areas.Dealer.Controllers
{
    public abstract class BaseServicePresentationController : BaseController
    {
        private readonly IBaseServicePresentationService _baseService;
        private IPdfDocumentService _pdfDocumentService;

        protected BaseServicePresentationController(IAppContext appContext, IBaseServicePresentationService baseService, IPdfDocumentService pdfDocumentService) : base(appContext)
        {
            _baseService = baseService;
            _pdfDocumentService = pdfDocumentService;
        }

        [HttpPost]
        public ActionResult ConfirmService(ConfirmServiceRequestModel modelRequest)
        {
            var model = _baseService.GetConfirmServiceViewModel(modelRequest);
            if (ModelState.IsValid)
            {
                return View("ConfirmService", model);
            }
            else
            {
                return View("ConfirmService", new ConfirmServiceRequestModel());
            }
        }

        [HttpPost]
        public ActionResult SaveServiceConfirmation(SaveConfirmServiceRequestModel request)
        {
            if (ModelState.IsValid)
            {
                var responseValidator = _baseService.CheckValidRequest(request);
                if (!responseValidator.IsSuccess)
                {
                    var confirmReq = Map(request, new ConfirmServiceRequestModel());
                    var modelConfirm = _baseService.GetConfirmServiceViewModel(confirmReq);
                    MapErrors(responseValidator.Errors);
                    return View("ConfirmService", modelConfirm);
                }
                if (!request.DealerCustomerId.HasValue && !request.IsParked && !request.IsDeclined)
                {
                    return CreateCustomer(request);
                }
                var response = _baseService.Save(request);
                if (response.IsSuccess)
                {
                    bool pdfGenerated = false;
                    int documentType = (int)DocumentType.CustomerCopy;
                    if (response.SavedItemId.HasValue && AppContext.CurrentIdentity.TenantId.HasValue)
                    {
                        if (request.InspectionId > 0)
                        {
                            var inspectionService = IocManager.Resolve<IInspectionService>();
                            inspectionService.SetAppointmentToInspection(request.InspectionId, response.SavedItemId.Value);
                        }

                        if (request.IsAccepted)
                        {
                            GeneratePdfFiles(response.SavedItemId.Value, request.DealerCustomerId, AppContext.CurrentIdentity.TenantId.Value, request.Mileage, false);
                            pdfGenerated = true;
                        }
                        if (request.IsDeclined && !request.IsLof)
                        {
                            GeneratePdfFiles(response.SavedItemId.Value, request.DealerCustomerId, AppContext.CurrentIdentity.TenantId.Value, request.Mileage, true);
                            pdfGenerated = true;
                            documentType = (int)DocumentType.MenuDeclined;
                        }
                        //foreach (DocumentType docType in Enum.GetValues(typeof(DocumentType)))
                        //{
                        //    GeneratePdfFile(docType, request.AppointmentPresentationId.Value, request.DealerCustomerId.Value, AppContext.CurrentIdentity.TenantId.Value);
                        //}
                    }

                    if (pdfGenerated)
                    {
                        response.RedirectUrl = Url.Action("Index", "AppointmentPresentation", new { Area = "Dealer", appointmentPresentationId = response.SavedItemId.Value, dealerId = AppContext.CurrentIdentity.TenantId.Value, dealerCustomerId = request.DealerCustomerId, documentType = documentType });
                    }
                    else
                    {
                        response.RedirectUrl = Url.Action("Index", "AppointmentPresentation", new { Area = "Dealer" });
                    }
                    return Json(response);
                }
                MapErrors(response.Errors);
            }

            var confirmRequest = Map(request, new ConfirmServiceRequestModel());
            var model = _baseService.GetConfirmServiceViewModel(confirmRequest);

            return View("ConfirmService", model);
        }

        public ActionResult CreateCustomer(SaveConfirmServiceRequestModel modelRequest)
        {
            var dealerCustomerService = IocManager.Resolve<IDealerCustomerService>();
            var model = dealerCustomerService.GetViewModel(request: modelRequest);
            return View("CreateCustomer", model);
        }

        [HttpPost]
        public ActionResult GetCarIdByFilters(int year, int make, int model, int engine, int transmission, int driveline)
        {
            var _carService = IocManager.Resolve<ICarService>();
            var carId = _carService.GetCarId(year, make, model, engine, transmission, driveline);

            return Json(new { carId = carId });
        }

        // moved to MenuPresentation/LOF
        //[HttpPost]
        //public ActionResult CreateCustomer(DealerCustomerViewModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var dealerCustomerService = IocManager.Resolve<IDealerCustomerService>();
        //        var response = dealerCustomerService.Save(model);
        //        if (response.IsSuccess)
        //        {
        //            var request = Map(model, response.SavedItemId, new ConfirmServiceRequestModel());
        //            return ConfirmService(request);
        //        }
        //        MapErrors(response.Errors);
        //    }
        //    return View("CreateCustomer", model);
        //}


        private ConfirmServiceRequestModel Map(DealerCustomerViewModel src, long? dealerCustomerId, ConfirmServiceRequestModel dest)
        {
            dest.Services = src.ConfirmServicesRequest.Services;
            dest.CarId = src.ConfirmServicesRequest.CarId;
            dest.Mileage = src.ConfirmServicesRequest.Mileage;
            dest.DealerCustomerId = (int?)dealerCustomerId;
            dest.AppointmentPresentationId = src.ConfirmServicesRequest.AppointmentPresentationId;
            dest.VIN = src.VIN;
            dest.Invoice = src.ConfirmServicesRequest.Invoice;

            return dest;
        }

        private ConfirmServiceRequestModel Map(SaveConfirmServiceRequestModel src, ConfirmServiceRequestModel dest)
        {
            dest.CarId = src.CarId;
            dest.Mileage = src.Mileage;
            dest.AppointmentPresentationId = src.AppointmentPresentationId;
            dest.DealerCustomerId = src.DealerCustomerId;
            dest.VIN = src.VIN;
            dest.Invoice = src.Invoice;
            dest.Services = src.Services;
            dest.MenuLevel = src.MenuLevel;
            dest.MenuPackageOpCode = src.MenuPackageOpCode;
            dest.InspectionId = src.InspectionId;
            dest.AllServices = src.AllServices;

            return dest;
        }

        private void GeneratePdfFiles(long appointmentPresentationID, int? dealerCustomerID, int dealerId, int? mileage, bool isDeclined)
        {
            var companySettings = IocManager.Resolve<ICompanyService>();
            var pdfDocumentService = IocManager.Resolve<IPdfDocumentService>();
            var fileManager = IocManager.Resolve<IFileManager>();

            string measurementUnit = companySettings.GetSettingAsInteger(CompanySettingsNames.MeasurementUnit) == (int)MeasurementUnits.Imperial ? Labels.Miles : Labels.km;

            PdfHeaderViewModel header = new PdfHeaderViewModel();

            header.CustomerInfo = _pdfDocumentService.GetPdfHeaderCustomerInfoViewModel(dealerId, appointmentPresentationID);
            if (header.CustomerInfo != null)
            {
                header.CustomerInfo.DateString = header.CustomerInfo.Date.ToString(companySettings.GetDateTimeFormat(CompanySettingsNames.DateFormat));
            }
            else
            {
                header.CustomerInfo = new PdfHeaderCustomerInfoViewModel();
                header.CustomerInfo.Date = DateTime.Now;
                header.CustomerInfo.DateString = header.CustomerInfo.Date.ToString(companySettings.GetDateTimeFormat(CompanySettingsNames.DateFormat));
            }

            header.ShowInvoiceNumber = isDeclined ? false : companySettings.GetSettingAsBool(CompanySettingsNames.DisplayInvoiceNumber);

            var printAdvisorInformation = companySettings.GetSettingAsBool(CompanySettingsNames.PrintAdvisorInformation);
            if (printAdvisorInformation && AppContext.CurrentIdentity.UserId.HasValue)
            {
                header.ShowAdvisor = true;
                header.Advisor = pdfDocumentService.GetPdfHeaderAdvisorViewModel(AppContext.CurrentIdentity.UserId.Value);
            }

            header.ShowDealer = true;
            header.Dealer = pdfDocumentService.GetPdfHeaderDealerViewModel();
            header.Dealer.LogoImg = companySettings.GetCompanyLogoPath(CompanySettingsNames.CompanyLogo);
            var stream = fileManager.Read(header.Dealer.LogoImg);
            header.Dealer.LogoImgByteArray = fileManager.ReadStreamToByte(stream);
            header.CustomerInfo.OdometerReading = string.Format("{0} {1}", header.CustomerInfo.OdometerReading, measurementUnit);

            var footer = _pdfDocumentService.GetPdfFooterViewModel(appointmentPresentationID, header.CustomerInfo.Date);

            var opCode = header.CustomerInfo.Odometer;
            var selectedMenuLevel = header.CustomerInfo.MenuLevel;

            string suffix = "";
            string performancePlan = "";
            string lofCode = "";
            string lofLabel = "";

            if (selectedMenuLevel == 1)
            {
                suffix = companySettings.GetSettingAsString(CompanySettingsNames.MenuSuffixLevel1);
                performancePlan = companySettings.GetSettingAsString(CompanySettingsNames.MenuLabelLevel1);
                lofCode = companySettings.GetSettingAsString(CompanySettingsNames.LOFMenuOpCodeLevel1);
                lofLabel = companySettings.GetSettingAsString(CompanySettingsNames.LOFMenuLabelLevel1);
            }
            if (selectedMenuLevel == 2)
            {
                suffix = companySettings.GetSettingAsString(CompanySettingsNames.MenuSuffixLevel2);
                performancePlan = companySettings.GetSettingAsString(CompanySettingsNames.MenuLabelLevel2);
                lofCode = companySettings.GetSettingAsString(CompanySettingsNames.LOFMenuOpCodeLevel2);
                lofLabel = companySettings.GetSettingAsString(CompanySettingsNames.LOFMenuLabelLevel2);
            }
            if (selectedMenuLevel == 3)
            {
                suffix = companySettings.GetSettingAsString(CompanySettingsNames.MenuSuffixLevel3);
                performancePlan = companySettings.GetSettingAsString(CompanySettingsNames.MenuLabelLevel3);
                lofCode = companySettings.GetSettingAsString(CompanySettingsNames.LOFMenuOpCodeLevel3);
                lofLabel = companySettings.GetSettingAsString(CompanySettingsNames.LOFMenuLabelLevel3);
            }
            
            Array languages = Enum.GetValues(typeof(_Languages));
            var defaultCulture = System.Threading.Thread.CurrentThread.CurrentUICulture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-US");
            var fileSuffix = "EN";
            var languageId = 1;
            var pdf = new ViewAsPdf();
            var filePath = string.Empty;
            var fileName = string.Empty;
            string headerPath = string.Format("{0}://{1}{2}/Dealer/PdfDocuments/PdfEmptyHeader",
                            Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port);
            string footerPath = string.Format("{0}://{1}{2}/Dealer/PdfDocuments/PdfFooter?printed={3}&form={4}",
                    Request.Url.Scheme, Request.Url.Host, Request.Url.Port == 80 ? "" : ":" + Request.Url.Port, footer.Printed, footer.From);

            string customSwitches = string.Format("--header-html {0} --footer-html {1}", headerPath, footerPath);

            if (isDeclined)
            {
                //Generating CC PDF
                var modelCC = _pdfDocumentService.GetCustomerCopyPdfViewModel(appointmentPresentationID);

                modelCC.IncludePrices = true;

                modelCC.Header = header;

                modelCC.ShowShopCharges = companySettings.GetSettingAsBool(CompanySettingsNames.PrintShopCharges);
                modelCC.ShowTaxes = companySettings.GetSettingAsBool(CompanySettingsNames.PrintTaxesOnDocuments);

                var modified = (modelCC.Services.Where(x => x.IsAdditional).Count() > 0 || modelCC.Services.Where(x => x.IsDeclined).Count() > 0);

                modelCC.TotalPrice = modelCC.Services.Where(x => !x.IsDeclined).Sum(x => x.ServicePrice);
                modelCC.GrandTotalPrice = modelCC.TotalPrice + modelCC.TotalTaxChargesAmount + modelCC.TotalShopChargesAmount;

                modelCC.Footer = footer;

                foreach (_Languages language in languages)
                {
                    if (language == _Languages.English)
                    {
                        System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-US");
                        fileSuffix = "EN";
                        languageId = 1;
                    }
                    else if (language == _Languages.French)
                    {
                        System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("fr-FR");
                        fileSuffix = "FR";
                        languageId = 2;
                    }
                    else if (language == _Languages.Spanish)
                    {
                        System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("es-ES");
                        fileSuffix = "ES";
                        languageId = 3;
                    }

                    measurementUnit = companySettings.GetSettingAsInteger(CompanySettingsNames.MeasurementUnit) == (int)MeasurementUnits.Imperial ? Labels.Miles : Labels.km;

                    if (header.CustomerInfo.IsLofMenu.HasValue && header.CustomerInfo.IsLofMenu.Value)
                    {
                        modelCC.Title = string.Format("{0} {1}", Labels.RecommendedMaintenance, lofCode);
                        modelCC.SubTitle = string.Format("{0} {1}", lofLabel, Labels.OilChange);
                    }
                    else
                    {
                        modelCC.Title = string.Format("{0} {1}{2}{3}", Labels.RecommendedMaintenance, modified ? Labels.IsModified : "", (opCode != null ? opCode.Length > 2 ? opCode.Substring(0, opCode.Length - 3) : string.Empty : string.Empty), suffix);
                        modelCC.SubTitle = string.Format("{0} {1} {2} - {3} {4}", opCode, measurementUnit, Labels.IntervalSMS, performancePlan, Labels.Plan);
                    }

                    modelCC.Disclaimer = Labels.DisclaimerDeclined;
                    modelCC.MenuStatus = Labels.DeclinedCapsLock;

                    pdf = new ViewAsPdf("PDFCustomerCopy", modelCC);

                    pdf.CustomSwitches = customSwitches;

                    filePath = string.Format("{0}\\{1}", dealerId, appointmentPresentationID);
                    fileName = string.IsNullOrEmpty(modelCC.Header.CustomerInfo.VIN) ? string.Format("MD_{0}_{1}.pdf", DateTime.Now.ToString("yyyyMMddHHmm"), fileSuffix) : string.Format("MD_VIN{0}_{1}_{2}.pdf", modelCC.Header.CustomerInfo.VIN, DateTime.Now.ToString("yyyyMMddHHmm"), fileSuffix);
                    ExportPdfFile(pdf, filePath, fileName);
                    PdfDocumentViewModel pdfDocument = new PdfDocumentViewModel
                    {
                        AppointmentPresentationId = appointmentPresentationID,
                        DealerCustomerId = dealerCustomerID,
                        Name = fileName,
                        FilePathName = filePath,
                        VIN = modelCC.Header.CustomerInfo.VIN,
                        InvoiceNumber = modelCC.Header.CustomerInfo.InvoiceNumberText,
                        DocumentType = (int)DocumentType.MenuDeclined,
                        DealerId = dealerId,
                        LanguageId = languageId
                    };
                    _pdfDocumentService.AddPdfDocument(pdfDocument);
                }
            }
            else
            {
                #region Customer Copy
                //Generating CC PDF
                var modelCC = _pdfDocumentService.GetCustomerCopyPdfViewModel(appointmentPresentationID);

                modelCC.IncludePrices = true;
                modelCC.Header = header;
                modelCC.ShowShopCharges = companySettings.GetSettingAsBool(CompanySettingsNames.PrintShopCharges);
                modelCC.ShowTaxes = companySettings.GetSettingAsBool(CompanySettingsNames.PrintTaxesOnDocuments);

                var modified = (modelCC.Services.Where(x => x.IsAdditional).Count() > 0 || modelCC.Services.Where(x => x.IsDeclined).Count() > 0);

                modelCC.TotalPrice = modelCC.Services.Where(x => !x.IsDeclined).Sum(x => x.ServicePrice);
                modelCC.GrandTotalPrice = modelCC.TotalPrice + modelCC.TotalTaxChargesAmount + modelCC.TotalShopChargesAmount;

                modelCC.Footer = footer;

                foreach (_Languages language in languages)
                {
                    System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-US");
                    fileSuffix = "EN";
                    if (language == _Languages.English)
                    {
                        System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-US");
                        fileSuffix = "EN";
                        languageId = 1;
                    }
                    else if (language == _Languages.French)
                    {
                        System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("fr-FR");
                        fileSuffix = "FR";
                        languageId = 2;
                    }
                    else if (language == _Languages.Spanish)
                    {
                        System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("es-ES");
                        fileSuffix = "ES";
                        languageId = 3;
                    }

                    if (header.CustomerInfo.IsLofMenu.HasValue && header.CustomerInfo.IsLofMenu.Value)
                    {
                        modelCC.Title = string.Format("{0} {1}", Labels.RecommendedMaintenance, lofCode);
                        modelCC.SubTitle = string.Format("{0} {1}", lofLabel, Labels.OilChange);
                    }
                    else
                    {
                        modelCC.Title = string.Format("{0} {1}{2}{3}", Labels.RecommendedMaintenance, modified ? Labels.IsModified : "", (opCode != null ? opCode.Length > 2 ? opCode.Substring(0, opCode.Length - 3) : string.Empty : string.Empty), suffix);
                        modelCC.SubTitle = string.Format("{0} {1} {2} - {3} {4}", opCode, measurementUnit, Labels.IntervalSMS, performancePlan, Labels.Plan);
                    }

                    modelCC.Disclaimer = Labels.Disclaimer;
                    modelCC.MenuStatus = Labels.AcceptedCapsLock;

                    pdf = new ViewAsPdf("PDFCustomerCopy", modelCC);                    
                    pdf.CustomSwitches = customSwitches;

                    filePath = string.Format("{0}\\{1}", dealerId, appointmentPresentationID);
                    fileName = string.Format("CC_VIN{0}_Invoice{1}_{2}_{3}.pdf", modelCC.Header.CustomerInfo.VIN, modelCC.Header.CustomerInfo.InvoiceNumberText, DateTime.Now.ToString("yyyyMMddHHmm"), fileSuffix);
                    ExportPdfFile(pdf, filePath, fileName);
                    PdfDocumentViewModel pdfDocument = new PdfDocumentViewModel
                    {
                        AppointmentPresentationId = appointmentPresentationID,
                        DealerCustomerId = dealerCustomerID,
                        Name = fileName,
                        FilePathName = filePath,
                        VIN = modelCC.Header.CustomerInfo.VIN,
                        InvoiceNumber = modelCC.Header.CustomerInfo.InvoiceNumberText,
                        DocumentType = (int)DocumentType.CustomerCopy,
                        DealerId = dealerId,
                        LanguageId = languageId
                    };
                    _pdfDocumentService.AddPdfDocument(pdfDocument);
                }
                #endregion

                #region Tech Copy
                //Generating TC PDF
                var modelTC = _pdfDocumentService.GetTechCopyPdfViewModel(appointmentPresentationID);

                modelTC.Header = header;
                modelTC.Footer = footer;

                foreach (_Languages language in languages)
                {
                    System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-US");
                    fileSuffix = "EN";
                    if (language == _Languages.English)
                    {
                        System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-US");
                        fileSuffix = "EN";
                        languageId = 1;
                    }
                    else if (language == _Languages.French)
                    {
                        System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("fr-FR");
                        fileSuffix = "FR";
                        languageId = 2;
                    }
                    else if (language == _Languages.Spanish)
                    {
                        System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("es-ES");
                        fileSuffix = "ES";
                        languageId = 3;
                    }

                    if (header.CustomerInfo.IsLofMenu.HasValue && header.CustomerInfo.IsLofMenu.Value)
                    {
                        modelTC.Title = Labels.ServiceChecklist;
                        modelTC.SubTitle = string.Format("{0} - {1} {2}", lofCode, lofLabel, Labels.OilChange);
                    }
                    else
                    {
                        modelTC.Title = Labels.ServiceChecklist;
                        modelTC.SubTitle = string.Format("{0}{1} - {2} {3} {4} - {5} {6}", opCode != null && opCode.Length > 2 ? opCode.Substring(0, opCode.Length - 3) : string.Empty, suffix, opCode, measurementUnit, Labels.IntervalSMS, performancePlan, Labels.Plan);
                    }

                    pdf = new ViewAsPdf("PdfTechCopy", modelTC);
                    pdf.CustomSwitches = customSwitches;

                    filePath = string.Format("{0}\\{1}", dealerId, appointmentPresentationID);
                    fileName = string.Format("TC_VIN{0}_Invoice{1}_{2}_{3}.pdf", modelTC.Header.CustomerInfo.VIN, modelTC.Header.CustomerInfo.InvoiceNumberText, DateTime.Now.ToString("yyyyMMddHHmm"), fileSuffix);
                    ExportPdfFile(pdf, filePath, fileName);
                    PdfDocumentViewModel pdfDocument = new PdfDocumentViewModel
                    {
                        AppointmentPresentationId = appointmentPresentationID,
                        DealerCustomerId = dealerCustomerID,
                        Name = fileName,
                        FilePathName = filePath,
                        VIN = modelTC.Header.CustomerInfo.VIN,
                        InvoiceNumber = modelTC.Header.CustomerInfo.InvoiceNumberText,
                        DocumentType = (int)DocumentType.TechCopy,
                        DealerId = dealerId,
                        LanguageId = languageId
                    };
                    _pdfDocumentService.AddPdfDocument(pdfDocument);
                }
                #endregion

                #region Parts Copy
                //Generating PC PDF
                var modelPC = _pdfDocumentService.GetPartsCopyPdfViewModel(appointmentPresentationID);
                modelPC.Header = header;
                modelPC.Footer = footer;

                foreach (_Languages language in languages)
                {
                    System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-US");
                    fileSuffix = "EN";
                    if (language == _Languages.English)
                    {
                        System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-US");
                        fileSuffix = "EN";
                        languageId = 1;
                    }
                    else if (language == _Languages.French)
                    {
                        System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("fr-FR");
                        fileSuffix = "FR";
                        languageId = 2;
                    }
                    else if (language == _Languages.Spanish)
                    {
                        System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("es-ES");
                        fileSuffix = "ES";
                        languageId = 3;
                    }

                    if (header.CustomerInfo.IsLofMenu.HasValue && header.CustomerInfo.IsLofMenu.Value)
                    {
                        modelPC.Title = Labels.PartsChecklist;
                        modelPC.SubTitle = string.Format("{0} - {1} {2}", lofCode, lofLabel, Labels.OilChange);
                    }
                    else
                    {
                        modelPC.Title = Labels.PartsChecklist;
                        modelPC.SubTitle = string.Format("{0}{1} - {2} {3} {4} - {5} {6}", opCode != null && opCode.Length > 2 ? opCode.Substring(0, opCode.Length - 3) : string.Empty, suffix, opCode, measurementUnit, Labels.IntervalSMS, performancePlan, Labels.Plan);
                    }

                    pdf = new ViewAsPdf("PdfPartsCopy", modelPC);
                    pdf.CustomSwitches = customSwitches;

                    filePath = string.Format("{0}\\{1}", dealerId, appointmentPresentationID);
                    fileName = string.Format("PC_VIN{0}_Invoice{1}_{2}_{3}.pdf", modelPC.Header.CustomerInfo.VIN, modelPC.Header.CustomerInfo.InvoiceNumberText, DateTime.Now.ToString("yyyyMMddHHmm"), fileSuffix);
                    ExportPdfFile(pdf, filePath, fileName);
                    PdfDocumentViewModel pdfDocument = new PdfDocumentViewModel
                    {
                        AppointmentPresentationId = appointmentPresentationID,
                        DealerCustomerId = dealerCustomerID,
                        Name = fileName,
                        FilePathName = filePath,
                        VIN = modelPC.Header.CustomerInfo.VIN,
                        InvoiceNumber = modelPC.Header.CustomerInfo.InvoiceNumberText,
                        DocumentType = (int)DocumentType.PartsCopy,
                        DealerId = dealerId,
                        LanguageId = languageId
                    };
                    _pdfDocumentService.AddPdfDocument(pdfDocument);
                }
                #endregion

                #region Walkaround Inspection
                //Generating WA PDF
                var modelWA = _pdfDocumentService.GetWalkaroundInspectionPdfViewModel(null, appointmentPresentationID);

                modelWA.Header = header;
                modelWA.Footer = footer;

                foreach (_Languages language in languages)
                {
                    System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-US");
                    fileSuffix = "EN";
                    if (language == _Languages.English)
                    {
                        System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-US");
                        fileSuffix = "EN";
                        languageId = 1;
                    }
                    else if (language == _Languages.French)
                    {
                        System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("fr-FR");
                        fileSuffix = "FR";
                        languageId = 2;
                    }
                    else if (language == _Languages.Spanish)
                    {
                        System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("es-ES");
                        fileSuffix = "ES";
                        languageId = 3;
                    }

                    pdf = new ViewAsPdf("PdfWalkaroundInspection", modelWA);
                    pdf.CustomSwitches = customSwitches;

                    filePath = string.Format("{0}\\{1}", dealerId, appointmentPresentationID);
                    fileName = string.Format("WA_VIN{0}_Invoice{1}_{2}_{3}.pdf", modelWA.Header.CustomerInfo.VIN, modelWA.Header.CustomerInfo.InvoiceNumberText, DateTime.Now.ToString("yyyyMMddHHmm"), fileSuffix);
                    ExportPdfFile(pdf, filePath, fileName);
                    PdfDocumentViewModel pdfDocument = new PdfDocumentViewModel
                    {
                        AppointmentPresentationId = appointmentPresentationID,
                        DealerCustomerId = dealerCustomerID,
                        Name = fileName,
                        FilePathName = filePath,
                        VIN = modelWA.Header.CustomerInfo.VIN,
                        InvoiceNumber = modelWA.Header.CustomerInfo.InvoiceNumberText,
                        DocumentType = (int)DocumentType.WalkAroundInspection,
                        DealerId = dealerId,
                        LanguageId = languageId
                    };
                    _pdfDocumentService.AddPdfDocument(pdfDocument);
                }
                #endregion
            }

            //resetting Culture
            System.Threading.Thread.CurrentThread.CurrentUICulture = defaultCulture;
        }

        private void SavePdf(string viewName, object model, string filePath, string fileName)
        {
            string output = this.RenderView(viewName, model);
            PdfDocument pdf = PdfGenerator.GeneratePdf(output, PageSize.A4);
            byte[] byteArray;
            using (MemoryStream ms = new MemoryStream())
            {
                pdf.Save(ms, false);
                byteArray = new byte[ms.Length];
                ms.Seek(0, SeekOrigin.Begin);
                ms.Flush();
                ms.Read(byteArray, 0, (int)ms.Length);
            }
            var appContext = IocManager.Resolve<IAppContext>();
            var pdfFilePath = $@"{appContext.DocumentsFolder}\{filePath}\{fileName}";
            var fileManager = IocManager.Resolve<IFileManager>();
            fileManager.Save(pdfFilePath, byteArray);
        }

    }
}