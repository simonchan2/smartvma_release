﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;
using System.Xml.Linq;

namespace SmartVMA.Web.Areas.Ws.Controllers
{

    public class XmlController : Controller
    {
        // GET: Ws/Test
        //[AllowAnonymous]
        //public ActionResult Test(string func, string account, string accounts)
        //{
        //    string xmlString = "<xmldata host=\"10.21.2.99\"><doc url=\"http://10.21.2.99/XML/pdfs/1145-CHK.pdf\" prn=\"Brother_DCP-7065DN\" drv=\"Brother DCP-7065DN, using brlaser v3 (grayscale, 2-sided printing)\" cpy=\"1\" tray=\"\">342365</doc></xmldata > ";

        //    return this.Content(xmlString, "text/xml");
        //}

        // GET: Ws/request
        [AllowAnonymous]
        public ActionResult XMLremoteprn(string func, string account, string accounts, int? id)
        {
            string xmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
            string host = Request.ServerVariables["SERVER_NAME"];

            switch (func)
            {
                case "request":
                    try
                    {
                        if (string.IsNullOrEmpty(accounts))
                        {
                            accounts = account;
                        }

                        if (!string.IsNullOrEmpty(accounts))
                        {
                            var service = IocManager.Resolve<IPrnSpoolService>();
                            IEnumerable<PrnSpoolViewModel> printJobs = service.GetRemotePrintJobs(string.IsNullOrEmpty(accounts) ? account : accounts);

                            xmlString += new XElement("xmldata", new XAttribute("host", host),
                                 from job in printJobs
                                 select new XElement("doc", job.Id,
                                      new XAttribute("url", job.docname),
                                      new XAttribute("prn", job.prnname),
                                      new XAttribute("drv", job.prndriver),
                                      new XAttribute("cpy", job.prncopy),
                                      new XAttribute("tray", job.prntray.Trim())
                             )).ToString(SaveOptions.DisableFormatting);
                        }
                    }
                    catch (Exception e)
                    {
                        xmlString += ExceptionResult(e.ToString());
                    }

                    //                        xmlString = @"<?xml version=""1.0"" encoding=""UTF-8"" ?><xmldata host=""10.21.2.12"">
                    //  <doc url=""http://10.21.2.12:54503/Content/1145-CHK.pdf"" prn=""Brother_DCP-7065DN"" drv=""Brother DCP-7065DN, using brlaser v3 (grayscale, 2-sided printing)"" cpy=""1"" tray="""">4</doc>
                    //</xmldata>";
                    //xmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><xmldata host=\"10.21.2.12\"><doc url=\"http://10.21.2.12:54503/Content/1145-CHK.pdf\" prn=\"Brother_DCP-7065DN\" drv=\"Brother DCP-7065DN, using brlaser v3 (grayscale, 2-sided printing)\" cpy=\"1\" tray=\"\">4</doc></xmldata>";
                    //xmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><xmldata host=\"10.21.2.12\"><doc url=\"http://10.21.2.12:54503/Content/1145-CHK.pdf\" prn=\"Brother_DCP-7065DN\" drv=\"Brother DCP-7065DN, using brlaser v3 (grayscale, 2-sided printing)\" cpy=\"1\" tray=\"\">2</doc></xmldata>";
                    System.Diagnostics.Debug.Print(xmlString);
                    break;

                case "printed":
                    if (id.HasValue)
                    {
                        xmlString += UpdateRemotePrintJobStatus(id.Value, "Done");
                    }
                    else
                    {
                        xmlString += ExceptionResult("id is required.");
                    }
                    break;

                case "failed":
                    if (id.HasValue)
                    {
                        xmlString += UpdateRemotePrintJobStatus(id.Value, "Error");
                    }
                    else
                    {
                        xmlString += ExceptionResult("id is required.");
                    }
                    break;
            }
            return this.Content(xmlString, "text/xml");
        }

        private IEnumerable<PrnSpoolViewModel> GetRemotePrintJobs(string accounts)
        {
            var service = IocManager.Resolve<IPrnSpoolService>();

            return service.GetRemotePrintJobs(accounts);
        }

        private string UpdateRemotePrintJobStatus(int id, string status)
        {
            string host = Request.ServerVariables["SERVER_NAME"];
            string s;

            var service = IocManager.Resolve<IPrnSpoolService>();
            ServiceResponse response = service.UpdateRemotePrintJobStatus(id, status);

            if (response.IsSuccess)
            {
                s = new XElement("xmldata", new XAttribute("host", host),
                        new XElement("Ok", "Ok")
                     ).ToString(SaveOptions.DisableFormatting);
            }
            else
            {
                var errors = response.Errors.Select(x => string.Format("{0}: {1}", x.Key, x.Value)).ToArray();
                s = ExceptionResult(string.Join("; ", errors));
            }
            return s;
        }

        private string ExceptionResult(string errorMsg)
        {
            string host = Request.ServerVariables["SERVER_NAME"];
            return new XElement("xmldata", new XAttribute("host", host),
                        new XElement("error", errorMsg)
                     ).ToString(SaveOptions.DisableFormatting);
        }
    }
}