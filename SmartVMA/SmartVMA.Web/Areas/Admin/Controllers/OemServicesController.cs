﻿using System.Collections.Generic;
using System.Linq;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;
using System.Web.Mvc;
using SmartVMA.Core;
using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Resources;

namespace SmartVMA.Web.Areas.Admin.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class OemServicesController : BaseController
    {
        private readonly IOemBasicServiceService _service;
        public OemServicesController(IAppContext appContext, IOemBasicServiceService service) : base(appContext)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            var model = _service.GetAmamServicesViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(OemServiceMaintenancePagingViewModel filter)
        {
            var model = _service.GetAmamServicesIndexViewModel(filter);
            return Json(model);
        }

        [HttpPost]
        public ActionResult Save(IEnumerable<OemServiceMaintenanceViewModel> items, OemServiceMaintenancePagingViewModel filter)
        {
            var selectedServices = string.IsNullOrEmpty(Request["SelectedServices"])
                ? null
                : Request["SelectedServices"].Replace(StaticSettings.DatatableRowId, string.Empty);
            var itemsList = items.ToList();
            ServiceResponse response;
            if (!string.IsNullOrEmpty(selectedServices))
            {
                response = _service.DeleteBulk(selectedServices);
            }
            else
            {
                if (!ModelState.IsValid)
                {
                    var errorResponse = GetErrorResponse();
                    return Json(errorResponse);
                }

                response = _service.Validate(itemsList);
                if (!response.IsSuccess)
                {
                    return Json(response);
                }
                response = _service.Save(itemsList, filter);
            }
            
            response.RedirectUrl = "/Admin/OemServices";
            return Json(response);
        }

        [AllowAnonymous]
        public ActionResult PageChangePrompt()
        {
            return View();
        }
        
        public ActionResult DeletePrompt()
        {
            return View();
        }
    }
}