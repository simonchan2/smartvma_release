﻿using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Core.Services;
using System.Web.Mvc;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.PageRequests;

namespace SmartVMA.Web.Areas.Admin.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class DistributorsController : BaseController
    {
        private readonly IConfigurationSetupService _service;
        public DistributorsController(IAppContext appContext, IConfigurationSetupService service) : base(appContext)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            var model = new CompanyViewModel();
            return View(model);
        }

        public ActionResult ProfileSetup(int? id = null)
        {
            var model = _service.GetProfileSetupDistributorViewModel(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult ProfileSetup(ConfigurationProfileSetupDistributorViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var response = _service.SaveProfileSetup(model, CompanyTypes.Distributor);
            if (response.IsSuccess)
            {
                return View("Success");
            }
            MapErrors(response.Errors);
            return View(model);
        }

        [HttpPost]
        public ActionResult LoadData(PagingRequest request)
        {
            var companyService = IocManager.Resolve<ICompanyService>();
            var model = companyService.GetPagedDistributors(request);

            return Json(new { draw = request.Draw + 1, recordsFiltered = model.ItemsCount, recordsTotal = model.ItemsCount, data = model.Items }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateDefaultDistributorValues(int corpId)
        {
            var model = _service.GetProfileSetupDefaultDistributorViewModel(corpId);
            return PartialView("DistributorProfile",model);
        }
    }
}