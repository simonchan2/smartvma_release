﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SmartVMA.Core;
using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;

namespace SmartVMA.Web.Areas.Admin.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class CustomServicesController : BaseController
    {
        private readonly IAdditionalServiceService _service;
        public CustomServicesController(IAppContext appContext, IAdditionalServiceService service) : base(appContext)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            var model = _service.GetCustomServiceInitViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(CustomServiceMaintenancePagingViewModel request)
        {
            var model = _service.GetIndexViewModel(request);
            return Json(model);
        }

        public ActionResult Save(IEnumerable<CustomServiceViewModel> items)
        {
            var selectedServices = string.IsNullOrEmpty(Request["SelectedServices"])
                ? null
                : Request["SelectedServices"].Replace(StaticSettings.DatatableRowId, string.Empty);
            var itemsList = items.ToList();
            ServiceResponse response;
            if (!string.IsNullOrEmpty(selectedServices))
            {
                response = _service.DeleteBulk(selectedServices);
            }
            else
            {
                if (!ModelState.IsValid)
                {
                    var errorResponse = GetErrorResponse();
                    return Json(errorResponse);
                }

                response = _service.Validate(itemsList);
                if (!response.IsSuccess)
                {
                    return Json(response);
                }

                response = _service.Save(itemsList);
            }
            response.RedirectUrl = "/Admin/CustomServices";
            return Json(response);
        }
        public ActionResult DeletePrompt()
        {
            return View();
        }
    }
}