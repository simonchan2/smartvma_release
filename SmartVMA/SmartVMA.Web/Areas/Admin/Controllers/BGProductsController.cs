﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;
using SmartVMA.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartVMA.Web.Areas.Admin.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class BGProductsController : BaseController
    {
        private readonly IBgProductService _service;

        public BGProductsController(IAppContext appContext, IBgProductService service) : base(appContext)
        {
            _service = service;
        }

        // GET: Admin/BGProducts
        public ActionResult Index(string categoryIds)
        {
            var selectedCategoryIds = categoryIds == null ? "" : categoryIds;
            var model = new BgProductViewModel();
            model.Filter.Category = DropdownItems.GetBgProductCategories().Select(x => Convert.ToInt32(x.Value)).ToList();
            return View(model);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            BgProductViewModel model = _service.GetBgProductProtectionPlanBySKUs(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(BgProductViewModel model)
        {
            var response = _service.Delete(model);
            if (response.IsSuccess)
            {
                return Json(response);
            }
            MapErrors(response.Errors);
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            BgProductViewModel model = _service.GetBgProductProtectionPlanBySKUs(id);           
            return View("Edit",model);
        }

        [HttpPost]
        public ActionResult Edit(BgProductViewModel model)
        {            
            if (ModelState.IsValid)
            {
                var responsePartNumber = _service.PartNumberValidation(model);
                if (responsePartNumber.IsSuccess)
                {
                    var response = _service.SaveBgProduct(model);
                    if (response.IsSuccess)
                    {
                        return Json(response);
                    }
                    MapErrors(response.Errors);
                }
                else
                {
                    MapErrors(responsePartNumber.Errors);
                }

            }
            return View("Edit", model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var model = new BgProductViewModel();
            model.Filter.Category = DropdownItems.GetBgProductCategories().Select(x => Convert.ToInt32(x.Value)).ToList();
            return View("Edit", model);
        }

        [HttpPost]
        public ActionResult LoadData(PagingRequest request, string categoryIds)
        {            
            var draw = Request.Form.GetValues("draw");
            var model = _service.GetPagedItems(request, categoryIds);
            return Json(new { draw = draw.FirstOrDefault() ?? "1", recordsFiltered = model.Items.FirstOrDefault().RecordCount, recordsTotal = model.Items.FirstOrDefault().RecordCount, data = model.Items }, JsonRequestBehavior.AllowGet);
        }
    }
}