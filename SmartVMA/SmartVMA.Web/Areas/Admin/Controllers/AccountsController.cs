﻿using System.Linq;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;
using System.Web.Mvc;
using SmartVMA.Infrastructure.AppContext;
using BotDetect.Web.Mvc;
using System.Web;
using System;
using SmartVMA.Core.Enums;
using System.Globalization;

namespace SmartVMA.Web.Areas.Admin.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class AccountsController : BaseController
    {
        private readonly IUserService _service;

        public AccountsController(IAppContext appContext, IUserService service) : base(appContext)
        {
            _service = service;
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            var model = new LoginViewModel();

            if (Request.Cookies["tempTranslateLanguage"] != null)
            {
                ViewBag.SelectedLanguage = Request.Cookies["tempTranslateLanguage"].Value.Split('/')[2].ToUpper();
            }
            else
            {
                ViewBag.SelectedLanguage = "EN";
            }            

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginViewModel model)
        {
            if (Request.Cookies["tempTranslateLanguage"] != null)
            {
                ViewBag.SelectedLanguage = Request.Cookies["tempTranslateLanguage"].Value.Split('/')[2].ToUpper();
            }
            else
            {
                ViewBag.SelectedLanguage = "EN";
            }

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var response = _service.Login(model);
            if (!response.IsSuccess)
            {
                MapErrors(response.Errors);
                return View(model);
            }
            if(response.ShowStoreSelectPage)
            {
                return View("StoreSelect", response.Model);
            }
            return RedirectToAction("Index", "AppointmentPresentation", new { area = "Dealer", isFromLogin = true });
        }

        [AllowAnonymous]
        public ActionResult PasswordForgot()
        {
            var model = new PasswordForgotViewModel();
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [CaptchaValidation("PasswordForgotField", "PasswordForgotFieldObject", "Incorrect CAPTCHA code!")]
        public ActionResult PasswordForgot(PasswordForgotViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var response = _service.CreateResetPasswordRequest(model);
            if (response.IsSuccess)
            {
                return View("PasswordForgotSuccess");
            }
            MapErrors(response.Errors);
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult PasswordReset(string t, string ttl)
        {
            ttl = ttl.Replace(" ", "+");
            ViewBag.LinkExpired = DateTime.Now > new DateTime(long.Parse(_service.Decrypt(ttl)));
            var model = new PasswordResetViewModel { Token = t };
            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult PasswordReset(PasswordResetViewModel model)
        {
            if (ModelState.IsValid)
            {
                var response = _service.ResetPassword(model);
                if (response.IsSuccess)
                {
                    return RedirectToAction("Index", "AppointmentPresentation", new { area = "Dealer", isFromLogin = true });
                }

                if (response.Errors.Any(x => x.Key == "Token"))
                {
                    return AppContext.IsUserAuthenticated 
                        ? RedirectToAction("Index", "AppointmentPresentation", new { area = "Dealer" }) 
                        : RedirectToAction("Login", "Accounts", new { area = "Admin" });
                }
                MapErrors(response.Errors);
            }
            return View(model);
        }


        public ActionResult Manage()
        {
            var model = _service.GetManageAccountViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Manage(AccountManagementViewModel model)
        {
            if (ModelState.IsValid)
            {
                var response = _service.Save(model);
                if (response.IsSuccess)
                {
                    return Json(response);
                }
                MapErrors(response.Errors);
            }
            return View(model);
        }
        
        public ActionResult ChangePassword()
        {
            var model = new PasswordChangeViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult ChangePassword(PasswordChangeViewModel model)
        {
            if (ModelState.IsValid)
            {
                var response = _service.ChangePassword(model);
                if (response.IsSuccess)
                {
                    return RedirectToAction("Index", "Home", new { area = "" });
                }
                MapErrors(response.Errors);
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult SendPasswordResetEmail(int id)
        {
            var response = _service.CreateResetPasswordRequest(id);
            return Json(response);
        }

        public ActionResult LogOut()
        {
            Session.Abandon();
            Response.Cookies.Clear();
            if (Request.Cookies["googtrans"] != null)
            {
                HttpCookie myCookie = new HttpCookie("googtrans");
                myCookie.Expires = DateTime.Now.AddDays(-1d);
                myCookie.HttpOnly = true;
                Response.Cookies.Add(myCookie);
            }
            if (Request.Cookies["tempTranslateLanguage"] != null)
            {
                HttpCookie myCookie = new HttpCookie("tempTranslateLanguage");
                myCookie.Expires = DateTime.Now.AddDays(-1d);
                myCookie.HttpOnly = true;
                Response.Cookies.Add(myCookie);
            }
            _service.LogOut();
            return RedirectToAction("Login");
        }
    }
}