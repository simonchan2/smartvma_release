﻿using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;
using System.Linq;
using System.Web.Mvc;

namespace SmartVMA.Web.Areas.Admin.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class DealerGroupsController : BaseController
    {
        private readonly ICompanyGroupsService _service;
        public DealerGroupsController(IAppContext appContext, ICompanyGroupsService service) : base(appContext)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            var model = new CompanyGroupViewModel();
            return View(model);
        }

        public ActionResult Create()
        {
            var model = _service.CompanyGroupViewModel();
            return View("Edit", model);
        }

        [HttpPost]
        public ActionResult Create(CompanyGroupViewModel model)
        {
            if (ModelState.IsValid)
            {
                var response = _service.Save(model);
                if (response.IsSuccess)
                {
                    return Json(response);
                }
                MapErrors(response.Errors);
            }
            return View("Edit", model);
        }

        public ActionResult Edit(int id)
        {
            var model = _service.CompanyGroupViewModel(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(CompanyGroupViewModel model)
        {
            if (ModelState.IsValid)
            {
                var response = _service.Save(model);
                if (response.IsSuccess)
                {
                    return Json(response);
                }
                MapErrors(response.Errors);
            }
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            var model = _service.CompanyGroupViewModel(id);
            return View(model);
        }


        [HttpPost]
        public ActionResult Delete(CompanyGroupViewModel model)
        {
            var response = _service.Delete(model);
            if (response.IsSuccess)
            {
                return Json(response);
            }
            MapErrors(response.Errors);
            return View(model);
        }

        [HttpPost]
        public ActionResult LoadData(PagingRequest request)
        {
            var draw = Request.Form.GetValues("draw");
            var model = _service.GetPagedItems(request);

            return Json(new { draw = draw.FirstOrDefault() ?? "1", recordsFiltered = model.ItemsCount, recordsTotal = model.ItemsCount, data = model.Items }, JsonRequestBehavior.AllowGet);
        }
    }
}