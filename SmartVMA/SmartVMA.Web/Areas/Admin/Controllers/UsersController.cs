﻿using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;
using System.Web.Mvc;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Core.InputModels.PageRequests;

namespace SmartVMA.Web.Areas.Admin.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class UsersController : BaseController
    {
        private readonly IUserService _service;
        public UsersController(IAppContext appContext, IUserService service) : base(appContext)
        {
            _service = service;
        }

        public ActionResult Index(int? companyId = null)
        {
            var model = new UserViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(PagingRequest request, int? companyId = null)
        {
            var model = _service.GetIndexViewModel(request, companyId);
            return Json(model);
        }

        public ActionResult Create()
        {
            var model = _service.GetViewModel();
            return View("Edit", model);
        }

        [HttpPost]
        public ActionResult Create(UserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var response = _service.Save(model);
                if (response.IsSuccess)
                {
                    return Json(response);
                }
                MapErrors(response.Errors);
            }
            return View("Edit", model);
        }

        public ActionResult Edit(int id)
        {
            var model = _service.GetViewModel(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(UserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var response = _service.Save(model);
                if (response.IsSuccess)
                {
                    return Json(response);
                }
                MapErrors(response.Errors);
            }
            return View(model);
        }
    }
}