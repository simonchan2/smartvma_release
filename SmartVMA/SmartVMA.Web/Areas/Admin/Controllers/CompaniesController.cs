﻿using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;
using System.Web.Mvc;
using SmartVMA.Core.InputModels.ViewModels;

namespace SmartVMA.Web.Areas.Admin.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class CompaniesController : BaseController
    {
        private readonly ICompanyService _service;
        public CompaniesController(IAppContext appContext, ICompanyService service) : base(appContext)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            var model = new CompanyViewModel();
            return View(model);
        }

        public ActionResult Cancel()
        {
            return View();
        }

        public ActionResult GetCompaniesBreadcrumb(int? id = null)
        {
            var model = _service.GetParentCompaniesNames(id);
            return View(model);
        }

        public ActionResult LoadData(PagingRequest request)
        {
            var model = _service.GetPagedCompanies(request);
            return Json(new { draw = request.Draw + 1, recordsFiltered = model.ItemsCount, recordsTotal = model.ItemsCount, data = model.Items }, JsonRequestBehavior.AllowGet);
        }
    }
}