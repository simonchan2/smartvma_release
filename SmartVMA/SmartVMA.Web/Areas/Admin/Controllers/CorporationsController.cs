﻿using SmartVMA.Web.Controllers;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Core.Services;
using System.Web.Mvc;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Enums;
using SmartVMA.Core;

namespace SmartVMA.Web.Areas.Admin.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class CorporationsController : BaseController
    {
        private readonly IConfigurationSetupService _service;
        public CorporationsController(IAppContext appContext, IConfigurationSetupService service) : base(appContext)
        {
            _service = service;
        }

        public ActionResult ProfileSetup(int? id = null)
        {
            var model = _service.GetProfileSetupViewModel(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult ProfileSetup(ConfigurationProfileSetupViewModel model)
        {
            if (ModelState.IsValid)
            {
                var response = _service.SaveProfileSetup(model, CompanyTypes.Corporation);
                if (response.IsSuccess)
                {
                    return RedirectToAction("ApplicationSettings", "Corporations", new { area = "Admin", id = response.SavedItemId });
                }
                MapErrors(response.Errors);
            }
            model.ApplicationName = StaticSettings.DefaultApplicationName;
            return View(model);
        }

        public ActionResult ApplicationSettings(int id)
        {
            var model = _service.GetCorpApplicationSettings(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult ApplicationSettings(ConfigurationCorpApplicationSettingsViewModel model)
        {
            if (ModelState.IsValid)
            {
                var response = _service.SaveApplicationSettings(model);
                if (response.IsSuccess)
                {
                    return RedirectToAction("Index", "Companies", new { area = "Admin" });
                }
            }
            return View(model);
        }

    }
}