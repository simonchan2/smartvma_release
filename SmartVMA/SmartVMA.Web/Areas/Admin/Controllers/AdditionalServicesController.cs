﻿using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;
using System.Web.Mvc;

namespace SmartVMA.Web.Areas.Admin.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class AdditionalServicesController : BaseController
    {
        private readonly IAdditionalServiceService _service;

        public AdditionalServicesController(IAppContext appContext, IAdditionalServiceService service) : base(appContext)
        {
            _service = service;
        }

        public ActionResult Create()
        {
            var model = _service.GetViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(AdditionalServiceViewModel model)
        {
            if (ModelState.IsValid)
            {
                var response = _service.Save(model);
                if (response.IsSuccess)
                {
                    return Json(response);
                }
                MapErrors(response.Errors);
            }
            return View(model);
        }
    }
}