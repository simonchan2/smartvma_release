﻿using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.FileManager;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;
using System;
using System.Web.Mvc;

namespace SmartVMA.Web.Areas.Admin.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class AppointmentServicesController : BaseController
    {
        private readonly IAppointmentService _service;

        public AppointmentServicesController(IAppContext appContext, IAppointmentService service) : base(appContext)
        {
            _service = service;
        }

        public ActionResult Create()
        {
            var model = _service.GetViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(AdditionalServiceViewModel model)
        {
            if (ModelState.IsValid)
            {
                var response = _service.Save(model);
                if (response.IsSuccess)
                {
                    return Json(response);
                }
                MapErrors(response.Errors);
            }
            return View(model);
        }

        public ActionResult DeleteObsolete()
        {
            DeleteObsoleteAppointmentsViewModel model = new DeleteObsoleteAppointmentsViewModel();
            model.DateTo = DateTime.Now;
            return View(model);
        }

        [HttpPost]
        public ActionResult DeleteObsolete(DeleteObsoleteAppointmentsViewModel model)
        {
            try
            {
                if(model.DateTo == DateTime.MinValue)
                {
                    model.DateTo = DateTime.Now;
                }

                var pdfDocuments = _service.GetPdfDocuments(model.DateTo);
                var fileManager = IocManager.Resolve<IFileManager>();
                var appContext = IocManager.Resolve<IAppContext>();
                
                foreach (var document in pdfDocuments)
                {
                    var pdfFilePath = $@"{appContext.DocumentsFolder}\{document.FilePathName}\{document.Name}";                    
                    fileManager.Delete(pdfFilePath);
                }
                
                fileManager.DeleteEmptyFolders(appContext.DocumentsFolder);

                _service.DeleteObsoleteAppointments(model.DateTo);
                ViewBag.Message = "Obsolete Appointments Were Deleted";
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
            }
            
            return View(model);
        }
    }
}