﻿using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Resources;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Xml;

namespace SmartVMA.Web.Areas.Admin.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class TranslateController : BaseController
    {
        public TranslateController(IAppContext appContext) : base(appContext)
        {
        }
        // GET: Admin/Translate
        public ActionResult Index()
        {
            /*
            string resourcespath = @"C:\Users\btanevski\Source\Workspaces\SmartVMAPlus.WebApp\SmartVMA\SmartVMA.Resources";
                //Request.ApplicationPath + "SmartVMA.Resources";
            DirectoryInfo dirInfo = new DirectoryInfo(resourcespath);
            List<string> cmbResources = new List<string>();
            foreach (FileInfo filInfo in dirInfo.GetFiles())
            {
                string file = filInfo.Name;
                cmbResources.Add(file);
            }
            //cmbResources.Items.Insert(0, new ListItem("Select a Resource File"));

            string filename = @"C:\Users\btanevski\Source\Workspaces\SmartVMAPlus.WebApp\SmartVMA\SmartVMA.Resources\" + "Labels.resx";
            Stream stream = new FileStream(filename, FileMode.Open,
            FileAccess.Read, FileShare.Read);
            ResXResourceReader RrX = new ResXResourceReader(stream);
            IDictionaryEnumerator RrEn = RrX.GetEnumerator();
            SortedList slist = new SortedList();
            while (RrEn.MoveNext())
            {
                slist.Add(RrEn.Key, RrEn.Value);
            }
            RrX.Close();
            stream.Dispose();

            //filename = Request.QueryString["file"];
            //int id = Convert.ToInt32(Request.QueryString["id"]);
            //filename = Request.PhysicalApplicationPath + "App_GlobalResources\\" + filename;
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(filename);
            XmlNodeList nlist = xmlDoc.GetElementsByTagName("data");
            XmlNode childnode = nlist.Item(1);
            childnode.Attributes["xml:space"].Value = "default";
            xmlDoc.Save(filename);
            XmlNode lastnode = childnode.SelectSingleNode("value");
            lastnode.InnerText = "ABOUT";
            xmlDoc.Save(filename);
            */

            // Create the web request  
            HttpWebRequest request = WebRequest.Create("https://www.googleapis.com/language/translate/v2?key=AIzaSyArp5F6VlFXLTroNu_EW7SroKB8jHjNqJo&source=en&target=de&callback=translateText&q='Hello world,Search field must have at least 3 alphanumeric characters and no more than 50 alphanumeric characters,World'") as HttpWebRequest;

            AdditionalServiceViewModel model = new AdditionalServiceViewModel();
            string result = "not translated";
            // Get response  
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                // Get the response stream  
                StreamReader reader = new StreamReader(response.GetResponseStream());

                // Console application output  
                //Console.WriteLine(reader.ReadToEnd());
                result = reader.ReadToEnd();
            }
            model.Description = result;

            return View(model);
        }
    }
}