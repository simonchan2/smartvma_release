﻿using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;
using System.Web.Mvc;
using SmartVMA.Infrastructure.AppContext;
using System;
using SmartVMA.Core.Contracts;
using SmartVMA.Resources;

namespace SmartVMA.Web.Areas.Admin.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class LOFMenuController : BaseController
    {
        private readonly ILofService _service;

        public LOFMenuController(IAppContext appContext, ILofService service) : base(appContext)
        {
            _service = service;
        }

        public ActionResult Index(int? id = null, int level = 1)
        {
            var model = _service.GetLOFMenuViewModel(id, level);
            return View(model);
        }

        [HttpPost]
        public ActionResult LOFMenuImageSelected(string imageID, string imageName, bool isSelected, int selectedLOFMenuLevel)
        {
            try
            {
                var response = new ServiceResponse() { IsSuccess = true };
                response = _service.SaveLOFMenuImages(AppContext.CurrentIdentity.TenantId.Value, imageName, isSelected, selectedLOFMenuLevel);
                if (response.IsSuccess)
                {
                    return Json(response);
                }
                else
                {
                    MapErrors(response.Errors);
                    return View();
                }
            
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ActionResult MessagePopUp()
        {
            ViewBag.Title = Labels.Message;
            return View();
        }

    }
}