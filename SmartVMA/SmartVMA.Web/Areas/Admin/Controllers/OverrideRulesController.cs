﻿using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmartVMA.Core;
using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.EfRepository;
using SmartVMA.Infrastructure;
using SmartVMA.Resources;
using SmartVMA.Web.Utilities;

namespace SmartVMA.Web.Areas.Admin.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class OverrideRulesController : BaseController
    {
        private readonly IOverrideRulesService _overrideRulesService;
        public OverrideRulesController(IAppContext appContext, IOverrideRulesService overrideRulesService) : base(appContext)
        {
            _overrideRulesService = overrideRulesService;
        }

        // GET: Admin/OverrideRules
        [HttpGet]
        [ActionName("Index")]
        public ActionResult Index(long? id = null)
        {
            ConfigurationSearchByOverridesViewModel model = _overrideRulesService.GetOverrideRuleById(id);
            ICarService carService = IocManager.Resolve<ICarService>();
            InitViewBagData(model);
            if (id == null)
            {
                model.Filter = carService.GetVehicleServiceFilter();
                model.Filter.Years = DropdownItems.GetVehicleYears().Select(x => Convert.ToInt32(x.Value)).ToList();
                model.Filter.Makes = DropdownItems.GetVehicleMakes(returnAll: true).Select(x => Convert.ToInt32(x.Value)).ToList();
                model.Filter.Models = DropdownItems.GetVehicleModels(returnAll: true).Select(x => Convert.ToInt32(x.Value)).ToList();
                model.Filter.EngineTypes = DropdownItems.GetVehicleEngines(returnAll: true).Select(x => Convert.ToInt32(x.Value)).ToList();
                model.Filter.TransmissionTypes = DropdownItems.GetVehicleTransmissions(returnAll: true, source: SPNames.GetVehicleTransmissionsFromVinMaster).Select(x => Convert.ToInt32(x.Value)).ToList();
                model.Filter.DriveLines = DropdownItems.GetVehicleDriveLines(returnAll: true, source: SPNames.GetVehicleDriveLinesFromVinMaster).Select(x => Convert.ToInt32(x.Value)).ToList();
            }
            model.Filter.PreserveNA = true;
            return View(model);
        }

        private void InitViewBagData(ConfigurationSearchByOverridesViewModel model)
        {
            ViewBag.OruleId = model.Id;
            ViewBag.Makes = model.FilterResponse.Makes;
            ViewBag.Models = model.FilterResponse.Models;
            ViewBag.EngineTypes = model.FilterResponse.EngineTypes;
            ViewBag.TransmissionTypes = model.FilterResponse.TransmissionTypes;
            ViewBag.DriveLines = model.FilterResponse.DriveLines;
        }

        [HttpPost]
        public JsonResult SearchOverrideRules(ConfigurationSearchByOverridesViewModel model)
        {
            AddFiltersToModel(model);
            model.Overrides = _overrideRulesService.SearchOverrides(model, (Convert.ToInt32(Request["start"]) + StaticSettings.PageSizeAdmin) / StaticSettings.PageSizeAdmin, StaticSettings.PageSizeAdmin);
            int draw = Convert.ToInt32(Request["draw"]);
            if(draw == 1)
            {
                Session[SessionValues.AllFilteredServices] = string.Join(StaticSettings.Delimiter, model.OemBasicServices.Select(x => x.Id.ToString()));
            }
            return Json(new
            {
                data = model.Overrides,
                draw = draw,
                recordsFiltered = model.FilteredServicesCount,
                recordsTotal = model.TotalServicesCount,
                selectedServices = model.OemBasicServices.Where(x => x.Selected).Select(x => StaticSettings.DatatableRowId + x.Id.ToString()).ToList(),
                allServices = model.OemBasicServices.Select(x => StaticSettings.DatatableRowId + x.Id.ToString()).ToList()
            });
        }

        [HttpGet]
        public ActionResult OverrideRules()
        {
            var model = _overrideRulesService.GetOverrideRules();
            return GetIndexView(model, "OverrideRules", "OverrideRules");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var model = _overrideRulesService.GetOverrideRuleByIdForList(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(ConfigurationOverrideRulesViewModel model)
        {
            var response = _overrideRulesService.DeleteMany(model.Id.ToString());
            if (response.IsSuccess)
            {
                return Json(response);
            }
            MapErrors(response.Errors);
            return View(model);
        }

        [HttpPost]
        public ActionResult LoadData(PagingRequest request)
        {
            var draw = Request.Form.GetValues("draw");
            var model = _overrideRulesService.GetPagedItems(request);
            return Json(new { draw = draw.FirstOrDefault() ?? "1", recordsFiltered = model.ItemsCount, recordsTotal = model.ItemsCount, data = model.Items }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Save(ConfigurationSearchByOverridesViewModel model)
        {
            AddFiltersToModel(model);
            if (!ModelState.IsValid)
            {
                return View("Index", model);
            }
            bool areBasicServicesChecked;
            string basicServices = GetBasicServices(model, out areBasicServicesChecked);
            model.OverrideConflicts = _overrideRulesService.GetConflicts(model, basicServices, areBasicServicesChecked);
            ServiceResponse response = new ServiceResponse();
            if (model.OverrideConflicts.Count == 0)
            {
                response = _overrideRulesService.ApplyOverride(model, basicServices, areBasicServicesChecked, null);
            }
            else
            {
                response.Errors.Add("ConflictingRules", Messages.OverrideRuleConflicts);
                MapErrors(response.Errors);
            }
            return View("CheckConflicts", model);
        }
        
        [HttpPost]
        public ActionResult ApplyConflicts(ConfigurationSearchByOverridesViewModel model)
        {
            AddFiltersToModel(model);
            bool areBasicServicesChecked;
            string basicServices = GetBasicServices(model, out areBasicServicesChecked);
            ServiceResponse response = _overrideRulesService.ApplyOverride(
                model, 
                basicServices, 
                areBasicServicesChecked, 
                Request["conflictingRuleIds"]);
            MapErrors(response.Errors);
            return View("CheckConflicts", model);
        }

        #region Private Methods
        private void AddFiltersToModel(ConfigurationSearchByOverridesViewModel model)
        {
            model.Filter.Years = Request[SessionValues.Years].Split(new[] { StaticSettings.Delimiter }, StringSplitOptions.None).Select(int.Parse).ToList();
            model.Filter.Makes = Request[SessionValues.Makes].Split(new[] { StaticSettings.Delimiter }, StringSplitOptions.None).Select(int.Parse).ToList();
            model.Filter.Models = Request[SessionValues.Models].Split(new[] { StaticSettings.Delimiter }, StringSplitOptions.None).Select(int.Parse).ToList();
            model.Filter.EngineTypes = Request[SessionValues.EngineTypes].Split(new[] { StaticSettings.Delimiter }, StringSplitOptions.None).Select(int.Parse).ToList();
            model.Filter.TransmissionTypes = Request[SessionValues.TransmissionTypes].Split(new[] { StaticSettings.Delimiter }, StringSplitOptions.None).Select(int.Parse).ToList();
            model.Filter.DriveLines = Request[SessionValues.DriveLines].Split(new[] { StaticSettings.Delimiter }, StringSplitOptions.None).Select(int.Parse).ToList();
            if (!string.IsNullOrEmpty(Request[SessionValues.SelectedServices]))
            {
                model.SelectedServices = Request[SessionValues.SelectedServices].Replace(StaticSettings.DatatableRowId, string.Empty).Trim(StaticSettings.Delimiter[0]).Split(new[] { StaticSettings.Delimiter }, StringSplitOptions.None).Select(int.Parse).ToList();
            }
            InitViewBagData(model);
        }

        private string GetBasicServices(ConfigurationSearchByOverridesViewModel model, out bool areBasicServicesChecked)
        {
            string basicServices = null;
            areBasicServicesChecked = false;
            if (model.SelectedServices != null)
            {
                IEnumerable<int> allFilteredServices = Session[SessionValues.AllFilteredServices].ToString().Split(new[] { StaticSettings.Delimiter }, StringSplitOptions.None).Select(int.Parse).ToList();
                areBasicServicesChecked = model.SelectedServices.Count() <= Math.Ceiling(Convert.ToDecimal(allFilteredServices.Count()) / 2);
                if (areBasicServicesChecked)
                {
                    basicServices = string.Join(StaticSettings.Delimiter, model.SelectedServices);
                }
                else
                {
                    basicServices = string.Join(StaticSettings.Delimiter, allFilteredServices.Except(model.SelectedServices));
                }
            }
            return basicServices;
        }

        private string GetRequestList(string list)
        {
            return !(string.IsNullOrEmpty(list)) ? list : string.Empty;
        }
        #endregion
    }
}