﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace SmartVMA.Web.Areas.Admin.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class AlertController : BaseController
    {

        private readonly IAlertService _service;
        private readonly IAlertUserService _alertUserService;

        public AlertController(IAppContext appContext, IAlertService service, IAlertUserService alertUserService) : base(appContext)
        {
            _service = service;
            _alertUserService = alertUserService;
        }

        public ActionResult Index()
        {
            var model = _service.GetIndexViewModel();
            return GetIndexView(model);
        }

        public ActionResult AllAlertsJson()
        {
            List<AlertViewModel> model = _service.GetAlerts();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var model = _service.GetViewModel();
            return View("Edit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AlertViewModel model)
        {
            if (ModelState.IsValid)
            {
                var response = _service.Save(model); //use the same method for "New" and "Edit", the service will recognise should it do .Insert() or .Update()
                if (response.IsSuccess)
                {
                    return Json(response);
                }
                MapErrors(response.Errors);
            }
            return View("Edit", model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var model = _service.GetAlert(id);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AlertViewModel model)
        {
            if (ModelState.IsValid)
            {
                var response = _service.Save(model);
                if (response.IsSuccess)
                {
                    return Json(response);
                }
                MapErrors(response.Errors);
            }
            return View("Edit", model);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var model = new AlertViewModel();
            _service.GetAlert(id);
            return View(model);
        }


        [HttpPost]
        public ActionResult Delete(AlertViewModel model)
        {
            var response = _service.Delete(model.Id.Value);
            return Json(response);
        }

        public ActionResult SingleAlert(int id)
        {
            AlertViewModel model = _service.GetAlert(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult SetAlertStatus(int[] alertIds, int statusId)
        {
            var response = new ServiceResponse() { IsSuccess = true };
            if (alertIds != null)
            {
                foreach (var alertId in alertIds)
                {
                    var model = new AlertUserViewModel();
                    model.AlertId = alertId;
                    model.UserId = AppContext.CurrentIdentity.UserId.Value;
                    model.StatusId = statusId;
                    response = _alertUserService.Save(model);
                    if (!response.IsSuccess)
                    {
                        return Json(response);
                    }
                }
            }
            return Json(response);
        }
        public ActionResult Alert(int id)
        {
            var model = _service.GetAlert(id);
            return View(model);
        }


        [HttpGet]
        public ActionResult ShowAlertsOnLongIn()
        {
            var model = _service.GetActiveAlertsForCurrentUser();
            model.Items = model.Items.ToList();
            return View(model);
        }

        [HttpPost]
        public ActionResult LoadData(PagingRequest request)
        {
            var model = _service.GetPagedItems(request);
            return Json(new { draw = request.Draw + 1, recordsFiltered = model.ItemsCount, recordsTotal = model.ItemsCount, data = model.Items }, JsonRequestBehavior.AllowGet);
        }

    }
}