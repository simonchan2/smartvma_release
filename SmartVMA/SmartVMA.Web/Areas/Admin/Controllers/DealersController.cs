﻿using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;
using System.Web.Mvc;
using System.Linq;

namespace SmartVMA.Web.Areas.Admin.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class DealersController : BaseController
    {
        private readonly IConfigurationSetupService _service;

        public DealersController(IAppContext appContext, IConfigurationSetupService service) : base(appContext)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            var appContext = IocManager.Resolve<IAppContext>();
            var model = new CompanyViewModel
            {
                CurentTenantId = appContext.CurrentIdentity.TenantId ?? 0,
                IsCurrentUserDistributorAdmin = appContext.CurrentIdentity.RoleId == (int) UserRoles.DistributorAdmin,
                IsCurrentUserSystemAdmin = appContext.CurrentIdentity.RoleId == (int)UserRoles.SystemAdministrator
            };
            return View(model);
        }

        public ActionResult ProfileSetup(int? id = null)
        {
            var model = _service.GetProfileSetupDealersViewModel(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult ProfileSetup(ConfigurationProfileSetupDealersViewModel model)
        {
            if (ModelState.IsValid)
            {
                var response = _service.SaveProfileSetup(model, CompanyTypes.Dealer);
                if (response.IsSuccess)
                {
                    return RedirectToAction("DefaultUserSetup", "Dealers", new { area = "Admin", tenant = response.SavedItemId });
                }
                MapErrors(response.Errors);
            }
            return View(model);
        }

        public ActionResult DefaultUserSetup(int tenant)
        {
            var userService = IocManager.Resolve<IUserService>();
            var model = userService.GetViewModel(tenant, UserRoles.DealerAdmin);
            model.TenantId = tenant;
            return View(model);
        }

        [HttpPost]
        public ActionResult DefaultUserSetup(UserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userService = IocManager.Resolve<IUserService>();
                var response = userService.Save(model);
                if (response.IsSuccess)
                {
                    return RedirectToAction("MenuSetup", "Dealers", new { area = "Admin", id = model.Tenants.FirstOrDefault() });
                }
                MapErrors(response.Errors);
            }
            return View(model);
        }

        public ActionResult MenuSetup(int id)
        {
            var model = _service.GetMenuSetupViewModel(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult MenuSetup(ConfigurationMenuSetupViewModel model)
        {
            if (ModelState.IsValid)
            {
                var response = _service.SaveMenuSetup(model);
                if (response.IsSuccess)
                {
                    return RedirectToAction("DocumentsSetup", "Dealers", new { area = "Admin", id = model.Id });
                }
                MapErrors(response.Errors);
            }
            return View(model);
        }

        public ActionResult DocumentsSetup(int id)
        {
            var model = _service.GetDocumentSetupViewModel(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult DocumentsSetup(ConfigurationDocumentSetupViewModel model)
        {
            if (ModelState.IsValid)
            {
                var response = _service.SaveDocumentsSetup(model);
                if (response.IsSuccess)
                {
                    return RedirectToAction("ApplicationSettings", "Dealers", new { area = "Admin", id = model.Id });
                }
                MapErrors(response.Errors);
            }
            return View(model);
        }


        public ActionResult ApplicationSettings(int id)
        {
            var model = _service.GetApplicationSettingsViewModel(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult ApplicationSettings(ConfigurationApplicationSettingViewModel model)
        {
            if (ModelState.IsValid)
            {
                var response = _service.SaveApplicationSettings(model);
                if (response.IsSuccess)
                {
                    return RedirectToAction("Index", "Dealers", new { area = "Admin", id = model.Id });
                }
                MapErrors(response.Errors);
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult LoadData(PagingRequest request)
        {
            var companyService = IocManager.Resolve<ICompanyService>();
            var model = companyService.GetPagedDealers(request);

            return Json(new { draw = request.Draw + 1, recordsFiltered = model.ItemsCount, recordsTotal = model.ItemsCount, data = model.Items }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateDefaultDealerValues(int distributorId)
        {
            var model = _service.GetProfileSetupDefaultDealerViewModel(distributorId);
            return PartialView("DealerProfile", model);
        }
    }
}