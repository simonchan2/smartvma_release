﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.InputModels.ViewModels.Admin.CustomServices;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;

namespace SmartVMA.Web.Areas.Admin.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class MapVehiclesController : BaseController
    {
        private readonly IAdditionalServiceService _service;
        public MapVehiclesController(IAppContext appContext, IAdditionalServiceService service) : base(appContext)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            var model = _service.GetCustomServiceInitViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(CustomServiceMaintenancePagingViewModel request)
        {
            var model = _service.GetAllServicesIndexViewModel(request);
            return Json(model);
        }

        public ActionResult Save(MapCustomServicesToVehiclesViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { isSuccess = true });
            }
            var response = _service.Save(model);
            return Json(response);
        }

        public ActionResult ResetFilter()
        {
            return PartialView("MapVehicleFilter", new CustomServicesVehicleFilterViewModel
            {
                Years = new List<int>(),
                Makes = new List<int>(),
                Models = new List<int>(),
                EngineTypes = new List<int>(),
                TransmissionTypes = new List<int>(),
                DriveLines = new List<int>()
            });
        }
    }
}