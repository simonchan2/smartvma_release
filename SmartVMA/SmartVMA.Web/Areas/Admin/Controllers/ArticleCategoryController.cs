﻿using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;
using SmartVMA.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartVMA.Web.Areas.Admin.Controllers
{
    //we are not using this controller for now
    [IocBindable(RegisterAsSelf = true)]
    public class ArticleCategoryController : BaseController
    {

        private readonly IArticleCategoryService _service;

        public ArticleCategoryController(IAppContext appContext, IArticleCategoryService service) : base(appContext)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            var model = _service.GetIndexViewModel();
            return GetIndexView(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var model = _service.GetViewModel();
            return View("CreateCategory", model);
        }

        [HttpPost]
        public ActionResult Create(string name)
        {
            ArticleCategoryViewModel model = new ArticleCategoryViewModel();
            model.Name = name;

            if (ModelState.IsValid)
            {
                var response = _service.Save(model);
                if (response.IsSuccess)
                {
                    return Json(response);
                }
                MapErrors(response.Errors);
            }
            return View("CreateCategory", model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var model = new ArticleCategoryViewModel();
            model = _service.GetArticleCategory(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(ArticleCategoryViewModel model)
        {

            if (ModelState.IsValid)
            {
                var response = _service.Save(model);
                if (response.IsSuccess)
                {
                    return Json(response);
                }
                MapErrors(response.Errors);
            }
            return View("Edit", model);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var model = new ArticleViewModel();
            _service.GetArticleCategory(id);
            return View(model);
        }


        [HttpPost]
        public ActionResult Delete(ArticleCategoryViewModel model)
        {
            var response = _service.Delete(model.Id.Value);
            return Json(response);
        }

        public ActionResult GetAllCategories()
        {
            var model = DropdownItems.GetArticleCategories();
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}