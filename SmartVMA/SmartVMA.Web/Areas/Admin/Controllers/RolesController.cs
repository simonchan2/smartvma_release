﻿using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;
using System.Web.Mvc;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Core.InputModels.PageRequests;
using System.Linq;

namespace SmartVMA.Web.Areas.Admin.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class RolesController : BaseController
    {
        private readonly IRoleService _service;
        public RolesController(IAppContext appContext, IRoleService service) : base(appContext)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            var model = new RoleViewModel();
            return View(model);
        }

        public ActionResult Create()
        {
            var model = _service.GetViewModel();
            return View("Edit", model);
        }

        [HttpPost]
        public ActionResult Create(RoleViewModel model)
        {
            if (ModelState.IsValid)
            {
                var response = _service.Save(model);
                return Json(response);
            }
            return View("Edit", model);
        }

        public ActionResult Edit(int id)
        {
            var model = _service.GetViewModel(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(RoleViewModel model)
        {
            if (ModelState.IsValid)
            {
                var response = _service.Save(model);
                return Json(response);
            }
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            var model = _service.GetViewModel(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(RoleViewModel model)
        {
            var response = _service.Delete(model);
            return Json(response);
        }

        [HttpPost]
        public ActionResult LoadData(PagingRequest request)
        {
            var draw = Request.Form.GetValues("draw");
            var model = _service.GetPagedItems(request);

            return Json(new { draw = draw.FirstOrDefault() ?? "1", recordsFiltered = model.ItemsCount, recordsTotal = model.ItemsCount, data = model.Items }, JsonRequestBehavior.AllowGet);
        }
    }
}