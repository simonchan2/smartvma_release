﻿using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Resources;
using SmartVMA.Web.Controllers;
using SmartVMA.Web.Utilities;
using System;
using System.Web.Mvc;
using System.Collections.Generic;
using SmartVMA.Core.InputModels.PageRequests;
using System.Linq;

namespace SmartVMA.Web.Areas.Admin.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class DataMaintenanceController : BaseController
    {
        private readonly IOemBasicServiceService _service;
        private readonly IOemPartService _partService;
        private readonly IOemServicePartService _servicePartService;

        public DataMaintenanceController(IAppContext appContext, IOemBasicServiceService service, IOemPartService partService, IOemServicePartService servicePartService) : base(appContext)
        {
            _service = service;
            _partService = partService;
            _servicePartService = servicePartService;
        }

        // GET: Admin/DataMaintenance
        public ActionResult Index()
        {
            var model = new List<OemBasicServiceViewModel>();
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var model = new OemBasicServiceViewModel();
            model = _service.GetViewModel(id);
            return View(model);
        }

        public ActionResult OemPart(int? id)
        {
            var model = new OemPartViewModel();
            if (id.HasValue)
            {
                model = _partService.GetViewModel(id);
            }
            return PartialView(model);
        }


        [ActionName("Edit")]
        [AcceptParameter(Name = "reloadEdit", Value = "Add")]
        public ActionResult AddOemPart(OemBasicServiceViewModel model)
        {
            model.OemParts.Add(new OemPartViewModel());
            return View("Edit", model);
        }

        [ActionName("Edit")]
        [AcceptParameter(Name = "reloadEdit", Value = "Save")]
        public ActionResult Save(OemBasicServiceViewModel model)
        {
            if (ModelState.IsValid)
            {
                var response = _service.SaveFromDataMaintenance(model);
                if (!response.IsSuccess)
                {
                    MapErrors(response.Errors);
                    return View("Edit", model);
                }

                foreach (var part in model.OemParts)
                {                 
                        part.OemBasicServiceId = model.Id;
                        if (!part.CarId.HasValue || part.CarId == 0)
                        {
                            part.CarId = model.CarIdFK;
                        }
                        var partResponse = _partService.Save(part);
                        if (!partResponse.IsSuccess )
                        {
                            model.Message = partResponse.IsSuccess ? Messages.DataSavedSuccessfully : Messages.SavingFailed;
                            return View("Edit", model);
                        }

                    //if it's a new part, add the ralationship in OemServiceParts table in DB
                    if (!part.Id.HasValue)
                    {
                        var servicePartModel = new OemServicePartViewModel
                        {
                            OemBasicServiceId = model.Id.Value,
                            OemPartId = (int)partResponse.SavedItemId.Value,
                            Quantity = part.Quantity
                        };
                        response = _servicePartService.Save(servicePartModel);
                    }
                    //if it is an existing part just update the quantity
                    else if(part.OemServicePartId.HasValue)
                    {
                        var servicePartModel = _servicePartService.GetOemServicePartById(part.OemServicePartId.Value);
                        servicePartModel.Quantity = part.Quantity;
                        response = _servicePartService.Save(servicePartModel);
                    }
                }
                model.Message = response.IsSuccess ? Messages.DataSavedSuccessfully: Messages.SavingFailed;
            }
            return View("Edit", model);
        }

        [HttpPost]
        public ActionResult LoadData(PagingRequest request)
        {
            var model = _service.GetPagedItems(request);
            return Json(new { draw = request.Draw + 1, recordsFiltered = model.ItemsCount, recordsTotal = model.ItemsCount, data = model.Items }, JsonRequestBehavior.AllowGet);
        }


        //#region search OemServiceMaintenance

        //public ActionResult OemServiceMaintenance()
        //{
        //    var model = new OemServiceMaintenanceViewModel();
        //    return View(model);
        //}
        //#endregion search OemServiceMaintenance
    }
}