﻿using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;
using System;
using System.Web.Mvc;
using System.Linq;

namespace SmartVMA.Web.Areas.Admin.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class ArticleController : BaseController
    {

        private readonly IArticleService _service;

        public ArticleController(IAppContext appContext, IArticleService service) : base(appContext)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            var model = _service.GetIndexViewModel();
            return GetIndexView(model);
        }

        [HttpGet]        
        public ActionResult Create()
        {
            var model = _service.GetViewModel();
            return View("Edit", model); 
        }
        [HttpPost]
        public ActionResult Create(ArticleViewModel model)
        {
            if (ModelState.IsValid)
            {
                var response = _service.Save(model); //use the same method for "New" and "Edit", the service will recognise should it do .Insert() or .Update()
                if (response.IsSuccess)
                {
                    return Json(response);
                }
                MapErrors(response.Errors);
            }
            return View("Edit", model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var model = new ArticleViewModel();
            model = _service.GetViewModel(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(ArticleViewModel model)
        {

            if (ModelState.IsValid)
            {
                var response = _service.Save(model);
                if (response.IsSuccess)
                {
                    return Json(response);
                }
                MapErrors(response.Errors);
            }
            return View("Edit", model);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var model = new ArticleViewModel();
            _service.GetArticle(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(ArticleViewModel model)
        {
            var response = _service.Delete(model.Id.Value);
            return Json(response);
        }

        [HttpGet]
        public ActionResult Search()
        {
            ArticleViewModel model = new ArticleViewModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult SearchedItems(string key, int[] categoryIds)
        {
            try
            {
                var model = _service.Search(key, categoryIds);
                return PartialView("SearchedItemsResults", model);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        [HttpGet]
        public ActionResult SingleArticle(int id)
        {
            var model = _service.GetViewModel(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult LoadData(PagingRequest request)
        {
            var draw = Request.Form.GetValues("draw");
            var model = _service.GetPagedItems(request);

            return Json(new { draw = draw.FirstOrDefault() ?? "1", recordsFiltered = model.ItemsCount, recordsTotal = model.ItemsCount, data = model.Items }, JsonRequestBehavior.AllowGet);
        }
    }
}