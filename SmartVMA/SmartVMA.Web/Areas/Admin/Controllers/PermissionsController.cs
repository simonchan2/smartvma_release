﻿using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Controllers;
using System.Linq;
using System.Web.Mvc;

namespace SmartVMA.Web.Areas.Admin.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class PermissionsController : BaseController
    {
        private readonly IPermissionService _service;

        
        public PermissionsController(IAppContext appContext, IPermissionService service) : base(appContext)
        {
            _service = service;
        }

        public ActionResult Index(int roleId)
        {
            var model = _service.GetIndexViewModel(roleId);
            return GetIndexView(model);
        }


        public ActionResult Edit(int id, int roleId)
        {
            var model = _service.GetViewModel(id, roleId);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(PermissionViewModel model)
        {
            if (ModelState.IsValid)
            {
                var response = _service.Save(model);
                return Json(response);
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult GrantPermissions(PermissionSaveViewModel model)
        {
            var response = _service.GrantPermissionsForRole(model);
            return RedirectToAction("Index", new { roleId = model.RoleId });
        }

        [HttpPost]
        public ActionResult SyncPermissions(int roleId)
        {
            var response = _service.UpdatePermissions();
            return RedirectToAction("Index", new { roleId });
        }

        [HttpPost]
        public ActionResult LoadData(PagingRequest request, int roleId)
        {
            var draw = Request.Form.GetValues("draw");
            var model = _service.GetPagedItems(request, roleId);

            return Json(new { draw = draw.FirstOrDefault() ?? "1", recordsFiltered = model.ItemsCount, recordsTotal = model.ItemsCount, data = model.Items }, JsonRequestBehavior.AllowGet);
        }

    }
}