﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SmartVMA.Core.Enums;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Core.Services;
using SmartVMA.Resources;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.EfRepository;
using SmartVMA.Infrastructure.AppContext;
using Newtonsoft.Json;
using SmartVMA.Core;
using SmartVMA.Infrastructure;
using SmartVMA.Infrastructure.CacheManager;
using SmartVMA.Infrastructure.Helper;

namespace SmartVMA.Web.Utilities
{
    public class DropdownItems
    {
        public static IEnumerable<SelectListItem> GetTimeFrames()
        {
            List<SelectListItem> timeFrameList = (from c in Enum.GetNames(typeof(TimeFrames))
                                                  let enumValue = Enum.Parse(typeof(TimeFrames), c)
                                                  select new SelectListItem
                                                  {
                                                      Value = ((int)enumValue).ToString(),
                                                      Text = Labels.ResourceManager.GetString(c)
                                                  }).ToList();

            return timeFrameList;
        }

        public static IEnumerable<SelectListItem> GetAdvisorList(bool addNotAssignedOption)
        {
            var service = IocManager.Resolve<IAppointmentPresentationService>();

            var advisorList = (from c in service.GetAdvisorList()
                               select new SelectListItem
                               {
                                   Value = c.Id.ToString(),
                                   Text = c.Name
                               }).ToList();

            if (addNotAssignedOption)
            {
                SelectListItem item = new SelectListItem();
                item.Text = Labels.NotAssigned;
                item.Value = "-1";
                advisorList.Add(item);
            }

            //return result as IEnumerable<SelectListItem>;            
            return advisorList;
        }

        public static IEnumerable<SelectListItem> GetAllAdvisors()
        {
            var service = IocManager.Resolve<IUserService>();
            var appContext = IocManager.Resolve<IAppContext>();
            var currentTenantId = appContext.CurrentIdentity.TenantId;
            if (currentTenantId == null)
            {
                return new List<SelectListItem>();
            }

            var model = service.GetUsersByRole(currentTenantId.Value, UserRoles.MenuAdvisor);
            return model.Items.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = $"{x.FirstName} {x.LastName}" });
        }

        public static IEnumerable<SelectListItem> GetUserStatusCodeList(int? id = null)
        {
            var service = IocManager.Resolve<IUserStatusCodeService>();
            var model = service.GetIndexViewModel();

            return model.Items.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Description, Selected = x.Id == id });
        }

        public static IEnumerable<SelectListItem> GetRoles(int? id = null)
        {
            var service = IocManager.Resolve<IRoleService>();
            var model = service.GetIndexViewModel();

            return model.Items.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Role, Selected = x.Id == id });
        }


        public static IEnumerable<SelectListItem> GetCompanies(int? id = null)
        {
            var service = IocManager.Resolve<ICompanyService>();
            var model = service.GetIndexViewModel();

            var result = new List<SelectListItem>();
            result.AddRange(model.Items.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name, Selected = x.Id == id }));
            return result;
        }

        public static IEnumerable<SelectListItem> GetDefaultCompany()
        {
            var service = IocManager.Resolve<ICompanyService>();
            var model = service.GetIndexViewModel();

            var result = new List<SelectListItem>();
            result.AddRange(model.Items.Where(x => x.Id == 1).Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name, Selected = x.Id == 1 }));
            return result;
        }

        public static IEnumerable<SelectListItem> GetCompanies(IEnumerable<int> ids)
        {
            var service = IocManager.Resolve<ICompanyService>();
            var model = service.GetIndexViewModel();

            var result = new List<SelectListItem>();
            result.AddRange(model.Items.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name, Selected = ids.Any(id => x.Id == id) }));
            return result;
        }
        public static IEnumerable<SelectListItem> GetDistributors(int? id = null)
        {
            var service = IocManager.Resolve<ICompanyService>();
            var model = service.GetDistributors();

            return model.Items.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name, Selected = x.Id == id });
        }

        public static IEnumerable<SelectListItem> GetCorporations(int? id = null)
        {
            var service = IocManager.Resolve<ICompanyService>();
            var model = service.GetCorporations();

            return model.Items.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name, Selected = x.Id == id });
        }

        public static IEnumerable<SelectListItem> GetCompaniesForUser(string email, int? tenantId)
        {
            var service = IocManager.Resolve<ICompanyService>();
            var result = new List<SelectListItem>(service.GetCompaniesForUser(email)
                .Items.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }));

            if(!tenantId.HasValue && result.Count > 0)
            {
                result[0].Selected = true;
            }

            return result;
        }

        public static IEnumerable<SelectListItem> GetCompaniesForUser(string email)
        {
            var service = IocManager.Resolve<ICompanyService>();
            var result = new List<SelectListItem>(service.GetCompaniesForUser(email)
                .Items.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }));

            return result;
        }



        public static List<SelectListItem> GetVehicleYears()
        {
            var service = IocManager.Resolve<ICarService>();
            var cacheManager = IocManager.Resolve<ICacheManager>();
            List<SelectListItem> years = new List<SelectListItem>();
            List<VehicleYearInfo> vehicleServiceResults = cacheManager.GetOrSet(ConfigurationSettings.CacheKeyVehicleYears, () => service.GetAllVehicleYears());
            if (vehicleServiceResults != null && vehicleServiceResults.Count > 0)
            {
                years = (from c in vehicleServiceResults
                         select new SelectListItem
                         {
                             Value = c.Id.ToString(),
                             Text = c.Year,
                             Selected = vehicleServiceResults.Count == 1
                         }).OrderByDescending(c => c.Value).ToList();
            }

            return years;
        }

        public static List<SelectListItem> GetBgProducts()
        {
            var service = IocManager.Resolve<IBgProductService>();

            return service.GetBgProducts()
                .Select(x => new SelectListItem { Text = $"{x.PartNumber} - {x.ProductDescription}", Value = x.Id.ToString() }).ToList();
        }

        public static List<SelectListItem> GetBgProductCategories()
        {
            var service = IocManager.Resolve<IBgProductService>();
            var categories = service.GetAllBgProductCategories().Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Category
            })
            .OrderBy(x => x.Text).ToList();

            return categories;
        }

        public static List<SelectListItem> GetBgProductCategories(List<int> bgProducts, int? currentValue)
        {
            var service = IocManager.Resolve<IBgProductService>();
            var categories = service.GetAllBgProductCategories(bgProducts).Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Category,
                Selected = x.Id == currentValue
            })
            .OrderBy(x => x.Text).ToList();

            return categories;
        }

        public static List<SelectListItem> GetVehicleMakes(string years = null, bool returnAll = false)
        {
            var service = IocManager.Resolve<ICarService>();
            List<SelectListItem> vehicleMakes = new List<SelectListItem>();
            List<VehicleMakeInfo> vehicleServiceResults = null;
            if (years != null)
            {
                vehicleServiceResults = service.GetFilteredVehicleMakesByYear(years);
            }
            else if (returnAll)
            {
                vehicleServiceResults = service.GetAllVehicleMakes();
            }
            if (vehicleServiceResults != null && vehicleServiceResults.Count > 0)
            {
                var vehicleGroups = (from w in vehicleServiceResults.Select(w => w.Make)
                                     group w by w[0] into g
                                     select new SelectListGroup
                                     {
                                         Name = g.Key.ToString()
                                     }).ToList();

                vehicleMakes = (from c in vehicleServiceResults
                                select new SelectListItem
                                {
                                    Value = c.Id.ToString(),
                                    Text = c.Make,
                                    Group = vehicleGroups.Where(d => c.Make.StartsWith(d.Name)).FirstOrDefault(),
                                    Selected = vehicleServiceResults.Count == 1
                                }).OrderBy(c => c.Group.Name).ToList();
            }

            return vehicleMakes;
        }

        public static List<SelectListItem> GetVehicleModels(string years = null, string makes = null, bool returnAll = false)
        {
            var service = IocManager.Resolve<ICarService>();
            List<SelectListItem> vehicleModels = new List<SelectListItem>();
            List<VehicleModelInfo> vehicleServiceResults = null;
            if (years != null && makes != null)
            {
                vehicleServiceResults = service.GetFilteredVehicleModels(years, makes);
            }
            else if (returnAll)
            {
                vehicleServiceResults = service.GetAllVehicleModels();
            }
            if (vehicleServiceResults != null && vehicleServiceResults.Count > 0)
            {
                var vehicleGroups = (from w in vehicleServiceResults.Select(w => w.Model)
                                     group w by w[0] into g
                                     select new SelectListGroup
                                     {
                                         Name = g.Key.ToString()
                                     }).ToList();

                vehicleModels = (from c in vehicleServiceResults
                                 select new SelectListItem
                                 {
                                     Value = c.Id.ToString(),
                                     Text = c.Model,
                                     Group = vehicleGroups.Where(d => c.Model.StartsWith(d.Name)).FirstOrDefault(),
                                     Selected = vehicleServiceResults.Count == 1
                                 }).OrderBy(c => c.Group.Name).ToList();
            }

            return vehicleModels;
        }

        public static List<SelectListItem> GetVehicleEngines(string years = null, string makes = null, string models = null, bool returnAll = false)
        {
            var service = IocManager.Resolve<ICarService>();
            List<SelectListItem> vehicleEngines = new List<SelectListItem>();
            List<VehicleEngineInfo> vehicleServiceResults = null;
            if (years != null && makes != null && models != null)
            {
                vehicleServiceResults = service.GetFilteredVehicleEngines(years, makes, models);
            }
            else if (returnAll)
            {
                vehicleServiceResults = service.GetAllVehicleEngines();
            }
            if (vehicleServiceResults != null && vehicleServiceResults.Count > 0)
            {
                var vehicleGroups = (from w in vehicleServiceResults.Select(w => w.Engine)
                                     group w by w[0] into g
                                     select new SelectListGroup
                                     {
                                         Name = g.Key.ToString()
                                     }).ToList();

                vehicleEngines = (from c in vehicleServiceResults
                                  select new SelectListItem
                                  {
                                      Value = c.Id.ToString(),
                                      Text = c.Engine,
                                      Group = vehicleGroups.FirstOrDefault(d => c.Engine.StartsWith(d.Name)),
                                      Selected = vehicleServiceResults.Count == 1
                                  }).OrderBy(c => c.Group.Name).ToList();
            }

            return vehicleEngines;
        }

        public static List<SelectListItem> GetVehicleTransmissions(string years = null, string makes = null, string models = null, string engines = null, bool returnAll = false, string source = SPNames.GetVehicleTransmissions)
        {
            var service = IocManager.Resolve<ICarService>();
            List<SelectListItem> vehicleTransmissions = new List<SelectListItem>();
            List<VehicleTransmissionInfo> vehicleServiceResults = null;
            if (years != null && makes != null && models != null && engines != null)
            {
                vehicleServiceResults = service.GetFilteredVehicleTransmissions(years, makes, models, engines, source);
            }
            else if (returnAll)
            {
                vehicleServiceResults = service.GetAllVehicleTransmissions(source);
            }

            if (vehicleServiceResults != null && vehicleServiceResults.Count > 0)
            {
                var vehicleGroups = (from w in vehicleServiceResults.Select(w => w.Transmission)
                                     group w by w[0] into g
                                     select new SelectListGroup
                                     {
                                         Name = g.Key.ToString()
                                     }).ToList();

                vehicleTransmissions = (from c in vehicleServiceResults
                                        select new SelectListItem
                                        {
                                            Value = c.Id.ToString(),
                                            Text = c.Transmission,
                                            Group = vehicleGroups.Where(d => c.Transmission.StartsWith(d.Name)).FirstOrDefault(),
                                            Selected = vehicleServiceResults.Count == 1
                                        }).OrderBy(c => c.Group.Name).ToList();
            }

            return vehicleTransmissions;
        }

        public static List<SelectListItem> GetVehicleDriveLines(string years = null, string makes = null, string models = null, string engines = null, string transmissions = null, bool returnAll = false, string source = SPNames.GetVehicleDriveLines)
        {
            var service = IocManager.Resolve<ICarService>();
            List<SelectListItem> vehicleDriveLines = new List<SelectListItem>();
            List<VehicleDriveLineInfo> vehicleServiceResults = null;
            if (years != null && makes != null && models != null && engines != null && transmissions != null)
            {
                vehicleServiceResults = service.GetFilteredVehicleDriveLines(years, makes, models, engines, transmissions, source);
            }
            else if (returnAll)
            {
                vehicleServiceResults = service.GetAllVehicleDriveLines(source);
            }
            if (vehicleServiceResults != null && vehicleServiceResults.Count > 0)
            {
                var vehicleGroups = (from w in vehicleServiceResults.Select(w => w.DriveLine)
                                     group w by w[0] into g
                                     select new SelectListGroup
                                     {
                                         Name = g.Key.ToString()
                                     }).ToList();

                vehicleDriveLines = (from c in vehicleServiceResults
                                     select new SelectListItem
                                     {
                                         Value = c.Id.ToString(),
                                         Text = c.DriveLine,
                                         Group = vehicleGroups.Where(d => c.DriveLine.StartsWith(d.Name)).FirstOrDefault(),
                                         Selected = vehicleServiceResults.Count == 1
                                     }).OrderBy(c => c.Group.Name).ToList();
            }

            return vehicleDriveLines;
        }

        public static List<SelectListItem> GetVehicleSteerings()
        {
            var service = IocManager.Resolve<IVinMasterService>();
            List<SelectListItem> vehicleSteerings = new List<SelectListItem>();
            List<string> vehicleServiceResults = service.GetAllVehicleSteerings();

            if (vehicleServiceResults != null && vehicleServiceResults.Count > 0)
            {
                var vehicleGroups = (from w in vehicleServiceResults.Select(w => w)
                                     group w by w[0] into g
                                     select new SelectListGroup
                                     {
                                         Name = g.Key.ToString()
                                     }).ToList();

                vehicleSteerings = (from c in vehicleServiceResults
                                    select new SelectListItem
                                    {
                                        Value = c,
                                        Text = c,
                                        Group = vehicleGroups.Where(d => c.StartsWith(d.Name)).FirstOrDefault()
                                    }).OrderBy(c => c.Group.Name).ToList();
            }
            return vehicleSteerings;
        }

        public static IEnumerable<SelectListItem> GetCountries(int? id = null)
        {
            var service = IocManager.Resolve<ICountryService>();
            return service.GetCountries().Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name, Selected = x.Id == id });
        }

        public static IEnumerable<SelectListItem> GetStates(int? id = null, int? countryId = null)
        {
            if (!countryId.HasValue || countryId == 0)
            {
                return new List<SelectListItem>();
            }
            var service = IocManager.Resolve<ICountryService>();
            return service.GetStates(countryId).Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name, Selected = x.Id == id });
        }

        public static IEnumerable<SelectListItem> GetLanguages(int? id = null)
        {
            var service = IocManager.Resolve<ICompanySettingService>();
            return service.GetLanguages().Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.LanguageName, Selected = x.Id == id });
        }

        public static IEnumerable<SelectListItem> GetLanguagesCode(int? id = null)
        {
            var service = IocManager.Resolve<ICompanySettingService>();
            return service.GetLanguagesCode().Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.LanguageCode, Selected = x.Id == id });
        }

        public static IEnumerable<SelectListItem> GetDateFormats(int? id = null)
        {
            var service = IocManager.Resolve<ICompanySettingService>();
            return service.GetDateFormats().Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Format, Selected = x.Id == id });
        }

        public static IEnumerable<SelectListItem> GetMeasurementUnits(int? id = null)
        {
            var service = IocManager.Resolve<ICompanySettingService>();
            return service.GetMeasurementUnits().Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Description, Selected = x.Id == id });
        }

        public static IEnumerable<SelectListItem> GetNumericDropdownItems(int minElement, int maxElement, int? selectedValue = null)
        {
            var result = new List<SelectListItem>();
            for (var i = minElement; i <= maxElement; i++)
            {
                result.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString(), Selected = i == selectedValue });
            }
            return result;
        }

        public static IEnumerable<SelectListItem> GetLOFMenuLabels(int id, int? selectedValue = null)
        {
            var result = new List<SelectListItem>();
            var service = IocManager.Resolve<ICompanyService>();

            foreach (LOFMenu item in service.GetLOFMenuLevels(id))
            {
                result.Add(new SelectListItem { Text = item.Name, Value = item.Level.ToString(), Selected = item.Level == selectedValue });
            }

            return result;
        }


        public static IEnumerable<SelectListItem> GetDmsUserTypes(int? id = null)
        {
            var service = IocManager.Resolve<IDMSUserTypeService>();
            return service.GetDmsUserTypes().Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name, Selected = x.Id == id });
        }

        public static IEnumerable<SelectListItem> GetDmsTypes(int? id = null)
        {
            var service = IocManager.Resolve<IDMSTypesService>();
            var result = service.GetDmsTypes().Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name, Selected = x.Id == id }).ToList();
            SelectListItem item = new SelectListItem();
            item.Text = Labels.NoDMS;
            item.Value = "-1";
            item.Selected = !id.HasValue || (id.HasValue && id.Value == -1);
            result.Add(item);

            return result as IEnumerable<SelectListItem>;
        }

        public static IEnumerable<SelectListItem> GetArticleCategories()
        {
            var service = IocManager.Resolve<IArticleCategoryService>();
            var model = service.GetIndexViewModel();

            return model.Items.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name }).ToList();
        }

        public static IEnumerable<SelectListItem> GetDealers(int? distributorId, IEnumerable<int> ids = null)
        {
            if (!distributorId.HasValue)
            {
                return new List<SelectListItem>();
            }

            var service = IocManager.Resolve<ICompanyService>();
            var model = service.GetDealers(distributorId);
            if (ids == null)
            {
                return model.Items.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name });
            }
            return model.Items.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name, Selected = ids.Any(y => y == x.Id) });
        }

        public static IEnumerable<SelectListItem> GetOilTypes()
        {
            var result = new List<SelectListItem>();
            foreach (OilType oilType in Enum.GetValues(typeof(OilType)))
            {
                result.Add(new SelectListItem { Value = ((int)oilType).ToString(), Text = Labels.ResourceManager.GetString(oilType.ToString()) });
            }
            return result;
        }

        public static IEnumerable<SelectListItem> GetOilViscosity()
        {
            var result = new List<SelectListItem>();
            foreach (OilViscosity oilViscosity in Enum.GetValues(typeof(OilViscosity)))
            {
                result.Add(new SelectListItem { Value = ((int)oilViscosity).ToString(), Text = Labels.ResourceManager.GetString(oilViscosity.ToString()) });
            }
            return result;
        }


        public static IEnumerable<SelectListItem> GetOemComponentsShownAsOption()
        {
            var service = IocManager.Resolve<IOemComponentService>();
            var model = service.GetAllOemComponentsShownAsOption();

            return model.Items.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Description }).ToList().OrderBy(x => x.Text);
        }

        public static IEnumerable<SelectListItem> GetMileagesByCar(int carId, int? selected = null)
        {
            var service = IocManager.Resolve<ICarService>();
            var mileages =
                service.GetAllMileages(carId, selected ?? 0)
                    .Select(
                        x => new SelectListItem { Value = x.ToString(), Text = x.ToString(), Selected = (x == selected) });
            return mileages;
        }

        public static IEnumerable<SelectListItem> GetMileages(int measurementUnitId, int? selected = null)
        {
            var service = IocManager.Resolve<ICarService>();
            var mileages =
                service.GetAllDistinctMileages(measurementUnitId)
                    .Select(
                        x => new SelectListItem { Value = x.ToString(), Text = x.ToString(), Selected = (x == selected) });
            return mileages;
        }

        public static IEnumerable<SelectListItem> GetAppointmentPresentationStatuses()
        {
            var result = new List<SelectListItem>();
            foreach (AppointmentPresentationStatus appointmentPresentationStatus in Enum.GetValues(typeof(AppointmentPresentationStatus)))
            {
                result.Add(new SelectListItem { Value = ((int)appointmentPresentationStatus).ToString(), Text = appointmentPresentationStatus.ToString() });
            }
            return result;
        }

        public static IEnumerable<SelectListItem> GetAppointmentFilterStatuses()
        {
            var result = new List<SelectListItem>();
            foreach (AppointmentFilterStatus appointmentFilterStatus in Enum.GetValues(typeof(AppointmentFilterStatus)))
            {
                result.Add(new SelectListItem { Value = ((int)appointmentFilterStatus).ToString(), Text = appointmentFilterStatus.ToString() });
            }
            return result;
        }

        public static IEnumerable<SelectListItem> GetEngineOilTypes()
        {
            var service = IocManager.Resolve<IEngineOilTypeService>();
            var list =
                service.GetAllEngineOilTypes().Select(x => new SelectListItem { Value = x.OilType, Text = x.OilType });
            return list;
        }

        public static IEnumerable<SelectListItem> GetFluidViscosityInDealerOilViscosityList()
        {
            var service = IocManager.Resolve<IFluidViscosityService>();
            var list =
                service.GetAllFluidViscosityInDealerOilViscosityList().Select(x => new SelectListItem { Value = x.Viscosity, Text = x.Viscosity });
            return list;
        }

        public static IEnumerable<SelectListItem> GetDistinctViscosity()
        {
            var service = IocManager.Resolve<IOemPartService>();
            var list =
                service.GetDistinctViscosity().Select(x => new SelectListItem { Value = x, Text = x }).OrderBy(x => x.Value);
            return list;
        }

        public static IEnumerable<SelectListItem> GetDistinctOilTypes()
        {
            var service = IocManager.Resolve<IOemPartService>();
            var list =
                service.GetDistinctOilTypes().Select(x => new SelectListItem { Value = x, Text = x }).OrderBy(x => x.Value);
            return list;
        }

        public static IEnumerable<SelectListItem> GetVideos()
        {
            var service = IocManager.Resolve<IBgVideoClipsService>();
            var list = service.GetVideos().Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Description });
            return list;
        }

        public static IEnumerable<SelectListItem> GetVideos(List<int> bgProducts, int? currentVideoId)
        {
            var service = IocManager.Resolve<IBgVideoClipsService>();
            var list = service.GetVideos(bgProducts).Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Description, Selected = x.Id == currentVideoId });
            return list;
        }

        //public static IEnumerable<SelectListItem> GetBgProtectionPlans()
        //{
        //    var service = IocManager.Resolve<IBgProtectionPlanService>();
        //    var list = service.GetPlans().Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Description });

        //    return list;
        //}

        public static IEnumerable<SelectListItem> GetBgProtectionSubPlans(int bgProtectionPlanId)
        {
            var service = IocManager.Resolve<IBgSubProtectionPlanService>();
            var list = service.GetSubPlans(bgProtectionPlanId).Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Description });

            return list;
        }

        public static VehicleFilterResponseViewModel GetVehicleFilter(VehicleFilterViewModel filter)
        {
            var service = IocManager.Resolve<ICarService>();
            var cacheManager = IocManager.Resolve<ICacheManager>();
            var key = CacheHelper.BuildCacheKey(ConfigurationSettings.CacheKeyVehicleFilter, filter);
            var response = cacheManager.GetOrSet(key, () => service.GetVehicleFilter(filter));
            return response;
        }

        public static IEnumerable<SelectListItem> GetBgProtectionPlans()
        {
            var service = IocManager.Resolve<IBgProtectionPlanService>();
            var response = service.GetPlans();
            return response.Select(x => new SelectListItem { Text = x.Description, Value = x.Id.ToString() });
        }

        public static IEnumerable<SelectListItem> GetBgProtectionSubPlans()
        {
            var service = IocManager.Resolve<IBgSubProtectionPlanService>();
            var list = service.GetSubPlans().Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Description });

            return list;
        }

        public static IEnumerable<SelectListItem> GetMenuLevelsCount()
        {
            var service = IocManager.Resolve<ICompanyService>();
            var maxLevelCount = service.GetSettingAsInteger(CompanySettingsNames.MenuLevels);
            return GetNumericDropdownItems(1, maxLevelCount, 1);
        }

        public static IEnumerable<SelectListItem> GetOpActions()
        {
            var service = IocManager.Resolve<IOemBasicServiceService>();
            var opActions = service.GetOpActions();
            return opActions.Select(x => new SelectListItem { Text = x, Value = x });
        }
    }
}