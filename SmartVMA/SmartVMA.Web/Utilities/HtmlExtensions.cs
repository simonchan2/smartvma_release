﻿using SmartVMA.Infrastructure.Extensions;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SmartVMA.Web.Utilities
{
    public static class HtmlExtensions
    {
        /* Buttons helpers */
        public static MvcHtmlString NavigationButton(this HtmlHelper helper, string action = "Index", string controller = null, object routeValues = null, string text = "", object htmlAttributes = null, string title = "")
        {
            if (string.IsNullOrEmpty(controller))
            {
                controller = helper.ViewContext.RouteData.GetRequiredString("controller");
            }

            var btn = new TagBuilder("a");
            btn.SetInnerText(text);

            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            btn.Attributes.Add("href", urlHelper.Action(action, controller, routeValues));
            btn.Attributes.Add("title", title);

            if (htmlAttributes != null)
            {
                var attributes = htmlAttributes.ToDictionary();
                foreach (var item in attributes)
                {
                    btn.Attributes.Add(item.Key, item.Value.ToString());
                }
            }
            else
            {
                btn.AddCssClass("btn btn-default");
            }
            return new MvcHtmlString(btn.ToString());
        }

        public static MvcHtmlString NavigationButton(this HtmlHelper helper, string url = "#", string text = "", object htmlAttributes = null)
        {
            var btn = new TagBuilder("a");
            btn.SetInnerText(text);
            btn.Attributes.Add("href", url);

            if (htmlAttributes != null)
            {
                var attributes = htmlAttributes.ToDictionary();
                foreach (var item in attributes)
                {
                    btn.Attributes.Add(item.Key, item.Value.ToString());
                }
            }
            else
            {
                btn.AddCssClass("btn btn-default");
            }
            return new MvcHtmlString(btn.ToString());
        }

        public static MvcHtmlString NavigationButtonWithIcon(this HtmlHelper helper, string icon, string action = "Index", string controller = null, object routeValues = null, string text = "", object htmlAttributes = null)
        {
            if (string.IsNullOrEmpty(controller))
            {
                controller = helper.ViewContext.RouteData.GetRequiredString("controller");
            }
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            return NavigationButtonWithIcon(helper, icon, urlHelper.Action(action, controller, routeValues), text, htmlAttributes);
        }

        public static MvcHtmlString NavigationButtonWithIcon(this HtmlHelper helper, string icon, string url = "#", string text = "", object htmlAttributes = null)
        {
            var btn = new TagBuilder("a");
            btn.InnerHtml = string.Format("<span class={0}></span><span class='link-text'> {1}</span>", icon, text);
            btn.Attributes.Add("href", url);

            if (htmlAttributes != null)
            {
                var attributes = htmlAttributes.ToDictionary();
                foreach (var item in attributes)
                {
                    btn.Attributes.Add(item.Key, item.Value.ToString());
                }
            }
            else
            {
                btn.AddCssClass("btn btn-default");
            }
            return new MvcHtmlString(btn.ToString());
        }

        public static MvcHtmlString ModalButton(this HtmlHelper helper, string action = "Edit", string controller = null, object routeValues = null, string text = "", string title = "", object htmlAttributes = null)
        {
            if (string.IsNullOrEmpty(controller))
            {
                controller = helper.ViewContext.RouteData.GetRequiredString("controller");
            }

            var btn = new TagBuilder("a");
            var cssClass = "";
            if (action.ToLower() == "create")
            {
                cssClass = "btn-create";
            }
            else if (action.ToLower() == "delete")
            {
                cssClass = "btn-icon icon-delete-btn";
            }
            else if (action.ToLower() == "edit")
            {
                cssClass = "btn-icon icon-edit-btn";
            }
            btn.AddCssClass(cssClass);

            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            btn.Attributes.Add("href", urlHelper.Action(action, controller, routeValues));
            btn.Attributes.Add("title", title);
            btn.InnerHtml = text;

            if (htmlAttributes != null)
            {
                var attributes = htmlAttributes.ToDictionary();
                foreach (var item in attributes.Where(x => x.Value != null))
                {
                    if (btn.Attributes.ContainsKey(item.Key))
                    {
                        btn.Attributes[item.Key] = item.Value.ToString();
                    }
                    else
                    {
                        btn.Attributes.Add(item.Key, item.Value.ToString());
                    }
                }
            }
            btn.AddCssClass("btn btn-modal");

            return new MvcHtmlString(btn.ToString());
        }

        public static MvcHtmlString ModalButtonWithIcon(this HtmlHelper helper, string icon, string action = "Edit", string controller = null, object routeValues = null, string text = "", string title = "", object htmlAttributes = null)
        {
            if (string.IsNullOrEmpty(controller))
            {
                controller = helper.ViewContext.RouteData.GetRequiredString("controller");
            }

            var btn = new TagBuilder("a");
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            btn.Attributes.Add("href", urlHelper.Action(action, controller, routeValues));
            btn.Attributes.Add("title", title);
            btn.InnerHtml = string.Format("<span class={0}>{1}</span>", icon, text);

            if (htmlAttributes != null)
            {
                var attributes = htmlAttributes.ToDictionary();
                foreach (var item in attributes.Where(x => x.Value != null))
                {
                    if (btn.Attributes.ContainsKey(item.Key))
                    {
                        btn.Attributes[item.Key] = item.Value.ToString();
                    }
                    else
                    {
                        btn.Attributes.Add(item.Key, item.Value.ToString());
                    }
                }
            }
            btn.AddCssClass("btn-modal");

            return new MvcHtmlString(btn.ToString());
        }

        public static MvcHtmlString SubmitButton(this HtmlHelper helper, string text = null, object htmlAttributes = null)
        {
            var btn = new TagBuilder("button");
            if (htmlAttributes != null)
            {
                var attributes = htmlAttributes.ToDictionary();
                foreach (var item in attributes)
                {
                    btn.Attributes.Add(item.Key, item.Value.ToString());
                }
            }
            else
            {
                btn.AddCssClass("btn btn-primary");
            }

            btn.Attributes.Add("type", "submit");
            btn.SetInnerText(text ?? "[Save]");

            return new MvcHtmlString(btn.ToString());
        }

        public static string IsActivePage(this HtmlHelper html, string controller = "", string action = "Index", string cssClass = "active")
        {
            var routeValues = html.ViewContext.RouteData.Values;
            var currentAction = routeValues["action"].ToString();
            var currentController = routeValues["controller"].ToString();
            if (currentController == controller && currentAction == action)
            {
                return cssClass;
            }
            return string.Empty;
        }

        public static string RenderIfTrue(this HtmlHelper html, bool condition, string text, string elseText = "")
        {
            if (condition)
            {
                return text;
            }
            return elseText;
        }

        /* Label helpers */

        public static MvcHtmlString CustomSpanLabel(this HtmlHelper html, string expression, object htmlAttributes = null)
        {
            return LabelHelper(html, ModelMetadata.FromStringExpression(expression, html.ViewData), expression, htmlAttributes, "span");
        }

        public static MvcHtmlString CustomLabel(this HtmlHelper html, string expression, object htmlAttributes = null)
        {
            return LabelHelper(html, ModelMetadata.FromStringExpression(expression, html.ViewData), expression, htmlAttributes);
        }

        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "This is an appropriate nesting of generic types")]
        public static MvcHtmlString CustomLabelFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, object htmlAttributes = null)
        {
            return LabelHelper(html, ModelMetadata.FromLambdaExpression(expression, html.ViewData), ExpressionHelper.GetExpressionText(expression), htmlAttributes);
        }

        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "This is an appropriate nesting of generic types")]
        public static MvcHtmlString CustomSpanLabelFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, object htmlAttributes = null)
        {
            return LabelHelper(html, ModelMetadata.FromLambdaExpression(expression, html.ViewData), ExpressionHelper.GetExpressionText(expression), htmlAttributes, "span");
        }

        internal static MvcHtmlString LabelHelper(HtmlHelper html, ModelMetadata metadata, string htmlFieldName, object htmlAttributes, string elementTag = "label")
        {
            string labelText = metadata.DisplayName ?? metadata.PropertyName ?? htmlFieldName.Split('.').Last();
            if (String.IsNullOrEmpty(labelText))
            {
                return MvcHtmlString.Empty;
            }
            var sb = new StringBuilder(labelText);

            if (!labelText.EndsWith(":"))
            {
                sb.Append(":");
            }

            var tag = new TagBuilder(elementTag);
            if (htmlAttributes != null)
            {
                var attributes = htmlAttributes.ToDictionary();
                foreach (var item in attributes.Where(x => x.Value != null))
                {
                    tag.Attributes.Add(item.Key, item.Value.ToString());
                }
            }

            if (tag.Attributes.ContainsKey("id"))
            {
                tag.Attributes.Add("id", html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(htmlFieldName) + "_Label");
            }
            tag.Attributes.Add("for", html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(htmlFieldName));

            if (metadata.IsRequired)
            {
                sb.Append("<span class=\"required-asterisk\">&nbsp;*</span>") ;
            }
            tag.InnerHtml = sb.ToString();

            return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
        }

        /* Video player */
        public static MvcHtmlString EmbededVideoPlayer(this HtmlHelper html, string url, int width = 560, int height = 315)
        {
            if (url.Contains("youtu"))
            {
                if (url.Contains("youtu.be/"))
                {
                    url = string.Format("https://www.youtube.com/embed/{0}", url.Split('/').Last());
                }

                return new MvcHtmlString(string.Format("<iframe width=\"{0}\" height=\"{1}\" src=\"{2}\" frameborder=\"0\" allowfullscreen></iframe>", width, height, url));
            }
            return new MvcHtmlString("");
        }
    }
}