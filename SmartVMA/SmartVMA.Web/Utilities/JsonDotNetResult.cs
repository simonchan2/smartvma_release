﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SmartVMA.Core.Enums;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Ioc;

namespace SmartVMA.Web.Utilities
{
    public class JsonDotNetResult : JsonResult
    {
        public override void ExecuteResult(ControllerContext context)
        {
            if (JsonRequestBehavior == JsonRequestBehavior.DenyGet &&
                string.Equals(context.HttpContext.Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase))
            {
                throw new InvalidOperationException("GET request not allowed");
            }

            var response = context.HttpContext.Response;
            response.ContentType = !string.IsNullOrEmpty(ContentType) ? ContentType : "application/json";
            if (ContentEncoding != null)
            {
                response.ContentEncoding = ContentEncoding;
            }
            if (Data == null)
            {
                return;
            }

            if((Data as Core.InputModels.ViewModels.AppointmentIndexViewModel) != null)
            {
                response.Write(JsonConvert.SerializeObject(Data));
            }
            else
            {
                var companyService = IocManager.Resolve<ICompanyService>();
                var settings = new JsonSerializerSettings
                {
                    DateFormatString = companyService.GetDateTimeFormat(CompanySettingsNames.DateFormat),
                    ContractResolver = new CamelCaseContractResolver(),
                    Converters = new List<JsonConverter> { new StringEnumConverter() }
                };
                response.Write(JsonConvert.SerializeObject(Data, settings));
            }

        }
    }
}