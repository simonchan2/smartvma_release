﻿using SmartVMA.Core.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace SmartVMA.Web.Utilities
{
    public class CustomModelMetadataProvider : DataAnnotationsModelMetadataProvider
    {
        protected override ModelMetadata CreateMetadata(IEnumerable<Attribute> attributes, Type containerType, Func<object> modelAccessor, Type modelType, string propertyName)
        {
            var attrs = attributes as Attribute[] ?? attributes.ToArray();
            var metadata = base.CreateMetadata(attrs, containerType, modelAccessor, modelType, propertyName);

            var requiredIfSettingAttribute = attrs.OfType<RequiredIfSettingAttribute>().FirstOrDefault();
            if (requiredIfSettingAttribute != null)
            {
                metadata.IsRequired = requiredIfSettingAttribute.IsRequired();
            }
            return metadata;
        }
    }
}