﻿using System;
using System.Globalization;
using System.Web.Mvc;
using SmartVMA.Core.Enums;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;

namespace SmartVMA.Web.Utilities
{
    public class CustomDateTimeModelBinder : IModelBinder
    {
        private readonly IAppContext _appContext;
        private readonly ICompanyService _companyService;

        public CustomDateTimeModelBinder()
        {
            _appContext = IocManager.Resolve<IAppContext>();
            _companyService = IocManager.Resolve<ICompanyService>();
        }

        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            bindingContext.ModelState.SetModelValue(bindingContext.ModelName, value);
            if (string.IsNullOrEmpty(value?.AttemptedValue))
            {
                return null;
            }
            if (!_appContext.IsUserAuthenticated)
            {
                return value;
            }

            DateTime date;
            var dateFormat = _companyService.GetDateTimeFormat(CompanySettingsNames.DateFormat);
            if (value.AttemptedValue.Contains(" "))
            {
                dateFormat += " HH:mm";
            }
            if (DateTime.TryParseExact(value.AttemptedValue, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
            {
                return date;
            }
            return null;
        }
    }
}