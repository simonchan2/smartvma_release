﻿using System;
using System.Collections.Generic;
using System.Linq;
using SmartVMA.Infrastructure.AppContext;
using System.Web.Mvc;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Services;
using SmartVMA.Web.Utilities;
using SmartVMA.Infrastructure.Ioc;

namespace SmartVMA.Web.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class DropdownController : BaseController
    {
        public DropdownController(IAppContext appContext) : base(appContext)
        {
        }

        public ActionResult FilterStates(int countryId)
        {
            var model = DropdownItems.GetStates(countryId: countryId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FilterVehicleMake(string years)
        {
            var result = DropdownItems.GetVehicleMakes(years);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FilterVehicleModel(string years, string makes)
        {
            var result = DropdownItems.GetVehicleModels(years, makes);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FilterVehicleEngine(string years, string makes, string models)
        {
            var result = DropdownItems.GetVehicleEngines(years, makes, models);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FilterVehicleTransmission(string years, string makes, string models, string engines)
        {
            var result = DropdownItems.GetVehicleTransmissions(years, makes, models, engines);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FilterVehicleDriveLine(string years, string makes, string models, string engines, string transmissions)
        {
            var result = DropdownItems.GetVehicleDriveLines(years, makes, models, engines, transmissions);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult RefreshArticleCategory()
        {
            var model = DropdownItems.GetArticleCategories();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FilterDealers(int distributorId)
        {
            var model = DropdownItems.GetDealers(distributorId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult FilterVehiclesSearch(VehicleFilterViewModel model)
        {
            var response = DropdownItems.GetVehicleFilter(model);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetProtectionPlans(string products, int? currentValue)
        {
            if (string.IsNullOrEmpty(products))
            {
                return Json(new { }, JsonRequestBehavior.AllowGet);
            }
            var service = IocManager.Resolve<IBgProtectionPlanService>();

            var productIds = products.Split(',').Where(x => !string.IsNullOrEmpty(x)).Select(int.Parse).ToList();
            var response = service.GetPlans(productIds).Select(x => new SelectListItem() { Value = x.Id.ToString(), Text = x.Description, Selected = x.Id == currentValue });
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetSubProtectionPlans(int protectionPlanId, int? currentValue)
        {
            var service = IocManager.Resolve<IBgSubProtectionPlanService>();
            var response = service.GetSubPlans(protectionPlanId).Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Description, Selected = x.Id == currentValue });
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetBgProductCategories(List<int> bgProducts, int? currentValue)
        {
            var service = IocManager.Resolve<IBgProductService>();
            var response = service.GetAllBgProductCategories(bgProducts)
                .Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Category, Selected = x.Id == currentValue });
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetBgProductsDependentItems(List<int> bgProducts, int rowIndex, int? currentCategoryId, int? currentProtectionPlanId, int? currentVideoId)
        {
            var productCategoryService = IocManager.Resolve<IBgProductService>();
            var bgProtectionPlanService = IocManager.Resolve<IBgProtectionPlanService>();

            var response = new Dictionary<string, IEnumerable<SelectListItem>>
            {
                {
                    $"#items_{rowIndex}_categoryId", productCategoryService.GetAllBgProductCategories(bgProducts)
                        .Select(x => new SelectListItem {Value = x.Id.ToString(), Text = x.Category, Selected = x.Id == currentCategoryId})
                },
                {
                    $"#items_{rowIndex}_bgProtectionPlanId", bgProtectionPlanService.GetPlans(bgProducts)
                        .Select(x => new SelectListItem {Value = x.Id.ToString(), Text = x.Description, Selected = x.Id == currentProtectionPlanId})
                },
                {
                    $"#items_{rowIndex}_videoId", DropdownItems.GetVideos(bgProducts,currentVideoId)
                }
            };

            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}