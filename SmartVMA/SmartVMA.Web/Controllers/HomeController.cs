﻿using System.Web.Mvc;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;

namespace SmartVMA.Web.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class HomeController : BaseController
    {
        public HomeController(IAppContext appContext) : base(appContext)
        {
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PageLoader()
        {
            return View();
        }        
    }
}