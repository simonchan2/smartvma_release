﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using SmartVMA.Core.Contracts;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Web.Utilities;
using Rotativa;
using SmartVMA.Infrastructure.FileManager;
using System.Globalization;
using System.Threading;

namespace SmartVMA.Web.Controllers
{
    public class BaseController : Controller
    {
        protected readonly IAppContext AppContext;

        public BaseController(IAppContext appContext)
        {
            AppContext = appContext;
        }

        /// <summary>
        ///     Maps the errors to modelstate dictionary.
        /// </summary>
        /// <param name="errors">The errors.</param>
        protected void MapErrors(Dictionary<string, string> errors)
        {
            foreach (var item in errors)
            {
                ModelState.AddModelError(item.Key, item.Value);
            }
        }


        protected ServiceResponse GetErrorResponse()
        {
            var errorResponse = new ServiceResponse();
            var errors = ModelState
                .ToDictionary(x => x.Key, x => string.Join(",", x.Value.Errors.Select(y => y.ErrorMessage)))
                .Where(x => !string.IsNullOrEmpty(x.Value));

            foreach (var error in errors)
            {
                var key = error.Key;
                errorResponse.Errors.Add(key, error.Value);
            }

            return errorResponse;
        }

        /// <summary>
        ///     Gets the index view.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model">The model.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <returns></returns>
        protected ActionResult GetIndexView<T>(BaseIndexViewModel<T> model, string viewName = "Index", string noItemsViewName = "NoItems")
        {
            return View(model.Items.Any() ? viewName : noItemsViewName, model);
        }

        /// <summary>
        /// Get View with search error message
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="errorMsg">Error message</param>
        /// <param name="viewName">Name of the view</param>
        /// <returns></returns>
        protected ActionResult GetSearchErrorView(object errorMsg, string viewName = "Index")
        {
            return View(viewName, errorMsg);
        }

        /// <summary>
        ///     Creates a <see cref="T:System.Web.Mvc.JsonResult" /> object that serializes the specified object to JavaScript
        ///     Object Notation (JSON) format using the content type, content encoding, and the JSON request behavior.
        /// </summary>
        /// <param name="data">The JavaScript object graph to serialize.</param>
        /// <param name="contentType">The content type (MIME type).</param>
        /// <param name="contentEncoding">The content encoding.</param>
        /// <param name="behavior">The JSON request behavior</param>
        /// <returns>
        ///     The result object that serializes the specified object to JSON format.
        /// </returns>
        protected override JsonResult Json(object data, string contentType, Encoding contentEncoding,
            JsonRequestBehavior behavior)
        {
            return new JsonDotNetResult
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior
            };
        }

        /// <summary>
        ///     Override of the OnAuthentication method.
        ///     This override contains logic for checking if the current user has granted permission to see
        ///     the requested resource.
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnAuthentication(AuthenticationContext filterContext)
        {
            base.OnAuthentication(filterContext);
            var customAttributes = filterContext.ActionDescriptor.GetCustomAttributes(typeof(AllowAnonymousAttribute), true);
            if (customAttributes.Any())
            {
                return;
            }
            var context = new HttpContextWrapper(filterContext.HttpContext.ApplicationInstance.Context);
            var urlHelper = new UrlHelper(filterContext.RequestContext);
            if (!AppContext.IsUserAuthenticated)
            {
                if (context.Request.IsAjaxRequest())
                {
                    filterContext.HttpContext.Response.StatusCode = 308;
                    filterContext.HttpContext.Response.Clear();
                    filterContext.HttpContext.Response.End();
                }
                else
                {
                    filterContext.Result = new RedirectResult(urlHelper.Action("Login", "Accounts", new { area = "Admin" }));
                }
                return;
            }

            var permissionService = IocManager.Resolve<IPermissionService>();
            var actionName = filterContext.ActionDescriptor.ActionName;
            var controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            if (!permissionService.IsPermissionGrantedForCurrentUser(actionName, controllerName))
            {
                //filterContext.Result = new RedirectResult(urlHelper.Action("NotAuthorizedAction", "Errors", new { area = "" }));
                filterContext.Result = new ViewResult { ViewName = "~/Views/Errors/NotAuthorizedAction.cshtml" };
            }
        }

        [AllowAnonymous]
        public void ExportPdfFile(ViewAsPdf pdf, string filePath, string fileName)
        {
            var appContext = IocManager.Resolve<IAppContext>();
            var pdfFilePath = $@"{appContext.DocumentsFolder}\{filePath}\{fileName}";
            var byteArray = pdf.BuildPdf(ControllerContext);
            var fileManager = IocManager.Resolve<IFileManager>();
            fileManager.Save(pdfFilePath, byteArray, true);
        }
    }
}