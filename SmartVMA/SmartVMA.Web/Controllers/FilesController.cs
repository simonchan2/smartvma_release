﻿using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.FileManager;
using SmartVMA.Infrastructure.Ioc;
using System;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace SmartVMA.Web.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class FilesController : BaseController
    {
        private readonly IFileManager _fileManager;
        public FilesController(IFileManager fileManager, IAppContext appContext) : base(appContext)
        {
            _fileManager = fileManager;
        }

        [HttpPost]
        public ActionResult Upload(Guid? fileGuid, int chunkId)
        {
            if (fileGuid == null)
            {
                fileGuid = Guid.NewGuid();
            }

            chunkId += 1;

            var chunk = Request.Files[0];

            var tempFileName = $"{fileGuid}{Path.GetExtension(chunk.FileName)}";
            var tempFilePath = $"{AppContext.TempFolderName}/{tempFileName}";
            var thumbnail = chunk.ContentType.Contains("image") ? AppContext.MapUrl(tempFilePath) : string.Empty;
            _fileManager.Append(tempFilePath, chunkId, chunk.InputStream);

            return Json(new { fileGuid, chunkId, thumbnail, tempFilePath, fileName = chunk.FileName, tempFileName });
        }

        //public void GetUploadImage(string fileName)
        //{
        //    var appContext = IocManager.Resolve<IAppContext>();
        //    string folderName = appContext.TempFolderName;
        //    string filePath = string.Format("{0}\\{1}", folderName, fileName);
        //    filePath = appContext.MapPath(filePath);

        //    var fileManager = IocManager.Resolve<IFileManager>();
        //    var stream = fileManager.Read(filePath);
        //    var bytes = new byte[stream.Length];
        //    stream.Read(bytes, 0, bytes.Length);

        //    string contentType = MimeMapping.GetMimeMapping(filePath);

        //    Response.Clear();

        //    Response.ContentType = contentType;
        //    Response.AddHeader("Accept-Header", stream.Length.ToString());
        //    //Response.AddHeader("Content-Disposition", String.Format("{0}; filename={1}", "Inline", fileName));
        //    Response.AddHeader("Content-Length", stream.Length.ToString());
        //    Response.BinaryWrite(bytes);

        //    Response.End();
        //    stream.Close();
        //    // return new FileStreamResult(stream, "image/jpeg");
        //}


        [AllowAnonymous]
        public ActionResult Download(string path)
        {
            var fileExtension = Path.GetExtension(path).TrimStart('.');

            //MIME type of jpg file is image/jpeg chrome recognises also image/jpg and shows image, but IE forces user to download it.
            if(fileExtension == "jpg")
            {
                fileExtension = "jpeg";
            }

            if (string.IsNullOrEmpty(fileExtension))
            {
                return null;
            }

            var stream = _fileManager.Read(path);
            if (stream == null)
            {
                return null;
            }
            return new FileStreamResult(stream, fileExtension == "pdf" ? "application/pdf" : $"image/{fileExtension}");

            //TODO -> discuss with Simon Raspberry PI mechanism of downloading files...
            //if (fileExtension == "pdf")
            //{
            //    var bytes = new byte[stream.Length];
            //    stream.Read(bytes, 0, bytes.Length);
            //    var fileName = Path.GetFileName(path);

            //    Response.Clear();

            //    Response.ContentType = "application/pdf";
            //    Response.AppendHeader("Content-Disposition", String.Format("Attachment; filename={0}", fileName));
            //    Response.AppendHeader("Content-Length", stream.Length.ToString());
            //    Response.BinaryWrite(bytes);

            //    Response.End();

            //    return null;
            //}
            //else
            //{
            //    return new FileStreamResult(stream, $"image/{fileExtension}");
            //}
        }
    }
}