﻿using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System.Web.Mvc;

namespace SmartVMA.Web.Controllers
{
    [IocBindable(RegisterAsSelf = true)]
    public class ErrorsController : BaseController
    {
        public ErrorsController(IAppContext appContext) : base(appContext)
        {
        }

        public ActionResult NotAuthorizedAction()
        {
            return View();
        }

        public ActionResult PageNotFound(string aspxerrorpath)
        {
            return View();
        }

        public ActionResult InternalServerError(string aspxerrorpath)
        {
            return View();
        }
    }
}