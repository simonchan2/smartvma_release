﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;

namespace SmartVMA.EfRepository
{
    internal abstract class StoredProceduresRepository
    {
        [Obsolete]
        protected readonly Models _dbContext;

        public StoredProceduresRepository(Models dbContext)
        {
            _dbContext = dbContext;
        }

        protected virtual List<SqlParameter> GetDefaultDBParameters()
        {
            return new List<SqlParameter>();
        }

        /// <summary>
        /// Returns multiple results from a select
        /// </summary>
        /// <param name="storedProcName">Stored procedure name</param>
        /// <param name="parameters">All input paramters</param>
        /// <param name="outputParameter1">Optional oputput parameter, output parameters are added at the end in the parameters list</param>
        /// <param name="outputParameter2">Optional oputput parameter, output parameters are added at the end in the parameters list</param>
        /// <param name="outputParameter3">Optional oputput parameter, output parameters are added at the end in the parameters list</param>
        /// <returns>DataSet with tables with inofrmation from the select</returns>
        public DataSet SPExecuteDataSet(string storedProcName, List<SqlParameter> parameters = null, SqlParameter outputParameter = null, DbTransaction tran = null, params string[] tableNames)
        {
            var resDbSet = new DataSet();
            var db = this._dbContext;

            // Create a SQL command to execute the sproc 
            var cmd = db.Database.Connection.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = storedProcName;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }

            var defaultParameters = GetDefaultDBParameters();
            if ((defaultParameters != null) && (defaultParameters.Count > 0))
            {
                cmd.Parameters.AddRange(defaultParameters.ToArray());
            }
            if ((parameters != null) && (parameters.Count > 0))
            {
                cmd.Parameters.AddRange(parameters.ToArray());
            }
            if (outputParameter != null)
            {
                outputParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputParameter);
            }

            bool closeConn = true;
            try
            {
                //Check if connection is oppened at the time when we call thwe stored procedure. This can happen if someone is calling the porcedure in a transaction.
                //In this case we don't open a new connection and don't close this one, we just use it and it will be closed outside this method.
                if (db.Database.Connection.State != ConnectionState.Open)
                {
                    db.Database.Connection.Open();
                }
                else
                {
                    closeConn = false;
                }

                // Run the sproc                    
                var reader = cmd.ExecuteReader();
                resDbSet.Load(reader, LoadOption.OverwriteChanges, tableNames);
             }
            finally
            {
                if (closeConn)
                {
                    db.Database.Connection.Close();
                }
            }

            return resDbSet;
        }

        /// <summary>
        /// Could be used to return list with a stored procedure
        /// </summary>
        /// <param name="storedProcName">Stored procedure name</param>
        /// <param name="parameters">All input paramters</param>
        /// <param name="outputParameter1">Optional oputput parameter</param>
        /// <param name="outputParameter2">Optional oputput parameter</param>
        /// <param name="outputParameter3">Optional oputput parameter</param>
        /// <returns>DataTable with information from the select</returns>
        public DataTable SPExecuteReader(string storedProcName, List<SqlParameter> parameters = null, SqlParameter outputParameter1 = null, SqlParameter outputParameter2 = null, SqlParameter outputParameter3 = null, DbTransaction tran = null)
        {
            DataTable resList = new DataTable();
            var db = this._dbContext;

            // Create a SQL command to execute the sproc 
            var cmd = db.Database.Connection.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = storedProcName;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }

            var defaultParameters = GetDefaultDBParameters();
            if ((defaultParameters != null) && (defaultParameters.Count > 0))
            {
                cmd.Parameters.AddRange(defaultParameters.ToArray());
            }
            if ((parameters != null) && (parameters.Count > 0))
            {
                cmd.Parameters.AddRange(parameters.ToArray());
            }

            if (outputParameter1 != null)
            {
                outputParameter1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputParameter1);
            }

            if (outputParameter2 != null)
            {
                outputParameter2.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputParameter2);
            }

            if (outputParameter3 != null)
            {
                outputParameter3.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputParameter3);
            }
            bool closeConn = true;
            try
            {
                //Check if connection is oppened at the time when we call thwe stored procedure. This can happen if someone is calling the porcedure in a transaction.
                //In this case we don't open a new connection and don't close this one, we just use it and it will be closed outside this method.
                if (db.Database.Connection.State != ConnectionState.Open)
                {
                    db.Database.Connection.Open();
                }
                else
                {
                    closeConn = false;
                }

                // Run the sproc  
                var reader = cmd.ExecuteReader();
                resList.Load(reader, LoadOption.OverwriteChanges);
            }
            finally
            {
                if (closeConn)
                {
                    db.Database.Connection.Close();
                }
            }

            return resList;
        }

        /// <summary>
        /// Could be used to return list with a stored procedure
        /// </summary>
        /// <param name="storedProcName">Stored procedure name</param>
        /// <param name="parameters">All input paramters</param>
        /// <param name="outputParameter1">Optional oputput parameter</param>
        /// <param name="outputParameter2">Optional oputput parameter</param>
        /// <param name="outputParameter3">Optional oputput parameter</param>
        /// <returns>List from type T2 with information from the select. 
        /// EF does not take any mapping into account when it creates entities using the Translate method. It will simply match column names in the result set with property names on your classes.
        /// </returns>
        public List<T2> SPExecuteReader<T2>(string storedProcName, List<SqlParameter> parameters = null, SqlParameter outputParameter1 = null, SqlParameter outputParameter2 = null, SqlParameter outputParameter3 = null, DbTransaction tran = null) where T2 : class, new()
        {
            List<T2> resList = new List<T2>();
            var db = this._dbContext;

            // Create a SQL command to execute the sproc 
            var cmd = db.Database.Connection.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = storedProcName;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }

            var defaultParameters = GetDefaultDBParameters();
            if ((defaultParameters != null) && (defaultParameters.Count > 0))
            {
                cmd.Parameters.AddRange(defaultParameters.ToArray());
            }
            if ((parameters != null) && (parameters.Count > 0))
            {
                cmd.Parameters.AddRange(parameters.ToArray());
            }

            if (outputParameter1 != null)
            {
                outputParameter1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputParameter1);
            }

            if (outputParameter2 != null)
            {
                outputParameter2.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputParameter2);
            }

            if (outputParameter3 != null)
            {
                outputParameter3.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputParameter3);
            }

            bool closeConn = true;
            try
            {
                //Check if connection is oppened at the time when we call thwe stored procedure. This can happen if someone is calling the porcedure in a transaction.
                //In this case we don't open a new connection and don't close this one, we just use it and it will be closed outside this method.
                if (db.Database.Connection.State != ConnectionState.Open)
                {
                    db.Database.Connection.Open();
                }
                else
                {
                    closeConn = false;
                }
                // Run the sproc  
                var reader = cmd.ExecuteReader();
                // Read Blogs from the first result set 
                var resultList = ((IObjectContextAdapter)db).ObjectContext.Translate<T2>(reader);
                resList = resultList.ToList<T2>();
            }
            finally
            {
                if (closeConn)
                {
                    db.Database.Connection.Close();
                }
            }

            return resList;
        }

        /// <summary>
        /// Could be used for Add, Update, Delete operations with a stored procedure
        /// </summary>
        /// <param name="storedProcName">Stored procedure name</param>
        /// <param name="parameters">All input paramters</param>
        /// <param name="outputParameter1">Optional oputput parameter</param>
        /// <param name="outputParameter2">Optional oputput parameter</param>
        /// <param name="outputParameter3">Optional oputput parameter</param>
        /// <returns>The number of updated/deleted records, if no such number returns -1</returns>
        public int SPExecuteNonQuery(string storedProcName, List<SqlParameter> parameters = null, SqlParameter outputParameter1 = null, SqlParameter outputParameter2 = null, SqlParameter outputParameter3 = null, DbTransaction tran = null)
        {
            int resVal = 0;

            var db = this._dbContext;

            // Create a SQL command to execute the sproc 
            var cmd = db.Database.Connection.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = storedProcName;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }

            var defaultParameters = GetDefaultDBParameters();
            if ((defaultParameters != null) && (defaultParameters.Count > 0))
            {
                cmd.Parameters.AddRange(defaultParameters.ToArray());
            }
            if ((parameters != null) && (parameters.Count > 0))
            {
                cmd.Parameters.AddRange(parameters.ToArray());
            }

            if (outputParameter1 != null)
            {
                outputParameter1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputParameter1);
            }

            if (outputParameter2 != null)
            {
                outputParameter2.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputParameter2);
            }

            if (outputParameter3 != null)
            {
                outputParameter3.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputParameter3);
            }

            bool closeConn = true;
            try
            {
                //Check if connection is oppened at the time when we call thwe stored procedure. This can happen if someone is calling the porcedure in a transaction.
                //In this case we don't open a new connection and don't close this one, we just use it and it will be closed outside this method.
                if (db.Database.Connection.State != ConnectionState.Open)
                {
                    db.Database.Connection.Open();
                }
                else
                {
                    closeConn = false;
                }
                // Run the sproc  
                resVal = cmd.ExecuteNonQuery();
            }
            finally
            {
                if (closeConn)
                {
                    db.Database.Connection.Close();
                }
            }

            return resVal;
        }

        /// <summary>
        /// Could be used for scalar operations with a stored procedure
        /// </summary>
        /// <param name="storedProcName">Stored procedure name</param>
        /// <param name="parameters">All input paramters</param>
        /// <param name="outputParameter1">Optional oputput parameter</param>
        /// <param name="outputParameter2">Optional oputput parameter</param>
        /// <param name="outputParameter3">Optional oputput parameter</param>
        /// <returns>Returns the first column value from the first select, if no such value null pointer exception is raised</returns>
        public object SPExecuteScalar(string storedProcName, List<SqlParameter> parameters = null, SqlParameter outputParameter1 = null, SqlParameter outputParameter2 = null, SqlParameter outputParameter3 = null, DbTransaction tran = null)
        {
            object resVal = DBNull.Value;

            var db = this._dbContext;

            // Create a SQL command to execute the sproc 
            var cmd = db.Database.Connection.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = storedProcName;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }

            var defaultParameters = GetDefaultDBParameters();
            if ((defaultParameters != null) && (defaultParameters.Count > 0))
            {
                cmd.Parameters.AddRange(defaultParameters.ToArray());
            }
            if ((parameters != null) && (parameters.Count > 0))
            {
                cmd.Parameters.AddRange(parameters.ToArray());
            }

            if (outputParameter1 != null)
            {
                outputParameter1.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputParameter1);
            }

            if (outputParameter2 != null)
            {
                outputParameter2.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputParameter2);
            }

            if (outputParameter3 != null)
            {
                outputParameter3.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputParameter3);
            }

            bool closeConn = true;
            try
            {
                //Check if connection is oppened at the time when we call thwe stored procedure. This can happen if someone is calling the porcedure in a transaction.
                //In this case we don't open a new connection and don't close this one, we just use it and it will be closed outside this method.
                if (db.Database.Connection.State != ConnectionState.Open)
                {
                    db.Database.Connection.Open();
                }
                else
                {
                    closeConn = false;
                }
                // Run the sproc  
                resVal = cmd.ExecuteScalar();
            }
            finally
            {
                if (closeConn)
                {
                    db.Database.Connection.Close();
                }
            }

            return resVal;
        }

        #region DS Readers to Multiple Types
        public Tuple<List<T1>, List<T2>> SPExecureDSReader<T1, T2>(string storedProcName, List<SqlParameter> parameters = null, SqlParameter outputParameter = null, DbTransaction tran = null) where T1 : class, new() where T2 : class, new()
        {
            List<T1> t1 = new List<T1>();
            List<T2> t2 = new List<T2>();
            var db = this._dbContext;

            // Create a SQL command to execute the sproc 
            var cmd = db.Database.Connection.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = storedProcName;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }

            var defaultParameters = GetDefaultDBParameters();
            if ((defaultParameters != null) && (defaultParameters.Count > 0))
            {
                cmd.Parameters.AddRange(defaultParameters.ToArray());
            }
            if ((parameters != null) && (parameters.Count > 0))
            {
                cmd.Parameters.AddRange(parameters.ToArray());
            }
            if (outputParameter != null)
            {
                outputParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputParameter);
            }

            bool closeConn = true;
            try
            {
                //Check if connection is oppened at the time when we call thwe stored procedure. This can happen if someone is calling the porcedure in a transaction.
                //In this case we don't open a new connection and don't close this one, we just use it and it will be closed outside this method.
                if (db.Database.Connection.State != ConnectionState.Open)
                {
                    db.Database.Connection.Open();
                }
                else
                {
                    closeConn = false;
                }

                // Run the sproc                    
                var reader = cmd.ExecuteReader();

                t1 = ((IObjectContextAdapter)db).ObjectContext.Translate<T1>(reader).ToList<T1>();
                reader.NextResult();

                t2 = ((IObjectContextAdapter)db).ObjectContext.Translate<T2>(reader).ToList<T2>();
                reader.NextResult();
            }
            finally
            {
                if (closeConn)
                {
                    db.Database.Connection.Close();
                }
            }

            return new Tuple<List<T1>, List<T2>>(t1, t2);
        }

        public Tuple<List<T1>, List<T2>, List<T3>> SPExecureDSReader<T1, T2, T3>(string storedProcName, List<SqlParameter> parameters = null, SqlParameter outputParameter = null, DbTransaction tran = null) where T1 : class, new() where T2 : class, new() where T3 : class, new()
        {
            List<T1> t1 = new List<T1>();
            List<T2> t2 = new List<T2>();
            List<T3> t3 = new List<T3>();

            var db = this._dbContext;

            // Create a SQL command to execute the sproc 
            var cmd = db.Database.Connection.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = storedProcName;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }

            var defaultParameters = GetDefaultDBParameters();
            if ((defaultParameters != null) && (defaultParameters.Count > 0))
            {
                cmd.Parameters.AddRange(defaultParameters.ToArray());
            }
            if ((parameters != null) && (parameters.Count > 0))
            {
                cmd.Parameters.AddRange(parameters.ToArray());
            }
            if (outputParameter != null)
            {
                outputParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputParameter);
            }

            bool closeConn = true;
            try
            {
                //Check if connection is oppened at the time when we call thwe stored procedure. This can happen if someone is calling the porcedure in a transaction.
                //In this case we don't open a new connection and don't close this one, we just use it and it will be closed outside this method.
                if (db.Database.Connection.State != ConnectionState.Open)
                {
                    db.Database.Connection.Open();
                }
                else
                {
                    closeConn = false;
                }

                // Run the sproc                    
                var reader = cmd.ExecuteReader();

                t1 = ((IObjectContextAdapter)db).ObjectContext.Translate<T1>(reader).ToList<T1>();
                reader.NextResult();

                t2 = ((IObjectContextAdapter)db).ObjectContext.Translate<T2>(reader).ToList<T2>();
                reader.NextResult();

                t3 = ((IObjectContextAdapter)db).ObjectContext.Translate<T3>(reader).ToList<T3>();
            }
            finally
            {
                if (closeConn)
                {
                    db.Database.Connection.Close();
                }
            }

            return new Tuple<List<T1>, List<T2>, List<T3>>(t1, t2, t3);
        }

        public Tuple<List<T1>, List<T2>, List<T3>, List<T4>> SPExecureDSReader<T1, T2, T3, T4>(string storedProcName, List<SqlParameter> parameters = null, SqlParameter outputParameter = null, DbTransaction tran = null) where T1 : class, new() where T2 : class, new() where T3 : class, new() where T4 : class, new()
        {
            List<T1> t1 = new List<T1>();
            List<T2> t2 = new List<T2>();
            List<T3> t3 = new List<T3>();
            List<T4> t4 = new List<T4>();

            var db = this._dbContext;

            // Create a SQL command to execute the sproc 
            var cmd = db.Database.Connection.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = storedProcName;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }

            var defaultParameters = GetDefaultDBParameters();
            if ((defaultParameters != null) && (defaultParameters.Count > 0))
            {
                cmd.Parameters.AddRange(defaultParameters.ToArray());
            }
            if ((parameters != null) && (parameters.Count > 0))
            {
                cmd.Parameters.AddRange(parameters.ToArray());
            }
            if (outputParameter != null)
            {
                outputParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputParameter);
            }

            bool closeConn = true;
            try
            {
                //Check if connection is oppened at the time when we call thwe stored procedure. This can happen if someone is calling the porcedure in a transaction.
                //In this case we don't open a new connection and don't close this one, we just use it and it will be closed outside this method.
                if (db.Database.Connection.State != ConnectionState.Open)
                {
                    db.Database.Connection.Open();
                }
                else
                {
                    closeConn = false;
                }

                // Run the sproc                    
                var reader = cmd.ExecuteReader();

                t1 = ((IObjectContextAdapter)db).ObjectContext.Translate<T1>(reader).ToList<T1>();
                reader.NextResult();

                t2 = ((IObjectContextAdapter)db).ObjectContext.Translate<T2>(reader).ToList<T2>();
                reader.NextResult();

                t3 = ((IObjectContextAdapter)db).ObjectContext.Translate<T3>(reader).ToList<T3>();
                reader.NextResult();

                t4 = ((IObjectContextAdapter)db).ObjectContext.Translate<T4>(reader).ToList<T4>();
            }
            finally
            {
                if (closeConn)
                {
                    db.Database.Connection.Close();
                }
            }

            return new Tuple<List<T1>, List<T2>, List<T3>, List<T4>>(t1, t2, t3, t4);
        }

        public Tuple<List<T1>, List<T2>, List<T3>, List<T4>, List<T5>> SPExecureDSReader<T1, T2, T3, T4, T5>(string storedProcName, List<SqlParameter> parameters = null, SqlParameter outputParameter = null, DbTransaction tran = null) where T1 : class, new() where T2 : class, new() where T3 : class, new() where T4 : class, new() where T5 : class, new()
        {
            List<T1> t1 = new List<T1>();
            List<T2> t2 = new List<T2>();
            List<T3> t3 = new List<T3>();
            List<T4> t4 = new List<T4>();
            List<T5> t5 = new List<T5>();

            var db = this._dbContext;

            // Create a SQL command to execute the sproc 
            var cmd = db.Database.Connection.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = storedProcName;
            if (tran != null)
            {
                cmd.Transaction = tran;
            }

            var defaultParameters = GetDefaultDBParameters();
            if ((defaultParameters != null) && (defaultParameters.Count > 0))
            {
                cmd.Parameters.AddRange(defaultParameters.ToArray());
            }
            if ((parameters != null) && (parameters.Count > 0))
            {
                cmd.Parameters.AddRange(parameters.ToArray());
            }
            if (outputParameter != null)
            {
                outputParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(outputParameter);
            }

            bool closeConn = true;
            try
            {
                //Check if connection is oppened at the time when we call thwe stored procedure. This can happen if someone is calling the porcedure in a transaction.
                //In this case we don't open a new connection and don't close this one, we just use it and it will be closed outside this method.
                if (db.Database.Connection.State != ConnectionState.Open)
                {
                    db.Database.Connection.Open();
                }
                else
                {
                    closeConn = false;
                }

                // Run the sproc                    
                var reader = cmd.ExecuteReader();

                t1 = ((IObjectContextAdapter)db).ObjectContext.Translate<T1>(reader).ToList<T1>();
                reader.NextResult();

                t2 = ((IObjectContextAdapter)db).ObjectContext.Translate<T2>(reader).ToList<T2>();
                reader.NextResult();

                t3 = ((IObjectContextAdapter)db).ObjectContext.Translate<T3>(reader).ToList<T3>();
                reader.NextResult();

                t4 = ((IObjectContextAdapter)db).ObjectContext.Translate<T4>(reader).ToList<T4>();
                reader.NextResult();

                t5 = ((IObjectContextAdapter)db).ObjectContext.Translate<T5>(reader).ToList<T5>();
            }
            finally
            {
                if (closeConn)
                {
                    db.Database.Connection.Close();
                }
            }

            return new Tuple<List<T1>, List<T2>, List<T3>, List<T4>, List<T5>>(t1, t2, t3, t4, t5);
        }

        #endregion DS Readers to Multiple Types
        public SqlParameter SqlParameter(string parameterName, object value)
        {
            SqlParameter parameter;
            if (value == null)
            {
                parameter = new SqlParameter(parameterName, DBNull.Value);
                //parameter.SqlDbType = type;
            }
            else
            {
                parameter = new SqlParameter(parameterName, value);
            }
            return parameter;
        }

        public SqlParameter SqlParameter(string parameterName, object value, SqlDbType sqlType)
        {
            SqlParameter parameter;
            if (value == null)
            {
                parameter = new SqlParameter(parameterName, DBNull.Value);
                parameter.SqlDbType = sqlType;
            }
            else
            {
                parameter = new SqlParameter(parameterName, value);
            }
            return parameter;
        }

        //public SqlParameter SqlParameter<T>(string parameterName, T value)
        //{
        //    SqlParameter parameter;
        //    if (value == null)
        //    {
        //        parameter = new SqlParameter(parameterName, DBNull.Value);
        //        parameter.SqlDbType = ConvertToSqlDbType(typeof(T));
        //    }
        //    else
        //    {
        //        parameter = new SqlParameter(parameterName, value);
        //    }
        //    return parameter;
        //}
        //public static SqlDbType ConvertToSqlDbType(Type giveType)
        //{
        //    var typeMap = new Dictionary<Type, SqlDbType>();
        //    typeMap[typeof(string)] = SqlDbType.NVarChar;
        //    typeMap[typeof(char[])] = SqlDbType.NVarChar;
        //    typeMap[typeof(int)] = SqlDbType.Int;
        //    typeMap[typeof(Int32)] = SqlDbType.Int;
        //    typeMap[typeof(Int16)] = SqlDbType.SmallInt;
        //    typeMap[typeof(Int64)] = SqlDbType.BigInt;
        //    typeMap[typeof(Byte[])] = SqlDbType.VarBinary;
        //    typeMap[typeof(Boolean)] = SqlDbType.Bit;
        //    typeMap[typeof(DateTime)] = SqlDbType.DateTime2;
        //    typeMap[typeof(DateTimeOffset)] = SqlDbType.DateTimeOffset;
        //    typeMap[typeof(Decimal)] = SqlDbType.Decimal;
        //    typeMap[typeof(Double)] = SqlDbType.Float;
        //    typeMap[typeof(Decimal)] = SqlDbType.Money;
        //    typeMap[typeof(Byte)] = SqlDbType.TinyInt;
        //    typeMap[typeof(TimeSpan)] = SqlDbType.Time;
        //    return typeMap[giveType];
        //}

    }
}
