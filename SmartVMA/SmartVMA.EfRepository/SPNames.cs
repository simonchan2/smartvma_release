﻿using SmartVMA.Core;

namespace SmartVMA.EfRepository
{
    public class SPNames
    {
        public const string InspectionAddSet = "vi.InspectionAddSet";
        public const string InspectionReset = "vi.InspectionReset";
        public const string VehiclePhotoDelete = "vi.VehiclePhotoDelete";
        public const string InspectionGet = "vi.InspectionGet";

        public const string SetAppointmentToInspection = "dbo.SetAppointmentToInspection";

        public const string GetCustomerInfo = "dbo.GetCustomerInfo";

        public static string GetAllServices = "dbo.GetAllServices";

        public const string AddPdfDocumentToPrintQueue = "rprint.AddPdfDocumentToPrintQueue";

        public const string OverrideRuleCheckConflicts = "dbo.OverrideRuleCheckConflicts";
        public const string OverrideRuleApply = "dbo.OverrideRuleApply";
        public const string OverrideRuleGetById = "dbo.OverrideRuleGetById";
        public const string OverrideRuleSearch = "dbo.OverrideRuleSearch";
        public const string OverrideRulesDelete = "dbo.OverrideRulesDelete";

        public const string GetAdditionalServicesForMaintenance = "dbo.GetAdditionalservicesForMaintenance";
        public const string SaveAdditionalServicesCarMapping = "dbo.SaveAdditionalServicesCarMapping";

        public const string GetMileagesByCarId = "dbo.GetMileagesByCarId";

        public const string GetOemBasicServicesForMaintenance = "dbo.GetOemBasicServicesForMaintenance";

        public const string GetVehicleFilters = "dbo.GetVehicleFilters";

        public const string GetVehicleFiltersByVIN = "dbo.GetVehicleFiltersByVIN";

        public const string GetBgProductsProtectionPlansPage = "dbo.GetBgProductsProtectionPlansPage";

        public const string GetBgProductsProtectionPlansBySKU = "dbo.GetBgProductsProtectionPlansBySKU";
        public const string GetVehicleTransmissions = "dbo.GetVehicleTransmissions";
        public const string GetVehicleTransmissionsFromVinMaster = "dbo.GetVehicleTransmissionsFromVinMaster";

        public const string GetVehicleDriveLines = "dbo.GetVehicleDriveLines";
        public const string GetVehicleDriveLinesFromVinMaster = "dbo.GetVehicleDriveLinesFromVinMaster";

        public const string SaveOemBasicService = "dbo.SaveOemBasicService";

        public const string GetVehicleInvoiceHistory = "dbo.GetVehicleInvoiceHistory";
        public const string DeleteOemBasicServices = "dbo.DeleteOemBasicServices";
        public const string DeleteAdditionalServices = "dbo.DeleteAdditionalServices";
        public const string GetOverridenPartNumber = "dbo.GetOverridenPartNumber";
    }
}