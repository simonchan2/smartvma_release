USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetBgProductsProtectionPlansBySKU]    Script Date: 9/23/2016 9:38:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[GetBgProductsProtectionPlansBySKU]
		@ProductId INT
AS
BEGIN
		DECLARE @TABLE TABLE
		(  
		ID INT,
		ProductId INT,  
		BGSKU VARCHAR(50),  
		ProductDescription NVARCHAR(255),  
		ProdCategoryId INT, 
		ProtectionPlanId NVARCHAR(50)
		)

		INSERT INTO @TABLE  
		SELECT  
			ROW_NUMBER()	over (order BY	bgp.[BgProductId])	AS ID,
			bgp.[BgProductId]						AS ProductId,
			bgp.[PartNumber]						AS BGSKU, 
			bgp.[Description]						AS ProductDescription,
			cat.[BgProdCategoryId]					AS ProdCategoryId,
			bgpp.[BgProtectionPlanId]				AS ProtectionPlanId				
		FROM [bg].[BgProducts] bgp
			INNER JOIN [bg].[BgProductsCategories] bgpc ON bgpc.[BgProductId] = bgp.[BgProductId]
			INNER JOIN [bg].[BgProdCategories] cat ON cat.[BgProdCategoryId] = bgpc.[BgProdCategoryId]	
			LEFT JOIN bg.BgProductsProtectionPlans bgppp ON bgppp.BgProductId = bgp.BgProductId
			LEFT JOIN bg.BgProtectionPlans bgpp On bgpp.BgProtectionPlanId = bgppp.BgProtectionPlanId	
		WHERE  bgp.[BgProductId] = @ProductId

		SELECT T1.ProductId AS BgProductId, MAX(T1.BGSKU) AS BGSKU, MAX(T1.ProductDescription) AS ProductDescription,
			
 				STUFF((
								SELECT ',' + CAST(T2.ProtectionPlanId AS NVARCHAR(50))
								FROM @TABLE T2
								WHERE T2.ProductId = T1.ProductId
								FOR XML PATH('')
								), 1, 1, '') AS BgProductProtectionPlanIds
								,
					STUFF((
					SELECT ',' + CAST(T2.ProdCategoryId AS nvarchar(50))
					FROM @TABLE T2
					WHERE T2.ProductId = T1.ProductId
					FOR XML PATH('')
					), 1, 1, '') AS BgProductCategoryIds
		FROM @TABLE T1 
		GROUP BY ProductId
END;

