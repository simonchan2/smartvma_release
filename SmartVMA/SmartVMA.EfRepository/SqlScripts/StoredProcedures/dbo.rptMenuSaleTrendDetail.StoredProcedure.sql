USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[rptMenuSaleTrendDetail]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[rptMenuSaleTrendDetail]
@DealerCompanyId int,
@Advisor	nvarchar(45),
@StartDate  DATETIME,
@EndDate    DATETIME,
@CPOnly				bit=0,
@UserOnly			bit=0,
@ExclLabor0			bit=0
AS 
BEGIN
WITH CTE AS
(
    SELECT @StartDate AS monthYear
    UNION ALL
    SELECT DATEADD(MONTH, 1, monthYear)
    FROM CTE
    WHERE DATEADD(MONTH, 1, monthYear) <= @EndDate   
)
SELECT 
	monthYear DateRange, 
	0 ROs,
	0 Pres,
	0 MenusSold,
	0 Pen,
	0 Menu$,
	0 Qty,
	0 Amount,
	0 PerMenu$
into #temp
FROM CTE
option (maxrecursion 1000);


create table #tempMenLevel
(    
    MenuLevel int
)
insert into  #tempMenLevel
select 1;
insert into  #tempMenLevel
select 2;
insert into  #tempMenLevel
select 3;
  
select * 
into #tempDateRangeSet
from #temp tt
cross join  #tempMenLevel l

select distinct 	   
       dmsu.[DmsUserNumber] AdvisorCode,
       dmsu.[DmsUserName] AdvisorName, 
	   i.InvoiceNumber,
	   (case when PresentationStatus='MC'  then 1 else 0 end) MenuSold,
       (MenuLevel) [MenuLevel],                        
	   [AppointmentTime],	   
	   isnull(p.PartQty,0) PartQty,
	   isnull(p.PartSale,0) PartSale,
	   isnull(d.LaborSale,0) LaborSale	 
into #Data
from [dealer].[DealerDmsUsers] dmsu
join [dealer].[AppointmentPresentations] ap on dmsu.UserId  = ap.[AdvisorId] 
join [dealer].[DealerVehicles] dv on dv.[DealerVehicleId] = ap.DealerVehicleId
left join dealer.Invoices i on i.InvoiceNumber = ap.InvoiceNumber
left join [dealer].[InvoiceDetails] d (nolock)on d.InvoiceId = i.InvoiceId
left join [dealer].[InvoiceDetailParts] p (nolock)on p.InvoiceDetailId = d.InvoiceDetailId
where	[AppointmentTime] between @StartDate and @EndDate and
		((@Advisor='Consolidated' or @Advisor='AllAdvisors') or (@Advisor!='Consolidated' and @Advisor!='AllAdvisors' and dmsu.DmsUserNumber =@Advisor)) and
		(@ExclLabor0 = 0 or (@ExclLabor0 = 1 and d.LaborHour = 0)) and
		(@UserOnly = 0 or (@UserOnly = 1 and dmsu.DmsUserName is not null)) and
		(@CPOnly = 0 or (@CPOnly=1 and d.[LaborType] = 'CP'))		
		and (@DealerCompanyId=0 or (@DealerCompanyId<>0 and ap.DealerId = @DealerCompanyId))

--select* from #Data
select 
	AdvisorCode,
	AdvisorName,
	InvoiceNumber,
	AppointmentTime,	
	MenuSold,	
	sum(PartQty) PartQty,
	sum(PartSale) PartSale,
	sum(LaborSale) LaborSale,
	[MenuLevel] 
 into #Data2
from #Data
group by AdvisorCode, AdvisorName,InvoiceNumber,AppointmentTime, MenuSold, [MenuLevel]

--select * from #Data2

	declare @InvoiceNumber int
	declare @AdvisorCode nvarchar(45)
	declare @AdvisorName nvarchar(45)
	declare @MenuSold int
	declare @menulevel int		
	declare @apptdate datetime
	declare @PartQty float
	declare @PartSale float
	declare @LaborSale float
	   	
	declare CustList cursor for
	select * from #Data2
	OPEN CustList
	FETCH NEXT FROM CustList 
	INTO @AdvisorName, @AdvisorName, @InvoiceNumber ,@apptdate, @MenuSold, @PartQty, @PartSale, @LaborSale,@menulevel
	
	WHILE @@FETCH_STATUS = 0
	BEGIN		
		  begin	
	  		update t2
				set 
					t2.ROs  = (t2.Ros + (case when @InvoiceNumber is null  then 0 else 1 end)),
					t2.MenusSold = (t2.MenusSold +	@MenuSold),
					t2.Menu$ = t2.Menu$ + (@PartSale + @LaborSale),
					t2.Qty = t2.Qty + @PartQty,
					t2.Amount = t2.Amount + (@PartSale + @LaborSale)
			from #tempDateRangeSet t2
			where
				t2.MenuLevel =  @menulevel and
				Month(t2.DateRange) = Month(@apptdate) and
				Year(t2.DateRange) = Year(@apptdate) 		
		  end
	  
	  FETCH NEXT FROM CustList 
	    INTO @AdvisorName, @AdvisorName,@InvoiceNumber, @apptdate, @MenuSold, @PartQty, @PartSale, @LaborSale,@menulevel
	END
	CLOSE CustList
	DEALLOCATE CustList

	--select * from #tempDateRangeSet order by DateRange


	--select DateRange, sum(ROs) Ros, sum(Pres) Pres, sum(MenusSold) MenusSold, sum(Pen) Pen,
	--sum(Menu$) Menu$, sum(Qty) Qty, sum(Amount) Amount
	--from #tempDateRangeSet 
	--group by DateRange



select * 
into #tempDateRangeSet2
from
( 
	select *, 'a'+ cast([MenuLevel] as varchar) aMenuLevel, 	
	'm'+ cast([MenuLevel] as varchar) mMenuLevel
	from #tempDateRangeSet
) s
pivot
( sum(Qty)
		for [MenuLevel] in ([1], [2], [3])
) as p1
pivot
( sum(Amount)
		for [aMenuLevel] in ([a1], [a2], [a3])
) as p2
pivot
( sum(Permenu$)
		for mMenuLevel in ([m1], [m2], [m3])
) as p2

update #tempDateRangeSet2
set [1] = isnull([1],0),[2] = isnull([2],0), [3] = isnull([3],0),
	[a1] = isnull([a1],0),[a2] = isnull([a2],0),[a3] = isnull([a3],0)
,[m1] = isnull([m1],0),[m2] = isnull([m2],0),[m3] = isnull([m3],0)



select
LEFT(DATENAME(MONTH,DateRange),3) +  ' ' +  CAST ( YEAR(DateRange)AS varchar(100) )  [RangeLabel], 
sum(ROs) Ros,
sum(Pres) Pres,
sum(MenusSold) MenusSold,
sum(Pen) Pen,
sum(Menu$) Menu$,
sum([1]) [1],
sum([2]) [2],
sum([3]) [3],
sum([a1]) [d1],
sum([a2]) [d2],
sum([a3]) [d3],
sum([m1]) [m1],
sum([m2]) [m2],
sum([m3]) [m3],
DateRange
from #tempDateRangeSet2
group by DateRange
union
select
'Total', 
sum(ROs) Ros,
sum(Pres) Pres,
sum(MenusSold) MenusSold,
sum(Pen) Pen,
sum(Menu$) Menu$,
sum([1]) [1],
sum([2]) [2],
sum([3]) [3],
sum([a1]) [d1],
sum([a2]) [d2],
sum([a3]) [d3],
sum([m1]) [m1],
sum([m2]) [m2],
sum([m3]) [m3],
'9999-01-01' DateRange
from #tempDateRangeSet2
order by DateRange

END



GO
