USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[rptGetContents]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[rptGetContents]	
	@Option int=0
AS 
BEGIN

select *
into #temp1
from 
(
SELECT 'Qty' conentLabel, 'Qty' conentValue, 1 sort
UNION
SELECT 'Labor$' conentLabel, 'Labor$' conentValue, 2 sort
UNION
SELECT 'Part$' conentLabel, 'Part$' conentValue, 3 sort
UNION
SELECT 'Labor & Parts $' conentLabel, 'LaborParts$' conentValue, 4 sort
UNION
SELECT 'Hours' conentLabel, 'Hours' conentValue, 5 sort
UNION
SELECT 'ELR' conentLabel, 'ELR' conentValue, 6 sort
UNION
SELECT 'Commission' conentLabel, 'Commission' conentValue, 7 sort
UNION
SELECT '%Pen' conentLabel, 'Pen' conentValue, 8 sort
UNION
SELECT 'GP Labor' conentLabel, 'GPLabor' conentValue, 9 sort
UNION
SELECT 'GP Parts' conentLabel, 'GPParts' conentValue, 10 sort
UNION
SELECT 'GP Labor & Parts' conentLabel, 'GPLaborParts' conentValue, 11 sort
) m

IF (@Option = 1)
BEGIN
	delete #temp1 where conentValue in ('Commission','Pen')
END

select * from #temp1
order by sort
end





GO
