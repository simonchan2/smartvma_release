USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[rptSalesReportDetail]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rptSalesReportDetail]
@AdvisorCode		nvarchar(45),
@StartDate			datetime,
@EndDate			datetime,
@LaborType		    nvarchar(20),
@CPOnly				bit,
@ExclLabor0			bit,
@UserOnly			bit
AS
BEGIN
    SET NOCOUNT ON;

if (@LaborType = ' ' or @LaborType = '999')
begin
	set @LaborType = null
end

select distinct 
	i.AdvisorCode, 
	i.AdvisorName, 
	i.InvoiceDate,		
	(dc.FirstName + ' ' + dc.LastName) CustomerName,
	i.Mileage,
	isnull(dv.VehicleMake, '') + ' ' + isnull(dv.VehicleModel, '') + ' ' + isnull(cast(dv.VehicleYear as varchar),'') CarDesc,
	(i.InvoiceNumber) InvoiceNumber, 
	ROUND((d.LaborHour),2) LaborHour, 
	Round(d.PartSale,2) PartSale,
	Round(d.LaborSale,2) LaborSale,
	d.LaborType
into #temp
from dealer.Invoices i 
	join [dealer].[InvoiceDetails] d (nolock)on d.InvoiceId = i.InvoiceId
	join [dealer].[InvoiceDetailParts] p (nolock)on p.InvoiceDetailId = d.InvoiceDetailId
	join [dealer].[DealerDmsUsers] u (nolock)on u.DmsUserNumber = i.AdvisorCode
	join [dealer].[DealerCustomers] dc (nolock)on dc.[DealerCustomerId] = i.DealerCustomerId
	join [dealer].[DealerVehicles] dv (nolock)on dv.DealerVehicleId = i.DealerVehicleId
	join [dbo].[Vehicles] v (nolock)on v.[VehicleId] = dv.VehicleId
where i.InvoiceDate between @StartDate and @EndDate and
(@ExclLabor0 = 0 or (@ExclLabor0 = 1 and d.LaborHour = 0)) and
(@UserOnly = 0 or (@UserOnly = 1 and u.DmsUserName is not null)) and
(@CPOnly = 0 or (@CPOnly=1 and d.[LaborType] = 'CP'))
 and ((@LaborType is null and d.[LaborType] is null) or (@LaborType is not null and d.[LaborType]=@LaborType))
and i.AdvisorCode = @AdvisorCode
order by AdvisorName
--select * from #temp

select distinct 
max(InvoiceDate) InvoiceDate,		
max(InvoiceNumber) InvoiceNumber, 
max(CustomerName) CustomerName,
max(Mileage) Mileage,
max(CarDesc) CarDesc,
LaborType,
ROUND(sum(LaborHour),2) LaborHour, 
ROUND(sum(PartSale),2) PartSale,
ROUND(sum(LaborSale),2) LaborSale,  
ROUND((sum(PartSale) + sum(LaborSale)),2) Total,
Round((sum(PartSale) + sum(LaborSale))/count(InvoiceNumber),2) Avg$Ro,
Round(sum(LaborHour)/count(InvoiceNumber),2) AvgHrRo,
CASE
  WHEN sum(LaborHour)= 0 THEN 0
  else
  Round(sum(LaborSale) / sum(LaborHour),2)
END
ELR
into #temp2
from #temp 
group by InvoiceNumber, LaborType

select * from #temp2



END


GO
