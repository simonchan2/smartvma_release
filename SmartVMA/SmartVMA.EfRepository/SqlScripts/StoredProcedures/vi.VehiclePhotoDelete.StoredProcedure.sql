USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [vi].[VehiclePhotoDelete]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Boris Kamenov
-- Create date: 22.06.2016
-- Description:	VehiclePhotoDelete
-- =============================================
CREATE PROCEDURE [vi].[VehiclePhotoDelete] --2,8
	@Id bigint,
	@TenantId bigint
AS
BEGIN

	SELECT *
	FROM vi.VehiclePhotos vp
	INNER JOIN dealer.Inspection i ON i.Id = vp.InspectionId
	WHERE vp.Id = @Id
	AND i.DealerId = @TenantId
END


GO
