USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetBgProductsProtectionPlansPage]    Script Date: 9/23/2016 9:31:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[GetBgProductsProtectionPlansPage]
		@CategoryIds VARCHAR(MAX)= NULL,
		@PageIndex INT,
		@PageSize INT,
		@RecordCount INT OUTPUT

AS
BEGIN
DECLARE @Delimiter NVARCHAR(8) = ','

IF @CategoryIds = ''
    SET @CategoryIds = NULL

IF (@CategoryIds IS NULL)
	BEGIN 

		DECLARE @TABLE TABLE
		(  
		ID INT,
		ProductId INT,  
		BGSKU VARCHAR(50),  
		ProductDescription NVARCHAR(255),  
		ProdCategoryId INT, 
		ProtectionPlanId NVARCHAR(50)
		)

		INSERT INTO @TABLE  
		SELECT  
			ROW_NUMBER()	over (order BY	bgp.[BgProductId])	AS ID,
			bgp.[BgProductId]						AS ProductId,
			bgp.[PartNumber]						AS BGSKU, 
			bgp.[Description]						AS ProductDescription,
			cat.[BgProdCategoryId]					AS ProdCategoryId,
			bgpp.[BgProtectionPlanId]				AS ProtectionPlanId				
		FROM [bg].[BgProducts] bgp
			LEFT JOIN [bg].[BgProductsCategories] bgpc ON bgpc.[BgProductId] = bgp.[BgProductId]
			LEFT JOIN [bg].[BgProdCategories] cat ON cat.[BgProdCategoryId] = bgpc.[BgProdCategoryId]	
			LEFT JOIN bg.BgProductsProtectionPlans bgppp ON bgppp.BgProductId = bgp.BgProductId
			LEFT JOIN bg.BgProtectionPlans bgpp On bgpp.BgProtectionPlanId = bgppp.BgProtectionPlanId	
		WHERE	cat.[BgProdCategoryId] in (select	[BgProdCategoryId] from [bg].[BgProdCategories])

			 SELECT 
				ROW_NUMBER() OVER (ORDER BY [BgProductId] ASC) AS RowNumber          
				,BgProductId
				,BGSKU
				,ProductDescription
				,BgProductProtectionPlanIds
				,BgProductCategoryIds
		INTO #Results
		FROM
		(

		SELECT 
			T1.ProductId					AS BgProductId, 
			MAX(T1.BGSKU)					AS BGSKU, 
			MAX(T1.ProductDescription)		AS ProductDescription,
 			STUFF((
					SELECT ',' + CAST(T2.ProtectionPlanId AS NVARCHAR(50))
					FROM @TABLE T2
					WHERE T2.ProductId = T1.ProductId
					FOR XML PATH('')
					), 1, 1, '')			AS BgProductProtectionPlanIds,		
			STUFF((
					SELECT ',' + CAST(T2.ProdCategoryId AS nvarchar(50))
					FROM @TABLE T2
					WHERE T2.ProductId = T1.ProductId
					FOR XML PATH('')
					), 1, 1, '')			AS BgProductCategoryIds
		FROM @TABLE T1 
		GROUP BY ProductId 
	) AS Res


	SELECT @RecordCount = COUNT(*)
	FROM #Results

	SELECT * FROM #Results
    WHERE RowNumber BETWEEN @PageIndex + 1 AND (@PageIndex + @PageSize)

	  DROP TABLE #Results
	END
ELSE
	BEGIN
		DECLARE @FILTERTABLE TABLE
		(  
		ID INT,
		ProductId INT,  
		BGSKU VARCHAR(50),  
		ProductDescription NVARCHAR(255),  
		ProdCategoryId INT, 
		ProtectionPlanId NVARCHAR(50)
		)

		INSERT INTO @FILTERTABLE  
		SELECT  
			ROW_NUMBER()	over (order BY	bgp.[BgProductId])	AS ID,
			bgp.[BgProductId]						AS ProductId,
			bgp.[PartNumber]						AS BGSKU, 
			bgp.[Description]						AS ProductDescription,
			cat.[BgProdCategoryId]					AS ProdCategoryId,
			bgpp.[BgProtectionPlanId]				AS ProtectionPlanId				
		FROM [bg].[BgProducts] bgp
			LEFT JOIN [bg].[BgProductsCategories] bgpc ON bgpc.[BgProductId] = bgp.[BgProductId]
			LEFT JOIN [bg].[BgProdCategories] cat ON cat.[BgProdCategoryId] = bgpc.[BgProdCategoryId]	
			LEFT JOIN bg.BgProductsProtectionPlans bgppp ON bgppp.BgProductId = bgp.BgProductId
			LEFT JOIN bg.BgProtectionPlans bgpp On bgpp.BgProtectionPlanId = bgppp.BgProtectionPlanId	
		WHERE cat.[BgProdCategoryId] in (select	* from   [dbo].[SplitDelimiterString](@CategoryIds, @Delimiter))

	    SELECT 
			ROW_NUMBER() OVER (ORDER BY [BgProductId] ASC) AS RowNumber          
				,BgProductId
				,BGSKU
				,ProductDescription
				,BgProductProtectionPlanIds
				,BgProductCategoryIds
		INTO #ResultsFilter
		FROM
		(
		SELECT T1.ProductId AS BgProductId, MAX(T1.BGSKU) AS BGSKU, MAX(T1.ProductDescription) AS ProductDescription,
			
 				STUFF((
								SELECT ',' + CAST(T2.ProtectionPlanId AS NVARCHAR(50))
								FROM @FILTERTABLE T2
								WHERE T2.ProductId = T1.ProductId
								FOR XML PATH('')
								), 1, 1, '') AS BgProductProtectionPlanIds
								,
					STUFF((
					SELECT ',' + CAST(T2.ProdCategoryId AS nvarchar(50))
					FROM @FILTERTABLE T2
					WHERE T2.ProductId = T1.ProductId
					FOR XML PATH('')
					), 1, 1, '') AS BgProductCategoryIds
		FROM @FILTERTABLE T1 
		GROUP BY ProductId
		) As FiltTab


	 SELECT @RecordCount = COUNT(*)
      FROM #ResultsFilter

	    SELECT * FROM #ResultsFilter
		WHERE RowNumber BETWEEN @PageIndex + 1 AND (@PageIndex + @PageSize)

	  DROP TABLE #ResultsFilter

	END
END;

