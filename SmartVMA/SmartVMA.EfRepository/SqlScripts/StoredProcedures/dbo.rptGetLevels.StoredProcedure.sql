USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[rptGetLevels]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[rptGetLevels]
AS 
BEGIN

	SELECT 'All' levelLabel, 0 levelValue, 1 sort
	UNION
	SELECT 'Level 1' levelLabel, 1 levelValue, 1 sort
	UNION
	SELECT 'Level 2' levelLabel, 2 levelValue, 1 sort
	UNION
	SELECT 'Level 3' levelLabel, 3 levelValue, 1 sort
	order by sort

end





GO
