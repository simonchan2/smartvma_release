USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetAdditionalServices]    Script Date: 07.09.2016 09:50:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Krste Bozinoski
-- Create date: 07.07.2016
-- Description:	List all additional services for specific dealer vehicle id and chosen mileage
-- =============================================
CREATE PROCEDURE [dbo].[GetAdditionalServices]
    (
      @TenantId INT,
      @IsLOF BIT = NULL,
      @MenuLevel INT = NULL,
      @DealerVehicleId INT = NULL,
      @AppointmentPresentationId BIGINT = NULL
	)
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
	
        DECLARE @LaborRate FLOAT;
        SET @LaborRate = ( SELECT TOP 1
                                    CAST(Value AS FLOAT)
                           FROM     CompanySettings
                           WHERE    CompanyId = @TenantId
                                    AND Name = 'DefaultLaborRate'
                         );

        DECLARE @includeShopCharges BIT;
        DECLARE @shopChargeAmount FLOAT;
        DECLARE @shopChargesApplyToParts BIT;
        DECLARE @shopChargesApplyToLabor BIT;
        DECLARE @minShopCharges FLOAT;
        DECLARE @maxShopCharges FLOAT;
        SET @includeShopCharges = ( SELECT TOP 1
                                            CAST(Value AS BIT)
                                    FROM    CompanySettings
                                    WHERE   CompanyId = @TenantId
                                            AND Name = 'PrintShopCharges'
                                  );
        IF ( @includeShopCharges = 1 )
            BEGIN
                SET @shopChargeAmount = ( SELECT TOP 1
                                                    CAST(Value AS FLOAT)
                                          FROM      CompanySettings
                                          WHERE     CompanyId = @TenantId
                                                    AND Name = 'ShopChargeAmount'
                                        );
                SET @shopChargesApplyToParts = ( SELECT TOP 1
                                                        CAST(Value AS BIT)
                                                 FROM   CompanySettings
                                                 WHERE  CompanyId = @TenantId
                                                        AND Name = 'ShopChargesApplyToParts'
                                               );
                SET @shopChargesApplyToLabor = ( SELECT TOP 1
                                                        CAST(Value AS BIT)
                                                 FROM   CompanySettings
                                                 WHERE  CompanyId = @TenantId
                                                        AND Name = 'ShopChargesApplyToLabor'
                                               );
                SET @minShopCharges = ( SELECT TOP 1
                                                CAST(Value AS FLOAT)
                                        FROM    CompanySettings
                                        WHERE   CompanyId = @TenantId
                                                AND Name = 'ShopChargeMinimum'
                                      );
                SET @maxShopCharges = ( SELECT TOP 1
                                                CAST(Value AS FLOAT)
                                        FROM    CompanySettings
                                        WHERE   CompanyId = @TenantId
                                                AND Name = 'ShopChargeMaximum'
                                      );
            END;

        DECLARE @includeTaxCharges BIT;
        DECLARE @taxChargeAmount FLOAT;
        DECLARE @taxChargesApplyToParts BIT;
        DECLARE @taxChargesApplyToLabor BIT;
        SET @includeTaxCharges = ( SELECT TOP 1
                                            CAST(Value AS BIT)
                                   FROM     CompanySettings
                                   WHERE    CompanyId = @TenantId
                                            AND Name = 'DisplayTexesInMenus'
                                 );
        IF ( @includeTaxCharges = 1 )
            BEGIN
                SET @taxChargeAmount = ( SELECT TOP 1
                                                CAST(Value AS FLOAT)
                                         FROM   CompanySettings
                                         WHERE  CompanyId = @TenantId
                                                AND Name = 'TaxRateAmount'
                                       );
                SET @taxChargesApplyToParts = ( SELECT TOP 1
                                                        CAST(Value AS BIT)
                                                FROM    CompanySettings
                                                WHERE   CompanyId = @TenantId
                                                        AND Name = 'TaxRateApplyToParts'
                                              );
                SET @taxChargesApplyToLabor = ( SELECT TOP 1
                                                        CAST(Value AS BIT)
                                                FROM    CompanySettings
                                                WHERE   CompanyId = @TenantId
                                                        AND Name = 'TaxRateApplyToLabor'
                                              );
            END;

        IF OBJECT_ID('tempdb..#AdditionalServices') IS NOT NULL
            BEGIN
                DROP TABLE #Services;
            END;
        SELECT  ServiceId,
                OpCode,
                FullDescription,
                CarId,
                Mileage,
                IsFluid,
                IsEngineOil,
                IsLOF,
                CanBeStriked,
                IsStrikeOut,
                IsDeclined,
                IsPreviouslyServed,
                BgProtectionPlanId,
                VideoCode,
                MenuLevel,
                ServiceType,
                ServiceMenuType,
                LaborHour,
                LabourRate,
                PartsPrice,
                LaborPrice,
                Price,
                ShopChargesPartsPrice,
                ShopChargesLaborPrice
        INTO    #AdditionalServices
        FROM    ( SELECT    ServiceId,
                            OpCode,
                            FullDescription,
                            0 AS CarId,
                            0 AS Mileage,
                            IsFluid,
                            IsEngineOil,
                            IsLOF,
                            CONVERT(BIT, 0) AS CanBeStriked,
                            CONVERT(BIT, 0) AS IsStrikeOut,
                            CONVERT(BIT, 0) AS IsDeclined,
                            CONVERT(BIT, 0) AS IsPreviouslyServed,
                            BgProtectionPlanId,
                            '' AS VideoCode,
                            MenuLevel,
                            2 AS ServiceType,
                            2 AS ServiceMenuType,
                            LaborHour,
                            @LaborRate AS LabourRate,
                            PartsPrice,
                            ( @LaborRate * LaborHour ) AS LaborPrice,
                            ( ( @LaborRate * LaborHour ) + PartsPrice ) AS Price,
                            CASE @shopChargesApplyToParts
                              WHEN 1
                              THEN CASE WHEN PartsPrice * ( @shopChargeAmount
                                                            / 100 ) < @minShopCharges
                                        THEN @minShopCharges
                                        WHEN PartsPrice * ( @shopChargeAmount
                                                            / 100 ) > @maxShopCharges
                                        THEN @maxShopCharges
                                        ELSE PartsPrice * ( @shopChargeAmount
                                                            / 100 )
                                   END
                              ELSE 0
                            END AS ShopChargesPartsPrice,
                            CASE @shopChargesApplyToLabor
                              WHEN 1
                              THEN CASE WHEN ( @LaborRate * LaborHour )
                                             * ( @shopChargeAmount / 100 ) < @minShopCharges
                                        THEN @minShopCharges
                                        WHEN ( @LaborRate * LaborHour )
                                             * ( @shopChargeAmount / 100 ) > @maxShopCharges
                                        THEN @maxShopCharges
                                        ELSE ( @LaborRate * LaborHour )
                                             * ( @shopChargeAmount / 100 )
                                   END
                              ELSE 0
                            END AS ShopChargesLaborPrice
                  FROM      ( SELECT    services.AdditionalServiceId AS ServiceId,
                                        services.OpCode,
                                        services.OpDescription AS FullDescription,
                                        parts.IsFluid AS IsFluid,
                                        parts.IsEngineOil AS IsEngineOil,
                                        services.IsLofService AS IsLOF,
                                        services.CanBeStriked,
                                        services.BgProtectionPlanId AS BgProtectionPlanId,
                                        services.MenuLevel AS MenuLevel,
                                        services.LaborHour AS LaborHour,
                                        SUM(parts.Quantity * parts.UnitPrice) AS PartsPrice
                              FROM      dealer.AdditionalServices AS services
                                        INNER JOIN dealer.AdditionalServiceParts
                                        AS parts ON parts.AdditionalServiceId = services.AdditionalServiceId
                              WHERE     services.DealerId = @TenantId
                                        AND ( @IsLOF IS NULL
                                              OR services.IsLofService = @IsLOF
                                            )
                              GROUP BY  services.AdditionalServiceId,
                                        services.OpCode,
                                        services.OpDescription,
                                        parts.IsFluid,
                                        parts.IsEngineOil,
                                        services.IsLofService,
                                        services.MenuLevel,
                                        services.LaborHour,
                                        services.BgProtectionPlanId,
                                        services.CanBeStriked
                            ) AS q
                  UNION
                  SELECT    services.OrigServiceId,
                            services.OpCode,
                            services.OpDescription AS FullDescription,
                            0 AS CarId,
                            0 AS Mileage,
                            CONVERT(BIT, 0) AS IsFluid,
                            CONVERT(BIT, 0) AS IsEngineOil,
                            CONVERT(BIT, 0) AS IsLOF,
							CONVERT(BIT, 0) AS CanBeStriked,
                            CONVERT(BIT, 0) AS IsStrikeOut,
                            services.IsStriked AS IsDeclined,
                            CONVERT(BIT, 0) AS IsPreviouslyServed,
                            NULL AS BgProtectionPlanId,
                            '' AS VideoCode,
                            1 AS MenuLevel,
                            5 AS ServiceType,
                            2 AS ServiceMenuType,
                            services.LaborHour,
                            services.LaborRate AS LabourRate,
                            services.PartsPrice,
                            services.LaborPrice,
                            services.Price,
                            CASE @shopChargesApplyToParts
                              WHEN 1
                              THEN CASE WHEN services.PartsPrice
                                             * ( @shopChargeAmount / 100 ) < @minShopCharges
                                        THEN @minShopCharges
                                        WHEN services.PartsPrice
                                             * ( @shopChargeAmount / 100 ) > @maxShopCharges
                                        THEN @maxShopCharges
                                        ELSE services.PartsPrice
                                             * ( @shopChargeAmount / 100 )
                                   END
                              ELSE 0
                            END AS ShopChargesPartsPrice,
                            CASE @shopChargesApplyToLabor
                              WHEN 1
                              THEN CASE WHEN services.LaborPrice
                                             * ( @shopChargeAmount / 100 ) < @minShopCharges
                                        THEN @minShopCharges
                                        WHEN services.LaborPrice
                                             * ( @shopChargeAmount / 100 ) > @maxShopCharges
                                        THEN @maxShopCharges
                                        ELSE services.LaborPrice
                                             * ( @shopChargeAmount / 100 )
                                   END
                              ELSE 0
                            END AS ShopChargesLaborPrice
                  FROM      dealer.AppointmentServices services
                            INNER JOIN dealer.AppointmentPresentations AS presentation ON services.AppointmentPresentationId = presentation.AppointmentPresentationId
                  WHERE     services.DealerId = @TenantId
                            AND IsStriked = 1
                            AND ( @DealerVehicleId IS NULL
                                  OR presentation.DealerVehicleId = @DealerVehicleId
                                )
                  UNION
                  SELECT    services.OrigServiceId,
                            services.OpCode,
                            services.OpDescription AS FullDescription,
                            0 AS CarId,
                            0 AS Mileage,
                            CONVERT(BIT, 0) AS IsFluid,
                            CONVERT(BIT, 0) AS IsEngineOil,
                            CONVERT(BIT, 0) AS IsLOF,
							CONVERT(BIT, 0) AS CanBeStriked,
                            CONVERT(BIT, 0) AS IsStrikeOut,
                            services.IsStriked AS IsDeclined,
                            CONVERT(BIT, 0) AS IsPreviouslyServed,
                            NULL AS BgProtectionPlanId,
                            '' AS VideoCode,
                            1 AS MenuLevel,
                            5 AS ServiceType,
                            1 AS ServiceMenuType,
                            services.LaborHour,
                            services.LaborRate AS LabourRate,
                            services.PartsPrice,
                            services.LaborPrice,
                            services.Price,
                            CASE @shopChargesApplyToParts
                              WHEN 1
                              THEN CASE WHEN services.PartsPrice
                                             * ( @shopChargeAmount / 100 ) < @minShopCharges
                                        THEN @minShopCharges
                                        WHEN services.PartsPrice
                                             * ( @shopChargeAmount / 100 ) > @maxShopCharges
                                        THEN @maxShopCharges
                                        ELSE services.PartsPrice
                                             * ( @shopChargeAmount / 100 )
                                   END
                              ELSE 0
                            END AS ShopChargesPartsPrice,
                            CASE @shopChargesApplyToLabor
                              WHEN 1
                              THEN CASE WHEN services.LaborPrice
                                             * ( @shopChargeAmount / 100 ) < @minShopCharges
                                        THEN @minShopCharges
                                        WHEN services.LaborPrice
                                             * ( @shopChargeAmount / 100 ) > @maxShopCharges
                                        THEN @maxShopCharges
                                        ELSE services.LaborPrice
                                             * ( @shopChargeAmount / 100 )
                                   END
                              ELSE 0
                            END AS ShopChargesLaborPrice
                  FROM      dealer.AppointmentServices services
                            INNER JOIN dealer.AppointmentPresentations AS presentation ON services.AppointmentPresentationId = presentation.AppointmentPresentationId
                  WHERE     services.DealerId = @TenantId
                            AND services.ServiceTypeId = 4
                            AND services.AppointmentPresentationId = @AppointmentPresentationId
                            AND ( @DealerVehicleId IS NULL
                                  OR presentation.DealerVehicleId = @DealerVehicleId
                                )
                ) AS r;

        SELECT  ServiceId,
                OpCode,
                FullDescription,
                CarId,
                Mileage,
                IsFluid,
                IsEngineOil,
                IsLOF,
				CanBeStriked,
                IsStrikeOut,
                IsDeclined,
                IsPreviouslyServed,
                BgProtectionPlanId,
                VideoCode,
                MenuLevel,
                ServiceType,
                ServiceMenuType,
                LaborHour,
                LabourRate,
                PartsPrice,
                LaborPrice,
                Price,
                ShopChargesPartsPrice,
                ShopChargesLaborPrice,
                CASE @taxChargesApplyToParts
                  WHEN 1
                  THEN ( PartsPrice + ShopChargesPartsPrice )
                       * ( @taxChargeAmount / 100 )
                  ELSE 0
                END AS TaxIncludedPartsPrice,
                CASE @taxChargesApplyToLabor
                  WHEN 1
                  THEN ( LaborPrice + ShopChargesLaborPrice )
                       * ( @taxChargeAmount / 100 )
                  ELSE 0
                END AS TaxIncludedLaborPrice
        FROM    #AdditionalServices;

    END;

/*
	 exec [dbo].[GetAdditionalServices] @TenantId=3,@IsLOF=null,@MenuLevel=null
	 exec [dbo].[GetAdditionalServices] @TenantId=3,@IsLOF=1,@MenuLevel=null
	 exec [dbo].[GetAdditionalServices] @TenantId=3,@IsLOF=0,@MenuLevel=null
*/



GO
