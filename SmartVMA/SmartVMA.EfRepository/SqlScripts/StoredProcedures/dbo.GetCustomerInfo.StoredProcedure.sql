USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetCustomerInfo]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[GetCustomerInfo]
	@TenantId int,
	@DealerCustomerId int = null,
	@CarId int = null,
	@VinMasterTransmissionId int = null,
	@VinMasterDriveLineId int = null,
	@AppointmentPresentationId bigint = null,
	@DealerVehicleId int = null
AS
	SELECT distinct
		customerQuery.Id,
		customerQuery.FullName,
		customerQuery.FirstName,
		customerQuery.LastName,
		customerQuery.Email,
		customerQuery.[Address],
		customerQuery.[City],
		customerQuery.[State],
		customerQuery.[Zip],
		customerQuery.[Country],
		customerQuery.[HomePhone],
		customerQuery.[WorkPhone],
		customerQuery.[CellPhone],
		Car.VehicleYear as [Year],
		VM.VehicleMake as [Make],
		VM.VehicleMakeId as [MakeId],
		VModels.VehicleModel  as [Model],
		VModels.VehicleModelId  as [ModelId],
		CONVERT(varchar(10), Car.VehicleYear) + ' ' + VM.VehicleMake + ' ' + VModels.VehicleModel as CarFullName,
		customerQuery.[Mileage],
		customerQuery.[VIN], -- COALESCE(customerQuery.[VIN],V.VIN) as VIN,
		CASE WHEN @VinMasterTransmissionId IS NULL THEN Transmission 
			ELSE (select case when Transmission = 'N/A' then 'All' else Transmission end from vinquery.VinMasterTransmission where Id = @VinMasterTransmissionId)
		END as Transmission,
		CASE WHEN @VinMasterDriveLineId IS NULL THEN DriveLine 
			ELSE (select case when Driveline = 'N/A' then 'All' else Driveline end from vinquery.VinMasterDriveline where Id = @VinMasterDriveLineId)
		END as DriveLine,
		Car.CarId,
		VE.VehicleEngine as [Engine],
		VE.VehicleEngineId as [EngineId],
		customerQuery.DealerVehicleId,
		ISNULL(@VinMasterTransmissionId, VinMasterTransmissionId) as VinMasterTransmissionId,
		ISNULL(@VinMasterDriveLineId, VinMasterDrivelineId) as VinMasterDrivelineId
	FROM
		(SELECT   
			DC.[DealerCustomerId] as Id,
			[FirstName] + ' ' + [LastName] as [FullName],
			[FirstName],
			[LastName],
			[Email],
			[AddressLine1] as [Address],
			[City],
			CountryStateShort as [State],
			[Zip],
			C.Country as [Country],
			[HomePhone],
			[WorkPhone],
			[CellPhone],
			AP.Mileage as [Mileage],
			ISNULL(DV.[VIN], V.VIN) as VIN,
			CASE WHEN DV.VehicleDriveline IS NULL OR DV.VehicleDriveline = 'N/A' THEN 'All' ELSE DV.VehicleDriveline END as [DriveLine],
			CASE WHEN DV.VehicleTransmission IS NULL OR DV.VehicleTransmission = 'N/A' THEN 'All' ELSE DV.VehicleTransmission END as [Transmission],
			DV.VehicleId,
			DV.DealerVehicleId,
			AP.AppointmentPresentationId,
			ISNULL(V.CarId, ISNULL(DV.CarId, ' ')) as CarId,
			DV.VinMasterTransmissionId,
			DV.VinMasterDrivelineId
		FROM [dealer].[DealerVehicles] DV
			LEFT JOIN [dealer].[DealerCustomers] DC ON DV.DealerCustomerId = DC.DealerCustomerId
			LEFT JOIN [dbo].Countries C ON DC.CountryId = C.CountryId
			LEFT JOIN [dbo].CountryStates CS ON DC.CountryStateId = CS.CountryStateId
			LEFT JOIN [dealer].[AppointmentPresentations] AP ON ((@DealerCustomerId IS NULL OR DC.DealerCustomerId = AP.DealerCustomerId) AND DV.DealerVehicleId = AP.DealerVehicleId)
			LEFT JOIN [dbo].Vehicles V ON DV.VehicleId = V.VehicleId
		WHERE
			DV.DealerId = @TenantId 
			AND ((@DealerCustomerId IS NULL OR DC.DealerCustomerId = @DealerCustomerId AND (@DealerVehicleId IS NULL OR DV.DealerVehicleId = @DealerVehicleId))
					AND (@AppointmentPresentationId IS NULL OR AP.AppointmentPresentationId = @AppointmentPresentationId))
		) as customerQuery
		FULL OUTER JOIN [amam].Cars Car ON Car.CarId = customerQuery.CarId OR car.CarId = @CarId
		JOIN [amam].VehicleEngines VE ON VE.VehicleEngineId = Car.VehicleEngineId
		JOIN [amam].VehicleMakes VM ON Car.VehicleMakeId = VM.VehicleMakeId
		JOIN [amam].VehicleModels VModels ON Car.VehicleModelId = VModels.VehicleModelId
	WHERE 
		(@CarId IS NULL OR Car.CarId = @CarId)
		AND ((@DealerCustomerId IS NULL AND @DealerVehicleId IS NULL AND @AppointmentPresentationId IS NULL AND customerQuery.Id IS NULL)
				OR (@DealerCustomerId IS NULL OR customerQuery.Id = @DealerCustomerId  AND (@DealerVehicleId IS NULL OR customerQuery.DealerVehicleId = @DealerVehicleId)) OR (customerQuery.AppointmentPresentationId = @AppointmentPresentationId))
	

/*
	exec dbo.GetCustomerInfo @TenantId=3, @DealerCustomerId=2, @CarId=2, @AppointmentPresentationId=null
	exec dbo.GetCustomerInfo @TenantId=3, @DealerCustomerId=null, @CarId=2, @AppointmentPresentationId=null
	exec dbo.GetCustomerInfo @TenantId=3, @DealerCustomerId=null, @CarId=null, @AppointmentPresentationId=4
	exec dbo.GetCustomerInfo @TenantId=3, @DealerCustomerId=2, @CarId=null, @AppointmentPresentationId=4
	exec dbo.GetCustomerInfo @TenantId=3, @DealerCustomerId=2, @CarId=null, @AppointmentPresentationId=null


	exec dbo.GetCustomerInfo @TenantId=1, @DealerCustomerId=null, @CarId=null, @AppointmentPresentationId=2

	exec dbo.GetCustomerInfo @TenantId=3, @DealerCustomerId=null, @CarId=null, @AppointmentPresentationId=19


	exec dbo.GetCustomerInfo @TenantId=1090, @DealerCustomerId=31658, @CarId=13038, @AppointmentPresentationId=null
*/