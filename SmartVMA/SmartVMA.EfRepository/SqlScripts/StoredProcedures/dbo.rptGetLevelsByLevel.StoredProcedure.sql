USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[rptGetLevelsByLevel]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[rptGetLevelsByLevel]
	@Level int = 0
AS 
BEGIN

if @Level = 0
begin
	SELECT 'Level 1' levelLabel, 1 levelValue
	UNION
	SELECT 'Level 2' levelLabel, 2 levelValue
	UNION
	SELECT 'Level 3'	levelLabel, 3 levelValue
end
else
begin
	SELECT 'Level ' +CAST(@Level AS varchar) levelLabel, @Level levelValue
end

end





GO
