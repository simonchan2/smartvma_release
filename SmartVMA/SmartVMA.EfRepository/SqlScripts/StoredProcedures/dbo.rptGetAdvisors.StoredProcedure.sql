USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[rptGetAdvisors]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[rptGetAdvisors]
AS 
BEGIN

		select distinct
          [DmsUserNumber] AdvisorCode,
          [DmsUserName] AdvisorName,
          2 sort
          from dealer.DealerDmsUsers
          where [DmsUserNumber] is not null and len([DmsUserNumber]) > 0
          union
          select 'AllAdvisors' AdvisorCode, 'All Advisors' AdvisorName, 0 sort
          union
          select 'Consolidated' AdvisorCode, 'Consolidated' AdvisorName, 1 sort
          order by sort, AdvisorName
END





GO
