USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [vi].[InspectionReset]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Boris Kamenov
-- Create date: 22.06.2016
-- Description:	InspectionReset
-- =============================================
CREATE PROCEDURE [vi].[InspectionReset] --4, 1
	@Id bigint,
	@TenantId bigint
AS
BEGIN

	UPDATE i
	SET 
	LFRimScratch = 0,
	LRRimScratch = 0,
	RFRimScratch = 0,
	RRRimScratch = 0,
	LFTireTypeId = NULL,
	LRTireTypeId = NULL,
	RFTireTypeId = NULL,
	RRTireTypeId = NULL,
	Fuel = NULL,
	TimeUpdated = GETUTCDATE()
	FROM dealer.Inspection i
	WHERE i.Id = @Id
	AND i.DealerId = @TenantId

	DELETE di
	FROM vi.DamageIndications di
	INNER JOIN dealer.Inspection i ON i.Id = di.InspectionId
	WHERE di.InspectionId = @Id
	AND i.DealerId = @TenantId

	--TO-DO Delete Photo Records
	SELECT *
	FROM vi.VehiclePhotos vp
	INNER JOIN dealer.Inspection i ON i.Id = vp.InspectionId
	WHERE vp.InspectionId = @Id
	AND i.DealerId = @TenantId
END


GO
