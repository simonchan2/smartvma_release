USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetMileagesByCarId]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Boris Kamenov
-- Create date: 04.08.2016
-- Description:	Get Distinct Mileages By CarId
-- =============================================
ALTER PROCEDURE [dbo].[GetMileagesByCarId](
	@CarId int, 
	@MeasurmentUnitId int
)
AS
BEGIN
	SELECT Mileage 
	FROM [GetMileagesByCarIdFunc](@CarId,@MeasurmentUnitId) 
	ORDER BY Mileage
END

/*
	EXEC [dbo].[GetMileagesByCarId] 3,1
*/

GO
