﻿USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[SetAppointmentToInspection]    Script Date: 31.10.2016 г. 13:31:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SetAppointmentToInspection] 
	@TenantId int, 
	@InspectionId bigint,
	@AppointmentPresentationId bigint
AS
BEGIN
	BEGIN
		UPDATE dealer.Inspection
		SET AppointmentPresentationId = @AppointmentPresentationId
		WHERE Id = @InspectionId
	END
END

GO
