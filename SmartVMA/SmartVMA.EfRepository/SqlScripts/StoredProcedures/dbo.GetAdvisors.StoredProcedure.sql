USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetAdvisors]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetAdvisors] 
	@TenantId int
AS
BEGIN
	select  distinct usr.UserId as Id, usr.FirstName + ' ' + usr.LastName as Name from dbo.Users as usr
	inner join dealer.AppointmentPresentations as app on usr.UserId = app.AdvisorId
	where app.DealerId = @TenantId
END


GO
