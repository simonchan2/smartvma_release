USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetCarIdByFilters]    Script Date: 19.08.2016 16:44:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetCarIdByFilters]
	@Year  int,
	@MakeId int,
	@ModelId int,
	@EngineId int,
	@VinMasterTransmissionId int,
	@VinMasterDriveLineId int 
AS

DECLARE @CarId int = null

SET @CarId = (SELECT TOP 1 car.CarId 
			  FROM amam.Cars as car
					INNER JOIN amam.CarVinShorts as cv on car.CarId = cv.CarId
					INNER JOIN vinquery.VinMaster as vm on cv.VinMasterId = vm.VinMasterId
					WHERE car.VehicleYear = @Year
					AND car.VehicleMakeId = @MakeId
					AND car.VehicleModelId = @ModelId
					AND car.VehicleEngineId = @EngineId
					AND vm.VinMasterTransmissionId = @VinMasterTransmissionId
					AND vm.VinmasterDrivelineId = @VinMasterDriveLineId)

IF (@CarId IS NULL)
BEGIN
	declare @NA nvarchar(5) = 'N/A';
	IF((select count(vt.VehicleTransmissionId) from amam.VehicleTransmissions vt inner join vinquery.VinMasterTransmission vmt on vt.VehicleTransmissionId = vmt.VehicleTransmissionId where vt.VehicleTransmission = @NA and vmt.Id = @VinMasterTransmissionId) = 1
	and (select count(vd.VehicleDrivelineId) from amam.VehicleDrivelines vd inner join vinquery.VinMasterDriveline vmd on vd.VehicleDrivelineId = vmd.VehicleDrivelineId where vd.VehicleDriveline = @NA and vmd.Id = @VinMasterDriveLineId) = 1)
	BEGIN
		SET @CarId = (SELECT TOP 1 car.CarId 
					  FROM amam.Cars as car
						WHERE car.VehicleYear = @Year
						AND car.VehicleMakeId = @MakeId
						AND car.VehicleModelId = @ModelId
						AND car.VehicleEngineId = @EngineId)
	END
END

SELECT @CarId