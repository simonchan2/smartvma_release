USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetColumnsInventory]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Antoni Ivanov
-- Create date: 2016-05-12
-- Description:	Gets an inventory of all tables and columns in the current database
-- =============================================
CREATE PROCEDURE [dbo].[GetColumnsInventory]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT  isc.TABLE_SCHEMA, 
			isc.TABLE_NAME, 
			isc.COLUMN_NAME, 
			isc.DATA_TYPE, 
			isc.IS_NULLABLE, 
			sep.[value] AS COLUMN_DESCRIPTION,
			isc.ORDINAL_POSITION, 
			isc.COLUMN_DEFAULT, 
			ISNULL(CHARACTER_MAXIMUM_LENGTH, NUMERIC_PRECISION) AS PRECISION,
			COLUMNPROPERTY(OBJECT_ID('[' + isc.TABLE_SCHEMA + '].[' + isc.TABLE_NAME + ']'), isc.COLUMN_NAME, 'IsIdentity') AS ISIDENTITY,
			COLUMNPROPERTY(OBJECT_ID('[' + isc.TABLE_SCHEMA + '].[' + isc.TABLE_NAME + ']'), isc.COLUMN_NAME, 'IsComputed') AS ISCOMPUTED,
			PK.ISPRIMARYKEY,
			PK.INDEX_NAME,
			PK.FOREIGN_KEY_NAME,
			PK.FOREIGN_TABLE_NAME,
			PK.FOREIGN_COLUMN_NAME,
			PK.ISDISABLED,
			PK.DELETE_ACTION,
			PK.UPDATE_ACTION

	FROM INFORMATION_SCHEMA.COLUMNS isc

	LEFT JOIN SYS.EXTENDED_PROPERTIES sep
	ON sep.major_id = OBJECT_ID(TABLE_SCHEMA + '.' + TABLE_NAME)
	AND sep.name = 'MS_Description' 
	AND sep.minor_id = ORDINAL_POSITION

	LEFT JOIN (
		SELECT 
				SCHEMA_NAME(o.schema_id) AS TABLE_SCHEMA
				,o.object_id
				,o.name AS TABLE_NAME
				,c.name AS COLUMN_NAME
				,i.is_primary_key AS ISPRIMARYKEY
				,i.name AS INDEX_NAME
				,f.name AS FOREIGN_KEY_NAME
				,OBJECT_NAME (f.referenced_object_id) AS FOREIGN_TABLE_NAME
				,COL_NAME(fc.referenced_object_id, fc.referenced_column_id) AS FOREIGN_COLUMN_NAME
				,f.is_disabled AS ISDISABLED
				,f.delete_referential_action_desc AS DELETE_ACTION
				,f.update_referential_action_desc AS UPDATE_ACTION
			FROM sys.indexes AS i 
			JOIN sys.index_columns AS ic 
				ON i.object_id = ic.object_id
				AND i.index_id = ic.index_id
			JOIN sys.objects AS o 
				ON i.object_id = o.object_id
			LEFT JOIN sys.columns AS c ON ic.object_id = c.object_id
				AND c.column_id = ic.column_id
			LEFT JOIN sys.foreign_key_columns AS fc 
				ON o.object_id = fc.parent_object_id 
				AND c.column_id = fc.parent_column_id
			LEFT JOIN 
				sys.foreign_keys AS f 
				ON f.object_id = fc.constraint_object_id 
		) AS PK
	ON isc.TABLE_NAME = pk.TABLE_NAME
	AND isc.TABLE_SCHEMA = pk.TABLE_SCHEMA
	AND isc.COLUMN_NAME = pk.COLUMN_NAME

END


GO
