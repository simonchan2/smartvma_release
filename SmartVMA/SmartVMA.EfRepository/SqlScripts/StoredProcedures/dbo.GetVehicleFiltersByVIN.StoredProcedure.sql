USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetVehicleFiltersByVIN]    Script Date: 29.09.2016 11:04:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetVehicleFiltersByVIN] 
	@Years nvarchar(max),
	@Makes nvarchar(max),
	@Models nvarchar(max),
	@EngineTypes nvarchar(max),
	@TransmissionTypes nvarchar(max),
	@DriveLines nvarchar(max),
	@VIN nvarchar(max)
AS
BEGIN
SET NOCOUNT ON;

	declare @FiltersTable table(Id int null,[Text] nvarchar(500),EntityType int);

	declare @Cars nvarchar(max);
	declare @VinShort nvarchar(20);
	set @VinShort = SUBSTRING(@VIN, 0, 9) + SUBSTRING(@VIN, 10, 2);

	SELECT @Cars = STUFF((
            SELECT ',' + Convert(nvarchar(max), CarId)
            FROM amam.CarVinShorts WHERE VinShort = @VinShort
			AND CarId IS NOT NULL
            FOR XML PATH('')
            ), 1, 1, '')

	IF(@Cars is null)
	BEGIN
		SELECT @Cars = STUFF((
            SELECT ',' + Convert(nvarchar(max), CarId)
            FROM dbo.Vehicles WHERE VinShort = @VinShort
			AND CarId IS NOT NULL
            FOR XML PATH('')
            ), 1, 1, '')
	END

	IF(@Cars is not null)
	BEGIN
		INSERT  INTO @FiltersTable
                EXEC GetVehicleFilters @Years, @Makes,
                    @Models, @EngineTypes, @TransmissionTypes, @DriveLines,
                    @Cars, @VinShort;
	END
	ELSE
	BEGIN
		declare @VinMasters nvarchar(max);
		SELECT @VinMasters = STUFF((
            SELECT ',' + Convert(nvarchar(max), VinMasterId)
            FROM vinquery.VinMaster WHERE VinShort = @VinShort
            FOR XML PATH('')
            ), 1, 1, '')

		IF(@VinMasters is not null)
		BEGIN
			INSERT  INTO @FiltersTable
					EXEC GetVehicleFiltersByVinMaster @Years, @Makes,
						@Models, @EngineTypes, @TransmissionTypes, @DriveLines,
						@VinMasters;
		END
	END

	SELECT Id, [Text], EntityType
	FROM @FiltersTable
END
