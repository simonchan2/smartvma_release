USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetVehicleEngines]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetVehicleEngines] 
	@Year NVARCHAR(max) = null,
	@Make NVARCHAR(max) = null,
	@Model NVARCHAR(max) = null
AS
BEGIN
	IF(@Year IS NOT NULL AND @Make IS NOT NULL AND @Model IS NOT NULL)
	BEGIN
	declare @Delimiter nvarchar(8);
	set @Delimiter =',';

		select  distinct ve.VehicleEngineId as Id, ve.VehicleEngine as Engine from amam.Cars as car
		inner join amam.VehicleEngines as ve on car.VehicleEngineId = ve.VehicleEngineId
		INNER JOIN dbo.SplitDelimiterString(@Year, @Delimiter) as years ON (years.Item = CAST(ISNULL(car.VehicleYear,null) as NVARCHAR(50)))
		where CAST(ISNULL(car.VehicleMakeId,null) as NVARCHAR(50)) in (select * from dbo.SplitDelimiterString(@Make, @Delimiter))
			AND CAST(ISNULL(car.VehicleModelId, null) as NVARCHAR(50)) in (select * from dbo.SplitDelimiterString(@Model, @Delimiter))
	END
	ELSE
	BEGIN
		select VehicleEngineId as Id, VehicleEngine as Engine from amam.VehicleEngines
	END
END
