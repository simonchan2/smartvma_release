USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[CreateAppointmentPresentationFromAppointment]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CreateAppointmentPresentationFromAppointment] 
	@TenantId int,
	@AppointmentId int
AS
BEGIN
	IF ((SELECT COUNT(*) FROM dealer.AppointmentPresentations WHERE AppointmentId = @AppointmentId) = 0)
	BEGIN
		INSERT INTO dealer.AppointmentPresentations
		(DealerId, AppointmentId, AdvisorId, DealerCustomerId, DealerVehicleId, VehicleLicense, Mileage, MeasurementUnitId, PresentationStatus, AppointmentTime, AppointmentDuration, PromisedTime, CustomerPhone, ServiceTime, RoNumber, DateRoCreated, InvoiceNumber, TagNumber, TagColor, Waiter, TimeCreated, TimeUpdated)
		SELECT 
		a.DealerId, a.AppointmentId, a.UserId, a.DealerCustomerId, a.DealerVehicleId, a.VehicleLicense, a.Mileage, c.MeasurementUnitId, a.AppointmentStatus PresentationStatus, a.AppointmentTime, a.AppointmentDuration, a.PromisedTime, a.CustomerPhone,a.ServiceTime, a.RoNumber, a.DateRoCreated, a.InvoiceNumber, a.TagNumber, a.TagColor, a.Waiter, GETDATE(), GETDATE()
		FROM dealer.Appointments a
		INNER JOIN Companies c ON a.DealerID = c.CompanyID
		WHERE a.AppointmentId = @AppointmentId
	END
END


GO
