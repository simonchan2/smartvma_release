USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[SearchCustomer_OLD]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SearchCustomer_OLD] 
	@TenantId int,
	@CustomerName nvarchar(355) = null,
	@CustomerNumber nvarchar(25) = null,
	@CustomerPhone nvarchar(35) = null,
	@VIN nvarchar(25) = null,
	@StockNo nvarchar(50) = null,
	@VehicleLicense nvarchar(15) = null
AS
BEGIN

	/*
	DECLARE @FirstName nvarchar(150)
	DECLARE @LastName nvarchar(150)

	if (CHARINDEX(', ',@CustomerName) > 0) OR (CHARINDEX(',',@CustomerName) > 0)
	BEGIN
	Select @LastName = SUBSTRING(@CustomerName, 1, CASE WHEN CHARINDEX(',', @CustomerName) > 0 THEN CHARINDEX(',', @CustomerName)-1 ELSE 0 END)
	Select @FirstName = LTRIM(Substring(@CustomerName, LEN(@LastName)+2, LEN(@CustomerName)))
	END
	else if (CHARINDEX(' ',@CustomerName) > 0)
	BEGIN
	Select @FirstName = SUBSTRING(@CustomerName, 1,CHARINDEX(' ', @CustomerName)-1)
	Select @LastName = LTRIM(SUBSTRING(@CustomerName, LEN(@FirstName)+1, LEN(@CustomerName)))
	END
	else
	BEGIN
	Select @FirstName = @CustomerName
	END
	*/

	SELECT * FROM
		(SELECT
			app.AppointmentPresentationId as AppointmentPresentationId,
			app.VehicleLicense as VehicleLicense,
			app.AppointmentTime as AppointmentTime,
			app.PresentationStatus as AppointmentPresentationStatus,
			app.DealerId as DealerId,
			dv.VIN as VIN,
			dv.VehicleMake as Make,
			dv.VehicleModel as Model,
			dv.VehicleYear as [Year],
			dv.VehicleDriveline as VehicleDriveline,
			dc.DealerCustomerId as CustomerId,
			dc.FirstName, 
			dc.LastName, 
			dc.CustomerNumber,
			dv.StockNo,
			dv.DealerVehicleId as VehicleId,
			app.CustomerPhone as CustomerPhone
		FROM dealer.AppointmentPresentations app
		INNER JOIN dealer.DealerCustomers dc 
			ON app.DealerCustomerId = dc.DealerCustomerId
		INNER JOIN dealer.DealerVehicles dv 
			ON app.DealerVehicleId = dv.DealerVehicleId) t
	WHERE t.DealerId = @TenantId
		AND ( ((t.FirstName like '%' + @CustomerName + '%' OR @CustomerName is NULL))
				OR 
			 ((t.LastName like '%' + @CustomerName + '%' OR @CustomerName is NULL)))
		AND (t.CustomerNumber = @CustomerNumber OR @CustomerNumber is NULL)
		AND (t.CustomerPhone = @CustomerPhone OR @CustomerPhone is NULL)
		AND (t.VIN = @VIN OR @VIN is NULL)
		AND (t.StockNo = @StockNo OR @StockNo is NULL)
		AND (t.VehicleLicense = @VehicleLicense OR @VehicleLicense is NULL)
		--AND ((CHARINDEX(@FirstName, t.FirstName) > 0) OR (CHARINDEX(@LastName,t.LastName) > 0))
		--AND (CHARINDEX(ISNULL(@CustomerNumber,t.CustomerNumber),t.CustomerNumber) > 0)
		--AND (CHARINDEX(ISNULL(@CustomerPhone,t.CustomerPhone),t.CustomerPhone) > 0)
		--AND (CHARINDEX(ISNULL(@VIN,t.VIN),t.VIN) > 0)
		--AND (CHARINDEX(ISNULL(@StockNo,t.StockNo),t.StockNo) > 0)
		--AND (CHARINDEX(ISNULL(@VehicleLicense,t.VehicleLicense),t.VehicleLicense) > 0)
END


GO
