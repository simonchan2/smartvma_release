USE [SmartVmaDev]
GO

/****** Object:  StoredProcedure [dbo].[GetVehicleTransmissionsFromVinMaster]    Script Date: 12/12/2016 7:27:58 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[GetVehicleTransmissionsFromVinMaster] 
	@Year nvarchar(max) = null,
	@Make nvarchar(max) = null,
	@Model nvarchar(max) = null,
	@Engine nvarchar(max) = null
AS
BEGIN
	DECLARE @Delimiter NVARCHAR(8) = ',';
	DECLARE @NA NVARCHAR(5)= 'N/A';
	
	SELECT DISTINCT vmt.Id, vmt.Transmission
	FROM GetCarIdByCriteria(@Year, @Make, @Model, @Engine, NULL, NULL, @Delimiter) c
	INNER JOIN amam.CarVinShorts as cv ON cv.CarId = c.CarId
	INNER JOIN vinquery.VinMaster as vm ON  vm.VinMasterId = cv.VinMasterId
	INNER JOIN vinquery.VinMasterTransmission vmt ON vmt.Id = vm.VinMasterTransmissionId OR vmt.Transmission = @NA
	ORDER BY vmt.Transmission
END


/*
EXEC [dbo].[GetVehicleTransmissionsFromVinMaster] 

@Year = NULL
,@Make = NULL
,@Model = NULL
,@Engine = NULL
*/



GO


