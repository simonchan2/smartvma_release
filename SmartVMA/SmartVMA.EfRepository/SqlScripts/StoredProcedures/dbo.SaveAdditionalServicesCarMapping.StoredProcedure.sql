USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[SaveAdditionalServicesCarMapping]    Script Date: 12/12/2016 7:29:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SaveAdditionalServicesCarMapping]
	--@TenantId int,
	@Years nvarchar(max),
	@Makes nvarchar(max),
	@Models nvarchar(max),
	@EngineTypes nvarchar(max),
	@TransmissionTypes nvarchar(max),
	@DriveLines nvarchar(max),
	@Services nvarchar(max),
	@MenuLevel int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @Delimiter NVARCHAR(8);
	SET @Delimiter =',';
	-----------------------------------------------------------------------
	
	DECLARE @ServiceIds TABLE(Id INT NULL);
	INSERT INTO @ServiceIds SELECT CAST(ISNULL(Item,null) as int) as Id FROM dbo.SplitDelimiterString(@Services, @Delimiter);

	DECLARE @CarIds TABLE(Id int null);
	INSERT INTO @CarIds
	SELECT DISTINCT cars.CarId
	FROM dbo.GetCarIdByCriteria(@Years, @Makes, @Models, @EngineTypes, @DriveLines, @TransmissionTypes, @Delimiter) cars

	--DELETE Previous Services which match the criteria
	DELETE
	FROM dbo.AdditionalServicesCars
	WHERE CarId IN (SELECT Id FROM @CarIds)
	AND AdditionalServiceId IN (SELECT Id FROM @ServiceIds)

	;WITH 
	CrossTableIds AS
	(
		SELECT 
		services.Id as AdditionalServiceId,
		cars.Id as CarId
		FROM @ServiceIds services
		CROSS JOIN @CarIds cars
	)
	--INSERT New services which match the criteria
	INSERT INTO dbo.AdditionalServicesCars (AdditionalServiceId, CarId, TimeCreated,TimeUpdated,MenuLevel)
	SELECT 
	ct.AdditionalServiceId,
	ct.CarId,
	GETDATE() as TimeCreated,
	GETDATE() as TimeUpdated,
	@MenuLevel as MenuLevel
	FROM CrossTableIds ct

	SELECT CAST(1 AS BIT)
END
