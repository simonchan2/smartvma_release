USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetAllServices_NEW]    Script Date: 27.10.2016 08:57:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Boris Kamenov
-- Create date: 07.09.2016
-- Description:	List all services.
-- Services can be filtered by:
--		DealerVehicleId
--		Mileage
--		Specific service ids in the following form: serviceId_serviceTableId
-- =============================================
ALTER PROCEDURE [dbo].[GetAllServices_NEW](
	@TenantId int,
	@CarId int = null, 
	@DealerVehicleId int = null,
	@Mileage int = null,
	@IsLOF bit = null,
	@ServiceIds nvarchar(max) = null,
	@VinMasterTransmission int = null,
	@VinMasterDriveLine int = null,
	@DealerCustomerId int = null,
	@AppointmentPresentationId bigint = null
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @MeasurementUnitId int;
	set @MeasurementUnitId = (select top 1 CAST(value as int) from CompanySettings where CompanyId = @TenantId and Name = 'MeasurementUnit');

	declare @LanguageId int;
	set @LanguageId = (select top 1 CAST(value as int) from CompanySettings where CompanyId = @TenantId and Name = 'Language');
	set @LanguageId = @LanguageId    -- values: English = 1,        French = 2,        Spanish = 3 

	declare @LaborRate float;
	set @LaborRate = ISNULL((select top 1 CAST(value as float) from CompanySettings where CompanyId = @TenantId and Name = 'DefaultLaborRate'),0);

	declare @UseOEMMenus bit;
	set @UseOEMMenus = (select top 1 CAST(value as bit) from CompanySettings where CompanyId = @TenantId and Name = 'UseOEMMenus');

	declare @VehicleTransmission int = null;
	if(@VinMasterTransmission IS NOT NULL)
	begin
	set @VehicleTransmission = (select VehicleTransmissionId from vinquery.VinMasterTransmission where Id = @VinMasterTransmission)
	end
	declare @VehicleDriveLine int = null;
	if(@VinMasterDriveLine IS NOT NULL)
	begin
	set @VehicleDriveLine = (select VehicleDrivelineId from vinquery.VinMasterDriveline where Id = @VinMasterDriveLine)
	end
	
	declare @ServicesTable table(
		ServiceId bigint,
		OpCode nvarchar(50),
		FullDescription nvarchar(1000),
		CarId int,
		Mileage int,
		
		IsFluid bit,
		IsEngineOil bit,
		IsLOF bit,
		CanBeStriked BIT,
		IsStrikeOut bit,
		IsDeclined bit,
		IsPreviouslyServed bit,
		BgProtectionPlanId int,
		
		VideoCode nvarchar(500),
		MenuLevel int,
		ServiceType int,
		ServiceMenuType int,

		LaborHour float,
		LabourRate float,
		PartsPrice float,
		LaborPrice float,
		Price float,
		MaxMileageBeforeFirstService int,
	    ServiceInterval int,
		BgProtectionPlanUrl nvarchar(100),
		ShopChargesPartsPrice float,
		ShopChargesLaborPrice float,
		TaxIncludedPartsPrice float,
		TaxIncludedLaborPrice float,

		PreviousMileage int,
		PreviousServedDate datetime
	)
	INSERT INTO @ServicesTable 
	SELECT *, NULL AS ShopChargesPartsPrice, NULL AS ShopChargesLaborPrice, NULL AS TaxIncludedPartsPrice, NULL AS TaxIncludedLaborPrice,
	NULL AS PreviousMileage, NULL AS PreviousServedDate
	FROM [dbo].[GetLeftHandServices](
	@TenantId,
	@CarId, 
	@DealerVehicleId,
	@Mileage,
	@IsLOF,
	@VehicleTransmission,
	@VehicleDriveLine, 
	@AppointmentPresentationId,
	@LanguageId,
	@MeasurementUnitId,
	@LaborRate,
	4) as lhs

	DECLARE @HasMatchingOemServices BIT = IIF((SELECT COUNT(st.ServiceId) FROM @ServicesTable st WHERE st.ServiceType = 1) > 0, CAST(1 as BIT), CAST(0 as BIT))
	-- if it's regular service then also load additional services
	IF (@IsLOF != 1 AND @HasMatchingOemServices = 1)
	BEGIN
		INSERT INTO @ServicesTable 
		SELECT * , NULL AS ShopChargesPartsPrice, NULL AS ShopChargesLaborPrice, NULL AS TaxIncludedPartsPrice, NULL AS TaxIncludedLaborPrice,
		NULL AS PreviousMileage, NULL AS PreviousServedDate
		FROM [dbo].[GetRightHandServices](
		@TenantId,
		@CarId, 
		@DealerVehicleId,
		@Mileage,
		@IsLOF,
		@VehicleTransmission,
		@VehicleDriveLine,
		@MeasurementUnitId,
		@LaborRate,
		@LanguageId) rhServices 
		--INCLUDE only services which do not exist yet
		WHERE rhServices.ServiceId NOT IN (select serviceId from @ServicesTable);
	END

	declare @ServiceIdsTable table(Id int,ServiceOriginTableId int);
	insert into @ServiceIdsTable select * from dbo.SplitServiceIdValues(@ServiceIds);

	IF(@DealerCustomerId IS NOT NULL)
	BEGIN
		declare @PreviousCustomerServices table(
			ServiceId bigint,
			IsStriked bit
		)

		SELECT  row_number() over (order by AppointmentPresentationId desc) value,AppointmentPresentationId
		INTO #LatestAppoitment
		FROM dealer.AppointmentPresentations
		WHERE DealerCustomerId = @DealerCustomerId AND PresentationStatus NOT IN ('M', 'MK')

		INSERT INTO @PreviousCustomerServices
			SELECT aps.OrigServiceId, aps.IsStriked
		FROM dealer.AppointmentPresentations ap
		INNER JOIN dealer.AppointmentServices aps ON ap.AppointmentPresentationId = aps.AppointmentPresentationId
		WHERE ap.DealerCustomerId = @DealerCustomerId AND ap.DealerId = @TenantId
		AND ap.AppointmentPresentationId = (select AppointmentPresentationId from #LatestAppoitment WHERE value = 1)
		--(select max(AppointmentPresentationId) from dealer.AppointmentPresentations WHERE DealerCustomerId = @DealerCustomerId)

		--Setting up Previous declined services for our customer
		UPDATE  @ServicesTable 
		SET IsDeclined = 1 WHERE ServiceId IN (SELECT ServiceId FROM @PreviousCustomerServices WHERE IsStriked=1)

		--Setting up Previous accepted services for our customer
		UPDATE  @ServicesTable 
		SET IsPreviouslyServed = 1 WHERE ServiceId IN (SELECT ServiceId FROM @PreviousCustomerServices WHERE IsStriked=0)

		UPDATE  @ServicesTable
		SET PreviousMileage = ap.Mileage FROM dealer.AppointmentPresentations ap WHERE ap.AppointmentPresentationId = (select AppointmentPresentationId from #LatestAppoitment WHERE value = 1)

		UPDATE  @ServicesTable
		SET PreviousServedDate = ISNULL(ap.DateRoCreated, ap.AppointmentTime) FROM dealer.AppointmentPresentations ap WHERE ap.AppointmentPresentationId = (select AppointmentPresentationId from #LatestAppoitment WHERE value = 1)
	END

	UPDATE st
	SET 
	ShopChargesPartsPrice = 0
	,ShopChargesLaborPrice = 0
	,TaxIncludedPartsPrice = 0
	,TaxIncludedLaborPrice = 0
	FROM @ServicesTable st


	-- RETURN distinct list of all services
	--select *
	--from @ServicesTable
	--ORDER BY IsLOF DESC, FullDescription ASC
	IF (@UseOEMMenus = 1)
	BEGIN 
		select distinct *
		from @ServicesTable
		where (@ServiceIds is null or @ServiceIds = '' or exists (
				select *
				from @ServiceIdsTable
				where ServiceId = Id and ServiceOriginTableId = ServiceType))
		order by IsLOF desc, FullDescription 
	END
	ELSE 
		BEGIN
			select distinct *
			from @ServicesTable
			where (@ServiceIds is null or @ServiceIds = '' or exists (
					select *
					from @ServiceIdsTable
					where ServiceId = Id and ServiceOriginTableId = ServiceType))
			order by FullDescription asc,ServiceId asc
		END
END