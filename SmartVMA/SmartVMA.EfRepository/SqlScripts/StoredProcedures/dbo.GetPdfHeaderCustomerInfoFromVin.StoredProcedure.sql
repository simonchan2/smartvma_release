USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetPdfHeaderCustomerInfoFromVin]    Script Date: 10/5/2016 5:46:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPdfHeaderCustomerInfoFromVin]
	@TenantId INT,
	@vin nvarchar(25)
AS
BEGIN
	SELECT TOP 1 CAST(dv.VehicleYear AS nvarchar(10)) AS VehicleYear, dv.VehicleMake, dv.VehicleModel, dv.VehicleColor, dv.VehicleLicense AS VehicleLicenseNumber, dv.VIN,
	LTRIM(RTRIM(COALESCE(dc.FirstName + ' ' + dc.LastName , dc.Company))) as CustomerName,
	LTRIM(RTRIM(dc.AddressLine1 + ' ' + dc.City + ' ' + dc.Zip)) as CustomerAddress,
	dc.CellPhone as CustomerMobilePhone, dc.WorkPhone as CustomerWorkPhone, dc.HomePhone as CustomerHomePhone
	FROM dealer.DealerVehicles dv
	LEFT JOIN dealer.DealerCustomers dc ON dv.DealerCustomerId = dc.DealerCustomerId
	WHERE VIN = @vin
END