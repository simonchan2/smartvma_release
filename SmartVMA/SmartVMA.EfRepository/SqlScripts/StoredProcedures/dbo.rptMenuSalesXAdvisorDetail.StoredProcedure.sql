USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[rptMenuSalesXAdvisorDetail]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rptMenuSalesXAdvisorDetail]
@AdvisorCode		nvarchar(45),
@Content			varchar(255),
@DealerCompanyId	int,
@StartDate			datetime,
@EndDate			datetime,
@Level				int=0, -- 0=All, 1=Level 1, 2=Level 2, 3=Level 3
@CPOnly				bit=0,
@ExclLabor0			bit=0,
@UserOnly			bit=0
AS
BEGIN
    SET NOCOUNT ON;

select distinct 
	i.InvoiceDate,	
	i.invoicenumber,
	(dc.FirstName + ' ' + dc.LastName) CustomerName,
	i.Mileage,
	isnull(dv.VehicleMake, '') + ' ' + isnull(dv.VehicleModel, '') + ' ' + isnull(cast(dv.VehicleYear as varchar),'') CarDesc,
	finalMileage = dbo.udf_GetMileage(i.Mileage),	
	CASE @Content		 
		WHEN 'QTY' THEN (PartQty)
		WHEN 'Labor$' THEN ([LaborSale])
		WHEN 'Part$' THEN (d.[PartSale])
		WHEN 'LaborParts$' THEN ([LaborSale] + d.[PartSale]) 
		WHEN 'Hours' THEN (d.[LaborHour])
		WHEN 'ELR' THEN ([LaborSale]/d.[LaborHour])		
		WHEN 'GPLabor' THEN (d.[LaborSale] - d.[LaborCost])
		WHEN 'GPParts' THEN (d.[PartSale] - d.[PartCost])
		WHEN 'GPLaborParts' THEN (d.[LaborSale] - d.[LaborCost] + d.[PartSale] - d.[PartCost])						
	END as content		
into #temp1
from [dealer].[Invoices] i (nolock)
	join [dealer].[DealerDmsUsers] u (nolock)on u.DmsUserNumber = i.AdvisorCode
	join [dealer].[AppointmentPresentations] ap on u.UserId = ap.[AdvisorId] 
	join [dealer].[InvoiceDetails] d (nolock)on d.InvoiceId = i.InvoiceId
	join [dealer].[InvoiceDetailParts] p (nolock)on p.InvoiceDetailId = d.InvoiceDetailId
	join [dealer].[DealerCustomers] dc (nolock)on dc.[DealerCustomerId] = i.DealerCustomerId
	join [dealer].[DealerVehicles] dv (nolock)on dv.DealerVehicleId = i.DealerVehicleId
	join [dbo].[Vehicles] v (nolock)on v.[VehicleId] = dv.VehicleId
where 	
	(@DealerCompanyId=0 or (@DealerCompanyId<>0 and i.DealerId = @DealerCompanyId)) and
	i.InvoiceDate between @StartDate and @EndDate and
	(@ExclLabor0 = 0 or (@ExclLabor0 = 1 and d.LaborHour = 0)) and
	(@UserOnly = 0 or (@UserOnly = 1 and u.DmsUserName is not null)) and
	(@CPOnly = 0 or (@CPOnly=1 and d.[LaborType] = 'CP')) 
	and (@Level = 0 or (@Level != 0 and MenuLevel=@Level) )	
	and i.AdvisorCode = @AdvisorCode
--select * from #temp1 order by InvoiceNumber

select *, 1 sort 
into #temp3
from #temp1 t
pivot (sum(content) for [finalMileage] in ([5000],[10000],[15000],[20000],[25000],[30000],[35000],[40000],[45000],[50000],[55000],[60000],[65000],[70000],[75000],[80000],[85000],[90000],[95000],[100000],[105000],[110000],[115000],[120000],[125000],[130000],[135000],[140000],[145000],[150000],[155000],[160000],[165000],[170000],[175000],[180000],[185000],[190000],[195000],[200000])) as [Mileage]

select * from #temp3

END





GO
