USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetVehicleModels]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetVehicleModels] 
	@Year NVARCHAR(max) = null,
	@Make NVARCHAR(max) = null
AS
BEGIN
	IF(@Year IS NOT NULL AND @Make IS NOT NULL)
	BEGIN
	declare @Delimiter nvarchar(8);
	set @Delimiter =',';

		select  distinct vm.VehicleModelId as Id, vm.VehicleModel as Model from amam.Cars as car
		INNER JOIN amam.VehicleModels as vm on car.VehicleModelId = vm.VehicleModelId
		INNER JOIN dbo.SplitDelimiterString(@Year, @Delimiter) as years ON (years.Item = CAST(ISNULL(car.VehicleYear,null) as NVARCHAR(50)))
		where CAST(ISNULL(car.VehicleMakeId,null) as NVARCHAR(50)) in (select * from dbo.SplitDelimiterString(@Make, @Delimiter))
	END
	ELSE
	BEGIN
		select VehicleModelId as Id, VehicleModel as Model from amam.VehicleModels
	END
END
