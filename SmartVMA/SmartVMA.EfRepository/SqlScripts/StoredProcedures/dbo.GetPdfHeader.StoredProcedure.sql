USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetPdfHeader]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPdfHeader]
	@TenantId INT,
	@AppointmentPresentationId BIGINT,
	@MenuMileage INT = null
AS
BEGIN
	DECLARE @CompanyLogo NVARCHAR(MAX)
	DECLARE @CompanyPhone NVARCHAR(MAX)
	DECLARE @CompanyWeb NVARCHAR(MAX)
	DECLARE @MenuLevel NVARCHAR(MAX) 
	DECLARE @MenuPrefix NVARCHAR(MAX)
	DECLARE @MenuSuffix NVARCHAR(MAX)
	DECLARE @MenuMileageUnit NVARCHAR(MAX)
	
	SET @CompanyLogo = 
	(SELECT TOP 1 Value FROM CompanySettings
	WHERE CompanyId = @TenantId AND Name='CompanyLogo')
	SET @CompanyPhone = 
	(SELECT TOP 1 Value FROM CompanySettings
	WHERE CompanyId = @TenantId AND Name='PhoneNumber')
	SET @CompanyWeb = 
	(SELECT TOP 1 Value FROM CompanySettings
	WHERE CompanyId = @TenantId AND Name='Web')
	IF @MenuMileage IS NULL
	BEGIN
	SET @MenuMileage = 
	(SELECT TOP 1 MenuMileage FROM dealer.AppointmentPresentations
	WHERE DealerId = @TenantId AND AppointmentPresentationId = @AppointmentPresentationId)
	END
	SET @MenuLevel = 
	(SELECT TOP 1 MenuLevel FROM dealer.AppointmentPresentations
	WHERE DealerId = @TenantId AND AppointmentPresentationId = @AppointmentPresentationId)

	IF @MenuLevel = '1' 
	BEGIN
		SELECT @MenuPrefix = 
		(SELECT TOP 1 Value FROM CompanySettings
		WHERE CompanyId = @TenantId AND Name='MenuPrefixLevel1')

		SELECT @MenuSuffix = 
		(SELECT TOP 1 Value FROM CompanySettings
		WHERE CompanyId = @TenantId AND Name='MenuSuffixLevel1')
	END
	ELSE IF @MenuLevel = '2' 
	BEGIN
		SELECT @MenuPrefix = 
		(SELECT TOP 1 Value FROM CompanySettings
		WHERE CompanyId = @TenantId AND Name='MenuPrefixLevel2')

		SELECT @MenuSuffix = 
		(SELECT TOP 1 Value FROM CompanySettings
		WHERE CompanyId = @TenantId AND Name='MenuSuffixLevel2')
	END
	ELSE IF @MenuLevel = '3'
	BEGIN
		SELECT @MenuPrefix = 
		(SELECT TOP 1 Value FROM CompanySettings
		WHERE CompanyId = @TenantId AND Name='MenuPrefixLevel3')

		SELECT @MenuSuffix = 
		(SELECT TOP 1 Value FROM CompanySettings
		WHERE CompanyId = @TenantId AND Name='MenuSuffixLevel3')
	END

	SELECT @MenuMileageUnit = 
		(SELECT TOP 1 Value FROM CompanySettings
		WHERE CompanyId = @TenantId AND Name='MeasurementUnit')

	IF(ISNULL(@MenuMileageUnit, '') = 1)
	BEGIN
		SET @MenuMileageUnit = ' km'
	END
	ELSE
	BEGIN
		SET @MenuMileageUnit = ' miles'
	END

	DECLARE @MenuMileageStr NVARCHAR(MAX)
	SET @MenuMileageStr = CASE WHEN @MenuMileage IS NULL THEN '' ELSE CASE WHEN @MenuMileage < 1000 THEN '' ELSE CAST((@MenuMileage/1000) as NVARCHAR(MAX)) END END
	 
	SELECT @CompanyLogo AS LogoImg, c.CompanyName, c.CompanyId,
	c.Zip + ' ' + c.City + ' ' + c.AddressLine1 + ' ' + c.AddressLine2 AS CompanyAddress, 
	@CompanyPhone AS CompanyPhone, @CompanyWeb AS CompanyWebSite, dv.VIN,
	dc.FirstName + ' ' + dc.LastName AS Customer,
	dv.VehicleMake AS Make, dv.VehicleModel AS Model, dv.VehicleColor AS Color, dv.VehicleYear AS [Year],
	ap.Mileage AS Odometer, ap.Waiter, ap.InvoiceNumber AS Ro, '' AS TagNo, ap.DateRoCreated AS OpenDate,
	u.FirstName + ' ' + u.LastName AS Advisor,
	ISNULL(@MenuPrefix, '') + ' ' + ISNULL(CAST(@MenuMileage as NVARCHAR(MAX)), '') + @MenuMileageUnit AS DocTitle,
	@MenuMileageStr + ISNULL(@MenuSuffix, '') AS DocSubTitle
	
	FROM Companies c
	INNER JOIN dealer.AppointmentPresentations ap ON ap.AppointmentPresentationId = @AppointmentPresentationId
	INNER JOIN dealer.DealerVehicles dv ON ap.DealerVehicleId = dv.DealerVehicleId
	INNER JOIN dealer.DealerCustomers dc ON ap.DealerCustomerId = dc.DealerCustomerId
	LEFT JOIN Users u ON ap.AdvisorId = u.UserId
	WHERE CompanyId = @TenantId
END
