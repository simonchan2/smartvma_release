USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetProtectionPlanLevels]    Script Date: 11.11.2016 09:57:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetProtectionPlanLevels]
	@TenantId int
AS
BEGIN
	DECLARE @MeasurementUnitId int
		set @MeasurementUnitId = (select top 1 CAST(value as int) from CompanySettings where CompanyId = @TenantId and Name = 'MeasurementUnit');

	SELECT DISTINCT ppl.BgProtectionPlanId, CAST(ppl.PlanLevel as int) as ProtectionPlanLevel, pplm.MaxMileageBeforeFirstService
	FROM bg.BgProtectionPlanLevels ppl
	INNER JOIN bg.BgProtectionPlanLevelMus pplm on ppl.BgProtectionPlanLevelId = pplm.BgProtectionPlanLevelId
	WHERE pplm.MeasurementUnitId = @MeasurementUnitId
	

END

GO
