USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[rptMenuActivityMain]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[rptMenuActivityMain]
@Advisor	nvarchar(45),
@StartDate  DATETIME,
@EndDate    DATETIME,
@CPOnly				bit=0,
@UserOnly			bit=0,
@ExclLabor0			bit=0
AS 
BEGIN

if (@Advisor = 'Consolidated') 
begin
	select @Advisor AdvisorCode, @Advisor AdvisorName, 1 sort
end
else
if (@Advisor = 'AllAdvisors') 
begin
	select @Advisor AdvisorCode,  @Advisor AdvisorName, 1 sort
	union
	select distinct 
       dmsu.[DmsUserNumber] AdvisorCode,  dmsu.[DmsUserName] AdvisorName, 0 sort
	from [dealer].[DealerDmsUsers] dmsu (nolock)
		join [dealer].[AppointmentPresentations] ap (nolock) on dmsu.UserId  = ap.[AdvisorId] 
		left join [dealer].[Invoices] i (nolock) on i.InvoiceNumber = ap.InvoiceNumber
		left join [dealer].[InvoiceDetails] d (nolock)on d.InvoiceId = i.InvoiceId
		where [AppointmentTime] between @StartDate and @EndDate and
		(@ExclLabor0 = 0 or (@ExclLabor0 = 1 and d.LaborHour = 0)) and
		(@UserOnly = 0 or (@UserOnly = 1 and dmsu.DmsUserName is not null)) and
		(@CPOnly = 0 or (@CPOnly=1 and d.[LaborType] = 'CP'))
		order by sort desc
end
else
begin
	select distinct 
		   dmsu.[DmsUserNumber] AdvisorCode,  dmsu.[DmsUserName] AdvisorName, 1 sort           
	from [dealer].[DealerDmsUsers] dmsu
	join [dealer].[AppointmentPresentations] ap on dmsu.UserId  = ap.[AdvisorId] 
	left join [dealer].[Invoices] i (nolock) on i.InvoiceNumber = ap.InvoiceNumber
	left join [dealer].[InvoiceDetails] d (nolock)on d.InvoiceId = i.InvoiceId
	where	[AppointmentTime] between @StartDate and @EndDate and
			dmsu.DmsUserNumber =@Advisor and
			(@ExclLabor0 = 0 or (@ExclLabor0 = 1 and d.LaborHour = 0)) and
			(@UserOnly = 0 or (@UserOnly = 1 and dmsu.DmsUserName is not null)) and
			(@CPOnly = 0 or (@CPOnly=1 and d.[LaborType] = 'CP'))
	end

end






GO
