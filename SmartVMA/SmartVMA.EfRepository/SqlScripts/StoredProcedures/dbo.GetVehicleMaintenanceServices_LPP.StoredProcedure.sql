﻿ALTER PROCEDURE [dbo].[GetVehicleMaintenanceServices]
	@TenantId int,
	@Vin nvarchar(25)
AS
BEGIN
	DECLARE @AverageMilesPerYear int
	SET @AverageMilesPerYear = CAST(ISNULL((SELECT Value FROM dbo.CompanySettings where CompanyId = @TenantId and Name = 'AverageMilesPerYear'),0) as int)

	SELECT DISTINCT i.InvoiceId as AppointmentPresentationId,
		i.DealerCustomerId,
		i.Mileage, 
		i.InvoiceNumber,
		i.InvoiceDate as AppointmentTime,
		dv.VIN,
		id.OpDescription,
		id.OpCode,
		CONVERT(bit, 0) as IsStriked,
		ads.BgProtectionPlanId,
		bgc.Description as BgProductCategory,
		ads.IntervalRepeat as ServiceInterval,
		i.Mileage + ads.IntervalRepeat as ExpectedMileage,
		@AverageMilesPerYear as AverageMilesPerYear
	FROM dealer.Invoices i
	INNER JOIN dealer.DealerVehicles dv ON i.DealerVehicleId = dv.DealerVehicleId
	INNER JOIN dealer.InvoiceDetails id ON i.InvoiceId = id.InvoiceId
	LEFT OUTER JOIN dealer.AppointmentServices aps ON id.OpCode = aps.OpCode and aps.DealerId = @TenantId
	LEFT OUTER JOIN dealer.AdditionalServices ads on aps.OrigServiceId = ads.AdditionalServiceId AND aps.ServiceTypeId = 2
	LEFT OUTER JOIN bg.BgProdCategories bgc on ads.BgProdCategoryId = bgc.BgProdCategoryId
	WHERE dv.VIN = @Vin AND i.DealerId = @TenantId
	UNION ALL
	SELECT DISTINCT ap.AppointmentPresentationId,
		ap.DealerCustomerId,
		ap.Mileage, 
		ap.InvoiceNumber,
		case ap.PresentationStatus 
		when 'MK' then ap.ParkedTime 
		when 'MC' then ap.DateRoCreated 
		when 'MD' then ap.DateRoCreated 
		else ap.AppointmentTime end as AppointmentTime,
		dv.VIN,
		aps.OpDescription,
		aps.OpCode,
		CONVERT(bit, 1) as IsStriked,
		null as BgProtectionPlanId,
		'' as BgProductCategory,
		0 as ServiceInterval,
		0 as ExpectedMileage,
		0 as AverageMilesPerYear
	FROM dealer.AppointmentPresentations ap
	INNER JOIN dealer.DealerVehicles dv ON ap.DealerVehicleId = dv.DealerVehicleId
	INNER JOIN dealer.AppointmentServices aps ON ap.AppointmentPresentationId = aps.AppointmentPresentationId
	WHERE dv.VIN = @Vin AND ap.DealerId = @TenantId AND aps.IsStriked = 1
	UNION ALL
	SELECT DISTINCT ap.AppointmentPresentationId,
		ap.DealerCustomerId,
		ap.Mileage, 
		ap.InvoiceNumber,
		case ap.PresentationStatus 
		when 'MK' then ap.ParkedTime 
		when 'MC' then ap.DateRoCreated 
		when 'MD' then ap.DateRoCreated 
		else ap.AppointmentTime end as AppointmentTime,
		dv.VIN,
		aps.OpDescription,
		aps.OpCode,
		CONVERT(bit, 1) as IsStriked,
		null as BgProtectionPlanId,
		'' as BgProductCategory,
		0 as ServiceInterval,
		0 as ExpectedMileage,
		0 as AverageMilesPerYear
	FROM dealer.AppointmentPresentations ap
	INNER JOIN dealer.DealerVehicles dv ON ap.DealerVehicleId = dv.DealerVehicleId
	INNER JOIN dealer.AppointmentServices aps ON ap.AppointmentPresentationId = aps.AppointmentPresentationId
	WHERE dv.VIN = @Vin AND ap.DealerId = @TenantId AND ap.PresentationStatus = 'MD'
	ORDER BY AppointmentTime DESC, AppointmentPresentationId, dv.VIN
END
