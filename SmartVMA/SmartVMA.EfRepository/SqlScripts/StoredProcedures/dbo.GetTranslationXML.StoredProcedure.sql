USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetTranslationXML]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetTranslationXML]
	@Language nvarchar(100)
AS
BEGIN

	PRINT '<?xml version="1.0" encoding="utf-8" ?>
	<localizationDictionary culture="' + @Language + '">'

	DECLARE @xmltext nvarchar(max);
	IF @Language = 'en'
		BEGIN 
			Set @xmltext = (SELECT *
							FROM en text
							FOR XML RAW ('text'), ROOT ('texts'))

			PRINT @xmltext
		END
	ELSE IF @Language = 'bg'
		BEGIN 
			Set @xmltext = (SELECT *
							FROM bg text
							FOR XML RAW ('text'), ROOT ('texts'))

			PRINT @xmltext
		END

	PRINT '</localizationDictionary>'
END

GO
