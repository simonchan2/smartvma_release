USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[OverrideRuleGetById]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- EXEC [dbo].[OverrideRuleGetById] 39,1089
ALTER PROCEDURE [dbo].[OverrideRuleGetById]
		@Id BIGINT=null,
		@TenantId INT
AS
BEGIN
DECLARE @Delimiter NVARCHAR(8) = ','

DECLARE @Years NVARCHAR(MAX) 
SELECT @Years = COALESCE(@Years + @Delimiter, '') + CAST(vy.VehicleYear as NVARCHAR(50)) 
FROM amam.VehicleYears vy
INNER JOIN dbo.OverrideRuleYears oyears ON oyears.VehicleYear = vy.VehicleYear
INNER JOIN dealer.OverrideRules orules ON orules.Id = oyears.OverrideRuleId
WHERE oyears.OverrideRuleId = @Id AND orules.DealerId = @TenantId
SELECT @Years as Years

DECLARE @Makes NVARCHAR(MAX) 
SELECT @Makes = COALESCE(@Makes + @Delimiter, '') + CAST(vy.VehicleMakeId as NVARCHAR(50)) 
FROM amam.VehicleMakes vy
INNER JOIN dbo.OverrideRuleMakes oMakes ON oMakes.VehicleMakeId = vy.VehicleMakeId
INNER JOIN dealer.OverrideRules orules ON orules.Id = oMakes.OverrideRuleId
WHERE oMakes.OverrideRuleId = @Id AND orules.DealerId = @TenantId
SELECT @Makes as Makes

DECLARE @Models NVARCHAR(MAX) 
SELECT @Models = COALESCE(@Models + @Delimiter, '') + CAST(vy.VehicleModelId as NVARCHAR(50)) 
FROM amam.VehicleModels vy
INNER JOIN dbo.OverrideRuleModels oModels ON oModels.VehicleModelId = vy.VehicleModelId
INNER JOIN dealer.OverrideRules orules ON orules.Id = oModels.OverrideRuleId
WHERE oModels.OverrideRuleId = @Id AND orules.DealerId = @TenantId
SELECT @Models as Models

DECLARE @Engines NVARCHAR(MAX) 
SELECT @Engines = COALESCE(@Engines + @Delimiter, '') + CAST(vy.VehicleEngineId as NVARCHAR(50)) 
FROM amam.VehicleEngines vy
INNER JOIN dbo.OverrideRuleEngines oEngines ON oEngines.VehicleEngineId = vy.VehicleEngineId
INNER JOIN dealer.OverrideRules orules ON orules.Id = oEngines.OverrideRuleId
WHERE oEngines.OverrideRuleId = @Id AND orules.DealerId = @TenantId
SELECT @Engines as Engines

DECLARE @Transmissions NVARCHAR(MAX) 
SELECT @Transmissions = COALESCE(@Transmissions + @Delimiter, '') + CAST(vmt.Id as NVARCHAR(50)) 
FROM vinquery.VinMasterTransmission vmt
--INNER JOIN amam.VehicleTransmissions vy ON vy.VehicleTransmissionId = vmt.VehicleTransmissionId
INNER JOIN dbo.OverrideRuleTransmissions oTransmissions ON oTransmissions.VinMasterTransmissionId = vmt.Id
INNER JOIN dealer.OverrideRules orules ON orules.Id = oTransmissions.OverrideRuleId
WHERE oTransmissions.OverrideRuleId = @Id AND orules.DealerId = @TenantId
SELECT @Transmissions as Transmissions


DECLARE @Drivelines NVARCHAR(MAX) 
SELECT @Drivelines = COALESCE(@Drivelines + @Delimiter, '') + CAST(vmd.Id as NVARCHAR(50))
FROM vinquery.VinMasterDriveline vmd
--INNER JOIN amam.VehicleDrivelines vy ON vy.VehicleDrivelineId = vmd.VehicleDrivelineId
INNER JOIN dbo.OverrideRuleDrivelines oDrivelines ON oDrivelines.VinMasterDrivelineId = vmd.Id
INNER JOIN dealer.OverrideRules orules ON orules.Id = oDrivelines.OverrideRuleId
WHERE oDrivelines.OverrideRuleId = @Id AND orules.DealerId = @TenantId
SELECT @Drivelines as Drivelines

SELECT 
Id
,Name
,Keyword
,PartNo
,OpCode
,LaborPricePercentage
,ROUND(LaborPriceAmount, 2, 1) AS LaborPriceAmount
,LaborHourPercentage
,ROUND(LaborHourAmount, 2, 1) AS LaborHourAmount
,PartPricePercentage
,ROUND(PartPriceAmount, 2, 1) AS PartPriceAmount
,OpCodeOverride
,PartNoOverride
FROM dealer.OverrideRules orules
WHERE orules.Id = @Id
AND orules.DealerId = @TenantId

END;


GO
