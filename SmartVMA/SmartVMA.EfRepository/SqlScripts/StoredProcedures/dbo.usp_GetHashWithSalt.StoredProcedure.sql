USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetHashWithSalt]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create procedures section -------------------------------------------------

/*
Author:             Simon Chan
Create date: 2016-05-30
Description: Gets the hashed value (with salt) of a string
Usage:
       DECLARE @RC int;
       DECLARE @Password nvarchar(255);
       DECLARE @PasswordHash varbinary(max);

       SELECT @Password = '123456';

       EXECUTE      @RC = dbo.usp_GetHashWithSalt @Password, @PasswordHash OUTPUT
       
       SELECT @PasswordHash, @RC;
*/
CREATE PROCEDURE [dbo].[usp_GetHashWithSalt]
@String                    nvarchar(255)
, @StringHash       varbinary(MAX) OUTPUT
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
    SET NOCOUNT ON;

--    Get random bytes for salt
    DECLARE @Salt    VARBINARY(4) = CRYPT_GEN_RANDOM(4);            

--    Embedd the salt in the hash output
    SET @StringHash = 0x0200 + @Salt + HASHBYTES('SHA2_512', CAST(ISNULL(@String, '') AS VARBINARY(MAX)) + @Salt);

       RETURN 0;
END


GO
