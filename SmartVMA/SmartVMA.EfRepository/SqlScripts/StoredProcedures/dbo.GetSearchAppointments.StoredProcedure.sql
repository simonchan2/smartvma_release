USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetSearchAppointments]    Script Date: 8/24/2016 3:00:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetSearchAppointments]
	@TenantId int,
	@CustomerName nvarchar(355),
	@CustomerNumber nvarchar(25),
	@CustomerPhone nvarchar(35),
	@VIN nvarchar(25),
	@StockNo nvarchar(50),
	@VehicleLicense nvarchar(15)
AS
BEGIN
	DECLARE @FirstName nvarchar(150)
	DECLARE @LastName nvarchar(150)
	DECLARE @SearchBothNames bit
	SELECT @SearchBothNames = 1

	if (CHARINDEX(', ',@CustomerName) > 0) OR (CHARINDEX(',',@CustomerName) > 0)
	BEGIN
	Select @LastName = SUBSTRING(@CustomerName, 1, CASE WHEN CHARINDEX(',', @CustomerName) > 0 THEN CHARINDEX(',', @CustomerName)-1 ELSE 0 END)
	Select @FirstName = LTRIM(Substring(@CustomerName, LEN(@LastName)+2, LEN(@CustomerName)))
	END
	else if (CHARINDEX(' ',@CustomerName) > 0)
	BEGIN
	Select @FirstName = SUBSTRING(@CustomerName, 1, CASE WHEN CHARINDEX(' ', @CustomerName) > 0 THEN CHARINDEX(' ', @CustomerName)-1 ELSE 0 END)
	Select @LastName = LTRIM(SUBSTRING(@CustomerName, LEN(@FirstName)+1, LEN(@CustomerName)))
	END
	else
	BEGIN
	Select @FirstName = @CustomerName
	Select @LastName = @CustomerName
	SELECT @SearchBothNames = 0
	END

	IF(@SearchBothNames = 1)
	BEGIN
	SELECT * FROM
		(SELECT 
			ac.AppointmentPresentationId,
			ac.VehicleLicense,
			ac.AppointmentTime,
			ac.DealerId,
			ac.DealerVehicleId,
			ac.PresentationStatus,
			ac.AppointmentStatus, 
			ac.AdvisorId,
			ac.VIN,
			ac.Make,
			ac.Model,
			ac.[Year],
			ac.Mileage,
			ac.VehicleTransmission,
			ac.VehicleDriveline,
			dc.FirstName, 
			dc.LastName, 
			dc.CellPhone, 
			dc.HomePhone, 
			dc.WorkPhone, 
			dc.CustomerNumber,
			ac.StockNo,
			ac.CustomerPhone,
			ac.CarId,
			dc.DealerCustomerId,
			u.FirstName as AdvisorName,
			u.LastName as AdvisorFamily,
			ac.AdvisorCode,
			ac.CarIdUndetermined
		FROM AppointmentsCombined ac
		INNER JOIN dealer.DealerCustomers dc 
			ON ac.DealerCustomerId = dc.DealerCustomerId
		LEFT JOIN Users u 
			ON u.UserID = ac.AdvisorId) t
	WHERE t.DealerId = @TenantId
		AND CONVERT(date, t.AppointmentTime) >= CONVERT(date, GETDATE())
		AND (@FirstName IS NULL OR CHARINDEX(ISNULL(@FirstName, t.FirstName),t.FirstName) > 0)
		AND (@LastName IS NULL OR CHARINDEX(ISNULL(@LastName, t.LastName),t.LastName) > 0)
		AND (@CustomerNumber IS NULL OR CHARINDEX(ISNULL(@CustomerNumber, t.CustomerNumber),t.CustomerNumber) > 0)
		AND (@CustomerPhone IS NULL OR CHARINDEX(ISNULL(@CustomerPhone, t.CustomerPhone),t.CustomerPhone) > 0)
		AND (@VIN IS NULL OR CHARINDEX(ISNULL(@VIN, t.VIN),t.VIN) > 0)
		AND (@StockNo IS NULL OR CHARINDEX(ISNULL(@StockNo, t.StockNo),t.StockNo) > 0)
		AND (@VehicleLicense IS NULL OR CHARINDEX(ISNULL(@VehicleLicense, t.VehicleLicense),t.VehicleLicense) > 0)
	END
	ELSE
	BEGIN
	SELECT * FROM
		(SELECT
			ac.AppointmentPresentationId,
			ac.VehicleLicense,
			ac.AppointmentTime,
			ac.DealerId,
			ac.DealerVehicleId,
			ac.PresentationStatus,
			ac.AppointmentStatus, 
			ac.AdvisorId,
			ac.VIN,
			ac.Make,
			ac.Model,
			ac.[Year],
			ac.Mileage,
			ac.VehicleTransmission,
			ac.VehicleDriveline,
			dc.FirstName, 
			dc.LastName, 
			dc.CellPhone, 
			dc.HomePhone, 
			dc.WorkPhone, 
			dc.CustomerNumber,
			ac.StockNo,
			ac.CustomerPhone,
			ac.CarId,
			dc.DealerCustomerId,
			u.FirstName as AdvisorName,
			u.LastName as AdvisorFamily,
			ac.AdvisorCode,
			ac.CarIdUndetermined
		FROM AppointmentsCombined ac
		INNER JOIN dealer.DealerCustomers dc 
			ON ac.DealerCustomerId = dc.DealerCustomerId
		LEFT JOIN Users u 
			ON u.UserID = ac.AdvisorId) t
	WHERE t.DealerId = @TenantId
		AND CONVERT(date, t.AppointmentTime) >= CONVERT(date, GETDATE())
		AND ((@FirstName IS NULL OR CHARINDEX(ISNULL(@FirstName, t.FirstName),t.FirstName) > 0)
		OR (@LastName IS NULL OR CHARINDEX(ISNULL(@LastName, t.LastName),t.LastName) > 0))
		AND (@CustomerNumber IS NULL OR CHARINDEX(ISNULL(@CustomerNumber, t.CustomerNumber),t.CustomerNumber) > 0)
		AND (@CustomerPhone IS NULL OR CHARINDEX(ISNULL(@CustomerPhone, t.CustomerPhone),t.CustomerPhone) > 0)
		AND (@VIN IS NULL OR CHARINDEX(ISNULL(@VIN, t.VIN),t.VIN) > 0)
		AND (@StockNo IS NULL OR CHARINDEX(ISNULL(@StockNo, t.StockNo),t.StockNo) > 0)
		AND (@VehicleLicense IS NULL OR CHARINDEX(ISNULL(@VehicleLicense, t.VehicleLicense),t.VehicleLicense) > 0)
	END
END
