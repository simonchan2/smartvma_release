USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetVehicleInvoiceHistory]    Script Date: 01.11.2016 16:38:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetVehicleInvoiceHistory]
	@DealerId  int,
	@OpCode nvarchar(MAX)
AS

declare @Delimiter nvarchar(8);
	set @Delimiter =',';

declare @OpCodeTable table(Id nvarchar(50) null);
INSERT INTO @OpCodeTable select CAST(ISNULL(Item,null) as nvarchar(50)) as Id FROM dbo.SplitDelimiterString(@OpCode, @Delimiter)

select distinct id.OpCode, i.Mileage from dealer.Invoices i
inner join dealer.InvoiceDetails id on i.InvoiceId = id.InvoiceId
where i.DealerId = @DealerId
and id.OpCode in (select Id from @OpCodeTable)
order by i.Mileage

GO
