USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[SearchCustomer]    Script Date: 21.10.2016 12:28:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SearchCustomer] 
	@TenantId int,
	@CustomerName nvarchar(355) = null,
	@CustomerNumber nvarchar(25) = null,
	@CustomerPhone nvarchar(35) = null,
	@VIN nvarchar(25) = null,
	@StockNo nvarchar(50) = null,
	@VehicleLicense nvarchar(15) = null,
	@ParkedMenuRetentionDuration  int = 3
AS
BEGIN

	DECLARE @FirstName nvarchar(150)
	DECLARE @LastName nvarchar(150)
	DECLARE @LastChangeDate as date
	SET @ParkedMenuRetentionDuration = @ParkedMenuRetentionDuration * (-1);
	SET @LastChangeDate = DATEADD(day,@ParkedMenuRetentionDuration,GetDate())

	if (CHARINDEX(', ',@CustomerName) > 0) OR (CHARINDEX(',',@CustomerName) > 0)
	begin
		Select @FirstName = SUBSTRING(@CustomerName, 1, CASE WHEN CHARINDEX(',', @CustomerName) > 0 THEN CHARINDEX(',', @CustomerName)-1 ELSE 0 END)
		Select @LastName = LTRIM(Substring(@CustomerName, LEN(@FirstName)+2, LEN(@CustomerName)))
	end
	else if (CHARINDEX(' ',@CustomerName) > 0)
	begin
		Select @FirstName = SUBSTRING(@CustomerName, 1, CASE WHEN CHARINDEX(' ', @CustomerName) > 0 THEN CHARINDEX(' ', @CustomerName)-1 ELSE 0 END)
		Select @LastName = LTRIM(SUBSTRING(@CustomerName, LEN(@FirstName)+1, LEN(@CustomerName)))
	end
	else
	begin
		Select @FirstName = @CustomerName
	end

	DECLARE @CustomerFullName nvarchar(150)
	SET @CustomerFullName = LTRIM(RTRIM(ISNULL(@FirstName,'') + ' ' + ISNULL(@LastName,'')))

	SELECT distinct * FROM
		(SELECT 
			dc.DealerCustomerId as CustomerId,
			ISNULL(v.CarId, dv.CarId) as CarId,
			dc.DealerId as DealerId,
			dv.DealerId as DealerVehicleDealerId,
			dv.DealerVehicleId,
			dv.VehicleMake as Make,
			dv.VehicleModel as Model,
			dv.VehicleYear as [Year],
			dv.VehicleDriveline as VehicleDriveline,
			case app.PresentationStatus 
			when 'MK' then app.ParkedTime 
			when 'MC' then app.DateRoCreated 
			when 'MD' then app.DateRoCreated 
			else app.AppointmentTime end as AppointmentTime,
			app.AppointmentPresentationId as AppointmentPresentationId,
			app.PresentationStatus as AppointmentPresentationStatus,
			ISNULL(app.IsLofMenu, 0) as IsLofMenu,
			ISNULL(dc.FirstName, ' ') as FirstName,
			ISNULL(dc.LastName, ' ') as LastName,
			ISNULL(dc.Company, ' ') as Company,
			ISNULL(dc.CustomerNumber, ' ') as CustomerNumber,
			ISNULL(dv.StockNo, ' ') as StockNo,
			ISNULL(dv.VehicleLicense, ' ') as VehicleLicense,
			ISNULL(v.VIN, ISNULL(dv.VIN, ' ')) as VIN,
			ISNULL(dc.CellPhone, ' ') as CustomerPhone,
			ISNULL(u.FirstName + ' ' + u.LastName,'') as AdvisorName,
			LTRIM(RTRIM(ISNULL(dc.FirstName + ' ' + dc.LastName,''))) as CustomerFullName,
			v.CarIdUndetermined as CarIdUndetermined
		FROM dealer.DealerCustomers dc
		LEFT OUTER JOIN dealer.DealerVehicles dv
			ON dc.DealerCustomerId = dv.DealerCustomerId
		LEFT OUTER JOIN dbo.Vehicles v
			ON dv.VehicleId = v.VehicleId
		LEFT OUTER JOIN dealer.AppointmentPresentations app
			ON dc.DealerCustomerId = app.DealerCustomerId
			AND app.DealerVehicleId = dv.DealerVehicleId	
		LEFT OUTER JOIN Users u
			on app.AdvisorId = u.UserId) t
	
	WHERE t.DealerId = @TenantId AND (t.DealerVehicleDealerId IS NULL OR t.DealerVehicleDealerId = @TenantId)
		AND ((@CustomerFullName = '') OR (CHARINDEX(ISNULL(@CustomerFullName,t.CustomerFullName),t.CustomerFullName) > 0))
		AND ((@CustomerNumber is null and t.CustomerNumber = '') OR (CHARINDEX(ISNULL(@CustomerNumber,t.CustomerNumber),t.CustomerNumber) > 0))
		AND ((@CustomerPhone is null and t.CustomerPhone = '') OR (CHARINDEX(ISNULL(@CustomerPhone,t.CustomerPhone),t.CustomerPhone) > 0))
		AND ((@VIN is null and t.VIN = '') OR (CHARINDEX(ISNULL(@VIN,t.VIN),t.VIN) > 0))
		AND ((@StockNo is null and t.StockNo = '') OR (CHARINDEX(ISNULL(@StockNo,t.StockNo),t.StockNo) > 0))
		AND ((@VehicleLicense is null and t.VehicleLicense = '') OR (CHARINDEX(ISNULL(@VehicleLicense,t.VehicleLicense),t.VehicleLicense) > 0))
		AND ((ISNULL(t.AppointmentPresentationStatus,'') = 'MK' AND  CAST(t.AppointmentTime AS DATE) > CAST(@LastChangeDate AS DATE)) OR  ISNULL(t.AppointmentPresentationStatus,'') != 'MK')
		AND ( ISNULL(t.AppointmentPresentationStatus,'') != 'PD')
		ORDER BY t.AppointmentTime DESC
END


