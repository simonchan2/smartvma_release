USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[rptMenuSaleTrendDetail2]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[rptMenuSaleTrendDetail2]
@DealerCompanyId int,
@Advisor	nvarchar(45),
@MonthYear	varchar(20),
@CPOnly		bit=0,
@UserOnly	bit=0,
@ExclLabor0	bit=0
AS 
BEGIN

if (@MonthYear='Total')
begin
	return
end

declare @StartDate datetime, @EndDate datetime
select @StartDate	= cast(@MonthYear as date)
select @EndDate		= dateadd(d,-1, dateadd(m, 1, @StartDate))

--select @StartDate, @EndDate

select distinct 	   		
		i.InvoiceDate,	
		i.invoicenumber,
		(dc.FirstName + ' ' + dc.LastName) CustomerName,
		i.Mileage,
		isnull(dv.VehicleMake, '') + ' ' + isnull(dv.VehicleModel, '') + ' ' + isnull(cast(dv.VehicleYear as varchar),'') CarDesc,
		(case when PresentationStatus='MC'  then 1 else 0 end) MenuSold,
		(MenuLevel) [MenuLevel],                        
		[AppointmentTime],	   
		isnull(p.PartQty,0) PartQty,
		isnull(p.PartSale,0) PartSale,
		isnull(d.LaborSale,0) LaborSale	 
into #Data
from [dealer].[DealerDmsUsers] dmsu
join [dealer].[AppointmentPresentations] ap on dmsu.UserId  = ap.[AdvisorId] 
join [dealer].[DealerVehicles] dv on dv.[DealerVehicleId] = ap.DealerVehicleId
left join dealer.Invoices i on i.InvoiceNumber = ap.InvoiceNumber
join [dealer].[DealerCustomers] dc (nolock)on dc.[DealerCustomerId] = i.DealerCustomerId
left join [dealer].[InvoiceDetails] d (nolock)on d.InvoiceId = i.InvoiceId
left join [dealer].[InvoiceDetailParts] p (nolock)on p.InvoiceDetailId = d.InvoiceDetailId
where	[AppointmentTime] between @StartDate and @EndDate and
		((@Advisor='Consolidated' or @Advisor='AllAdvisors') or (@Advisor!='Consolidated' and @Advisor!='AllAdvisors' and dmsu.DmsUserNumber =@Advisor)) and
		(@ExclLabor0 = 0 or (@ExclLabor0 = 1 and d.LaborHour = 0)) and
		(@UserOnly = 0 or (@UserOnly = 1 and dmsu.DmsUserName is not null)) and
		(@CPOnly = 0 or (@CPOnly=1 and d.[LaborType] = 'CP'))		
		and (@DealerCompanyId=0 or (@DealerCompanyId<>0 and ap.DealerId = @DealerCompanyId))
--select* from #Data
select 	
	max(InvoiceDate) InvoiceDate,	
	invoicenumber,
	max(CustomerName) CustomerName,
	max(Mileage) Mileage,
	max(CarDesc) CarDesc,
	AppointmentTime,		
	sum(PartQty) PartQty,
	sum(PartSale) PartSale,
	sum(LaborSale) LaborSale,
	sum(PartSale) + sum(LaborSale) MenuSold,		
	sum(PartSale) + sum(LaborSale) Amount,	
	sum(PartSale) + sum(LaborSale) Menu$,	
	0	Permenu$,
	0	Pen,
	0	PencentagePresent,
	[MenuLevel] 
 into #Data2
from #Data
group by InvoiceNumber,AppointmentTime, MenuSold, [MenuLevel]

--select * from #Data2

select * 
into #Data3
from
( 
	select *, 'a'+ cast([MenuLevel] as varchar) aMenuLevel, 	
	'm'+ cast([MenuLevel] as varchar) mMenuLevel
	from #Data2
) s
pivot
( sum(PartQty)
		for [MenuLevel] in ([1], [2], [3])
) as p1
pivot
( sum(Amount)
		for [aMenuLevel] in ([a1], [a2], [a3])
) as p2
pivot
( sum(Permenu$)
		for mMenuLevel in ([m1], [m2], [m3])
) as p2

update #Data3
set [1] = isnull([1],0),[2] = isnull([2],0), [3] = isnull([3],0),
	[a1] = isnull([a1],0),[a2] = isnull([a2],0),[a3] = isnull([a3],0)
,[m1] = isnull([m1],0),[m2] = isnull([m2],0),[m3] = isnull([m3],0)

select * from #Data3

END




GO
