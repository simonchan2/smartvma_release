USE [SmartVmaDev]
GO

/****** Object:  StoredProcedure [dbo].[GetVehicleDriveLinesFromVinMaster]    Script Date: 12/12/2016 7:26:57 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[GetVehicleDriveLinesFromVinMaster] 
	@Year nvarchar(max) = null,
    @Make nvarchar(max) = null,
    @Model nvarchar(max) = null,
    @Engine nvarchar(max) = null,
	@Transmission nvarchar(max) = null
AS
BEGIN
	declare @Delimiter nvarchar(8) = ',';
	DECLARE @NA NVARCHAR(5)= 'N/A';

SELECT DISTINCT vmd.Id, vmd.DriveLine
	FROM GetCarIdByCriteria(@Year, @Make, @Model, @Engine, NULL, @Transmission, @Delimiter) c
	INNER JOIN amam.CarVinShorts as cv ON cv.CarId = c.CarId
	INNER JOIN vinquery.VinMaster as vm ON  vm.VinMasterId = cv.VinMasterId
	INNER JOIN vinquery.VinMasterDriveline vmd ON vmd.Id = vm.VinMasterDrivelineId OR vmd.Driveline = @NA
	ORDER BY vmd.DriveLine
END

/*

EXEC [dbo].[GetVehicleDriveLinesFromVinMaster] 
NULL, 
NULL, 
NULL, 
NULL, 
NULL
*/



GO


