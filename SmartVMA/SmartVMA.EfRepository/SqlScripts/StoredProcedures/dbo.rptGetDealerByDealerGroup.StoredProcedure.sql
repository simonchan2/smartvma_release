USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[rptGetDealerByDealerGroup]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[rptGetDealerByDealerGroup]
	@CompanyGroupId int
AS 
BEGIN
	IF (@CompanyGroupId = 0 )
	BEGIN
		select 0 CompanyId, 'All' CompanyName, 0 sort
		union
		select distinct c.CompanyId, CompanyName, 1 sort
		from companies c
		join [dbo].[CompanyCompanyGroups] ccg on ccg.CompanyId = c.CompanyId		
		order by sort, CompanyName
	END
	ELSE
	BEGIN
		select distinct c.CompanyId, CompanyName from companies c
		join [dbo].[CompanyCompanyGroups] ccg on ccg.CompanyId = c.CompanyId
		where CompanyGroupId=@CompanyGroupId
		order by CompanyName
	END
END





GO
