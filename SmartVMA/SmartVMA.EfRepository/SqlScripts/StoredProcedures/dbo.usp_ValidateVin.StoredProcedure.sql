USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[usp_ValidateVin]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Return
	0 -- Good Vin
	1 -- Length Error
	2 -- Bad Character
	3 -- Bad Check Digit

Usage
	SELECT dbo.udf_ValidateVin('1FTPX145X4NA75710');
	SELECT dbo.udf_ValidateVin('1FTPX145X4NA757101');	-- Length Error
	SELECT dbo.udf_ValidateVin('1FTPX145X4NA7571');		-- Length Error
	SELECT dbo.udf_ValidateVin('1FTPX145X4NA75711');
	EXEC dbo.usp_ValidateVin '1FTPX145X4NA75710';
	EXEC dbo.usp_ValidateVin '1FTPX145X4NA7571Q';		--	Bad Character

*/

CREATE PROCEDURE [dbo].[usp_ValidateVin] (@Vin VARCHAR(40)) AS
BEGIN
	DECLARE @MyError int;
	SET @MyError = 0 -- Good Vin

	IF (LEN(@Vin) <> 17) OR (@Vin IS NULL)
	BEGIN
		SET @MyError = 1 -- Length Error
	END
	ELSE
	BEGIN
		DECLARE @position AS INTEGER
		DECLARE @checksum AS INTEGER
		DECLARE @weight AS INTEGER
		DECLARE @value AS INTEGER

		SET @position = 1
		SET @checksum = 0

		WHILE (@position <= 17)
		BEGIN
			SELECT @weight = [Weight]
			FROM VinWeights
			WHERE VinWeightId = @position

			SELECT @value = Value
			FROM VinDigits
			WHERE Digit = SUBSTRING(@Vin, @position, 1);

			IF @@ROWCOUNT = 0
			BEGIN
				SET @MyError = 2 -- Bad Character;
				SET @position = 20;
			END
			ELSE
			BEGIN
				SET @checksum = @checksum + (@value * @weight);
				--SELECT @position, @value, @weight, @checksum;
				SET @position = @position + 1;
			END
		END

		IF @MyError = 0
		BEGIN
			SET @checksum = @checksum % 11
			IF @checksum = 10
			BEGIN
				IF SUBSTRING(@Vin, 9, 1) <> 'X'
				SET @MyError = 3 -- Bad Check Digit
			END
			ELSE
			BEGIN
				IF SUBSTRING(@Vin, 9, 1) <> CAST(@checksum AS CHAR(1))
				SET @MyError = 3 -- Bad Check Digit
			END
		END
	END
	SELECT @MyError as Result
END

GO
