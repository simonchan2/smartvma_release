USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetOemBasicServicesForMaintenance]    Script Date: 12/12/2016 7:25:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetOemBasicServicesForMaintenance]
	@Years nvarchar(max),
	@Makes nvarchar(max),
	@Models nvarchar(max),
	@EngineTypes nvarchar(max),
	@TransmissionTypes nvarchar(max),
	@DriveLines nvarchar(max),
	@PageSize int,
	@CurrentPage int,
	@OrderExpression nvarchar(500),
	@SearchKeyword  nvarchar(500)
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @Delimiter nvarchar(8);
	set @Delimiter =',';
	SET @SearchKeyword = (SELECT dbo.Trim(@SearchKeyword));
	-----------------------------------------------------------------------
	declare @CarIds table(Id int null);
	INSERT INTO @CarIds
	select distinct cars.CarId
	from GetCarIdByCriteria(@Years, @Makes, @Models, @EngineTypes, @DriveLines, @TransmissionTypes, @Delimiter) cars

	declare @AllSelectedServicetable table(Id int null, OrderId INT IDENTITY(1, 1) primary key);
	declare @FilteredBasicServices table(OemBasicServiceId bigint);
	
	;WITH Services AS(
	SELECT obs.OemBasicServiceId
	FROM amam.OemBasicServices obs
	INNER JOIN @CarIds c ON c.Id = obs.CarId
	WHERE obs.IsIncomplete = 0 AND obs.IsDeleted = 0
	),
	FilteredBasicServices AS(
		SELECT DISTINCT x.OemBasicServiceId
		FROM (
		SELECT obs.OemBasicServiceId
		FROM Services obs 
		INNER JOIN amam.OemServiceParts osp ON osp.OemBasicServiceId = obs.OemBasicServiceId AND osp.IsDeleted = 0
		INNER JOIN amam.OemParts op ON osp.OemPartId = op.OemPartId
		LEFT JOIN amam.OemPartNumbers opn ON opn.OemPartId = op.OemPartId
		WHERE
		((@SearchKeyword IS NULL OR @SearchKeyword='')
		OR opn.OemPartNo LIKE N'%' + @SearchKeyword + '%')

		UNION

		SELECT obs.OemBasicServiceId
		FROM Services AS obs
		INNER JOIN amam.OemBasicServices AS robs ON robs.OemBasicServiceId = obs.OemBasicServiceId
		INNER JOIN amam.OemComponents AS component ON robs.OemComponentId = component.OemComponentId
		WHERE 
		((@SearchKeyword IS NULL OR @SearchKeyword='')
			OR robs.OpDescription LIKE N'%' + @SearchKeyword + '%'
			OR robs.OpAction LIKE N'%' + @SearchKeyword + '%'
			OR component.[Description] LIKE N'%' + @SearchKeyword + '%')
		) as x
	)
	INSERT INTO @AllSelectedServicetable
	SELECT fbs.OemBasicServiceId
	FROM FilteredBasicServices fbs
	inner join amam.OemBasicServices as service on service.OemBasicServiceId = fbs.OemBasicServiceId
	inner join amam.OemComponents as component on service.OemComponentId = component.OemComponentId
	order by
		case when @OrderExpression = 'OemComponentId asc' then component.Description end asc,
		case when @OrderExpression = 'OemComponentId desc' then component.Description end desc,
		case when @OrderExpression = 'OpDescription asc' then service.OpDescription end asc,
		case when @OrderExpression = 'OpDescription desc' then service.OpDescription end desc,
		case when @OrderExpression = 'LaborHour asc' then service.LaborHour end asc,
		case when @OrderExpression = 'LaborHour desc' then service.LaborHour end desc

	declare @SelectedServiceTable table(Id bigint null);
	insert into @SelectedServiceTable
	select Id
	from @AllSelectedServicetable
	order by OrderId
	offset (@PageSize * @CurrentPage) rows
	FETCH NEXT @PageSize ROWS ONLY

	DECLARE @Mileages TABLE (RowID BIGINT, Id BIGINT, Mileage NVARCHAR(50), MeasurementUnitId INT)
	INSERT INTO @Mileages
	SELECT
		ROW_NUMBER() OVER (PARTITION BY Id ORDER BY Mileage) AS RowID,
		sst.Id,
		CAST(ISNULL(omm.Mileage, '') AS NVARCHAR(MAX)) AS Mileage,
		MeasurementUnitId
	FROM @SelectedServiceTable sst
	INNER JOIN amam.OemServices AS os ON os.OemBasicServiceId = sst.Id
	INNER JOIN amam.OemMenus AS om ON om.OemMenuId = os.OemMenuId
	INNER JOIN amam.OemMenuMileages AS omm ON omm.OemMenuId = om.OemMenuId

	--set nocount off
	;WITH Mileages AS
	(SELECT
		t1.Id
			,STUFF(
					(SELECT
						', ' + t2.Mileage
						FROM @Mileages t2
						WHERE t1.Id=t2.Id
						AND t2.MeasurementUnitId = 0
						ORDER BY t2.Mileage
						FOR XML PATH(''), TYPE
					).value('.','varchar(max)')
					,1,2, ''
				  ) AS MileagesM
			,STUFF(
				(SELECT
					', ' + t2.Mileage
					FROM @Mileages t2
					WHERE t1.Id=t2.Id
					AND t2.MeasurementUnitId = 1
					ORDER BY t2.Mileage
					FOR XML PATH(''), TYPE
				).value('.','varchar(max)')
				,1,2, ''
			) AS MileagesKM
		FROM @Mileages t1
		GROUP BY t1.Id)
		select  service.*, component.*,m.MileagesM,m.MileagesKM
	FROM amam.OemBasicServices AS service
	INNER JOIN amam.OemComponents AS component ON service.OemComponentId = component.OemComponentId
	LEFT JOIN Mileages m ON m.Id = service.OemBasicServiceId
	WHERE service.OemBasicServiceId in (SELECT * FROM @SelectedServiceTable) AND service.IsDeleted = 0 and service.IsIncomplete = 0;

	select servicePart.*, part.*, components.IsFluid
	from amam.OemServiceParts as servicePart
		inner join amam.OemParts as part on servicePart.OemPartId = part.OemPartId
		inner join amam.OemComponents as components on components.OemComponentId = part.OemComponentId
	where servicePart.OemBasicServiceId in (select * from @SelectedServiceTable) AND servicePart.IsDeleted = 0;

	select partNumbers.*
	from amam.OemServiceParts as servicePart
		inner join amam.OemParts as part on servicePart.OemPartId = part.OemPartId
		inner join amam.OemPartNumbers as partNumbers ON partNumbers.OemPartId = part.OemPartId
	where servicePart.OemBasicServiceId in  (select * from @SelectedServiceTable) AND servicePart.IsDeleted = 0
	ORDER BY OemPartNo;

	select  count(*)
	from @AllSelectedServicetable

	SELECT [Id]
	FROm @AllSelectedServicetable
END
