USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [vi].[InspectionAddSet]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Boris Kamenov
-- Create date: 22.06.2016
-- Description:	InspectionAddSet
-- =============================================
CREATE PROCEDURE [vi].[InspectionAddSet] --3, 4, 1, 0, 0, 0, 0, NULL, NULL, NULL, NULL, 51, 1
	@Id bigint,
	@AppointmentPresentationId bigint,
	@TenantId bigint,
	@LFRimScratch bit,
	@LRRimScratch bit,
	@RFRimScratch bit,
	@RRRimScratch bit,
	@LFTireTypeId int,
	@LRTireTypeId int,
	@RFTireTypeId int,
	@RRTireTypeId int,
	@Fuel int
AS
BEGIN

if @Id is null
	begin
	insert into dealer.[Inspection] 
	(LFRimScratch, LRRimScratch, RFRimScratch, RRRimScratch, LFTireTypeId, LRTireTypeId, RFTireTypeId, RRTireTypeId, Fuel, TimeCreated, TimeUpdated, AppointmentPresentationId, DealerId)
	values (@LFRimScratch, @LRRimScratch, @RFRimScratch, @RRRimScratch, @LFTireTypeId, @LRTireTypeId, @RFTireTypeId, @RRTireTypeId, @Fuel, GETUTCDATE(), GETUTCDATE(), @AppointmentPresentationId, @TenantId)
	select  @@identity
	end
else
	begin
	UPDATE i
	SET 
	LFRimScratch = @LFRimScratch,
	LRRimScratch = @LRRimScratch,
	RFRimScratch = @RFRimScratch,
	RRRimScratch = @RRRimScratch,
	LFTireTypeId = @LFTireTypeId,
	LRTireTypeId = @LRTireTypeId,
	RFTireTypeId = @RFTireTypeId,
	RRTireTypeId = @RRTireTypeId,
	Fuel = @Fuel,
	TimeUpdated = GETUTCDATE()
	FROM dealer.Inspection i
	WHERE i.Id = @Id
	AND i.DealerId = @TenantId

	select  @Id
	end
END



GO
