USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetPdfHeader]    Script Date: 10/5/2016 5:46:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPdfHeaderCustomerInfo]
	@TenantId INT,
	@AppointmentPresentationId BIGINT
AS
BEGIN
	SELECT TOP 1
	COALESCE(NULLIF(dc.FullName,''), dc.Company) AS CustomerName,
	dv.VehicleMake, dv.VehicleYear, dv.VehicleModel, dv.VehicleColor,
	dv.VIN, ap.Mileage AS Odometer, ap.InvoiceNumber, ap.TimeCreated AS [Date],
	ISNULL(ap.MenuLevel, 1) AS MenuLevel, ISNULL(ap.IsLofMenu, 0) AS IsLofMenu 
	FROM 
	dealer.AppointmentPresentations ap 
	INNER JOIN dealer.DealerVehicles dv ON ap.DealerVehicleId = dv.DealerVehicleId
	INNER JOIN dealer.DealerCustomers dc ON ap.DealerCustomerId = dc.DealerCustomerId
	WHERE ap.DealerId = @TenantId AND ap.AppointmentPresentationId = @AppointmentPresentationId
END
