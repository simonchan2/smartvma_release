USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetCalendarAppointments]    Script Date: 9/30/2016 5:06:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetCalendarAppointments]
	@TenantId int,
	@AppointmentStartTime datetime,
	@AppointmentEndTime datetime,
	--AdvisorID -1 means no advisor assigned; AdvisorID is NULL
	@AdvisorIDs nvarchar(max),
	--@PresentationStatus values are: 0 All, 1 Scheduled and 2 Completed.
	@PresentationStatus nvarchar(10)
AS
BEGIN
	--Scheduled – is today, or in the future and has not been completed.
	--Any appointments for past days (yesterday and onward) that did not get completed, should no longer show as scheduled.
	--Any appointments today or in the future that have an RO or a sold menu, should not show as scheduled.
	IF(@PresentationStatus = '1')
	BEGIN
		SELECT * FROM
			(SELECT 
				ac.AppointmentPresentationId,
				ac.AppointmentID,
				ac.VehicleLicense,
				ac.AppointmentTime,
				DATEADD(mi, DATEDIFF(mi, 0, ISNULL(ac.AppointmentTime, 0))/30*30, 0) as start,
				FORMAT(DATEADD(mi, DATEDIFF(mi, 0, ISNULL(ac.AppointmentTime, 0))/30*30, 0), 'HHmm') as startHHmm,
				ac.DealerId,
				ac.DealerCustomerId,
				ac.PresentationStatus,
				ac.AppointmentStatus, 
				ac.AdvisorId,
				ac.VIN,				
				ac.Make,
				ac.Model,
				ac.[Year],
				ac.VehicleEngine as Engine,
				ac.VehicleTransmission as Transmission,
				ac.VehicleDriveline  as DriveLine,
				ac.RoNumber,
				dc.FirstName, 
				dc.LastName, 
				dc.CellPhone, 
				dc.HomePhone, 
				dc.WorkPhone, 
				dc.CustomerNumber,
				ac.StockNo,
				ac.CarId,
				ac.DealerVehicleId,
				u.FirstName as AdvisorName,
				u.LastName as AdvisorFamily,
				ac.CarIdUndetermined as CarIdUndetermined,
				ac.Mileage
			FROM AppointmentsCombined ac
			INNER JOIN dealer.DealerCustomers dc ON ac.DealerCustomerId = dc.DealerCustomerId
			LEFT JOIN dbo.Users u ON u.UserId = ac.AdvisorId) t
		WHERE t.DealerId = @TenantId
			AND t.AppointmentTime > @AppointmentStartTime 
			AND t.AppointmentTime < @AppointmentEndTime
			AND (t.PresentationStatus != 'MC' AND isnull(t.RoNumber, '') = '')
			AND CONVERT(date, t.AppointmentTime) >= CONVERT(date, GETDATE())
			AND Charindex(','+cast(ISNULL(t.AdvisorId, -1) as varchar(8000))+',', @AdvisorIDs) > 0
		ORDER BY t.AppointmentTime
	END
	--Completed – Has an RO number or a Sold Menu
	ELSE IF(@PresentationStatus = '2')
	BEGIN
		SELECT * FROM
			(SELECT 
				ac.AppointmentPresentationId,
				ac.AppointmentID,
				ac.VehicleLicense,
				ac.AppointmentTime,
				DATEADD(mi, DATEDIFF(mi, 0, ISNULL(ac.AppointmentTime, 0))/30*30, 0) as start,
				FORMAT(DATEADD(mi, DATEDIFF(mi, 0, ISNULL(ac.AppointmentTime, 0))/30*30, 0), 'HHmm') as startHHmm,
				ac.DealerId,
				ac.DealerCustomerId,
				ac.PresentationStatus,
				ac.AppointmentStatus, 
				ac.AdvisorId,
				ac.VIN,				
				ac.Make,
				ac.Model,
				ac.[Year],
				ac.VehicleEngine  as Engine,
				ac.VehicleTransmission as Transmission,
				ac.VehicleDriveline  as DriveLine,
				ac.RoNumber,
				dc.FirstName, 
				dc.LastName, 
				dc.CellPhone, 
				dc.HomePhone, 
				dc.WorkPhone, 
				dc.CustomerNumber,
				ac.StockNo,
				ac.CarId,
				ac.DealerVehicleId,
				u.FirstName as AdvisorName,
				u.LastName as AdvisorFamily,
				ac.CarIdUndetermined as CarIdUndetermined,
				ac.Mileage
			FROM AppointmentsCombined ac
			INNER JOIN dealer.DealerCustomers dc ON ac.DealerCustomerId = dc.DealerCustomerId
			LEFT JOIN dbo.Users u ON u.UserId = ac.AdvisorId) t
		WHERE t.DealerId = @TenantId
			AND t.AppointmentTime > @AppointmentStartTime 
			AND t.AppointmentTime < @AppointmentEndTime
			AND (t.PresentationStatus = 'MC' OR isnull(t.RoNumber, '') != '')
			AND Charindex(','+cast(ISNULL(t.AdvisorId, -1) as varchar(8000))+',', @AdvisorIDs) > 0
		ORDER BY t.AppointmentTime
	END
	--All: 
	--for appointments in the past: All = Completed.
	--For appointments in the future: All = Completed + Scheduled
	ELSE
	BEGIN
		SELECT * FROM
			(SELECT 
				ac.AppointmentPresentationId,
				ac.AppointmentID,
				ac.VehicleLicense,
				ac.AppointmentTime,
				DATEADD(mi, DATEDIFF(mi, 0, ISNULL(ac.AppointmentTime, 0))/30*30, 0) as start,
				FORMAT(DATEADD(mi, DATEDIFF(mi, 0, ISNULL(ac.AppointmentTime, 0))/30*30, 0), 'HHmm') as startHHmm,
				ac.DealerId,
				ac.DealerCustomerId,
				ac.PresentationStatus,
				ac.AppointmentStatus, 
				ac.AdvisorId,
				ac.VIN,
				ac.Make,
				ac.Model,
				ac.[Year],
				ac.VehicleEngine  as Engine,
				ac.VehicleTransmission as Transmission,
				ac.VehicleDriveline as DriveLine,
				ac.RoNumber,
				dc.FirstName, 
				dc.LastName, 
				dc.CellPhone, 
				dc.HomePhone, 
				dc.WorkPhone, 
				dc.CustomerNumber,
				ac.StockNo,
				ac.CarId,
				ac.DealerVehicleId,
				u.FirstName as AdvisorName,
				u.LastName as AdvisorFamily,
				ac.CarIdUndetermined as CarIdUndetermined,
				ac.Mileage
			FROM AppointmentsCombined ac
			INNER JOIN dealer.DealerCustomers dc ON ac.DealerCustomerId = dc.DealerCustomerId
			LEFT JOIN dbo.Users u ON u.UserId = ac.AdvisorId) t
		WHERE t.DealerId = @TenantId
			AND t.AppointmentTime > @AppointmentStartTime 
			AND t.AppointmentTime < @AppointmentEndTime
			AND Charindex(','+cast(ISNULL(t.AdvisorId, -1) as varchar(8000))+',', @AdvisorIDs) > 0
			AND ((CONVERT(date, t.AppointmentTime) >= CONVERT(date, GETDATE())
			AND Charindex(','+cast(ISNULL(t.AdvisorId, -1) as varchar(8000))+',', @AdvisorIDs) > 0)
			OR (t.PresentationStatus = 'MC' OR isnull(t.RoNumber, '') != ''))
		ORDER BY t.AppointmentTime
	END
END