USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetAppointmentServices]    Script Date: 17.08.2016 11:00:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetAppointmentServices]
	@TenantId int,
	@DealerCustomerID int
AS
BEGIN
	SELECT ap.AppointmentPresentationId, 
		ap.AppointmentId,
		ap.DealerCustomerId,
		ap.Mileage, 
		ap.InvoiceNumber,
		case ap.PresentationStatus 
		when 'MK' then ap.ParkedTime 
		when 'MC' then ap.DateRoCreated 
		when 'MD' then ap.DateRoCreated 
		else ap.AppointmentTime end as AppointmentTime,
		dv.VIN,
		aps.OpDescription,
		aps.OpCode,
		aps.IsStriked
	FROM dealer.AppointmentPresentations ap
	INNER JOIN dealer.DealerVehicles dv ON ap.DealerCustomerId = dv.DealerCustomerId
	INNER JOIN dealer.AppointmentServices aps ON ap.AppointmentPresentationId = aps.AppointmentPresentationId
	WHERE ap.DealerCustomerId = @DealerCustomerID AND ap.DealerId = @TenantId
	ORDER BY AppointmentTime DESC, ap.AppointmentPresentationId, dv.VIN
END
