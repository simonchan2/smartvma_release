USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetMultipleVehicleMakes]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetMultipleVehicleMakes] 
	@Year NVARCHAR(MAX) = null,
	@Delimiter NVARCHAR(8)
AS
BEGIN
	IF(@Year IS NOT NULL)
	BEGIN
		select  distinct vm.VehicleMakeId as Id, vm.VehicleMake as Make from amam.Cars as car
		INNER JOIN amam.VehicleMakes as vm on car.VehicleMakeId = vm.VehicleMakeId
		INNER JOIN dbo.SplitDelimiterString(@Year, @Delimiter) sds ON sds.Item = CAST(car.VehicleYear as NVARCHAR(50))
	END
	ELSE
	BEGIN
		select VehicleMakeId as Id, VehicleMake as Make from amam.VehicleMakes
	END
END

/*

EXEC [dbo].[GetMultipleVehicleMakes] NULL, ','
*/

GO
