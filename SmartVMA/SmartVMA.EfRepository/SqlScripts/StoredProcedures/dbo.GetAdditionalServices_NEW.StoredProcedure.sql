USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetAdditionalServices_NEW]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Krste Bozinoski
-- Create date: 07.07.2016
-- Description:	List all additional services for specific dealer vehicle id and chosen mileage
-- =============================================
CREATE PROCEDURE [dbo].[GetAdditionalServices_NEW](
	@TenantId int,
	@IsLOF bit = null,
	@MenuLevel int = null,
	@DealerVehicleId int = null
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	declare @LaborRate float;
	set @LaborRate = (select top 1 CAST(value as float) from CompanySettings where CompanyId = @TenantId and Name = 'DefaultLaborRate');
	declare @includeShopCharges bit;
	declare @shopChargeAmount float;
	declare @shopChargesApplyToParts bit;
	declare @shopChargesApplyToLabor bit;
	declare @minShopCharges float;
	declare @maxShopCharges float;
	set @includeShopCharges = (select top 1 CAST(value as bit) from CompanySettings where CompanyId = @TenantId and Name = 'PrintShopCharges');
	if(@includeShopCharges = 1)
	BEGIN
		set @shopChargeAmount = (select top 1 CAST(value as float) from CompanySettings where CompanyId = @TenantId and Name = 'ShopChargeAmount');
		set @shopChargesApplyToParts = (select top 1 CAST(value as bit) from CompanySettings where CompanyId = @TenantId and Name = 'ShopChargesApplyToParts');
		set @shopChargesApplyToLabor = (select top 1 CAST(value as bit) from CompanySettings where CompanyId = @TenantId and Name = 'ShopChargesApplyToLabor');
		set @minShopCharges = (select top 1 CAST(value as float) from CompanySettings where CompanyId = @TenantId and Name = 'ShopChargeMinimum');
		set @maxShopCharges = (select top 1 CAST(value as float) from CompanySettings where CompanyId = @TenantId and Name = 'ShopChargeMaximum');
	END

	declare @includeTaxCharges bit;
	declare @taxChargeAmount float;
	declare @taxChargesApplyToParts bit;
	declare @taxChargesApplyToLabor bit;
	set @includeTaxCharges = (select top 1 CAST(value as bit) from CompanySettings where CompanyId = @TenantId and Name = 'DisplayTexesInMenus');
	if(@includeTaxCharges = 1)
	BEGIN
		set @taxChargeAmount = (select top 1 CAST(value as float) from CompanySettings where CompanyId = @TenantId and Name = 'TaxRateAmount');
		set @taxChargesApplyToParts = (select top 1 CAST(value as bit) from CompanySettings where CompanyId = @TenantId and Name = 'TaxRateApplyToParts');
		set @taxChargesApplyToLabor = (select top 1 CAST(value as bit) from CompanySettings where CompanyId = @TenantId and Name = 'TaxRateApplyToLabor');
	END

	SELECT 
	x.* 
	--,0 AS CarId
	--,0 AS Mileage
	--,2 AS ServiceMenuType
	--,'' AS VideoCode
	--,CONVERT(BIT,0) AS IsPreviouslyServed
	,SC.ShopChargesPartsPrice
	,SC.ShopChargesLaborPrice
	,Tax.TaxIncludedPartsPrice
	,Tax.TaxIncludedLaborPrice
	FROM(
		SELECT *
		--ServiceId
		--,OpCode
		--,FullDescription
		-----HARDCODED FOR THE VIEW---
		--,CONVERT(BIT,0) AS IsDeclined
		--,2 AS ServiceType
		------------------------------
		--,IsFluid
		--,IsEngineOil
		--,IsLOF
		--,IsStrikeOut
		--,BgProtectionPlanId
		--,MenuLevel
		--,LaborHour
		--,PartsPrice
		--,@LaborRate AS LabourRate
		--,(@LaborRate * LaborHour) AS LaborPrice
		--,((@LaborRate * LaborHour) + PartsPrice) AS Price
		FROM [dbo].[GetFilteredAdditionalServices](@TenantId, @IsLOF, @LaborRate)
		-- calculate LaborPrice, Price
		--CROSS APPLY(
		--	SELECT 
			
		--) calc

		UNION

		SELECT *
		--ServiceId
		--,OpCode
		--,FullDescription
		--,IsDeclined
		-----HARDCODED FOR THE VIEW---
		--,5 AS ServiceType
		--,CONVERT(BIT,0) AS IsFluid
		--,CONVERT(BIT,0) AS IsEngineOil
		--,CONVERT(BIT,0) AS IsLOF
		--,CONVERT(BIT,0) AS IsStrikeOut
		--,NULL AS BgProtectionPlanId
		--,1 AS MenuLevel
		------------------------------
		--,PartsPrice
		--,LaborHour
		--,LabourRate
		--,LaborPrice
		--,Price
		
		 FROM [dbo].[GetFilteredAppointmentServices](@TenantId, @DealerVehicleId, 1)
	) AS x
	-- helper calculations
	CROSS APPLY(
		SELECT 
		PartsPrice * (@shopChargeAmount / 100) as PartsPriceSC,
		(@LaborRate * LaborHour) * (@shopChargeAmount / 100) as LaborPriceSC
	) calc
	-- calculate ShopCharges for PartPrice and LaborHour
	CROSS APPLY(
	SELECT
		CASE @shopChargesApplyToParts 
		WHEN 1 THEN CASE 
			WHEN calc.PartsPriceSC < @minShopCharges 
			THEN @minShopCharges
			WHEN calc.PartsPriceSC > @maxShopCharges
			THEN @maxShopCharges
			ELSE calc.PartsPriceSC
		END
		ELSE 0 END AS ShopChargesPartsPrice

		,CASE @shopChargesApplyToLabor 
		WHEN 1 THEN CASE 
			WHEN calc.LaborPriceSC < @minShopCharges 
			THEN @minShopCharges
			WHEN calc.LaborPriceSC > @maxShopCharges
			THEN @maxShopCharges
			ELSE calc.LaborPriceSC
		END
		ELSE 0 END AS ShopChargesLaborPrice
	) SC
	-- calculate Taxes for PartPrice and LaborHour
	CROSS APPLY(
	SELECT 
		CASE @taxChargesApplyToParts 
		WHEN 1 THEN (PartsPrice + SC.ShopChargesPartsPrice) * (@taxChargeAmount / 100)
		ELSE 0 END AS TaxIncludedPartsPrice

		,CASE @taxChargesApplyToLabor 
		WHEN 1 THEN (LaborPrice + SC.ShopChargesLaborPrice) * (@taxChargeAmount / 100)
		ELSE 0 END AS TaxIncludedLaborPrice
	) Tax
END

/*
	 exec [dbo].[GetAdditionalServices] @TenantId=3,@IsLOF=null,@MenuLevel=null
	 exec [dbo].[GetAdditionalServices] @TenantId=3,@IsLOF=1,@MenuLevel=null

	 exec [dbo].[GetAdditionalServices] @TenantId=1089,@IsLOF=0,@MenuLevel=null

	 exec [dbo].[GetAdditionalServices_NEW] @TenantId=1089,@IsLOF=0,@MenuLevel=null
*/



GO
