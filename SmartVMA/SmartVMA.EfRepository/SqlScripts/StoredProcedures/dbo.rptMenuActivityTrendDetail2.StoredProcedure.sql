USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[rptMenuActivityTrendDetail2]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[rptMenuActivityTrendDetail2]
@DealerCompanyId int,
@MonthYear		varchar(30),
@Advisor		nvarchar(45),
@CPOnly				bit=0,
@UserOnly			bit=0,
@ExclLabor0			bit=0
AS 
BEGIN

if (@MonthYear='Total')
begin
	return
end
declare @StartDate datetime, @EndDate datetime
select @StartDate	= cast(@MonthYear as date)
select @EndDate		= dateadd(d,-1, dateadd(m, 1, @StartDate))

	
	select distinct 
		i.InvoiceDate,	
		i.invoicenumber,
		(dc.FirstName + ' ' + dc.LastName) CustomerName,
		i.Mileage,		
		isnull(dv.VehicleMake, '') + ' ' + isnull(dv.VehicleModel, '') + ' ' + isnull(cast(dv.VehicleYear as varchar),'') CarDesc,
       dmsu.[DmsUserNumber] AdvisorCode,
       dmsu.[DmsUserName] AdvisorName,                
       1 Presented,
       (case when PresentationStatus='MC'  then 1 else 0 end) Accepted,
	   (MenuLevel) [MenuLevel],
       [AppointmentTime]    
into #Data
from [dealer].[DealerDmsUsers] dmsu
	join [dealer].[AppointmentPresentations] ap on dmsu.UserId  = ap.[AdvisorId] 
	join [dealer].[DealerVehicles] dv on dv.[DealerVehicleId] = ap.DealerVehicleId	
	left join [dealer].[Invoices] i (nolock) on i.InvoiceNumber = ap.InvoiceNumber
	left join [dealer].[InvoiceDetails] d (nolock)on d.InvoiceId = i.InvoiceId
	left join [dealer].[DealerCustomers] dc (nolock)on dc.[DealerCustomerId] = i.DealerCustomerId
where	[AppointmentTime] between @StartDate and @EndDate and
		((@Advisor='Consolidated' or @Advisor='AllAdvisors') or (@Advisor!='Consolidated' and @Advisor!='AllAdvisors' and dmsu.DmsUserNumber =@Advisor)) and
		(@ExclLabor0 = 0 or (@ExclLabor0 = 1 and d.LaborHour = 0)) and
		(@UserOnly = 0 or (@UserOnly = 1 and dmsu.DmsUserName is not null)) and
		(@CPOnly = 0 or (@CPOnly=1 and d.[LaborType] = 'CP'))		
		and (@DealerCompanyId=0 or (@DealerCompanyId<>0 and ap.DealerId = @DealerCompanyId))

--select * from #Data order by AdvisorCode 

	select 
		InvoiceDate,
		invoicenumber,	
		CustomerName,
		Mileage,
		CarDesc,		
		Presented, 
		Accepted,
		Accepted result, 
		MenuLevel		
	into #Data2
	from #Data
	
	
	--select * from #tempDateRangeSet2 order by DateRange
	--return
	select *
	into #Data3
	from #Data2
	pivot (sum(result) for MenuLevel in ([1],[2],[3])) as [MenuLevel1]
	
	update #Data3
	set [1]=ISNULL([1] ,0), [2]=ISNULL([2] ,0),[3]=ISNULL([3] ,0)

	select * from #Data3
end





GO
