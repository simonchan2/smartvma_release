USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetPdfBodyPartsCopy]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
EXEC [dbo].[GetPdfBodyPartsCopy] @AppointmentPresentationId=65
*/
CREATE PROCEDURE [dbo].[GetPdfBodyPartsCopy]
	@AppointmentPresentationId BIGINT
AS
BEGIN
	SELECT asp.AppointmentServicePartId, asp.OriginalPartId, aps.AppointmentServicesId, aps.OpCode, aps.OpDescription,
	asp.PartName, ISNULL(aps.LaborHour, 0) as ServiceLaborHour, ISNULL(asp.LaborHour, 0) as LaborHour, ISNULL(asp.Quantity, 0) as Quantity,
	ISNULL(aps.PartsPrice, 0) as ServicePartsPrice,	ISNULL(asp.UnitPrice, 0) as PartsPrice, 
	ISNULL(aps.LaborPrice, 0) as LaborPrice,  ISNULL(aps.Price, 0) as Price 
	FROM dealer.AppointmentServices aps
	LEFT JOIN dealer.AppointmentServiceParts asp ON aps.AppointmentServicesId = asp.AppointmentServicesId
	WHERE aps.AppointmentPresentationId = @AppointmentPresentationId AND aps.IsStriked=0
END

GO