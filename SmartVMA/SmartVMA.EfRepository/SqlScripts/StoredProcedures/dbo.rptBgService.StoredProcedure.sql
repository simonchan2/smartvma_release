USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[rptBgService]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec [dbo].[rptBgService] 'QTY', 1041,'2015-01-01', '2016-01-01',1,0,0,0,0
CREATE PROCEDURE [dbo].[rptBgService]
@Content			varchar(255),
@DistributorId		int,
@DealerCompanyId	int,
@StartDate			datetime,
@EndDate			datetime,
@By					int=1,
@CPOnly				bit=0,
@ExclLabor0			bit=0,
@UserOnly			bit=0,
@ALaCateOnly		bit=0
AS
BEGIN
    SET NOCOUNT ON;

-- Get distinct rows
select distinct 
	i.AdvisorCode, 	
	i.[AdvisorName] ,
	bgpc.BgType [VehicleSystem],
	i.invoiceid,
	CASE @Content		 
		WHEN 'QTY' THEN (PartQty)
		WHEN 'Labor$' THEN ([LaborSale])
		WHEN 'Part$' THEN (d.[PartSale])
		WHEN 'LaborParts$' THEN ([LaborSale] + d.[PartSale]) 
		WHEN 'Hours' THEN (d.[LaborHour])
		WHEN 'ELR' THEN ([LaborSale]/d.[LaborHour])
		WHEN 'Commission' THEN (AdvisorIncentive)
		WHEN '%Pen' THEN case when PresentationStatus='MC' then 1 else 0 end
		WHEN 'GPLabor' THEN (d.[LaborSale] - d.[LaborCost])
		WHEN 'GPParts' THEN (d.[PartSale] - d.[PartCost])
		WHEN 'GPLaborParts' THEN (d.[LaborSale] - d.[LaborCost] + d.[PartSale] - d.[PartCost])						
	END as content
into #temp1
from [dealer].[Invoices] i (nolock)
join [dealer].[DealerDmsUsers] u (nolock)on u.DmsUserNumber = i.AdvisorCode
join [dealer].[InvoiceDetails] d (nolock)on d.InvoiceId = i.InvoiceId
join [dealer].[InvoiceDetailParts] p (nolock)on p.InvoiceDetailId = d.InvoiceDetailId
join [dealer].[DealerCustomers] dc (nolock)on dc.[DealerCustomerId] = i.DealerCustomerId
join [dealer].[DealerVehicles] dv (nolock)on dv.DealerVehicleId = i.DealerVehicleId
join [dbo].[Vehicles] v (nolock)on v.[VehicleId] = dv.VehicleId
join [dealer].[AppointmentServices] aps (nolock) on aps.DealerId = i.[DealerId] and aps.OpCode = d.[OpCode]
join [dealer].[AppointmentPresentations] aptpres (nolock) on aptpres.AppointmentPresentationId = aps.AppointmentPresentationId
join [dealer].[AdditionalServices] adds (nolock)on adds.[DealerId] = i.[DealerId] and adds.[OpCode] = d.[OpCode]
join [dealer].[AdditionalServiceParts] addsp (nolock) on addsp.AdditionalServiceId = addsp.AdditionalServiceId
join [bg].[BgProductsCategories] prodsc (nolock) on prodsc.[BgProductId] = addsp.[BgProductId]
join [bg].[BgProdCategories] bgpc (nolock) on bgpc.[BgProdCategoryId] = prodsc.[BgProdCategoryId] 
--from [dealer].[Invoices] i (nolock)
--join [dealer].[DealerDmsUsers] u (nolock)on u.DmsUserNumber = i.AdvisorCode
--join [dealer].[InvoiceDetails] d (nolock)on d.InvoiceId = i.InvoiceId
--join [dealer].[InvoiceDetailParts] p (nolock)on p.InvoiceDetailId = d.InvoiceDetailId
--join [dealer].[DealerCustomers] dc (nolock)on dc.[DealerCustomerId] = i.DealerCustomerId
--join [dealer].[DealerVehicles] dv (nolock)on dv.DealerVehicleId = i.DealerVehicleId
--join [dbo].[Vehicles] v (nolock)on v.[VehicleId] = dv.VehicleId
--join [dealer].[AppointmentServices] aps (nolock) on aps.DealerId = i.[DealerId] and aps.OpCode = d.[OpCode]
--join [dealer].[AppointmentPresentations] aptpres (nolock) on aptpres.AppointmentPresentationId = aps.AppointmentPresentationId
--join [bg].[DealerBgServices] dbs (nolock)on dbs.[DealerId] = i.[DealerId] and dbs.[OpCode] = d.[OpCode]
--join [bg].[BgProdSubcategories] bgpsc (nolock) on bgpsc.BgProdSubcategoryId= aps.BgProdSubcategoryId
--join [bg].[BgProdCategories] bgpc (nolock) on bgpc.BgProdCategoryId = bgpsc.BgProdCategoryId
where ((@DistributorId = 0 and @DealerCompanyId = 0) or (@DealerCompanyId > 0 and i.DealerId = @DealerCompanyId) or (@DistributorId > 0 and @DealerCompanyId = 0 and i.DealerId in (select CompanyId from Companies where parentId = @DistributorId)))
	--(@DealerCompanyId=0 or (@DealerCompanyId<>0 and i.DealerId = @DealerCompanyId))
and i.InvoiceDate between @StartDate and @EndDate
and [DmsUserTypeId] = @By -- advisor or tech
and (@ExclLabor0 = 0 or (@ExclLabor0 = 1 and d.LaborHour = 0))
and (@UserOnly = 0 or (@UserOnly = 1 and u.DmsUserName is not null))
and (@CPOnly = 0 or (@CPOnly=1 and d.[LaborType] = 'CP'))
and (@ALaCateOnly = 0 or (@ALaCateOnly=1 and aps.IsAdditionService=1))

select	AdvisorCode, 	
		[AdvisorName] ,
		[VehicleSystem],
		invoiceid, 
		sum(content) content,
		sum(content) as contentForSum
into #temp2
from #temp1
group by AdvisorCode, AdvisorName, InvoiceId, [VehicleSystem]


--select * from #temp2 return

select *, 1 sort 
into #temp3
from #temp2 t
pivot (sum(contentForSum) for [VehicleSystem] in ([AC],[Batt],[Brake],[Cool],[Diff],[DSL Emiss],[DSL Induct],[DSL Inject],[Ethanol],[Fuel],[GDI],[Oil],[PS],[Trans])) as [VehicleSystem]


select 
	AdvisorCode
	,[AdvisorName]
	, count(invoiceid) Ro
	,sum(content) Content 
	,isnull(sum([AC]),0) AC
	,isnull(sum([Batt]),0) [Batt]
	,isnull(sum([Brake]),0) [Brake]
	,isnull(sum([Cool]),0) [Cool]
	,isnull(sum([Diff]),0) [Diff]
	,isnull(sum([DSL Emiss]),0) [DSL Emiss]
	,isnull(sum([DSL Induct]),0) [DSL Induct]
	,isnull(sum([DSL Inject]),0) [DSL Inject]
	,isnull(sum([Ethanol]),0) [Ethanol]
	,isnull(sum([Fuel]),0) [Fuel]
	,isnull(sum([GDI]),0) [GDI]
	,isnull(sum([Oil]),0) [Oil]
	,isnull(sum([PS]),0) [PS]
	,isnull(sum([Trans]),0) [Trans]
	,max(sort) sort  
from #temp3
group by AdvisorCode, [AdvisorName]
union
select 
	' '
	,'Total' [Advisor Name]
	,count(invoiceid) Ro
	,sum(content) Content
	,isnull(sum([AC]),0) AC
	,isnull(sum([Batt]),0) [Batt]
	,isnull(sum([Brake]),0) [Brake]
	,isnull(sum([Cool]),0) [Cool]
	,isnull(sum([Diff]),0) [Diff]
	,isnull(sum([DSL Emiss]),0) [DSL Emiss]
	,isnull(sum([DSL Induct]),0) [DSL Induct]
	,isnull(sum([DSL Inject]),0) [DSL Inject]
	,isnull(sum([Ethanol]),0) [Ethanol]
	,isnull(sum([Fuel]),0) [Fuel]
	,isnull(sum([GDI]),0) [GDI]
	,isnull(sum([Oil]),0) [Oil]
	,isnull(sum([PS]),0) [PS]
	,isnull(sum([Trans]),0) [Trans]
	,2 sort  
from #temp3
order by sort, [AdvisorName]


END





GO
