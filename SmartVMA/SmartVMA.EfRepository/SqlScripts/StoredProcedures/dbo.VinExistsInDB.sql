USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[VinExistsInDB]    Script Date: 03.10.2016 12:19:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[VinExistsInDB]
	@VIN nvarchar(MAX)
AS
BEGIN
SET NOCOUNT ON;

declare @VinExist bit
declare @VinShort nvarchar(20);
	set @VinShort = SUBSTRING(@VIN, 0, 9) + SUBSTRING(@VIN, 10, 2);

SET @VinExist = (CASE WHEN (SELECT COUNT(*) FROM amam.CarVinShorts
				WHERE VinShort = @VinShort AND CarId IS NOT NULL) > 0 THEN 1 ELSE 0 END)

IF(@VinExist = 0)
BEGIN
	SET @VinExist = (CASE WHEN (SELECT COUNT(*) FROM vinquery.VinMaster
					WHERE VinShort = @VinShort) > 0 THEN 1 ELSE 0 END)
END

SELECT @VinExist

END
GO
