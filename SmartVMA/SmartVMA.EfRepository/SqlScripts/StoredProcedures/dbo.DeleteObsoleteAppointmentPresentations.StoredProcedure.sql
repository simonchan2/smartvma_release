USE [SmartVmaDev]
GO

/****** Object:  StoredProcedure [dbo].[DeleteObsoleteAppointmentPresentations]    Script Date: 10/19/2016 3:03:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DeleteObsoleteAppointmentPresentations]
	@TenantId int,
	@DateTo datetime
AS
	delete from document.PdfDocuments
where AppointmentPresentationId IN 
	(select AppointmentPresentationId from dealer.AppointmentPresentations 
	where TimeCreated < @DateTo)

delete from dealer.AppointmentServiceParts
where AppointmentServicesId IN 
	(select AppointmentServicesId from dealer.AppointmentServices
	where AppointmentPresentationId IN 
		(select AppointmentPresentationId from dealer.AppointmentPresentations 
		where TimeCreated < @DateTo))

delete from dealer.AppointmentServices
where AppointmentPresentationId IN 
	(select AppointmentPresentationId from dealer.AppointmentPresentations 
	where TimeCreated < @DateTo)

delete from vi.DamageIndications
where InspectionId IN 
	(select InspectionId from dealer.Inspection
	where AppointmentPresentationId IN 
		(select AppointmentPresentationId from dealer.AppointmentPresentations 
		where TimeCreated < @DateTo))

delete from vi.VehiclePhotos
where InspectionId IN 
	(select InspectionId from dealer.Inspection
	where AppointmentPresentationId IN 
		(select AppointmentPresentationId from dealer.AppointmentPresentations 
		where TimeCreated < @DateTo))

delete from dealer.Inspection
where AppointmentPresentationId IN 
	(select AppointmentPresentationId from dealer.AppointmentPresentations 
	where TimeCreated < @DateTo)

delete from dealer.AppointmentPresentations 
where TimeCreated < @DateTo

/*
	exec dbo.DeleteObsoleteAppointmentPresentations @DateTo='20160912'
*/

GO