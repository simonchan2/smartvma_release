USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetPdfBodyCustomerCopy]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPdfBodyCustomerCopy]
	@AppointmentPresentationId BIGINT
AS
BEGIN
	SELECT aps.OpCode, aps.OpDescription, isnull(aps.Price, 0) as Price, 
	isnull(ap.ShopChargesAmount, 0) as ShopChargesAmount, isnull(ap.TaxChargesAmount, 0) as TaxChargesAmount
	FROM dealer.AppointmentServices aps
	INNER JOIN dealer.AppointmentPresentations ap ON ap.AppointmentPresentationId = aps.AppointmentPresentationId
	WHERE ap.AppointmentPresentationId = @AppointmentPresentationId AND aps.IsStriked=0
END

GO