USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [vi].[InspectionGet]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Boris Kamenov
-- Create date: 23.06.2016
-- Description:	[InspectionGet]
-- EXEC vi.[InspectionGet] NULL, 27, 3
-- =============================================
CREATE PROCEDURE [vi].[InspectionGet]
	@Id bigint,
	@AppointmentPresentationId BIGINT,
	@TenantId bigint
AS
BEGIN
	IF(@Id IS NULL AND @AppointmentPresentationId IS NOT NULL)
	BEGIN
		SET @Id = (SELECT TOP 1 Id FROM dealer.Inspection WHERE AppointmentPresentationId = @AppointmentPresentationId)
	END
	--Get default damage indication Types
	SELECT 
	dit.Id, 
	dit.Url,
	dit.Name, 
	dit.Position,
	dit.IsComment
	FROM vi.DamageIndicationTypes dit
	ORDER BY dit.Position

	--Get default car viewpoints
	SELECT 
	vp.Id,
	vp.Name,
	vp.Url,
	vp.Position 
	FROM vi.ViewPoints vp
	ORDER BY vp.Position

	--Get photos for inspection
	SELECT 
	vp.Id, 
	i.Id as [InspectionId], 
	--vp.FileName as Url,
	vp.FileName as [FileName]
	--vp.Url
	FROM dealer.Inspection i
	INNER JOIN vi.VehiclePhotos vp ON vp.InspectionId = i.Id
	WHERE i.Id = @Id
	AND i.DealerId = @TenantId
	ORDER BY vp.TimeUpdated

	--Get Damage Indications for inspection
	SELECT 
	di.Id, 
	dit.Id as [TypeId], 
	di.Comment, di.OffsetLeft, 
	di.OffsetTop, 
	viewp.Id as [ViewPointId],
	i.Id as [InspectionId]
	FROM dealer.Inspection i
	INNER JOIN vi.DamageIndications di ON di.InspectionId = i.Id
	LEFT JOIN vi.ViewPoints viewp ON viewp.Id = di.ViewPointId
	LEFT JOIN vi.DamageIndicationTypes dit ON dit.Id = di.TypeId
	WHERE i.Id = @Id
	AND i.DealerId = @TenantId
	ORDER BY viewp.Position

	--Get Inspection/Tire Details
	SELECT
	i.Id,
	i.LFRimScratch,
	i.LRRimScratch,
	i.RFRimScratch,
	i.RRRimScratch,
	i.LFTireTypeId,
	i.LRTireTypeId,
	i.RFTireTypeId,
	i.RRTireTypeId,
	i.Fuel,
	i.AppointmentPresentationId,
	i.DealerId as [TenantId]
	FROM dealer.Inspection i
	WHERE i.Id = @Id
	AND i.DealerId = @TenantId
END

GO
