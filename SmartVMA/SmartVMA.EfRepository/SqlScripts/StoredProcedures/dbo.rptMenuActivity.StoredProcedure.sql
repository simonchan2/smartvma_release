USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[rptMenuActivity]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[rptMenuActivity]
@DealerCompanyId	int,
@StartDate			datetime,
@EndDate			datetime,
@UserOnly			bit
AS
BEGIN
    SET NOCOUNT ON;
-- MENU ACTIVITY REPORT
select distinct 
	dmsu.[DmsUserNumber] AdvisorCode,
	dmsu.[DmsUserName] AdvisorName, 
	ap.InvoiceNumber,	
	(MenuLevel) [MenuLevel],		
	(1) Presented,
	(case when PresentationStatus='MC' then 1 else 0 end) Accepted,
	(case when PresentationStatus='MD' then 1 else 0 end) Declined
	--Status of the meny presenrtation; V: Void; C: Closed; M: Menu Presented; MC: Menu Accepted; MD: Menu Declined; MK: Menu Parked			
into #temp1
from [dealer].[AppointmentPresentations] ap 
join [dealer].[DealerVehicles] dv on dv.[DealerVehicleId] = ap.DealerVehicleId
left join [dealer].[DealerDmsUsers] dmsu on dmsu.UserId = ap.[AdvisorId] 
where 
(@DealerCompanyId=0 or (@DealerCompanyId<>0 and dmsu.DealerId = @DealerCompanyId))
and [AppointmentTime] between @StartDate and @EndDate
and (@UserOnly = 0 or (@UserOnly = 1 and dmsu.DmsUserName is not null)) 
--and dmsu.DmsUserNumber = 2746

--select * from #temp1

select 
	AdvisorCode,
	AdvisorName,
	sum(Presented) Presented, 
	sum(Accepted) Accepted, 
	sum(Accepted) Result, 
	sum(Declined) Declined,
	MenuLevel
into #tempAdvisorData
from #temp1
group by AdvisorCode, AdvisorName,MenuLevel

--select * from #tempAdvisorData


select *
into #tempAdvisorData2
from #tempAdvisorData
pivot (count([result]) for MenuLevel in ([1],[2],[3])) as [MenuLevel]


select 
	AdvisorCode,
	AdvisorName,
	sum(Presented) Presented, 
	sum(Accepted) Accepted, 	
	(sum(Accepted) /sum(Presented) * 100) Closing, 
	sum(Declined) Declined,
	(sum(Presented) - sum(Accepted) - sum(Declined)) UnFinished,
	sum([1]) [1],
	sum([2]) [2],
	sum([3]) [3]
into #tempAdvisorData3
from #tempAdvisorData2
group by AdvisorCode, AdvisorName


update t2
	set  t2.Presented = m.sumPresented,
		t2.Accepted = m.sumAccepted
from #tempAdvisorData3 t2
join (
select AdvisorCode, sum(Presented) sumPresented, sum(Accepted) sumAccepted from #tempAdvisorData
group by AdvisorCode) m on m.AdvisorCode = t2.AdvisorCode



select 
	' ' AdvisorCode
	,'Total' AdvisorName
	,sum(Presented) Presented
	,sum(Accepted) Accepted	
	,0 Closing
	,sum(Declined) Declined
	,sum(UnFinished) UnFinished
	,sum([1]) [1]
	,sum([2]) [2]
	,sum([3]) [3]
	,2 sort	
into #tempTotal
from #tempAdvisorData3

update t 
set Closing = isnull(round(((CAST(Accepted as decimal(10,2)) / CAST(Presented as decimal(10,2))) * 100),2),0)
from #tempTotal t

select *, 1 sort 
from #tempAdvisorData3 t2
union
select * from #tempTotal
order by sort


END


GO
