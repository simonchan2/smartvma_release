USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[spGetDateByOption]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[spGetDateByOption](@DateOption varchar(100))
AS 
BEGIN

declare @stareDate date, @endDate date

SET @stareDate = GETDATE() 
SET @endDate = GETDATE() 

IF @DateOption = 'Today'  ----Today
BEGIN
	SET @stareDate = GETDATE() 
	SET @endDate = GETDATE() 
END
ELSE
IF @DateOption = 'Yesterday'  ----Yesterday
BEGIN
	SET @stareDate = DATEADD(d,-1,GETDATE()) 	
END
ELSE
IF @DateOption = 'ThisWeek'  ----This week
BEGIN
	SET @stareDate = DATEADD(wk,DATEDIFF(wk,0,GETDATE()),0)	
END
IF @DateOption = 'LastWeek'  ----last week
BEGIN
	SET @stareDate = DATEADD(wk,DATEDIFF(wk,7,GETDATE()),0)
	SET @endDate = DATEADD(wk,DATEDIFF(wk,7,GETDATE()),6)
END
ELSE
IF @DateOption = 'ThisMonth'  ----This month
BEGIN
	SET @stareDate = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)	
END
ELSE
IF @DateOption = 'LastMonth'  ----Last month
BEGIN
	SET @stareDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0))
	SET @endDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)))
END
ELSE
IF @DateOption = 'ThisYear'  ----This year
BEGIN
	SET @stareDate = DATEADD(yy,DATEDIFF(yy,0,GETDATE()),0)	
END
ELSE
IF @DateOption = 'LastYear'  ----Last year
BEGIN
	SET @stareDate = DATEADD(yy,-1,DATEADD(yy,DATEDIFF(yy,0,GETDATE()),0)) 
	SET @endDate = DATEADD(ms,-3,DATEADD(yy,0,DATEADD(yy,DATEDIFF(yy,0,GETDATE()),0)))
END

select @stareDate StartDate, @endDate EndDate

return
end



/*
 select * from dbo.fnGetDate(8)
 */


GO
