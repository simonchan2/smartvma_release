USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[OverrideRulesDelete]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
EXEC [dbo].[OverrideRulesDelete]
'1,2',
3,
','
*/
CREATE PROCEDURE [dbo].[OverrideRulesDelete]
	@RuleIds NVARCHAR(MAX),
	@TenantId INT,
	@Delimiter NVARCHAR(8)
AS
BEGIN
	--Get all Rule Ids for delete in a set
	IF OBJECT_ID('tempdb..#RuleIds') is not null 
	BEGIN
		DROP TABLE #Years;
	END;
	SELECT Item AS RuleId
	INTO #RuleIds
	FROM dbo.SplitDelimiterString(@RuleIds, @Delimiter) sds
	INNER JOIN dealer.OverrideRules orules ON CAST(orules.Id AS NVARCHAR(50)) = sds.Item
	WHERE orules.DealerId = @TenantId
	
	--Delete Related Years
	DELETE oYears
	FROM dbo.OverrideRuleYears oYears
	INNER JOIN #RuleIds ri ON ri.RuleId= oYears.OverrideRuleId;

	--Delete Related Makes
	DELETE oMakes
	FROM dbo.OverrideRuleMakes oMakes
	INNER JOIN #RuleIds ri ON ri.RuleId= oMakes.OverrideRuleId;

	--Delete Related Models
	DELETE oModels
	FROM dbo.OverrideRuleModels oModels
	INNER JOIN #RuleIds ri ON ri.RuleId= oModels.OverrideRuleId;

	--Delete Related Engines
	DELETE oEngines
	FROM dbo.OverrideRuleEngines oEngines
	INNER JOIN #RuleIds ri ON ri.RuleId= oEngines.OverrideRuleId;

	--Delete Related Transmissions
	DELETE oTransmissions
	FROM dbo.OverrideRuleTransmissions oTransmissions
	INNER JOIN #RuleIds ri ON ri.RuleId= oTransmissions.OverrideRuleId;

	--Delete Related Drivelines
	DELETE oDrivelines
	FROM dbo.OverrideRuleDrivelines oDrivelines
	INNER JOIN #RuleIds ri ON ri.RuleId= oDrivelines.OverrideRuleId;

	----Delete Related Steerings
	--DELETE oSteerings
	--FROM dbo.OverrideRuleSteerings oSteerings
	--INNER JOIN #RuleIds ri ON ri.RuleId= oSteerings.OverrideRuleId;

	--Set Related Rule Ids in Basic Services to NULL
	UPDATE tor
	SET 
	LaborHourRuleId = IIF(tor.LaborHourRuleId = ri.RuleId, NULL, tor.LaborHourRuleId)
	,LaborPriceRuleId = IIF(tor.LaborPriceRuleId = ri.RuleId, NULL, tor.LaborPriceRuleId)
	,PartPriceRuleId = IIF(tor.PartPriceRuleId = ri.RuleId, NULL, tor.PartPriceRuleId)
	,OpCodeRuleId = IIF(tor.OpCodeRuleId = ri.RuleId, NULL, tor.OpCodeRuleId)
	,PartNoRuleId = IIF(tor.PartNoRuleId = ri.RuleId, NULL, tor.PartNoRuleId)
	FROM dbo.[TenantOverrideRules] tor
	INNER JOIN #RuleIds ri ON ri.RuleId IN (tor.LaborHourRuleId, tor.LaborPriceRuleId, tor.PartPriceRuleId, tor.OpCodeRuleId, tor.PartNoRuleId)

	--Delete Rules
	DELETE orules
	FROM dealer.OverrideRules orules
	INNER JOIN #RuleIds ri ON ri.RuleId = orules.Id
	
END;



GO
