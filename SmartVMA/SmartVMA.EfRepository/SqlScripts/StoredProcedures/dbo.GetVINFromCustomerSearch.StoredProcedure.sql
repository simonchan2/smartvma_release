USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetVINFromCustomerSearch]    Script Date: 29.09.2016 11:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetVINFromCustomerSearch] 
	@TenantId int,
	@CustomerName nvarchar(355) = null,
	@CustomerNumber nvarchar(25) = null,
	@CustomerPhone nvarchar(35) = null,
	@StockNo nvarchar(50) = null,
	@VehicleLicense nvarchar(15) = null
AS
BEGIN
SET NOCOUNT ON;

Declare @VIN nvarchar(max);

	DECLARE @FirstName nvarchar(150)
	DECLARE @LastName nvarchar(150)

	if (CHARINDEX(', ',@CustomerName) > 0) OR (CHARINDEX(',',@CustomerName) > 0)
	begin
		Select @FirstName = SUBSTRING(@CustomerName, 1, CASE WHEN CHARINDEX(',', @CustomerName) > 0 THEN CHARINDEX(',', @CustomerName)-1 ELSE 0 END)
		Select @LastName = LTRIM(Substring(@CustomerName, LEN(@FirstName)+2, LEN(@CustomerName)))
	end
	else if (CHARINDEX(' ',@CustomerName) > 0)
	begin
		Select @FirstName = SUBSTRING(@CustomerName, 1, CASE WHEN CHARINDEX(' ', @CustomerName) > 0 THEN CHARINDEX(' ', @CustomerName)-1 ELSE 0 END)
		Select @LastName = LTRIM(SUBSTRING(@CustomerName, LEN(@FirstName)+1, LEN(@CustomerName)))
	end
	else
	begin
		Select @FirstName = @CustomerName
	end

	SET @VIN = (SELECT TOP 1 t.VIN FROM
		(SELECT 
			ISNULL(dv.VIN, '') as VIN,
			dc.DealerId as DealerId,
			ISNULL(dc.FirstName, ' ') as FirstName,
			ISNULL(dc.LastName, ' ') as LastName,
			ISNULL(dc.CustomerNumber, ' ') as CustomerNumber,
			ISNULL(dv.StockNo, ' ') as StockNo,
			ISNULL(dv.VehicleLicense, ' ') as VehicleLicense,
			ISNULL(dc.CellPhone, ' ') as CustomerPhone
		FROM dealer.DealerCustomers dc
		INNER JOIN dealer.DealerVehicles dv
			ON dc.DealerCustomerId = dv.DealerCustomerId) t
	WHERE t.DealerId = @TenantId
		AND (((@FirstName is null and t.FirstName = '') OR (CHARINDEX(ISNULL(@FirstName,t.FirstName), t.FirstName) > 0) OR (CHARINDEX(ISNULL(@FirstName,t.LastName), t.LastName) > 0))
		AND ((@LastName is null and t.LastName = '') OR (CHARINDEX(ISNULL(@LastName,t.LastName), t.LastName) > 0) ))
		AND ((@CustomerNumber is null and t.CustomerNumber = '') OR (CHARINDEX(ISNULL(@CustomerNumber,t.CustomerNumber),t.CustomerNumber) > 0))
		AND ((@CustomerPhone is null and t.CustomerPhone = '') OR (CHARINDEX(ISNULL(@CustomerPhone,t.CustomerPhone),t.CustomerPhone) > 0))
		AND ((@StockNo is null and t.StockNo = '') OR (CHARINDEX(ISNULL(@StockNo,t.StockNo),t.StockNo) > 0))
		AND ((@VehicleLicense is null and t.VehicleLicense = '') OR (CHARINDEX(ISNULL(@VehicleLicense,t.VehicleLicense),t.VehicleLicense) > 0)))


SELECT @VIN
END

GO
