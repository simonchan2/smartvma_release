USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetAdditionalservicesForMaintenance]    Script Date: 12/12/2016 7:24:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

EXEC PROCEDURE [dbo].[GetAdditionalservicesForMaintenance]
	@TenantId = 1089,
	@Years = 1089,
	@Makes = 1089,
	@Models = 1089,
	@EngineTypes = 1089,
	@TransmissionTypes = 1089,
	@DriveLines = 1089,
	@PageSize = 10,
	@CurrentPage = 1,
	@OrderExpression = 'OpCode asc',
	@SearchKeyword = NULL

*/
ALTER PROCEDURE [dbo].[GetAdditionalservicesForMaintenance]
	@TenantId int,
	@Years nvarchar(max),
	@Makes nvarchar(max),
	@Models nvarchar(max),
	@EngineTypes nvarchar(max),
	@TransmissionTypes nvarchar(max),
	@DriveLines nvarchar(max),
	@PageSize int,
	@CurrentPage int,
	@OrderExpression nvarchar(500),
	@SearchKeyword  nvarchar(500),
	@OpCode nvarchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @Delimiter NVARCHAR(8);
	SET @Delimiter =',';
	SET @SearchKeyword = (SELECT dbo.Trim(@SearchKeyword));
	SET @OpCode = (SELECT dbo.Trim(@OpCode));

	-----------------------------------------------------------------------

	IF OBJECT_ID('tempdb..#Services') is not null 
	BEGIN
		DROP TABLE #Services;
	END;
	SELECT DISTINCT 
			services.*,
			protectionPlan.ProtectionPlanName AS ProtectionPlanName,
			protectionSubPlan.SubPlanName AS SubPlanName
	INTO #Services
	FROM dealer.Additionalservices as services
			INNER JOIN dbo.AdditionalServicesCars as serviceCars ON (services.AdditionalServiceId = serviceCars.AdditionalServiceId)
			INNER JOIN dbo.GetCarIdByCriteria(@Years, @Makes, @Models, @EngineTypes, @DriveLines, @TransmissionTypes, @Delimiter) cars ON (serviceCars.CarId = cars.CarId)
			LEFT OUTER JOIN bg.BgProtectionPlans as protectionPlan ON (services.BgProtectionPlanId = protectionPlan.BgProtectionPlanId)
			LEFT OUTER JOIN bg.BgProtectionSubPlans as protectionSubPlan ON (services.BgProtectionSubPlanId = protectionSubPlan.BgProtectionSubPlanId)
		WHERE services.DealerId = @TenantId
			AND (@SearchKeyword IS NULL OR @SearchKeyword='' OR services.OpDescription LIKE '%' + @SearchKeyword + '%'
					OR services.Comment LIKE '%' + @SearchKeyword + '%'
					OR services.IntervalStart LIKE '%' + @SearchKeyword + '%'
					OR services.IntervalRepeat LIKE '%' + @SearchKeyword + '%'
					OR protectionPlan.ProtectionPlanName LIKE '%' + @SearchKeyword + '%')
			AND (@OpCode IS NULL OR @OpCode = '' or services.OpCode like '%' + @OpCode +'%')

	DECLARE @ServiceIds TABLE(Id INT NULL);
	INSERT INTO @ServiceIds
	SELECT services.AdditionalServiceId
	FROM #Services as services
	ORDER BY 
		CASE WHEN @OrderExpression = 'OpCode asc' THEN services.OpCode END ASC,
		CASE WHEN @OrderExpression = 'OpCode desc' THEN services.OpCode END DESC,
		CASE WHEN @OrderExpression = 'OpDescription asc' THEN services.OpDescription END ASC,
		CASE WHEN @OrderExpression = 'OpDescription desc' THEN services.OpDescription END DESC,
		CASE WHEN @OrderExpression = 'LaborHour asc' THEN services.LaborHour END ASC,
		CASE WHEN @OrderExpression = 'LaborHour desc' THEN services.LaborHour END DESC,
		CASE WHEN @OrderExpression = 'Comment asc' THEN services.Comment END ASC,
		CASE WHEN @OrderExpression = 'Comment desc' THEN services.Comment END DESC,
		CASE WHEN @OrderExpression = 'AdditionalServiceId asc' THEN services.AdditionalServiceId END ASC,
		CASE WHEN @OrderExpression = 'AdditionalServiceId desc' THEN services.AdditionalServiceId END DESC,
        CASE WHEN @OrderExpression = 'LaborRate asc' THEN services.LaborRate END ASC,
		CASE WHEN @OrderExpression = 'LaborRate desc' THEN services.LaborRate END DESC,
        CASE WHEN @OrderExpression = 'IntervalStart asc' THEN services.IntervalStart END ASC,
		CASE WHEN @OrderExpression = 'IntervalStart desc' THEN services.IntervalStart END DESC,
        CASE WHEN @OrderExpression = 'IntervalRepeat asc' THEN services.IntervalRepeat END ASC,
		CASE WHEN @OrderExpression = 'IntervalRepeat desc' THEN services.IntervalRepeat END DESC,
        CASE WHEN @OrderExpression = 'AdvisorIncentive asc' THEN services.AdvisorIncentive END ASC,
		CASE WHEN @OrderExpression = 'AdvisorIncentive desc' THEN services.AdvisorIncentive END DESC,
		CASE WHEN @OrderExpression = 'TechIncentive asc' THEN services.TechIncentive END ASC,
		CASE WHEN @OrderExpression = 'TechIncentive desc' THEN services.TechIncentive END DESC,
        CASE WHEN @OrderExpression = 'BgProtectionPlan.ProtectionPlanName asc' THEN services.ProtectionPlanName END ASC,
		CASE WHEN @OrderExpression = 'BgProtectionPlan.ProtectionPlanName desc' THEN services.ProtectionPlanName END DESC,
        CASE WHEN @OrderExpression = 'BgProtectionSubPlan.SubPlanName asc' THEN services.SubPlanName END ASC,
		CASE WHEN @OrderExpression = 'BgProtectionSubPlan.SubPlanName desc' THEN services.SubPlanName END DESC
	OFFSET (@PageSize * @CurrentPage) ROWS
	FETCH NEXT @PageSize ROWS ONLY;

	
	SELECT *
	FROM dealer.AdditionalServices as services
		LEFT OUTER JOIN bg.BgProtectionPlans as protectionPlan ON (services.BgProtectionPlanId = protectionPlan.BgProtectionPlanId)
		LEFT OUTER JOIN bg.BgProtectionSubPlans as protectionSubPlan ON (services.BgProtectionSubPlanId = protectionSubPlan.BgProtectionSubPlanId)
	WHERE AdditionalServiceId IN (SELECT * FROM @ServiceIds)
	ORDER BY 
		CASE WHEN @OrderExpression = 'OpCode asc' THEN services.OpCode END ASC,
		CASE WHEN @OrderExpression = 'OpCode desc' THEN services.OpCode END DESC,
		CASE WHEN @OrderExpression = 'OpDescription asc' THEN services.OpDescription END ASC,
		CASE WHEN @OrderExpression = 'OpDescription desc' THEN services.OpDescription END DESC,
		CASE WHEN @OrderExpression = 'LaborHour asc' THEN services.LaborHour END ASC,
		CASE WHEN @OrderExpression = 'LaborHour desc' THEN services.LaborHour END DESC,
		CASE WHEN @OrderExpression = 'Comment asc' THEN services.Comment END ASC,
		CASE WHEN @OrderExpression = 'Comment desc' THEN services.Comment END DESC,
		CASE WHEN @OrderExpression = 'AdditionalServiceId asc' THEN services.AdditionalServiceId END ASC,
		CASE WHEN @OrderExpression = 'AdditionalServiceId desc' THEN services.AdditionalServiceId END DESC,
        CASE WHEN @OrderExpression = 'LaborRate asc' THEN services.LaborRate END ASC,
		CASE WHEN @OrderExpression = 'LaborRate desc' THEN services.LaborRate END DESC,
        CASE WHEN @OrderExpression = 'IntervalStart asc' THEN services.IntervalStart END ASC,
		CASE WHEN @OrderExpression = 'IntervalStart desc' THEN services.IntervalStart END DESC,
        CASE WHEN @OrderExpression = 'IntervalRepeat asc' THEN services.IntervalRepeat END ASC,
		CASE WHEN @OrderExpression = 'IntervalRepeat desc' THEN services.IntervalRepeat END DESC,
        CASE WHEN @OrderExpression = 'AdvisorIncentive asc' THEN services.AdvisorIncentive END ASC,
		CASE WHEN @OrderExpression = 'AdvisorIncentive desc' THEN services.AdvisorIncentive END DESC,
		CASE WHEN @OrderExpression = 'TechIncentive asc' THEN services.TechIncentive END ASC,
		CASE WHEN @OrderExpression = 'TechIncentive desc' THEN services.TechIncentive END DESC,
        CASE WHEN @OrderExpression = 'BgProtectionPlan.ProtectionPlanName asc' THEN protectionPlan.ProtectionPlanName END ASC,
		CASE WHEN @OrderExpression = 'BgProtectionPlan.ProtectionPlanName desc' THEN protectionPlan.ProtectionPlanName END DESC,
        CASE WHEN @OrderExpression = 'BgProtectionSubPlan.SubPlanName asc' THEN protectionSubPlan.SubPlanName END ASC,
		CASE WHEN @OrderExpression = 'BgProtectionSubPlan.SubPlanName desc' THEN protectionSubPlan.SubPlanName END DESC;

	
	SELECT *
	FROM dealer.AdditionalServiceParts 
	WHERE AdditionalServiceId IN (SELECT * FROM @ServiceIds);

	SELECT COUNT(*) AS TotalCount,
	substring(
	(SELECT @Delimiter + CAST(s.AdditionalServiceId AS NVARCHAR(25))
	FROM #Services s
	ORDER BY s.AdditionalServiceId
	FOR XML PATH ('')), 2, 999999999999) AS FilteredServiceIds
	FROM #Services

END

