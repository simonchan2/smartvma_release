USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[rptSalesReport]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rptSalesReport]
@By					nvarchar(20),
@StartDate			datetime,
@EndDate			datetime,
@LaborType		    nvarchar(20),
@CPOnly				bit,
@ExclLabor0			bit,
@UserOnly			bit
AS
BEGIN
    SET NOCOUNT ON;

if (@LaborType = ' ' or @LaborType = '999')
begin
	set @LaborType = null
end

select distinct i.AdvisorCode, i.AdvisorName, 
	(i.InvoiceNumber) InvoiceNumber, 
	ROUND((d.LaborHour),2) LaborHour, 
	Round(d.PartSale,2) PartSale,
	Round(d.LaborSale,2) LaborSale,
	d.LaborType
into #temp
from [dealer].[Invoices] i (nolock)
	join [dealer].[DealerDmsUsers] u (nolock)on u.DmsUserNumber = i.AdvisorCode
	join [dealer].[AppointmentPresentations] ap on u.UserId = ap.[AdvisorId] 
	join [dealer].[InvoiceDetails] d (nolock)on d.InvoiceId = i.InvoiceId
	join [dealer].[InvoiceDetailParts] p (nolock)on p.InvoiceDetailId = d.InvoiceDetailId	
	join [dealer].[DealerCustomers] dc (nolock)on dc.[DealerCustomerId] = i.DealerCustomerId
	join [dealer].[DealerVehicles] dv (nolock)on dv.DealerVehicleId = i.DealerVehicleId
	join [dbo].[Vehicles] v (nolock)on v.[VehicleId] = dv.VehicleId
where i.InvoiceDate between @StartDate and @EndDate
and (@ExclLabor0 = 0 or (@ExclLabor0 = 1 and d.LaborHour = 0))
and (@UserOnly = 0 or (@UserOnly = 1 and u.DmsUserName is not null))
and (@CPOnly = 0 or (@CPOnly=1 and d.[LaborType] = 'CP'))
and (@LaborType = 'All' or (@LaborType is null and d.[LaborType] is null)  or (@LaborType is not null and d.[LaborType]=@LaborType))
--and i.AdvisorCode = 2425
order by AdvisorName


select distinct AdvisorCode, AdvisorName, 
count(InvoiceNumber) InvoiceNumber, 
ROUND(sum(LaborHour),2) LaborHour, 
ROUND(sum(PartSale),2) PartSale,
ROUND(sum(LaborSale),2) LaborSale,  
ROUND((sum(PartSale) + sum(LaborSale)),2) Total,
Round((sum(PartSale) + sum(LaborSale))/count(InvoiceNumber),2) Avg$Ro,
Round(sum(LaborHour)/count(InvoiceNumber),2) AvgHrRo,
CASE
  WHEN sum(LaborHour) = 0 THEN 0
  ELSE Round(sum(LaborSale) / sum(LaborHour),2) 
END as ELR,
LaborType 
into #temp2
from #temp 
--where AdvisorCode = 1943
group by AdvisorCode, AdvisorName, LaborType
order by AdvisorName

select *, 0 LineType 
into #temp3
from #temp2
union
select distinct AdvisorCode, AdvisorName, 
sum(InvoiceNumber) InvoiceNumber, 
round(sum(LaborHour),2) LaborHour, 
round(sum(PartSale),2) PartSale,
round(sum(LaborSale),2) LaborSale,  
round((sum(PartSale) + sum(LaborSale)) ,2) Total,
round(sum(Avg$Ro),2) Avg$Ro ,
round(sum(AvgHrRo),2) AvgHrRo,
round(sum(ELR),2) ELR
, ' ' LaborType
, 1 LineType
from #temp2
group by AdvisorCode, AdvisorName
union
select distinct ' ' AdvisorCode,  'ZZZZ' AdvisorName, 
0 InvoiceNumber, 
0 LaborHour, 
0 PartSale,
0 LaborSale,  
0 Total,
0 Avg$Ro ,
0 AvgHrRo,
0 ELR
, ' ' LaborType
, 2 LineType
from #temp2
group by AdvisorCode, AdvisorName
order by AdvisorName, AdvisorCode, linetype



update t3
set AdvisorCode = ' ', AdvisorName = 'Total'
from #temp3 t3
where LineType = 1

update t3
set t3.AdvisorName = 'Grand Total',	
	t3.InvoiceNumber = m.InvoiceNumber,
	t3.LaborHour = m.LaborHour,
	t3.PartSale = m.PartSale,
	t3.LaborSale = m.LaborSale,
	t3.Total = m.Total,
	t3.Avg$Ro = m.Avg$Ro,
	t3.AvgHrRo = m.AvgHrRo,
	t3.ELR = m.ELR
from #temp3 t3 
join
(
select 
	sum(InvoiceNumber) InvoiceNumber, 
	round(sum(LaborHour),2) LaborHour, 
	round(sum(PartSale),2) PartSale,
	round(sum(LaborSale),2) LaborSale,  
	round((sum(PartSale) + sum(LaborSale)) ,2) Total,
	round(sum(Avg$Ro),2) Avg$Ro ,
	round(sum(AvgHrRo),2) AvgHrRo,
	round(sum(ELR),2) ELR,
	max(2) lineType
 from #temp3 where AdvisorName = 'Total'
 ) m on m.lineType = t3.lineType

select * from #temp3




END


GO
