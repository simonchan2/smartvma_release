USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetPdfBodyTechCopy]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
EXEC dbo.GetPdfBodyTechCopy @AppointmentPresentationId=65 
*/
CREATE PROCEDURE [dbo].[GetPdfBodyTechCopy]
	@AppointmentPresentationId BIGINT
AS
BEGIN
	SELECT aps.AppointmentServicesId, aps.OpCode, aps.OpDescription,
	asp.PartName, ISNULL(asp.Quantity, 0) as Quantity, aps.LaborHour 
	FROM dealer.AppointmentServices aps
	LEFT JOIN dealer.AppointmentServiceParts asp ON aps.AppointmentServicesId = asp.AppointmentServicesId
	WHERE aps.AppointmentPresentationId = @AppointmentPresentationId AND aps.IsStriked=0
END

GO
