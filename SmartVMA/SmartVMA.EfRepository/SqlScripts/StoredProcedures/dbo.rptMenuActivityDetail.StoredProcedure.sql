USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[rptMenuActivityDetail]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[rptMenuActivityDetail]
@DealerCompanyId	int,
@AdvisorCode		nvarchar(45),
@StartDate			datetime,
@EndDate			datetime,
@UserOnly			bit
AS
BEGIN
    SET NOCOUNT ON;
-- MENU ACTIVITY REPORT

select distinct 
	dmsu.[DmsUserNumber] AdvisorCode,
	dmsu.[DmsUserName] AdvisorName, 
	i.InvoiceDate,	
	i.invoicenumber,
	(dc.FirstName + ' ' + dc.LastName) CustomerName,
	i.Mileage,
	isnull(dv.VehicleMake, '') + ' ' + isnull(dv.VehicleModel, '') + ' ' + isnull(cast(dv.VehicleYear as varchar),'') CarDesc,
	(MenuLevel) [MenuLevel],		
	(1) Presented,
	(case when PresentationStatus='MC' then 1 else 0 end) Accepted,
	(case when PresentationStatus='MD' then 1 else 0 end) Declined
	--Status of the meny presenrtation; V: Void; C: Closed; M: Menu Presented; MC: Menu Accepted; MD: Menu Declined; MK: Menu Parked			
into #temp1
from [dealer].[AppointmentPresentations] ap 
join [dealer].[DealerVehicles] dv on dv.[DealerVehicleId] = ap.DealerVehicleId
left join [dealer].[DealerDmsUsers] dmsu on dmsu.UserId = ap.[AdvisorId] 
join [dealer].[Invoices] i (nolock) on i.InvoiceNumber = ap.InvoiceNumber
join [dealer].[DealerCustomers] dc (nolock)on dc.[DealerCustomerId] = i.DealerCustomerId
join [dealer].[InvoiceDetails] d (nolock)on d.InvoiceId = i.InvoiceId
join [dbo].[Vehicles] v (nolock)on v.[VehicleId] = dv.VehicleId
left join [dealer].[InvoiceDetailParts] p (nolock)on p.InvoiceDetailId = d.InvoiceDetailId
where 
[AppointmentTime] between @StartDate and @EndDate
and (@UserOnly = 0 or (@UserOnly = 1 and dmsu.DmsUserName is not null)) 
and (@DealerCompanyId=0 or (@DealerCompanyId<>0 and i.DealerId = @DealerCompanyId))
and dmsu.DmsUserNumber = @AdvisorCode

--select * from #temp1 order by invoicenumber

select 
	AdvisorCode,
	AdvisorName,
	max(InvoiceDate) InvoiceDate,	
	invoicenumber,
	max(CustomerName) CustomerName,
	max(Mileage) Mileage,
	max(CarDesc) CarDesc,
	sum(Presented) Presented, 
	sum(Accepted) Accepted, 
	sum(Accepted) Result, 
	(sum(Accepted) /sum(Presented) * 100) Closing, 
	sum(Declined) Declined, 
	(sum(Presented) - sum(Accepted) - sum(Declined)) UnFinsihed,
	MenuLevel
into #tempAdvisorData
from #temp1 
group by AdvisorCode, AdvisorName, invoicenumber, MenuLevel



select *
into #tempAdvisorData2
from #tempAdvisorData
pivot (count([result]) for MenuLevel in ([1],[2],[3])) as [MenuLevel]


select * from #tempAdvisorData2


END


GO
