USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetVehicleDriveLines]    Script Date: 12/8/2016 4:55:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[GetVehicleDriveLines] 
	@Year nvarchar(max) = null,
    @Make nvarchar(max) = null,
    @Model nvarchar(max) = null,
    @Engine nvarchar(max) = null,
	@Transmission nvarchar(100) = null
AS
BEGIN
	IF(@Year IS NOT NULL AND @Make IS NOT NULL AND @Model IS NOT NULL AND @Engine IS NOT NULL AND @Transmission IS NOT NULL)
	BEGIN
	declare @Delimiter nvarchar(8);
	set @Delimiter =',';

		select distinct vd.VehicleDrivelineId as Id, vd.VehicleDriveline as DriveLine
		from amam.VehicleDrivelines as vd
		inner join amam.VehicleTransmissions as vt on (select * from dbo.SplitDelimiterString(@Transmission, @Delimiter)) = vt.VehicleTransmissionId
		inner join amam.OemComponents as oc on vt.VehicleTransmission = oc.VehicleTransmissionShort
		inner join amam.OemBasicServices as obs on oc.OemComponentId = obs.OemComponentId AND obs.IsDeleted = 0 AND obs.IsIncomplete = 0
		inner join amam.Cars as car on vd.VehicleDriveline = car.VehicleDriveLine and obs.CarId = car.CarId
		INNER JOIN dbo.SplitDelimiterString(@Year, @Delimiter) as carYears ON (carYears.Item = CAST(ISNULL(car.VehicleYear,null) as NVARCHAR(50)))
		where CAST(ISNULL(car.VehicleMakeId,null) as NVARCHAR(50)) in (select * from dbo.SplitDelimiterString(@Make, @Delimiter))
			AND CAST(ISNULL(car.VehicleModelId, null) as NVARCHAR(50)) in (select * from dbo.SplitDelimiterString(@Model, @Delimiter))
			AND CAST(ISNULL(car.VehicleEngineId, null) as NVARCHAR(50)) in (select * from dbo.SplitDelimiterString(@Engine, @Delimiter))
	END
	ELSE
	BEGIN
		select distinct VehicleDrivelineId as Id, VehicleDriveline as DriveLine
		from amam.VehicleDrivelines
	END
END

