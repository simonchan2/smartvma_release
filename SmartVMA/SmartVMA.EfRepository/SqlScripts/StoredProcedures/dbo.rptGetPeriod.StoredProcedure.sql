USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[rptGetPeriod]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[rptGetPeriod]
AS 
BEGIN

SELECT 'Today' periodLabel, 'Today' periodValue, 1 sort
UNION
SELECT 'Yesterday' periodLabel, 'Yesterday' periodValue, 2 sort
UNION
SELECT 'ThisWeek' periodLabel, 'ThisWeek' periodValue, 3 sort
UNION
SELECT 'LastWeek' periodLabel, 'LastWeek' periodValue, 4 sort
UNION
SELECT 'ThisMonth' periodLabel, 'ThisMonth' periodValue, 5 sort
UNION
SELECT 'LastMonth' periodLabel, 'LastMonth' periodValue, 6 sort
UNION
SELECT 'ThisYear' periodLabel, 'ThisYear' periodValue, 7 sort
UNION
SELECT 'LastYear' periodLabel, 'LastYear' periodValue, 8 sort
UNION
SELECT 'Custom' periodLabel, 'Custom' periodValue, 9 sort
order by sort

end





GO
