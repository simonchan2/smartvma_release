USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[SaveOemBasicService]    Script Date: 12/21/2016 6:44:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SaveOemBasicService]
	--@TenantId int,
	@ServiceAsXml nvarchar(max),
	@Years nvarchar(max),
	@Makes nvarchar(max),
	@Models nvarchar(max),
	@EngineTypes nvarchar(max),
	@TransmissionTypes nvarchar(max),
	@DriveLines nvarchar(max)
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION
	BEGIN TRY
	DECLARE @ErrorCode INt = 0;
	DECLARE @Delimiter NVARCHAR(8);
	SET @Delimiter =',';
	DECLARE @OemPartNoPrefix NVARCHAR(30) = 'Oem Part Name of '

	-----------------------------------------------------------------------
	IF OBJECT_ID('tempdb..#CarIds') is not null 
	BEGIN
		DROP TABLE #CarIds;
	END;

	CREATE TABLE #CarIds(Id int null);
	INSERT INTO #CarIds
	SELECT DISTINCT cars.CarId
	FROM GetCarIdByCriteria(@Years, @Makes, @Models, @EngineTypes, @DriveLines, @TransmissionTypes, @Delimiter) cars;

	DECLARE @ServiceXmlObject XML = @ServiceAsXml;
	--get the new oem basic service id from xml
	DECLARE @ServiceId int = (SELECT service.value('Id[1]', 'int') as Id FROM @ServiceXmlObject.nodes('/OemServiceMaintenanceViewModel') as tbl(service));

	--extract new oem basic service data from xml
	DECLARE @XMLOemBasicServices as table (ServiceId int, OpDescription nvarchar(1000), LaborHour float, OemComponentId int, OpAction nvarchar(1000), IsDeleted BIT)
	INSERT INTO @XMLOemBasicServices
	SELECT 
	nobs.value('Id[1]', 'int') as ServiceId,
	nobs.value('Description[1]', 'nvarchar(1000)') AS OpDescription,
	nobs.value('LaborTime[1]', 'float') AS LaborHour,
	nobs.value('OemComponentId[1]', 'int') AS OemComponentId,
	nobs.value('OpAction[1]', 'nvarchar(1000)') AS OpAction,
	nobs.value('IsSelected[1]', 'bit') AS IsDeleted
	FROM @ServiceXmlObject.nodes('/OemServiceMaintenanceViewModel') as tbl(nobs)
	
	IF OBJECT_ID('tempdb..#UpdatedOemBasicServices') is not null 
	BEGIN
		DROP TABLE #UpdatedOemBasicServices;
	END;

	CREATE TABLE #UpdatedOemBasicServices(OemBasicServiceId BIGINT, CarId BIGINT, OemComponentId BIGINT, OpAction NVARCHAR(50), OpDescription NVARCHAR(1000), LaborHour FLOAT, NoOverwrite BIT, TimeCreated DATETIME, TimeUpdated DATETIME, IsCreatedByAdmin BIT, IsDeleted BIT);
	IF(@ServiceId = 0 OR @ServiceId IS NULL)
	BEGIN
		INSERT INTO #UpdatedOemBasicServices
		SELECT xobs.ServiceId AS OemBasicServiceId, c.Id as CarId, xobs.OemComponentId, xobs.OpAction, xobs.OpDescription, xobs.LaborHour, CAST(1 AS BIT) as NoOverwrite, GETUTCDATE() AS TimeCreated, GETUTCDATE() AS TimeUpdated, CAST(1 AS BIT) AS IsCreatedByAdmin, xobs.IsDeleted
		FROM @XMLOemBasicServices xobs
		CROSS JOIN #CarIds as c;
	END
	ELSE
	BEGIN
		INSERT INTO #UpdatedOemBasicServices
		SELECT xobs.ServiceId AS OemBasicServiceId, obs.CarId AS CarId, xobs.OemComponentId, xobs.OpAction, xobs.OpDescription, xobs.LaborHour, CAST(1 AS BIT) as NoOverwrite, GETUTCDATE() AS TimeCreated, GETUTCDATE() AS TimeUpdated, CAST(0 AS BIT) AS IsCreatedByAdmin, xobs.IsDeleted
		FROM @XMLOemBasicServices xobs
		INNER JOIN amam.OemBasicServices obs ON obs.OemBasicServiceId = xobs.ServiceId AND obs.IsIncomplete = 0 AND obs.IsDeleted = 0

		--INSERT INTO #CarIds SELECT service.value('CarId[1]', 'int') FROM @ServiceXmlObject.nodes('/OemServiceMaintenanceViewModel') as tbl(service);
		DELETE FROM amam.OemServices where OemBasicServiceId = @ServiceId;
	END

	DECLARE @XMLOemParts as table (PartId INT, OemPartNo NVARCHAR(MAX) NULL, UnitPrice MONEY NULL, Quantity FLOAT NULL, IsDeleted BIT NULL)
	INSERT INTO @XMLOemParts
	SELECT 
	IIF(nop.value('Id[1]', 'nvarchar(max)') IS NULL OR nop.value('Id[1]', 'nvarchar(max)') = '', 0, nop.value('Id[1]', 'int'))  AS PartId,
	nop.value('PartNumber[1]', 'nvarchar(max)') AS OemPartNo,
	IIF(nop.value('Price[1]', 'nvarchar(max)') IS NULL OR nop.value('Price[1]', 'nvarchar(max)') = '', NULL, nop.value('Price[1]', 'money')) AS UnitPrice,
	IIF(nop.value('Quantity[1]', 'nvarchar(max)') IS NULL OR nop.value('Quantity[1]', 'nvarchar(max)') = '', NULL, nop.value('Quantity[1]', 'float')) as Quantity,
	IIF(nop.value('IsDeleted[1]', 'bit') IS NULL OR nop.value('IsDeleted[1]', 'bit') = CAST(0 AS BIT), 0, 1) AS IsDeleted
	FROM @ServiceXmlObject.nodes('/OemServiceMaintenanceViewModel/Parts/PartItemViewModel') as tbl(nop)

	IF OBJECT_ID('tempdb..#UpdatedOemParts') is not null 
	BEGIN
		DROP TABLE #UpdatedOemParts;
	END;
	CREATE TABLE #UpdatedOemParts(OrderId BIGINT IDENTITY(1,1),OemPartId BIGINT,CarId BIGINT, OemComponentId BIGINT, Quantity FLOAT, IsDeleted BIT, OemPartName NVARCHAR(100), OemPartNo NVARCHAR(50), UnitPrice MONEY, LaborHour FLOAT, TimeCreated DATETIME, TimeUpdated DATETIME, IsCreatedByAdmin BIT, NoOverwrite BIT);

	INSERT INTO #UpdatedOemParts
	SELECT xop.PartId AS OemPartId, uobs.CarId, uobs.OemComponentId, xop.Quantity, xop.IsDeleted, @OemPartNoPrefix + ISNULL(xop.OemPartNo, '') as OemPartName, ISNULL(xop.OemPartNo, ''), xop.UnitPrice, 0 AS LaborHour,GETUTCDATE() AS TimeCreated, GETUTCDATE() AS TimeUpdated,1 AS IsCreatedByAdmin,CAST(1 AS BIT) AS NoOverwrite
	FROM @XMLOemParts xop
	CROSS JOIN #UpdatedOemBasicServices as uobs
	WHERE xop.UnitPrice IS NOT NULL;
	
	--extract selected mileages from xml
	IF OBJECT_ID('tempdb..#Mileages') is not null 
	BEGIN
		DROP TABLE #Mileages;
	END;

	CREATE TABLE #Mileages(MeasurementUnitId BIT, Mileage INT);
	INSERT INTO #Mileages
	SELECT 
	0 AS MeasurementUnitId,
	mileagesM.value('(.)', 'int') AS Mileage
	FROM @ServiceXmlObject.nodes('/OemServiceMaintenanceViewModel/MileagesM/int') as tbl(mileagesM)

	INSERT INTO #Mileages
	SELECT 
	1 AS MeasurementUnitId,
	mileagesKm.value('(.)', 'int') AS Mileage
	FROM @ServiceXmlObject.nodes('/OemServiceMaintenanceViewModel/MileagesKm/int') as tbl(mileagesKm)

	DECLARE @HasDuplicateKeys BIT = (SELECT IIF(COUNT(*) > 0, 1, 0) as HasDuplicateKeys
	FROM #UpdatedOemBasicServices uobs
	INNER JOIN amam.OemBasicServices obs ON 
		obs.CarId = uobs.CarId
	AND obs.OemComponentId = uobs.OemComponentId
	AND obs.OpAction = uobs.OpAction
	AND obs.IsCreatedByAdmin = uobs.IsCreatedByAdmin
	WHERE uobs.OemBasicServiceId = 0)

	IF(@HasDuplicateKeys = CAST(0 AS BIT))
	BEGIN
		
		IF OBJECT_ID('tempdb..#InsertedServices') is not null 
		BEGIN
			DROP TABLE #InsertedServices;
		END;
		CREATE TABLE #InsertedServices (Id int, OemComponentId int, CarId int);
		--MERGE OemBasicServices
		MERGE amam.OemBasicServices AS TARGET
		USING #UpdatedOemBasicServices AS SOURCE 
		ON (TARGET.OemBasicServiceId = SOURCE.OemBasicServiceId) 
		WHEN MATCHED 
		THEN 
		UPDATE SET 
		TARGET.OemComponentId = SOURCE.OemComponentId,
		TARGET.OpAction = SOURCE.OpAction,
		TARGET.OpDescription = SOURCE.OpDescription,
		TARGET.LaborHour = SOURCE.LaborHour,
		TARGET.TimeUpdated = GETUTCDATE(),
		TARGET.NoOverwrite = 1,
		TARGET.IsDeleted = SOURCE.IsDeleted
		WHEN NOT MATCHED BY TARGET THEN 
		INSERT (CarId, OemComponentId, OpAction, OpDescription, LaborHour, NoOverwrite, TimeCreated, TimeUpdated, IsCreatedByAdmin, IsDeleted)
		VALUES (CarId, OemComponentId, OpAction, OpDescription, LaborHour, NoOverwrite, TimeCreated, TimeUpdated, IsCreatedByAdmin, 0)
		OUTPUT INSERTED.OemBasicServiceId, INSERTED.OemComponentId, INSERTED.CarId INTO #InsertedServices;
		
		IF OBJECT_ID('tempdb..#InsertedParts') is not null 
		BEGIN
			DROP TABLE #InsertedParts;
		END;
		CREATE TABLE #InsertedParts (OrderId BIGINT, Id int, OemComponentId int, CarId int, OemPartName nvarchar(500), Quantity float NULL , IsUpdated BIT);
		--MERGE OemParts
		MERGE amam.OemParts AS TARGET
		USING #UpdatedOemParts AS SOURCE 
		ON (TARGET.OemPartId = SOURCE.OemPartId) 
		WHEN MATCHED 
		THEN 
		UPDATE SET 
		TARGET.UnitPrice = SOURCE.UnitPrice,
		TARGET.TimeUpdated = SOURCE.TimeUpdated,
		TARGET.NoOverwrite = SOURCE.NoOverwrite
		WHEN NOT MATCHED BY TARGET THEN 
		INSERT (CarId, OemComponentId, OemPartName, UnitPrice, LaborHour, TimeCreated, TimeUpdated, IsCreatedByAdmin, NoOverwrite, IsDeleted)
		VALUES (SOURCE.CarId, SOURCE.OemComponentId, SOURCE.OemPartName, SOURCE.UnitPrice, SOURCE.LaborHour, SOURCE.TimeCreated, SOURCE.TimeUpdated, SOURCE.IsCreatedByAdmin, SOURCE.NoOverwrite, 0)
		OUTPUT SOURCE.OrderId AS OrderId, INSERTED.OemPartId, INSERTED.OemComponentId, INSERTED.CarId, INSERTED.OemPartName, NULL as Quantity, IIF($action = 'INSERT', 0, 1) as IsUpdated INTO #InsertedParts;
		
	
	
		--UPDATE helper tables
		IF OBJECT_ID('tempdb..#UpdatedOemPartNumbers') is not null 
		BEGIN
			DROP TABLE #UpdatedOemPartNumbers;
		END;
		CREATE TABLE #UpdatedOemPartNumbers(IsUpdated BIT,OemPartId BIGINT, InitialOemPartId BIGINT, CountryId INT, OemPartNo NVARCHAR(50), NoOverwrite BIT, TimeCreated DATETIME, TimeUpdated DATETIME, IsCreatedByAdmin BIT);

		INSERT INTO #UpdatedOemPartNumbers
		SELECT IIF(ip.IsUpdated = 1 AND opn.OemPartNumberId IS NOT NULL, CAST(1 AS BIT),  CAST(0 AS BIT)) AS IsUpdated, ip.Id AS OemPartId, uop.OemPartId as InitialOemPartId, 1 AS CountryId, uop.OemPartNo, CAST(1 AS BIT) AS NoOverwrite, GETUTCDATE() AS TimeCreated, GETUTCDATE() AS TimeUpdated, CAST(1 AS BIT) AS IsCreatedByAdmin
		FROM #UpdatedOemParts uop
		INNER JOIN #InsertedParts ip ON ip.OrderId = uop.OrderId
		LEFT JOIN amam.OemPartNumbers opn ON opn.OemPartId = ip.Id
		WHERE uop.OemPartNo IS NOT NULL

		IF OBJECT_ID('tempdb..#UpdatedOemPartsWithIds') is not null 
		BEGIN
			DROP TABLE #UpdatedOemPartsWithIds;
		END;
		CREATE TABLE #UpdatedOemPartsWithIds(IsUpdated BIT, InitialOemPartId BIGINT, FinalOemPartId BIGINT,CarId BIGINT, OemComponentId BIGINT, Quantity FLOAT, IsDeleted BIT, OemPartName NVARCHAR(100), OemPartNo NVARCHAR(50), UnitPrice MONEY, LaborHour FLOAT, TimeCreated DATETIME, TimeUpdated DATETIME, IsCreatedByAdmin BIT, NoOverwrite BIT);

		INSERT INTO #UpdatedOemPartsWithIds
		SELECT ip.IsUpdated, uop.OemPartId as InitialOemPartId, ip.Id as FinalOemPartId, uop.CarId, uop.OemComponentId, uop.Quantity, uop.IsDeleted, uop.OemPartName, uop.OemPartNo, uop.UnitPrice, uop.LaborHour, uop.TimeCreated, uop.TimeUpdated, uop.IsCreatedByAdmin, uop.NoOverwrite
		FROM #UpdatedOemParts uop
		INNER JOIN #InsertedParts ip ON ip.OrderId = uop.OrderId
			
		----MERGE PartNumbers
		/*MERGE amam.OemPartNumbers AS TARGET
		USING #UpdatedOemPartNumbers AS SOURCE 
		ON (TARGET.OemPartId = SOURCE.OemPartId) 
		WHEN MATCHED 
		THEN 
		UPDATE SET 
		TARGET.OemPartNo = SOURCE.OemPartNo, 
		TARGET.TimeUpdated = SOURCE.TimeUpdated,
		TARGET.NoOverwrite = SOURCE.NoOverwrite
		WHEN NOT MATCHED BY TARGET THEN 
		INSERT (OemPartId, CountryId, OemPartNo, NoOverwrite, TimeCreated, TimeUpdated, IsCreatedByAdmin) 
		VALUES (SOURCE.OemPartId, SOURCE.CountryId, SOURCE.OemPartNo, SOURCE.NoOverwrite, SOURCE.TimeCreated, SOURCE.TimeUpdated, SOURCE.IsCreatedByAdmin);*/


		UPDATE opn
		SET 
		opn.OemPartNo = uopn.OemPartNo, 
		opn.TimeUpdated = uopn.TimeUpdated,
		opn.NoOverwrite = uopn.NoOverwrite
		FROM amam.OemPartNumbers opn
		INNER JOIN #UpdatedOemPartNumbers uopn ON uopn.OemPartId = opn.OemPartId
		WHERE uopn.IsUpdated = 1

		
		INSERT INTO amam.OemPartNumbers
		SELECT OemPartId, CountryId, OemPartNo, NoOverwrite, TimeCreated, TimeUpdated, IsCreatedByAdmin 
		FROM #UpdatedOemPartNumbers uopn
		WHERE uopn.IsUpdated = 0;

		
		-- link services with parts
		INSERT INTO amam.OemServiceParts (OemBasicServiceId, OemPartId, Quantity, TimeCreated, TimeUpdated, NoOverwrite)
		SELECT 
			services.Id as OemBasicServiceId,
			parts.FinalOemPartId as OemPartId,
			parts.Quantity,
			GetUtcDate() AS TimeCreated,
			GetUtcDate() AS TimeUpdated,
			1 as NoOverwrite
			FROM #InsertedServices as services
			CROSS JOIN #UpdatedOemPartsWithIds as parts
			WHERE parts.IsUpdated = 0
			AND parts.CarId = services.CarId
			AND parts.OemComponentId = services.OemComponentId
			AND parts.Quantity IS NOT NULL
			AND parts.IsDeleted = 0

		--Update Quantity in OemServiceParts if any changes have been made
		UPDATE amam.OemServiceParts
		SET
			Quantity = parts.Quantity,
			IsDeleted = parts.IsDeleted,
			TimeUpdated = GetUtcDate()
		FROM #InsertedServices as services
			CROSS JOIN #UpdatedOemPartsWithIds as parts
		WHERE parts.IsUpdated = 1
			AND parts.CarId = services.CarId 
			AND parts.OemComponentId = services.OemComponentId
			AND OemBasicServiceId = services.Id
			AND OemPartId = parts.FinalOemPartId
			AND parts.Quantity IS NOT NULL;


		--Filter OemMenuIds by the selected mileages 
		IF OBJECT_ID('tempdb..#FilteredByMileage') is not null 
		BEGIN
			DROP TABLE #FilteredByMileage;
		END;

		CREATE TABLE #FilteredByMileage(OemMenuId BIGINT, OemMenuListId BIGINT);
		INSERT INTO #FilteredByMileage(OemMenuId, OemMenuListId)
		SELECT DISTINCT om.OemMenuId, om.OemMenuListId
			FROM amam.OemMenus om
			INNER JOIN amam.OemMenuMileages omm ON omm.OemMenuId = om.OemMenuId
			INNER JOIN #Mileages m ON m.Mileage = omm.Mileage AND m.MeasurementUnitId = omm.MeasurementUnitId

		--Filter the OemMenuIds by the selected cars
		IF OBJECT_ID('tempdb..#FilteredByCar') is not null 
		BEGIN
			DROP TABLE #FilteredByCar;
		END;

		CREATE TABLE #FilteredByCar(OemMenuListId BIGINT, CarId BIGINT);
		INSERT INTO #FilteredByCar(OemMenuListId, CarId)
		SELECT DISTINCT oml.OemMenuListId, oml.CarId
			FROM amam.OemMenuLists oml
			WHERE oml.CarId IN (SELECT Id FROM #CarIds);

		IF OBJECT_ID('tempdb..#FilteredOemMenus') is not null 
		BEGIN
			DROP TABLE #FilteredOemMenus;
		END;

		CREATE TABLE #FilteredOemMenus(OemMenuId BIGINT, CarId BIGINT);
		INSERT INTO #FilteredOemMenus(OemMenuId, CarId)
		SELECT DISTINCT fbm.OemMenuId, fbc.CarId
			FROM #FilteredByMileage fbm
			INNER JOIN #FilteredByCar fbc ON fbc.OemMenuListId = fbm.OemMenuListId;

		--Join the inserted services with the filtered menus and insert the result in the junction table OemServices
		INSERT INTO amam.OemServices (OemMenuId, OemBasicServiceId, TimeCreated, TimeUpdated, NoOverwrite)
		SELECT 
		fom.OemMenuId,
		services.Id as OemBasicServiceId,
		GetUtcDate() as TimeCreated,
		GetUtcDate() as TimeUpdated,
		1 as NoOverwrite
		FROM #InsertedServices as services
		INNER JOIN #FilteredOemMenus fom ON fom.CarId = services.CarId;

	END
	ELSE
	BEGIN
		SET @ErrorCode = 1;
	END

	COMMIT TRANSACTION
	SELECT @ErrorCode;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		SET @ErrorCode = IIF((SELECT CHARINDEX('Cannot insert duplicate key row in object' , (SELECT ERROR_MESSAGE()))) > 0, 1, 2)
		SELECT @ErrorCode;
		--SELECT ERROR_MESSAGE()
	END CATCH
END

/*
exec dbo.SaveOemBasicService @ServiceAsXml=N'<?xml version="1.0" encoding="utf-16"?><OemServiceMaintenanceViewModel xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><Id xsi:nil="true" /><Description>NEW BOBI SERVICE TEST</Description><IsLof>true</IsLof><LaborTime>7</LaborTime><OemComponentId>402</OemComponentId><MileagesKm><int>4000</int><int>4800</int><int>6000</int><int>8000</int><int>9600</int><int>10000</int><int>11200</int><int>12000</int><int>12800</int><int>14400</int><int>16000</int><int>18000</int><int>19200</int><int>20000</int><int>20800</int><int>22400</int><int>24000</int><int>25600</int><int>28000</int><int>28800</int><int>30000</int><int>32000</int><int>33600</int><int>36000</int><int>36800</int><int>38400</int><int>40000</int><int>41600</int><int>42000</int><int>43200</int><int>44000</int><int>44800</int><int>48000</int><int>49600</int><int>50000</int><int>51200</int><int>52000</int><int>52800</int><int>54000</int><int>56000</int><int>57600</int><int>59200</int><int>60000</int><int>60800</int><int>62400</int><int>64000</int><int>66000</int><int>67200</int><int>68000</int><int>68800</int><int>70000</int><int>70400</int><int>72000</int><int>74400</int><int>76000</int><int>76800</int><int>78000</int><int>78400</int><int>80000</int><int>81600</int><int>83200</int><int>84000</int><int>84800</int><int>86400</int><int>88000</int><int>88800</int><int>89600</int><int>90000</int><int>91200</int><int>92000</int><int>92800</int><int>96000</int><int>99200</int><int>100000</int><int>100800</int><int>102000</int><int>102400</int><int>104000</int><int>105600</int><int>108000</int><int>108800</int><int>110000</int><int>110400</int><int>112000</int><int>114000</int><int>115200</int><int>116000</int><int>116800</int><int>118400</int><int>120000</int><int>121600</int><int>123200</int><int>124000</int><int>124800</int><int>126000</int><int>128000</int><int>129600</int><int>130000</int><int>132000</int><int>132800</int><int>134400</int><int>136000</int><int>138000</int><int>139200</int><int>140000</int><int>140800</int><int>144000</int><int>145600</int><int>147200</int><int>148000</int><int>148800</int><int>150000</int><int>152000</int><int>153600</int><int>156000</int><int>156800</int><int>158400</int><int>160000</int><int>162000</int><int>163200</int><int>164000</int><int>164800</int><int>166400</int><int>168000</int><int>170000</int><int>172000</int><int>172800</int><int>173600</int><int>174000</int><int>176000</int><int>177600</int><int>179200</int><int>180000</int><int>180800</int><int>182400</int><int>184000</int><int>185600</int><int>186000</int><int>187200</int><int>188000</int><int>188800</int><int>190000</int><int>190400</int><int>192000</int><int>196000</int><int>196800</int><int>198000</int><int>198400</int><int>200000</int><int>201600</int><int>204000</int><int>204800</int><int>206400</int><int>207200</int><int>208000</int><int>210000</int><int>211200</int><int>212000</int><int>212800</int><int>216000</int><int>217600</int><int>220000</int><int>220800</int><int>222000</int><int>223200</int><int>224000</int><int>225600</int><int>228000</int><int>228800</int><int>230000</int><int>230400</int><int>232000</int><int>234000</int><int>235200</int><int>236000</int><int>236800</int><int>240000</int><int>248000</int><int>256000</int><int>272000</int><int>372000</int><int>496000</int><int>620000</int><int>744000</int><int>868000</int><int>992000</int><int>1116000</int><int>1240000</int><int>1364000</int><int>1488000</int><int>1612000</int><int>1736000</int><int>1860000</int><int>1984000</int><int>2108000</int><int>2232000</int><int>2356000</int><int>2480000</int><int>2604000</int></MileagesKm><MileagesM><int>2500</int><int>3000</int><int>3750</int><int>5000</int><int>6000</int><int>6250</int><int>7000</int><int>7500</int><int>8000</int><int>9000</int><int>10000</int><int>11250</int><int>12000</int><int>12500</int><int>13000</int><int>14000</int><int>15000</int><int>16000</int><int>17500</int><int>18000</int><int>18750</int><int>20000</int><int>21000</int><int>22500</int><int>23000</int><int>24000</int><int>25000</int><int>26000</int><int>26250</int><int>27000</int><int>27500</int><int>28000</int><int>30000</int><int>31000</int><int>31250</int><int>32000</int><int>32500</int><int>33000</int><int>33750</int><int>35000</int><int>36000</int><int>37000</int><int>37500</int><int>38000</int><int>39000</int><int>40000</int><int>41250</int><int>42000</int><int>42500</int><int>43000</int><int>43750</int><int>44000</int><int>45000</int><int>46500</int><int>47500</int><int>48000</int><int>48750</int><int>49000</int><int>50000</int><int>51000</int><int>52000</int><int>52500</int><int>53000</int><int>54000</int><int>55000</int><int>55500</int><int>56000</int><int>56250</int><int>57000</int><int>57500</int><int>58000</int><int>60000</int><int>62000</int><int>62500</int><int>63000</int><int>63750</int><int>64000</int><int>65000</int><int>66000</int><int>67500</int><int>68000</int><int>68750</int><int>69000</int><int>70000</int><int>71250</int><int>72000</int><int>72500</int><int>73000</int><int>74000</int><int>75000</int><int>76000</int><int>77000</int><int>77500</int><int>78000</int><int>78750</int><int>80000</int><int>81000</int><int>81250</int><int>82500</int><int>83000</int><int>84000</int><int>85000</int><int>86250</int><int>87000</int><int>87500</int><int>88000</int><int>90000</int><int>91000</int><int>92000</int><int>92500</int><int>93000</int><int>93750</int><int>95000</int><int>96000</int><int>97500</int><int>98000</int><int>99000</int><int>100000</int><int>101250</int><int>102000</int><int>102500</int><int>103000</int><int>104000</int><int>105000</int><int>106250</int><int>107500</int><int>108000</int><int>108500</int><int>108750</int><int>110000</int><int>111000</int><int>112000</int><int>112500</int><int>113000</int><int>114000</int><int>115000</int><int>116000</int><int>116250</int><int>117000</int><int>117500</int><int>118000</int><int>118750</int><int>119000</int><int>120000</int><int>122500</int><int>123000</int><int>123750</int><int>124000</int><int>125000</int><int>126000</int><int>127500</int><int>128000</int><int>129000</int><int>129500</int><int>130000</int><int>131250</int><int>132000</int><int>132500</int><int>133000</int><int>135000</int><int>136000</int><int>137500</int><int>138000</int><int>138750</int><int>139500</int><int>140000</int><int>141000</int><int>142500</int><int>143000</int><int>143750</int><int>144000</int><int>145000</int><int>146250</int><int>147000</int><int>147500</int><int>148000</int><int>150000</int><int>155000</int><int>160000</int><int>170000</int><int>232500</int><int>310000</int><int>387500</int><int>465000</int><int>542500</int><int>620000</int><int>697500</int><int>775000</int><int>852500</int><int>930000</int><int>1007500</int><int>1085000</int><int>1162500</int><int>1240000</int><int>1317500</int><int>1395000</int><int>1472500</int><int>1550000</int><int>1627500</int></MileagesM><Parts><PartItemViewModel><Id xsi:nil="true" /><PartNumber>7</PartNumber><Quantity>7</Quantity><Price>7</Price><IsFluid>true</IsFluid></PartItemViewModel></Parts><OpAction>RESEAL</OpAction></OemServiceMaintenanceViewModel>',@Years=N'2017,2016,2015,2014,2013,2012,2011,2010,2009,2008,2007,2006,2005,2004,2003,2002',@Makes=N'1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69',@Models=N'1265,1266,1267,1268,1406,1408,1576,1953,1954,1496,1565,2073,2112,2113,2159,2176,2177,325,462,463,464,465,466,467,468,469,470,471,502,503,588,608,706,789,886,935,982,1172,1173,1269,1270,1271,1272,1273,1274,1275,1278,1279,1280,1281,1282,1380,1395,1396,1397,1398,1399,1400,1502,1503,1753,1754,1755,1817,1818,1846,1879,1880,1955,1956,1957,1958,1969,1990,2031,2032,2033,2034,2035,2036,2263,2266,2267,86,87,228,229,230,231,2037,2038,2039,2040,2087,2088,2160,2161,2162,2163,2164,2165,476,477,478,479,480,792,793,794,1006,1015,1276,1277,1283,1284,1285,1286,1390,1391,1392,1393,1394,1473,1505,1506,1507,1508,1509,1510,1511,1512,1601,1605,1775,1937,1938,1976,1978,2041,2042,2127,2225,2226,2325,242,267,713,871,872,1287,1288,1577,1578,1603,1697,1698,1869,1899,1900,1901,2043,2044,2045,671,691,788,923,1299,1300,1304,1305,1474,1483,1484,1513,1514,1527,1528,1913,2166,2268,2269,2270,2271,2272,330,331,364,371,372,483,484,485,676,677,678,679,694,902,903,961,962,1346,1351,1352,1353,1354,1355,1356,1357,1358,1359,1360,1361,1362,1363,1364,1365,1366,1367,1368,1369,1370,1371,1372,1373,1374,1375,1376,1377,1378,1384,1385,1386,1482,1497,1498,1566,1567,1571,1572,1806,1807,1927,1928,1932,1933,1934,1935,2027,2028,2029,2119,2134,2192,2193,2331,2332,2333,2334,2335,2336,17,36,71,101,103,123,134,140,143,163,201,202,203,204,205,208,210,218,219,333,500,611,612,613,618,619,620,621,639,640,641,642,695,696,747,748,749,756,757,768,791,819,820,821,822,823,851,852,857,882,936,937,1010,1069,1070,1121,1128,1129,1143,1237,1515,1516,1517,1518,1554,1555,1606,1615,1616,1617,1618,1619,1621,1622,1624,1626,1627,1628,1630,1631,1640,1644,1645,1699,1700,1701,1702,1703,1705,1706,1709,1710,1714,1715,1716,1721,1722,1723,1725,1726,1731,1732,1824,1825,1826,1881,1882,1906,1907,1925,1926,1946,1979,2056,2083,2084,2142,2143,2145,2146,2154,2259,2293,2294,2295,2299,2300,2307,2349,2350,55,56,73,119,120,121,122,148,149,247,268,269,270,403,404,405,458,527,528,529,580,581,629,735,1038,1083,1324,1325,1347,1348,1349,1350,1493,1607,1608,1785,1847,1848,1898,1929,1930,1943,1970,2186,2191,2195,2330,6,7,9,10,18,24,43,44,46,47,48,65,66,67,75,78,79,80,88,91,102,104,105,109,114,117,124,125,144,145,151,187,212,213,214,215,216,217,262,263,292,293,294,295,337,338,339,349,376,378,379,380,381,382,421,422,442,486,493,494,495,501,505,510,546,549,559,560,576,577,578,654,658,659,660,661,685,686,716,717,718,719,724,776,777,778,779,780,781,787,796,804,805,806,818,865,878,879,880,881,889,892,893,894,895,896,918,921,925,926,951,1001,1002,1018,1019,1020,1021,1022,1023,1029,1030,1033,1037,1046,1057,1071,1072,1077,1078,1079,1080,1088,1104,1105,1132,1138,1140,1154,1155,1156,1161,1162,1164,1165,1204,1205,1215,1216,1217,1218,1232,1233,1245,1246,1247,1248,1252,1253,1254,1255,1256,1257,1306,1307,1329,1340,1341,1342,1343,1379,1383,1388,1389,1420,1485,1486,1499,1500,1501,1529,1530,1531,1532,1533,1534,1535,1542,1550,1551,1552,1564,1573,1590,1641,1642,1651,1652,1653,1654,1736,1737,1744,1745,1746,1747,1756,1757,1758,1768,1769,1773,1774,1776,1777,1778,1779,1780,1781,1802,1804,1805,1822,1852,1853,1854,1855,1856,1857,1858,1865,1866,1870,1871,1892,1908,1909,1916,1921,1922,1931,1952,1959,1983,1984,1985,1986,1987,1988,1989,2030,2057,2058,2059,2060,2061,2071,2072,2122,2155,2156,2157,2158,2190,2194,2196,2197,2198,2203,2204,2205,2231,2232,2233,2236,2238,2245,2246,2258,2260,2261,2276,2277,2278,2279,2280,2281,2329,33,64,76,77,110,248,249,260,261,327,368,369,437,450,451,606,607,993,2133,2153,4,20,23,32,45,90,94,95,111,112,113,153,160,184,185,255,266,271,272,273,287,288,289,290,291,299,300,301,304,305,306,360,383,384,385,386,387,388,391,392,406,423,438,439,440,441,455,456,457,530,617,668,669,670,687,688,721,722,775,784,801,802,803,807,813,814,815,824,825,826,827,828,873,874,885,890,891,907,911,912,913,919,922,947,1024,1025,1026,1027,1028,1034,1035,1055,1056,1059,1068,1075,1087,1106,1107,1118,1119,1120,1125,1157,1158,1159,1160,1171,1221,1222,1230,1231,1260,1335,1338,1339,1382,1432,1433,1434,1435,1436,1437,1438,1448,1536,1537,1538,1539,1540,1553,1568,1649,1763,1764,1808,1809,1810,1811,1812,1813,1814,1815,1816,1827,1849,1850,1894,1895,1904,1905,1910,1923,1924,1971,1972,2013,2050,2062,2063,2064,2065,2066,2082,2140,2141,2199,2200,2201,2202,2230,13,38,96,97,279,280,281,282,283,285,286,318,319,320,321,393,395,407,408,409,410,452,453,454,460,461,534,535,536,537,652,653,693,704,711,731,762,763,798,799,800,856,901,927,928,933,934,983,1051,1052,1053,1054,1058,1061,1062,1108,1109,1145,1146,1311,1312,1315,1402,1403,1404,1407,1794,1795,1796,1797,1798,1799,1889,1890,1891,1912,1917,1918,1919,1920,1991,1992,1995,2008,2009,2234,2235,2289,2290,2326,22,30,39,40,81,82,89,93,106,107,116,186,250,251,252,344,355,416,524,544,545,597,598,599,600,601,664,723,750,751,760,761,767,946,980,981,988,1000,1009,1050,1063,1092,1101,1135,1139,1168,1195,1196,1219,1223,1225,1234,1317,1318,1330,1344,1345,1421,1422,1427,1440,1441,1442,1466,1480,1547,1569,1575,1612,1613,1661,1667,1668,1669,1676,1677,1820,1832,1864,1963,1964,1965,1966,2016,2017,2026,2067,2089,2090,2151,2152,2214,2215,2218,2219,2220,2221,2224,2229,2251,2252,2253,2254,2255,2256,2283,2286,2308,2309,2310,2315,2316,2317,2318,2319,554,555,648,649,909,910,976,992,1411,2227,2228,1011,1012,1181,1182,1183,1184,1541,1792,1967,2075,2086,2240,2241,2262,12,63,310,370,401,506,543,571,573,574,575,626,650,712,733,734,989,990,1039,1197,1226,1387,1412,1413,1591,1592,1837,1893,2025,2239,2282,1174,1326,1327,1548,1549,1610,1611,1650,1659,1662,1663,1670,1671,1678,1679,1680,1681,1682,1683,1684,1685,1867,1868,2149,2150,49,50,51,68,69,70,126,127,146,377,508,509,511,579,1490,1491,2111,42,52,118,206,207,209,211,284,316,396,427,428,429,430,431,432,433,525,582,583,584,585,586,587,602,603,604,605,636,637,638,697,698,736,855,860,905,916,945,991,1103,1136,1227,1228,1336,1337,1381,1449,1453,1454,1461,1580,1790,1791,2118,8,27,28,31,34,83,92,100,115,156,157,192,193,194,243,253,254,274,275,276,277,278,314,315,317,322,323,334,345,346,402,459,472,481,504,526,531,532,533,547,572,663,665,666,680,681,682,683,684,705,709,710,766,773,797,853,854,929,930,931,932,957,958,971,984,985,987,999,1005,1007,1008,1016,1047,1102,1110,1147,1148,1149,1150,1166,1167,1220,1224,1244,1259,1291,1292,1293,1294,1295,1301,1302,1313,1314,1331,1417,1418,1443,1444,1445,1446,1543,1544,1545,1546,1596,1600,1604,1784,1800,1833,1838,1885,1886,1914,1915,1944,1945,1997,2021,2022,2046,2074,2092,2093,2167,2168,2169,2170,2175,2188,2189,2216,2217,2237,2244,2247,2248,2250,2264,2265,1,131,258,259,424,425,426,444,445,1141,1142,1405,1597,1598,1599,1664,1665,1686,1687,1688,1689,1749,1821,1836,1982,2091,2136,2137,2138,2139,2187,139,220,221,523,538,539,540,541,542,623,645,732,737,738,906,965,1122,1191,1192,1235,1236,1562,1563,1828,1830,26,57,62,84,85,197,198,199,222,223,224,225,244,245,246,332,411,434,548,589,628,720,770,861,897,1073,1074,1084,1085,1175,1470,1471,1472,1494,1495,1648,1655,1656,1657,1658,1672,1673,1674,1675,1690,1691,1692,1693,1694,1695,1696,1734,1793,1803,1823,1859,1860,1861,1862,1863,1873,1874,1875,1876,1877,1878,2018,2019,2020,2128,2311,2328,2346,21,200,227,347,348,764,765,864,970,978,979,1632,1633,1998,1999,2000,2001,2002,2003,2004,2005,2006,2007,2010,2011,2012,2080,2081,2129,2130,2131,2132,2147,2148,2305,2306,2351,3,98,99,132,135,136,137,152,154,155,171,172,173,174,175,176,236,237,238,239,340,341,342,343,518,519,522,567,609,610,622,758,759,808,809,810,811,858,859,875,887,952,968,969,977,1013,1014,1036,1048,1049,1064,1065,1066,1067,1086,1130,1131,1144,1169,1170,1176,1193,1194,1199,1200,1201,1202,1214,1261,1262,1319,1332,1414,1415,1447,1450,1451,1460,1488,1489,1609,1634,1635,1643,1647,1660,1666,1708,1712,1727,1728,1752,1783,1788,1841,1842,1947,1951,1968,1973,1974,1977,1994,2023,2054,2055,2069,2070,2106,2114,2115,2120,2121,2123,2135,2284,2285,2287,2288,2298,5,16,19,29,35,37,41,53,54,58,59,60,61,72,74,128,129,130,138,147,161,162,166,167,168,169,170,178,179,180,183,195,196,240,241,256,296,297,298,307,308,309,328,329,352,353,354,356,357,358,359,363,365,366,373,389,390,394,397,398,399,400,446,447,448,449,487,488,490,492,496,497,498,499,507,520,521,550,551,552,553,570,590,591,592,593,594,595,596,614,615,616,624,625,627,643,644,662,667,689,690,700,725,726,727,741,744,745,746,774,782,783,785,786,790,816,817,829,830,831,834,835,836,837,838,839,840,841,842,848,849,866,867,868,876,877,888,908,914,915,917,920,938,939,940,941,948,949,950,959,960,963,964,966,972,975,994,996,1003,1017,1031,1032,1040,1041,1042,1081,1082,1089,1090,1091,1093,1094,1095,1096,1097,1098,1099,1100,1111,1112,1113,1114,1115,1116,1117,1124,1126,1127,1137,1151,1152,1153,1163,1185,1186,1206,1207,1208,1209,1242,1243,1251,1258,1263,1316,1333,1334,1401,1409,1410,1425,1426,1439,1452,1492,1556,1557,1558,1559,1574,1579,1581,1582,1583,1584,1585,1586,1587,1588,1589,1595,1620,1623,1625,1629,1704,1707,1711,1713,1717,1718,1719,1720,1724,1729,1730,1733,1762,1765,1767,1770,1782,1801,1835,1851,1883,1884,1902,1903,1936,1948,1949,1950,1960,1961,1962,1996,2014,2015,2024,2068,2076,2077,2078,2079,2096,2102,2105,2126,2144,2185,2206,2207,2208,2209,2210,2211,2212,2213,2222,2249,2257,2296,2297,2301,2327,2337,2338,2339,2340,924,14,15,25,141,142,150,158,159,164,165,226,232,233,234,235,312,313,324,326,335,336,350,351,361,362,512,513,514,515,516,517,556,557,558,568,569,630,631,632,633,634,635,701,702,714,715,728,729,730,769,862,863,904,953,954,967,997,998,1134,1187,1188,1198,1238,1239,1240,1241,1320,1321,1322,1323,1416,1458,1459,1560,1561,1593,1594,1614,1636,1637,1638,1639,1646,1735,1738,1739,1740,1741,1742,1743,1748,1786,1787,1834,1980,1981,2107,2108,2109,2110,2116,2117,2124,2125,2302,2303,2304,2312,2313,2314,955,956,1133,1829,11,108,188,189,257,264,265,311,374,417,418,419,420,443,489,491,651,655,656,699,740,752,753,832,833,850,883,884,900,942,943,944,973,1043,1044,1045,1076,1123,1189,1190,1210,1211,1212,1308,1309,1464,1570,1750,1751,1789,1819,1839,1840,1975,1993,2051,2052,2053,2223,2242,2243,2341,2342,2343,2,133,177,1249,1423,1424,1831,2094,2095,898,899,1229,190,191,302,303,367,412,413,414,415,435,436,561,562,563,564,565,566,646,647,657,672,673,674,675,692,707,708,739,742,743,754,755,771,772,812,869,870,974,1004,1177,1203,1213,1250,1264,1310,1328,1419,1428,1429,1430,1462,1463,1465,1467,1468,1469,1475,1476,1477,1481,1487,1519,1520,1521,1522,1523,1524,1525,1526,1759,1760,1761,1766,1843,1844,1845,1872,1887,1888,1896,1897,1911,1939,1940,1941,1942,2047,2048,2049,2085,2097,2098,2099,2100,2101,2103,2104,2171,2172,2173,2174,2178,2179,2180,2181,2182,2183,2184,2273,2274,2275,2291,2292,2320,2321,2322,2323,2324,2344,2345,2347,2348,181,182,375,843,844,845,846,847,995,1060,1178,1179,1180,1303,1455,1456,1457,1771,1772,473,474,475,482,703,795,986,1289,1290,1296,1297,1298,1431,1478,1479,1504,1602',@EngineTypes=N'362,81,830,838,878,881,928,970,971,981,1001,1030,1053,1070,1076,1107,80,89,136,137,142,186,197,209,316,319,320,418,436,437,492,795,804,868,885,918,998,1136,3,4,6,7,9,10,14,15,18,19,23,24,28,34,42,43,47,48,50,51,52,53,54,55,59,60,61,62,63,67,70,71,75,83,87,97,98,105,106,110,112,118,121,140,141,143,144,145,146,148,149,150,153,155,156,158,159,160,161,162,163,164,165,170,177,178,179,180,182,184,189,193,198,200,201,202,204,205,206,207,211,212,214,220,221,227,230,231,232,233,238,239,243,245,252,253,256,257,258,261,262,263,264,266,270,284,288,291,292,300,322,323,324,325,326,327,328,329,331,332,333,335,336,337,338,339,340,341,345,346,347,348,349,350,357,359,361,363,368,371,372,373,374,375,376,377,378,379,381,383,385,393,396,398,399,400,401,402,403,408,409,410,415,420,421,422,429,431,432,438,445,447,449,451,453,462,463,468,480,481,488,489,507,510,513,514,515,516,517,520,521,522,523,524,525,528,531,537,538,539,540,541,542,545,546,554,555,556,557,567,571,572,573,574,576,578,589,591,592,593,594,599,603,604,607,609,612,622,623,643,644,645,653,654,659,663,665,666,669,670,671,672,678,679,685,688,691,696,711,712,713,714,715,716,717,720,723,730,731,732,735,737,738,740,741,743,744,745,748,752,753,754,755,756,757,758,759,760,763,765,766,767,768,770,788,789,799,801,802,806,807,808,809,810,811,812,813,814,816,818,819,820,821,824,826,827,828,829,833,844,846,847,849,851,859,861,863,864,865,869,874,877,879,880,889,890,891,892,896,897,899,900,901,902,903,908,909,910,912,915,919,920,921,922,924,925,927,929,930,931,932,935,936,937,943,944,945,949,950,951,952,954,974,975,976,977,978,979,980,984,985,989,990,991,992,994,995,996,997,999,1000,1004,1005,1006,1017,1018,1019,1021,1022,1024,1025,1026,1028,1029,1031,1035,1036,1037,1038,1039,1040,1041,1043,1046,1049,1051,1052,1054,1059,1060,1061,1062,1063,1064,1065,1066,1067,1068,1069,1071,1072,1077,1078,1086,1089,1090,1091,1092,1094,1095,1097,1098,1099,1100,1102,1103,1104,1105,1106,1111,1112,1114,1115,1116,1119,1123,1125,1126,1129,1130,1131,1132,1133,1134,1138,1140,1141,1142,1,2,5,8,11,12,13,16,17,20,21,22,25,26,27,29,30,31,32,33,35,36,37,38,39,40,41,44,45,46,49,56,57,58,64,65,66,68,69,72,73,74,76,77,78,79,82,84,85,86,88,90,91,92,93,94,95,96,99,100,101,102,103,104,107,108,109,111,113,114,115,116,117,119,120,122,123,124,125,126,127,128,129,130,131,132,133,134,135,138,139,147,151,152,154,157,166,167,168,169,171,172,173,174,175,176,181,183,185,187,188,190,191,192,194,195,196,199,203,208,210,213,215,216,217,218,219,222,223,224,225,226,228,229,234,235,236,237,240,241,242,244,246,247,248,249,250,251,254,255,259,260,265,267,268,269,271,272,273,274,275,276,277,278,279,280,281,282,283,285,286,287,289,290,293,294,295,296,297,298,299,301,302,303,304,305,306,307,308,309,310,311,312,313,314,315,317,318,321,334,342,343,344,351,352,353,354,355,356,358,360,364,365,366,367,369,370,380,382,384,386,387,388,389,390,391,392,394,395,404,405,406,407,411,412,413,414,416,417,419,423,424,425,426,427,428,430,433,434,435,439,440,441,442,443,444,446,448,450,452,454,456,457,458,459,460,461,464,465,466,467,469,470,471,472,473,474,475,476,477,478,479,482,483,484,485,486,487,490,491,493,494,495,496,497,498,499,500,501,502,503,504,505,506,508,509,511,512,518,519,527,529,530,532,533,534,535,536,543,544,547,548,549,550,551,552,553,558,559,560,561,562,563,564,565,566,568,569,570,575,577,579,580,581,582,583,584,585,586,587,588,590,595,596,597,598,600,601,602,605,606,608,610,611,613,614,615,616,617,618,619,620,621,624,625,626,627,628,629,630,631,632,633,634,635,636,637,638,639,640,641,642,646,647,648,649,650,651,652,655,656,657,658,660,661,662,664,667,668,673,674,675,676,677,680,681,682,683,684,686,687,689,690,692,693,694,695,697,698,699,700,701,702,703,704,705,706,707,708,709,710,718,719,721,722,724,725,726,727,728,729,733,734,736,739,742,746,747,749,750,751,761,762,764,769,771,772,773,774,775,776,777,778,779,780,781,782,783,784,785,786,787,790,791,792,793,794,796,797,798,800,803,805,815,817,822,823,825,831,832,834,835,836,837,839,840,841,842,843,845,848,850,852,853,854,855,856,857,858,860,866,867,870,871,872,873,875,876,882,883,884,886,887,888,893,894,895,898,904,905,906,907,911,913,914,916,917,923,926,933,934,938,939,940,941,942,946,947,948,953,955,956,957,958,959,960,961,962,963,964,965,966,967,968,969,972,973,982,983,986,987,988,993,1002,1003,1007,1008,1009,1010,1011,1012,1013,1014,1015,1016,1020,1023,1027,1032,1033,1034,1042,1044,1045,1047,1048,1050,1055,1057,1058,1073,1074,1075,1079,1080,1081,1082,1083,1084,1085,1087,1088,1093,1096,1101,1108,1109,1110,1113,1117,1118,1120,1121,1122,1124,1127,1128,1135,1137,1139,330,397,455,526,862,1056',@TransmissionTypes=N'10,49,50,75,77,1,3,4,23,25,29,32,38,44,52,56,57,58,63,64,76,8,12,14,17,20,36,48,54,60,61,62,66,67,68,70,74,79,2,13,16,18,22,24,28,31,45,69,72,81,9,19,33,59,6,34,41,21,43,82,39,46,65,80,84,42',@DriveLines=N'4,5,16,18,7,10,19,8,9,12,14'
go

	exec SaveOemBasicService @ServiceAsXml=N'<?xml version="1.0" encoding="utf-16"?><OemServiceMaintenanceViewModel xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><Id xsi:nil="true" /><Description>TEST 1031-10102016</Description><IsLof>false</IsLof><LaborTime>5</LaborTime><Price>5</Price><OemComponentId>27</OemComponentId><MileagesKm /><MileagesM /><Parts><PartItemViewModel><Id xsi:nil="true" /><PartNumber>5</PartNumber><Quantity>5</Quantity><Price>5</Price><IsFluid>false</IsFluid></PartItemViewModel></Parts><OpAction>TUNE-UP</OpAction></OemServiceMaintenanceViewModel>',@Years=N'2017',@Makes=N'4',@Models=N'2345',@EngineTypes=N'806',@TransmissionTypes=N'5',@DriveLines=N'6'
*/
