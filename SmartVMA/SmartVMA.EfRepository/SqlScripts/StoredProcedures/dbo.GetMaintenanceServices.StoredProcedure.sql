USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetMaintenanceServices]    Script Date: 06.09.2016 12:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Krste Bozinoski
-- Create date: 06.07.2016
-- Description:	List all maintenance services for specific dealer vehicle id and chosen mileage
-- =============================================
CREATE PROCEDURE [dbo].[GetMaintenanceServices]
    (
      @TenantId INT,
      @CarId INT = NULL,
      @DealerVehicleId INT = NULL,
      @Mileage INT = NULL,
      @IsLOF BIT = NULL,
      @VehicleTransmission INT = NULL,
      @VehicleDriveLine INT = NULL
    )
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
	
        DECLARE @LaborRate FLOAT;
        SET @LaborRate = ISNULL(( SELECT TOP 1
                                            CAST(Value AS FLOAT)
                                  FROM      CompanySettings
                                  WHERE     CompanyId = @TenantId
                                            AND Name = 'DefaultLaborRate'
                                ), 0);

        DECLARE @MeasurementUnitId INT;
        SET @MeasurementUnitId = ( SELECT TOP 1
                                            CAST(Value AS INT)
                                   FROM     CompanySettings
                                   WHERE    CompanyId = @TenantId
                                            AND Name = 'MeasurementUnit'
                                 );

        DECLARE @LanguageId INT;
        SET @LanguageId = ( SELECT TOP 1
                                    CAST(Value AS INT)
                            FROM    CompanySettings
                            WHERE   CompanyId = @TenantId
                                    AND Name = 'Language'
                          );
        SET @LanguageId = @LanguageId;    -- values: English = 1,        French = 2,        Spanish = 3 
	 

        DECLARE @includeShopCharges BIT;
        DECLARE @shopChargeAmount FLOAT;
        DECLARE @shopChargesApplyToParts BIT;
        DECLARE @shopChargesApplyToLabor BIT;
        DECLARE @minShopCharges FLOAT;
        DECLARE @maxShopCharges FLOAT;
        SET @includeShopCharges = ( SELECT TOP 1
                                            CAST(Value AS BIT)
                                    FROM    CompanySettings
                                    WHERE   CompanyId = @TenantId
                                            AND Name = 'PrintShopCharges'
                                  );
        IF ( @includeShopCharges = 1 )
            BEGIN
                SET @shopChargeAmount = ( SELECT TOP 1
                                                    CAST(Value AS FLOAT)
                                          FROM      CompanySettings
                                          WHERE     CompanyId = @TenantId
                                                    AND Name = 'ShopChargeAmount'
                                        );
                SET @shopChargesApplyToParts = ( SELECT TOP 1
                                                        CAST(Value AS BIT)
                                                 FROM   CompanySettings
                                                 WHERE  CompanyId = @TenantId
                                                        AND Name = 'ShopChargesApplyToParts'
                                               );
                SET @shopChargesApplyToLabor = ( SELECT TOP 1
                                                        CAST(Value AS BIT)
                                                 FROM   CompanySettings
                                                 WHERE  CompanyId = @TenantId
                                                        AND Name = 'ShopChargesApplyToLabor'
                                               );
                SET @minShopCharges = ( SELECT TOP 1
                                                CAST(Value AS FLOAT)
                                        FROM    CompanySettings
                                        WHERE   CompanyId = @TenantId
                                                AND Name = 'ShopChargeMinimum'
                                      );
                SET @maxShopCharges = ( SELECT TOP 1
                                                CAST(Value AS FLOAT)
                                        FROM    CompanySettings
                                        WHERE   CompanyId = @TenantId
                                                AND Name = 'ShopChargeMaximum'
                                      );
            END;

        DECLARE @includeTaxCharges BIT;
        DECLARE @taxChargeAmount FLOAT;
        DECLARE @taxChargesApplyToParts BIT;
        DECLARE @taxChargesApplyToLabor BIT;
        SET @includeTaxCharges = ( SELECT TOP 1
                                            CAST(Value AS BIT)
                                   FROM     CompanySettings
                                   WHERE    CompanyId = @TenantId
                                            AND Name = 'DisplayTexesInMenus'
                                 );
        IF ( @includeTaxCharges = 1 )
            BEGIN
                SET @taxChargeAmount = ( SELECT TOP 1
                                                CAST(Value AS FLOAT)
                                         FROM   CompanySettings
                                         WHERE  CompanyId = @TenantId
                                                AND Name = 'TaxRateAmount'
                                       );
                SET @taxChargesApplyToParts = ( SELECT TOP 1
                                                        CAST(Value AS BIT)
                                                FROM    CompanySettings
                                                WHERE   CompanyId = @TenantId
                                                        AND Name = 'TaxRateApplyToParts'
                                              );
                SET @taxChargesApplyToLabor = ( SELECT TOP 1
                                                        CAST(Value AS BIT)
                                                FROM    CompanySettings
                                                WHERE   CompanyId = @TenantId
                                                        AND Name = 'TaxRateApplyToLabor'
                                              );
            END;

	-- Get OemBasicServices
	--DROP/RECREATE Temp table for ServicesWithParts
        IF OBJECT_ID('tempdb..#Services') IS NOT NULL
            BEGIN
                DROP TABLE #Services;
            END;
        SELECT  ServiceId,
                OpCode,
                FullDescription,
                CarId,
                Mileage,
                IsFluid,
                IsEngineOil,
                IsLOF,
				CanBeStriked,
                IsStrikeOut,
                IsDeclined,
                IsPreviouslyServed,
                BgProtectionPlanId,
                VideoCode,
                MenuLevel,
                ServiceType,
                ServiceMenuType,
                LaborHour,
                LabourRate,
                PartsPrice,
                ( LabourRate * LaborHour ) AS LaborPrice,
                ( ( LabourRate * LaborHour ) + PartsPrice ) AS Price
        INTO    #Services
        FROM    ( SELECT    ServiceId,
                            OpCode,
                            FullDescription,
                            CarId,
                            IsFluid,
                            IsEngineOil,
                            IsLOF,
							CanBeStriked,
                            IsStrikeOut,
                            IsDeclined,
                            IsPreviouslyServed,
                            BgProtectionPlanId,
                            VideoCode,
                            MenuLevel,
                            ServiceType,
                            ServiceMenuType,
                            LaborHour,
                            LabourRate,
                            PartsPrice,
                            Mileage AS Mileage
                  FROM      ( SELECT    CONVERT(BIGINT, obs.OemBasicServiceId) AS ServiceId,
                                        '' AS OpCode,
                                        obs.OpAction + ' ' + oc.Description AS FullDescription,
                                        obs.CarId,
                                        omm.Mileage,
                                        oc.IsFluid,
                                        oc.IsEngineOil,
                                        oc.IsLOF,
										CONVERT(BIT, 0) AS CanBeStriked,
                                        CONVERT(BIT, 0) AS IsStrikeOut,
                                        CONVERT(BIT, 0) AS IsDeclined,
                                        CONVERT(BIT, 0) AS IsPreviouslyServed,
				--IsDeclined and IsPreviouslyServed fields are handled in GetAllServices procedure to be set to 1 we need to have Customer!
				--Changed by Dimitar Stefanov
				--convert(bit,case when (select count(*) from dealer.AppointmentServices where OrigServiceId = obs.OemBasicServiceId and ServiceTypeId = 1 and IsStriked = 1) > 0 then 1 else 0 end) as IsDeclined,
				--convert(bit,case when (select count(*) from dealer.AppointmentServices where OrigServiceId = obs.OemBasicServiceId and ServiceTypeId = 1 and IsStriked = 0) > 0 then 1 else 0 end) as IsPreviouslyServed,
                                        NULL AS BgProtectionPlanId,
                                        vid.VideoUrl AS VideoCode,
                                        CONVERT(INT, 1) AS MenuLevel, -- in which level this item should be shown
                                        CONVERT(INT, 1) AS ServiceType, -- table origin (this can be OemBasic 1, AdditionaService 2 ...)
                                        CONVERT(INT, 1) AS ServiceMenuType, -- this can be Maintenance or Additional service
                                        CONVERT(FLOAT, obs.LaborHour
                                        + COALESCE(( SELECT SUM(op.LaborHour)
                                                     FROM   amam.OemServiceParts osp
                                                            INNER JOIN amam.OemParts
                                                            AS op ON osp.OemPartId = op.OemPartId
                                                     WHERE  OemBasicServiceId = obs.OemBasicServiceId
                                                   ), 0)) AS LaborHour,
                                        @LaborRate AS LabourRate,
                                        CONVERT(FLOAT, COALESCE(( SELECT
                                                              SUM(op.UnitPrice
                                                              * osp.Quantity)
                                                              FROM
                                                              amam.OemServiceParts osp
                                                              INNER JOIN amam.OemParts
                                                              AS op ON osp.OemPartId = op.OemPartId
                                                              WHERE
                                                              OemBasicServiceId = obs.OemBasicServiceId
                                                              ), 0)) AS PartsPrice,
				--convert(float,coalesce((select sum(op.unitprice*osp.quantity) from amam.oemserviceparts osp inner join amam.oemparts as op on osp.OemPartId = op.OemPartId where OemBasicServiceId = obs.OemBasicServiceId and ((op.OemPartTypeId = case @IsLOF when 1 then 0 else op.OemPartTypeId end) or (op.OemPartTypeId = case @IsLOF when 1 then 2 else op.OemPartTypeId end))),0)) as PartsPrice,
                                        CONVERT(FLOAT, 0) AS LaborPrice
                              FROM      amam.OemBasicServices obs
                                        INNER JOIN amam.OemComponents AS oc ON oc.OemComponentId = obs.OemComponentId
                                        INNER JOIN amam.OemMenuLists AS oml ON oml.CarId = obs.CarId
                                        INNER JOIN amam.OemMenus AS om ON om.OemMenuListId = oml.OemMenuListId
                                        INNER JOIN amam.OemMenuMileages AS omm ON omm.OemMenuId = om.OemMenuId
                                        INNER JOIN amam.OemServices AS os ON os.OemMenuId = om.OemMenuId
                                                              AND os.OemBasicServiceId = obs.OemBasicServiceId
                                        INNER JOIN dbo.Vehicles AS v ON v.CarId = obs.CarId
                                        LEFT JOIN dealer.DealerVehicles AS dv ON dv.VehicleId = v.VehicleId
                                        LEFT OUTER JOIN bg.BgVideoClips AS bgvid ON bgvid.ServiceId = obs.OemBasicServiceId
                                                              AND bgvid.ServiceTypeId = 1 -- OemBasicService
                                        LEFT OUTER JOIN bg.BgVideoClipEditions
                                        AS vid ON vid.BgVideoClipId = bgvid.BgVideoClipId
                                                  AND vid.LanguageId = @LanguageId -- from company settings
                              WHERE     obs.IsIncomplete = 0
                                        AND ( @CarId IS NULL OR obs.CarId = @CarId)
                                        AND ( @DealerVehicleId IS NULL OR dv.DealerVehicleId = @DealerVehicleId)
                                        AND ( @IsLOF = 0 OR @IsLOF IS NULL OR oc.IsLOF = @IsLOF)
                                        AND ( @Mileage IS NULL OR omm.Mileage = @Mileage)
                                        AND omm.MeasurementUnitId = @MeasurementUnitId
                                        AND ( @VehicleTransmission IS NULL OR @VehicleTransmission = 5 --(N/A Unknown value)
                                              OR oc.VehicleTransmissionShort IN (
                                              SELECT    VehicleTransmission
                                              FROM      amam.VehicleTransmissions
                                              WHERE     VehicleTransmissionId IN (
                                                        @VehicleTransmission,
                                                        5 ) )
                                            )
                                        AND ( @VehicleDriveLine IS NULL
                                              OR @VehicleDriveLine = 6 --(N/A Unknown value)
                                              OR oc.VehicleDrivelineShort IN (
                                              SELECT    VehicleDriveline
                                              FROM      amam.VehicleDrivelines
                                              WHERE     VehicleDrivelineId IN (
                                                        @VehicleDriveLine, 6 ) )
                                            )
                            ) AS q
                  GROUP BY  ServiceId,
                            OpCode,
                            FullDescription,
                            CarId,
                            IsFluid,
                            IsEngineOil,
                            IsLOF,
							CanBeStriked,
                            IsStrikeOut,
                            IsDeclined,
                            IsPreviouslyServed,
                            BgProtectionPlanId,
                            VideoCode,
                            MenuLevel,
                            ServiceType,
                            ServiceMenuType,
                            LaborHour,
                            LabourRate,
                            PartsPrice,
                            Mileage
                ) AS q2;
		
		--UNION

		--select
		--	q.ServiceId
		--	,OpCode
		--	,FullDescription
		--	,0 as CarId
		--	,0 as Mileage
		--	,IsFluid
		--	,IsEngineOil
		--	,IsLOF
		--	,convert(bit,0) as IsStrikeOut
		--	,convert(bit,0) as IsDeclined
		--	,convert(bit,0) as IsPreviouslyServed
		--	,BgProtectionPlanId
		--	,vid.VideoUrl as VideoCode
		--	,MenuLevel
		--	,2 as ServiceType
		--	,1 as ServiceMenuType
		--	,LaborHour
		--	,@LaborRate as LabourRate
		--	,PartsPrice
		--	,(@LaborRate * LaborHour) as LaborPrice
		--	,((@LaborRate * LaborHour) + PartsPrice) as Price
		--from (select
		--		services.AdditionalServiceId as ServiceId,
		--		services.OpCode,
		--		services.OpDescription as FullDescription,
		--		parts.IsFluid as IsFluid,
		--		parts.IsEngineOil as IsEngineOil,
		--		services.IsLofService as IsLOF,
		--		services.BgProtectionPlanId as BgProtectionPlanId,
		--		services.MenuLevel as MenuLevel,
		--		services.LaborHour as LaborHour,
		--		sum(parts.Quantity * parts.UnitPrice) as PartsPrice			
		--	from dealer.additionalservices as services
		--		inner join dealer.AdditionalServiceParts as parts on parts.AdditionalServiceId = services.AdditionalServiceId
			
		--	where 
		--		services.DealerId = @TenantId
		--		and (@IsLOF = 0 or @IsLOF is null or services.IsLofService = @IsLOF)
		--	group by
		--		services.AdditionalServiceId,services.OpCode,services.OpDescription,parts.IsFluid,
		--		parts.IsEngineOil,services.IsLofService,services.MenuLevel,services.LaborHour,services.BgProtectionPlanId) as q

		--left outer join bg.BgVideoClips as bgvid on bgvid.ServiceId = q.ServiceId 
		--	and bgvid.ServiceTypeId = 2 -- additionalservices
		--left outer join bg.bgvideoclipeditions as vid on vid.BgVideoClipId = bgvid.BgVideoClipId 
		--	and vid.LanguageId = @LanguageId -- from company settings

        SELECT  ServiceId,
                ISNULL(overrides.OpCodeOverride, s.OpCode) AS OpCode,
                FullDescription,
                CarId,
                Mileage,
                IsFluid,
                IsEngineOil,
                IsLOF,
				CanBeStriked,
                IsStrikeOut,
                IsDeclined,
                IsPreviouslyServed,
                BgProtectionPlanId,
                VideoCode,
                MenuLevel,
                ServiceType,
                ServiceMenuType,
                ISNULL(overrides.LaborHourOverride, LaborHour) AS LaborHour,
                LabourRate,
                ISNULL(overrides.PartsPriceOverride, PartsPrice) AS PartsPrice,
                ISNULL(overrides.LaborPriceOverride, LaborPrice) AS LaborPrice,
                ISNULL(overrides.PartsPriceOverride, PartsPrice)
                + ISNULL(overrides.LaborPriceOverride, LaborPrice) AS Price,
                CASE @shopChargesApplyToParts
                  WHEN 1
                  THEN CASE WHEN ISNULL(overrides.PartsPriceOverride,
                                        PartsPrice) * ( @shopChargeAmount
                                                        / 100 ) < @minShopCharges
                            THEN @minShopCharges
                            WHEN ISNULL(overrides.PartsPriceOverride,
                                        PartsPrice) * ( @shopChargeAmount
                                                        / 100 ) > @maxShopCharges
                            THEN @maxShopCharges
                            ELSE ISNULL(overrides.PartsPriceOverride,
                                        PartsPrice) * ( @shopChargeAmount
                                                        / 100 )
                       END
                  ELSE 0
                END AS ShopChargesPartsPrice,
                CASE @shopChargesApplyToLabor
                  WHEN 1
                  THEN CASE WHEN ISNULL(overrides.LaborPriceOverride,
                                        LaborPrice) * ( @shopChargeAmount
                                                        / 100 ) < @minShopCharges
                            THEN @minShopCharges
                            WHEN ISNULL(overrides.LaborPriceOverride,
                                        LaborPrice) * ( @shopChargeAmount
                                                        / 100 ) > @maxShopCharges
                            THEN @maxShopCharges
                            ELSE ISNULL(overrides.LaborPriceOverride,
                                        LaborPrice) * ( @shopChargeAmount
                                                        / 100 )
                       END
                  ELSE 0
                END AS ShopChargesLaborPrice
        INTO    #TempServices
        FROM    #Services s
                LEFT JOIN GetOverridenBasicServicesByCarId(@CarId, NULL,
                                                             NULL, NULL,
                                                             @TenantId,
                                                             @LaborRate, ',') overrides ON overrides.OemBasicServiceId = s.ServiceId
                                                              AND s.ServiceType = 1
        ORDER BY s.ServiceType,
                ServiceId;

        SELECT  ServiceId,
                OpCode,
                FullDescription,
                CarId,
                Mileage,
                IsFluid,
                IsEngineOil,
                IsLOF,
				CanBeStriked,
                IsStrikeOut,
                IsDeclined,
                IsPreviouslyServed,
                BgProtectionPlanId,
                VideoCode,
                MenuLevel,
                ServiceType,
                ServiceMenuType,
                LaborHour,
                LabourRate,
                PartsPrice,
                LaborPrice,
                Price,
                ShopChargesPartsPrice,
                ShopChargesLaborPrice,
                CASE @taxChargesApplyToParts
                  WHEN 1
                  THEN ( PartsPrice + ShopChargesPartsPrice )
                       * ( @taxChargeAmount / 100 )
                  ELSE 0
                END AS TaxIncludedPartsPrice,
                CASE @taxChargesApplyToLabor
                  WHEN 1
                  THEN ( LaborPrice + ShopChargesLaborPrice )
                       * ( @taxChargeAmount / 100 )
                  ELSE 0
                END AS TaxIncludedLaborPrice
        FROM    #TempServices
        ORDER BY IsLOF DESC,
                FullDescription ASC;
		
	

    END;

/*
	exec GetMaintenanceServices @DealerVehicleId=2, @Mileage=15000, @TenantId = 3
		
	exec dbo.GetMaintenanceServices @TenantId=3,@CarId=2,@DealerVehicleId=default,@Mileage=100000,@IsLof=0,@VehicleTransmission=N'6-SPEED MANUAL',@VehicleDriveLine=N'RWD'

	exec dbo.GetMaintenanceServices @TenantId=3,@CarId=2,@DealerVehicleId=default,@Mileage=100000,@IsLof=0,@VehicleTransmission=null,@VehicleDriveLine=null

	exec dbo.GetMaintenanceServices @TenantId=3,@CarId=2,@DealerVehicleId=default,@Mileage=99000,@IsLof=0,@VehicleTransmission=null,@VehicleDriveLine=null

	
	exec dbo.GetMaintenanceServices @TenantId=3,@CarId=2,@DealerVehicleId=default,@Mileage=105000,@IsLof=0,@VehicleTransmission=null,@VehicleDriveLine=null
*/


GO
