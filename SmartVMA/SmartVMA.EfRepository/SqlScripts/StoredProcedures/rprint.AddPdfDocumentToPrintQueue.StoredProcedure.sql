USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [rprint].[AddPdfDocumentToPrintQueue]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Dimitar Stefanov
-- Create date: 22.07.2016
-- Description:	AddPdfDocumentToPrintQueue
-- =============================================
ALTER PROCEDURE [rprint].[AddPdfDocumentToPrintQueue] --43, 3, 'http://localhost:54503/Dealer/PdfDocuments/ViewPdfFile?filePath=1089\19&fileName=TC_VINJN1CV6AR0BM351220_Invoice5555.pdf&pdfAction=1', 79832, 'HIST'
	@UserId int,
	@DealerID int,
	@FileName nvarchar(255),
	@AppointmentPresentationId bigint,
	@PrnJobTypeCode nvarchar(45)
AS
BEGIN
	DECLARE @fileCount int
	SELECT @fileCount = count(*) FROM rprint.PrnSpool
	WHERE docname = @FileName
	IF @fileCount = 0
	BEGIN
		INSERT INTO rprint.PrnSpool (account, idhist, doctype, docname, prnname, prndriver, prnport, prncopy, prntray, datecreated, [status], comment, assno, [session])
		SELECT TOP 1 ISNULL(pv.AccountId, '') as account, @AppointmentPresentationId as idhist, pv.PrnJobTypeCode as doctype, @FileName as docname, 
		pv.PrinterName as prnname, pv.PrinterDriver as prndriver, pv.PrinterPort as prnport, pv.NumberOfCopy as prncopy, ISNULL(pv.PrinterTray, '') as prntray,
		GETDATE() as datecreated, 'Pending' as [status], '' as comment, u.DmsUserNumber as assno, '' as [session]
		FROM  rprint.vwPrnConfigurations pv
		LEFT JOIN dealer.DealerDmsUsers u ON pv.UserId = u.UserId
		WHERE 
		pv.DealerId = @DealerID 
		AND (pv.UserId IS NULL OR pv.UserId = @UserId) 
		AND PrnJobTypeCode = @PrnJobTypeCode
		ORDER BY pv.UserId ASC
	END
	ELSE
	BEGIN
		INSERT INTO rprint.PrnSpool (account, idhist, doctype, docname, prnname, prndriver, prnport, prncopy, prntray, datecreated, [status], comment, assno, [session])
		SELECT TOP 1 ISNULL(pv.AccountId, '') as account, @AppointmentPresentationId as idhist, pv.PrnJobTypeCode as doctype, @FileName as docname, 
		pv.PrinterName as prnname, pv.PrinterDriver as prndriver, pv.PrinterPort as prnport, pv.NumberOfODC as prncopy, ISNULL(pv.PrinterTray, '') as prntray,
		GETDATE() as datecreated, 'Pending' as [status], '' as comment, u.DmsUserNumber as assno, '' as [session]
		FROM  rprint.vwPrnConfigurations pv
		LEFT JOIN dealer.DealerDmsUsers u ON pv.UserId = u.UserId
		WHERE 
		pv.DealerId = @DealerID 
		AND (pv.UserId IS NULL OR pv.UserId = @UserId) 
		AND PrnJobTypeCode = @PrnJobTypeCode
		ORDER BY pv.UserId ASC
	END
END