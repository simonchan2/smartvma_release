USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[sp_Insert]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		
-- Create date:
-- Description:	exec sp_Insert
-- =============================================
CREATE PROCEDURE [dbo].[sp_Insert] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @OemMl int
	declare @OemCompId int
	declare @miles int
	declare @km int
	declare @NoOver int
	declare @i int
create table #temp
([OemMenuId] int,
[MeasurementUnitId] int,
[Mileage] int,
[NoOverwrite] bit)

DECLARE db_cursor CURSOR FOR  
select ml.OemMenuListId,am.OemComponentId, Miles, Kilometers, 0 as [NoOverwrite]
FROM [AllData].[dbo].[AllDataMenu] am inner join [SmartVmaPlus].[amam].Cars c on am.CarExtId=c.CarExtId
inner join [SmartVmaPlus].[amam].[OemMenuLists] ml on ml.CarId=c.CarId and ml.OemComponentId=am.OemComponentId
where IsSync = 0 and IsValid = 1

OPEN db_cursor   
FETCH NEXT FROM db_cursor INTO @OemMl, @OemCompId, @miles, @km, @NoOver  

WHILE @@FETCH_STATUS = 0   
BEGIN   
       insert into [amam].[OemMenus]([OemMenuListId],[OemComponentId],[NoOverwrite],[TimeCreated],[TimeUpdated])
       values (@OemMl, @OemCompId, @NoOver, getutcdate(), GETUTCDATE())
	   select @i=@@IDENTITY
	   insert into #temp
	   values (@i, 0, @miles, @NoOver)

	   insert into #temp
	   values (@i, 1, @km, @NoOver)

       FETCH NEXT FROM db_cursor INTO @OemMl, @OemCompId, @miles, @km, @NoOver   
END   

CLOSE db_cursor   
DEALLOCATE db_cursor 

 insert into [amam].[OemMenuMileages]([OemMenuId],[MeasurementUnitId],[Mileage],[NoOverwrite])
 SELECT [OemMenuId],[MeasurementUnitId],[Mileage],[NoOverwrite]
 FROM #temp

END

GO
