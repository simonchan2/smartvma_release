USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[OverrideRuleCheckConflicts]    Script Date: 1/3/2017 12:24:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[OverrideRuleCheckConflicts]
		@TenantId INT,
		@RuleId BIGINT,
		@VehicleYears NVARCHAR(MAX),
		@VehicleMakes NVARCHAR(MAX),
		@VehicleModels NVARCHAR(MAX),
		@VehicleEngines NVARCHAR(MAX),
		@VinMasterTransmissions NVARCHAR(MAX),
		@VinMasterDriveLines NVARCHAR(MAX),
		@VehicleSteerings NVARCHAR(MAX),
		@BasicServices NVARCHAR(MAX),
		@AreServicesChecked BIT,
		@LaborHourPercentage INT,
		@LaborHourAmount MONEY,
		@LaborPricePercentage INT,
		@LaborPriceAmount MONEY,
		@PartPricePercentage INT,
		@PartPriceAmount MONEY,
		@OpCodeOverride NVARCHAR(50),
		@PartNoOriginal NVARCHAR(20),
		@PartNoOverride NVARCHAR(20),
		@OpCode NVARCHAR(50),
		@PartNo NVARCHAR(20),
		@Keyword NVARCHAR(255),
		@Delimiter NVARCHAR(8)
AS
BEGIN
	DECLARE @LaborRate FLOAT;
	SET @LaborRate = (SELECT TOP 1 CAST(value AS FLOAT) FROM CompanySettings WHERE CompanyId = @TenantId and Name = 'DefaultLaborRate');

	DECLARE @LHOverride BIT = (SELECT IIF(@LaborHourPercentage IS NOT NULL OR @LaborHourAmount IS NOT NULL, 1, 0))
	DECLARE @LPOverride BIT = (SELECT IIF(@LaborPricePercentage IS NOT NULL OR @LaborPriceAmount IS NOT NULL, 1, 0))
	DECLARE @PPOverride BIT = (SELECT IIF(@PartPricePercentage IS NOT NULL OR @PartPriceAmount IS NOT NULL, 1, 0))
	DECLARE @OCOverride BIT = (SELECT IIF(@OpCodeOverride IS NOT NULL, 1, 0))
	DECLARE @PNOverride BIT = (SELECT IIF(@PartNoOverride IS NOT NULL, 1, 0))


	DECLARE @LaborHourRuleIdOverride BIGINT = (SELECT IIF(@LaborHourPercentage IS NULL AND @LaborHourAmount IS NULL, NULL, @RuleId))
	DECLARE @LaborPriceRuleIdOverride BIGINT = (SELECT IIF(@LaborPricePercentage IS NULL AND @LaborPriceAmount IS NULL, NULL, @RuleId))
	DECLARE @PartPriceRuleIdOverride BIGINT = (SELECT IIF(@PartPricePercentage IS NULL AND @PartPriceAmount IS NULL, NULL, @RuleId))
	DECLARE @OpCodeRuleIdOverride BIGINT = (SELECT IIF(@OpCodeOverride IS NULL, NULL, @RuleId))
	DECLARE @PartNoRuleIdOverride BIGINT = (SELECT IIF(@PartNoOverride IS NULL, NULL, @RuleId))
	DECLARE	@CurrencySign NVARCHAR(1)= N'$'
	DECLARE	@PercentSign NVARCHAR(1)= N'%'

	IF OBJECT_ID('tempdb..#ExistingRules') is not null 
	BEGIN
		DROP TABLE #ExistingRules;
	END;
	SELECT 
	orules.Name AS RuleName
	,orules.Id AS RuleId
	,ovalues.LaborHourPercentage
	,ovalues.LaborHourAmount
	,ovalues.LaborPricePercentage
	,ovalues.LaborPriceAmount
	,ovalues.PartPricePercentage
	,ovalues.PartPriceAmount
	,ovalues.OpCodeOverride
	,ovalues.PartNoOverride
	INTO #ExistingRules
	FROM dealer.OverrideRules orules
	CROSS APPLY(
	SELECT
	IIF(@LHOverride = 1, orules.LaborHourPercentage, NULL) AS LaborHourPercentage
	,IIF(@LHOverride = 1, orules.LaborHourAmount, NULL) AS LaborHourAmount
	,IIF(@LPOverride = 1, orules.LaborPricePercentage, NULL) AS LaborPricePercentage
	,IIF(@LPOverride = 1, orules.LaborPriceAmount, NULL) AS LaborPriceAmount
	,IIF(@PPOverride = 1, orules.PartPricePercentage, NULL) AS PartPricePercentage
	,IIF(@PPOverride = 1, orules.PartPriceAmount, NULL) AS PartPriceAmount
	,IIF(@OCOverride = 1, orules.OpCodeOverride, NULL) AS OpCodeOverride
	,IIF(@PNOverride = 1, orules.PartNoOverride, NULL) AS PartNoOverride
	) ovalues
	WHERE 
	(ovalues.LaborHourPercentage IS NOT NULL OR
	ovalues.LaborHourAmount IS NOT NULL OR
	ovalues.LaborPricePercentage IS NOT NULL OR
	ovalues.LaborPriceAmount IS NOT NULL OR
	ovalues.PartPricePercentage IS NOT NULL OR
	ovalues.PartPriceAmount IS NOT NULL OR
	ovalues.OpCodeOverride IS NOT NULL OR
	ovalues.PartNoOverride IS NOT NULL) AND
	(@RuleId IS NULL OR @RuleId <> orules.Id)

	--SELECT *
	--FROM #ExistingRules
	--Get Matching Services filtered by the provided criteria and keywords
	IF OBJECT_ID('tempdb..#ServicesWithParts') is not null 
	BEGIN
		DROP TABLE #ServicesWithParts;
	END;
	SELECT obs.OemBasicServiceId
	INTO #ServicesWithParts
	FROM amam.OemServiceParts osp
	INNER JOIN amam.OemPartNumbers opn ON opn.OemPartId = osp.OemPartId 
	INNER JOIN amam.OemBasicServices obs ON osp.OemBasicServiceId = obs.OemBasicServiceId AND obs.IsDeleted = 0 AND obs.IsIncomplete = 0 
	WHERE @PartNo IS NULL OR opn.OemPartNo = @PartNo

	IF OBJECT_ID('tempdb..#Conflicts') is not null 
	BEGIN
		DROP TABLE #Conflicts;
	END;
	WITH FilteredServices AS 
	(SELECT 
	exRules.RuleId
	,OemBasicServiceId
	,FullDescription
	FROM #ExistingRules exrules
	INNER JOIN dbo.GetFilteredBasicServices_TEST
	(@TenantId, NULL, @VehicleYears, @VehicleMakes, @VehicleModels, @VehicleEngines, 
	@VinMasterTransmissions, @VinMasterDriveLines, @VehicleSteerings, @Delimiter) AS fbs 
	ON exRules.RuleId IN (fbs.LaborHourRuleId, fbs.LaborPriceRuleId, fbs.PartPriceRuleId, fbs.OpCodeRuleId, fbs.PartNoRuleId)
	WHERE
		(@Keyword IS NULL OR FullDescription LIKE ('%' + @Keyword + '%'))
		AND (@OpCode IS NULL OR @OpCode = fbs.OpCodeOverride)
		AND (@PartNo IS NULL OR (@PartNo = fbs.PartNoOverride OR fbs.OemBasicServiceId IN (SELECT * FROM #ServicesWithParts)))
	)

	SELECT *
	INTO #Conflicts
	FROM(
	SELECT fs.*
	FROM FilteredServices fs
	INNER JOIN dbo.SplitDelimiterString(@BasicServices, @Delimiter) bs ON CAST(fs.OemBasicServiceId AS NVARCHAR(20)) = bs.Item
	WHERE @AreServicesChecked = 1

	UNION ALL

	SELECT fs.*
	FROM FilteredServices fs
	LEFT JOIN dbo.SplitDelimiterString(@BasicServices, @Delimiter) bs ON CAST(fs.OemBasicServiceId AS NVARCHAR(20)) = bs.Item
	WHERE @AreServicesChecked = 0 AND bs.Item IS NULL) x


	;WITH ConflictingServicesGrouped
	AS(
	SELECT
	RuleId,
	COUNT(c.OemBasicServiceId) AS ConflictingServices
	FROM #Conflicts c
	GROUP BY RuleId
	)
	SELECT 
	erules.RuleId
	,erules.RuleName
	,erules.LaborHourPercentage
	,erules.LaborHourAmount
	,erules.LaborPricePercentage
	,erules.LaborPriceAmount
	,erules.PartPricePercentage
	,erules.PartPriceAmount
	,erules.OpCodeOverride
	,erules.PartNoOverride
	,c.ConflictingServices
	,@LaborHourPercentage AS NewLaborHourPercentage
	,@LaborHourAmount AS NewLaborHourAmount
	,@LaborPricePercentage AS NewLaborPricePercentage
	,@LaborPriceAmount AS NewLaborPriceAmount
	,@PartPricePercentage AS NewPartPricePercentage
	,@PartPriceAmount AS NewPartPriceAmount
	,@OpCodeOverride AS NewOpCodeOverride
	,@PartNoOverride AS NewPartNoOverride
	FROM #ExistingRules erules
	INNER JOIN ConflictingServicesGrouped c ON erules.RuleId = c.RuleId
	ORDER BY erules.RuleId

	;WITH partioned AS
	(SELECT RuleId, 'ID: ' + CAST(OemBasicServiceId as NVARCHAR(50)) + ' - ' + FullDescription as FullDescription, ROW_NUMBER() OVER (PARTITION BY RuleId ORDER BY OemBasicServiceId ASC) AS Row
	 FROM #Conflicts),
	 limited AS (
	SELECT 
	partioned.RuleId
	,partioned.FullDescription 
	FROM partioned
	WHERE Row <= 5)
	SELECT RuleId, FullDescription
	FROM limited

END;



