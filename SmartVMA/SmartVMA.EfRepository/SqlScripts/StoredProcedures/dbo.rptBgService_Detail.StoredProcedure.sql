USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[rptBgService_Detail]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec [dbo].[rptBgService_Detail] 'nikco','QTY', 1041,'2015-01-01', '2016-01-01',1,0,0,0,0
CREATE PROCEDURE [dbo].[rptBgService_Detail]
	@AdvisorCode nvarchar(45),
	@Content			varchar(255),
	@DealerCompanyId	int,
	@StartDate			datetime,
	@EndDate			datetime,
	@By					int,
	@CPOnly				bit,
	@ExclLabor0			bit,
	@UserOnly			bit,
	@ALaCateOnly		bit
AS
BEGIN
    SET NOCOUNT ON;

select distinct 
i.InvoiceDate,
i.InvoiceNumber ro,
dc.FullName Customer,
i.mileage odom,
vm.VehicleMake + '-' +  vm.VehicleModel + '-' + SUBSTRING(CAST(vm.VehicleYear AS varchar(4)),3,2) [Make]
--,d.opcode
,bgpc.BgType [VehicleSystem]
,CASE @Content		 
		WHEN 'QTY' THEN (PartQty)
		WHEN 'Labor$' THEN ([LaborSale])
		WHEN 'Part$' THEN (d.[PartSale])
		WHEN 'LaborParts$' THEN ([LaborSale] + d.[PartSale]) 
		WHEN 'Hours' THEN (d.[LaborHour])
		WHEN 'ELR' THEN ([LaborSale]/d.[LaborHour])
		WHEN 'Commission' THEN (AdvisorIncentive)
		WHEN '%Pen' THEN case when PresentationStatus='MC' then 1 else 0 end
		WHEN 'GPLabor' THEN (d.[LaborSale] - d.[LaborCost])
		WHEN 'GPParts' THEN (d.[PartSale] - d.[PartCost])
		WHEN 'GPLaborParts' THEN (d.[LaborSale] - d.[LaborCost] + d.[PartSale] - d.[PartCost])						
	END as content
into #temp
from [dealer].[Invoices] i (nolock)
join [dealer].[DealerDmsUsers] u (nolock)on u.DmsUserNumber = i.AdvisorCode
join [dealer].[InvoiceDetails] d (nolock)on d.InvoiceId = i.InvoiceId
join [dealer].[InvoiceDetailParts] p (nolock)on p.InvoiceDetailId = d.InvoiceDetailId
join [dealer].[DealerCustomers] dc (nolock)on dc.[DealerCustomerId] = i.DealerCustomerId
join [dealer].[DealerVehicles] dv (nolock)on dv.DealerVehicleId = i.DealerVehicleId
join [dbo].[Vehicles] v (nolock)on v.[VehicleId] = dv.VehicleId
join [dealer].[AppointmentServices] aps (nolock) on aps.DealerId = i.[DealerId] and aps.OpCode = d.[OpCode]
join [dealer].[AppointmentPresentations] aptpres (nolock) on aptpres.AppointmentPresentationId = aps.AppointmentPresentationId
join [dealer].[AdditionalServices] adds (nolock)on adds.[DealerId] = i.[DealerId] and adds.[OpCode] = d.[OpCode]
join [dealer].[AdditionalServiceParts] addsp (nolock) on addsp.AdditionalServiceId = addsp.AdditionalServiceId
join [bg].[BgProductsCategories] prodsc (nolock) on prodsc.[BgProductId] = addsp.[BgProductId]
join [bg].[BgProdCategories] bgpc (nolock) on bgpc.[BgProdCategoryId] = prodsc.[BgProdCategoryId] 
--join [bg].[DealerBgServices] dbs (nolock)on dbs.[DealerId] = i.[DealerId] and dbs.[OpCode] = d.[OpCode]
--join [bg].[BgProdSubcategories] bgpsc (nolock) on bgpsc.BgProdSubcategoryId= aps.BgProdSubcategoryId
--join [bg].[BgProdCategories] bgpc (nolock) on bgpc.BgProdCategoryId = bgpsc.BgProdCategoryId
join vinquery.VinMaster vm (nolock)on vm.VinMasterId = v.VinMasterId
where i.AdvisorCode=@AdvisorCode 
	and (@DealerCompanyId=0 or (@DealerCompanyId<>0 and i.DealerId = @DealerCompanyId))
	and i.InvoiceDate between @StartDate and @EndDate 
	and [DmsUserTypeId] = @By  -- advisor or tech
	and (@ExclLabor0 = 0 or (@ExclLabor0 = 1 and d.LaborHour = 0)) 
	and (@UserOnly = 0 or (@UserOnly = 1 and u.DmsUserName is not null)) 
	and (@CPOnly = 0 or (@CPOnly=1 and d.[LaborType] = 'CP'))
	and (@ALaCateOnly = 0 or (@ALaCateOnly=1 and aps.IsAdditionService=1))

select * 
into #temp2
from #temp
pivot (sum(content) for [VehicleSystem] in (
	[AC],
	[Batt],
	[Brake],
	[Cool],
	[Diff],
	[DSL Emiss],
	[DSL Induct],
	[DSL Inject],
	[Ethanol],
	[Fuel],
	[GDI],
	[Oil],
	[PS],
	[Trans])) as AvgIncomePerDay

select 
	InvoiceDate,
	ro,
	Customer,
	odom,
	Make,
	--opcode,
	isnull(AC,0) AC,	
	isnull(Batt,0) Batt,	
	isnull(Brake,0) Brake,	
	isnull(Cool,0) Cool,	
	isnull(Diff,0) Diff,
	isnull(([DSL Emiss]),0) [DSL Emiss],
	isnull(([DSL Induct]),0) [DSL Induct],
	isnull(([DSL Inject]),0) [DSL Inject],
	isnull(([Ethanol]),0) [Ethanol],
	isnull(Fuel,0) Fuel,	
	isnull(Oil,0) Oil,	
	isnull(([GDI]),0) [GDI],
	isnull(PS,0) PS,	
	isnull(Trans,0)	Trans
from #temp2

END



GO
