USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[OverrideRuleAddSet]    Script Date: 1/3/2017 12:26:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[OverrideRuleAddSet]
	@Id BIGINT,
	@Name NVARCHAR(255),
	@TenantId INT,
	@VehicleYears NVARCHAR(MAX),
	@VehicleMakes NVARCHAR(MAX),
	@VehicleModels NVARCHAR(MAX),
	@VehicleEngines NVARCHAR(MAX),
	@VinMasterTransmissions NVARCHAR(MAX),
	@VinMasterDriveLines NVARCHAR(MAX),
	@VehicleSteerings NVARCHAR(MAX),
	@BasicServices NVARCHAR(MAX),
	@AreServicesChecked BIT,
	@LaborHourPercentage INT,
	@LaborHourAmount MONEY,
	@LaborPricePercentage INT,
	@LaborPriceAmount MONEY,
	@PartPricePercentage INT,
	@PartPriceAmount MONEY,
	@OpCodeOverride NVARCHAR(50),
	@PartNoOriginal NVARCHAR(20),
	@PartNoOverride NVARCHAR(20),
	@OpCode NVARCHAR(50),
	@PartNo NVARCHAR(20),
	@Keyword NVARCHAR(255),
	@Delimiter NVARCHAR(8)
AS
BEGIN
DECLARE @NA NVARCHAR(5) = 'N/A';
IF @Id IS NULL
	BEGIN
	INSERT INTO dealer.[OverrideRules]
	        ( 
			Name
			,OpCode
			,LaborPricePercentage
			,LaborPriceAmount
			,LaborHourPercentage
			,LaborHourAmount
			,PartPricePercentage
			,PartPriceAmount
			,PartNo
			,DealerId
			,OpCodeOverride
			,PartNoOverride
			,Keyword
	        )
	VALUES  ( 
			@Name,
			@OpCode,
			@LaborPricePercentage,
			@LaborPriceAmount,
			@LaborHourPercentage,
			@LaborHourAmount,
			@PartPricePercentage,
			@PartPriceAmount,
			@PartNo,
			@TenantId,
			@OpCodeOverride,
			@PartNoOverride,
			@Keyword
	        ) 
	SET @Id = @@identity
	SELECT  @Id
	END
ELSE
	BEGIN
	UPDATE orules
	SET 
		Name= @Name
		,OpCode= @OpCode
		,LaborPricePercentage= @LaborPricePercentage
		,LaborPriceAmount= @LaborPriceAmount
		,LaborHourPercentage= @LaborHourPercentage
		,LaborHourAmount= @LaborHourAmount
		,PartPricePercentage= @PartPricePercentage
		,PartPriceAmount= @PartPriceAmount
		,PartNo= @PartNo
		,OpCodeOverride= @OpCodeOverride
		,PartNoOverride= @PartNoOverride
		,Keyword= @Keyword
		,TimeUpdated = GETUTCDATE()
		FROM dealer.[OverrideRules] orules
		WHERE orules.Id = @Id
		AND orules.DealerId = @TenantId
	SELECT  @Id
	END

	--Insert or delete records to linked VehicleYears table
	IF OBJECT_ID('tempdb..#Years') is not null 
	BEGIN
		DROP TABLE #Years;
	END;

	SELECT @Id AS OverrideRuleId, ISNULL(CAST(nYears.Item AS BIGINT), orr.VehicleYear) AS VehicleYear
	,IIF(orr.OverrideRuleId IS NULL, 1, 0) AS ToInsert
	,IIF(nYears.Item IS NULL, 1, 0) AS ToDelete
	INTO #Years
	FROM dbo.SplitDelimiterString(@VehicleYears, @Delimiter) nYears
	FULL OUTER JOIN (SELECT VehicleYear, OverrideRuleId FROM dbo.OverrideRuleYears WHERE OverrideRuleId = @Id) orr ON orr.VehicleYear = CAST(nYears.Item AS BIGINT);

	INSERT INTO dbo.OverrideRuleYears (OverrideRuleId, VehicleYear)
	SELECT OverrideRuleId , VehicleYear
	FROM #Years y
	WHERE ToInsert = 1

	DELETE dest
	FROM dbo.OverrideRuleYears dest
	INNER JOIN #Years temp ON temp.VehicleYear = dest.VehicleYear AND temp.OverrideRuleId = dest.OverrideRuleId
	WHERE ToDelete = 1

	--Insert or delete records to linked VehicleMakes table
	IF OBJECT_ID('tempdb..#Makes') is not null 
	BEGIN
		DROP TABLE #Makes;
	END;

	SELECT @Id AS OverrideRuleId, ISNULL(CAST(nMakes.Item AS BIGINT), orr.VehicleMakeId) AS VehicleMakeId
	,IIF(orr.OverrideRuleId IS NULL, 1, 0) AS ToInsert
	,IIF(nMakes.Item IS NULL, 1, 0) AS ToDelete
	INTO #Makes
	FROM dbo.SplitDelimiterString(@VehicleMakes, @Delimiter) nMakes
	FULL OUTER JOIN (SELECT VehicleMakeId, OverrideRuleId FROM dbo.OverrideRuleMakes WHERE OverrideRuleId = @Id) orr ON orr.VehicleMakeId = CAST(nMakes.Item AS BIGINT);

	INSERT INTO dbo.OverrideRuleMakes (OverrideRuleId, VehicleMakeId)
	SELECT OverrideRuleId, VehicleMakeId
	FROM #Makes
	WHERE ToInsert = 1

	DELETE dest
	FROM dbo.OverrideRuleMakes dest
	INNER JOIN #Makes temp ON temp.VehicleMakeId = dest.VehicleMakeId AND temp.OverrideRuleId = dest.OverrideRuleId
	WHERE ToDelete = 1

	--Insert or delete records to linked VehicleModels table
	IF OBJECT_ID('tempdb..#Models') is not null 
	BEGIN
		DROP TABLE #Models;
	END;

	SELECT @Id AS OverrideRuleId, ISNULL(CAST(nModels.Item AS BIGINT), orr.VehicleModelId) AS VehicleModelId
	,IIF(orr.OverrideRuleId IS NULL, 1, 0) AS ToInsert
	,IIF(nModels.Item IS NULL, 1, 0) AS ToDelete
	INTO #Models
	FROM dbo.SplitDelimiterString(@VehicleModels, @Delimiter) nModels
	FULL OUTER JOIN (SELECT VehicleModelId, OverrideRuleId FROM dbo.OverrideRuleModels WHERE OverrideRuleId = @Id) orr ON orr.VehicleModelId = CAST(nModels.Item AS BIGINT);

	INSERT INTO dbo.OverrideRuleModels (OverrideRuleId, VehicleModelId)
	SELECT OverrideRuleId, VehicleModelId
	FROM #Models
	WHERE ToInsert = 1

	DELETE dest
	FROM dbo.OverrideRuleModels dest
	INNER JOIN #Models temp ON temp.VehicleModelId = dest.VehicleModelId AND temp.OverrideRuleId = dest.OverrideRuleId
	WHERE ToDelete = 1

	--Insert or delete records to linked VehicleEngines table
	IF OBJECT_ID('tempdb..#Engines') is not null 
	BEGIN
		DROP TABLE #Engines;
	END;

	SELECT @Id AS OverrideRuleId, ISNULL(CAST(nEngines.Item AS BIGINT), orr.VehicleEngineId) AS VehicleEngineId
	,IIF(orr.OverrideRuleId IS NULL, 1, 0) AS ToInsert
	,IIF(nEngines.Item IS NULL, 1, 0) AS ToDelete
	INTO #Engines
	FROM dbo.SplitDelimiterString(@VehicleEngines, @Delimiter) nEngines
	FULL OUTER JOIN (SELECT VehicleEngineId, OverrideRuleId FROM dbo.OverrideRuleEngines WHERE OverrideRuleId = @Id) orr ON orr.VehicleEngineId = CAST(nEngines.Item AS BIGINT);

	INSERT INTO dbo.OverrideRuleEngines (OverrideRuleId, VehicleEngineId)
	SELECT OverrideRuleId, VehicleEngineId
	FROM #Engines
	WHERE ToInsert = 1

	DELETE dest
	FROM dbo.OverrideRuleEngines dest
	INNER JOIN #Engines temp ON temp.VehicleEngineId = dest.VehicleEngineId AND temp.OverrideRuleId = dest.OverrideRuleId
	WHERE ToDelete = 1

	--Insert or delete records to linked VehicleTransmissions table
	IF OBJECT_ID('tempdb..#Transmissions') is not null 
	BEGIN
		DROP TABLE #Transmissions;
	END;

	SELECT DISTINCT @Id AS OverrideRuleId, ISNULL(nTransmissions.Id, orr.VinMasterTransmissionId) AS VinMasterTransmissionId
	,IIF(orr.OverrideRuleId IS NULL, 1, 0) AS ToInsert
	,IIF(nTransmissions.Id IS NULL, 1, 0) AS ToDelete
	INTO #Transmissions
	FROM vinquery.VinMasterTransmission nTransmissions
	INNER JOIN dbo.SplitDelimiterString(@VinMasterTransmissions, @Delimiter) sds ON nTransmissions.Id = CAST(ISNULL(Item,null) as int) OR nTransmissions.Transmission = @NA
	FULL OUTER JOIN (SELECT VinMasterTransmissionId, OverrideRuleId FROM dbo.OverrideRuleTransmissions WHERE OverrideRuleId = @Id) orr ON orr.VinMasterTransmissionId = nTransmissions.Id

	INSERT INTO dbo.OverrideRuleTransmissions (OverrideRuleId, VinMasterTransmissionId)
	SELECT OverrideRuleId, VinMasterTransmissionId
	FROM #Transmissions
	WHERE ToInsert = 1

	DELETE dest
	FROM dbo.OverrideRuleTransmissions dest
	INNER JOIN #Transmissions temp ON temp.VinMasterTransmissionId = dest.VinMasterTransmissionId AND temp.OverrideRuleId = dest.OverrideRuleId
	WHERE ToDelete = 1

	--Insert or delete records to linked VehicleDrivelines table
	IF OBJECT_ID('tempdb..#Drivelines') is not null 
	BEGIN
		DROP TABLE #Drivelines;
	END;

	SELECT DISTINCT @Id AS OverrideRuleId, ISNULL(nDrivelines.Id, orr.VinMasterDrivelineId) AS VinMasterDrivelineId
	,IIF(orr.OverrideRuleId IS NULL, 1, 0) AS ToInsert
	,IIF(nDrivelines.Id IS NULL, 1, 0) AS ToDelete
	INTO #Drivelines
	FROM vinquery.VinMasterDriveline nDrivelines
	INNER JOIN dbo.SplitDelimiterString(@VinMasterDrivelines, @Delimiter) sds ON nDrivelines.Id = CAST(ISNULL(Item,null) as int) OR nDrivelines.Driveline = @NA
	FULL OUTER JOIN (SELECT VinMasterDrivelineId, OverrideRuleId FROM dbo.OverrideRuleDrivelines WHERE OverrideRuleId = @Id) orr ON orr.VinMasterDrivelineId = nDrivelines.Id
	
	INSERT INTO dbo.OverrideRuleDrivelines (OverrideRuleId, VinMasterDrivelineId)
	SELECT OverrideRuleId, VinMasterDrivelineId
	FROM #Drivelines
	WHERE ToInsert = 1

	DELETE dest
	FROM dbo.OverrideRuleDrivelines dest
	INNER JOIN #Drivelines temp ON temp.VinMasterDrivelineId = dest.VinMasterDrivelineId AND temp.OverrideRuleId = dest.OverrideRuleId
	WHERE ToDelete = 1

	--Insert or delete records to linked BasicServices table
	DECLARE @LaborHourRuleIdOverride BIGINT = (SELECT IIF(@LaborHourPercentage IS NULL AND @LaborHourAmount IS NULL, NULL, @Id))
	DECLARE @LaborPriceRuleIdOverride BIGINT = (SELECT IIF(@LaborPricePercentage IS NULL AND @LaborPriceAmount IS NULL, NULL, @Id))
	DECLARE @PartPriceRuleIdOverride BIGINT = (SELECT IIF(@PartPricePercentage IS NULL AND @PartPriceAmount IS NULL, NULL, @Id))
	DECLARE @OpCodeRuleIdOverride BIGINT = (SELECT IIF(@OpCodeOverride IS NULL, NULL, @Id))
	DECLARE @PartNoRuleIdOverride BIGINT = (SELECT IIF(@PartNoOverride IS NULL, NULL, @Id))

	--Get Matching Services filtered by the provided criteria and keywords
	IF OBJECT_ID('tempdb..#ServicesWithParts') is not null 
	BEGIN
		DROP TABLE #ServicesWithParts;
	END;
	SELECT obs.OemBasicServiceId
	INTO #ServicesWithParts
	FROM amam.OemServiceParts osp
	INNER JOIN amam.OemPartNumbers opn ON opn.OemPartId = osp.OemPartId 
	INNER JOIN amam.OemBasicServices obs ON osp.OemBasicServiceId = obs.OemBasicServiceId AND obs.IsDeleted = 0 AND obs.IsIncomplete = 0 
	WHERE @PartNo IS NULL OR opn.OemPartNo = @PartNo

	IF OBJECT_ID('tempdb..#SelectedServices') is not null 
	BEGIN
		DROP TABLE #SelectedServices;
	END;
	WITH FilteredServices AS
	(SELECT afbs.OemBasicServiceId
	FROM dbo.[GetAllFilteredBasicServices](@TenantId,@VehicleYears,@VehicleMakes,@VehicleModels,@VehicleEngines,@VinMasterTransmissions,@VinMasterDriveLines,@VehicleSteerings,@Delimiter) afbs
	WHERE (@Keyword IS NULL OR afbs.[Description] LIKE ('%' + @Keyword + '%'))
	AND (@OpCode IS NULL OR @OpCode = afbs.OpCodeOverride)
	AND (@PartNo IS NULL OR (@PartNo = afbs.PartNoOverride OR afbs.OemBasicServiceId IN (SELECT * FROM #ServicesWithParts))))
	SELECT OemBasicServiceId
	INTO #SelectedServices
	FROM(
	SELECT fs.OemBasicServiceId
	FROM FilteredServices fs
	INNER JOIN dbo.SplitDelimiterString(@BasicServices, @Delimiter) bs ON CAST(fs.OemBasicServiceId AS NVARCHAR(50)) = bs.Item
	WHERE @AreServicesChecked = 1

	UNION ALL

	SELECT fs.OemBasicServiceId
	FROM FilteredServices fs
	LEFT JOIN dbo.SplitDelimiterString(@BasicServices, @Delimiter) bs ON CAST(fs.OemBasicServiceId AS NVARCHAR(50)) = bs.Item
	WHERE @AreServicesChecked = 0 AND bs.Item IS NULL) x
	
	IF OBJECT_ID('tempdb..#ModifiedBasicServices') is not null 
	BEGIN
		DROP TABLE #ModifiedBasicServices;
	END;
	SELECT 
	afbs.OemBasicServiceId AS OemBasicServiceId,
	afbs.ExistsInOverrides AS IsUpdate
	,IIF(afbs.LaborHourRuleId = @Id AND (ss.OemBasicServiceId IS NULL OR @LaborHourRuleIdOverride IS NULL), NULL, 
	IIF(afbs.LaborHourRuleId = @Id OR afbs.LaborHourRuleId IS NULL AND ss.OemBasicServiceId IS NOT NULL, @LaborHourRuleIdOverride, afbs.LaborHourRuleId)) AS LaborHourRuleId
	,IIF(afbs.LaborPriceRuleId = @Id AND (ss.OemBasicServiceId IS NULL OR @LaborPriceRuleIdOverride IS NULL), NULL, 
	IIF(afbs.LaborPriceRuleId = @Id OR afbs.LaborPriceRuleId IS NULL AND ss.OemBasicServiceId IS NOT NULL, @LaborPriceRuleIdOverride, afbs.LaborPriceRuleId)) AS LaborPriceRuleId
	,IIF(afbs.PartPriceRuleId = @Id AND (ss.OemBasicServiceId IS NULL OR @PartPriceRuleIdOverride IS NULL), NULL, 
	IIF(afbs.PartPriceRuleId = @Id OR afbs.PartPriceRuleId IS NULL AND ss.OemBasicServiceId IS NOT NULL, @PartPriceRuleIdOverride, afbs.PartPriceRuleId)) AS PartPriceRuleId
	,IIF(afbs.OpCodeRuleId = @Id AND (ss.OemBasicServiceId IS NULL OR @OpCodeRuleIdOverride IS NULL), NULL, 
	IIF(afbs.OpCodeRuleId = @Id OR afbs.OpCodeRuleId IS NULL AND ss.OemBasicServiceId IS NOT NULL, @OpCodeRuleIdOverride, afbs.OpCodeRuleId)) AS OpCodeRuleId
	,IIF(afbs.PartNoRuleId = @Id AND (ss.OemBasicServiceId IS NULL OR @PartNoRuleIdOverride IS NULL), NULL, 
	IIF(afbs.PartNoRuleId = @Id OR afbs.PartNoRuleId IS NULL AND ss.OemBasicServiceId IS NOT NULL, @PartNoRuleIdOverride, afbs.PartNoRuleId)) AS PartNoRuleId
	INTO #ModifiedBasicServices
	FROM dbo.[GetAllFilteredBasicServices](@TenantId,NULL,NULL,NULL,NULL,NULL,NULL,NULL,@Delimiter) afbs
	LEFT JOIN #SelectedServices ss ON ss.OemBasicServiceId = afbs.OemBasicServiceId
	ORDER BY ss.OemBasicServiceId

	UPDATE orbsc
	SET 
	 orbsc.LaborHourRuleId = mbs.LaborHourRuleId
	,orbsc.LaborPriceRuleId = mbs.LaborPriceRuleId
	,orbsc.PartPriceRuleId = mbs.PartPriceRuleId
	,orbsc.OpCodeRuleId = mbs.OpCodeRuleId
	,orbsc.PartNoRuleId = mbs.PartNoRuleId
	FROM dealer.OverrideRuleBasicServicesCross orbsc
	INNER JOIN #ModifiedBasicServices mbs on mbs.OemBasicServiceId = orbsc.OemBasicServiceId
	WHERE @TenantId = orbsc.DealerId AND IsUpdate = 1

	INSERT INTO dealer.OverrideRuleBasicServicesCross(DealerId, OemBasicServiceId, LaborHourRuleId, LaborPriceRuleId, PartPriceRuleId, OpCodeRuleId, PartNoRuleId)
	SELECT @TenantId as DealerId, mbs.OemBasicServiceId, mbs.LaborHourRuleId, mbs.LaborPriceRuleId, mbs.PartPriceRuleId, mbs.OpCodeRuleId, mbs.PartNoRuleId
	FROM #ModifiedBasicServices mbs
	WHERE IsUpdate = 0
END;