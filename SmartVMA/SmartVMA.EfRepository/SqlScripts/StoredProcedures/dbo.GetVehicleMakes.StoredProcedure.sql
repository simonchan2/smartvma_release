USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetVehicleMakes]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetVehicleMakes] 
	@Year NVARCHAR(max) = null
AS
BEGIN
	IF(@Year IS NOT NULL)
	BEGIN
	declare @Delimiter nvarchar(8);
	set @Delimiter =',';

		select  distinct vm.VehicleMakeId as Id, vm.VehicleMake as Make 
		from amam.Cars as car
		INNER JOIN amam.VehicleMakes as vm on car.VehicleMakeId = vm.VehicleMakeId
		INNER JOIN dbo.SplitDelimiterString(@Year, @Delimiter) as sds ON (sds.Item = CAST(ISNULL(car.VehicleYear,null) as NVARCHAR(50)))
		
	END
	ELSE
	BEGIN
		select VehicleMakeId as Id, VehicleMake as Make from amam.VehicleMakes
	END
END
