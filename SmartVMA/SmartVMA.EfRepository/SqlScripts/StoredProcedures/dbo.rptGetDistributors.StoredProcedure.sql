USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[rptGetDistributors]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[rptGetDistributors]
	@UserRoleCompany varchar(25) = '0_0_0' --'User_Role_Company'
AS 
BEGIN
	if @UserRoleCompany is null or @UserRoleCompany = ''
		select -1 companyid, 'Not available' companyname, 0 sort  
	else
	begin
		declare @UserId int
		declare @RoleId int
		declare @CompanyId int

		set @userId = convert(int,substring(@UserRoleCompany,1,CHARINDEX('_',@UserRoleCompany) - 1))

		set @UserRoleCompany = substring(@UserRoleCompany,CHARINDEX('_',@UserRoleCompany) + 1,len(@UserRoleCompany) - CHARINDEX('_',@UserRoleCompany))

		set @RoleId = convert(int,substring(@UserRoleCompany,1,CHARINDEX('_',@UserRoleCompany) - 1))

		set @CompanyId = convert(int,substring(@UserRoleCompany,CHARINDEX('_',@UserRoleCompany) + 1,len(@UserRoleCompany) - CHARINDEX('_',@UserRoleCompany)))

		declare @Role nvarchar(100)

		select @Role = Role from Roles where RoleId = @RoleId

		if (@Role is null)
			select -1 companyid, 'Not available' companyname, 0 sort  
		else
		begin
			if (@Role = 'SystemAdministrator' or @Role = 'BGExecutive')
				select 0 companyid, 'All' companyname, 0 sort  
				union all
				select distinct companyid, companyname, 1 sort from companies where parentid in (select CompanyId from Companies where parentId is null)
			else if (@Role = 'DistributorAdmin' or @Role = 'DistributorExecutive')
				select t1.companyId, t1.companyname, 1 sort from companies t1 
					inner join Companies t2 on t1.ParentId = t2.CompanyId 
					where t1.companyId = @CompanyId and
						t2.ParentId is null
			else 
				select -1 companyid, 'Not available' companyname, 0 sort  

		end
			
	end
end

GO
