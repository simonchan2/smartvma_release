USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetVehicleSteerings]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetVehicleSteerings]

AS
BEGIN
	select distinct VehicleSteering from vinquery.VinMaster
	where VehicleSteering is not null
		and VehicleSteering != ''
END

GO
