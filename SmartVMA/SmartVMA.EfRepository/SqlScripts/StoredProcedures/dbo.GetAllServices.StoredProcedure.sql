USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetAllServices]    Script Date: 06.09.2016 12:43:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Krste Bozinoski
-- Create date: 06.07.2016
-- Description:	List all services.
-- Services can be filtered by:
--		DealerVehicleId
--		Mileage
--		Specific service ids in the following form: serviceId_serviceTableId
-- =============================================
ALTER PROCEDURE [dbo].[GetAllServices]
    (
	@TenantId int,
	@CarId int = null, 
	@DealerVehicleId int = null,
	@Mileage int = null,
	@IsLOF bit = null,
	@ServiceIds nvarchar(max) = null,
	@VinMasterTransmission int = null,
	@VinMasterDriveLine int = null,
	@DealerCustomerId int = null,
	@AppointmentPresentationId bigint = null
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @MeasurementUnitId int;
	set @MeasurementUnitId = (select top 1 CAST(value as int) from CompanySettings where CompanyId = @TenantId and Name = 'MeasurementUnit');

	declare @LanguageId int;
	set @LanguageId = (select top 1 CAST(value as int) from CompanySettings where CompanyId = @TenantId and Name = 'Language');
	set @LanguageId = @LanguageId    -- values: English = 1,        French = 2,        Spanish = 3 

	declare @LaborRate float;
	set @LaborRate = ISNULL((select top 1 CAST(value as float) from CompanySettings where CompanyId = @TenantId and Name = 'DefaultLaborRate'),0);

	declare @UseOEMMenus bit;
	set @UseOEMMenus = (select top 1 CAST(value as bit) from CompanySettings where CompanyId = @TenantId and Name = 'UseOEMMenus');

	declare @VehicleTransmission int = null;
	if(@VinMasterTransmission IS NOT NULL)
	begin
	set @VehicleTransmission = (select VehicleTransmissionId from vinquery.VinMasterTransmission where Id = @VinMasterTransmission)
	end
	declare @VehicleDriveLine int = null;
	if(@VinMasterDriveLine IS NOT NULL)
	begin
	set @VehicleDriveLine = (select VehicleDrivelineId from vinquery.VinMasterDriveline where Id = @VinMasterDriveLine)
	end
	
	declare @ServicesTable table(
		ServiceId bigint,
		OpCode nvarchar(50),
		FullDescription nvarchar(1000),
		CarId int,
		Mileage int,
		
		IsFluid bit,
		IsEngineOil bit,
		IsLOF bit,
		CanBeStriked BIT,
		IsStrikeOut bit,
		IsDeclined bit,
		IsPreviouslyServed bit,
		BgProtectionPlanId int,
		
		VideoCode nvarchar(500),
		MenuLevel int,
		ServiceType int,
		ServiceMenuType int,

		LaborHour float,
		LabourRate float,
		PartsPrice float,
		LaborPrice float,
		Price float,
		ShopChargesPartsPrice float,
		ShopChargesLaborPrice float,
		TaxIncludedPartsPrice float,
		TaxIncludedLaborPrice float
	)
	INSERT INTO @ServicesTable 
	SELECT *, NULL AS ShopChargesPartsPrice, NULL AS ShopChargesLaborPrice, NULL AS TaxIncludedPartsPrice, NULL AS TaxIncludedLaborPrice
	FROM [dbo].[GetLeftHandServices](
	@TenantId,
	@CarId, 
	@DealerVehicleId,
	@Mileage,
	@IsLOF,
	@VehicleTransmission,
	@VehicleDriveLine, 
	@AppointmentPresentationId,
	@LanguageId,
	@MeasurementUnitId,
	@LaborRate,
	4) as lhs
	-- if it's regular service then also load additional services
	IF @IsLOF != 1
	BEGIN
		INSERT INTO @ServicesTable 
		SELECT * , NULL AS ShopChargesPartsPrice, NULL AS ShopChargesLaborPrice, NULL AS TaxIncludedPartsPrice, NULL AS TaxIncludedLaborPrice
		FROM [dbo].[GetRightHandServices](
		@TenantId,
		@CarId, 
		@DealerVehicleId,
		@Mileage,
		@IsLOF,
		@VehicleTransmission,
		@VehicleDriveLine,
		@MeasurementUnitId,
		@LaborRate,
		@LanguageId) rhServices 
		--INCLUDE only services which do not exist yet
		WHERE rhServices.ServiceId NOT IN (select serviceId from @ServicesTable);
	END

	declare @ServiceIdsTable table(Id int,ServiceOriginTableId int);
	insert into @ServiceIdsTable select * from dbo.SplitServiceIdValues(@ServiceIds);

	IF(@DealerCustomerId IS NOT NULL)
	BEGIN
		declare @PreviousCustomerServices table(
			ServiceId bigint,
			IsStriked bit
		)

		INSERT INTO @PreviousCustomerServices
			SELECT aps.OrigServiceId, aps.IsStriked
		FROM dealer.AppointmentPresentations ap
		INNER JOIN dealer.AppointmentServices aps ON ap.AppointmentPresentationId = aps.AppointmentPresentationId
		WHERE ap.DealerCustomerId = @DealerCustomerId AND ap.DealerId = @TenantId
		AND ap.AppointmentPresentationId = (select max(AppointmentPresentationId) from dealer.AppointmentPresentations WHERE DealerCustomerId = @DealerCustomerId)

		--Setting up Previous declined services for our customer
		UPDATE  @ServicesTable 
		SET IsDeclined = 1 WHERE ServiceId IN (SELECT ServiceId FROM @PreviousCustomerServices WHERE IsStriked=1)

		--Setting up Previous accepted services for our customer
		UPDATE  @ServicesTable 
		SET IsPreviouslyServed = 1 WHERE ServiceId IN (SELECT ServiceId FROM @PreviousCustomerServices WHERE IsStriked=0)
	END

	--PREPARE params for calculationg shop and tax charges
	declare @includeShopCharges bit;
	declare @shopChargeAmount float;
	declare @shopChargesApplyToParts bit;
	declare @shopChargesApplyToLabor bit;
	declare @minShopCharges float;
	declare @maxShopCharges float;
	set @includeShopCharges = (select top 1 CAST(value as bit) from CompanySettings where CompanyId = @TenantId and Name = 'PrintShopCharges');
	if(@includeShopCharges = 1)
	BEGIN
		set @shopChargeAmount = (select top 1 CAST(value as float) from CompanySettings where CompanyId = @TenantId and Name = 'ShopChargeAmount');
		set @shopChargesApplyToParts = (select top 1 CAST(value as bit) from CompanySettings where CompanyId = @TenantId and Name = 'ShopChargesApplyToParts');
		set @shopChargesApplyToLabor = (select top 1 CAST(value as bit) from CompanySettings where CompanyId = @TenantId and Name = 'ShopChargesApplyToLabor');
		set @minShopCharges = (select top 1 CAST(value as float) from CompanySettings where CompanyId = @TenantId and Name = 'ShopChargeMinimum');
		set @maxShopCharges = (select top 1 CAST(value as float) from CompanySettings where CompanyId = @TenantId and Name = 'ShopChargeMaximum');
	END

	declare @includeTaxCharges bit;
	declare @taxChargeAmount float;
	declare @taxChargesApplyToParts bit;
	declare @taxChargesApplyToLabor bit;
	set @includeTaxCharges = (select top 1 CAST(value as bit) from CompanySettings where CompanyId = @TenantId and Name = 'DisplayTexesInMenus');
	if(@includeTaxCharges = 1)
	BEGIN
		set @taxChargeAmount = (select top 1 CAST(value as float) from CompanySettings where CompanyId = @TenantId and Name = 'TaxRateAmount');
		set @taxChargesApplyToParts = (select top 1 CAST(value as bit) from CompanySettings where CompanyId = @TenantId and Name = 'TaxRateApplyToParts');
		set @taxChargesApplyToLabor = (select top 1 CAST(value as bit) from CompanySettings where CompanyId = @TenantId and Name = 'TaxRateApplyToLabor');
	END

	--CALCULATE Shop Charges And Tax Charges
	--UPDATE st
	--SET 
	--ShopChargesPartsPrice = SC.ShopChargesPartsPrice
	--,ShopChargesLaborPrice = SC.ShopChargesLaborPrice
	--,TaxIncludedPartsPrice = Tax.TaxIncludedPartsPrice
	--,TaxIncludedLaborPrice = Tax.TaxIncludedLaborPrice
	--FROM @ServicesTable st
	---- helper calculations
	--CROSS APPLY(
	--	SELECT 
	--	PartsPrice * (@shopChargeAmount / 100) as PartsPriceSC,
	--	(@LaborRate * LaborHour) * (@shopChargeAmount / 100) as LaborPriceSC
	--) calc
	---- calculate ShopCharges for PartPrice and LaborHour
	--CROSS APPLY(
	--SELECT
	--	CASE @shopChargesApplyToParts 
	--	WHEN 1 THEN CASE 
	--		WHEN calc.PartsPriceSC < @minShopCharges 
	--		THEN @minShopCharges
	--		WHEN calc.PartsPriceSC > @maxShopCharges
	--		THEN @maxShopCharges
	--		ELSE calc.PartsPriceSC
	--	END
	--	ELSE 0 END AS ShopChargesPartsPrice

	--	,CASE @shopChargesApplyToLabor 
	--	WHEN 1 THEN CASE 
	--		WHEN calc.LaborPriceSC < @minShopCharges 
	--		THEN @minShopCharges
	--		WHEN calc.LaborPriceSC > @maxShopCharges
	--		THEN @maxShopCharges
	--		ELSE calc.LaborPriceSC
	--	END
	--	ELSE 0 END AS ShopChargesLaborPrice
	--) SC
	---- calculate Taxes for PartPrice and LaborHour
	--CROSS APPLY(
	--SELECT 
	--	CASE @taxChargesApplyToParts 
	--	WHEN 1 THEN (PartsPrice + SC.ShopChargesPartsPrice) * (@taxChargeAmount / 100)
	--	ELSE 0 END AS TaxIncludedPartsPrice

	--	,CASE @taxChargesApplyToLabor 
	--	WHEN 1 THEN (LaborPrice + SC.ShopChargesLaborPrice) * (@taxChargeAmount / 100)
	--	ELSE 0 END AS TaxIncludedLaborPrice
	--) Tax

	UPDATE st
	SET 
	ShopChargesPartsPrice = 0
	,ShopChargesLaborPrice = 0
	,TaxIncludedPartsPrice = 0
	,TaxIncludedLaborPrice = 0
	FROM @ServicesTable st


	-- RETURN distinct list of all services
	--select *
	--from @ServicesTable
	--ORDER BY IsLOF DESC, FullDescription ASC
	IF (@UseOEMMenus = 1)
	BEGIN 
		select distinct *
		from @ServicesTable
		where (@ServiceIds is null or @ServiceIds = '' or exists (
				select *
				from @ServiceIdsTable
				where ServiceId = Id and ServiceOriginTableId = ServiceType))
		order by IsLOF desc, FullDescription 
	END
	ELSE 
		BEGIN
			select distinct *
			from @ServicesTable
			where (@ServiceIds is null or @ServiceIds = '' or exists (
					select *
					from @ServiceIdsTable
					where ServiceId = Id and ServiceOriginTableId = ServiceType))
			order by FullDescription asc,ServiceId asc
		END
END

GO
