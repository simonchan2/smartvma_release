USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetVehicleFiltersByVinMaster]    Script Date: 29.09.2016 11:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetVehicleFiltersByVinMaster] 
	@Years nvarchar(max),
	@Makes nvarchar(max),
	@Models nvarchar(max),
	@EngineTypes nvarchar(max),
	@TransmissionTypes nvarchar(max),
	@DriveLines nvarchar(max),
	@VinMasters nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @Delimiter nvarchar(8);
	set @Delimiter =',';
	declare @FiltersTable table(Id int null,[Text] nvarchar(500),EntityType int);

	declare @VinMastersTable table(Id int null);
	INSERT INTO @VinMastersTable select CAST(ISNULL(Item,null) as int) as Id FROM dbo.SplitDelimiterString(@VinMasters, @Delimiter)

	--insert year
	INSERT INTO @FiltersTable
	SELECT distinct vinMaster.VehicleYear as Id, vinMaster.VehicleYear as [Text], 0 as EntityType
	FROM vinquery.VinMaster as vinMaster
	WHERE vinMaster.VinMasterId in (SELECT * FROM @VinMastersTable)

	declare @YearsTable table(Id int null);
	IF((select COUNT(*) from @FiltersTable where EntityType = 0) = 1)
	BEGIN INSERT INTO @YearsTable select Id from @FiltersTable where EntityType = 0 END
	ELSE BEGIN INSERT INTO @YearsTable SELECT CAST(ISNULL(Item,null) as int) as Id FROM dbo.SplitDelimiterString(@Years, @Delimiter) END
	
	-- get vehicle makes
	INSERT INTO @FiltersTable
	SELECT DISTINCT MAX(vinMaster.VinMasterId) as Id, vinMaster.VehicleMake as [Text], 1 as EntityType
	FROM vinquery.VinMaster as vinMaster
	INNER JOIN @YearsTable as Years ON (Years.Id = vinMaster.VehicleYear)
	WHERE vinMaster.VinMasterId in (SELECT * FROM @VinMastersTable)
	GROUP BY vinMaster.VehicleMake

	declare @MakesTable table(Id int null);
	IF((select COUNT(*) from @FiltersTable where EntityType = 1) = 1)
	BEGIN INSERT INTO @MakesTable select Id from @FiltersTable where EntityType = 1 END
	ELSE BEGIN INSERT INTO @MakesTable SELECT CAST(ISNULL(Item,null) as int) as Id FROM dbo.SplitDelimiterString(@Makes, @Delimiter) END
	
	-- get vehicle models
	INSERT INTO @FiltersTable
	select distinct MAX(vinMaster.VinMasterId) as Id, vinMaster.VehicleModel as [Text], 2 as EntityType
	FROM vinquery.VinMaster as vinMaster
	INNER JOIN @YearsTable as Years ON (Years.Id = vinMaster.VehicleYear)
	INNER JOIN (SELECT VehicleMake FROM vinquery.VinMaster where VinMasterId in (SELECT * FROM @MakesTable)) as Makes ON (Makes.VehicleMake = vinMaster.VehicleMake)
	WHERE vinMaster.VinMasterId in (SELECT * FROM @VinMastersTable)
	GROUP BY vinMaster.VehicleModel
	
	declare @ModelsTable table(Id int null);
	IF((select COUNT(*) from @FiltersTable where EntityType = 2) = 1)
	BEGIN INSERT INTO @ModelsTable select Id from @FiltersTable where EntityType = 2 END
	ELSE BEGIN INSERT INTO @ModelsTable SELECT CAST(ISNULL(Item,null) as int) as Id FROM dbo.SplitDelimiterString(@Models, @Delimiter) END
	
	-- get vehicle engines
	INSERT INTO @FiltersTable
	select distinct MAX(vinMaster.VinMasterId) as Id, vinMaster.VehicleEngine as [Text], 3 as EntityType
	FROM vinquery.VinMaster as vinMaster
	INNER JOIN @YearsTable as Years ON (Years.Id = vinMaster.VehicleYear)
	INNER JOIN (SELECT VehicleMake FROM vinquery.VinMaster where VinMasterId in (SELECT * FROM @MakesTable)) as Makes ON (Makes.VehicleMake = vinMaster.VehicleMake)
	INNER JOIN (SELECT VehicleModel FROM vinquery.VinMaster where VinMasterId in (SELECT * FROM @ModelsTable)) as Models ON (Models.VehicleModel = vinMaster.VehicleModel)
	WHERE vinMaster.VinMasterId in (SELECT * FROM @VinMastersTable)
	GROUP BY vinMaster.VehicleEngine

	declare @EngineTypesTable table(Id int null);
	IF((select COUNT(*) from @FiltersTable where EntityType = 3) = 1)
	BEGIN INSERT INTO @EngineTypesTable select Id from @FiltersTable where EntityType = 3 END
	ELSE BEGIN INSERT INTO @EngineTypesTable SELECT CAST(ISNULL(Item,null) as int) as Id FROM dbo.SplitDelimiterString(@EngineTypes, @Delimiter) END
	
	-- get vehicle transmissions
	INSERT INTO @FiltersTable
	select distinct MAX(vinMaster.VinMasterId) as Id, IIF(vinMaster.VehicleTransmission IS NULL OR vinMaster.VehicleTransmission = '', ISNULL(vinMaster.VehicleTransmissionShort,'N/A'), vinMaster.VehicleTransmission)  as [Text], 4 as EntityType
	FROM vinquery.VinMaster as vinMaster
	INNER JOIN @YearsTable as Years ON (Years.Id = vinMaster.VehicleYear)
	INNER JOIN (SELECT VehicleMake FROM vinquery.VinMaster where VinMasterId in (SELECT * FROM @MakesTable)) as Makes ON (Makes.VehicleMake = vinMaster.VehicleMake)
	INNER JOIN (SELECT VehicleModel FROM vinquery.VinMaster where VinMasterId in (SELECT * FROM @ModelsTable)) as Models ON (Models.VehicleModel = vinMaster.VehicleModel)
	INNER JOIN (SELECT VehicleEngine FROM vinquery.VinMaster where VinMasterId in (SELECT * FROM @EngineTypesTable)) as Engines ON (Engines.VehicleEngine = vinMaster.VehicleEngine)
	WHERE vinMaster.VinMasterId in (SELECT * FROM @VinMastersTable)
	GROUP BY IIF(vinMaster.VehicleTransmission IS NULL OR vinMaster.VehicleTransmission = '', ISNULL(vinMaster.VehicleTransmissionShort,'N/A'), vinMaster.VehicleTransmission)

	declare @TransmissionTypesTable table(Id int null);
	IF((select COUNT(*) from @FiltersTable where EntityType = 4) = 1)
	BEGIN INSERT INTO @TransmissionTypesTable select Id from @FiltersTable where EntityType = 4 END
	ELSE BEGIN INSERT INTO @TransmissionTypesTable SELECT CAST(ISNULL(Item,null) as int) as Id FROM dbo.SplitDelimiterString(@TransmissionTypes, @Delimiter) END
	

	-- get vehicle drivelines
	INSERT INTO @FiltersTable
	select distinct MAX(vinMaster.VinMasterId) as Id, IIF(vinMaster.VehicleDriveline IS NULL OR vinMaster.VehicleDriveline = '', ISNULL(vinMaster.VehicleDrivelineShort,'N/A'), vinMaster.VehicleDriveline) as [Text], 5 as EntityType
	FROM vinquery.VinMaster as vinMaster
	INNER JOIN @YearsTable as Years ON (Years.Id = vinMaster.VehicleYear)
	INNER JOIN (SELECT VehicleMake FROM vinquery.VinMaster where VinMasterId in (SELECT * FROM @MakesTable)) as Makes ON (Makes.VehicleMake = vinMaster.VehicleMake)
	INNER JOIN (SELECT VehicleModel FROM vinquery.VinMaster where VinMasterId in (SELECT * FROM @ModelsTable)) as Models ON (Models.VehicleModel = vinMaster.VehicleModel)
	INNER JOIN (SELECT VehicleEngine FROM vinquery.VinMaster where VinMasterId in (SELECT * FROM @EngineTypesTable)) as Engines ON (Engines.VehicleEngine = vinMaster.VehicleEngine)
	INNER JOIN (SELECT IIF(vinMaster.VehicleTransmission IS NULL OR vinMaster.VehicleTransmission = '', ISNULL(vinMaster.VehicleTransmissionShort,'N/A'), vinMaster.VehicleTransmission) as Transmission FROM vinquery.VinMaster where VinMasterId in (SELECT * FROM @TransmissionTypesTable)) as TransmissionTypes ON (TransmissionTypes.Transmission = IIF(vinMaster.VehicleTransmission IS NULL OR vinMaster.VehicleTransmission = '', ISNULL(vinMaster.VehicleTransmissionShort,'N/A'), vinMaster.VehicleTransmission))
	WHERE vinMaster.VinMasterId in (SELECT * FROM @VinMastersTable)
	GROUP BY IIF(vinMaster.VehicleDriveline IS NULL OR vinMaster.VehicleDriveline = '', ISNULL(vinMaster.VehicleDrivelineShort,'N/A'), vinMaster.VehicleDriveline)

	INSERT INTO @FiltersTable
	VALUES(1, 'SecondarySearch', 6)

	SELECT Id, [Text], EntityType
	FROM @FiltersTable
END

GO
