USE [SmartVmaDev]
GO

/****** Object:  StoredProcedure [dbo].[DeleteObsoleteAppointmentPresentations]    Script Date: 10/19/2016 3:03:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetPdfDocumentsByAppointmentPresentationDate]
	@TenantId int,
	@DateTo datetime
AS
	select PdfDocumentId, AppointmentPresentationId, Name, FilePathName from document.PdfDocuments
where AppointmentPresentationId IN 
	(select AppointmentPresentationId from dealer.AppointmentPresentations 
	where TimeCreated < @DateTo)

/*
	exec dbo.GetPdfDocumentsByAppointmentPresentationDate null, @DateTo='20160919'
*/

GO