USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[GetVehicleYears]    Script Date: 01.09.2016 16:01:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[GetVehicleYears] 
AS
BEGIN
		select  distinct VehicleYear as Id, CAST(VehicleYear as nvarchar(10))  as [Year]
		from amam.Cars
END



GO
