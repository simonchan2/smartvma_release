USE [SmartVmaDev]
GO
/****** Object:  UserDefinedFunction [dbo].[udf_UTC2Central]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[udf_UTC2Central](@UTCDateTime DateTime)

RETURNS DateTime


AS


BEGIN


DECLARE @CentralTime Datetime


DECLARE @DSTBeginMonth datetime

SET @DSTBeginMonth=  CONCAT('3/1/',CAST(datepart(yyyy,@UTCDateTime) AS char))

DECLARE @DSTEndMonth datetime

SET @DSTEndMonth=  CONCAT('11/1/',CAST(datepart(yyyy,@UTCDateTime) AS char))


SET @CentralTime = (SELECT


                                         CASE 

                                                WHEN @UTCDateTime >= (SELECT DateADD(SECOND, 7200,dateadd(dd,8-datepart(dw,@DSTBeginMonth),@DSTBeginMonth)+7)) AND  @UTCDateTime < (SELECT DateADD(SECOND, 7200,dateadd(dd,8-datepart(dw,@DSTEndMonth),@DSTEndMonth)))

                                                THEN DATEADD(SECOND, -18000,  @UTCDateTime) 

                                                ELSE DATEADD(SECOND, -21600,  @UTCDateTime)

                                         END)


RETURN @CentralTime


END


GO
