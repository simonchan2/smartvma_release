USE [SmartVmaDev]
GO
/****** Object:  UserDefinedFunction [dbo].[GetFilteredAppointmentServices]    Script Date: 7.9.2016 г. 18:48:36 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
SELECT * FROM [dbo].[GetFilteredAppointmentServices](1089,null,1,null,null)

*/
CREATE FUNCTION [dbo].[GetFilteredAppointmentServices](
@TenantId INT,
@DealerVehicleId INT = NULL,
@IsStriked BIT = NULL,
@AppointmentPresentationId BIGINT = NULL,
@ServiceTypeId INT = NULL
)
RETURNS TABLE
AS
RETURN
WITH AppointmentServiceIds AS(
SELECT
		MAX(services.AppointmentServicesId) as AppointmentServicesId
	FROM dealer.AppointmentServices services
		INNER JOIN dealer.AppointmentPresentations as presentation on services.AppointmentPresentationId = presentation.AppointmentPresentationId
	WHERE 
		services.DealerId = @TenantId
		AND (@ServiceTypeId is null or services.ServiceTypeId = @ServiceTypeId)
		AND (@AppointmentPresentationId is null or services.AppointmentPresentationId = @AppointmentPresentationId)
		AND (@IsStriked is null or IsStriked = @IsStriked)
		AND (@DealerVehicleId IS NULL OR presentation.DealerVehicleId = @DealerVehicleId)
	GROUP BY OrigServiceId
		)
		--SELECT *
		--FROM AppointmentServiceIds
		SELECT DISTINCT
		services.OrigServiceId as ServiceId
		,services.OpCode
		,services.OpDescription as FullDescription
		--,0 as CarId
		--,0 as Mileage
		--,convert(bit,0) as IsFluid
		--,convert(bit,0) as IsEngineOil
		--,convert(bit,0) as IsLOF
		--,convert(bit,0) as IsStrikeOut
		,services.IsStriked as IsDeclined
		--,convert(bit,0) as IsPreviouslyServed
		--,null as BgProtectionPlanId
		--,'' as VideoCode
		--,1 as MenuLevel
		--,5 as ServiceType
		--,2 as ServiceMenuType
		,services.LaborHour
		,services.LaborRate as LabourRate
		,services.PartsPrice
		,services.LaborPrice
		,services.Price

		FROM AppointmentServiceIds apsi
		INNER JOIN dealer.AppointmentServices services ON services.AppointmentServicesId = apsi.AppointmentServicesId


GO
