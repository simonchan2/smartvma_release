USE [SmartVmaDev]
GO

/****** Object:  UserDefinedFunction [dbo].[GetOemComponentIdByCriteria_TEST]    Script Date: 12/12/2016 7:37:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[GetOemComponentIdByCriteria](
@Transmissions NVARCHAR(MAX),
@DriveLines NVARCHAR(MAX),
@Steerings NVARCHAR(MAX), 
@Delimiter NVARCHAR(8))
RETURNS @Components TABLE (
    OemComponentId BIGINT
)
AS
BEGIN
	DECLARE @NA NVARCHAR(5) = 'N/A';
	
	DECLARE @DriveLinesTable TABLE(Id int null);
	INSERT INTO @DriveLinesTable 
	SELECT DISTINCT CAST(ISNULL(Item,null) as int) as Id FROM dbo.SplitDelimiterString(@Drivelines, @Delimiter)
	UNION
	SELECT Id FROM vinquery.VinMasterDriveline WHERE Driveline = @NA

	DECLARE @TransmissionTypesTable TABLE(Id int null);
	INSERT INTO @TransmissionTypesTable 
	SELECT DISTINCT CAST(ISNULL(Item,null) as int) as Id FROM dbo.SplitDelimiterString(@Transmissions, @Delimiter)
	UNION
	SELECT Id FROM vinquery.VinMasterTransmission WHERE Transmission = @NA

	;WITH VD AS(
		SELECT DISTINCT vd.VehicleDrivelineId, vd.VehicleDriveline
		FROM vinquery.VinMasterDriveline vmd
		INNER JOIN amam.VehicleDrivelines vd ON vd.VehicleDrivelineId = vmd.VehicleDrivelineId
		WHERE @Drivelines IS NULL OR (vmd.Id IN (SELECT Id FROM @DriveLinesTable))
	),
	VT AS(
		SELECT DISTINCT vt.VehicleTransmissionId, vt.VehicleTransmission
		FROM vinquery.VinMasterTransmission vmt
		INNER JOIN amam.VehicleTransmissions vt ON vt.VehicleTransmissionId = vmt.VehicleTransmissionId
		WHERE @Transmissions IS NULL OR (vmt.Id IN (SELECT Id FROM @TransmissionTypesTable))
	)
	INSERT INTO @Components
	SELECT DISTINCT oc.OemComponentId as ComponentId
	FROM amam.OemComponents oc
	INNER JOIN VD ON VD.VehicleDriveline = oc.VehicleDrivelineShort
	INNER JOIN VT ON VT.VehicleTransmission = oc.VehicleTransmissionShort

	
RETURN
END
GO


