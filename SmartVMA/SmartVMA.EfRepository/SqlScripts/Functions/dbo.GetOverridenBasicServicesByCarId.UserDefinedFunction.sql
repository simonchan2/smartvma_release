USE [SmartVmaDev]
GO

/****** Object:  UserDefinedFunction [dbo].[GetOverridenBasicServicesByCarId_TEST]    Script Date: 12/12/2016 7:46:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--SELECT * FROM GetOverridenBasicServicesByCarId(2, NULL, NULL, NULL, 3, 500, ',')
ALTER FUNCTION [dbo].[GetOverridenBasicServicesByCarId](
@CarId INT, 
@Transmissions NVARCHAR(MAX),
@DriveLines NVARCHAR(MAX),
@Steerings NVARCHAR(MAX),
@OemBasicServiceIds NVARCHAR(MAX),
@TenantId INT, 
@LaborRate FLOAT,
@Delimiter NVARCHAR(8))
RETURNS TABLE
AS
RETURN
WITH CarProperties AS(
SELECT TOP 1 
CAST(VehicleYear AS NVARCHAR(50)) AS VehicleYears, 
CAST(VehicleMakeId AS NVARCHAR(50)) AS VehicleMakes, 
CAST(VehicleModelId AS NVARCHAR(50)) AS VehicleModels, 
CAST(VehicleEngineId AS NVARCHAR(50)) AS VehicleEngines
FROM amam.Cars c
WHERE c.CarId = @CarId)
SELECT 
	orules.OemBasicServiceId
	,orules.OpCodeOverride
	,orules.LaborHourBeforeOverride
	,orules.LaborHourOverride
	,orules.LaborPriceBeforeOverride
	,orules.LaborPriceOverride
	,orules.PartsPriceBeforeOverride
	,orules.PartsPriceOverride
	FROM dbo.GetOverridenBasicServices_TEST1(@TenantId, NULL, (SELECT VehicleYears FROM CarProperties),  (SELECT VehicleMakes FROM CarProperties), (SELECT VehicleModels FROM CarProperties), (SELECT VehicleEngines FROM CarProperties), 
	@Transmissions, @DriveLines, @Steerings,@OemBasicServiceIds,@LaborRate,@Delimiter) orules


GO


