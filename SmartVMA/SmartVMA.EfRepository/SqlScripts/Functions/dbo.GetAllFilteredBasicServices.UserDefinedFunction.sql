USE [SmartVmaDev]
GO
/****** Object:  UserDefinedFunction [dbo].[GetAllFilteredBasicServices]    Script Date: 12/8/2016 4:46:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[GetAllFilteredBasicServices](
@TenantId INT,
@VehicleYears NVARCHAR(MAX),
@VehicleMakes NVARCHAR(MAX),
@VehicleModels NVARCHAR(MAX),
@VehicleEngines NVARCHAR(MAX),
@VinMasterTransmissions NVARCHAR(MAX),
@VinMasterDriveLines NVARCHAR(MAX),
@VehicleSteerings NVARCHAR(MAX),
@Delimiter NVARCHAR(8))
RETURNS TABLE
AS
RETURN
		SELECT 
		 obs.OemBasicServiceId
		 ,IIF(orbs.OemBasicServiceId IS NULL, 0, 1) ExistsInOverrides
		,obs.CarId
		,obs.OemComponentId
		,orpartno.PartNoOverride
		,oropcode.OpCodeOverride
		,oc.[Description] + ' ' + obs.OpDescription AS [Description]
		,orbs.LaborHourRuleId
		,orbs.LaborPriceRuleId
		,orbs.PartPriceRuleId
		,orbs.OpCodeRuleId
		,orbs.PartNoRuleId
		FROM amam.OemBasicServices obs
		INNER JOIN GetCarIdByCriteria(@VehicleYears, @VehicleMakes, @VehicleModels, @VehicleEngines, @VinMasterDriveLines, @VinMasterTransmissions, @Delimiter) cars ON cars.CarId = obs.CarId
		INNER JOIN dbo.GetOemComponentIdByCriteria(@VinMasterTransmissions, @VinMasterDriveLines, @VehicleSteerings, @Delimiter) components ON components.OemComponentId = obs.OemComponentId
		INNER JOIN amam.OemComponents oc ON oc.OemComponentId = components.OemComponentId
		LEFT JOIN dealer.OverrideRuleBasicServicesCross orbs ON orbs.OemBasicServiceId = obs.OemBasicServiceId AND orbs.DealerId = @TenantId
		LEFT JOIN dealer.OverrideRules oropcode ON oropcode.Id = orbs.OpCodeRuleId
		LEFT JOIN dealer.OverrideRules orpartno ON orpartno.Id = orbs.PartNoRuleId
		WHERE obs.IsIncomplete = 0 AND obs.IsDeleted = 0

