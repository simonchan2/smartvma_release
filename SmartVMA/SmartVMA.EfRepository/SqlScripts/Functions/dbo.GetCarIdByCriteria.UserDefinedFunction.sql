USE [SmartVmaDev]
GO
/****** Object:  UserDefinedFunction [dbo].[GetCarIdByCriteria]    Script Date: 12/12/2016 7:31:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[GetCarIdByCriteria](
	@Years NVARCHAR(MAX), 
	@Makes NVARCHAR(MAX), 
	@Models NVARCHAR(MAX), 
	@Engines NVARCHAR(MAX), 
	@Drivelines NVARCHAR(MAX),
	@Transmissions NVARCHAR(MAX),
	@Delimiter NVARCHAR(8)
)
RETURNS @Cars TABLE (
    CarId BIGINT
)
AS
BEGIN
	DECLARE @NA NVARCHAR(5) = 'N/A';
	DECLARE @YearsTable TABLE(Id int null);
	INSERT INTO @YearsTable select DISTINCT CAST(ISNULL(Item,null) as int) as Id FROM dbo.SplitDelimiterString(@Years, @Delimiter);

	DECLARE @MakesTable TABLE(Id int null);
	INSERT INTO @MakesTable SELECT DISTINCT CAST(ISNULL(Item,null) as int) as Id FROM dbo.SplitDelimiterString(@Makes, @Delimiter);

	DECLARE @ModelsTable TABLE(Id int null);
	INSERT INTO @ModelsTable SELECT DISTINCT CAST(ISNULL(Item,null) as int) as Id FROM dbo.SplitDelimiterString(@Models, @Delimiter);

	DECLARE @EngineTypesTable TABLE(Id int null);
	INSERT INTO @EngineTypesTable SELECT DISTINCT CAST(ISNULL(Item,null) as int) as Id FROM dbo.SplitDelimiterString(@Engines, @Delimiter);
		
	DECLARE @DriveLinesTable TABLE(Id int null);
	INSERT INTO @DriveLinesTable 
	SELECT DISTINCT CAST(ISNULL(Item,null) as int) as Id FROM dbo.SplitDelimiterString(@Drivelines, @Delimiter)
	UNION
	SELECT Id FROM vinquery.VinMasterDriveline WHERE Driveline = @NA

	DECLARE @TransmissionTypesTable TABLE(Id int null);
	INSERT INTO @TransmissionTypesTable 
	SELECT DISTINCT CAST(ISNULL(Item,null) as int) as Id FROM dbo.SplitDelimiterString(@Transmissions, @Delimiter)
	UNION
	SELECT Id FROM vinquery.VinMasterTransmission WHERE Transmission = @NA

	INSERT INTO @Cars
	SELECT DISTINCT c.CarId 
	FROM amam.Cars as c 
	INNER JOIN amam.CarVinShorts as cv on cv.CarId = c.CarId 
	INNER JOIN vinquery.VinMaster as vm on vm.VinMasterId = cv.VinMasterId
	INNER JOIN amam.VehicleYears vy ON vy.VehicleYear = c.VehicleYear AND (@Years IS NULL OR @Years = '' OR (vy.VehicleYear IN (SELECT Id FROM @YearsTable)))
	INNER JOIN amam.VehicleMakes vma ON vma.VehicleMakeId = c.VehicleMakeId AND (@Makes IS NULL OR @Makes = '' OR (vma.VehicleMakeId IN (SELECT Id FROM @MakesTable)))
	INNER JOIN amam.VehicleModels vmo ON vmo.VehicleModelId = c.VehicleModelId AND (@Models IS NULL OR @Models = '' OR (vmo.VehicleModelId IN (SELECT Id FROM @ModelsTable)))
	INNER JOIN amam.VehicleEngines ve ON ve.VehicleEngineId = c.VehicleEngineId AND (@Engines IS NULL OR @Engines = '' OR (ve.VehicleEngineId IN (SELECT Id FROM @EngineTypesTable)))
	INNER JOIN vinquery.VinMasterTransmission vmt ON vmt.Id = vm.VinMasterTransmissionId AND (@Transmissions IS NULL OR @Transmissions = '' OR (vmt.Id IN (SELECT Id FROM @TransmissionTypesTable)))
	INNER JOIN vinquery.VinMasterDriveline vmd ON vmd.Id = vm.VinMasterDrivelineId AND (@Drivelines IS NULL OR @Drivelines = '' OR (vmd.Id IN (SELECT Id FROM @DriveLinesTable)))

RETURN
END


	



