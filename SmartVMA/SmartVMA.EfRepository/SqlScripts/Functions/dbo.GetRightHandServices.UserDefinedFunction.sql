USE [SmartVmaDev]
GO
/****** Object:  UserDefinedFunction [dbo].[GetRightHandServices]    Script Date: 7.9.2016 г. 18:48:36 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Boris Kamenov
-- Create date: 02.09.2016
-- Description:	Get the services displayed on the right hand side of menu presentation
-- =============================================
ALTER FUNCTION [dbo].[GetRightHandServices](
@TenantId int,
@CarId int = null, 
@DealerVehicleId int = null,
@Mileage int = null,
@IsLOF bit = null,
@VehicleTransmission int = null,
@VehicleDriveLine int = null,
@MeasurementUnitId int,
@LaborRate FLOAT = NULL,
@LanguageId INT)
RETURNS TABLE
AS
RETURN
	--GET Additional services for the dealer
	WITH 
	AdditionalServices AS(
	SELECT
		ServiceId
		,OpCode
		,FullDescription
		---FOR THE VIEW---
		,0 AS CarId
		,0 AS Mileage
		,CONVERT(BIT, 0) AS CanBeStriked
		,CONVERT(BIT, 0) AS IsStrikeOut
		,CONVERT(BIT,0) AS IsDeclined
		,2 AS ServiceType
		,CONVERT(BIT,0) AS IsPreviouslyServed
		,'' AS VideoCode
		----------------------------
		,IsFluid
		,IsEngineOil
		,IsLOF
		,BgProtectionPlanId
		,MenuLevel
		,LaborHour
		,LabourRate
		,PartsPrice
		,LaborPrice
		,Price
		,MaxMileageBeforeFirstService
		,ServiceInterval
		FROM [dbo].[GetFilteredAdditionalServices](@TenantId, @IsLOF, @LaborRate, @LanguageId, @CarId, NULL, @MeasurementUnitId)
	),
	----GET Previously Declined Services for this vehicle and relevant to the dealer
	PreviouslyDeclined AS(
	SELECT
		ServiceId
		,OpCode
		,FullDescription
		---FOR THE VIEW---
		,0 AS CarId
		,0 AS Mileage
		,CONVERT(BIT, 0) AS CanBeStriked
		,CONVERT(BIT,0) AS IsStrikeOut
		,IsDeclined
		,5 AS ServiceType
		,CONVERT(BIT,0) AS IsPreviouslyServed
		,'' AS VideoCode
		,CONVERT(BIT,0) AS IsFluid
		,CONVERT(BIT,0) AS IsEngineOil
		,CONVERT(BIT,0) AS IsLOF
		,NULL AS BgProtectionPlanId
		,1 AS MenuLevel
		----------------------------
		,LaborHour
		,LabourRate
		,PartsPrice
		,LaborPrice
		,Price
		,0 as MaxMileageBeforeFirstService
		,0 as ServiceInterval
		 FROM [dbo].[GetFilteredAppointmentServices](@TenantId, @DealerVehicleId, 1, null, null))

	---- GET Overriden basic services
	--,OverridenBasicServices AS(
	--SELECT
	--ServiceId
	--,OpCode
	--,FullDescription
	--,CarId
	--,Mileage
	--,IsFluid
	--,IsEngineOil
	--,IsLOF
	-----FOR THE VIEW-----------
	--,CONVERT(BIT,0) AS IsStrikeOut
	--,CONVERT(BIT,0) AS IsDeclined
	--,NULL AS BgProtectionPlanId
	--,convert(int,1) as MenuLevel -- in which level this item should be shown
	--,convert(int,1) as ServiceType -- table origin (this can be OemBasic 1, AdditionaService 2 ...)
	--,CONVERT(BIT,0) AS IsPreviouslyServed
	--,'' AS VideoCode
	----------------------------
	--,LaborHour
	--,LabourRate
	--,PartsPrice
	--,LaborPrice
	--,Price 
	--	FROM [dbo].[GetBasicServices](@TenantId,@CarId,@DealerVehicleId,NULL,@IsLOF,@VehicleTransmission,@VehicleDriveLine,@LanguageId,@MeasurementUnitId,@LaborRate, 1)
	--)

	--Add default properties for all
	SELECT 
	x.ServiceId,
	x.OpCode,
	x.FullDescription,
	x.CarId,
	x.Mileage,

	x.IsFluid,
	x.IsEngineOil,
	x.IsLOF,
	x.CanBeStriked,
	x.IsStrikeOut,
	x.IsDeclined,
	x.IsPreviouslyServed,
	x.BgProtectionPlanId,

	x.VideoCode,
	x.MenuLevel,
	x.ServiceType,
	2 AS ServiceMenuType,

	x.LaborHour,
	x.LabourRate,
	x.PartsPrice,
	x.LaborPrice,
	x.Price,
	x.MaxMileageBeforeFirstService,
	x.ServiceInterval
	FROM(
		SELECT * FROM PreviouslyDeclined
		UNION ALL
		SELECT * FROM AdditionalServices ads
		WHERE ads.ServiceId NOT IN (SELECT ServiceId FROM PreviouslyDeclined)

		--UNION ALL
		--SELECT * FROM OverridenBasicServices obs
	) AS x
--SELECT * FROM dbo.GetRightHandServices(1089,0,NULL,100)
/*
@TenantId int,
@CarId int = null, 
@DealerVehicleId int = null,
@Mileage int = null,
@IsLOF bit = null,
@VehicleTransmission int = null,
@VehicleDriveLine int = null,
@MeasurementUnitId int,
@LaborRate FLOAT = NULL,
@LanguageId INT
SELECT * FROM [GetRightHandServices](1090,123,NULL,15000,0,NULL,NULL,1,100,1)

*/