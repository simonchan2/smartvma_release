USE [SmartVmaDev]
GO
/****** Object:  UserDefinedFunction [dbo].[GetBasicServicesByCar]    Script Date: 12/8/2016 4:48:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--SELECT * FROM GetBasicServicesByCar(3)
ALTER FUNCTION [dbo].[GetBasicServicesByCar](@CarId BIGINT)
RETURNS TABLE
AS
RETURN
	SELECT OemBasicServiceId 
	FROM amam.OemBasicServices obs
	WHERE obs.CarId = @CarId AND obs.IsDeleted = 0 AND obs.IsIncomplete = 0
