USE [SmartVmaDev]
GO
/****** Object:  UserDefinedFunction [dbo].[GetLeftHandServices]    Script Date: 03.10.2016 10:45:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Boris Kamenov	
-- Create date: 09.02.2016
-- Description:	Get the services displayed on the left hand side of menu presentation
-- =============================================
ALTER FUNCTION [dbo].[GetLeftHandServices](
	@TenantId int,
	@CarId int = null, 
	@DealerVehicleId int = null,
	@Mileage int = null,
	@IsLOF bit = null,
	@VehicleTransmission int = null,
	@VehicleDriveLine int = null,
	@AppointmentPresentationId BIGINT = NULL,
	@LanguageId int,
	@MeasurementUnitId int,
	@LaborRate Float,
	@AdHocServiceTypeId int
)
RETURNS TABLE
AS
RETURN
	-- GET Overriden basic services
	WITH OverridenBasicServices AS(
	SELECT
	ServiceId
	,OpCode
	,FullDescription
	,CarId
	,Mileage
	,IsFluid
	,IsEngineOil
	,IsLOF
	---FOR THE VIEW-----------
	,CONVERT(BIT, 1) AS CanBeStriked -- previous was 0, by request chagned to 1 
	,CONVERT(BIT,0) AS IsStrikeOut
	,CONVERT(BIT,0) AS IsDeclined
	,CONVERT(BIT,0) AS IsPreviouslyServed
	,null as BgProtectionPlanId
	,convert(int,1) as MenuLevel -- in which level this item should be shown
	,convert(int,1) as ServiceType -- table origin (this can be OemBasic 1, AdditionaService 2 ...)
	--------------------------
	,VideoCode
	,LaborHour
	,LabourRate
	,PartsPrice
	,LaborPrice
	,Price 
	,0 as MaxMileageBeforeFirstService
	,0 as ServiceInterval
		FROM [dbo].[GetBasicServices](@TenantId,@CarId,@DealerVehicleId,@Mileage,@IsLOF,@VehicleTransmission,@VehicleDriveLine,@LanguageId,@MeasurementUnitId,@LaborRate, 1)
	)
	--GET Additional services for the dealer
	,AdditionalServices AS(
	SELECT
	ServiceId
	,OpCode
	,FullDescription
	,0 as CarId
	,0 as Mileage
	,IsFluid
	,IsEngineOil
	,IsLOF
	,CONVERT(BIT, 1) AS CanBeStriked
	,CONVERT(BIT,0) AS IsStrikeOut
	,CONVERT(BIT,0) AS IsDeclined
	,CONVERT(BIT,0) AS IsPreviouslyServed
	,BgProtectionPlanId
	,MenuLevel
	,2 AS ServiceType
	,VideoCode
	,LaborHour
	,LabourRate
	,PartsPrice
	,LaborPrice
	,Price
	,MaxMileageBeforeFirstService
	,ServiceInterval
	FROM [dbo].[GetFilteredAdditionalServices](@TenantId, @IsLOF, @LaborRate, @LanguageId, @CarId, @Mileage, @MeasurementUnitId)
	)
	--Add default properties for all
	SELECT 
	x.ServiceId,
	x.OpCode,
	x.FullDescription,
	x.CarId,
	x.Mileage,

	x.IsFluid,
	x.IsEngineOil,
	x.IsLOF,
	x.CanBeStriked,
	x.IsStrikeOut,
	x.IsDeclined,
	x.IsPreviouslyServed,
	x.BgProtectionPlanId,

	x.VideoCode,
	x.MenuLevel,
	x.ServiceType,
	CONVERT(INT,1) AS ServiceMenuType,

	x.LaborHour,
	x.LabourRate,
	x.PartsPrice,
	x.LaborPrice,
	x.Price,
	x.MaxMileageBeforeFirstService,
	x.ServiceInterval
	FROM(
		SELECT * FROM OverridenBasicServices
		UNION ALL
		SELECT * FROM AdditionalServices
		WHERE ((SELECT COUNT(*) FROM OverridenBasicServices) > 0)-- List additional services only when there are oem basic services for this vehicle and mileage
	) AS x
	 
	


/*
	@TenantId int,
	@CarId int = null, 
	@DealerVehicleId int = null,
	@Mileage int = null,
	@IsLOF bit = null,
	@VehicleTransmission int = null,
	@VehicleDriveLine int = null,
	@AppointmentPresentationId BIGINT = NULL,
	@LanguageId int,
	@MeasurementUnitId int,
	@LaborRate Float,
	@AdHocServiceTypeId int

	SELECT * FROM dbo.GetLeftHandServices(1090,7993,NULL,24000,0, 1, 1, null, 1, 0,100, 4)
	SELECT * FROM [GetRightHandServices](1090,7993,NULL,24000,0,1,1,1,100,1)
	exec dbo.GetAllServices_NEW @TenantId=1090,@CarId=7993,@DealerVehicleId=NULL,@Mileage=24000,@IsLof=0,@VehicleTransmission=1,@VehicleDriveLine=1,@DealerCustomerId=NULL,@AppointmentPresentationId=default,@ServiceIds=N''
go
*/


