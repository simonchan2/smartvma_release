USE [SmartVmaDev]
GO
/****** Object:  UserDefinedFunction [dbo].[SplitDelimiterString]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[SplitDelimiterString]
(
   @List       NVARCHAR(MAX),
   @Delimiter  NVARCHAR(255)
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN 
(  
    SELECT Item = y.i.value('(./text())[1]', 'nvarchar(4000)')
    FROM 
    ( 
    SELECT x = CONVERT(XML, '<i>' 
        + REPLACE(@List, @Delimiter, '</i><i>') 
        + '</i>').query('.')
    ) AS a CROSS APPLY x.nodes('i') AS y(i)
);
--RETURNS @ItemTable TABLE (Item VARCHAR(MAX))

--AS
--BEGIN
--    DECLARE @StartingPosition INT;
--    DECLARE @ItemInString VARCHAR(MAX);

--    SELECT @StartingPosition = 1;
--    --Return if string is null or empty
--    IF LEN(@StringWithDelimiter) = 0 OR @StringWithDelimiter IS NULL RETURN; 
    
--    WHILE @StartingPosition > 0
--    BEGIN
--        --Get starting index of delimiter .. If string
--        --doesn't contain any delimiter than it will returl 0 
--        SET @StartingPosition = CHARINDEX(@Delimiter,@StringWithDelimiter); 
        
--        --Get item from string        
--        IF @StartingPosition > 0                
--            SET @ItemInString = SUBSTRING(@StringWithDelimiter,0,@StartingPosition)
--        ELSE
--            SET @ItemInString = @StringWithDelimiter;
--        --If item isn't empty than add to return table    
--        IF( LEN(@ItemInString) > 0)
--            INSERT INTO @ItemTable(Item) VALUES (@ItemInString);            
        
--        --Remove inserted item from string
--        SET @StringWithDelimiter = SUBSTRING(@StringWithDelimiter,@StartingPosition + 
--                     LEN(@Delimiter),LEN(@StringWithDelimiter) - @StartingPosition)
        
--        --Break loop if string is empty
--        IF LEN(@StringWithDelimiter) = 0 BREAK;
--    END
     
--    RETURN
--END



GO
