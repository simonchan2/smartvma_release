USE [SmartVmaDev]
GO
/****** Object:  UserDefinedFunction [dbo].[udf_GetVehicleTransmission]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Author:		 Simon Chan
Create date: 2016-05-30
Description: Return the vehicle transmission
				Result:
				AT: Automatic Transmission 
				MT: Manual Transmission

Usage:
	SELECT dbo.udf_GetVehicleTransmission(NULL);
	SELECT dbo.udf_GetVehicleTransmission('3-SPEED AUTOMATIC');
	SELECT dbo.udf_GetVehicleTransmission('4-SPEED MANUAL');
	SELECT dbo.udf_GetVehicleTransmission('CVT');
	SELECT dbo.udf_GetVehicleTransmission('CONTINUOUSLY VARIABLE TRANSMISSION');
	SELECT dbo.udf_GetVehicleTransmission('CONTINUOUSLY VARIABLE TRANSMISSION | 5-SPEED MANUAL');
	SELECT dbo.udf_GetVehicleTransmission('CONTINUOUSLY VARIABLE TRANSMISSION | 6-SPEED AUTOMATIC');
*/

CREATE FUNCTION [dbo].[udf_GetVehicleTransmission] 
(
	@VehicleTransmission				nvarchar(120)
)
RETURNS nvarchar(5)
WITH SCHEMABINDING 
AS
BEGIN
    RETURN 
	CASE WHEN CHARINDEX('AUTOMATIC', @VehicleTransmission) > 0 AND CHARINDEX('MANUA', @VehicleTransmission) = 0 AND CHARINDEX('CVT', @VehicleTransmission) = 0 AND CHARINDEX('CONTINUOUSLY VARIABLE TRANSMISSION', @VehicleTransmission) = 0 THEN 'A/T' 
		WHEN CHARINDEX('AUTOMATIC', @VehicleTransmission) = 0 AND CHARINDEX('MANUA', @VehicleTransmission) > 0 AND CHARINDEX('CVT', @VehicleTransmission) = 0 AND CHARINDEX('CONTINUOUSLY VARIABLE TRANSMISSION', @VehicleTransmission) = 0 THEN 'M/T' 
		WHEN (CHARINDEX('CVT', @VehicleTransmission) > 0 OR CHARINDEX('CONTINUOUSLY VARIABLE TRANSMISSION', @VehicleTransmission) > 0) AND CHARINDEX('MANUA', @VehicleTransmission) = 0 AND  CHARINDEX('AUTOMATIC', @VehicleTransmission) = 0 THEN 'CVT' 
		ELSE NULL END;
END





GO
