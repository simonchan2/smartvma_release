USE [SmartVmaDev]
GO
/****** Object:  UserDefinedFunction [dbo].[GetOverridenParts]    Script Date: 12/12/2016 7:54:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[GetOverridenParts](@TenantId BIGINT, @PartOverrides PartOverrides READONLY)
RETURNS TABLE
AS
RETURN
	WITH
	EngineOilOverrides AS (
		SELECT 
		eop.UnitPrice,
		ROUND(osp.Quantity,1) as Quantity,
		osp.OemBasicServiceId,
		op.OemPartId,
		eop.UnitPrice * ROUND(osp.Quantity,1) AS PartPrice,
		eop.DealerId
		FROM amam.EngineOilProducts eop
		INNER JOIN amam.EngineOilOrules orr ON orr.EngineOilProductId =  eop.EngineOilProductId
		INNER JOIN amam.OemParts op ON op.EngineOilType = orr.FluidType AND op.FluidViscosity = orr.FluidViscosity
		INNER JOIN amam.OemServiceParts osp ON osp.OemPartId = op.OemPartId AND osp.IsDeleted = 0
		WHERE eop.DealerId = @TenantId
	)
	SELECT 
	osp.OemBasicServiceId,
	ISNULL(po.PartNoOverride, opn.OemPartNo) AS PartNo,
	ROUND(osp.Quantity, 1) AS PartQuantity, 
	ROUND(ISNULL(Parts.PartsPriceOverride,Parts.PartsPrice), 2) AS PartPrice, 
	CASE 
	WHEN oc.IsFluid = 1 AND oc.IsEngineOil = 1 THEN 'Engine Oil' 
	WHEN oc.IsFluid = 1 AND oc.IsEngineOil = 0  THEN 'Fluid' 
	WHEN oc.IsFluid = 0 THEN 'Part' 
	ELSE NULL END AS PartType
	FROM @PartOverrides po
	INNER JOIN amam.OemServiceParts osp ON po.OemBasicServiceId = osp.OemBasicServiceId AND osp.IsDeleted = 0
	INNER JOIN amam.OemParts op ON osp.OemPartId = op.OemPartId
	LEFT JOIN amam.OemPartNumbers opn ON opn.OemPartId = op.OemPartId AND CountryId = 0
	INNER JOIN amam.OemComponents oc ON oc.OemComponentId = op.OemComponentId
	LEFT JOIN EngineOilOverrides eoo ON eoo.OemPartId = op.OemPartId
	CROSS APPLY(
	SELECT 
		CASE
			WHEN eoo.PartPrice IS NOT NULL
			THEN eoo.PartPrice
			WHEN po.PartPricePercentage IS NOT NULL
			THEN CASE 
					WHEN ROUND((CAST(po.PartPricePercentage as decimal(18,2))/100.00)*(ISNULL(op.UnitPrice * ROUND(osp.Quantity, 1), 0)),2) >= 0
					THEN ROUND((CAST(po.PartPricePercentage as decimal(18,2))/100.00)*(ISNULL(op.UnitPrice * ROUND(osp.Quantity, 1), 0)),2)
					ELSE 0
					END
			WHEN po.PartPriceAmount IS NOT NULL
			THEN CASE 
					WHEN ISNULL(po.PartPriceAmount * ROUND(osp.Quantity, 1), 0) >= 0
					THEN ISNULL(po.PartPriceAmount * ROUND(osp.Quantity, 1), 0)
					ELSE 0
					END
			ELSE NULL
		END AS PartsPriceOverride,
		ISNULL(op.UnitPrice * ROUND(osp.Quantity, 1), 0) AS PartsPrice
	) Parts
	GROUP BY 
	osp.OemBasicServiceId,
	ISNULL(po.PartNoOverride, opn.OemPartNo),
	ROUND(osp.Quantity, 1), 
	ROUND(ISNULL(Parts.PartsPriceOverride,Parts.PartsPrice), 2),
	oc.IsFluid, oc.IsEngineOil

