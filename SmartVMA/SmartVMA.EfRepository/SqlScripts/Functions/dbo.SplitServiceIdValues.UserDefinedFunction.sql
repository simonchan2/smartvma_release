USE [SmartVmaDev]
GO
/****** Object:  UserDefinedFunction [dbo].[SplitServiceIdValues]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[SplitServiceIdValues]
(
	@IDs nvarchar(max)
)
RETURNS 
@SplitValues TABLE 
(
	Id int,
	ServiceOriginTableId int
)
AS
BEGIN
	IF @IDS IS NULL OR @IDs =''
		RETURN

	DECLARE @xml xml
	SET @xml = N'<root><r><id>' + replace( replace(@IDs,'_','</id><serviceid>' ),',','</serviceid></r><r><id>') + '</serviceid></r></root>';

	INSERT INTO @SplitValues(Id,ServiceOriginTableId)
	SELECT r.query('id').value('.','int'),r.query('serviceid').value('.','int')
	FROM @xml.nodes('//root/r') as records(r)

	RETURN
END


GO
