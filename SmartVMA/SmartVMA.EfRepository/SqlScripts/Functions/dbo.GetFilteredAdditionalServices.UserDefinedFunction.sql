USE [SmartVmaDev]
GO
/****** Object:  UserDefinedFunction [dbo].[GetFilteredAdditionalServices]    Script Date: 7.9.2016 г. 18:48:36 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
SELECT * FROM [dbo].[GetFilteredAppointmentServices](1089,NULL,1)

*/

ALTER FUNCTION [dbo].[GetFilteredAdditionalServices](
@TenantId INT,
@IsLOF BIT,
@LaborRate FLOAT,
@LanguageId INT,
@CarId INT = NULL,
@Mileage INT = NULL,
@MeasurementUnitId INT = NULL)
RETURNS @AdditionalServices TABLE 
(
    ServiceId BIGINT NULL, 
    OpCode nvarchar(50) NULL, 
    FullDescription nvarchar(255) NULL, 
    IsFluid BIT NULL, 
    IsEngineOil BIT NULL,
	IsLOF BIT NULL,
    IsStrikeOut BIT NULL,
    BgProtectionPlanId BIGINT NULL,
    VideoCode NVARCHAR(1000) NULL,
    MenuLevel INT NULL,
    LaborHour FLOAT NULL,
    LabourRate MONEY NULL,
    PartsPrice MONEY NULL,
    LaborPrice MONEY NULL,
    Price MONEY NULL,
    MaxMileageBeforeFirstService INT NULL,
    ServiceInterval INT NULL,
    BgProtectionPlanUrl NVARCHAR(1000) NULL
)
AS
BEGIN

	;WITH ServicePartsInfo AS(
	SELECT
		ads.AdditionalServiceId AS ServiceId
		,ads.OpCode
		,ads.OpDescription AS FullDescription
		,CAST(MAX(CAST(ISNULL(asp.IsFluid, 0) as int)) AS BIT) AS IsFluid
		,CAST(MAX(CAST(ISNULL(asp.IsEngineOil, 0) as int)) AS BIT) AS IsEngineOil
		,ads.IsLofService AS IsLOF
		,ads.CanBeStriked AS IsStrikeOut
		,ads.BgProtectionPlanId AS BgProtectionPlanId
		,vid.VideoUrl AS VideoCode
		,adsc.MenuLevel AS MenuLevel
		,ads.LaborHour AS LaborHour
		,ISNULL(ads.LaborRate, @LaborRate) AS LabourRate
		,SUM(ISNULL(asp.Quantity, 0) * ISNULL(asp.UnitPrice, 0)) AS PartsPrice
		,(ISNULL(ads.LaborRate, @LaborRate) * LaborHour) AS LaborPrice
		,((ISNULL(ads.LaborRate, @LaborRate) * LaborHour) + SUM(ISNULL(asp.Quantity, 0) * ISNULL(asp.UnitPrice, 0))) AS Price
		,ads.IntervalRepeat as ServiceInterval
	FROM dealer.AdditionalServices AS ads
	INNER JOIN dbo.AdditionalServicesCars adsc ON adsc.AdditionalServiceId = ads.AdditionalServiceId
	LEFT JOIN dealer.AdditionalServiceParts AS asp ON asp.AdditionalServiceId = ads.AdditionalServiceId
	LEFT OUTER JOIN bg.BgVideoClips AS bgvid ON bgvid.ServiceId = ads.AdditionalServiceId
	LEFT OUTER JOIN bg.bgvideoclipeditions AS vid ON vid.BgVideoClipId = bgvid.BgVideoClipId and vid.LanguageId = @LanguageId
	where 
		ads.DealerId = @TenantId
		AND (@CarId = adsc.CarId)
		AND (@IsLOF IS NULL OR ads.IsLofService = @IsLOF)
		AND dbo.IsServiceApplicable(@TenantId,ads.AdditionalServiceId,@CarId,@Mileage,@MeasurementUnitId, 1000000) = CAST(1 AS BIT)
	group by
		ads.AdditionalServiceId
		,ads.OpCode
		,ads.OpDescription
		,ads.IsLofService
		,vid.VideoUrl
		,adsc.MenuLevel
		,ads.LaborHour
		,ads.BgProtectionPlanId
		,ads.CanBeStriked
		,ads.IntervalRepeat
		,ads.LaborRate
	),
	BgInfo AS(
		SELECT
		spi.ServiceId,
		spi.BgProtectionPlanId, 
		MAX(pplm.MaxMileageBeforeFirstService) as MaxMileageBeforeFirstService, 
		ppd.Url AS BgProtectionPlanUrl
		FROM ServicePartsInfo spi
		LEFT OUTER JOIN bg.BgProtectionPlans pp on spi.BgProtectionPlanId = pp.BgProtectionPlanId
		LEFT OUTER JOIN bg.BgProtectionPlanLevels ppl on pp.BgProtectionPlanId = ppl.BgProtectionPlanId
		LEFT OUTER JOIN bg.BgProtectionPlanLevelMus pplm on ppl.BgProtectionPlanLevelId = pplm.BgProtectionPlanLevelId and pplm.MeasurementUnitId = @MeasurementUnitId
		LEFT OUTER JOIN bg.BgProtectionPlanDocs ppd on pp.BgProtectionPlanId = ppd.BgProtectionPlanId
		GROUP BY spi.ServiceId,spi.BgProtectionPlanId, ppd.Url
	)
	INSERT INTO @AdditionalServices
	SELECT spi.ServiceId, OpCode, FullDescription, IsFluid, IsEngineOil, IsLOF, IsStrikeOut, spi.BgProtectionPlanId, VideoCode, MenuLevel, LaborHour, LabourRate, PartsPrice, LaborPrice, Price,MaxMileageBeforeFirstService, ServiceInterval, BgProtectionPlanUrl
	FROM ServicePartsInfo spi
	INNER JOIN BgInfo bi ON bi.ServiceId = spi.ServiceId

	RETURN;
END
	
