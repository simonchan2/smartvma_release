USE [SmartVmaDev]
GO
/****** Object:  UserDefinedFunction [dbo].[GetBasicServices]    Script Date: 12/12/2016 6:53:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Boris Kamenov
-- Create date: 02.09.2016
-- Description:	Get the services displayed on the right hand side of menu presentation
-- =============================================
ALTER FUNCTION [dbo].[GetBasicServices](
@TenantId int,
@CarId int = null, 
@DealerVehicleId int = null,
@Mileage int = null,
@IsLOF bit = null,
@VehicleTransmission int = null,
@VehicleDriveLine int = null,
@LanguageId int = null,
@MeasurementUnitId int = null,
@LaborRate FLOAT,
@ApplyOverrides BIT = 1
)
RETURNS TABLE
AS
RETURN
WITH
BasicServices AS (
select
			ServiceId
			,OpCode
			,FullDescription
			,CarId
			,Mileage
			,IsFluid
			,IsEngineOil
			,IsLOF
			--,IsStrikeOut
			--,IsDeclined
			--,IsPreviouslyServed
			--,BgProtectionPlanId
			,VideoCode
			--,MenuLevel
			--,ServiceType
			--,ServiceMenuType
			,LaborHour
			,LabourRate
			,PartsPrice
			,(LabourRate * LaborHour) as LaborPrice
			,((LabourRate * LaborHour) + PartsPrice) as Price
		from (
			select
				convert(bigint,obs.OemBasicServiceId) as ServiceId,
				'' as OpCode,
				obs.OpAction + ' ' + oc.[Description] as [FullDescription],
				obs.CarId,
				omm.Mileage,
				oc.IsFluid,
				oc.IsEngineOil,
				oc.IsLOF,
				--convert(bit,0) as IsStrikeOut,
				--convert(bit,0) as IsDeclined,
				--convert(bit,0) as IsPreviouslyServed,
				--IsDeclined and IsPreviouslyServed fields are handled in GetAllServices procedure to be set to 1 we need to have Customer!
				--Changed by Dimitar Stefanov
				--convert(bit,case when (select count(*) from dealer.AppointmentServices where OrigServiceId = obs.OemBasicServiceId and ServiceTypeId = 1 and IsStriked = 1) > 0 then 1 else 0 end) as IsDeclined,
				--convert(bit,case when (select count(*) from dealer.AppointmentServices where OrigServiceId = obs.OemBasicServiceId and ServiceTypeId = 1 and IsStriked = 0) > 0 then 1 else 0 end) as IsPreviouslyServed,
				--null as BgProtectionPlanId,
				vid.VideoUrl as VideoCode,
				--convert(int,1) as MenuLevel, -- in which level this item should be shown
				--convert(int,1) as ServiceType, -- table origin (this can be OemBasic 1, AdditionaService 2 ...)
				--convert(int,1) as ServiceMenuType, -- this can be Maintenance or Additional service

				convert(float,obs.LaborHour + COALESCE((select sum(op.laborhour) from amam.oemserviceparts osp inner join amam.oemparts as op on osp.OemPartId = op.OemPartId where osp.IsDeleted = 0 AND OemBasicServiceId = obs.OemBasicServiceId),0)) as LaborHour,
				@LaborRate as LabourRate,
				convert(float,coalesce((select sum(op.unitprice*osp.quantity) from amam.oemserviceparts osp inner join amam.oemparts as op on osp.OemPartId = op.OemPartId where osp.IsDeleted = 0 AND OemBasicServiceId = obs.OemBasicServiceId),0)) as PartsPrice
				--convert(float,coalesce((select sum(op.unitprice*osp.quantity) from amam.oemserviceparts osp inner join amam.oemparts as op on osp.OemPartId = op.OemPartId where osp.IsDeleted = 0 AND OemBasicServiceId = obs.OemBasicServiceId and ((op.OemPartTypeId = case @IsLOF when 1 then 0 else op.OemPartTypeId end) or (op.OemPartTypeId = case @IsLOF when 1 then 2 else op.OemPartTypeId end))),0)) as PartsPrice,
				,convert(float,0) as LaborPrice
				from amam.oembasicservices obs
					inner join amam.oemcomponents as oc on oc.OemComponentId = obs.OemComponentId
					inner join amam.OemMenuLists as oml on oml.CarId = obs.CarId
					inner join amam.OemMenus as om on om.OemMenuListId = oml.OemMenuListId
					inner join amam.OemMenuMileages as omm on omm.OemMenuId = om.OemMenuId
					inner join amam.OemServices as os on os.OemMenuId = om.OemMenuId and os.OemBasicServiceId = obs.OemBasicServiceId
					inner join dbo.vehicles as v on v.carid = obs.carid
					left join dealer.dealervehicles as dv on dv.vehicleId = v.vehicleId
					left outer join bg.BgVideoClips as bgvid on bgvid.ServiceId = obs.OemBasicServiceId 
						and bgvid.ServiceTypeId = 1 -- OemBasicService
					left outer join bg.bgvideoclipeditions as vid on vid.BgVideoClipId = bgvid.BgVideoClipId 
						and vid.LanguageId = @LanguageId -- from company settings
			where 
			obs.IsIncomplete = 0  AND obs.IsDeleted = 0 and 
				(@CarId is null or obs.CarId = @CarId)
				and (@DealerVehicleId is null or dv.DealerVehicleId = @DealerVehicleId)
				and (@IsLOF = 0 or @IsLOF is null or oc.IsLOF=@IsLOF)
				and (@Mileage is null or omm.mileage = @Mileage)
				and omm.MeasurementUnitId = @MeasurementUnitId
				and (@VehicleTransmission is null 
					or @VehicleTransmission = 5 --(N/A Unknown value)
					or oc.VehicleTransmissionShort in (select VehicleTransmission from amam.VehicleTransmissions where VehicleTransmissionId in (@VehicleTransmission, 5)))
				and (@VehicleDriveLine is null 
					or @VehicleDriveLine = 6 --(N/A Unknown value)
					or oc.VehicleDrivelineShort in (select VehicleDriveline from amam.VehicleDrivelines where VehicleDrivelineId in (@VehicleDriveLine, 6)))
				) as q
			group by
				ServiceId,OpCode,FullDescription,CarId,Mileage,IsFluid,IsEngineOil,IsLOF,
				--IsStrikeOut,IsDeclined,IsPreviouslyServed,BgProtectionPlanId,
				VideoCode,
				--MenuLevel,ServiceType,ServiceMenuType,
				LaborHour,LabourRate,PartsPrice
)
,OverridenBasicServices AS(
SELECT 
	ServiceId
	,ISNULL(overrides.OpCodeOverride, s.OpCode) AS OpCode
	,FullDescription
	,CarId
	,Mileage
	,IsFluid
	,IsEngineOil
	,IsLOF
	--,IsStrikeOut
	--,IsDeclined
	--,IsPreviouslyServed
	--,BgProtectionPlanId
	,VideoCode
	--,MenuLevel
	--,ServiceType
	--,ServiceMenuType
	,ISNULL(overrides.LaborHourOverride, LaborHour) AS LaborHour
	,LabourRate
	,ISNULL(overrides.PartsPriceOverride, PartsPrice) AS PartsPrice
	,ISNULL(overrides.LaborPriceOverride, LaborPrice) AS LaborPrice
	,ISNULL(overrides.PartsPriceOverride, PartsPrice) + ISNULL(overrides.LaborPriceOverride, LaborPrice) AS Price
	FROM BasicServices s
	LEFT JOIN [GetOverridenBasicServicesByCarId](@CarId, NULL, NULL, NULL, NULL, @TenantId, @LaborRate, ',') overrides ON overrides.OemBasicServiceId = s.ServiceId
)
SELECT *
FROM BasicServices
WHERE @ApplyOverrides = 0

UNION ALL

SELECT *
FROM OverridenBasicServices
WHERE @ApplyOverrides = 1
/*

SELECT * FROM [dbo].[GetBasicServices](1089,522,NULL,15000,0,NULL,NULL,1,0,100,1)


SELECT *
FROm dbo.CompanySettings
WHERE name='DefaultLaborRate' and CompanyId=1089

SELECT * FROM [dbo].[GetBasicServices](1089,7208,NULL,12000,0,NULL,NULL,1,0,100,1)

exec dbo.GetMaintenanceServices @TenantId=1089,@CarId=7929,@DealerVehicleId=NULL,@Mileage=120000,@IsLof=0,@VehicleTransmission=null,@VehicleDriveLine=null
*/