USE [SmartVmaDev]
GO

/****** Object:  View [dbo].[TenantOverrideRules]    Script Date: 12/8/2016 5:17:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





ALTER VIEW [dbo].[TenantOverrideRules] AS
SELECT 
 orbs.Id
,orbs.DealerId
,orbs.OemBasicServiceId
,orbs.LaborHourRuleId
,orbs.LaborPriceRuleId
,orbs.PartPriceRuleId
,orbs.OpCodeRuleId
,orbs.PartNoRuleId
,c.CarId
,oc.OemComponentId
,oc.[Description]
,obs.OpDescription
,obs.LaborHour
,lhorules.LaborHourPercentage
,lhorules.LaborHourAmount
,lporules.LaborPricePercentage
,lporules.LaborPriceAmount
,pporules.PartPricePercentage
,pporules.PartPriceAmount
,ocorules.OpCodeOverride
,pnorules.PartNoOverride
FROM dealer.OverrideRuleBasicServicesCross orbs
INNER JOIN amam.OemBasicServices obs ON obs.OemBasicServiceId = orbs.OemBasicServiceId AND obs.IsIncomplete = 0 AND obs.IsDeleted = 0
INNER JOIN amam.Cars c ON c.CarId = obs.CarId
INNER JOIN amam.OemComponents oc ON oc.OemComponentId = obs.OemComponentId
LEFT JOIN dealer.OverrideRules lhorules ON lhorules.Id = orbs.LaborHourRuleId
LEFT JOIN dealer.OverrideRules lporules ON lporules.Id = orbs.LaborPriceRuleId
LEFT JOIN dealer.OverrideRules pporules ON pporules.Id = orbs.PartPriceRuleId
LEFT JOIN dealer.OverrideRules ocorules ON ocorules.Id = orbs.OpCodeRuleId
LEFT JOIN dealer.OverrideRules pnorules ON pnorules.Id = orbs.PartNoRuleId




GO


