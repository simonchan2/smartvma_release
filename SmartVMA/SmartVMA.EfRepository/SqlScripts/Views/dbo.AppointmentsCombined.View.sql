USE [SmartVmaDev]
GO
/****** Object:  View [dbo].[AppointmentsCombined]    Script Date: 12.8.2016 г. 17:31:41 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[AppointmentsCombined] AS
	SELECT
		ISNULL(app.AppointmentId, -1) AS AppointmentId,
		app.AppointmentPresentationId,
		app.DealerCustomerId,
		app.DealerVehicleId,
		app.VehicleLicense,
		app.AppointmentTime,
		app.DealerId,
		app.PresentationStatus, 
		ap.AppointmentStatus, 
		app.AdvisorId,
		app.RoNumber,
		dv.VIN,
		dv.VehicleMake AS Make,
		dv.VehicleModel AS Model,
		dv.VehicleYear AS [Year],
		ISNULL(app.Mileage, 0) AS Mileage,
		dv.VehicleTransmission,
		dv.VehicleDriveline,
		dv.VehicleEngine,
		app.CustomerPhone,
		dv.StockNo,
		v.CarId,
		'' AS AdvisorCode
	FROM dealer.AppointmentPresentations app
		LEFT JOIN dealer.Appointments ap ON ap.AppointmentId = app.AppointmentId
		INNER JOIN dealer.DealerVehicles dv ON app.DealerVehicleId = dv.DealerVehicleId
		LEFT JOIN dbo.Vehicles v on dv.VehicleId = v.VehicleId
UNION ALL
	SELECT
		ap.AppointmentId,
		-1 AS AppointmentPresentationId,
		ap.DealerCustomerId,
		ap.DealerVehicleId,
		ap.VehicleLicense,
		ap.AppointmentTime,
		ap.DealerId as DealerId,
		'' AS PresentationStatus, 
		ap.AppointmentStatus, 
		ap.UserId as AdvisorId,
		ap.RoNumber,
		ap.VIN,
		ap.VehicleMake AS Make,
		ap.VehicleModel AS Model,
		ap.VehicleYear AS [Year],
		ISNULL(ap.Mileage, 0) AS Mileage,
		ap.VehicleTransmission,
		ap.VehicleDriveline,
		dv.VehicleEngine,
		ap.CustomerPhone,
		dv.StockNo,
		v.CarId,
		ap.AdvCode AS AdvisorCode
	FROM dealer.Appointments ap
		INNER JOIN dealer.DealerVehicles dv ON ap.DealerVehicleId = dv.DealerVehicleId
		LEFT JOIN dbo.Vehicles v on dv.VehicleId = v.VehicleId
	WHERE ap.AppointmentId NOT IN (SELECT AppointmentId FROM dealer.AppointmentPresentations WHERE AppointmentId IS NOT NULL)

GO