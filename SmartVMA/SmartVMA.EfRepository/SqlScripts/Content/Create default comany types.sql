﻿-- the id of this row MUST be: 1
insert into CompanyTypes (Name,TimeCreated,TimeUpdated) values ('Corp',GetDate(),GetDate());

-- the id of this row MUST be: 2
insert into CompanyTypes (Name,TimeCreated,TimeUpdated) values ('Distributor',GetDate(),GetDate());

-- the id of this row MUST be: 3
insert into CompanyTypes (Name,TimeCreated,TimeUpdated) values ('Dealer',GetDate(),GetDate());
