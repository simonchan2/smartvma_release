﻿ALTER TABLE document.PdfDocuments
ADD DealerId INT

UPDATE pd
SET pd.DealerId = ap.DealerId
FROM document.PdfDocuments AS pd
INNER JOIN dealer.AppointmentPresentations AS ap
ON pd.AppointmentPresentationId = ap.AppointmentPresentationId

ALTER TABLE [document].[PdfDocuments]  WITH CHECK ADD  CONSTRAINT [FK_PdfDocuments_Companies] FOREIGN KEY([DealerId])
REFERENCES [dbo].[Companies] ([CompanyId])
GO