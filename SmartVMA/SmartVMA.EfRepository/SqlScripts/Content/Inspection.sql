﻿
--view points
INSERT INTO vi.ViewPoints ([Name],[Url],[TimeCreated],[TimeUpdated],[Position])
VALUES ('Front','/Content/_images/Front.PNG',GETUTCDATE(),GETUTCDATE(),1);

INSERT INTO vi.ViewPoints ([Name],[Url],[TimeCreated],[TimeUpdated],[Position])
VALUES ('Left','/Content/_images/Left.PNG',GETUTCDATE(),GETUTCDATE(),2);

INSERT INTO vi.ViewPoints ([Name],[Url],[TimeCreated],[TimeUpdated],[Position])
VALUES ('Back','/Content/_images/Back.PNG',GETUTCDATE(),GETUTCDATE(),3);

INSERT INTO vi.ViewPoints ([Name],[Url],[TimeCreated],[TimeUpdated],[Position])
VALUES ('Right','/Content/_images/Right.PNG',GETUTCDATE(),GETUTCDATE(),4);

INSERT INTO vi.ViewPoints ([Name],[Url],[TimeCreated],[TimeUpdated],[Position])
VALUES ('Top','/Content/_images/Top.PNG',GETUTCDATE(),GETUTCDATE(),5);

--damage indication types
INSERT INTO vi.DamageIndicationTypes(Url,Name,Position,IsComment)
VALUES ('/Content/_images/Scratch.PNG','Scratch',1,0);

INSERT INTO vi.DamageIndicationTypes(Url,Name,Position,IsComment)
VALUES ('/Content/_images/Dent.PNG','Dent',2,0);

INSERT INTO vi.DamageIndicationTypes(Url,Name,Position,IsComment)
VALUES ('/Content/_images/Crack.PNG','Crack',3,0);

INSERT INTO vi.DamageIndicationTypes(Url,Name,Position,IsComment)
VALUES ('/Content/_images/Other.PNG','Other',4,1);