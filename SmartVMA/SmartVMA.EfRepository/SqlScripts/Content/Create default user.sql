﻿if not exists (select * from users)
begin
	insert into [dbo].[Users]
           ([UserStatusCodeId]
           ,[Login]
           ,[Email]
           ,[PasswordHash]
           ,[LastName]
           ,[FirstName]
           ,[MiddleName]
           ,[Configuration]
           ,[Comment]
           ,[TimeCreated]
           ,[TimeUpdated])
     values
           (0
           ,'admin'
           ,'admin@admin.com'
           ,'0x2D84D8D5915F6AEA39B63405FCB588B0' -- default password: smartvmaplus
           ,'Admin'
           ,'Admin'
           ,'Admin'
           ,null
           ,'comment'
           ,getDate()
           ,getDate())
end