 SET IDENTITY_INSERT dbo.Languages ON
 GO
 INSERT INTO languages (LanguageId,LanguageCode,Description,TimeCreated,TimeUpdated)
 VALUES(3,'es','Spanish',GETDATE(),GETDATE())
 
 SET IDENTITY_INSERT dbo.Languages OFF
 GO
 
 SELECT * FROM dbo.Languages
 UPDATE dbo.Languages SET LanguageCode = 'en', Description='English' WHERE LanguageId = 1;
 UPDATE dbo.Languages SET LanguageCode = 'fr', Description='French' WHERE LanguageId = 2;

 UPDATE bg.BgVideoClipEditions SET LanguageId = 3 WHERE LanguageId = 2;
 UPDATE bg.BgVideoClipEditions SET LanguageId = 2 WHERE LanguageId = 1;
 UPDATE bg.BgVideoClipEditions SET LanguageId = 1 WHERE LanguageId = 0;

 UPDATE bg.BgProtectionPlanDocs SET LanguageId = 3 WHERE LanguageId = 2;
 UPDATE bg.BgProtectionPlanDocs SET LanguageId = 2 WHERE LanguageId = 1;
 UPDATE bg.BgProtectionPlanDocs SET LanguageId = 1 WHERE LanguageId = 0;
  
 UPDATE dealer.DealerCustomers SET LanguageId = 3 WHERE LanguageId = 2;
 UPDATE dealer.DealerCustomers SET LanguageId = 2 WHERE LanguageId = 1;
 UPDATE dealer.DealerCustomers SET LanguageId = 1 WHERE LanguageId = 0;

 UPDATE dbo.Companies SET LanguageId = 3 WHERE LanguageId = 2;
 UPDATE dbo.Companies SET LanguageId = 2 WHERE LanguageId = 1;
 UPDATE dbo.Companies SET LanguageId = 1 WHERE LanguageId = 0;

 UPDATE dbo.CompanySettings SET value = '3' WHERE Value = '2' AND Name='Language';
 UPDATE dbo.CompanySettings SET value = '2' WHERE Value = '1' AND Name='Language';
 UPDATE dbo.CompanySettings SET value = '1' WHERE Value = '0' AND Name='Language';

 DELETE FROM dbo.Languages WHERE LanguageId = 0;