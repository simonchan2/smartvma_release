﻿-- RoleId of this row must be: 1
insert into roles ([Role],[Description],Configuraiton,Comment,IsSystemRole,TimeCreated,TimeUpdated) values ('SystemAdministrator','System Administrator','','',1,getdate(),getdate());

-- RoleId of this row must be: 2
insert into roles ([Role],[Description],Configuraiton,Comment,IsSystemRole,TimeCreated,TimeUpdated) values ('DealerAdmin','Dealer Admin','','',1,getdate(),getdate());

-- RoleId of this row must be: 3
insert into roles ([Role],[Description],Configuraiton,Comment,IsSystemRole,TimeCreated,TimeUpdated) values ('DistributorAdmin','Distributor Admin','','',1,getdate(),getdate());

-- RoleId of this row must be: 4
insert into roles ([Role],[Description],Configuraiton,Comment,IsSystemRole,TimeCreated,TimeUpdated) values ('BGExecutive','BG Executive','','',1,getdate(),getdate());

-- RoleId of this row must be: 5
insert into roles ([Role],[Description],Configuraiton,Comment,IsSystemRole,TimeCreated,TimeUpdated) values ('DistributorExecutive','Distributor Executive','','',1,getdate(),getdate());

-- RoleId of this row must be: 6
insert into roles ([Role],[Description],Configuraiton,Comment,IsSystemRole,TimeCreated,TimeUpdated) values ('Manager','Manager','','',1,getdate(),getdate());

-- RoleId of this row must be: 7
insert into roles ([Role],[Description],Configuraiton,Comment,IsSystemRole,TimeCreated,TimeUpdated) values ('MenuAdvisor','Menu Advisor','','',1,getdate(),getdate());