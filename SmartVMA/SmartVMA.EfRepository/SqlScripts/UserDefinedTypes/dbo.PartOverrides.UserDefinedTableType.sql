USE [SmartVmaDev]
GO
/****** Object:  UserDefinedTableType [dbo].[PartOverrides]    Script Date: 12.8.2016 г. 17:31:39 ч. ******/
CREATE TYPE [dbo].[PartOverrides] AS TABLE(
	[OemBasicServiceId] [bigint] NULL,
	[PartPricePercentage] [int] NULL,
	[PartPriceAmount] [money] NULL,
	[PartNoOverride] [nvarchar](20) NULL
)
GO
