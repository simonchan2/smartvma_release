﻿using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Core.Contracts;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using SmartVMA.Core;

namespace SmartVMA.EfRepository
{
    internal abstract class BaseTenantRepository<T, K> : BaseRepository<T, K> where T : class, IBaseEntity<K>, IBaseTenantEntity<K>, new()
    {
        protected readonly IAppContext _appContext;

        protected BaseTenantRepository(Models dbContext, IAppContext appContext) : base(dbContext)
        {
            _appContext = appContext;
        }

        protected override IQueryable<T> GetDbSet(params string[] includeReferences)
        {
            var result = base.GetDbSet(includeReferences);
            var currentIdentity = _appContext.CurrentIdentity;
            if (currentIdentity.TenantId == StaticSettings.BaseCompanyId)
            {
                return result;
            }
            return result.Where(x => x.TenantId == currentIdentity.TenantId);
        }

        protected override List<SqlParameter> GetDefaultDBParameters()
        {
            var result = base.GetDefaultDBParameters();

            // return tenantID as default parameter
            var tenantIdParam = new SqlParameter("TenantId", _appContext.CurrentIdentity.TenantId)
            {
                SqlDbType = System.Data.SqlDbType.Int
            };
            result.Add(tenantIdParam);

            return result;
        }

        public override void Insert(T entity)
        {
            entity.TenantId = _appContext.CurrentIdentity.TenantId ?? 0;
            base.Insert(entity);
        }
    }
}