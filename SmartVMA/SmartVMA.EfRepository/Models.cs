using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Interception;

using SmartVMA.Core.Entities;
using SmartVMA.Infrastructure.Ioc;

namespace SmartVMA.EfRepository
{
    [IocBindable(RegisterAsSelf = true, ScopeType = InstanceScopeType.PerRequest)]
    internal partial class Models : DbContext
    {
        public Models()
            : base("name=Models")
        {
            // For debug only
            //DbInterception.Add(new EFCommandInterceptor());   // To intercept Entity Framework when it executes database commands
        }

        public virtual DbSet<Car> Cars { get; set; }
        public virtual DbSet<EngineOilORule> EngineOilORules { get; set; }
        public virtual DbSet<EngineOilProduct> EngineOilProducts { get; set; }
        public virtual DbSet<OemBasicService> OemBasicServices { get; set; }
        public virtual DbSet<OemComponentRule> OemComponentRules { get; set; }
        public virtual DbSet<OemComponentRuleType> OemComponentRuleTypes { get; set; }
        public virtual DbSet<OemComponent> OemComponents { get; set; }
        public virtual DbSet<OemMenuList> OemMenuLists { get; set; }
        public virtual DbSet<OemMenuMileage> OemMenuMileages { get; set; }
        public virtual DbSet<OemMenu> OemMenus { get; set; }
        public virtual DbSet<OemPartNumber> OemPartNumbers { get; set; }
        public virtual DbSet<OemPart> OemParts { get; set; }
        public virtual DbSet<OemServiceException> OemServiceExceptions { get; set; }
        public virtual DbSet<OemServicePart> OemServiceParts { get; set; }
        public virtual DbSet<OemService> OemServices { get; set; }
        public virtual DbSet<OpAction> OpActions { get; set; }
        public virtual DbSet<VehicleEngine> VehicleEngines { get; set; }
        public virtual DbSet<VehicleMake> VehicleMakes { get; set; }
        public virtual DbSet<VehicleModel> VehicleModels { get; set; }
        public virtual DbSet<BgProdCategory> BgProdCategories { get; set; }
        public virtual DbSet<BgProdSubcategory> BgProdSubcategories { get; set; }
        public virtual DbSet<BgProduct> BgProducts { get; set; }
        public virtual DbSet<BgProductsProtectionPlan> BgProductsProtectionPlans { get; set; }
        public virtual DbSet<BgProtectionPlanDoc> BgProtectionPlanDocs { get; set; }
        public virtual DbSet<BgProtectionPlanLevelMu> BgProtectionPlanLevelMus { get; set; }
        public virtual DbSet<BgProtectionPlanLevel> BgProtectionPlanLevels { get; set; }
        public virtual DbSet<BgProtectionPlanMu> BgProtectionPlanMus { get; set; }
        public virtual DbSet<BgProtectionPlan> BgProtectionPlans { get; set; }
        public virtual DbSet<BgVideoClip> BgVideoClips { get; set; }
        public virtual DbSet<MenuGroup> MenuGroups { get; set; }
        public virtual DbSet<MenuName> MenuNames { get; set; }
        public virtual DbSet<MenuSchedule> MenuSchedules { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<CompanyCompanyGroup> CompanyCompanyGroups { get; set; }
        public virtual DbSet<CompanyGroup> CompanyGroups { get; set; }
        public virtual DbSet<CompanySetting> CompanySettings { get; set; }
        public virtual DbSet<CompanyStatusCode> CompanyStatusCodes { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<CountryState> CountryStates { get; set; }
        public virtual DbSet<DealerCustomerStatusCode> DealerCustomerStatusCodes { get; set; }
        public virtual DbSet<DealerVehicleStatusCode> DealerVehicleStatusCodes { get; set; }
        public virtual DbSet<DmsUserType> DmsUserTypes { get; set; }
        public virtual DbSet<Language> Languages { get; set; }
        public virtual DbSet<MeasurementUnit> MeasurementUnits { get; set; }
        public virtual DbSet<OverrideRuleBasicService> OverrideRuleBasicServices { get; set; }
        public virtual DbSet<OverrideRuleEngine> OverrideRuleEngines { get; set; }
        public virtual DbSet<OverrideRuleMake> OverrideRuleMakes { get; set; }
        public virtual DbSet<OverrideRuleModel> OverrideRuleModels { get; set; }
        public virtual DbSet<PasswordResetRequest> PasswordResetRequests { get; set; }
        public virtual DbSet<Permission> Permissions { get; set; }
        public virtual DbSet<RolePermission> RolePermissions { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<StateTimeZone> StateTimeZones { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<Core.Entities.TimeZone> TimeZones { get; set; }
        public virtual DbSet<UserCompanyRole> UserCompanyRoles { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserSetting> UserSettings { get; set; }
        public virtual DbSet<UserStatusCode> UserStatusCodes { get; set; }
        public virtual DbSet<Vehicle> Vehicles { get; set; }
        public virtual DbSet<VinDigit> VinDigits { get; set; }
        public virtual DbSet<VinWeight> VinWeights { get; set; }
        public virtual DbSet<AdditionalServicePart> AdditionalServiceParts { get; set; }
        public virtual DbSet<AdditionalService> AdditionalServices { get; set; }
        public virtual DbSet<AppointmentDetail> AppointmentDetails { get; set; }
        public virtual DbSet<AppointmentPresentation> AppointmentPresentations { get; set; }
        public virtual DbSet<Appointment> Appointments { get; set; }
        public virtual DbSet<AppointmentService> AppointmentServices { get; set; }
        public virtual DbSet<AppointmentServicePart> AppointmentServiceParts { get; set; }
        public virtual DbSet<DealerCustomer> DealerCustomers { get; set; }
        public virtual DbSet<DealerDmsUser> DealerDmsUsers { get; set; }
        public virtual DbSet<DealerOpCode> DealerOpCodes { get; set; }
        public virtual DbSet<DealerVehicle> DealerVehicles { get; set; }
        public virtual DbSet<Inspection> Inspections { get; set; }
        public virtual DbSet<InvoiceDetailPart> InvoiceDetailParts { get; set; }
        public virtual DbSet<InvoiceDetail> InvoiceDetails { get; set; }
        public virtual DbSet<Invoice> Invoices { get; set; }
        public virtual DbSet<OemLofMenuExpandedORule> OemLofMenuExpandedORules { get; set; }
        public virtual DbSet<OemLofMenuORule> OemLofMenuORules { get; set; }
        public virtual DbSet<OemMenuExpandedORule> OemMenuExpandedORules { get; set; }
        public virtual DbSet<OemMenuORule> OemMenuORules { get; set; }
        public virtual DbSet<OemServiceExpandedORule> OemServiceExpandedORules { get; set; }
        public virtual DbSet<OemServiceORule> OemServiceORules { get; set; }
        public virtual DbSet<OverrideRule> OverrideRules { get; set; }
        public virtual DbSet<PdfDocument> PdfDocuments { get; set; }
        public virtual DbSet<ILife> ILives { get; set; }
        public virtual DbSet<ILife_History> ILife_History { get; set; }
        public virtual DbSet<ArticleArticleCategory> ArticleArticleCategories { get; set; }
        public virtual DbSet<ArticleCategory> ArticleCategories { get; set; }
        public virtual DbSet<Article> Articles { get; set; }
        public virtual DbSet<Alert> Alerts { get; set; }
        public virtual DbSet<AlertsStatus> AlertsStatuses { get; set; }
        public virtual DbSet<AlertsUser> AlertsUsers { get; set; }
        public virtual DbSet<PrnConfigPrinter> PrnConfigPrinters { get; set; }
        public virtual DbSet<PrnConfiguration> PrnConfigurations { get; set; }
        public virtual DbSet<PrnJobType> PrnJobTypes { get; set; }
        public virtual DbSet<PrnPrinter> PrnPrinters { get; set; }
        public virtual DbSet<PrnSpool> PrnSpools { get; set; }
        public virtual DbSet<DamageIndication> DamageIndications { get; set; }
        public virtual DbSet<DamageIndicationType> DamageIndicationTypes { get; set; }
        public virtual DbSet<VehiclePhoto> VehiclePhotos { get; set; }
        public virtual DbSet<ViewPoint> ViewPoints { get; set; }
        public virtual DbSet<VinMaster> VinMasters { get; set; }
        public virtual DbSet<DmsEventLogEntry> DmsEventLogEntries { get; set; }
        public virtual DbSet<DmsTypes> DmsTypes { get; set; }
        public virtual DbSet<BgPpRule> BgPpRules { get; set; }
        public virtual DbSet<BgPpRulesBgProduct> BgPpRulesBgProducts { get; set; }
        public virtual DbSet<BgProductsCategory> BgProductsCategories { get; set; }
        public virtual DbSet<BgProtectionSubPlanLevel> BgProtectionSubPlanLevels { get; set; }
        public virtual DbSet<BgProtectionSubPlanMu> BgProtectionSubPlanMus { get; set; }
        public virtual DbSet<BgProtectionSubPlan> BgProtectionSubPlans { get; set; }
        public virtual DbSet<CompanyType> CompanyTypes { get; set; }
        public virtual DbSet<BgVideoClipEdition> BgVideoClipEditions { get; set; }
        public virtual DbSet<OverrideRuleDriveLine> OverrideRuleDriveLines { get; set; }
        public virtual DbSet<OverrideRuleTransmission> OverrideRuleTransmissions { get; set; }
        public virtual DbSet<OverrideRuleYear> OverrideRuleYears { get; set; }
        public virtual DbSet<AdditionalServiceVideoClip> AdditionalServiceVideoClips { get; set; }
        public virtual DbSet<DealerVehiclesPpRecord> DealerVehiclesPpRecords { get; set; }
        public virtual DbSet<OverrideRuleBasicServicesCross> OverrideRuleBasicServicesCrosses { get; set; }
        public virtual DbSet<CarVinShort> CarVinShorts { get; set; }
        public virtual DbSet<EngineOilType> EngineOilType { get; set; }
        public virtual DbSet<FluidViscosity> FluidViscosity { get; set; }
        public virtual DbSet<AdditionalServicesCar> AdditionalServicesCars { get; set; }
        public virtual DbSet<VinMasterDriveline> VinMasterDriveline { get; set; }
        public virtual DbSet<VinMasterTransmission> VinMasterTransmission { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Car>()
                .HasMany(e => e.OemBasicServices)
                .WithRequired(e => e.Car)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Car>()
                .HasMany(e => e.OemMenuLists)
                .WithRequired(e => e.Car)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Car>()
                .HasMany(e => e.OemParts)
                .WithRequired(e => e.Car)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Car>()
                .HasMany(e => e.Vehicles)
                .WithRequired(e => e.Car)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EngineOilProduct>()
                .Property(e => e.UnitPrice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<EngineOilProduct>()
                .HasMany(e => e.EngineOilORules)
                .WithRequired(e => e.EngineOilProduct)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OemBasicService>()
                .Property(e => e.VehicleTransmission)
                .IsUnicode(false);

            modelBuilder.Entity<OemBasicService>()
                .HasMany(e => e.OemServiceParts)
                .WithRequired(e => e.OemBasicService)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OemBasicService>()
                .HasMany(e => e.OemServices)
                .WithRequired(e => e.OemBasicService)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OemBasicService>()
               .HasMany(e => e.OverrideRuleBasicServices)
               .WithRequired(e => e.OemBasicService)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<OemBasicService>()
                .HasMany(e => e.OverrideRuleBasicServicesCrosses)
                .WithRequired(e => e.OemBasicService)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OemComponentRule>()
                .HasMany(e => e.OemComponents)
                .WithOptional(e => e.OemComponentRule)
                .HasForeignKey(e => e.LaborTimeRuleId);

            modelBuilder.Entity<OemComponentRule>()
                .HasMany(e => e.OemComponents1)
                .WithOptional(e => e.OemComponentRule1)
                .HasForeignKey(e => e.PartRuleId);

            modelBuilder.Entity<OemComponentRuleType>()
                .HasMany(e => e.OemComponentRules)
                .WithRequired(e => e.OemComponentRuleType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OemComponent>()
                .Property(e => e.VehicleDrivelineShort)
                .IsUnicode(false);

            modelBuilder.Entity<OemComponent>()
                .HasMany(e => e.OemBasicServices)
                .WithRequired(e => e.OemComponent)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OemComponent>()
                .HasMany(e => e.OemMenuLists)
                .WithRequired(e => e.OemComponent)
                .HasForeignKey(e => e.OemComponentId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OemComponent>()
                .HasMany(e => e.OemMenuLists1)
                .WithRequired(e => e.OemComponent1)
                .HasForeignKey(e => e.OemComponentId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OemComponent>()
                .HasMany(e => e.OemMenus)
                .WithRequired(e => e.OemComponent)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OemMenuList>()
                .HasMany(e => e.OemMenus)
                .WithRequired(e => e.OemMenuList)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OemMenu>()
                .HasMany(e => e.OemMenuMileages)
                .WithRequired(e => e.OemMenu)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OemMenu>()
                .HasMany(e => e.OemServices)
                .WithRequired(e => e.OemMenu)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OemPart>()
                .Property(e => e.UnitPrice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OemPart>()
                .HasMany(e => e.OemServiceParts)
                .WithRequired(e => e.OemPart)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OemPart>()
               .HasMany(e => e.OemPartNumbers)
               .WithRequired(e => e.OemPart)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<VehicleEngine>()
                .HasMany(e => e.Cars)
                .WithRequired(e => e.VehicleEngine)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VehicleEngine>()
                .HasMany(e => e.OemLofMenuExpandedORules)
                .WithRequired(e => e.VehicleEngine)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VehicleEngine>()
                .HasMany(e => e.OemMenuExpandedORules)
                .WithRequired(e => e.VehicleEngine)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VehicleEngine>()
                .HasMany(e => e.OverrideRuleEngines)
                .WithRequired(e => e.VehicleEngine)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VehicleMake>()
                .HasMany(e => e.Cars)
                .WithRequired(e => e.VehicleMake)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VehicleMake>()
                .HasMany(e => e.OemLofMenuExpandedORules)
                .WithRequired(e => e.VehicleMake)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VehicleMake>()
                .HasMany(e => e.OemMenuExpandedORules)
                .WithRequired(e => e.VehicleMake)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VehicleMake>()
                .HasMany(e => e.OverrideRuleMakes)
                .WithRequired(e => e.VehicleMake)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VehicleModel>()
                .Property(e => e.VehicleDriveline)
                .IsUnicode(false);

            modelBuilder.Entity<VehicleModel>()
                .HasMany(e => e.Cars)
                .WithRequired(e => e.VehicleModel)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VehicleModel>()
                .HasMany(e => e.OemLofMenuExpandedORules)
                .WithRequired(e => e.VehicleModel)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VehicleModel>()
                .HasMany(e => e.OemMenuExpandedORules)
                .WithRequired(e => e.VehicleModel)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VehicleModel>()
                .HasMany(e => e.OverrideRuleModels)
                .WithRequired(e => e.VehicleModel)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BgPpRule>()
               .HasMany(e => e.BgPpRulesBgProducts)
               .WithRequired(e => e.BgPpRule)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<BgProdCategory>()
                .HasMany(e => e.BgProdSubcategories)
                .WithRequired(e => e.BgProdCategory)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BgProduct>()
                .Property(e => e.Price)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BgProduct>()
                .HasMany(e => e.BgProductsCategories)
                .WithRequired(e => e.BgProduct)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BgProduct>()
                .HasMany(e => e.BgPpRulesBgProducts)
                .WithRequired(e => e.BgProduct)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BgProtectionPlanLevel>()
                .HasMany(e => e.BgProtectionPlanLevelMus)
                .WithRequired(e => e.BgProtectionPlanLevel)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BgProtectionPlan>()
                .HasMany(e => e.BgProtectionPlanDocs)
                .WithRequired(e => e.BgProtectionPlan)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BgProtectionPlan>()
                .HasMany(e => e.BgProtectionPlanLevels)
                .WithRequired(e => e.BgProtectionPlan)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BgProtectionPlan>()
                .HasMany(e => e.BgProtectionPlanMus)
                .WithRequired(e => e.BgProtectionPlan)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BgProtectionPlan>()
                .HasMany(e => e.BgProtectionSubPlans)
                .WithRequired(e => e.BgProtectionPlan)
                .WillCascadeOnDelete(false);


            modelBuilder.Entity<BgProduct>()
                .HasMany(e => e.BgProductsProtectionPlans)
                .WithRequired(e => e.BgProduct)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BgProtectionPlan>()
                .HasMany(e => e.BgProductsProtectionPlans)
                .WithRequired(e => e.BgProtectionPlan)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BgProtectionSubPlanLevel>()
                .Property(e => e.MaxReimbursement)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BgProtectionSubPlanLevel>()
                .HasMany(e => e.DealerVehiclesPpRecords)
                .WithRequired(e => e.BgProtectionSubPlanLevel)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BgProtectionSubPlan>()
                .HasMany(e => e.BgPpRules)
                .WithRequired(e => e.BgProtectionSubPlan)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BgProtectionSubPlan>()
                .HasMany(e => e.BgProtectionSubPlanLevels)
                .WithRequired(e => e.BgProtectionSubPlan)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BgProtectionSubPlan>()
                .HasMany(e => e.BgProtectionSubPlanMus)
                .WithRequired(e => e.BgProtectionSubPlan)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BgVideoClip>()
                .HasMany(e => e.BgVideoClipEditions)
                .WithRequired(e => e.BgVideoClip)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BgVideoClip>()
                .HasMany(e => e.AdditionalServiceVideoClips)
                .WithRequired(e => e.BgVideoClip)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MenuSchedule>()
                .Property(e => e.Price)
                .HasPrecision(19, 4);

            modelBuilder.Entity<MenuSchedule>()
                .Property(e => e.Total)
                .HasPrecision(19, 4);

            modelBuilder.Entity<MenuSchedule>()
                .Property(e => e.CommAdv)
                .HasPrecision(19, 4);

            modelBuilder.Entity<MenuSchedule>()
                .Property(e => e.CommMgr)
                .HasPrecision(19, 4);

            modelBuilder.Entity<MenuSchedule>()
                .Property(e => e.LaborAmnt)
                .HasPrecision(19, 4);

            modelBuilder.Entity<MenuSchedule>()
                .Property(e => e.PartsAmnt)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.EngineOilORules)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.TenantId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.EngineOilProducts)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.TenantId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.MenuGroups)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.DealerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.MenuNames)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.DealerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.MenuSchedules)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.DealerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.AdditionalServiceParts)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.TenantId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.AdditionalServices)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.TenantId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.AppointmentDetails)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.DealerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.AppointmentPresentations)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.TenantId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.Appointments)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.TenantId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.AppointmentServiceParts)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.TenantId);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.AppointmentServices)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.TenantId);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.Companies1)
                .WithOptional(e => e.ParentCompany)
                .HasForeignKey(e => e.ParentId);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.CompanyCompanyGroups)
                .WithRequired(e => e.Company)
                 .HasForeignKey(e => e.TenantId);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.CompanyGroups)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.TenantId);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.CompanySettings)
                .WithRequired(e => e.Company)
                 .HasForeignKey(e => e.TenantId);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.DealerCustomers)
                .WithRequired(e => e.Company1)
                .HasForeignKey(e => e.TenantId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.DealerDmsUsers)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.DealerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.DealerOpCodes)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.DealerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.DealerVehicles)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.TenantId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.DealerVehiclesPpRecords)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.DealerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.Inspections)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.TenantId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.InvoiceDetailParts)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.DealerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.InvoiceDetails)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.DealerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.Invoices)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.DealerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.OemLofMenuExpandedORules)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.DealerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.OemLofMenuORules)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.DealerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.OemMenuExpandedORules)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.DealerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.OemMenuORules)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.DealerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.OemServiceExpandedORules)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.DealerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.OemServiceORules)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.DealerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.OverrideRuleBasicServicesCrosses)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.DealerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.OverrideRules)
                .WithRequired(e => e.Company)
                .HasForeignKey(e => e.TenantId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
               .HasMany(e => e.AppointmentServiceParts)
               .WithRequired(e => e.Company)
               .HasForeignKey(e => e.TenantId)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
            .HasMany(e => e.AppointmentServices)
            .WithRequired(e => e.Company)
            .HasForeignKey(e => e.TenantId);

            modelBuilder.Entity<CompanyGroup>()
                .HasMany(e => e.CompanyCompanyGroups)
                .WithRequired(e => e.CompanyGroup)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Country>()
                .Property(e => e.Iso3166A2)
                .IsFixedLength();

            modelBuilder.Entity<Country>()
                .Property(e => e.Iso3166A3)
                .IsFixedLength();

            modelBuilder.Entity<Country>()
                .Property(e => e.Iso3166Number)
                .IsFixedLength();

            modelBuilder.Entity<Country>()
                .HasMany(e => e.CountryStates)
                .WithRequired(e => e.Country)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DealerCustomerStatusCode>()
                .HasMany(e => e.DealerCustomers)
                .WithRequired(e => e.DealerCustomerStatusCode)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DealerVehicleStatusCode>()
                .HasMany(e => e.DealerVehicles)
                .WithRequired(e => e.DealerVehicleStatusCode)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Language>()
                .HasMany(e => e.BgProtectionPlanDocs)
                .WithRequired(e => e.Language)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Language>()
                .HasMany(e => e.BgVideoClipEditions)
                .WithRequired(e => e.Language)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Language>()
                .HasMany(e => e.DealerCustomers)
                .WithRequired(e => e.Language)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MeasurementUnit>()
                .HasMany(e => e.OemMenuMileages)
                .WithRequired(e => e.MeasurementUnit)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MeasurementUnit>()
                .HasMany(e => e.BgProtectionPlanLevelMus)
                .WithRequired(e => e.MeasurementUnit)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MeasurementUnit>()
                .HasMany(e => e.BgProtectionSubPlanMus)
                .WithRequired(e => e.MeasurementUnit)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MeasurementUnit>()
                .HasMany(e => e.DealerVehiclesPpRecords)
                .WithRequired(e => e.MeasurementUnit)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MeasurementUnit>()
                .HasMany(e => e.Companies)
                .WithRequired(e => e.MeasurementUnit)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MeasurementUnit>()
                .HasMany(e => e.AppointmentPresentations)
                .WithRequired(e => e.MeasurementUnit)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Permission>()
                .HasMany(e => e.RolePermissions)
                .WithRequired(e => e.Permission)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Role>()
                .HasMany(e => e.RolePermissions)
                .WithRequired(e => e.Role)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Role>()
                .HasMany(e => e.UserCompanyRoles)
                .WithRequired(e => e.Role)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Core.Entities.TimeZone>()
                  .Property(e => e.TimeOffset)
                  .HasPrecision(5, 2);

            //modelBuilder.Entity<StateTimeZone>()
            //    .Property(e => e.StateTimeZoneId)
            //    .IsFixedLength()
            //    .IsUnicode(false);

            modelBuilder.Entity<Core.Entities.TimeZone>()
                .HasMany(e => e.StateTimeZones)
                .WithRequired(e => e.TimeZone)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.UserCompanyRoles)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Alerts)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatorUserId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.AlertsUsers)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.AppointmentPresentations)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.AdvisorId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Articles)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.ModifyUserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Articles1)
                .WithRequired(e => e.User1)
                .HasForeignKey(e => e.CreatorId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserStatusCode>()
                .HasMany(e => e.Users)
                .WithRequired(e => e.UserStatusCode)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VinDigit>()
                .Property(e => e.Digit)
                .IsFixedLength();

            modelBuilder.Entity<AdditionalServicePart>()
                .Property(e => e.UnitPrice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<AdditionalService>()
                .Property(e => e.LaborRate)
                .HasPrecision(19, 4);

            modelBuilder.Entity<AdditionalService>()
                .Property(e => e.AdvisorIncentive)
                .HasPrecision(19, 4);

            modelBuilder.Entity<AdditionalService>()
                .Property(e => e.TechIncentive)
                .HasPrecision(19, 4);

            modelBuilder.Entity<AdditionalService>()
                .HasMany(e => e.AdditionalServiceParts)
                .WithRequired(e => e.AdditionalService)
                .HasForeignKey(e => e.AdditionalServiceId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AdditionalService>()
                .HasMany(e => e.AdditionalServiceVideoClips)
                .WithRequired(e => e.AdditionalService)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AppointmentDetail>()
                .Property(e => e.Price)
                .HasPrecision(19, 4);

            modelBuilder.Entity<AppointmentPresentation>()
                .HasMany(e => e.AppointmentServices)
                .WithRequired(e => e.AppointmentPresentation)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AppointmentPresentation>()
                .HasMany(e => e.PdfDocuments)
                .WithRequired(e => e.AppointmentPresentation)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Appointment>()
                .HasMany(e => e.AppointmentDetails)
                .WithRequired(e => e.Appointment)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Appointment>()
                .HasMany(e => e.AppointmentPresentations)
                .WithRequired(e => e.Appointment)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AppointmentService>()
                .Property(e => e.LaborRate)
                .HasPrecision(19, 4);

            modelBuilder.Entity<AppointmentService>()
                .Property(e => e.PartsPrice)
                .HasPrecision(19, 4);

            //modelBuilder.Entity<AppointmentService>()
            //  .HasMany(e => e.AppointmentServiceParts)
            //  .WithRequired(e => e.AppointmentService)
            //  .WillCascadeOnDelete(false);

            //modelBuilder.Entity<AppointmentService>()
            //.HasMany(o => o.AppointmentServiceParts)
            //.WithOptional()
            //.HasForeignKey(c => c.AppointmentServicesId);

            modelBuilder.Entity<AppointmentService>()
            .HasMany(e => e.AppointmentServiceParts)
            .WithRequired(e => e.AppointmentService)
            .HasForeignKey(e => e.AppointmentServicesId);


            modelBuilder.Entity<AppointmentServicePart>()
              .Property(e => e.UnitPrice);
             // .HasPrecision(19, 4);



            //modelBuilder.Entity<DealerCustomer>()
            //    .HasMany(e => e.AppointmentPresentations)
            //    .WithRequired(e => e.DealerCustomer)
            //    .WillCascadeOnDelete(false);

            modelBuilder.Entity<DealerCustomer>()
                .HasMany(e => e.Appointments)
                .WithRequired(e => e.DealerCustomer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DealerCustomer>()
                .HasMany(e => e.Invoices)
                .WithRequired(e => e.DealerCustomer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DealerCustomer>()
                .HasMany(e => e.PdfDocuments)
                .WithRequired(e => e.DealerCustomer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DealerOpCode>()
                .Property(e => e.LaborCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DealerOpCode>()
                .Property(e => e.LaborRate)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DealerOpCode>()
                .Property(e => e.Price)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DealerOpCode>()
                .Property(e => e.ServiceMenuType)
                .IsFixedLength();

            modelBuilder.Entity<DealerVehicle>()
                .HasMany(e => e.AppointmentPresentations)
                .WithRequired(e => e.DealerVehicle)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DealerVehicle>()
                .HasMany(e => e.DealerVehiclesPpRecords)
                .WithRequired(e => e.DealerVehicle)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DealerVehicle>()
                .HasMany(e => e.Invoices)
                .WithRequired(e => e.DealerVehicle)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<InvoiceDetailPart>()
                .Property(e => e.PartCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<InvoiceDetailPart>()
                .Property(e => e.PartSale)
                .HasPrecision(19, 4);

            modelBuilder.Entity<InvoiceDetailPart>()
                .Property(e => e.PartDiscount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<InvoiceDetail>()
                .Property(e => e.LaborCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<InvoiceDetail>()
                .Property(e => e.LaborSale)
                .HasPrecision(19, 4);

            modelBuilder.Entity<InvoiceDetail>()
                .Property(e => e.PartSale)
                .HasPrecision(19, 4);

            modelBuilder.Entity<InvoiceDetail>()
                .Property(e => e.PartCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<InvoiceDetail>()
                .HasMany(e => e.InvoiceDetailParts)
                .WithRequired(e => e.InvoiceDetail)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Invoice>()
                .HasMany(e => e.DealerVehiclesPpRecords)
                .WithRequired(e => e.Invoice)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Invoice>()
                .HasMany(e => e.InvoiceDetails)
                .WithRequired(e => e.Invoice)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OemLofMenuORule>()
                .HasMany(e => e.OemLofMenuExpandedORules)
                .WithRequired(e => e.OemLofMenuORule)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OemMenuORule>()
                .HasMany(e => e.OemMenuExpandedORules)
                .WithRequired(e => e.OemMenuORule)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OemServiceORule>()
                .HasMany(e => e.OemServiceExpandedORules)
                .WithRequired(e => e.OemServiceORule)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OverrideRule>()
                .Property(e => e.LaborPriceAmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OverrideRule>()
                .Property(e => e.LaborHourAmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OverrideRule>()
                .Property(e => e.PartPriceAmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OverrideRule>()
                .HasMany(e => e.OverrideRuleBasicServices)
                .WithRequired(e => e.OverrideRule)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OverrideRule>()
               .HasMany(e => e.OverrideRuleDriveLines)
               .WithRequired(e => e.OverrideRule)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<OverrideRule>()
                .HasMany(e => e.OverrideRuleEngines)
                .WithRequired(e => e.OverrideRule)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OverrideRule>()
                .HasMany(e => e.OverrideRuleMakes)
                .WithRequired(e => e.OverrideRule)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OverrideRule>()
                .HasMany(e => e.OverrideRuleModels)
                .WithRequired(e => e.OverrideRule)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OverrideRule>()
                .HasMany(e => e.OverrideRuleTransmissions)
                .WithRequired(e => e.OverrideRule)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OverrideRule>()
                .HasMany(e => e.OverrideRuleYears)
                .WithRequired(e => e.OverrideRule)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OverrideRule>()
                .HasMany(e => e.OverrideRuleBasicServicesCrosses)
                .WithOptional(e => e.OverrideRule)
                .HasForeignKey(e => e.LaborHourRuleId);

            modelBuilder.Entity<OverrideRule>()
                .HasMany(e => e.OverrideRuleBasicServicesCrosses1)
                .WithOptional(e => e.OverrideRule1)
                .HasForeignKey(e => e.LaborPriceRuleId);

            modelBuilder.Entity<OverrideRule>()
                .HasMany(e => e.OverrideRuleBasicServicesCrosses2)
                .WithOptional(e => e.OverrideRule2)
                .HasForeignKey(e => e.PartPriceRuleId);

            modelBuilder.Entity<OverrideRule>()
                .HasMany(e => e.OverrideRuleBasicServicesCrosses3)
                .WithOptional(e => e.OverrideRule3)
                .HasForeignKey(e => e.OpCodeRuleId);

            modelBuilder.Entity<OverrideRule>()
                .HasMany(e => e.OverrideRuleBasicServicesCrosses4)
                .WithOptional(e => e.OverrideRule4)
                .HasForeignKey(e => e.PartNoRuleId);

            modelBuilder.Entity<ILife>()
                .Property(e => e.Sale_Price)
                .HasPrecision(19, 4);

            modelBuilder.Entity<ILife>()
                .Property(e => e.Surcharge)
                .HasPrecision(19, 4);

            modelBuilder.Entity<ILife_History>()
                .Property(e => e.Sale_Price)
                .HasPrecision(19, 4);

            modelBuilder.Entity<ILife_History>()
                .Property(e => e.Surcharge)
                .HasPrecision(19, 4);

            modelBuilder.Entity<ArticleCategory>()
                .HasMany(e => e.ArticleArticleCategories)
                .WithRequired(e => e.ArticleCategory)
                .HasForeignKey(e => e.ArticleCategoryId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Article>()
                .HasMany(e => e.ArticleArticleCategories)
                .WithRequired(e => e.Article)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Alert>()
                .HasMany(e => e.AlertsUsers)
                .WithRequired(e => e.Alert)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AlertsStatus>()
                .HasMany(e => e.AlertsUsers)
                .WithRequired(e => e.AlertsStatus)
                .HasForeignKey(e => e.StatusId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PrnConfiguration>()
                .HasMany(e => e.PrnConfigPrinters)
                .WithRequired(e => e.PrnConfiguration)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PrnJobType>()
                .HasMany(e => e.PrnConfigurations)
                .WithRequired(e => e.PrnJobType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PrnPrinter>()
                .HasMany(e => e.PrnConfigPrinters)
                .WithRequired(e => e.PrnPrinter)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DamageIndication>()
                .Property(e => e.OffsetLeft)
                .HasPrecision(5, 2);

            modelBuilder.Entity<DamageIndication>()
                .Property(e => e.OffsetTop)
                .HasPrecision(5, 2);

            modelBuilder.Entity<VinMasterDriveline>()
                .HasMany(e => e.VinMaster)
                .WithRequired(e => e.VinMasterDriveline)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VinMasterTransmission>()
                .HasMany(e => e.VinMaster)
                .WithRequired(e => e.VinMasterTransmission)
                .WillCascadeOnDelete(false);
        }
    }
}
