﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using SmartVMA.Core.Contracts;
using System.Linq;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Validation;
using System.Text;

namespace SmartVMA.EfRepository
{
    internal abstract class BaseRepository<T, K> : StoredProceduresRepository, IRepository<T, K> where T : class, IBaseEntity<K>, new()
    {
        public BaseRepository(Models dbContext) : base(dbContext)
        {
            _dbContext.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
        }

        protected virtual IQueryable<T> GetDbSet(params string[] includeReferences)
        {
            var result = _dbContext.Set<T>().AsQueryable();
            foreach (var item in includeReferences)
            {
                result = result.Include(item);
            }
            return result;
        }


        public IEnumerable<T> Find(Expression<Func<T, bool>> expression = null)
        {
            if (expression != null)
            {
                return GetDbSet().Where(expression);
            }
            return GetDbSet();
        }

        public T FirstOrDefault(Expression<Func<T, bool>> expression = null)
        {
            if (expression != null)
            {
                return GetDbSet().FirstOrDefault(expression);
            }
            return GetDbSet().FirstOrDefault();
        }

        public virtual void Insert(T entity)
        {
            entity.TimeCreated = DateTime.UtcNow;
            entity.TimeUpdated = DateTime.UtcNow;

            _dbContext.Set<T>().Add(entity);
        }

        public void Insert(IEnumerable<T> entities)
        {
            var entitiesList = entities.ToList();
            foreach (var entity in entitiesList)
            {
                entity.TimeCreated = DateTime.UtcNow;
                entity.TimeUpdated = DateTime.UtcNow;
            }

            _dbContext.Set<T>().AddRange(entitiesList);
        }

        public virtual void Update(T entity)
        {
            entity.TimeUpdated = DateTime.UtcNow;
            _dbContext.Entry<T>(entity).State = EntityState.Modified;
        }

        public void Delete(T entity)
        {
            _dbContext.Set<T>().Attach(entity);
            _dbContext.Set<T>().Remove(entity);
        }

        public void Delete(K id)
        {
            var entity = new T { Id = id };

            _dbContext.Set<T>().Attach(entity);
            _dbContext.Set<T>().Remove(entity);
        }

        public void Delete(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                _dbContext.Set<T>().Attach(entity);
            }
            _dbContext.Set<T>().RemoveRange(entities);
        }

        public ServiceResponse CommitChanges()
        {
            var response = new ServiceResponse();
            try
            {
                _dbContext.SaveChanges();
                response.IsSuccess = true;
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    sb.Append($"Entity of type \"{eve.Entry.Entity.GetType().Name}\" in state \"{eve.Entry.State}\" has the following validation errors:");
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.Append($" - Property: \"{ve.PropertyName}\", Error: \"{ve.ErrorMessage}\"");
                    }
                }
                response.Errors.Add("ErrorMessage", sb.ToString());
            }
            catch (Exception e)
            {
                response.Errors.Add("ErrorMessage", e.ToString());
            }
            
            return response;
        }        
    }
}
