﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class EngineOilORulesRepository : BaseTenantRepository<EngineOilORule, int>, IEngineOilORulesRepository
    {
        public EngineOilORulesRepository(Models dbContext, IAppContext appContext) : base(dbContext, appContext)
        {
        }
        
        public IEnumerable<EngineOilORule> GetAllEngineOilORuleForDealer()
        {
            var eop = GetDbSet("EngineOilProduct");
           
            return eop;
        }

        public EngineOilORule GetEngineOilORuleById(int id)
        {
           return GetDbSet("EngineOilProduct").FirstOrDefault(x => x.Id == id);
        }

        public EngineOilORule GetEngineOilORuleByTypeAndViscosity(string oilType, string oilViscosity)
        {
            return GetDbSet().FirstOrDefault(x => x.FluidType == oilType && x.FluidViscosity == oilViscosity);
        }

        public int GetPagedResultsTotal(PagingRequest request)
        {
            var result = GetDbSet("EngineOilProduct");
            if (request.Search != null && !string.IsNullOrEmpty(request.Search.Value))
            {
                result = result.Where(x => x.FluidType.ToLower().Contains(request.Search.Value.ToLower()) 
                || x.FluidViscosity.ToLower().Contains(request.Search.Value.ToLower())
                || x.EngineOilProduct.PartNumber.ToLower().Contains(request.Search.Value.ToLower())
                || x.EngineOilProduct.PartName.ToLower().Contains(request.Search.Value.ToLower())
                || x.EngineOilProduct.UnitPrice.ToString(CultureInfo.InvariantCulture).ToLower().Contains(request.Search.Value.ToLower())
                || x.EngineOilProduct.FluidType.ToLower().Contains(request.Search.Value.ToLower())
                || x.EngineOilProduct.FluidViscosity.ToLower().Contains(request.Search.Value.ToLower()));
            }
            return result.Count();
        }

        public IEnumerable<EngineOilORule> GetPagedResults(PagingRequest request)
        {
            var result = GetDbSet("EngineOilProduct");
            var columnsSort = new Dictionary<int, string> {
                {0, "FluidType"},
                {1, "FluidViscosity"},
                {2, "EngineOilProduct.PartNumber"},
                {3, "EngineOilProduct.PartName"},
                {4, "EngineOilProduct.UnitPrice"},
                {5, "EngineOilProduct.FluidType"},
                {6, "EngineOilProduct.FluidViscosity"},
                {7, "EngineOilProduct.MenuLevel"}
            };

            if (request.Search != null && !string.IsNullOrEmpty(request.Search.Value))
            {
                result = result.Where(x => x.FluidType.ToLower().Contains(request.Search.Value.ToLower())
                || x.FluidViscosity.ToLower().Contains(request.Search.Value.ToLower())
                || x.EngineOilProduct.PartNumber.ToLower().Contains(request.Search.Value.ToLower())
                || x.EngineOilProduct.PartName.ToLower().Contains(request.Search.Value.ToLower())
                || x.EngineOilProduct.UnitPrice.ToString(CultureInfo.InvariantCulture).ToLower().Contains(request.Search.Value.ToLower())
                || x.EngineOilProduct.FluidType.ToLower().Contains(request.Search.Value.ToLower())
                || x.EngineOilProduct.FluidViscosity.ToLower().Contains(request.Search.Value.ToLower()));
            }

            var order = request.Order.FirstOrDefault();
            if (order != null && order.Dir == "asc")
            {
                result = result.OrderByTest(columnsSort[order.Column]).Skip(request.Start).Take(request.Length);
            }
            else if (order != null)
            {
                result = result.OrderByDescendingTest(columnsSort[order.Column]).Skip(request.Start).Take(request.Length);
            }
            return result;
        }

        public EngineOilORule GetEngineOilOverride(string oilType, string oilViscosity, int? menuLevel = null)
        {
            return GetDbSet("EngineOilProduct").FirstOrDefault(x => x.FluidType == oilType && x.FluidViscosity == oilViscosity);        
        }
    }
}
