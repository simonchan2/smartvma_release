﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class VinMasterDrivelineRepository : BaseRepository<VinMasterDriveline, int>, IVinMasterDrivelineRepository
    {
        public VinMasterDrivelineRepository(Models dbContext, IAppContext appContext) : base(dbContext)
        {
        }

        public string GetVinMasterDriveline(int vinMasterDrivelineId)
        {
            var vinmasterDriveline = GetDbSet().FirstOrDefault(c => c.Id == vinMasterDrivelineId);

            return vinmasterDriveline != null ? vinmasterDriveline.Driveline : string.Empty;
        }
    }
}
