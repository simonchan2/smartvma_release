﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class AlertRepository : BaseRepository<Alert, int>, IAlertRepository
    {
        protected readonly IAppContext _appContext;
        public AlertRepository(Models dbContext, IAppContext appContext) : base(dbContext)
        {
            _appContext = appContext;
        }

        public IEnumerable<Alert> GetAllActiveAlertsForCurrentUser()
        {
            var currentUserId = _appContext.CurrentIdentity.UserId;
            var currentDateTime = DateTime.Now;
            //if AlertUser.StatusId == 2 ("Closed"), the Alert is closed by User, so don't load it againg. 
            //Get only new Alerts AND Alerts that has Status != "Closed" (like: "Reminde me lateer", "I'm with Client".. etc)
            var alerts = GetDbSet("AlertsUsers", "AlertsUsers.User", "AlertsUsers.AlertsStatus");
            
            alerts = alerts.Where(x => x.StartDate <= currentDateTime && currentDateTime <= x.EndDate && x.AlertsUsers.Where(y => y.UserId == currentUserId && y.StatusId == 2).Count() == 0);
           
            return alerts;
        }

        public int GetPagedResultsTotal(PagingRequest request)
        {
            var result = GetDbSet();
            if (request.Search != null && !string.IsNullOrEmpty(request.Search.Value))
            {
                result = result.Where(x => x.Description.ToLower().Contains(request.Search.Value.ToLower()) || x.Title.ToLower().Contains(request.Search.Value.ToLower()));
            }
            return result.Count();
        }

        public IEnumerable<Alert> GetPagedResults(PagingRequest request)
        {
            var result = GetDbSet();
            var columnsSort = new Dictionary<int, string> {
                {0, "Title"},
                {1, "Description"},
                {2, "StartDate"},
                {3, "EndDate"}
            };

            if (request.Search != null && !string.IsNullOrEmpty(request.Search.Value))
            {
                result = result.Where(x => x.Description.ToLower().Contains(request.Search.Value.ToLower()) || x.Title.ToLower().Contains(request.Search.Value.ToLower()));
            }

            var order = request.Order.FirstOrDefault();
            if (order.Dir == "asc")
            {
                result = result.OrderByTest(columnsSort[order.Column]).Skip(request.Start).Take(request.Length);
            }
            else
            {
                result = result.OrderByDescendingTest(columnsSort[order.Column]).Skip(request.Start).Take(request.Length);
            }
            return result;
        }
    }
}
