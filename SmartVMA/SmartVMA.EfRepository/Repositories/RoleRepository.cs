﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.Ioc;
using System.Collections.Generic;
using System.Linq;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class RoleRepository : BaseRepository<Role, int>, IRoleRepository
    {
        public RoleRepository(Models dbContext) : base(dbContext)
        {
        }

        public int GetPagedResultsTotal(PagingRequest request)
        {
            var result = GetDbSet();
            if (request.Search != null && !string.IsNullOrEmpty(request.Search.Value))
            {
                result = result.Where(x => x.Description.ToLower().Contains(request.Search.Value.ToLower()) || x.Role1.ToLower().Contains(request.Search.Value.ToLower()));
            }
            return result.Count();
        }

        public IEnumerable<Role> GetPagedResults(PagingRequest request)
        {
            var result = GetDbSet();
            var columnsSort = new Dictionary<int, string> {
                {0, "Role1"},
                {1, "Description"}
            };

            if (request.Search != null && !string.IsNullOrEmpty(request.Search.Value))
            {
                result = result.Where(x => x.Description.ToLower().Contains(request.Search.Value.ToLower()) || x.Role1.ToLower().Contains(request.Search.Value.ToLower()));
            }

            var order = request.Order.FirstOrDefault();
            if (order.Dir == "asc")
            {
                result = result.OrderByTest(columnsSort[order.Column]).Skip(request.Start).Take(request.Length);
            }
            else
            {
                result = result.OrderByDescendingTest(columnsSort[order.Column]).Skip(request.Start).Take(request.Length);
            }
            return result;
        }
    }
}
