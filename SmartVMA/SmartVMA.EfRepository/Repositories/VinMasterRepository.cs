﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class VinMasterRepository : BaseRepository<VinMaster, int>, IVinMasterRepository
    {
        public VinMasterRepository(Models dbContext, IAppContext appContext) : base(dbContext)
        {
        }

        public IEnumerable<string> GetAllVehicleSteerings()
        {
            var response = SPExecuteReader("GetVehicleSteerings");

            return response.AsEnumerable().Select(r => r.Field<string>("VehicleSteering")).ToList();
        }

        public int? GetVinMasterByVinShort(string vinShort)
        {
            int? id = null;
            var result = GetDbSet().FirstOrDefault(x => x.VinShort == vinShort);
            if (result != null)
            {
                id = result.Id;
            }
            return id;
        }
    }
}
