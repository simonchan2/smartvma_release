﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class DamageIndicationRepository : BaseRepository<DamageIndication, long>, IDamageIndicationRepository
    {
        public DamageIndicationRepository(Models dbContext, IAppContext appContext) : base(dbContext)
        {
        }
        public IEnumerable<DamageIndication> GetDamageIndications(long inspectionId)
        {
            var list = Find(x => x.InspectionId == inspectionId);
            return list;
        }

        public ServiceResponse CreateDamageIndications(List<DamageIndication> entities)
        {
            Insert(entities);
            var response = CommitChanges();
            return response;
        }

        public ServiceResponse DeleteDamageIndications(List<DamageIndication> entities)
        {
            Delete(entities);
            var response = CommitChanges();
            return response;
        }
    }
}