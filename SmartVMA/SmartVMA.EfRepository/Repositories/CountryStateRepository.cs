﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.Ioc;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class CountryStateRepository : BaseRepository<CountryState, int>, ICountryStateRepository
    {
        public CountryStateRepository(Models dbContext) : base(dbContext)
        {
        }
    }
}
