﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.Ioc;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class MeasurementUnitRepository : BaseRepository<MeasurementUnit, int>, IMeasurementUnitRepository
    {
        public MeasurementUnitRepository(Models dbContext) : base(dbContext)
        {
        }
    }
}
