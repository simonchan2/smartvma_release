﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class CompanyRepository : BaseRepository<Company, int>, ICompanyRepository
    {
        private readonly ICompanySettingRepository _companySettingRepository;

        public CompanyRepository(Models dbContext, ICompanySettingRepository companySettingRepository) : base(dbContext)
        {
            _companySettingRepository = companySettingRepository;
        }

        public Company GetCompanyAndSettings(int id)
        {
            return GetDbSet("CompanySettings", "ParentCompany").FirstOrDefault(x => x.Id == id);
        }

        public virtual IEnumerable<Company> GetCompanies(Expression<Func<Company, bool>> expression = null)
        {
            var appContext = IocManager.Resolve<IAppContext>();
            var currentIdentiy = appContext.CurrentIdentity;
            var result = GetDbSet("ParentCompany.CompanyGroups.CompanyCompanyGroups");
            if (currentIdentiy.RoleId != (int)UserRoles.SystemAdministrator)
            {
                result = result.Where(x => x.Id == currentIdentiy.TenantId || x.ParentId == currentIdentiy.TenantId);
            }
            return expression != null ? result.Where(expression) : result;
        }

        public virtual IEnumerable<Company> GetParentCompanies(Expression<Func<Company, bool>> expression = null)
        {
            var appContext = IocManager.Resolve<IAppContext>();
            var currentIdentiy = appContext.CurrentIdentity;
            var tenantId = currentIdentiy.TenantId;
            var result = GetDbSet("ParentCompany.CompanyGroups.CompanyCompanyGroups", "Companies1");
            if (currentIdentiy.RoleId != (int)UserRoles.SystemAdministrator)
            {
                result = result.Where(x => x.Id == tenantId || x.Companies1.Any(child => child.Id == tenantId));
            }
            return expression != null ? result.Where(expression) : result;
        }


        public void Update(Company company, IEnumerable<CompanySetting> settings)
        {
            Update(company);
            foreach (var item in settings)
            {
                var setting = company.CompanySettings.FirstOrDefault(x => x.Name == item.Name);
                if (setting != null)
                {
                    setting.Value = item.Value;
                    _companySettingRepository.Update(setting);
                }
                else
                {
                    _companySettingRepository.Insert(item);
                    company.CompanySettings.Add(item);
                }
            }
        }


        public void Insert(Company company, IEnumerable<CompanySetting> settings)
        {
            Insert(company);
            foreach (var setting in settings)
            {
                _companySettingRepository.Insert(setting);
                company.CompanySettings.Add(setting);
            }
        }

        public Company GetCompanyAndParents(int id)
        {
            return GetDbSet("ParentCompany", "ParentCompany.ParentCompany").FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Company> GetCompaniesForUser(string email)
        {
            return GetDbSet("UserCompanyRoles.User").Where(x => x.UserCompanyRoles.Any(y => y.User.Email == email));
        }

        //methods for server site pagination        

        public int GetPagedResultsTotal(PagingRequest request, Expression<Func<Company, bool>> expression = null)
        {
            var appContext = IocManager.Resolve<IAppContext>();
            var currentIdentiy = appContext.CurrentIdentity;
            var result = GetDbSet("ParentCompany.CompanyGroups.CompanyCompanyGroups");

            if (currentIdentiy.RoleId != (int)UserRoles.SystemAdministrator)
            {
                result = result.Where(x => x.Id == currentIdentiy.TenantId || x.ParentId == currentIdentiy.TenantId);
            }

            if (expression != null)
            {
                result = result.Where(expression);
            }
            if (request.Search != null && !string.IsNullOrEmpty(request.Search.Value))
            {
                result = result.Where(x => x.ParentCompany.CompanyName.Contains(request.Search.Value) || x.CompanyName.Contains(request.Search.Value));
            }
            return result.Count();
        }

        public IEnumerable<Company> GetPagedResults(PagingRequest request, Expression<Func<Company, bool>> expression = null)
        {
            var appContext = IocManager.Resolve<IAppContext>();
            var currentIdentiy = appContext.CurrentIdentity;
            var result = GetDbSet("ParentCompany.CompanyGroups.CompanyCompanyGroups");

            if (currentIdentiy.RoleId != (int)UserRoles.SystemAdministrator)
            {
                result = result.Where(x => x.Id == currentIdentiy.TenantId || x.ParentId == currentIdentiy.TenantId);
            }

            if (expression != null)
            {
                result = result.Where(expression);
            }
            if (request.Search != null && !string.IsNullOrEmpty(request.Search.Value))
            {
                result = result.Where(x => x.ParentCompany.CompanyName.Contains(request.Search.Value) || x.CompanyName.Contains(request.Search.Value));
            }
            var columnsSort = new Dictionary<int, string> {
                {0, "ParentCompany.CompanyName"},
                {1, "CompanyName"},
                {2, "CompanyTypeId"}
            };
            var order = request.Order.FirstOrDefault();
            if (order.Dir == "asc")
            {
                result = result.OrderByTest(columnsSort[order.Column]).Skip(request.Start).Take(request.Length);
            }
            else
            {
                result = result.OrderByDescendingTest(columnsSort[order.Column]).Skip(request.Start).Take(request.Length);
            }
            return result;
        }
    }
}
