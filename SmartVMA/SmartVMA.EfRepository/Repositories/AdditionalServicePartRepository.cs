﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class AdditionalServicePartRepository : BaseTenantRepository<AdditionalServicePart, long>, IAdditionalServicePartRepository
    {
        public AdditionalServicePartRepository(Models dbContext, IAppContext appContext) : base(dbContext, appContext)
        {
        }
    }
}
