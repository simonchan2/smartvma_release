﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.InputModels.ViewModels.Admin;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class BgProductRepository: BaseRepository<BgProduct, int>, IBgProductRepository
    {
        public ParameterDirection Direction { get; private set; }

        public BgProductRepository(Models dbContext, IAppContext appContext) : base(dbContext)
        {
        }

        public BgProduct BgProductById(int id)
        {
            return GetDbSet().FirstOrDefault(x => x.Id == id);
        }

        public BgProductsResultView GetBgProductProtectionPlanBySKUs(int id)
        {
            BgProductsResultView response = new BgProductsResultView();

            List<BgProductsResultView> result = new List<BgProductsResultView>();

            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@ProductId", id));

            string[] tableNames = { "tbResults" };

            var resultsById = SPExecuteDataSet(SPNames.GetBgProductsProtectionPlansBySKU, parameters, tableNames: tableNames);

            foreach (DataRow row in resultsById.Tables["tbResults"].Rows)
            {
                var res = new BgProductsResultView();
                string[] resultPlanIds;
                string[] resultCategoryIds;
                char[] charSeparator = new char[] { ',' };
                string selectedCategoryNames = string.Empty;

                res.BGSKU = (string)row["BGSKU"];
                res.ProductId = (int)row["BgProductId"];
                res.ProductDescription = row["ProductDescription"].ToString();
                res.BgProductProtectionPlanIds = row["BgProductProtectionPlanIds"].ToString();
                resultPlanIds = res.BgProductProtectionPlanIds.Split(charSeparator, StringSplitOptions.None);
                resultPlanIds = resultPlanIds.Distinct().ToArray();
                resultPlanIds = resultPlanIds.Where(w => w != "").ToArray();
                res.BgProductCategoryIds = row["BgProductCategoryIds"].ToString();
                resultCategoryIds = res.BgProductCategoryIds.Split(charSeparator, StringSplitOptions.None);
                resultCategoryIds = resultCategoryIds.Distinct().ToArray();
                resultCategoryIds = resultCategoryIds.Where(w => w != "").ToArray();
                var selectedCategories = new List<int>();
                foreach (var item in resultCategoryIds)
                {
                    selectedCategories.Add(Convert.ToInt32(item));
                }
                res.SelectedCategoryIDs = selectedCategories;
                var selectedPlanIDs = new List<int>();
                var dicSelectedPlanIds = new Dictionary<string, bool>();
                dicSelectedPlanIds.Add("LifetimeProtection", res.LifetimeProtection == null ? false : true);
                dicSelectedPlanIds.Add("ForeverDiesel", res.ForeverDiesel == null ? false : true);
                dicSelectedPlanIds.Add("EngineAssurance", res.EngineAssurance == null ? false : true);
 
                res.SelectedCategoriesNames = selectedCategoryNames;
                res.dicSelectedProjectPlanIDs = dicSelectedPlanIds;
                if (resultPlanIds.Contains("1")) res.LifetimeProtection = "LifetimeProtection"; else res.LifetimeProtection = "";
                if (resultPlanIds.Contains("2")) res.ForeverDiesel = "ForeverDiesel"; else res.ForeverDiesel = "";
                if (resultPlanIds.Contains("3")) res.EngineAssurance = "EngineAssurance"; else res.EngineAssurance = "";
                result.Add(res);
            }

            response = result.FirstOrDefault();

            return response;

        }


        public IEnumerable<BgProductsResultView> GetPagedResults(PagingRequest request, string categoryIds)
        {

            List<BgProductsResultView> result = new List<BgProductsResultView>();

            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@CategoryIds", categoryIds));
            parameters.Add(new SqlParameter("@PageIndex", request.Start));
            parameters.Add(new SqlParameter("@PageSize", request.Length));

            SqlParameter outputRecordCount = new SqlParameter("@RecordCount", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };

            string[] tableNames = { "tbResults" };

            var resultsById = SPExecuteDataSet(SPNames.GetBgProductsProtectionPlansPage, parameters, outputRecordCount, tableNames: tableNames);
            var bgProdCategoryrepository = IocManager.Resolve<IBgProdCategoryRepository>();

            foreach (DataRow row in resultsById.Tables["tbResults"].Rows)
            {
                var res = new BgProductsResultView();
                string[] resultPlanIds;
                string[] resultCategoryIds;
                char[] charSeparator = new char[] { ',' };
                string selectedCategoryNames = string.Empty;

                res.RecordCount = Convert.ToInt32(outputRecordCount.SqlValue.ToString());
                res.BGSKU = (string)row["BGSKU"];
                res.ProductId = (int)row["BgProductId"];
                res.ProductDescription = row["ProductDescription"].ToString();
                res.BgProductProtectionPlanIds = row["BgProductProtectionPlanIds"].ToString();
                resultPlanIds = res.BgProductProtectionPlanIds.Split(charSeparator, StringSplitOptions.None);
                resultPlanIds = resultPlanIds.Distinct().ToArray();
                resultPlanIds = resultPlanIds.Where(w => w != "").ToArray();
                res.BgProductCategoryIds = row["BgProductCategoryIds"].ToString();
                resultCategoryIds = res.BgProductCategoryIds.Split(charSeparator, StringSplitOptions.None);
                resultCategoryIds = resultCategoryIds.Distinct().ToArray();
                resultCategoryIds = resultCategoryIds.Where(w => w != "").ToArray();
                foreach (var item in resultCategoryIds)
                {
                    int ProdCategoryId = Convert.ToInt32(item);
                    var bgprodCat = bgProdCategoryrepository.FirstOrDefault(x => x.Id == ProdCategoryId);

                    if (selectedCategoryNames == string.Empty)
                    {
                        selectedCategoryNames = selectedCategoryNames + bgprodCat.Description.ToString(); ;
                    }
                    else
                    {
                        selectedCategoryNames = selectedCategoryNames + ";" + bgprodCat.Description.ToString(); ;
                    }
                }
                res.SelectedCategoriesNames = selectedCategoryNames;

                var dicSelectedPlanIds = new Dictionary<string, bool>();
                dicSelectedPlanIds.Add("LifetimeProtection", res.LifetimeProtection == null ? false : true);
                dicSelectedPlanIds.Add("ForeverDiesel", res.ForeverDiesel == null ? false : true);
                dicSelectedPlanIds.Add("EngineAssurance", res.EngineAssurance == null ? false : true);

                res.dicSelectedProjectPlanIDs = dicSelectedPlanIds;

                if (resultPlanIds.Contains("1")) res.LifetimeProtection = "LifetimeProtection"; else res.LifetimeProtection = "";
                if (resultPlanIds.Contains("2")) res.ForeverDiesel = "ForeverDiesel"; else res.ForeverDiesel = "";
                if (resultPlanIds.Contains("3")) res.EngineAssurance = "EngineAssurance"; else res.EngineAssurance = "";
                result.Add(res);
            }

            return result;
        }

    }
}
