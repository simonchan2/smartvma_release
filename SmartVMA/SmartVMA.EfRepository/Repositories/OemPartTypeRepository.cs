﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.Ioc;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class OemPartTypeRepository : BaseRepository<OemPartType, int>, IOemPartTypeRepository
    {
        public OemPartTypeRepository(Models dbContext) : base(dbContext)
        {
        }
    }
}
