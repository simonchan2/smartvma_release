﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.Ioc;
using System.Collections.Generic;
using System.Linq;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class OemComponentRepository : BaseRepository<OemComponent, int>, IOemComponentRepository
    {
        public OemComponentRepository(Models dbContext) : base(dbContext)
        {            
        }

        public OemComponent GetOemComponentById(int id)
        {
            return GetDbSet("OemBasicServices", "OemComponentRule", "OemMenuLists", "OemMenus", "OemParts").FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<OemComponent> GetAllOemComponentsShownAsOption()
        {
            return GetDbSet();//.Where(x => x.IsShownAsOption == true); -- add it back this after, if the client decide to be like so
        }
    }
}
