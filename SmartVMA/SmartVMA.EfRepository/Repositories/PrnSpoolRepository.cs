﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.Ioc;
using System.Data.SqlClient;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class PrnSpoolRepository : BaseRepository<PrnSpool, int>, IPrnSpoolRepository
    {
        public PrnSpoolRepository(Models dbContext) : base(dbContext)
        {
        }

        public IEnumerable<PrnSpool> GetRemotePrintJobs(string accounts)
        {
            List<SqlParameter> parameters = new List<SqlParameter>() { new SqlParameter("Accounts", accounts) };
            return this.SPExecuteReader<PrnSpool>("rprint.usp_GetRemotePrintJobs", parameters);
         }

        public void UpdateRemotePrintJobStatus(int id, string status)
        {
            List<SqlParameter> parameters = new List<SqlParameter>() { new SqlParameter("id", id), new SqlParameter("status", status) };
            this.SPExecuteNonQuery("rprint.usp_UpdatePrnSpoolStatus", parameters);
        }
    }
}
