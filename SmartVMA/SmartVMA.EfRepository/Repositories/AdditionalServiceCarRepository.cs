﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.Ioc;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class AdditionalServiceCarRepository : BaseRepository<AdditionalServicesCar, long>, IAdditionalServiceCarRepository
    {
        public AdditionalServiceCarRepository(Models dbContext) : base(dbContext)
        {
        }


        public bool Save(IEnumerable<int> services, IEnumerable<int> years, IEnumerable<int> makes, IEnumerable<int> models, IEnumerable<int> engines, IEnumerable<int> transmissions, IEnumerable<int> driveLines, int menuLevel)
        {
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@Services", string.Join(",", services)));
            parameters.Add(new SqlParameter("@Years", string.Join(",", years)));
            parameters.Add(new SqlParameter("@Makes", string.Join(",", makes)));
            parameters.Add(new SqlParameter("@Models", string.Join(",", models)));
            parameters.Add(new SqlParameter("@EngineTypes", string.Join(",", engines)));
            parameters.Add(new SqlParameter("@TransmissionTypes", string.Join(",", transmissions)));
            parameters.Add(new SqlParameter("@DriveLines", string.Join(",", driveLines)));
            parameters.Add(new SqlParameter("@MenuLevel", menuLevel));

            try
            {
                var result = SPExecuteScalar(SPNames.SaveAdditionalServicesCarMapping, parameters: parameters);
                return result != null;
            }
            catch (Exception)
            {

            }
            return false;
        }
    }
}
