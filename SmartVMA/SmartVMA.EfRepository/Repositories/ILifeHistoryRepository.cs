﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class ILifeHistoryRepository : BaseRepository<ILife_History, int>, IILifeHistoryRepository
    {
        public ILifeHistoryRepository(Models dbContext) : base(dbContext)
        {
        }

        public ILife_History GetLastHistory(string contractNumber, string vin)
        {
           // ILife_History result = null;
            var historyList = GetDbSet().Where(x => x.Contract_Number == contractNumber && x.VIN == vin).OrderByDescending(x => x.LastClaimDate).FirstOrDefault();
            //if (historyList != null)
            //{
            //    result = historyList.First();
            //}
            return historyList;
        }

        public List<ILife_History> GetAllHistoryForItem(string contractNumber, string vin)
        {
            var historyList = GetDbSet().Where(x => x.Contract_Number == contractNumber && x.VIN == vin).OrderByDescending(x => x.LastClaimDate).ToList();
            return historyList;
        }
    }
}
