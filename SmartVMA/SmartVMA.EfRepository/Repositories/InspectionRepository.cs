﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using SmartVMA.Core.Contracts;
using SmartVMA.Resources;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class InspectionRepository : BaseTenantRepository<Inspection, long>, IInspectionRepository
    {
        public InspectionRepository(Models dbContext, IAppContext appContext) : base(dbContext, appContext)
        {
        }

        public ServiceResponse AddSetInspection(Inspection inspection)
        {
            var response = new ServiceResponse();
            try
            {
                var parameters = new List<SqlParameter>
                {
                    SqlParameter("@Id", (inspection.Id != 0 ? inspection.Id : (long?) null)),
                    SqlParameter("@AppointmentPresentationId", inspection.AppointmentPresentationId),
                    SqlParameter("@LFRimScratch", inspection.LFRimScratch),
                    SqlParameter("@LRRimScratch", inspection.LRRimScratch),
                    SqlParameter("@RFRimScratch", inspection.RFRimScratch),
                    SqlParameter("@RRRimScratch", inspection.RRRimScratch),
                    SqlParameter("@LFTireTypeId", inspection.LFTireTypeId),
                    SqlParameter("@LRTireTypeId", inspection.LRTireTypeId),
                    SqlParameter("@RFTireTypeId", inspection.RFTireTypeId),
                    SqlParameter("@RRTireTypeId", inspection.RRTireTypeId),
                    SqlParameter("@Fuel", inspection.Fuel)
                };
                inspection.Id = Convert.ToInt64(SPExecuteScalar(SPNames.InspectionAddSet, parameters));
            }
            catch (Exception ex)
            {
                response.Errors.Add("AddSetInspection", Messages.InspectionAddSetFailed);
            }
            response.IsSuccess = !response.Errors.Any();
            return response;
        }

        public InspectionViewModel GetInspectionViewModel(long? id, long? apId, bool hideVehiclePhotos)
        {
            if(hideVehiclePhotos)
            {
                var appContext = IocManager.Resolve<IAppContext>();
                var inspection = new InspectionViewModel();
                var parameters = new List<SqlParameter>
                {
                    SqlParameter("@Id", id),
                    SqlParameter("@AppointmentPresentationId", apId)
                };
                _appContext.CurrentIdentity.TenantId = 1089;

                var data = SPExecureDSReader<DamageIndicationTypeViewModel,
                                            ViewPointViewModel,
                                            VehiclePhotoViewModel,
                                            DamageIndicationViewModel,
                                            InspectionViewModel>
                                            (SPNames.InspectionGet, parameters);
                if (data.Item5.Count > 0 && data.Item5[0] != null)
                {
                    inspection = data.Item5[0];              
                    inspection.DamageIndications = data.Item4;
                }
                else
                {
                    inspection.Id = 0;
                }
                if (data.Item1.Count > 0 && data.Item2.Count > 0)
                {
                    inspection.DamageIndicationTypes = data.Item1;
                    inspection.ViewPoints = data.Item2;
                }
                return inspection;
            }
            else
            {
                return GetInspectionViewModel(id, apId);
            }
        }

        public InspectionViewModel GetInspectionViewModel(long? id, long? apId)
        {
            var appContext = IocManager.Resolve<IAppContext>();
            var inspection = new InspectionViewModel();
            var parameters = new List<SqlParameter>
            {
                SqlParameter("@Id", id),
                SqlParameter("@AppointmentPresentationId", apId)
            };

            var data = SPExecureDSReader<DamageIndicationTypeViewModel,
                                        ViewPointViewModel,
                                        VehiclePhotoViewModel,
                                        DamageIndicationViewModel,
                                        InspectionViewModel>
                                        (SPNames.InspectionGet, parameters);
            if(data.Item5.Count > 0 && data.Item5[0] != null)
            {
                inspection = data.Item5[0];
                inspection.VehiclePhotos = data.Item3;
                foreach (VehiclePhotoViewModel photo in inspection.VehiclePhotos)
                {
                    photo.Url = appContext.MapUrl($@"{appContext.VehiclePhotosFolder}/{appContext.CurrentIdentity.TenantId}/{photo.FileName}");
                    photo.ContentPath = $@"{appContext.VehiclePhotosFolder}/{appContext.CurrentIdentity.TenantId}/{photo.FileName}";
                }
                inspection.DamageIndications = data.Item4;
            }
            else
            {
                inspection.Id = 0;
            }
            if (data.Item1.Count > 0 && data.Item2.Count > 0)
            {
                inspection.DamageIndicationTypes = data.Item1;
                inspection.ViewPoints = data.Item2;
            }
            return inspection;
        }
        
        public void ResetInspection(long inspectionId)
        {
            var parameters = new List<SqlParameter> {new SqlParameter("@Id", inspectionId)};
            SPExecuteReader<AppointmentViewModel>(SPNames.InspectionReset, parameters);
        }

        public Inspection GetById(long id)
        {
            return FirstOrDefault(x => x.Id == id);
        }

        public void SetAppointmentToInspection(long inspectionId, long appointmentPresentationId)
        {
            var parameters = new List<SqlParameter> { new SqlParameter("@InspectionId", inspectionId), new SqlParameter("@AppointmentPresentationId", appointmentPresentationId) };
            SPExecuteReader(SPNames.SetAppointmentToInspection, parameters: parameters);
        }

    }
}
