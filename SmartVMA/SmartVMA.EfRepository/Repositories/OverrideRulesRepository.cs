﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.OutputModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using SmartVMA.Core;
using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.Extensions;
using SmartVMA.Resources;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class OverrideRulesRepository : BaseTenantRepository<OverrideRule, long>, IOverrideRulesRepository
    {
        public OverrideRulesRepository(Models dbContext, IAppContext appContext) : base(dbContext, appContext)
        {
        }

        public ConfigurationSearchByOverridesViewModel GetOverrideRuleById(long? id)
        {
            ConfigurationSearchByOverridesViewModel response = new ConfigurationSearchByOverridesViewModel();
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@Id", id));
            string[] tableNames = {"tbYears", "tbMakes", "tbModels", "tbEngines", "tbTransmission", "tbDriveLine", "tbResults"};
            var resultsById = SPExecuteDataSet(SPNames.OverrideRuleGetById, parameters, tableNames: tableNames);

            VehicleFilterViewModel filter = new VehicleFilterViewModel();
            ICarService carService = IocManager.Resolve<ICarService>();
            if (id.HasValue)
            {
                if (resultsById.Tables[tableNames[0]] != null && resultsById.Tables[tableNames[0]].Rows.Count > 0)
                {
                    filter.Years = resultsById.Tables[tableNames[0]].Rows[0][0].ToString()
                        .Split(new[] {StaticSettings.Delimiter}, StringSplitOptions.RemoveEmptyEntries)
                        .Select(int.Parse)
                        .ToList();
                }
                if (resultsById.Tables[tableNames[1]] != null && resultsById.Tables[tableNames[1]].Rows.Count > 0)
                {
                    filter.Makes = resultsById.Tables[tableNames[1]].Rows[0][0].ToString()
                        .Split(new[] {StaticSettings.Delimiter}, StringSplitOptions.RemoveEmptyEntries)
                        .Select(int.Parse)
                        .ToList();
                }
                if (resultsById.Tables[tableNames[2]] != null && resultsById.Tables[tableNames[2]].Rows.Count > 0)
                {
                    filter.Models = resultsById.Tables[tableNames[2]].Rows[0][0].ToString()
                        .Split(new[] {StaticSettings.Delimiter}, StringSplitOptions.RemoveEmptyEntries)
                        .Select(int.Parse)
                        .ToList();
                }
                if (resultsById.Tables[tableNames[3]] != null && resultsById.Tables[tableNames[3]].Rows.Count > 0)
                {
                    filter.EngineTypes = resultsById.Tables[tableNames[3]].Rows[0][0].ToString()
                        .Split(new[] {StaticSettings.Delimiter}, StringSplitOptions.RemoveEmptyEntries)
                        .Select(int.Parse)
                        .ToList();
                }
                if (resultsById.Tables[tableNames[4]] != null && resultsById.Tables[tableNames[4]].Rows.Count > 0)
                {
                    filter.TransmissionTypes = resultsById.Tables[tableNames[4]].Rows[0][0].ToString()
                        .Split(new[] {StaticSettings.Delimiter}, StringSplitOptions.RemoveEmptyEntries)
                        .Select(int.Parse)
                        .ToList();
                }
                if (resultsById.Tables[tableNames[5]] != null && resultsById.Tables[tableNames[5]].Rows.Count > 0)
                {
                    filter.DriveLines = resultsById.Tables[tableNames[5]].Rows[0][0].ToString()
                        .Split(new[] {StaticSettings.Delimiter}, StringSplitOptions.RemoveEmptyEntries)
                        .Select(int.Parse)
                        .ToList();
                }
            }
            filter.PreserveNA = true;
            response.FilterResponse = carService.GetVehicleFilter(filter);
            response.Filter = filter;
            foreach (DataRow row in resultsById.Tables["tbResults"].Rows)
            {
                response.Id = (long)row["Id"];
                response.RuleName = row["Name"].ToString();
                response.Keyword = row["Keyword"].ToString();
                response.PartNumber = row["PartNo"].ToString();
                response.OverridePartNumber = row["PartNoOverride"].ToString();
                response.OriginalPartNumber = row["PartNoOriginal"].ToString();
                response.PartNumberChecked = !string.IsNullOrEmpty(response.PartNumber) || !string.IsNullOrEmpty(response.OverridePartNumber);
                response.OpCode = row["OpCode"].ToString();
                response.OverrideOpcode = row["OpCodeOverride"].ToString();
                response.OverrideOpcodeChecked = !string.IsNullOrEmpty(response.OpCode) || !string.IsNullOrEmpty(response.OverrideOpcode);
                response.LaborHourPercentage = row.GetValue<int>("LaborHourPercentage");
                response.LaborHourPercentageChecked = response.LaborHourPercentage != null;
                response.LaborHourAmount = row.GetValue<decimal>("LaborHourAmount");
                response.LaborHourAmountChecked = response.LaborHourAmount != null;
                response.LaborHourChecked = response.LaborHourPercentage != null || response.LaborHourAmount != null;
                response.LaborPricePercentage = row.GetValue<int>("LaborPricePercentage");
                response.LaborPricePercentageChecked = response.LaborPricePercentage != null;
                response.LaborPriceAmount = row.GetValue<decimal>("LaborPriceAmount");
                response.LaborPriceAmountChecked = response.LaborPriceAmount != null;
                response.LaborPriceChecked = response.LaborPricePercentage != null || response.LaborPriceAmount != null;
                response.PartPricePercentage = row.GetValue<int>("PartPricePercentage");
                response.PartPricePercentageChecked = response.PartPricePercentage != null;
                response.PartPriceAmount = row.GetValue<decimal>("PartPriceAmount");
                response.PartPriceAmountChecked = response.PartPriceAmount != null;
                response.PartPriceChecked = response.PartPricePercentage != null || response.PartPriceAmount != null;
            }

            //response.Overrides = SearchOverrides(response.Years.Select(c => c.Id).ToList(), response.Makes.Select(c => c.Id).ToList(), response.Models.Select(c => c.Id).ToList(), response.EngineTypes.Select(c => c.Id).ToList(), response.TransmissionTypes.Select(c => c.Id).ToList(), response.DriveLines.Select(c => c.Id).ToList(), response.Steerings.Select(c => c.Id).ToList(), response.OpCode, response.PartNumber, response.Keyword, ",", 5, 15);

            return response;
        }

        public List<ConfigurationOverridesSearchResults> SearchOverrides(ConfigurationSearchByOverridesViewModel model, int pageNumber, int pageSize)
        {
            List<ConfigurationOverridesSearchResults> searchOverrides = new List<ConfigurationOverridesSearchResults>();
            
            var parameters = new List<SqlParameter>();
            long? ruleId = null;
            if (model.Id != 0)
            {
                ruleId = model.Id;
            }
            parameters.Add(SqlParameter("@RuleId", ruleId));
            AddFilterParams(parameters, model);
            parameters.Add(new SqlParameter("@PageNumber", pageNumber));
            parameters.Add(new SqlParameter("@PageSize", pageSize));

            var searchResults = SPExecuteDataSet(SPNames.OverrideRuleSearch, parameters: parameters, tableNames: new[] { "tbSearchServices", "tbParts", "tbGeneralInfo", "tbServicesList" });

            ConfigurationOverridesSearchResults searchResult = null;
            foreach (DataRow row in searchResults.Tables["tbSearchServices"].Rows)
            {
                searchResult = new ConfigurationOverridesSearchResults();
                searchResult.PartInfo = new List<ConfigurationOverridesSearchPartsResults>();
                searchResult.Id = (int)row["OemBasicServiceId"];
                searchResult.Description = row["Description"].ToString();
                searchResult.IsLof = (bool)row["IsLOF"];
                searchResult.Checked = (bool)row["Checked"];
                searchResult.Opcode = (row["OpCode"] ?? string.Empty).ToString();
                searchResult.LaborTime = (row["LaborTime"] ?? 0).ToString();
                searchResult.LaborPrice = (row["LaborPrice"] ?? 0).ToString();
                searchResult.RowId = StaticSettings.DatatableRowId + row["OemBasicServiceId"].ToString();
                searchOverrides.Add(searchResult);
            }

            foreach (DataRow row in searchResults.Tables["tbParts"].Rows)
            {
                int serviceId = (int)row["OemBasicServiceId"];
                searchResult = searchOverrides.Where(c => c.Id == serviceId).FirstOrDefault();
                ConfigurationOverridesSearchPartsResults partInfo = null;
                if (searchResult != null)
                {
                    partInfo = new ConfigurationOverridesSearchPartsResults();
                    partInfo.PartNumber = (row["PartNo"] ?? string.Empty).ToString();
                    partInfo.PartPrice = (row["PartPrice"] ?? 0).ToString();
                    partInfo.PartQuantity = (row["PartQuantity"] ?? 0).ToString();
                    partInfo.PartType = (row["PartType"] ?? string.Empty).ToString();

                    searchResult.PartInfo.Add(partInfo);
                }
            }
            
            if (searchResults.Tables["tbGeneralInfo"].Rows.Count > 0)
            {
                model.FilteredServicesCount = (long)searchResults.Tables["tbGeneralInfo"].Rows[0]["FilteredServicesCount"];
                model.TotalServicesCount = (long)searchResults.Tables["tbGeneralInfo"].Rows[0]["TotalServicesCount"];
                model.AreServicesChecked = (bool)searchResults.Tables["tbGeneralInfo"].Rows[0]["AreServicesChecked"];
            }

            model.OemBasicServices = new List<ConfigurationOverrideListModel>();
            foreach (DataRow row in searchResults.Tables["tbServicesList"].Rows)
            {
                ConfigurationOverrideListModel item = new ConfigurationOverrideListModel
                {
                    Id = (int)row["OemBasicServiceId"],
                    Name = row["OemBasicServiceId"].ToString(),
                    Selected = (bool) row["Checked"]
                };

                model.OemBasicServices.Add(item);
            }

            return searchOverrides;
        }

        public IEnumerable<OverrideRule> GetOverrideRules()
        {
            return GetDbSet();
        }

        public int GetPagedResultsTotal(PagingRequest request)
        {
            var result = GetDbSet();
            return result.Count();
        }

        public IEnumerable<OverrideRule> GetPagedResults(PagingRequest request)
        {
            var result = GetDbSet();
                result = result.OrderByDescending(x => x.Id).Skip(request.Start).Take(request.Length);
            return result;
        }

        public List<OverrideRuleConflictsModel> GetConflicts(ConfigurationSearchByOverridesViewModel model, string basicServices, bool areServicesChecked)
        {
            var RuleConflicts = new List<OverrideRuleConflictsModel>();
            var parameters = new List<SqlParameter>();
            AddFilterParams(parameters, model);
            AddRuleParams(parameters, model, basicServices, areServicesChecked);
            var data = SPExecureDSReader<
                OverrideRuleConflictsModel,
                OverrideRuleConflictsServiceDescriptionsModel>
                (SPNames.OverrideRuleCheckConflicts, parameters);

            if (data.Item1.Count > 0)
            {
                RuleConflicts = data.Item1;
                if (data.Item2.Count > 0)
                {
                    foreach (var item in data.Item1)
                    {
                        item.ServiceDescriptions = data.Item2.Where(x => x.RuleId == item.RuleId).ToList();
                    }
                }
            }
            return RuleConflicts;
        }

        public ServiceResponse ApplyOverride(ConfigurationSearchByOverridesViewModel model, string basicServices, bool areServicesChecked, string ruleIdsToDelete)
        {
            var response = new ServiceResponse();
            var parameters = new List<SqlParameter>();
            parameters.Add(SqlParameter("@RuleIdsToDelete", ruleIdsToDelete));
            parameters.Add(SqlParameter("@Name", model.RuleName));
            AddFilterParams(parameters, model);
            AddRuleParams(parameters, model, basicServices, areServicesChecked);
            using (var dbContextTransaction = _dbContext.Database.BeginTransaction())
            {
                try
                {
                    var data = SPExecuteNonQuery(SPNames.OverrideRuleApply, parameters, null, null, null, dbContextTransaction.UnderlyingTransaction);
                    dbContextTransaction.Commit();
                    
                }
                catch (Exception e)
                {
                    response.Errors.Add(Messages.OverrideRuleApplyFailed, Messages.OverrideRuleApplyFailed + e.Message);
                    dbContextTransaction.Rollback();
                    throw;
                }
                finally
                {
                    response.IsSuccess = !response.Errors.Any();
                }

                return response;
            }
        }

        public ServiceResponse DeleteMany(string ruleIds)
        {
            var response = new ServiceResponse();
            var parameters = new List<SqlParameter>
            {
                SqlParameter("@RuleIds", ruleIds),
                SqlParameter("@Delimiter", StaticSettings.Delimiter)
            };
            try
            {
                SPExecuteNonQuery(SPNames.OverrideRulesDelete, parameters);
            }
            catch (Exception e)
            {
                response.Errors.Add(Messages.OverrideRuleDeleteFailed, Messages.OverrideRuleDeleteFailed + e.Message);
                throw;
            }
            finally
            {
                response.IsSuccess = !response.Errors.Any();
            }
            //TO-DO make transaction
            return response;
        }

        #region Private Methods

        private void AddFilterParams(List<SqlParameter> parameters, ConfigurationSearchByOverridesViewModel model)
        {
            parameters.Add(SqlParameter("@VehicleYears", string.Join(StaticSettings.Delimiter, model.Filter.Years)));
            parameters.Add(SqlParameter("@VehicleMakes", string.Join(StaticSettings.Delimiter, model.Filter.Makes)));
            parameters.Add(SqlParameter("@VehicleModels", string.Join(StaticSettings.Delimiter, model.Filter.Models)));
            parameters.Add(SqlParameter("@VehicleEngines", string.Join(StaticSettings.Delimiter, model.Filter.EngineTypes)));
            parameters.Add(SqlParameter("@VinMasterTransmissions", string.Join(StaticSettings.Delimiter, model.Filter.TransmissionTypes)));
            parameters.Add(SqlParameter("@VinMasterDriveLines", string.Join(StaticSettings.Delimiter, model.Filter.DriveLines)));
            parameters.Add(SqlParameter("@VehicleSteerings", null));
            parameters.Add(SqlParameter("@OpCode", model.OpCode));
            parameters.Add(SqlParameter("@PartNo", model.PartNumber));
            parameters.Add(SqlParameter("@Keyword", model.Keyword));
            parameters.Add(SqlParameter("@Delimiter", StaticSettings.Delimiter));
        }

        private void AddRuleParams(List<SqlParameter> parameters, ConfigurationSearchByOverridesViewModel model, string basicServices, bool areServicesChecked)
        {
            long? ruleId = null;
            if (model.Id != 0)
            {
                ruleId = model.Id;
            }
            parameters.Add(SqlParameter("@RuleId", ruleId, SqlDbType.BigInt));
            parameters.Add(SqlParameter("@LaborHourPercentage", model.LaborHourPercentage, SqlDbType.Int));
            parameters.Add(SqlParameter("@LaborHourAmount", model.LaborHourAmount, SqlDbType.Money));
            parameters.Add(SqlParameter("@LaborPricePercentage", model.LaborPricePercentage, SqlDbType.Int));
            parameters.Add(SqlParameter("@LaborPriceAmount", model.LaborPriceAmount, SqlDbType.Money));
            parameters.Add(SqlParameter("@PartPricePercentage", model.PartPricePercentage, SqlDbType.Int));
            parameters.Add(SqlParameter("@PartPriceAmount", model.PartPriceAmount, SqlDbType.Money));
            parameters.Add(SqlParameter("@OpCodeOverride", model.OverrideOpcode, SqlDbType.NVarChar));
            parameters.Add(SqlParameter("@PartNoOriginal", model.OriginalPartNumber, SqlDbType.NVarChar));
            parameters.Add(SqlParameter("@PartNoOverride", model.OverridePartNumber, SqlDbType.NVarChar));
            parameters.Add(SqlParameter("@BasicServices", basicServices, SqlDbType.NVarChar));
            parameters.Add(SqlParameter("@AreServicesChecked", areServicesChecked, SqlDbType.Bit));
        }

        #endregion
    }
}
