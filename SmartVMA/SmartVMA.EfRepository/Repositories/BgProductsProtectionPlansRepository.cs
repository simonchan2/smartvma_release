﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class BgProductsProtectionPlansRepository : BaseRepository<BgProductsProtectionPlan, int>, IBgProductsProtectionPlansRepository
    {
        public BgProductsProtectionPlansRepository(Models dbContext, IAppContext appContext) : base(dbContext)
        {
        }
    }
}
