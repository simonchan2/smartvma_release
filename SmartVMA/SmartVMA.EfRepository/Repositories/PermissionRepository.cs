﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.Ioc;
using System.Collections.Generic;
using System.Linq;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class PermissionRepository : BaseRepository<Permission, int>, IPermissionRepository
    {
        private readonly IUserCompanyRoleRepository _userCompanyRoleRepository;
        public PermissionRepository(Models dbContext, IUserCompanyRoleRepository userCompanyRoleRepository) : base(dbContext)
        {
            _userCompanyRoleRepository = userCompanyRoleRepository;
        }

        public IEnumerable<Permission> GetPermissionsWithRoleInfo()
        {
            return GetDbSet("RolePermissions");
        }

        public bool IsPermissionGranted(string permissionName, int? userId, int? roleId, int? tenantId)
        {
            // TODO: make additional check whether the current user for the current Comppany has the right role
            //if (!_userCompanyRoleRepository.Find(x => x.UserId == userId && x.RoleId == roleId && x.TenantId == tenantId).Any())
            //{
            //    return false;
            //}
            return GetDbSet("RolePermissions").Any(x => x.Name == permissionName && x.RolePermissions.Any(y => y.RoleId == roleId));
        }

        public int GetPagedResultsTotal(PagingRequest request)
        {
            var result = GetDbSet("RolePermissions");
            if (request.Search != null && !string.IsNullOrEmpty(request.Search.Value))
            {
                result = result.Where(x => x.Name.ToLower().Contains(request.Search.Value.ToLower()) || x.Displayname.ToLower().Contains(request.Search.Value.ToLower()) || x.Description.ToLower().Contains(request.Search.Value.ToLower()));
            }
            return result.Count();
        }

        public IEnumerable<Permission> GetPagedResults(PagingRequest request)
        {
            var result = GetDbSet("RolePermissions");

            var columnsSort = new Dictionary<int, string> {
                {0, "Name"},
                {1, "Displayname"},
                {2, "Description"}
            };

            if (request.Search != null && !string.IsNullOrEmpty(request.Search.Value))
            {
                result = result.Where(x => x.Name.ToLower().Contains(request.Search.Value.ToLower()) || x.Displayname.ToLower().Contains(request.Search.Value.ToLower()) || x.Description.ToLower().Contains(request.Search.Value.ToLower()));
            }

            var order = request.Order.FirstOrDefault();
            if (order.Dir == "asc")
            {
                result = result.OrderByTest(columnsSort[order.Column]).Skip(request.Start).Take(request.Length);
            }
            else
            {
                result = result.OrderByDescendingTest(columnsSort[order.Column]).Skip(request.Start).Take(request.Length);
            }
            return result;
        }
    }
}
