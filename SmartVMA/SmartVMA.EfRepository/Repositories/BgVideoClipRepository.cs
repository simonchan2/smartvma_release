﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.Ioc;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class BgVideoClipRepository : BaseRepository<BgVideoClip, int>, IBgVideoClipsRepository
    {
        public BgVideoClipRepository(Models dbContext) : base(dbContext)
        {
        }
    }
}
