﻿using System;
using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.Ioc;
using System.Data.Entity;
using System.Linq;
using SmartVMA.Core;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class PasswordResetRequestRepository : BaseRepository<PasswordResetRequest, long>, IPasswordResetRequestRepository
    {
        public PasswordResetRequestRepository(Models dbContext) : base(dbContext)
        {
        }

        public PasswordResetRequest GetRequestByToken(Guid token)
        {
            return Find(x => x.Token == token
                         && DbFunctions.AddHours(x.TimeCreated, StaticSettings.PasswordExpirationTimeInHours) > DateTime.UtcNow)
                  .LastOrDefault();
        }
    }
}
