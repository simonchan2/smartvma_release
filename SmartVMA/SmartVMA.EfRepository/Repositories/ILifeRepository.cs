﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class ILifeRepository : BaseRepository<ILife, int>, IILifeRepository
    {
        public ILifeRepository(Models dbContext) : base(dbContext)
        {
        }      
    }
}
