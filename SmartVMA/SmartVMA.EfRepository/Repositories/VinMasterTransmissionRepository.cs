﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class VinMasterTransmissionRepository : BaseRepository<VinMasterTransmission, int>, IVinMasterTransmissionRepository
    {
        public VinMasterTransmissionRepository(Models dbContext, IAppContext appContext) : base(dbContext)
        {
        }

        public string GetVinMasterTransmission(int vinMasterTransmissionId)
        {
            var vinmasterTransmission = GetDbSet().FirstOrDefault(c => c.Id == vinMasterTransmissionId);

            return vinmasterTransmission != null ? vinmasterTransmission.Transmission : string.Empty;
        }
    }
}
