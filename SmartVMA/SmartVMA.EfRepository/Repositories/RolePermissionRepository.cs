﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.Ioc;
using System.Collections.Generic;
using System.Linq;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class RolePermissionRepository : BaseRepository<RolePermission, long>, IRolePermissionRepository
    {
        public RolePermissionRepository(Models dbContext) : base(dbContext)
        {
        }

        public IEnumerable<RolePermission> GetRoleWithPermissions(int roleId)
        {
            return GetDbSet("Permission").Where(x => x.RoleId == roleId);
        }
    }
}
