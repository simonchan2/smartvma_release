﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.Repositories;
using SmartVMA.Core.Services;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class UserRepository : BaseRepository<User, long>, IUserRepository
    {
        private readonly IUserSettingRepository _userSettingRepository;
        private readonly IRoleRepository _repository;

        public UserRepository(Models dbContext, IUserSettingRepository userSettingRepository) : base(dbContext)
        {
            _userSettingRepository = userSettingRepository;
        }

        public IEnumerable<User> GetAllUsers(int? companyId = null)
        {
            var appContext = IocManager.Resolve<IAppContext>();
            var currentIdentiy = appContext.CurrentIdentity;
            var result = GetDbSet("UserCompanyRoles", "UserCompanyRoles.Company", "UserCompanyRoles.Role");
            if (currentIdentiy.RoleId != (int)UserRoles.SystemAdministrator)
            {
                result = result.Where(x => x.UserCompanyRoles.Any(y => y.Company != null && (y.Company.Id == currentIdentiy.TenantId) || y.Company.ParentId == currentIdentiy.TenantId));
            }
            if (companyId.HasValue)
            {
                return result.Where(x => x.UserCompanyRoles.Any(y => y.TenantId == companyId));
            }
            return result;
        }

        public User GetDefaultUserForCompany(int companyId, UserRoles userRole)
        {
            return GetDbSet("UserCompanyRoles", "UserCompanyRoles.Company", "UserCompanyRoles.Role").FirstOrDefault(x => x.UserCompanyRoles.Any(y => y.TenantId == companyId && y.RoleId == (int)userRole));
        }

        public IEnumerable<User> GetAllUsersByRole(int companyId, UserRoles userRole)
        {
            var result = GetDbSet("UserCompanyRoles");
            result = result.Where(x => x.UserCompanyRoles.Any(y => y.Company != null && ((y.Company.Id == companyId) || y.Company.ParentId == companyId) && y.RoleId == (int)userRole));
            return result;
        }

        public User GetUserByEmail(string email)
        {
            return GetDbSet("UserCompanyRoles").FirstOrDefault(x => x.Email == email);
        }

        public User GetUserById(long id)
        {
            return GetDbSet("UserCompanyRoles", "UserCompanyRoles.Company").FirstOrDefault(x => x.Id == id);
        }

        public User GetUserAndSettings(long id)
        {
            return GetDbSet("UserSettings").FirstOrDefault(x => x.Id == id);
        }

        public void Insert(User user, IEnumerable<UserSetting> settings)
        {
            Insert(user);
            foreach (var setting in settings)
            {
                _userSettingRepository.Insert(setting);
                user.UserSettings.Add(setting);
            }
        }

        public void Update(User user, IEnumerable<UserSetting> settings)
        {
            Update(user);
            foreach (var item in settings)
            {
                var setting = user.UserSettings.FirstOrDefault(x => x.Name == item.Name);
                if (setting != null)
                {
                    setting.Value = item.Value;
                    _userSettingRepository.Update(setting);
                }
                else
                {
                    _userSettingRepository.Insert(item);
                    user.UserSettings.Add(item);
                }
            }
        }

        public int GetPagedResultsTotal(PagingRequest request, int? companyId = null)
        {
            var appContext = IocManager.Resolve<IAppContext>();
            var currentIdentiy = appContext.CurrentIdentity;
            var result = GetDbSet("UserCompanyRoles", "UserCompanyRoles.Company", "UserCompanyRoles.Role");
            if (currentIdentiy.RoleId != (int)UserRoles.SystemAdministrator)
            {
                var service = IocManager.Resolve<IRoleService>();
                var model = service.GetIndexViewModel();
                var userIds = new List<long>();

                foreach (var item in model.Items.Distinct())
                {
                    userIds.AddRange(result.Where(x => x.UserCompanyRoles.Any(y => y.Company != null
                                                       && y.RoleId >= item.ParentId
                                                       && (y.Company.Id == currentIdentiy.TenantId) || y.Company.ParentId == currentIdentiy.TenantId)).Select(x => x.Id));
                }
                result = result.Where(x => userIds.Any(y => y == x.Id));
            }
            if (companyId.HasValue)
            {
                result = result.Where(x => x.UserCompanyRoles.Any(y => y.TenantId == companyId));
            }
            if (!string.IsNullOrEmpty(request.Search?.Value))
            {
                var searchValues = request.Search.Value.Split(' ');
                if (searchValues.Length == 2)
                {
                    var firstName = searchValues[0].ToLower().Trim();
                    var lastName = searchValues[1].ToLower().Trim();

                    result = result.Where(x => x.UserCompanyRoles.FirstOrDefault().Company.CompanyName.ToLower().Contains(request.Search.Value.ToLower())
                                        || x.UserCompanyRoles.FirstOrDefault().Role.Description.ToLower().Contains(request.Search.Value.ToLower())
                                        || (x.FirstName.ToLower().Contains(firstName)
                                            && x.LastName.ToLower().StartsWith(lastName)));
                }
                else
                {
                    result = result.Where(x => x.UserCompanyRoles.FirstOrDefault().Company.CompanyName.ToLower().Contains(request.Search.Value.ToLower())
                                            || x.UserCompanyRoles.FirstOrDefault().Role.Description.ToLower().Contains(request.Search.Value.ToLower())
                                            || x.FirstName.ToLower().Contains(request.Search.Value.ToLower())
                                            || x.LastName.ToLower().Contains(request.Search.Value.ToLower()));
                }
            }
            return result.Count();
        }

        public IEnumerable<User> GetPagedResults(PagingRequest request, int? companyId = null)
        {
            var appContext = IocManager.Resolve<IAppContext>();
            var currentIdentiy = appContext.CurrentIdentity;
            var result = GetDbSet("UserCompanyRoles", "UserCompanyRoles.Company", "UserCompanyRoles.Role");
            if (currentIdentiy.RoleId != (int)UserRoles.SystemAdministrator)
            {
                var service = IocManager.Resolve<IRoleService>();
                var model = service.GetIndexViewModel();
                var userIds = new List<long>();

                foreach (var item in model.Items.Distinct())
                {
                    userIds.AddRange(result.Where(x => x.UserCompanyRoles.Any(y => y.Company != null
                                                       && y.RoleId >= item.ParentId
                                                       && (y.Company.Id == currentIdentiy.TenantId) || y.Company.ParentId == currentIdentiy.TenantId)).Select(x => x.Id));
                }
                result = result.Where(x => userIds.Any(y => y == x.Id));
            }
            if (companyId.HasValue)
            {
                result = result.Where(x => x.UserCompanyRoles.Any(y => y.TenantId == companyId));
            }
            if (!string.IsNullOrEmpty(request.Search?.Value))
            {
                var searchValues = request.Search.Value.Split(' ');
                if (searchValues.Length == 2)
                {
                    var firstName = searchValues[0].ToLower().Trim();
                    var lastName = searchValues[1].ToLower().Trim();

                    result = result.Where(x => x.UserCompanyRoles.FirstOrDefault().Company.CompanyName.ToLower().Contains(request.Search.Value.ToLower())
                                        || x.UserCompanyRoles.FirstOrDefault().Role.Description.ToLower().Contains(request.Search.Value.ToLower())
                                        || (x.FirstName.ToLower().Contains(firstName)
                                            && x.LastName.ToLower().StartsWith(lastName)));
                }
                else
                {
                    result = result.Where(x => x.UserCompanyRoles.FirstOrDefault().Company.CompanyName.ToLower().Contains(request.Search.Value.ToLower())
                                            || x.UserCompanyRoles.FirstOrDefault().Role.Description.ToLower().Contains(request.Search.Value.ToLower())
                                            || x.FirstName.ToLower().Contains(request.Search.Value.ToLower())
                                            || x.LastName.ToLower().Contains(request.Search.Value.ToLower()));
                }
            }
            var columnsSort = new Dictionary<int, Expression<Func<User, string>>> {
                {0, x => x.UserCompanyRoles.FirstOrDefault().Company.CompanyName},
                {1, x => x.FirstName},
                {2, x => x.UserCompanyRoles.FirstOrDefault().Role.Description}
            };
            var order = request.Order.FirstOrDefault();
            if (order != null)
            {
                var orderByExpression = columnsSort[order.Column];
                result = order.Dir == "asc" 
                    ? result.OrderBy(orderByExpression).Skip(request.Start).Take(request.Length) 
                    : result.OrderByDescending(orderByExpression).Skip(request.Start).Take(request.Length);
            }
            return result;
        }
    }
}
