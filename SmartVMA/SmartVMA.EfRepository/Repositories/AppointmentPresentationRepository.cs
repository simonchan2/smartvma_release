﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using SmartVMA.Core;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class AppointmentPresentationRepository : BaseTenantRepository<AppointmentPresentation, long>, IAppointmentPresentationRepository
    {
        public AppointmentPresentationRepository(Models dbContext, IAppContext appContext) : base(dbContext, appContext)
        {
        }

        public AppointmentPresentation GetAppointmentPresentationWithVehiclesById(long appointmentPresentationId)
        {
            string[] includereference = new string[3] { "DealerVehicle", "DealerVehicle.Vehicle", "DealerVehicle.Vehicle.Car",};
            return GetDbSet(includereference).FirstOrDefault(c => c.Id == appointmentPresentationId);
        }

        public AppointmentPresentation GetAppointmentPresentationWithServicesById(long appointmentPresentationId)
        {
            return GetDbSet("AppointmentServices").FirstOrDefault(c => c.Id == appointmentPresentationId);
        }

        public AppointmentPresentation GetAppointmentWithServicesAndPartsById(long appointmentPresentationId)
        {
            return GetDbSet("AppointmentServices.AppointmentServiceParts").FirstOrDefault(c => c.Id == appointmentPresentationId);
        }
        
        public IEnumerable<AppointmentPresentation> GetFullAppointmentPresentationList(AppointmentPresentationStatus statusType, DateTime? startDate, DateTime? endDate, DateTime? parkedMenuRetentionDuration, List<long> advisorIds = null, int? timeframeId = null)
        {
            if (timeframeId == (int)TimeFrames.NotApplicable && startDate == null && endDate == null)
            {
                return null;
            }

            string[] includereference = new string[10] { "User", "DealerCustomer", "DealerCustomer.CountryState", "DealerCustomer.Country", "DealerVehicle", "DealerVehicle.Vehicle", "DealerVehicle.Vehicle.Car", "DealerVehicle.Vehicle.Car.VehicleMake", "DealerVehicle.Vehicle.Car.VehicleModel", "DealerVehicle.Vehicle.Car.VehicleEngine" };
            var presentationList = GetDbSet(includereference);

            switch (statusType)
            {
                case AppointmentPresentationStatus.MC:
                    presentationList = from c in presentationList
                                       where c.PresentationStatus == AppointmentPresentationStatus.MC.ToString()
                                       select c;
                    if (startDate != null)
                    {
                        presentationList = from c in presentationList
                                           where c.DateRoCreated != null && DbFunctions.TruncateTime(c.DateRoCreated) >= DbFunctions.TruncateTime(startDate)
                                           select c;
                    }
                    if (endDate != null)
                    {
                        presentationList = from c in presentationList
                                           where c.DateRoCreated != null && DbFunctions.TruncateTime(c.DateRoCreated) <= DbFunctions.TruncateTime(endDate)
                                           select c;
                    }
                    break;
                case AppointmentPresentationStatus.MK:
                    presentationList = from c in presentationList
                                       where c.PresentationStatus == AppointmentPresentationStatus.MK.ToString()
                                       select c;
                    if (startDate != null)
                    {
                        presentationList = from c in presentationList
                                           where c.ParkedTime != null && DbFunctions.TruncateTime(c.ParkedTime) >= DbFunctions.TruncateTime(startDate)
                                           select c;
                    }
                    if (endDate != null)
                    {
                        presentationList = from c in presentationList
                                           where c.ParkedTime != null && DbFunctions.TruncateTime(c.ParkedTime) <= DbFunctions.TruncateTime(endDate)
                                           select c;
                    }
                    if(parkedMenuRetentionDuration != null)
                    presentationList = from c in presentationList
                                       where c.ParkedTime != null && DbFunctions.TruncateTime(c.ParkedTime) >= DbFunctions.TruncateTime(parkedMenuRetentionDuration)
                                       select c;
                    break;
            }

            if (advisorIds != null && advisorIds.Count > 0)
            {
                presentationList = from c in presentationList
                                   where c.AdvisorId.HasValue && advisorIds.Contains(c.AdvisorId.Value)
                                   select c;
            }

            return presentationList;
        }
        
        public List<AdvisorInfo> GetAdvisorList()
        {
            List<AdvisorInfo> advisors = SPExecuteReader<AdvisorInfo>("GetAdvisors");

            return advisors;
        }

        public long? GetAppointmentPresentationId(int year, int make, int model, int engine, string transmission, string driveLine, int mileage)
        {
            string[] includereference = new string[4] { "DealerVehicle", "DealerVehicle.Vehicle", "DealerVehicle.Vehicle.VinMaster", "DealerVehicle.Vehicle.Car" };
            var appointmentPresentation = (from c in GetDbSet(includereference)
                                           where c.Mileage == mileage
                                           && c.DealerVehicle != null
                                           && c.DealerVehicle.Vehicle != null
                                           && c.DealerVehicle.Vehicle.Car != null
                                           && c.DealerVehicle.Vehicle.Car.VehicleYear == year
                                           && c.DealerVehicle.Vehicle.Car.VehicleMakeId == make
                                           && c.DealerVehicle.Vehicle.Car.VehicleModelId == model
                                           && c.DealerVehicle.Vehicle.Car.VehicleEngineId == engine
                                           && c.DealerVehicle.Vehicle.VinMaster != null
                                           && c.DealerVehicle.Vehicle.VinMaster.VehicleTransmission != null
                                           && c.DealerVehicle.Vehicle.VinMaster.VehicleTransmission.ToLower() == transmission.ToLower()
                                           && c.DealerVehicle.Vehicle.VinMaster.VehicleDriveLine != null
                                           && c.DealerVehicle.Vehicle.VinMaster.VehicleDriveLine.ToLower() == driveLine.ToLower()
                                           select c).FirstOrDefault();

            return appointmentPresentation != null ? (long?)appointmentPresentation.Id : null;
        }

       
    }
}
