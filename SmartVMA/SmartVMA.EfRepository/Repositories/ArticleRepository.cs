﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class ArticleRepository : BaseRepository<Article, int>, IArticleRepository
    {
        public ArticleRepository(Models dbContext) : base(dbContext)
        {
        }
        //get the "ArticleArticleCategories",  "ArticleCategory" DbSets first, so we can read so properties like:
        //Article.ArticleArticleCategories.ArticleCategoryId or Article.ArticleArticleCategories.ArticleCategory.Name
        //so we can get the Ids and Names of Categories that this Article is in a relationship with
        public Article GetArticleById(int id)
        {
            return GetDbSet("ArticleArticleCategories", "ArticleArticleCategories.ArticleCategory").FirstOrDefault(x => x.Id == id && x.TimeDeleted == null);
        }

        //same here, only it's used for getting all Articles
        public IEnumerable<Article> GetAllArticles()
        {
            return GetDbSet("ArticleArticleCategories", "ArticleArticleCategories.ArticleCategory").Where(x => x.TimeDeleted == null).AsEnumerable();
        }

        public IEnumerable<Article> GetArticleByKeyWordAndCategoryId(string key, int[] categoryIds)
        {
            var articles = GetDbSet("ArticleArticleCategories", "ArticleArticleCategories.ArticleCategory").Where(x => x.TimeDeleted == null && x.IsActive == true).AsEnumerable();
            if (!String.IsNullOrEmpty(key))
            {
                articles = articles.Where(x => x.Title.ToLower().Contains(key.ToLower()) || x.Description.ToLower().Contains(key.ToLower()));
            }
            if (categoryIds != null)
            {
                articles = articles.Where(x => x.ArticleArticleCategories.Any(y => categoryIds.Contains(y.ArticleCategoryId)));
            }

            return articles;
        }

        public int GetPagedResultsTotal(PagingRequest request)
        {
            var result = GetDbSet("ArticleArticleCategories", "ArticleArticleCategories.ArticleCategory").Where(x => x.TimeDeleted == null && x.IsActive == true);
            if (request.Search != null && !string.IsNullOrEmpty(request.Search.Value))
            {
                result = result.Where(x => x.Description.ToLower().Contains(request.Search.Value.ToLower()) || x.Title.ToLower().Contains(request.Search.Value.ToLower()));
            }
            return result.Count();
        }

        public IEnumerable<Article> GetPagedResults(PagingRequest request)
        {
            var result = GetDbSet("ArticleArticleCategories", "ArticleArticleCategories.ArticleCategory")
                .Where(x => x.TimeDeleted == null);
            var sortExpressions = new Dictionary<int, Expression<Func<Article, object>>>
            {
                {0, x => x.Title},
                { 1, x => x.Description},
                { 2, x => x.ArticleArticleCategories.Select(y => y.ArticleCategory.Name).OrderBy(y => y).FirstOrDefault()}
            };

            if (!string.IsNullOrEmpty(request.Search?.Value))
            {
                result = result.Where(x => x.Title.ToLower().Contains(request.Search.Value.ToLower()) || x.ArticleArticleCategories.Any(y => y.ArticleCategory.Name.ToLower().Contains(request.Search.Value.ToLower())));
            }

            var order = request.Order.FirstOrDefault();
            if (order != null)
            {
                result = order.Dir == "asc" 
                    ? result.OrderBy(sortExpressions[order.Column]).Skip(request.Start).Take(request.Length) 
                    : result.OrderByDescending(sortExpressions[order.Column]).Skip(request.Start).Take(request.Length);
            }
            return result;
        }
    }
}
