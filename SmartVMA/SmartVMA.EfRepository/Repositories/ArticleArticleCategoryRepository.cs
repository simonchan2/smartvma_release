﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class ArticleArticleCategoryRepository : BaseRepository<ArticleArticleCategory, int>, IArticleArticleCategoryRepository
    {
        public ArticleArticleCategoryRepository(Models dbContext) : base(dbContext)
        {
        }

        public void Insert(Article article, int categoryId)
        {
            var entity = new ArticleArticleCategory
            {
                ArticleId = article.Id,
                ArticleCategoryId = categoryId,
                
                TimeCreated = DateTime.UtcNow,
                TimeUpdated = DateTime.UtcNow
            };
            article.ArticleArticleCategories.Add(entity);
        }
    }
}
