﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class BgProductsCategoryRepository : BaseRepository<BgProductsCategory, int>, IBgProductsCategoryRepository
    {
        public BgProductsCategoryRepository(Models dbContext, IAppContext appContext) : base(dbContext)
        {
        }

        public List<BgProductsCategory> GetBgProductCategoryById(int id)
        {
            var bgprodCatById = (from c in GetDbSet()
                                where c.BgProductId == id
                                select new BgProductsCategory
                                {
                                    BgProductId = c.BgProductId,
                                    BgProdCategoryId = c.BgProdCategoryId
                                }).ToList();
            return bgprodCatById;
        }


        public void Insert(BgProduct bgproduct, int categoryId)
        {
            var entity = new BgProductsCategory
            {
                BgProductId = bgproduct.Id,
                BgProdCategoryId = categoryId,

                TimeCreated = DateTime.UtcNow,
                TimeUpdated = DateTime.UtcNow
            };
            bgproduct.BgProductsCategories.Add(entity);
        }
    }
}
