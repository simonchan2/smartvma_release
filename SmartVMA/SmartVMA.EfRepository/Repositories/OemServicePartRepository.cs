﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.Ioc;
using System.Collections.Generic;
using System.Linq;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class OemServicePartRepository : BaseRepository<OemServicePart, int>, IOemServicePartRepository
    {
        public OemServicePartRepository(Models dbContext) : base(dbContext)
        {            
        }

        public OemServicePart GetOemServicePartById(int id)
        {
            return GetDbSet("OemPart", "OemBasicService").FirstOrDefault(x => x.Id == id && x.IsDeleted == false);
        }
        
    }
}
