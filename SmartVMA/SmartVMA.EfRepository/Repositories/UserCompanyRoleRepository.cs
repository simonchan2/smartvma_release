﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System;
using System.Linq;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class UserCompanyRoleRepository : BaseTenantRepository<UserCompanyRole, long>, IUserCompanyRoleRepository
    {
        public UserCompanyRoleRepository(Models dbContext, IAppContext appContext) : base(dbContext, appContext)
        {
        }

        public void Insert(User user, int roleId, int tenantId)
        {
            var entity = new UserCompanyRole
            {
                UserId = user.Id,
                RoleId = roleId,
                TenantId = tenantId,
                TimeCreated = DateTime.UtcNow,
                TimeUpdated= DateTime.UtcNow
            };
            _dbContext.Set<UserCompanyRole>().Add(entity);
        }

        public bool IsRoleAlreadySet(User user, int roleId, int tenantId)
        {
            return user.UserCompanyRoles.Any(x => x.RoleId == roleId && x.TenantId == tenantId);
        }

        public UserCompanyRole GetUserRole(User user, int tenantId)
        {
            return user.UserCompanyRoles.FirstOrDefault(x => x.TenantId == tenantId);
        }

        public UserCompanyRole GetRole(long userId, int tenantId)
        {
            return GetDbSet().Where(x => x.TenantId == tenantId && x.UserId == userId).FirstOrDefault();
        }
    }
}
