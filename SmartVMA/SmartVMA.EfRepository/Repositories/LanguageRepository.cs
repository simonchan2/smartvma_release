﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class LanguageRepository : BaseRepository<Language, int>, ILanguageRepository
    {
        private readonly ICompanySettingRepository _companySettingRepository;

        public LanguageRepository(Models dbContext, ICompanySettingRepository companySettingRepository) : base(dbContext)
        {
            _companySettingRepository = companySettingRepository;
        }

        public Language GetLanguage(int id)
        {
            return GetDbSet().FirstOrDefault(x => x.Id == id);
        }
    }
}
