﻿using System;
using System.Collections.Generic;
using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Infrastructure.CacheManager;
using System.Linq;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class UserSettingRepository : BaseRepository<UserSetting, long>, IUserSettingRepository
    {
        //private readonly ICacheManager _cache;
        public UserSettingRepository(Models dbContext, IAppContext appContext) : base(dbContext)
        {
        }

        public string GetSettingValue(UserSettingsNames setting)
        {
            //var cacheKey = string.Format("cSetting_{0}_{1}", _appContext.CurrentIdentity.TenantId, (int)setting);
            //var cachedItem = _cache.Get<string>(cacheKey);
            //if (!string.IsNullOrEmpty(cachedItem))
            //{
            //    return cachedItem;
            //}

            //var value = Find(x => x.Name == setting.ToString()).Select(x => x.Value).FirstOrDefault();
            var appContext = IocManager.Resolve<IAppContext>();
            var userId = appContext.CurrentIdentity.UserId;
            var value = Find(x => x.Name == setting.ToString() && x.User.Id == userId).Select(x => x.Value).FirstOrDefault();
            //if (!string.IsNullOrEmpty(value))
            //{
            //    _cache.Set(cacheKey, value);
            //}
            return value;
        }

        public List<UserSetting> GetAllSettings()
        {
            var appContext = IocManager.Resolve<IAppContext>();
            var userId = appContext.CurrentIdentity.UserId;
            List<UserSetting> allSettings = GetDbSet().Where(x => x.User.Id == userId).ToList();
            return allSettings;
        }
    }
}
