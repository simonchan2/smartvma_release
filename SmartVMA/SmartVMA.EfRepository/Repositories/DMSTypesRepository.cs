﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.Ioc;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class DMSTypesRepository : BaseRepository<DmsTypes, int>, IDMSTypesRepository
    {
        public DMSTypesRepository(Models dbContext) : base(dbContext)
        {
        }
    }
}
