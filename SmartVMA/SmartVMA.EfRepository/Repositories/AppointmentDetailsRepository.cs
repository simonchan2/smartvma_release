﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.Ioc;
using System.Collections.Generic;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class AppointmentDetailsRepository : BaseRepository<AppointmentDetail, long>, IAppointmentDetailsRepository
    {
        public AppointmentDetailsRepository(Models dbContext) : base(dbContext)
        {
        }

        public IEnumerable<AppointmentDetail> GetAppointmentDetailsList(long appointmentID)
        {
            return Find(x => x.AppointmentId == appointmentID);
        }
    }
}
