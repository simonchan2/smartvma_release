﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using SmartVMA.Core;
using SmartVMA.Core.Contracts;
using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Resources;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class AdditionalServiceRepository : BaseTenantRepository<AdditionalService, long>, IAdditionalServiceRepository
    {
        public AdditionalServiceRepository(Models dbContext, IAppContext appContext) : base(dbContext, appContext)
        {
        }

        public IEnumerable<AdditionalService> GetAllServicesAndParts(CustomServiceMaintenancePagingViewModel request, out int totalCount, out string filteredServiceIds)
        {
            var results = GetDbSet("AdditionalServiceParts.BgProduct", "AdditionalServiceVideoClips", "BgProtectionPlan", "BgProtectionSubPlan", "BgProdCategory");
            if (!string.IsNullOrEmpty(request.ServiceKeyword))
            {
                results = results.Where(x => x.OpDescription.Contains(request.ServiceKeyword)
                     || x.Comment.Contains(request.ServiceKeyword)
                     || x.AdditionalServiceParts.Any(y => y.PartNo.Contains(request.ServiceKeyword)
                            || y.Quantity.ToString().Contains(request.ServiceKeyword)
                            || y.UnitPrice.ToString().Contains(request.ServiceKeyword)
                            || (y.BgProduct != null && y.BgProduct.Description.Contains(request.ServiceKeyword)))
                     || (x.IntervalStart.HasValue && x.IntervalStart.ToString().Contains(request.ServiceKeyword))
                     || (x.IntervalRepeat.HasValue && x.IntervalRepeat.ToString().Contains(request.ServiceKeyword))
                     || (x.BgProdCategory != null && x.BgProdCategory.Description.Contains(request.ServiceKeyword))
                     || (x.BgProtectionPlan != null && x.BgProtectionPlan.ProtectionPlanName.Contains(request.ServiceKeyword))
                     || x.AdditionalServiceVideoClips.Any(y => y.BgVideoClip != null && y.BgVideoClip.Description.Contains(request.ServiceKeyword)));
            }

            if (!string.IsNullOrEmpty(request.OpCode))
            {
                results = results.Where(x => x.OpCode.Contains(request.OpCode));
            }

            var order = request.Order?.FirstOrDefault();
            if (order != null)
            {
                switch (order.Column)
                {
                    case 0: results = order.Dir == "asc" ? results.OrderBy(x => x.OpCode) : results.OrderByDescending(x => x.OpCode); break;
                    case 1: results = order.Dir == "asc" ? results.OrderBy(x => x.OpCode) : results.OrderByDescending(x => x.OpCode); break;
                    case 2: results = order.Dir == "asc" ? results.OrderBy(x => x.OpDescription) : results.OrderByDescending(x => x.OpDescription); break;
                    case 3: results = order.Dir == "asc" ? results.OrderBy(x => x.Comment) : results.OrderByDescending(x => x.Comment); break;
                    case 4: results = order.Dir == "asc" ? results.OrderBy(x => x.LaborHour) : results.OrderByDescending(x => x.LaborHour); break;
                    case 5: results = order.Dir == "asc" ? results.OrderBy(x => x.LaborRate) : results.OrderByDescending(x => x.LaborRate); break;
                    case 11: results = order.Dir == "asc" ? results.OrderBy(x => x.IsLofService) : results.OrderByDescending(x => x.IsLofService); break;
                    case 12: results = order.Dir == "asc" ? results.OrderBy(x => x.CanBeStriked) : results.OrderByDescending(x => x.CanBeStriked); break;
                    case 13: results = order.Dir == "asc" ? results.OrderBy(x => x.IntervalStart) : results.OrderByDescending(x => x.IntervalStart); break;
                    case 14: results = order.Dir == "asc" ? results.OrderBy(x => x.IntervalRepeat) : results.OrderByDescending(x => x.IntervalRepeat); break;
                    case 15: results = order.Dir == "asc" ? results.OrderBy(x => x.AdvisorIncentive) : results.OrderByDescending(x => x.AdvisorIncentive); break;
                    case 16: results = order.Dir == "asc" ? results.OrderBy(x => x.TechIncentive) : results.OrderByDescending(x => x.TechIncentive); break;
                    case 17: results = order.Dir == "asc" ? results.OrderBy(x => x.BgProtectionPlan.ProtectionPlanName) : results.OrderByDescending(x => x.BgProtectionPlan.ProtectionPlanName); break;
                    case 18: results = order.Dir == "asc" ? results.OrderBy(x => x.BgProtectionSubPlan.SubPlanName) : results.OrderByDescending(x => x.BgProtectionSubPlan.SubPlanName); break;
                    default: results = results.OrderBy(x => x.Id); break;
                }
            }
            else
            {
                results = results.OrderByDescending(x => x.Id);
            }
            totalCount = results.Count();
            filteredServiceIds = string.Join(StaticSettings.Delimiter,results.Select(x => x.Id).ToList());
            results = results.Skip(request.Start).Take(request.Length);
            return results;
        }

        public IEnumerable<AdditionalService> GetServicesAndParts(CustomServiceMaintenancePagingViewModel request, out int totalCount, out string filteredServiceIds)
        {
            var parameters = new List<SqlParameter>();
            if (request.Length == 0)
            {
                request.Length = 10;
            }

            parameters.Add(new SqlParameter("@Years", request.Years != null ? string.Join(",", request.Years) : ""));
            parameters.Add(new SqlParameter("@Makes", request.Makes != null ? string.Join(",", request.Makes) : ""));
            parameters.Add(new SqlParameter("@Models", request.Models != null ? string.Join(",", request.Models) : ""));
            parameters.Add(new SqlParameter("@EngineTypes", request.EngineTypes != null ? string.Join(",", request.EngineTypes) : ""));
            parameters.Add(new SqlParameter("@TransmissionTypes", request.TransmissionTypes != null ? string.Join(",", request.TransmissionTypes) : ""));
            parameters.Add(new SqlParameter("@DriveLines", request.DriveLines != null ? string.Join(",", request.DriveLines) : ""));
            parameters.Add(new SqlParameter("@CurrentPage", request.Start / request.Length));
            parameters.Add(new SqlParameter("@PageSize", request.Length));
            parameters.Add(new SqlParameter("@SearchKeyword", request.ServiceKeyword ?? ""));
            parameters.Add(new SqlParameter("@OpCode", request.OpCode ?? ""));

            var order = request.Order?.FirstOrDefault();
            if (order != null)
            {
                switch (order.Column)
                {
                    case 0: parameters.Add(new SqlParameter("@OrderExpression", $"OpCode {order.Dir}")); break;
                    case 1: parameters.Add(new SqlParameter("@OrderExpression", $"OpDescription {order.Dir}")); break;
                    case 2: parameters.Add(new SqlParameter("@OrderExpression", $"Comment {order.Dir}")); break;
                    case 3: parameters.Add(new SqlParameter("@OrderExpression", $"LaborHour {order.Dir}")); break;
                    case 4: parameters.Add(new SqlParameter("@OrderExpression", $"LaborRate {order.Dir}")); break;
                    case 12: parameters.Add(new SqlParameter("@OrderExpression", $"IntervalStart {order.Dir}")); break;
                    case 13: parameters.Add(new SqlParameter("@OrderExpression", $"IntervalRepeat {order.Dir}")); break;
                    case 14: parameters.Add(new SqlParameter("@OrderExpression", $"AdvisorIncentive {order.Dir}")); break;
                    case 15: parameters.Add(new SqlParameter("@OrderExpression", $"TechIncentive {order.Dir}")); break;
                    case 16: parameters.Add(new SqlParameter("@OrderExpression", $"BgProtectionPlan.ProtectionPlanName {order.Dir}")); break;
                    case 17: parameters.Add(new SqlParameter("@OrderExpression", $"BgProtectionSubPlan.SubPlanName {order.Dir}")); break;
                    default: parameters.Add(new SqlParameter("@OrderExpression", $"AdditionalServiceId {order.Dir}")); break;
                }
            }
            else
            {
                parameters.Add(new SqlParameter("@OrderExpression", "AdditionalServiceId asc"));
            }


            var results = SPExecuteDataSet(SPNames.GetAdditionalServicesForMaintenance, parameters, tableNames: new[] { "services", "parts", "totalFilteredServicesCount" });
            var services = new List<AdditionalService>();

            foreach (DataRow row in results.Tables["services"].Rows)
            {
                var service = new AdditionalService
                {
                    Id = (long)row["AdditionalServiceId"],
                    TenantId = (int)row["DealerId"],
                    OpCode = row["OpCode"].ToString(),
                    OpDescription = row["OpDescription"].ToString(),
                    Comment = row["Comment"].ToString(),
                    IsLofService = (bool)row["IsLofService"],
                    CanBeStriked = (bool)row["CanBeStriked"],
                    IsFixLabor = (bool)row["IsFixLabor"],
                    MeasurementUnitId = (int)row["MeasurementUnitId"],
                    TimeCreated = (DateTime)row["TimeCreated"],
                    TimeUpdated = (DateTime)row["TimeUpdated"]
                };

                if (!string.IsNullOrEmpty(row["BgProtectionPlanId"].ToString()))
                {
                    service.BgProtectionPlanId = (int)row["BgProtectionPlanId"];
                }
                if (!string.IsNullOrEmpty(row["LaborHour"].ToString()))
                {
                    service.LaborHour = (double?)row["LaborHour"];
                }
                if (!string.IsNullOrEmpty(row["LaborRate"].ToString()))
                {
                    service.LaborRate = (decimal?)row["LaborRate"];
                }
                if (!string.IsNullOrEmpty(row["MenuLevel"].ToString()))
                {
                    service.MenuLevel = (byte?)row["MenuLevel"];
                }
                if (!string.IsNullOrEmpty(row["AdvisorIncentive"].ToString()))
                {
                    service.AdvisorIncentive = (decimal?)row["AdvisorIncentive"];
                }
                if (!string.IsNullOrEmpty(row["TechIncentive"].ToString()))
                {
                    service.TechIncentive = (decimal?)row["TechIncentive"];
                }
                if (!string.IsNullOrEmpty(row["IntervalStart"].ToString()))
                {
                    service.IntervalStart = (int?)row["IntervalStart"];
                }
                if (!string.IsNullOrEmpty(row["IntervalRepeat"].ToString()))
                {
                    service.IntervalRepeat = (int?)row["IntervalRepeat"];
                }
                if (!string.IsNullOrEmpty(row["BgProtectionSubPlanId"].ToString()))
                {
                    service.BgProtectionSubPlanId = (int?)row["BgProtectionSubPlanId"];
                }

                if (!string.IsNullOrEmpty(row["BgProdCategoryId"].ToString()))
                {
                    service.BgProdCategoryId = (int?)row["BgProdCategoryId"];
                }

                services.Add(service);
            }

            foreach (DataRow row in results.Tables["parts"].Rows)
            {
                var servicePart = new AdditionalServicePart
                {
                    Id = (long)row["AdditionalServicePartId"],
                    TenantId = (int)row["DealerId"],
                    AdditionalServiceId = (long)row["AdditionalServiceId"],
                    PartNo = row["PartNo"].ToString(),
                    PartName = row["PartName"].ToString(),
                    Quantity = (double)row["Quantity"],
                    Unit = row["Unit"].ToString(),
                    UnitPrice = (decimal)row["UnitPrice"],
                    FluidType = row["FluidType"].ToString(),
                    FluidViscosity = row["FluidViscosity"].ToString(),
                    IsEngineOil = (bool)row["IsEngineOil"],
                    IsFluid = (bool)row["IsFluid"],
                    TimeCreated = (DateTime)row["TimeCreated"],
                    TimeUpdated = (DateTime)row["TimeUpdated"]
                };

                if (!string.IsNullOrEmpty(row["BgProductId"].ToString()))
                {
                    servicePart.BgProductId = (int?)row["BgProductId"];
                }
                services.Single(x => x.Id == servicePart.AdditionalServiceId).AdditionalServiceParts.Add(servicePart);
            }

            totalCount = (int)results.Tables["totalFilteredServicesCount"].Rows[0]["TotalCount"];
            filteredServiceIds = results.Tables["totalFilteredServicesCount"].Rows[0]["FilteredServiceIds"].ToString();

            return services;
        }

        public IEnumerable<AdditionalService> GetServicesAndParts(Expression<Func<AdditionalService, bool>> expression = null)
        {
            return expression == null
                ? GetDbSet("AdditionalServiceParts", "AdditionalServiceVideoClips")
                : GetDbSet("AdditionalServiceParts", "AdditionalServiceVideoClips").Where(expression);
        }

        public int GetItemsCount()
        {
            return GetDbSet().Count();
        }

        public int GetItemsCount(CustomServiceMaintenancePagingViewModel request)
        {
            var results = GetDbSet("AdditionalServiceParts.BgProduct", "AdditionalServiceVideoClips", "BgProtectionPlan", "BgProtectionSubPlan", "BgProdCategory");
            if (!string.IsNullOrEmpty(request.OpCode))
            {
                results = results.Where(x => x.OpCode.Contains(request.OpCode));
            }

            if (!string.IsNullOrEmpty(request.ServiceKeyword))
            {
                results = results.Where(x => x.OpDescription.Contains(request.ServiceKeyword)
                     || x.Comment.Contains(request.ServiceKeyword)
                     || x.AdditionalServiceParts.Any(y => y.PartNo.Contains(request.ServiceKeyword)
                            || y.Quantity.ToString().Contains(request.ServiceKeyword)
                            || y.UnitPrice.ToString().Contains(request.ServiceKeyword)
                            || (y.BgProduct != null && y.BgProduct.Description.Contains(request.ServiceKeyword)))
                     || (x.IntervalStart.HasValue && x.IntervalStart.ToString().Contains(request.ServiceKeyword))
                     || (x.IntervalRepeat.HasValue && x.IntervalRepeat.ToString().Contains(request.ServiceKeyword))
                     || (x.BgProdCategory != null && x.BgProdCategory.Description.Contains(request.ServiceKeyword))
                     || (x.BgProtectionPlan !=null && x.BgProtectionPlan.ProtectionPlanName.Contains(request.ServiceKeyword))
                     || x.AdditionalServiceVideoClips.Any(y => y.BgVideoClip != null && y.BgVideoClip.Description.Contains(request.ServiceKeyword)));
            }

            return results.Count();
        }

        public IEnumerable<AdditionalService> GetAdditionalService(List<long> service)
        {
            return GetDbSet("BgProtectionPlan", "BgProtectionPlan.BgProtectionPlanDocs", "BgProtectionPlan.BgProtectionPlanLevels", "BgProtectionPlan.BgProtectionPlanLevels.BgProtectionPlanLevelMus").Where(r => service.Contains(r.Id));
        }

        public ServiceResponse DeleteBulk(string ids)
        {
            var response = new ServiceResponse();
            var parameters = new List<SqlParameter>
            {
                new SqlParameter("@Ids", ids)
            };

            try
            {
                int result = Convert.ToInt32(SPExecuteScalar(SPNames.DeleteAdditionalServices, parameters));
                switch (result)
                {
                    case 0:
                        response.IsSuccess = true;
                        break;
                }
            }
            catch (Exception e)
            {
                response.Errors.Add("DeleteBulk", Messages.UnknownErrorOccurred);
            }
            return response;
        }
    }
}