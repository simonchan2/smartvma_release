﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.OutputModels;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class CarRepository : BaseRepository<Car, int>, ICarRepository
    {
        public CarRepository(Models dbContext, IAppContext appContext) : base(dbContext)
        {
        }

        public List<VehicleYearInfo> GetAllVehicleYears()
        {
            List<VehicleYearInfo> vehicleYears = SPExecuteReader<VehicleYearInfo>("GetVehicleYears");

            return vehicleYears;
        }

        public List<VehicleMakeInfo> GetAllVehicleMakes()
        {
            List<VehicleMakeInfo> vehicleMakes = SPExecuteReader<VehicleMakeInfo>("GetVehicleMakes");

            return vehicleMakes;
        }

        public List<VehicleMakeInfo> GetFilteredVehicleMakes(string years)
        {
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@Year", years));

            List<VehicleMakeInfo> vehicleMakes = SPExecuteReader<VehicleMakeInfo>("GetVehicleMakes",
                parameters: parameters);

            return vehicleMakes;
        }

        public List<VehicleModelInfo> GetAllVehicleModels()
        {
            List<VehicleModelInfo> vehicleModels = SPExecuteReader<VehicleModelInfo>("GetVehicleModels");

            return vehicleModels;
        }

        public List<VehicleModelInfo> GetFilteredVehicleModels(string years, string makes)
        {
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@Year", years));
            parameters.Add(new SqlParameter("@Make", makes));

            List<VehicleModelInfo> vehicleModels = SPExecuteReader<VehicleModelInfo>("GetVehicleModels",
                parameters: parameters);

            return vehicleModels;
        }

        public List<VehicleEngineInfo> GetAllVehicleEngines()
        {
            List<VehicleEngineInfo> vehicleEngines = SPExecuteReader<VehicleEngineInfo>("GetVehicleEngines");

            return vehicleEngines;
        }

        public List<VehicleEngineInfo> GetFilteredVehicleEngines(string years, string makes, string models)
        {
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@Year", years));
            parameters.Add(new SqlParameter("@Make", makes));
            parameters.Add(new SqlParameter("@Model", models));

            List<VehicleEngineInfo> vehicleEngines = SPExecuteReader<VehicleEngineInfo>("GetVehicleEngines",
                parameters: parameters);

            return vehicleEngines;
        }

        public List<VehicleTransmissionInfo> GetAllVehicleTransmissions(string source)
        {
            List<VehicleTransmissionInfo> vehicleTransmissions = SPExecuteReader<VehicleTransmissionInfo>(source);

            return vehicleTransmissions;
        }

        public List<VehicleTransmissionInfo> GetFilteredVehicleTransmissions(string years, string makes, string models, string engines, string source)
        {
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@Year", years));
            parameters.Add(new SqlParameter("@Make", makes));
            parameters.Add(new SqlParameter("@Model", models));
            parameters.Add(new SqlParameter("@Engine", engines));

            List<VehicleTransmissionInfo> vehicleTransmissions = SPExecuteReader<VehicleTransmissionInfo>(source,
                parameters: parameters);

            return vehicleTransmissions;
        }

        public List<VehicleDriveLineInfo> GetAllVehicleDriveLines(string source)
        {
            List<VehicleDriveLineInfo> vehicleDriveLines = SPExecuteReader<VehicleDriveLineInfo>(source);

            return vehicleDriveLines;
        }

        public List<VehicleDriveLineInfo> GetFilteredVehicleDriveLines(string years, string makes, string models, string engines, string transmissions, string source)
        {
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@Year", years));
            parameters.Add(new SqlParameter("@Make", makes));
            parameters.Add(new SqlParameter("@Model", models));
            parameters.Add(new SqlParameter("@Engine", engines));
            parameters.Add(new SqlParameter("@Transmission", transmissions));

            List<VehicleDriveLineInfo> vehicleDriveLines = SPExecuteReader<VehicleDriveLineInfo>(source,
                parameters: parameters);

            return vehicleDriveLines;
        }


        public int? GetCarId(int year, int make, int model, int engine, int transmission, int driveLine)
        {
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@Year", year));
            parameters.Add(new SqlParameter("@MakeId", make));
            parameters.Add(new SqlParameter("@ModelId", model));
            parameters.Add(new SqlParameter("@EngineId", engine));
            parameters.Add(new SqlParameter("@VinMasterTransmissionId", transmission));
            parameters.Add(new SqlParameter("@VinMasterDriveLineId", driveLine));

            var carId = SPExecuteScalar("GetCarIdByFilters",
                parameters: parameters);

            return (int?)carId;
        }

        public IEnumerable<CarProcedureModel> GetCarId(IEnumerable<int> years, IEnumerable<int> makes, IEnumerable<int> models, IEnumerable<int> engines, IEnumerable<int> transmissions, IEnumerable<int> driveLines)
        {
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@Years", string.Join(",", years)));
            parameters.Add(new SqlParameter("@Makes", string.Join(",", makes)));
            parameters.Add(new SqlParameter("@Models", string.Join(",", models)));
            parameters.Add(new SqlParameter("@EngineTypes", string.Join(",", engines)));
            parameters.Add(new SqlParameter("@TransmissionTypes", string.Join(",", transmissions)));
            parameters.Add(new SqlParameter("@DriveLines", string.Join(",", driveLines)));

            var result = SPExecuteReader<CarProcedureModel>("GetCarIdByMultipleFilters", parameters: parameters);
            return result;
        }

        public Car GetById(int id)
        {
            return
                GetDbSet("VehicleMake", "VehicleModel", "VehicleEngine", "CarVinShorts", "Vehicles")
                    .FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<int> GetAllDistinctMileages(int carId, int measurmentUnitId)
        {
            List<int> result = new List<int>();
            var parameters = new List<SqlParameter>
            {
                new SqlParameter("@CarId", carId),
                new SqlParameter("@MeasurmentUnitId", measurmentUnitId)
            };
            DataTable dt = SPExecuteReader(SPNames.GetMileagesByCarId, parameters);
            if (dt != null)
            {
                result.AddRange(from DataRow row in dt.Rows select (int)row["Mileage"]);
            }
            return result;
        }

        public IEnumerable<int> GetAllDistinctMileages(int measurementUnitId)
        {
            return _dbContext.OemMenuMileages.AsQueryable()
                .Where(x => x.MeasurementUnitId == measurementUnitId)
                .Select(x => x.Mileage)
                .ToList()
                .Distinct()
                .OrderBy(x=>x);
        }

        public VehicleFilterItemsViewModel[] GetVehicleFilter(VehicleFilterViewModel filter)
        {
            var parameters = new List<SqlParameter>
            {
                new SqlParameter("@Years", filter.Years != null ? string.Join(",", filter.Years) : ""),
                new SqlParameter("@Makes", filter.Makes != null ? string.Join(",", filter.Makes) : ""),
                new SqlParameter("@Models", filter.Models != null ? string.Join(",", filter.Models) : ""),
                new SqlParameter("@EngineTypes",filter.EngineTypes != null ? string.Join(",", filter.EngineTypes) : ""),
                new SqlParameter("@TransmissionTypes",filter.TransmissionTypes != null ? string.Join(",", filter.TransmissionTypes) : ""),
                new SqlParameter("@DriveLines", filter.DriveLines != null ? string.Join(",", filter.DriveLines) : "")
            };

            if (!string.IsNullOrEmpty(filter.VIN))
            {
                parameters.Add(new SqlParameter("@VIN", filter.VIN));
            }
            return SPExecuteReader<VehicleFilterItemsViewModel>(string.IsNullOrEmpty(filter.VIN) ? SPNames.GetVehicleFilters : SPNames.GetVehicleFiltersByVIN, parameters).ToArray();
        }

        public bool VinExists(string vin)
        {
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@VIN", vin));

            var vinExists = SPExecuteScalar("VinExistsInDB", parameters: parameters);

            return (bool)vinExists;
        }
    }
}