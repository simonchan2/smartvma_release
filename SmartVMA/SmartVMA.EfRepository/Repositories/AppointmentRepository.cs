﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System;
using SmartVMA.Core.InputModels.ViewModels;
using System.Linq;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class AppointmentRepository : BaseTenantRepository<Appointment, long>, IAppointmentRepository
    {
        public AppointmentRepository(Models dbContext, IAppContext appContext) : base(dbContext, appContext)
        {
        }

        public List<AppointmentServiceViewModel> GetAppointmentServiceModel(string vin)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            SqlParameter pVin = new SqlParameter("Vin", vin);
            pVin.SqlDbType = SqlDbType.NVarChar;
            parameters.Add(pVin);

            List<AppointmentServiceViewModel> response = SPExecuteReader<AppointmentServiceViewModel>("GetVehicleMaintenanceServices", parameters: parameters);

            return response;
        }

        public List<VehicleHistoryPlanLevelsViewModel> GetVehicleHistoryPlanLevels()
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            List<VehicleHistoryPlanLevelsViewModel> response = SPExecuteReader<VehicleHistoryPlanLevelsViewModel>("GetProtectionPlanLevels", parameters: parameters);

            return response;
        }

        public List<AppointmentServiceViewModel> GetAppointmentServiceModel(int dealerCustomerID)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            SqlParameter pDealerCustomerID = new SqlParameter("DealerCustomerID", dealerCustomerID);
            pDealerCustomerID.SqlDbType = SqlDbType.Int;
            parameters.Add(pDealerCustomerID);

            List<AppointmentServiceViewModel> response = SPExecuteReader<AppointmentServiceViewModel>("GetAppointmentServices", parameters: parameters);

            return response;
        }

        public List<AppointmentViewModel> GetCalendarAppointments(DateTime startTime, DateTime endTime, string advisorIds, string presentationStatus)
        {
            List<SqlParameter> parameters = new List<SqlParameter>(); 
            GetDefaultDBParameters();

            SqlParameter appointmentStartTime = new SqlParameter("AppointmentStartTime", startTime);
            appointmentStartTime.SqlDbType = SqlDbType.DateTime;
            parameters.Add(appointmentStartTime);
            
            SqlParameter appointmentEndTime = new SqlParameter("AppointmentEndTime", endTime);
            appointmentEndTime.SqlDbType = SqlDbType.DateTime;
            parameters.Add(appointmentEndTime);

            if (string.IsNullOrEmpty(advisorIds))
            {
                SqlParameter appointmentAdvisorIds = new SqlParameter("AdvisorIDs", DBNull.Value);
                appointmentAdvisorIds.SqlDbType = SqlDbType.NVarChar;
                parameters.Add(appointmentAdvisorIds);
            }
            else
            {
                SqlParameter appointmentAdvisorIds = new SqlParameter("AdvisorIDs", advisorIds);
                appointmentAdvisorIds.SqlDbType = SqlDbType.NVarChar;
                parameters.Add(appointmentAdvisorIds);
            }

            if (string.IsNullOrEmpty(presentationStatus))
            {
                SqlParameter appointmentPresentationStatus = new SqlParameter("PresentationStatus", DBNull.Value);
                appointmentPresentationStatus.SqlDbType = SqlDbType.NVarChar;
                parameters.Add(appointmentPresentationStatus);
            }
            else
            {
                SqlParameter appointmentPresentationStatus = new SqlParameter("PresentationStatus", presentationStatus);
                appointmentPresentationStatus.SqlDbType = SqlDbType.NVarChar;
                parameters.Add(appointmentPresentationStatus);
            }

            List<AppointmentViewModel> response = SPExecuteReader<AppointmentViewModel>("GetCalendarAppointments", parameters: parameters);

            return response;
        }

        public List<AppointmentViewModel> GetSearchAppointments(string customerName, string customerNr, string customerPhone, string vin, string vehicleStock, string licensePlate)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            GetDefaultDBParameters();

            if (string.IsNullOrEmpty(customerName))
            {
                SqlParameter appointmentCustomerName = new SqlParameter("CustomerName", DBNull.Value);
                appointmentCustomerName.SqlDbType = SqlDbType.NVarChar;
                parameters.Add(appointmentCustomerName);
            }
            else
            {
                SqlParameter appointmentCustomerName = new SqlParameter("CustomerName", customerName);
                appointmentCustomerName.SqlDbType = SqlDbType.NVarChar;
                parameters.Add(appointmentCustomerName);
            }

            if (string.IsNullOrEmpty(customerNr))
            {
                SqlParameter appointmentCustomerNr = new SqlParameter("CustomerNumber", DBNull.Value);
                appointmentCustomerNr.SqlDbType = SqlDbType.NVarChar;
                parameters.Add(appointmentCustomerNr);
            }
            else
            {
                SqlParameter appointmentCustomerNr = new SqlParameter("CustomerNumber", customerNr);
                appointmentCustomerNr.SqlDbType = SqlDbType.NVarChar;
                parameters.Add(appointmentCustomerNr);
            }

            if (string.IsNullOrEmpty(customerPhone))
            {
                SqlParameter appointmentCustomerPhone = new SqlParameter("CustomerPhone", DBNull.Value);
                appointmentCustomerPhone.SqlDbType = SqlDbType.NVarChar;
                parameters.Add(appointmentCustomerPhone);
            }
            else
            {
                SqlParameter appointmentCustomerPhone = new SqlParameter("CustomerPhone", customerPhone);
                appointmentCustomerPhone.SqlDbType = SqlDbType.NVarChar;
                parameters.Add(appointmentCustomerPhone);
            }

            if (string.IsNullOrEmpty(vin))
            {
                SqlParameter appointmentVIN = new SqlParameter("VIN", DBNull.Value);
                appointmentVIN.SqlDbType = SqlDbType.NVarChar;
                parameters.Add(appointmentVIN);
            }
            else
            {
                SqlParameter appointmentVIN = new SqlParameter("VIN", vin);
                appointmentVIN.SqlDbType = SqlDbType.NVarChar;
                parameters.Add(appointmentVIN);
            }

            if (string.IsNullOrEmpty(vehicleStock))
            {
                SqlParameter appointmentVehicleStock = new SqlParameter("StockNo", DBNull.Value);
                appointmentVehicleStock.SqlDbType = SqlDbType.NVarChar;
                parameters.Add(appointmentVehicleStock);
            }
            else
            {
                SqlParameter appointmentVehicleStock = new SqlParameter("StockNo", vehicleStock);
                appointmentVehicleStock.SqlDbType = SqlDbType.NVarChar;
                parameters.Add(appointmentVehicleStock);
            }

            if (string.IsNullOrEmpty(licensePlate))
            {
                SqlParameter appointmentLicensePlate = new SqlParameter("VehicleLicense", DBNull.Value);
                appointmentLicensePlate.SqlDbType = SqlDbType.NVarChar;
                parameters.Add(appointmentLicensePlate);
            }
            else
            {
                SqlParameter appointmentLicensePlate = new SqlParameter("VehicleLicense", licensePlate);
                appointmentLicensePlate.SqlDbType = SqlDbType.NVarChar;
                parameters.Add(appointmentLicensePlate);
            }

            List<AppointmentViewModel> response = SPExecuteReader<AppointmentViewModel>("GetSearchAppointments", parameters: parameters);

            return response;
        }

        public long CreateAppointmentPresentation(long appointmentID, int? userId, string status)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            
            SqlParameter pAppointmentID = new SqlParameter("AppointmentId", appointmentID);
            pAppointmentID.SqlDbType = SqlDbType.BigInt;
            parameters.Add(pAppointmentID);

            SqlParameter pUserID = new SqlParameter("AdvisorId", userId);
            pUserID.SqlDbType = SqlDbType.Int;
            parameters.Add(pUserID);

            SqlParameter pStatus = new SqlParameter("PresentationStatus", status);
            pStatus.SqlDbType = SqlDbType.NVarChar;
            parameters.Add(pStatus);

            var res = SPExecuteReader<AppointmentViewModel>("CreateAppointmentPresentationFromAppointment", parameters: parameters);

            if (res.Count > 0)
                return res[0].AppointmentPresentationId;
            else
                return -1;
        }

        public List<PdfDocumentViewModel> GetPdfDocuments(DateTime DateTo)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            SqlParameter pDateTo = new SqlParameter("DateTo", DateTo);
            pDateTo.SqlDbType = SqlDbType.DateTime;
            parameters.Add(pDateTo);

            List<PdfDocumentViewModel> response = SPExecuteReader<PdfDocumentViewModel>("GetPdfDocumentsByAppointmentPresentationDate", parameters: parameters);
            return response;
        }

        public void DeleteObsoleteAppointments(DateTime DateTo)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            SqlParameter pDateTo = new SqlParameter("DateTo", DateTo);
            pDateTo.SqlDbType = SqlDbType.DateTime;
            parameters.Add(pDateTo);

            SPExecuteReader("DeleteObsoleteAppointmentPresentations", parameters: parameters);
        }
    }
}
