﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.Ioc;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class CountryRepository : BaseRepository<Country, int>, ICountryRepository
    {
        public CountryRepository(Models dbContext) : base(dbContext)
        {
        }
    }
}
