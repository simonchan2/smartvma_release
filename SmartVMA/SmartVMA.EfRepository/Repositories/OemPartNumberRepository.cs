﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.Ioc;
using System.Linq;
using SmartVMA.Core;
using SmartVMA.Core.Contracts;
using SmartVMA.Resources;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class OemPartNumberRepository : BaseRepository<OemPartNumber, int>, IOemPartNumberRepository
    {
        public OemPartNumberRepository(Models dbContext) : base(dbContext)
        {

        }
        public OemPartNumber GetOemPartNumberById(int id)
        {
            return GetDbSet("OemPart").FirstOrDefault(x => x.Id == id);
        }

        public List<OemPartNumber> GetOverridenPartNumbersByPartId(int tenantId, int partId)
        {
            var parameters = new List<SqlParameter>
            {
                SqlParameter("@TenantId", tenantId),
                SqlParameter("@OemPartId", partId)
            };
            return SPExecuteReader<OemPartNumber>(SPNames.GetOverridenPartNumber, parameters);
        }
    }
}
