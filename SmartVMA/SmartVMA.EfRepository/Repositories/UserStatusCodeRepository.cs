﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.Ioc;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class UserStatusCodeRepository : BaseRepository<UserStatusCode, int>, IUserStatusCodeRepository
    {
        public UserStatusCodeRepository(Models dbContext) : base(dbContext)
        {
        }
    }
}
