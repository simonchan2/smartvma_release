﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class ArticleCategoryRepository : BaseRepository<ArticleCategory, int>, IArticleCategoryRepository
    {
        public ArticleCategoryRepository(Models dbContext) : base(dbContext)
        {
        }
    }
}
