﻿using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.OutputModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class GeneralCarServiceRepository : StoredProceduresRepository, IGeneralCarServiceRepository
    {
        private readonly IAppContext _appContext;
        public GeneralCarServiceRepository(Models dbContext, IAppContext appContext) : base(dbContext)
        {
            _appContext = appContext;
        }

        public IEnumerable<ServicesProcedureModel> GetAllServices(int? carId = null, int? dealerVehicleId = null, int? mileage = null, bool? isLOF = null, IEnumerable<ServiceIdsRequestModel> serviceIds = null, int? vehicleTransmission = null, int? vehicleDriveline = null, int? dealerCustomerId = null, long? appointmentPresentationId = null)
        {
            var parameters = new List<SqlParameter>();
            parameters.Add(SqlParameter("@TenantId", _appContext.CurrentIdentity.TenantId));
            parameters.Add(SqlParameter("@CarId", carId));
            parameters.Add(SqlParameter("@DealerVehicleId", dealerVehicleId));
            parameters.Add(SqlParameter("@Mileage", mileage));
            parameters.Add(SqlParameter("@IsLof", isLOF));
            parameters.Add(SqlParameter("@VinMasterTransmission", vehicleTransmission));
            parameters.Add(SqlParameter("@VinMasterDriveLine", vehicleDriveline));
            parameters.Add(SqlParameter("@DealerCustomerId", dealerCustomerId));
            parameters.Add(new SqlParameter("@AppointmentPresentationId", appointmentPresentationId));


            if (serviceIds == null)
            {
                parameters.Add(SqlParameter("@ServiceIds", ""));
            }
            else
            {
                parameters.Add(SqlParameter("@ServiceIds", string.Join(",", serviceIds.Select(x => string.Format("{0}_{1}", x.Id, (int)x.ServiceTypeId)))));
            }

            var response = SPExecuteReader<ServicesProcedureModel>(SPNames.GetAllServices, parameters: parameters);
            return response;
        }

        public IEnumerable<ServiceDetailInvoiceHistoryViewModel> GetHistoryInvoices(List<ServiceDetailViewModel> allServices)
        {
            var parameters = new List<SqlParameter>();
            parameters.Add(SqlParameter("@DealerId", _appContext.CurrentIdentity.TenantId));
            parameters.Add(SqlParameter("@OpCode", string.Join(",", allServices.Where(c => !string.IsNullOrEmpty(c.OpCode)).Select(c => c.OpCode).Distinct())));

            var response = SPExecuteReader<ServiceDetailInvoiceHistoryViewModel>(SPNames.GetVehicleInvoiceHistory, parameters: parameters);
            return response;
        }
    }
}
