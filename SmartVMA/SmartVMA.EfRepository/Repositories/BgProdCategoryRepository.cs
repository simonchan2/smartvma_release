﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.Ioc;
using System.Collections.Generic;
using System.Linq;

namespace SmartVMA.EfRepository.Repositories
{

    [IocBindable]

    internal class BgProdCategoryRepository : BaseRepository<BgProdCategory, int>, IBgProdCategoryRepository
    {
        public BgProdCategoryRepository(Models dbContext) : base(dbContext)
        {
        }

        public List<BgProductCategoryInfo> GetAllBgProductCategories()
        {
            var result = GetDbSet().Select(x => new { x.Id, x.Description }).ToList();
            return result.Select(item => new BgProductCategoryInfo { Id = item.Id, Category = item.Description }).ToList();
        }

        public List<BgProductCategoryInfo> GetAllBgProductCategories(List<int> bgProducts)
        {
            var result = GetDbSet()
                .Where(x => x.BgProductsCategories.Any(y => bgProducts.Contains(y.BgProductId)))
                .Select(x => new { x.Id, x.Description }).ToList();
            return result.Select(item => new BgProductCategoryInfo { Id = item.Id, Category = item.Description }).ToList();
        }
    }
}
