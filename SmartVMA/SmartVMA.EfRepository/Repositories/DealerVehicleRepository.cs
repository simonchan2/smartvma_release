﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class DealerVehicleRepository : BaseTenantRepository<DealerVehicle, int>, IDealerVehicleRepository
    {
        public DealerVehicleRepository(Models dbContext, IAppContext appContext) : base(dbContext, appContext)
        {
        }

        public DealerVehicle GetDealerVehicle(string vin, int? carId, int? dealerCustomerId, long? appointmentPresentationId)
        {
            var dealerVehicles = GetDbSet("Vehicle", "AppointmentPresentations");
           
            if (dealerCustomerId.HasValue)
            {
                dealerVehicles = dealerVehicles.Where(x => !string.IsNullOrEmpty(vin) ? x.VIN != null && x.VIN.Trim() == vin.Trim() : x.VIN == x.VIN 
                && (x.Vehicle != null ? x.Vehicle.CarId == carId : x.Vehicle == null) 
                && x.DealerCustomerId == dealerCustomerId);
            }
            else
            {
                dealerVehicles = dealerVehicles.Where(x => !string.IsNullOrEmpty(vin) ? x.VIN != null && x.VIN.Trim() == vin.Trim() : x.VIN == null
                && x.CarId == carId 
                && x.DealerCustomerId == null);
            }
            if (appointmentPresentationId != null && dealerVehicles.Count() > 1)
            {
                dealerVehicles = dealerVehicles.Where(x => x.AppointmentPresentations.Any(c => c.Id == appointmentPresentationId));
            }

            return dealerVehicles.FirstOrDefault();
        }

        public int? GetDealerVehicleId(int year, int make, int model, int engine, string transmission, string driveLine)
        {
            string[] includereference = new string[4] { "Vehicle", "Vehicle.Car", "Vehicle.Car.VehicleEngine", "Vehicle.VinMaster" };

            var dealerVehicle = (from c in GetDbSet(includereference)
                                 where c.Vehicle != null
                                 && c.Vehicle.Car != null
                                 && c.Vehicle.Car.VehicleYear == year
                                 && c.Vehicle.Car.VehicleMakeId == make
                                 && c.Vehicle.Car.VehicleModelId == model
                                 && c.Vehicle.Car.VehicleEngineId == engine
                                 && c.Vehicle.VinMaster != null
                                 && c.Vehicle.VinMaster.VehicleTransmission != null
                                 && c.Vehicle.VinMaster.VehicleTransmission.ToLower() == transmission.ToLower()
                                 && c.Vehicle.VinMaster.VehicleDriveLine != null
                                 && c.Vehicle.VinMaster.VehicleDriveLine.ToLower() == driveLine.ToLower()
                                 select c).FirstOrDefault();

            return dealerVehicle != null ? (int?)dealerVehicle.Id : null;
        }

        public int? GetVehicleId(int carId, string vin)
        {
            var vehicle = (from c in GetDbSet("Vehicle")
                           where c.Id == carId && c.VIN == vin
                           select c).FirstOrDefault();

            return vehicle.VehicleId;
        }

        public bool IsExistingVIN(string vin, int? carId)
        {
            bool isExisting = false;
            var result = GetDbSet().FirstOrDefault(x => x.VIN == vin);
            if (result != null)
            {
                //dealervehicle exists when parking menu with vin and carid with no customer, validation should pass so the procedure could use the same dealervehicle
                if (result.CarId == carId && (result.DealerCustomerId == null || result.DealerCustomerId == 0))
                {
                    isExisting = false;
                }
                else
                {
                    isExisting = true;
                }
            }
            return isExisting;
        }

        public int? GetDealerCustomerIdByVIN(string vin)
        {
            var result = GetDbSet().FirstOrDefault(x => x.VIN == vin);
            return result != null ? result.DealerCustomerId : null;
        }
    }
}
