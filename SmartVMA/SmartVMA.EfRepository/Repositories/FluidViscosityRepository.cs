﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.Ioc;
using System.Collections.Generic;
using System.Linq;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class FluidViscosityRepository : BaseRepository<FluidViscosity, int>, IFluidViscosityRepository
    {
        public FluidViscosityRepository(Models dbContext) : base(dbContext)
        {
        }

        public IEnumerable<FluidViscosity> GetAllFluidViscosityInDealerOilViscosityList()
        {
            return GetDbSet().Where(x => x.InDealerOilViscosityList == true).OrderBy(x => x.SortOrder).ThenBy(x => x.Viscosity);
        }
    }
}
