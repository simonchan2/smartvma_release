﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class EngineOilTypeRepository : BaseRepository<EngineOilType, int>, IEngineOilTypeRepository
    {
        public EngineOilTypeRepository(Models dbContext) : base(dbContext)
        {
        }

        public IEnumerable<EngineOilType> GetAllEngineOilTypes()
        {
            return GetDbSet().OrderBy(x => x.SortOrder).ThenBy(y => y.OilType);
        }
    }
}
