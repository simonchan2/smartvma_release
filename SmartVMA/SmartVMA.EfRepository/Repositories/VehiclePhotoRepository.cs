﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartVMA.Core.Contracts;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class VehiclePhotoRepository : BaseRepository<VehiclePhoto, long>, IVehiclePhotoRepository
    {
        public VehiclePhotoRepository(Models dbContext, IAppContext appContext) : base(dbContext)
        {
        }
        public IEnumerable<VehiclePhoto> GetVehiclePhotos(long inspectionId)
        {
            var list = Find(x => x.InspectionId == inspectionId);

            return list;
        }
    }
}