﻿using System.Collections.Generic;
using System.Linq;
using SmartVMA.Core;
using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class CompanySettingRepository : BaseTenantRepository<CompanySetting, int>, ICompanySettingRepository
    {
        public CompanySettingRepository(Models dbContext, IAppContext appContext) : base(dbContext, appContext)
        {
        }

        public string GetSettingValue(CompanySettingsNames setting)
        {
            var currentIdentity = _appContext.CurrentIdentity;
            var dbSetting = FirstOrDefault(x => x.Name == setting.ToString() && x.TenantId == currentIdentity.TenantId);
            return dbSetting != null ? dbSetting.Value : string.Empty;
        }

        public string GetDefaultSettingValue(CompanySettingsNames setting)
        {
            var dbSetting = _dbContext.CompanySettings.FirstOrDefault(x => x.Name == setting.ToString() && x.TenantId == StaticSettings.BaseCompanyId);
            return dbSetting != null ? dbSetting.Value : string.Empty;
        }

        public List<CompanySetting> GetAllSettings()
        {
            var currentIdentity = _appContext.CurrentIdentity;
            List<CompanySetting> allSettings = GetDbSet().Where(x => x.TenantId == currentIdentity.TenantId.Value).ToList();
            return allSettings;
        }
    }
}
