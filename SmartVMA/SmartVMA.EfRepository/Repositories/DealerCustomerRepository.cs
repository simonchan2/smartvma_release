﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System;
using System.Data;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class DealerCustomerRepository : BaseTenantRepository<DealerCustomer, int>, IDealerCustomerRepository
    {
        public DealerCustomerRepository(Models dbContext, IAppContext appContext) : base(dbContext, appContext)
        {
        }

        public List<AdvisorSearchViewModel> CustomerSearch(string customerName, string customerNumber, string customerPhoneNumber, string vin, string vehicleStock, string licensePlate, int parkedMenuRetentionDuration)
        {
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@CustomerName", customerName));
            parameters.Add(new SqlParameter("@CustomerNumber", customerNumber));
            parameters.Add(new SqlParameter("@CustomerPhone", customerPhoneNumber));
            parameters.Add(new SqlParameter("@VIN", vin));
            parameters.Add(new SqlParameter("@StockNo", vehicleStock));
            parameters.Add(new SqlParameter("@VehicleLicense", licensePlate));
            parameters.Add(new SqlParameter("@ParkedMenuRetentionDuration", parkedMenuRetentionDuration));

            List<AdvisorSearchViewModel> response = SPExecuteReader<AdvisorSearchViewModel>("SearchCustomer", parameters: parameters);

            return response;
        }

        public CustomerInfoViewModel GetCustomerInfo(long appointmentPresentationId)
        {
            return GetCustomerInfo(null, null, appointmentPresentationId, null, null, null);
        }

        public CustomerInfoViewModel GetCustomerInfo(int? dealerCustomerId, int carId, int? transmissionId, int? driveLineId, int? dealerVehicleId)
        {
            return GetCustomerInfo(dealerCustomerId, carId, null, transmissionId, driveLineId, dealerVehicleId);
        }

        public CustomerInfoViewModel GetCustomerInfo(int? dealerCustomerId, int? carId, long? appointmentPresentationId, int? transmissionId, int? driveLineId, int? dealerVehicleId)
        {
            var parameters = new List<SqlParameter>();
            parameters.Add(SqlParameter("@DealerCustomerId", dealerCustomerId == 0 ? null : dealerCustomerId));
            parameters.Add(SqlParameter("@CarId", carId));
            parameters.Add(SqlParameter("@AppointmentPresentationId", appointmentPresentationId));
            parameters.Add(SqlParameter("@VinMasterTransmissionId", transmissionId));
            parameters.Add(SqlParameter("@VinMasterDriveLineId", driveLineId));
            parameters.Add(SqlParameter("@DealerVehicleId", dealerVehicleId));

            var response = SPExecuteReader<CustomerInfoViewModel>(SPNames.GetCustomerInfo, parameters: parameters);
            return response.FirstOrDefault();
        }

        public string GetVinByCustomerSearchInfo(string customerName, string customerNumber, string customerPhoneNumber, string vehicleStock, string licensePlate)
        {
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@CustomerName", customerName));
            parameters.Add(new SqlParameter("@CustomerNumber", customerNumber));
            parameters.Add(new SqlParameter("@CustomerPhone", customerPhoneNumber));
            parameters.Add(new SqlParameter("@StockNo", vehicleStock));
            parameters.Add(new SqlParameter("@VehicleLicense", licensePlate));

            var vin = SPExecuteScalar("GetVINFromCustomerSearch", parameters: parameters);

            return vin.ToString();
        }
    }
}
