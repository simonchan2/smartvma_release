﻿using System;
using SmartVMA.Core.Entities;
using SmartVMA.Core.OutputModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.Ioc;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using Elmah;
using SmartVMA.Core;
using SmartVMA.Core.Contracts;
using SmartVMA.Core.InputModels.PageRequests;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Resources;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class OemBasicServiceRepository : BaseRepository<OemBasicService, int>, IOemBasicServiceRepository
    {
        public OemBasicServiceRepository(Models dbContext) : base(dbContext)
        {
        }

        public OemBasicService GetOemBasicServiceById(int id)
        {
            return GetDbSet("Car", "OemServiceParts", "OemServiceParts.OemPart.OemPartNumbers", "OemServiceException").FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<OemBasicService> GetAllOemBasicServicesWithException()
        {
            return GetDbSet("Car", "OemServiceParts", "OemServiceParts.OemPart.OemPartNumbers", "OemServiceException").Where(x => x.IsIncomplete == true && x.IsDeleted == false).AsEnumerable();
        }

        public IEnumerable<OemBasicService> GetServicesForCar(int carId)
        {
            var includeReference = new[] { "OemComponent", "OemServiceParts", "OemServiceParts.OemPart.OemPartNumbers" };
            return GetDbSet(includeReference).Where(x => x.CarId == carId);
        }

        public IEnumerable<OemBasicService> GetPagedResults(OemServiceMaintenancePagingViewModel request, out int totalCount, out string filteredServiceIds)
        {
            var parameters = new List<SqlParameter>();
            if (request.Length == 0)
            {
                request.Length = 10;
            }
            parameters.Add(new SqlParameter("@Years", string.Join(",", request.Years)));
            parameters.Add(new SqlParameter("@Makes", string.Join(",", request.Makes)));
            parameters.Add(new SqlParameter("@Models", string.Join(",", request.Models)));
            parameters.Add(new SqlParameter("@EngineTypes", string.Join(",", request.EngineTypes)));
            parameters.Add(new SqlParameter("@TransmissionTypes", string.Join(",", request.TransmissionTypes)));
            parameters.Add(new SqlParameter("@DriveLines", string.Join(",", request.DriveLines)));

            parameters.Add(new SqlParameter("@CurrentPage", request.Start / request.Length));
            parameters.Add(new SqlParameter("@PageSize", request.Length));
            parameters.Add(new SqlParameter("@SearchKeyword", request.ServiceKeyword ?? ""));

            var order = request.Order?.FirstOrDefault();
            if (order != null && order.Column == 0)
            {
                parameters.Add(new SqlParameter("@OrderExpression", $"OemComponentId {order.Dir}"));
            }
            else if (order != null && order.Column == 1)
            {
                parameters.Add(new SqlParameter("@OrderExpression", $"OpDescription {order.Dir}"));
            }
            else if (order != null && order.Column == 2)
            {
                parameters.Add(new SqlParameter("@OrderExpression", $"LaborHour {order.Dir}"));
            }
            else if (order != null && order.Column == 8)
            {
                parameters.Add(new SqlParameter("@OrderExpression", $"LaborHour {order.Dir}"));
            }
            else
            {
                parameters.Add(new SqlParameter("@OrderExpression", "OemComponentId asc"));
            }

            var results = SPExecuteDataSet(SPNames.GetOemBasicServicesForMaintenance, parameters: parameters, tableNames: new[] { "services", "parts", "partNumbers", "totalFilteredServicesCount", "filteredServiceIds" });
            var services = new List<OemBasicService>();

            foreach (DataRow row in results.Tables["services"].Rows)
            {
                var service = new OemBasicService();

                service.Id = (int)row["OemBasicServiceId"];
                service.CarId = (int)row["CarId"];
                service.OemComponentId = (int)row["OemComponentId"];
                if (!string.IsNullOrEmpty(row["OemServiceExceptionId"].ToString()))
                {
                    service.OemServiceExceptionId = (int?)row["OemServiceExceptionId"];
                }
                service.OpAction = row["OpAction"].ToString();
                service.OpDescription = (row["OpDescription"] ?? string.Empty).ToString();
                service.LaborSkillLevel = (row["LaborSkillLevel"] ?? string.Empty).ToString();
                service.LaborHour = (double)row["LaborHour"];
                service.VehicleTransmission = (row["VehicleTransmission"] ?? string.Empty).ToString();
                service.OemComponent = new OemComponent
                {
                    Id = Convert.ToInt32(row["OemBasicServiceId"] ?? 0),
                    IsLOF = Convert.ToBoolean(row["IsLof"] ?? false)
                };
                if (!string.IsNullOrEmpty(row["IsIncomplete"].ToString()))
                {
                    service.IsIncomplete = (bool?)row["IsIncomplete"];
                }

                service.TimeCreated = (DateTime)row["TimeCreated"];
                service.TimeUpdated = (DateTime)row["TimeUpdated"];
                service.NoOverwrite = (bool)row["NoOverwrite"];

                if (!string.IsNullOrEmpty(row["MileagesM"].ToString()))
                {
                    service.MileagesM = (string)row["MileagesM"];
                }

                if (!string.IsNullOrEmpty(row["MileagesKM"].ToString()))
                {
                    service.MileagesKM = (string)row["MileagesKM"];
                }

                services.Add(service);
            }

            foreach (DataRow row in results.Tables["parts"].Rows)
            {
                var servicePart = new OemServicePart
                {
                    Id = (int)row["OemServicePartId"],
                    OemBasicServiceId = (int)row["OemBasicServiceId"],
                    OemPartId = (int)row["OemPartId"],
                    Quantity = (double)row["Quantity"],
                    TimeCreated = (DateTime)row["TimeCreated"],
                    TimeUpdated = (DateTime)row["TimeUpdated"],
                    NoOverwrite = (bool)row["NoOverwrite"],
                    OemPart = new OemPart
                    {
                        Id = (int)row["OemPartId"],
                        CarId = (int)row["CarId"],
                        OemPartName = row["OemPartName"].ToString(),
                        Unit = (row["Unit"] ?? string.Empty).ToString(),
                        UnitPrice = (decimal)row["UnitPrice"],
                        FluidType = (row["FluidType"] ?? string.Empty).ToString(),
                        FluidViscosity = (row["FluidViscosity"] ?? string.Empty).ToString(),
                        EngineOilType = (row["EngineOilType"] ?? string.Empty).ToString(),
                        LaborSkillLevel = (row["LaborSkillLevel"] ?? string.Empty).ToString(),
                        LaborHour = (double)row["LaborHour"],
                        TimeCreated = (DateTime)row["TimeCreated"],
                        TimeUpdated = (DateTime)row["TimeUpdated"],
                        NoOverwrite = (bool)row["NoOverwrite"],
                        OemComponent = new OemComponent
                        {
                            Id = Convert.ToInt32(row["OemComponentId"] ?? 0),
                            IsFluid = Convert.ToBoolean(row["IsFluid"] ?? false)
                        }
                    }
                };

                foreach (DataRow r in results.Tables["partNumbers"].Rows)
                {
                    var partNumber = new OemPartNumber
                    {
                        Id = (int)r["OemPartNumberId"],
                        OemPartId = (int)r["OemPartId"],
                        CountryId = (int)r["CountryId"],
                        OemPartNo = (r["OemPartNo"] ?? string.Empty).ToString(),
                        NoOverwrite = (bool)r["NoOverwrite"],
                        TimeCreated = (DateTime)r["TimeCreated"],
                        TimeUpdated = (DateTime)r["TimeUpdated"]
                        //IsCreatedByAdmin = (bool)r["IsCreatedByAdmin"]
                    };
                    if (servicePart.OemPartId == partNumber.OemPartId)
                    {
                        servicePart.OemPart.OemPartNumbers.Add(partNumber);
                    }
                }

                if (!string.IsNullOrEmpty(row["OemComponentId"].ToString()))
                {
                    servicePart.OemPart.OemComponentId = (int?)row["OemComponentId"];
                }
                services.Single(x => x.Id == servicePart.OemBasicServiceId).OemServiceParts.Add(servicePart);
            }

            totalCount = (int)results.Tables["totalFilteredServicesCount"].Rows[0].ItemArray[0];
            if (results.Tables["filteredServiceIds"].Rows.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                List<string> filteredIdsList = new List<string>();
                foreach (DataRow row in results.Tables["filteredServiceIds"].Rows)
                {
                    filteredIdsList.Add(row["Id"].ToString());
                }
                filteredServiceIds = string.Join(StaticSettings.Delimiter, filteredIdsList);
            }
            else
            {
                filteredServiceIds = string.Empty;
            }
            return services;
        }

        public int GetPagedResultsTotal()
        {
            return GetDbSet().Count(x => x.IsIncomplete != true);
        }

        public int GetPagedResultsTotal(PagingRequest request)
        {
            var result = GetDbSet("Car", "OemServiceParts", "OemServiceParts.OemPart", "OemServiceException").Where(x => x.IsIncomplete == true);
            if (!string.IsNullOrEmpty(request.Search?.Value))
            {
                result = result.Where(x => x.OpDescription.ToLower().Contains(request.Search.Value.ToLower()) || x.Car.Description.Contains(request.Search.Value) || x.Car.CarExtId.ToString().Equals(request.Search.Value));
            }
            return result.Count();
        }

        public ServiceResponse Save(string serviceAsXml, IEnumerable<int> years, IEnumerable<int> makes, IEnumerable<int> models, IEnumerable<int> engines, IEnumerable<int> transmissions, IEnumerable<int> driveLines, int counter)
        {
            var response = new ServiceResponse();
            var parameters = new List<SqlParameter>
            {
                new SqlParameter("@ServiceAsXml", serviceAsXml),
                new SqlParameter("@Years", string.Join(",", years)),
                new SqlParameter("@Makes", string.Join(",", makes)),
                new SqlParameter("@Models", string.Join(",", models)),
                new SqlParameter("@EngineTypes", string.Join(",", engines)),
                new SqlParameter("@TransmissionTypes", string.Join(",", transmissions)),
                new SqlParameter("@DriveLines", string.Join(",", driveLines))
            };

            try
            {
                int result = Convert.ToInt32(SPExecuteScalar(SPNames.SaveOemBasicService, parameters: parameters));
                switch (result)
                {
                    case 0:
                        response.IsSuccess = true;
                        break;
                    case 1:
                        response.Errors.Add($"items[{counter}].OpAction", Messages.OpActionAlreadyUsed);
                        break;
                    default:
                        response.Errors.Add($"items[{counter}].OemComponentId", Messages.UnknownErrorOccurred);
                        break;
                }
            }
            catch (Exception e)
            {
                response.Errors.Add($"items[{counter}].OemComponentId", Messages.UnknownErrorOccurred);
            }
            return response;
        }

        public IEnumerable<string> GetDistinctOpActions()
        {
            return GetDbSet().Select(x => x.OpAction).Distinct().ToList();
        }

        public IEnumerable<OemBasicService> GetPagedResults(PagingRequest request)
        {
            var result = GetDbSet("Car", "OemServiceParts", "OemServiceParts.OemPart", "OemServiceException").Where(x => x.IsIncomplete == true);
            var columnsSort = new Dictionary<int, string>
            {
                {0, "Car.CarExtId"},
                {1, "Car.Description"},
                {2, "OpDescription"}
            };

            if (!string.IsNullOrEmpty(request.Search?.Value))
            {
                result = result.Where(x => x.OpDescription.ToLower().Contains(request.Search.Value.ToLower()) || x.Car.Description.Contains(request.Search.Value) || x.Car.CarExtId.ToString().Equals(request.Search.Value));
            }

            var order = request.Order.FirstOrDefault();
            if (order != null && order.Dir == "asc")
            {
                result = result.OrderByTest(columnsSort[order.Column]).Skip(request.Start).Take(request.Length);
            }
            else if (order != null)
            {
                result = result.OrderByDescendingTest(columnsSort[order.Column]).Skip(request.Start).Take(request.Length);
            }
            return result;
        }

        public ServiceResponse DeleteBulk(string ids)
        {
            var response = new ServiceResponse();
            var parameters = new List<SqlParameter>
            {
                new SqlParameter("@Ids", ids)
            };

            try
            {
                int result = Convert.ToInt32(SPExecuteScalar(SPNames.DeleteOemBasicServices, parameters));
                switch (result)
                {
                    case 0:
                        response.IsSuccess = true;
                        break;
                }
            }
            catch (Exception e)
            {
                response.Errors.Add("DeleteBulk", Messages.UnknownErrorOccurred);
            }
            return response;
        }

        public override void Update(OemBasicService entity)
        {
            // TODO: do not remove this comment. This is for SVMAPHASE1-2221
            //foreach (var part in entity.OemServiceParts)
            //{
            //    if (_dbContext.Entry(part).State == EntityState.Detached && part.Id != 0)
            //    {
            //        part.TimeUpdated = DateTime.UtcNow;
            //        _dbContext.Entry(part).State = EntityState.Modified;
            //    }
            //    else if (part.Id == 0)
            //    {
            //        part.TimeCreated = DateTime.UtcNow;
            //        part.TimeUpdated = DateTime.UtcNow;
            //        _dbContext.Entry(part).State = EntityState.Added;
            //    }

            //    if (_dbContext.Entry(part.OemPart).State == EntityState.Detached && part.OemPart.Id != 0)
            //    {
            //        part.OemPart.TimeUpdated = DateTime.UtcNow;
            //        _dbContext.Entry(part.OemPart).State = EntityState.Modified;
            //    }
            //    else if (part.OemPart.Id != 0)
            //    {
            //        part.OemPart.TimeCreated = DateTime.UtcNow;
            //        part.OemPart.TimeUpdated = DateTime.UtcNow;
            //        _dbContext.Entry(part.OemPart).State = EntityState.Added;
            //    }
            //}
            base.Update(entity);
        }
    }
}