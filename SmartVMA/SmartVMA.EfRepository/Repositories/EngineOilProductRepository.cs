﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class EngineOilProductRepository : BaseTenantRepository<EngineOilProduct, int>, IEngineOilProductRepository
    {
        public EngineOilProductRepository(Models dbContext, IAppContext appContext) : base(dbContext, appContext)
        {
        }        
    }
}
