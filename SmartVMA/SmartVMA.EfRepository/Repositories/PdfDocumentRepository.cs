﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System.Linq;
using System.Collections.Generic;
using SmartVMA.Core.InputModels.ViewModels;
using System.Data.SqlClient;
using System.Data;
using System;
using SmartVMA.Core.Services;
using SmartVMA.Core.Enums;
using SmartVMA.Resources;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class PdfDocumentRepository : BaseTenantRepository<PdfDocument, int>, IPdfDocumentRepository
    {
        public PdfDocumentRepository(Models dbContext, IAppContext appContext) : base(dbContext, appContext)
        {
        }

        public PdfDocument GetPdfDocument(long appointmentPresentationId, int documentType, int languageId)
        {
            return GetDbSet().Where(x => x.AppointmentPresentationId == appointmentPresentationId && x.DocumentType == documentType && (!x.LanguageId.HasValue || x.LanguageId == languageId)).FirstOrDefault();
        }

        public IEnumerable<PdfDocument> GetDocuments(string VIN, string InvoiceNumber, DateTime? startDate, DateTime? endDate)
        {
            if (endDate.HasValue)
            {
                endDate = endDate.Value.AddDays(1);
            }

            if (!string.IsNullOrEmpty(VIN) && !string.IsNullOrEmpty(InvoiceNumber))
            {
                return GetDbSet().Where(x => x.VIN == VIN && x.InvoiceNumber.Contains(InvoiceNumber) && (!startDate.HasValue || x.TimeCreated > startDate.Value) && (!endDate.HasValue || x.TimeCreated < endDate.Value));
            }
            else if (!string.IsNullOrEmpty(VIN))
            {
                return GetDbSet().Where(x => x.VIN == VIN && (!startDate.HasValue || x.TimeCreated > startDate.Value) && (!endDate.HasValue || x.TimeCreated < endDate.Value));
            }
            else if (!string.IsNullOrEmpty(InvoiceNumber))
            {
                return GetDbSet().Where(x => x.InvoiceNumber.Contains(InvoiceNumber) && (!startDate.HasValue || x.TimeCreated > startDate.Value) && (!endDate.HasValue || x.TimeCreated < endDate.Value));
            }
            else if (startDate.HasValue || endDate.HasValue)
            {
                return GetDbSet().Where(x => (!startDate.HasValue || x.TimeCreated > startDate.Value) && (!endDate.HasValue || x.TimeCreated < endDate.Value));
            }
            else
            {
                return null;
            }
        }

        public void AddPdfDocumentToPrintQueue(int dealerCustomerId, int? userId, string fileName, long appointmentPresentationID, string prnJobTypeCode)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            SqlParameter pDealerCustomerId = new SqlParameter("DealerId", dealerCustomerId);
            pDealerCustomerId.SqlDbType = SqlDbType.Int;
            parameters.Add(pDealerCustomerId);

            SqlParameter pUserId = userId.HasValue ? new SqlParameter("UserId", userId.Value) : new SqlParameter("UserId", DBNull.Value);
            pUserId.SqlDbType = SqlDbType.Int;
            parameters.Add(pUserId);

            SqlParameter pAppointmentPresentationID = new SqlParameter("AppointmentPresentationId", appointmentPresentationID);
            pDealerCustomerId.SqlDbType = SqlDbType.BigInt;
            parameters.Add(pAppointmentPresentationID);

            SqlParameter pFileName = new SqlParameter("FileName", fileName);
            pDealerCustomerId.SqlDbType = SqlDbType.NVarChar;
            parameters.Add(pFileName);

            SqlParameter pPrnJobTypeCode = new SqlParameter("PrnJobTypeCode", prnJobTypeCode);
            pDealerCustomerId.SqlDbType = SqlDbType.NVarChar;
            parameters.Add(pPrnJobTypeCode);

            SPExecuteReader(SPNames.AddPdfDocumentToPrintQueue, parameters: parameters);
        }

        public PdfHeaderViewModel GetPdfHeaderViewModel(int dealerId, long appointmentPresentationId, int? mileage)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            SqlParameter Mileage = new SqlParameter("MenuMileage", mileage);
            parameters.Add(Mileage);
            SqlParameter pAppointmentPresentationId = new SqlParameter("AppointmentPresentationId", appointmentPresentationId);
            pAppointmentPresentationId.SqlDbType = SqlDbType.BigInt;
            parameters.Add(pAppointmentPresentationId);

            PdfHeaderViewModel response = SPExecuteReader<PdfHeaderViewModel>("GetPdfHeader", parameters: parameters).First();

            return response;
        }

        public PdfHeaderCustomerInfoViewModel GetPdfHeaderCustomerInfoViewModel(int dealerId, long appointmentPresentationId)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            SqlParameter pAppointmentPresentationId = new SqlParameter("AppointmentPresentationId", appointmentPresentationId);
            pAppointmentPresentationId.SqlDbType = SqlDbType.BigInt;
            parameters.Add(pAppointmentPresentationId);

            PdfHeaderCustomerInfoViewModel response = SPExecuteReader<PdfHeaderCustomerInfoViewModel>("GetPdfHeaderCustomerInfo", parameters: parameters).FirstOrDefault();

            return response;
        }

        public PdfHeaderCustomerInfoViewModel GetPdfHeaderCustomerInfoViewModelFromVin(string vin)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            SqlParameter pvin  = new SqlParameter("vin", vin);
            pvin.SqlDbType = SqlDbType.NVarChar;
            parameters.Add(pvin);

            PdfHeaderCustomerInfoViewModel response = SPExecuteReader<PdfHeaderCustomerInfoViewModel>("GetPdfHeaderCustomerInfoFromVin", parameters: parameters).FirstOrDefault();

            return response;
        }

        public PartsCopyPdfViewModel GetPartsCopyPdfViewModel(long appointmentPresentationId)
        {
            PartsCopyPdfViewModel result = new PartsCopyPdfViewModel();
            List<SqlParameter> parameters = new List<SqlParameter>();

            SqlParameter pAppointmentPresentationId = new SqlParameter("AppointmentPresentationId", appointmentPresentationId);
            pAppointmentPresentationId.SqlDbType = SqlDbType.BigInt;
            parameters.Add(pAppointmentPresentationId);

            result.Services = SPExecuteReader<PartsCopyPdfService>("GetPdfBodyPartsCopy", parameters: parameters);

            return result;
        }

        public TechCopyPdfViewModel GetTechCopyPdfViewModel(long appointmentPresentationId)
        {
            TechCopyPdfViewModel result = new TechCopyPdfViewModel();
            List<SqlParameter> parameters = new List<SqlParameter>();

            SqlParameter pAppointmentPresentationId = new SqlParameter("AppointmentPresentationId", appointmentPresentationId);
            pAppointmentPresentationId.SqlDbType = SqlDbType.BigInt;
            parameters.Add(pAppointmentPresentationId);

            result.Services = SPExecuteReader<TechCopyPdfService>("GetPdfBodyTechCopy", parameters: parameters);

            return result;
        }

        public CustomerCopyPdfViewModel GetCustomerCopyPdfViewModel(long appointmentPresentationId)
        {
            CustomerCopyPdfViewModel result = new CustomerCopyPdfViewModel();
            List<SqlParameter> parameters = new List<SqlParameter>();

            SqlParameter pAppointmentPresentationId = new SqlParameter("AppointmentPresentationId", appointmentPresentationId);
            pAppointmentPresentationId.SqlDbType = SqlDbType.BigInt;
            parameters.Add(pAppointmentPresentationId);

            result.Services = SPExecuteReader<CustomerCopyPdfService>("GetPdfBodyCustomerCopy", parameters: parameters);

            if (result.Services.Count > 0)
            {
                var servicesPrice = result.Services.Sum(x => x.ServicePrice);
                result.TotalPrice = servicesPrice;
            }
            else
            {
                result.TotalPrice = 0;
            }

            return result;
        }

        public PdfFooterViewModel GetPdfFooterViewModel(long? appointmentPresentationId, DateTime? printedDate)
        {
            var companyService = IocManager.Resolve<ICompanyService>();
            var model = new PdfFooterViewModel();
            model.From = appointmentPresentationId > 0 ? appointmentPresentationId.ToString() : string.Empty;
            model.Printed = printedDate.HasValue ? printedDate.Value.ToString(companyService.GetDateTimeFormat(CompanySettingsNames.DateFormat)) : DateTime.Now.Date.ToString(companyService.GetDateTimeFormat(CompanySettingsNames.DateFormat));

            return model;
        }

        public PdfHeaderAdvisorViewModel GetPdfHeaderAdvisorViewModel(int userId)
        {
            var model = new PdfHeaderAdvisorViewModel();

            var users = IocManager.Resolve<IUserService>();
            var advisor = users.GetViewModel(userId);

            model.AdvisorCode = advisor.AdvisorCode;
            model.AdvisorPhoneNumber = advisor.PhoneNumber;
            model.AdvisorName = advisor.FullName;
            model.AdvisorEmail = advisor.Email;

            return model;
        }

        public PdfHeaderDealerViewModel GetPdfHeaderDealerViewModel()
        {
            var model = new PdfHeaderDealerViewModel();
            var appContext = IocManager.Resolve<IAppContext>();
            var companies = IocManager.Resolve<IConfigurationSetupService>();

            var dealer = companies.GetProfileSetupViewModel(appContext.CurrentIdentity.TenantId);

            model.CompanyAddress = dealer.Address;
            model.CompanyName = dealer.Name;
            model.CompanyPhone = dealer.PhoneNumber;
            model.CompanyWebSite = dealer.Web;

            return model;
        }
    }
}
