﻿using System;
using System.Collections.Generic;
using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System.Linq;
using SmartVMA.Core.InputModels.PageRequests;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class CompanyGroupRepository : BaseTenantRepository<CompanyGroup, int>, ICompanyGroupRepository
    {
        public CompanyGroupRepository(Models dbContext, IAppContext appContext) : base(dbContext, appContext)
        {
        }

        public IEnumerable<CompanyGroup> GetAllDealerGroups()
        {
            return GetDbSet("Company", "CompanyCompanyGroups");
        }

        public void Update(CompanyGroup companyGroup, IEnumerable<CompanyCompanyGroup> companyCompanyGroups)
        {
            Update(companyGroup);
            var companyCompanyGroupRepository = IocManager.Resolve<ICompanyCompanyGroupRepository>();

            var currentGroups = new List<CompanyCompanyGroup>(companyGroup.CompanyCompanyGroups);
            foreach (var item in currentGroups)
            {
                if (!companyCompanyGroups.Any(x => x.CompanyGroupId == item.CompanyGroupId && x.TenantId == item.TenantId))
                {
                    companyCompanyGroupRepository.Delete(item);
                }
            }

            foreach (var item in companyCompanyGroups)
            {
                if (!currentGroups.Any(x => x.CompanyGroupId == item.CompanyGroupId && x.TenantId == item.TenantId))
                {
                    companyCompanyGroupRepository.Insert(item);
                    companyGroup.CompanyCompanyGroups.Add(item);
                }
            }
        }


        public void Insert(CompanyGroup companyGroup, IEnumerable<CompanyCompanyGroup> companyCompanyGroups)
        {
            Insert(companyGroup);
            var companyCompanyGroupRepository = IocManager.Resolve<ICompanyCompanyGroupRepository>();
            foreach (var item in companyCompanyGroups)
            {
                companyCompanyGroupRepository.Insert(item);
                companyGroup.CompanyCompanyGroups.Add(item);
            }
        }

        public int GetPagedResultsTotal(PagingRequest request)
        {
            var result = GetDbSet("Company", "CompanyCompanyGroups");
            if (request.Search != null && !string.IsNullOrEmpty(request.Search.Value))
            {
                result = result.Where(x => x.Description.ToLower().Contains(request.Search.Value.ToLower()) || x.Name.ToLower().Contains(request.Search.Value.ToLower()));
            }
            return result.Count();
        }

        public IEnumerable<CompanyGroup> GetPagedResults(PagingRequest request)
        {
            var result = GetDbSet("Company", "CompanyCompanyGroups");

            var columnsSort = new Dictionary<int, string> {
                {0, "Name"},
                {1, "Company.CompanyName"}
            };

            if (request.Search != null && !string.IsNullOrEmpty(request.Search.Value))
            {
                result = result.Where(x => x.Company.CompanyName.ToLower().Contains(request.Search.Value.ToLower()) || x.Name.ToLower().Contains(request.Search.Value.ToLower()));
            }

            var order = request.Order.FirstOrDefault();
            if (order.Dir == "asc")
            {
                result = result.OrderByTest(columnsSort[order.Column]).Skip(request.Start).Take(request.Length);
            }
            else
            {
                result = result.OrderByDescendingTest(columnsSort[order.Column]).Skip(request.Start).Take(request.Length);
            }
            return result;
        }
    }
}
