﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class AppointmentServiceRepository : BaseTenantRepository<AppointmentService, long>, IAppointmentServiceRepository
    {
        public AppointmentServiceRepository(Models dbContext, IAppContext appContext) : base(dbContext, appContext)
        {
        }
    }
}
