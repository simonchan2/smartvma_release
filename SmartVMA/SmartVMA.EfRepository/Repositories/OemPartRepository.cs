﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class OemPartRepository : BaseRepository<OemPart, int>, IOemPartRepository
    {
        public OemPartRepository(Models dbContext) : base(dbContext)
        {            
        }

        public OemPart GetOemPartById(int id)
        {
            return GetDbSet("OemComponent", "Car").FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<OemPart> GetAllOemPart()
        {
            return GetDbSet("OemComponent", "Car").AsEnumerable();
        }

        public IEnumerable<string> GetDistinctViscosity()
        {
           return GetDbSet().Where(x => x.IsEngineOil == true && !String.IsNullOrEmpty(x.FluidViscosity)).OrderBy(x => x.FluidViscosity).Select(x => x.FluidViscosity).Distinct();
        }

        public IEnumerable<string> GetDistinctOilTypes()
        {
            return GetDbSet().Where(x => !String.IsNullOrEmpty(x.EngineOilType)).OrderBy(x => x.EngineOilType).Select(x => x.EngineOilType).Distinct();
        }
    }
}
