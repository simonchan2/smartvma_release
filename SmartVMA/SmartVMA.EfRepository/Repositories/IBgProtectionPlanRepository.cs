﻿using System.Collections.Generic;
using System.Linq;
using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.Ioc;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class BgProtectionPlanRepository : BaseRepository<BgProtectionPlan, int>, IBgProtectionPlanRepository
    {
        public BgProtectionPlanRepository(Models dbContext) : base(dbContext)
        {
        }

        public IEnumerable<BgProtectionPlan> GetByProducts(List<int> productIds)
        {
            return GetDbSet("BgProductsProtectionPlans").Where(x => x.BgProductsProtectionPlans.Any(y => productIds.Any(z => z == y.BgProductId)));
        }
    }
}
