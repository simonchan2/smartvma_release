﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class VehicleRepository : BaseRepository<Vehicle, int>, IVehicleRepository
    {
        public VehicleRepository(Models dbContext) : base(dbContext)
        {
        }

        public Vehicle GetVehicle(int carId, string vin)
        {
            string[] includereference = new string[4] { "Car", "Car.VehicleMake", "Car.VehicleModel", "Car.VehicleEngine" };
            return GetDbSet(includereference).FirstOrDefault(x => x.CarId == carId && x.VIN == vin);
        }

        public bool IsExistingVINForOtherCar(int carId, string vin)
        {
            var vehicle = GetDbSet().FirstOrDefault(x => x.CarId != null && x.CarId != carId && x.VIN == vin);

            return vehicle != null;
        }

        public Vehicle GetVehicleForNewCustomer(int carId, string vin)
        {
            var vehicle = GetDbSet().Where(x => x.CarId == carId && x.VIN == vin).FirstOrDefault();
            if (vehicle == null)
            {
                var allVinVehicles = GetDbSet().Where(x => x.VIN != null && x.VIN.Trim() == vin).ToList();
                vehicle = allVinVehicles.Where(x => x.CarId == null).FirstOrDefault();
            }

            return vehicle;
        }

    }
}
