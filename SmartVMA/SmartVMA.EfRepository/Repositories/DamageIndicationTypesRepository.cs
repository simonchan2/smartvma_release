﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Enums;
using SmartVMA.Core.InputModels.ViewModels;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.AppContext;
using SmartVMA.Infrastructure.Ioc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class DamageIndicationTypesRepository : BaseRepository<DamageIndicationType, int>, IDamageIndicationTypeRepository
    {
        public DamageIndicationTypesRepository(Models dbContext, IAppContext appContext) : base(dbContext)
        {
        }

        public DamageIndicationType Get(int Id)
        {
            var obj = FirstOrDefault(x => x.Id == Id);
            return obj;
        }

        public IEnumerable<DamageIndicationType> ListAll()
        {
            var list = Find();
            return list;
        }

    }
}