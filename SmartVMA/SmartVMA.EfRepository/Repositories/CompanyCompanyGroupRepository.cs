﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.Ioc;
using SmartVMA.Infrastructure.AppContext;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class CompanyCompanyGroupRepository : BaseTenantRepository<CompanyCompanyGroup, int>, ICompanyCompanyGroupRepository
    {
        public CompanyCompanyGroupRepository(Models dbContext, IAppContext appContext) : base(dbContext, appContext)
        {
        }
    }
}
