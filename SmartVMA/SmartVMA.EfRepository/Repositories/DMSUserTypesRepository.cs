﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.Ioc;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class DMSUserTypesRepository : BaseRepository<DmsUserType, int>, IDMSUserTypesRepository
    {
        public DMSUserTypesRepository(Models dbContext) : base(dbContext)
        {
        }
    }
}
