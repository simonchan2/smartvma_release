﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.Ioc;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class BgSubProtectionPlanRepository : BaseRepository<BgProtectionSubPlan, int>, IBgSubProtectionPlanRepository
    {
        public BgSubProtectionPlanRepository(Models dbContext) : base(dbContext)
        {
        }
    }
}
