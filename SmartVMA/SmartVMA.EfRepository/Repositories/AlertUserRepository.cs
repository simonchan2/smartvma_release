﻿using SmartVMA.Core.Entities;
using SmartVMA.Core.Repositories;
using SmartVMA.Infrastructure.Ioc;
using System.Collections.Generic;
using System.Linq;

namespace SmartVMA.EfRepository.Repositories
{
    [IocBindable]
    internal class AlertUserRepository : BaseRepository<AlertsUser, int>, IAlertUserRepository
    {
        public AlertUserRepository(Models dbContext) : base(dbContext)
        {
        }

        public IEnumerable<AlertsUser> GetAllNotEndedAlertsForUser()
        {
            return GetDbSet("Alert", "User", "AlertsStatus");
        }

        public AlertsUser GetByUserIdAndAlertId(long userId, int alertId)
        {
            return GetDbSet().Where(x => x.UserId == userId && x.AlertId == alertId).FirstOrDefault();
        }



    }
}
