namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("amam.OemComponents")]
    public partial class OemComponent
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OemComponent()
        {
            OemBasicServices = new HashSet<OemBasicService>();
            OemMenuLists = new HashSet<OemMenuList>();
            OemMenuLists1 = new HashSet<OemMenuList>();
            OemMenus = new HashSet<OemMenu>();
            OemParts = new HashSet<OemPart>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int OemComponentId { get; set; }

        [Required]
        [StringLength(200)]
        public string Description { get; set; }

        public bool IsFluid { get; set; }

        public bool IsEngineOil { get; set; }

        public bool IsLOF { get; set; }

        public bool ToBeIgnored { get; set; }

        public int? LaborTimeRuleId { get; set; }

        public int? PartRuleId { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemBasicService> OemBasicServices { get; set; }

        public virtual OemComponentRule OemComponentRule { get; set; }

        public virtual OemComponentRule OemComponentRule1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemMenuList> OemMenuLists { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemMenuList> OemMenuLists1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemMenu> OemMenus { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemPart> OemParts { get; set; }
    }
}
