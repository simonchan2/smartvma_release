namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UserCompanyRole
    {
        public long Id { get; set; }

        public long UserId { get; set; }

        public int RoleId { get; set; }

        public int CompanyId { get; set; }

        public DateTime TimeUpdated { get; set; }

        public DateTime TimeCreated { get; set; }

        public virtual Company Company { get; set; }

        public virtual Role Role { get; set; }

        public virtual User User { get; set; }
    }
}
