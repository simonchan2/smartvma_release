namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("vinquery.VinMaster")]
    public partial class VinMaster
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public VinMaster()
        {
            Vehicles = new HashSet<Vehicle>();
        }

        public int VinMasterId { get; set; }

        [Required]
        [StringLength(15)]
        public string VinShort { get; set; }

        [Required]
        [StringLength(25)]
        public string VIN { get; set; }

        public int VehicleYear { get; set; }

        [Required]
        [StringLength(50)]
        public string VehicleMake { get; set; }

        [Required]
        [StringLength(50)]
        public string VehicleModel { get; set; }

        [StringLength(50)]
        public string VehicleEngine { get; set; }

        [StringLength(120)]
        public string VehicleTransmission { get; set; }

        [StringLength(50)]
        public string VehicleDriveLine { get; set; }

        [StringLength(50)]
        public string VehicleSteering { get; set; }

        [StringLength(50)]
        public string VehicleTireSize { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public DateTime TimeChecked { get; set; }

        public int? VinMasterExtId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Vehicle> Vehicles { get; set; }
    }
}
