namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("bg.VideoStatusCodes")]
    public partial class VideoStatusCode
    {
        public int VideoStatusCodeId { get; set; }

        [Required]
        [StringLength(50)]
        public string Description { get; set; }

        public string Comment { get; set; }

        public DateTime TimeUpdated { get; set; }

        public DateTime TimeCreated { get; set; }
    }
}
