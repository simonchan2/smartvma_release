namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("amam.OemBasicServices")]
    public partial class OemBasicService
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OemBasicService()
        {
            OemServiceParts = new HashSet<OemServicePart>();
            OemServices = new HashSet<OemService>();
        }

        public int OemBasicServiceId { get; set; }

        public int CarId { get; set; }

        public int OemComponentId { get; set; }

        public int? OemServiceExceptionId { get; set; }

        [Required]
        [StringLength(50)]
        public string OpAction { get; set; }

        [StringLength(1000)]
        public string OpDescription { get; set; }

        [StringLength(10)]
        public string LaborSkillLevel { get; set; }

        public double LaborHour { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [StringLength(2)]
        public string VehicleTransmission { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public bool? IsIncomplete { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public bool NoOverwrite { get; set; }

        public virtual Car Car { get; set; }

        public virtual OemComponent OemComponent { get; set; }

        public virtual OemServiceException OemServiceException { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemServicePart> OemServiceParts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemService> OemServices { get; set; }
    }
}
