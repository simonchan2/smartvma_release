namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("bg.BgProtectionPlanVehicles")]
    public partial class BgProtectionPlanVehicle
    {
        public int BgProtectionPlanVehicleId { get; set; }

        public int InvoiceId { get; set; }

        public int BgServiceId { get; set; }

        public int VehicleId { get; set; }

        public int BgProtectionPlanId { get; set; }

        public int MeasurementUnitId { get; set; }

        public short ProtectionPlanLevel { get; set; }

        [Column(TypeName = "date")]
        public DateTime DateServed { get; set; }

        public int Mileage { get; set; }

        public int MileageNext { get; set; }

        [Column(TypeName = "date")]
        public DateTime DateNext { get; set; }

        public int RecordStatus { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual BgProtectionPlan BgProtectionPlan { get; set; }

        public virtual BgService BgService { get; set; }

        public virtual Invoice Invoice { get; set; }

        public virtual MeasurementUnit MeasurementUnit { get; set; }

        public virtual Vehicle Vehicle { get; set; }
    }
}
