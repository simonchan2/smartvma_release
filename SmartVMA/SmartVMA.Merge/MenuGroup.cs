namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("cm.MenuGroups")]
    public partial class MenuGroup
    {
        public int MenuGroupId { get; set; }

        public int DealerId { get; set; }

        [Required]
        [StringLength(45)]
        public string Idbrd { get; set; }

        [Required]
        [StringLength(50)]
        public string VehicleMake { get; set; }

        [Required]
        [StringLength(50)]
        public string VehicleModel { get; set; }

        public int VehicleYear { get; set; }

        [Required]
        [StringLength(50)]
        public string VehicleEngine { get; set; }

        [StringLength(120)]
        public string VehicleTransmition { get; set; }

        [StringLength(50)]
        public string VehicleDriveLine { get; set; }

        [StringLength(50)]
        public string VehicleSteering { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual Company Company { get; set; }
    }
}
