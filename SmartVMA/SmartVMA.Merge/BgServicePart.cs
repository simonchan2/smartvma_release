namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("bg.BgServiceParts")]
    public partial class BgServicePart
    {
        public int BgServicePartId { get; set; }

        public int BgServiceId { get; set; }

        public int BgProductId { get; set; }

        public int Quantity { get; set; }

        public string Comment { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual BgProduct BgProduct { get; set; }

        public virtual BgService BgService { get; set; }
    }
}
