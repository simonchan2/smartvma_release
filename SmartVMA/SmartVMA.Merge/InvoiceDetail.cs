namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dealer.InvoiceDetails")]
    public partial class InvoiceDetail
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public InvoiceDetail()
        {
            InvoiceDetailParts = new HashSet<InvoiceDetailPart>();
        }

        public long InvoiceDetailId { get; set; }

        public int DealerId { get; set; }

        public int InvoiceId { get; set; }

        public int InvoiceLineNo { get; set; }

        [Required]
        [StringLength(5)]
        public string InvoiceLineCode { get; set; }

        [StringLength(50)]
        public string LineAfter { get; set; }

        [StringLength(50)]
        public string OpCode { get; set; }

        [StringLength(255)]
        public string OpDescription { get; set; }

        [StringLength(15)]
        public string LaborType { get; set; }

        public double? LaborHour { get; set; }

        [Column(TypeName = "money")]
        public decimal? LaborCost { get; set; }

        [Column(TypeName = "money")]
        public decimal? LaborSale { get; set; }

        [Column(TypeName = "money")]
        public decimal? PartSale { get; set; }

        [Column(TypeName = "money")]
        public decimal? PartCost { get; set; }

        [StringLength(255)]
        public string TechCode { get; set; }

        [StringLength(45)]
        public string TechName { get; set; }

        [StringLength(1000)]
        public string Complaint { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? PartitionElement { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public int? InvoiceDetailExtId { get; set; }

        public virtual Company Company { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvoiceDetailPart> InvoiceDetailParts { get; set; }

        public virtual Invoice Invoice { get; set; }
    }
}
