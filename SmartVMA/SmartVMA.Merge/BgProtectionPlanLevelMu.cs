namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("bg.BgProtectionPlanLevelMus")]
    public partial class BgProtectionPlanLevelMu
    {
        public int BgProtectionPlanLevelId { get; set; }

        public int MeasurementUnitId { get; set; }

        public int BgProtectionPlanLevelMuId { get; set; }

        public int MaxMileageOfFirstService { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual BgProtectionPlanLevel BgProtectionPlanLevel { get; set; }

        public virtual MeasurementUnit MeasurementUnit { get; set; }
    }
}
