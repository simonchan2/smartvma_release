namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("amam.VehicleEngines")]
    public partial class VehicleEngine
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public VehicleEngine()
        {
            Cars = new HashSet<Car>();
            OemLofMenuExpandedORules = new HashSet<OemLofMenuExpandedORule>();
            OemMenuExpandedORules = new HashSet<OemMenuExpandedORule>();
            OemServiceExpandedORules = new HashSet<OemServiceExpandedORule>();
        }

        public int VehicleEngineId { get; set; }

        [Column("VehicleEngine")]
        [Required]
        [StringLength(50)]
        public string VehicleEngine1 { get; set; }

        public byte? NumberOfCylinders { get; set; }

        public int VehicleEngineExtId { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Car> Cars { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemLofMenuExpandedORule> OemLofMenuExpandedORules { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemMenuExpandedORule> OemMenuExpandedORules { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemServiceExpandedORule> OemServiceExpandedORules { get; set; }
    }
}
