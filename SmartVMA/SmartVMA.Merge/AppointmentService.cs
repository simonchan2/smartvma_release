namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dealer.AppointmentServices")]
    public partial class AppointmentService
    {
        [Key]
        public long AppointmentServicesId { get; set; }

        public long AppointmentPresentationId { get; set; }

        public int? BgProdSubcategoryId { get; set; }

        public int DealerId { get; set; }

        [StringLength(50)]
        public string OpCode { get; set; }

        [StringLength(255)]
        public string OpDescription { get; set; }

        public double? LaborHour { get; set; }

        [Column(TypeName = "money")]
        public decimal? LaborRate { get; set; }

        [Column(TypeName = "money")]
        public decimal? PartsPrice { get; set; }

        public bool IsStriked { get; set; }

        public string Comment { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? PartitionElement { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public double? LaborPrice { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public double? Price { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual BgProdSubcategory BgProdSubcategory { get; set; }

        public virtual Company Company { get; set; }

        public virtual AppointmentPresentation AppointmentPresentation { get; set; }
    }
}
