namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dealer.InvoiceDetailParts")]
    public partial class InvoiceDetailPart
    {
        public long InvoiceDetailPartId { get; set; }

        public int DealerId { get; set; }

        public long InvoiceDetailId { get; set; }

        [StringLength(50)]
        public string SequenceNumber { get; set; }

        [Required]
        [StringLength(50)]
        public string PartCode { get; set; }

        [Required]
        [StringLength(255)]
        public string PartDescription { get; set; }

        public double PartQty { get; set; }

        [Column(TypeName = "money")]
        public decimal PartCost { get; set; }

        [Column(TypeName = "money")]
        public decimal PartSale { get; set; }

        [Column(TypeName = "money")]
        public decimal? PartDiscount { get; set; }

        [Column(TypeName = "date")]
        public DateTime? PartDate { get; set; }

        public string Comment { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? PartitionElement { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public int? InvoiceDetailPartExtId { get; set; }

        public virtual Company Company { get; set; }

        public virtual InvoiceDetail InvoiceDetail { get; set; }
    }
}
