namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("bg.DealerBgServices")]
    public partial class DealerBgService
    {
        public int DealerBgServiceId { get; set; }

        public int DealerId { get; set; }

        public int BgServiceId { get; set; }

        [Required]
        [StringLength(50)]
        public string OpCode { get; set; }

        [Required]
        [StringLength(255)]
        public string OpDescription { get; set; }

        public int Goal { get; set; }

        [Column(TypeName = "money")]
        public decimal Price { get; set; }

        [Column(TypeName = "money")]
        public decimal Commision { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual BgService BgService { get; set; }

        public virtual Company Company { get; set; }
    }
}
