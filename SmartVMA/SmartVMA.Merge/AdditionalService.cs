namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dealer.AdditionalServices")]
    public partial class AdditionalService
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AdditionalService()
        {
            AdditionalServiceParts = new HashSet<AdditionalServicePart>();
        }

        public long AdditionalServiceId { get; set; }

        public int DealerId { get; set; }

        [StringLength(50)]
        public string OpCode { get; set; }

        [StringLength(255)]
        public string OpDescription { get; set; }

        public double? LaborHour { get; set; }

        [Column(TypeName = "money")]
        public decimal? LaborRate { get; set; }

        public string Comment { get; set; }

        public bool IsLofService { get; set; }

        public bool CanBeStriked { get; set; }

        public bool IsFixLabor { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? PartitionElement { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual Company Company { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AdditionalServicePart> AdditionalServiceParts { get; set; }
    }
}
