namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("rprint.PrnConfigurations")]
    public partial class PrnConfiguration
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PrnConfiguration()
        {
            PrnConfigPrinters = new HashSet<PrnConfigPrinter>();
        }

        public int PrnConfigurationId { get; set; }

        public int DealerId { get; set; }

        public long? UserId { get; set; }

        public int PrnJobTypeId { get; set; }

        public bool ShowOnScreen { get; set; }

        public DateTime TimeUpdated { get; set; }

        public DateTime TimeCreated { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PrnConfigPrinter> PrnConfigPrinters { get; set; }

        public virtual PrnJobType PrnJobType { get; set; }
    }
}
