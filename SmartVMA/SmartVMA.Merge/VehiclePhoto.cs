namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("vi.VehiclePhotos")]
    public partial class VehiclePhoto
    {
        public long Id { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public long InspectionId { get; set; }

        [Required]
        [StringLength(255)]
        public string Url { get; set; }
    }
}
