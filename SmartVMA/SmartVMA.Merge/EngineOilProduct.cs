namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("amam.EngineOilProducts")]
    public partial class EngineOilProduct
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public EngineOilProduct()
        {
            EngineOilORules = new HashSet<EngineOilORule>();
        }

        public int EngineOilProductId { get; set; }

        public int DealerId { get; set; }

        [StringLength(50)]
        public string PartNumber { get; set; }

        [StringLength(255)]
        public string PartName { get; set; }

        [Column(TypeName = "money")]
        public decimal? UnitPrice { get; set; }

        [StringLength(100)]
        public string FluidType { get; set; }

        [StringLength(25)]
        public string FluidViscosity { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? PartitionElement { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EngineOilORule> EngineOilORules { get; set; }

        public virtual Company Company { get; set; }
    }
}
