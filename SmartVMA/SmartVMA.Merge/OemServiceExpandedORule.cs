namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dealer.OemServiceExpandedORules")]
    public partial class OemServiceExpandedORule
    {
        public long OemServiceExpandedORuleId { get; set; }

        public int OemServiceORuleId { get; set; }

        public int DealerId { get; set; }

        public int? VehicleYear { get; set; }

        public int? VehicleMakeId { get; set; }

        public int? VehicleModelId { get; set; }

        public int? VehicleEngineId { get; set; }

        [StringLength(120)]
        public string VehicleTransmission { get; set; }

        [StringLength(50)]
        public string VehicleDriveline { get; set; }

        [StringLength(50)]
        public string VehicleSteering { get; set; }

        [StringLength(100)]
        public string TextFilter { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? PartitionElement { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual VehicleEngine VehicleEngine { get; set; }

        public virtual VehicleMake VehicleMake { get; set; }

        public virtual VehicleModel VehicleModel { get; set; }

        public virtual Company Company { get; set; }

        public virtual OemServiceORule OemServiceORule { get; set; }
    }
}
