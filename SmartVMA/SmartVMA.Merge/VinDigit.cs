namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class VinDigit
    {
        public int VinDigitId { get; set; }

        [Required]
        [StringLength(1)]
        public string Digit { get; set; }

        public int Value { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }
    }
}
