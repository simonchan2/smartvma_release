namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("amam.OemPartNumbers")]
    public partial class OemPartNumber
    {
        public int OemPartNumberId { get; set; }

        public int OemPartId { get; set; }

        public int CountryId { get; set; }

        [Required]
        [StringLength(50)]
        public string OemPartNo { get; set; }

        public bool NoOverwrite { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }
    }
}
