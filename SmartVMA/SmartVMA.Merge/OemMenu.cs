namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("amam.OemMenus")]
    public partial class OemMenu
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OemMenu()
        {
            OemMenuMileages = new HashSet<OemMenuMileage>();
            OemServices = new HashSet<OemService>();
        }

        public int OemMenuId { get; set; }

        public int OemMenuListId { get; set; }

        public int OemComponentId { get; set; }

        [StringLength(50)]
        public string ExtInfoChecksum { get; set; }

        public bool NoOverwrite { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual OemComponent OemComponent { get; set; }

        public virtual OemMenuList OemMenuList { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemMenuMileage> OemMenuMileages { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemService> OemServices { get; set; }
    }
}
