namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("msg.AlertsUsers")]
    public partial class AlertsUser
    {
        public int Id { get; set; }

        public long UserId { get; set; }

        public int AlertId { get; set; }

        public int StatusId { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual User User { get; set; }

        public virtual Alert Alert { get; set; }
    }
}
