namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("amam.OemServices")]
    public partial class OemService
    {
        public int OemServiceId { get; set; }

        public int OemMenuId { get; set; }

        public int OemBasicServiceId { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public bool NoOverwrite { get; set; }

        public virtual OemBasicService OemBasicService { get; set; }

        public virtual OemMenu OemMenu { get; set; }
    }
}
