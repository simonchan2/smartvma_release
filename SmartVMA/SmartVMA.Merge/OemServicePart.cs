namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("amam.OemServiceParts")]
    public partial class OemServicePart
    {
        public int OemServicePartId { get; set; }

        public int OemBasicServiceId { get; set; }

        public int OemPartId { get; set; }

        public double Quantity { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public bool NoOverwrite { get; set; }

        public virtual OemBasicService OemBasicService { get; set; }

        public virtual OemPart OemPart { get; set; }
    }
}
