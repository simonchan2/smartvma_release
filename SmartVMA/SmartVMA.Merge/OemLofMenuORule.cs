namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dealer.OemLofMenuORules")]
    public partial class OemLofMenuORule
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OemLofMenuORule()
        {
            OemLofMenuExpandedORules = new HashSet<OemLofMenuExpandedORule>();
        }

        public int OemLofMenuORuleId { get; set; }

        public int DealerId { get; set; }

        [Column(TypeName = "xml")]
        [Required]
        public string Filter { get; set; }

        [Column(TypeName = "xml")]
        [Required]
        public string OverrideRule { get; set; }

        public int RecordStatus { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? PartitionElement { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual Company Company { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemLofMenuExpandedORule> OemLofMenuExpandedORules { get; set; }
    }
}
