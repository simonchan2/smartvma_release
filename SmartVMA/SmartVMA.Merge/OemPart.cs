namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("amam.OemParts")]
    public partial class OemPart
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OemPart()
        {
            OemServiceParts = new HashSet<OemServicePart>();
        }

        public int OemPartId { get; set; }

        public int CarId { get; set; }

        public int? OemComponentId { get; set; }

        [Required]
        [StringLength(100)]
        public string OemPartName { get; set; }

        [StringLength(10)]
        public string Unit { get; set; }

        [Column(TypeName = "money")]
        public decimal UnitPrice { get; set; }

        [StringLength(100)]
        public string FluidType { get; set; }

        [StringLength(25)]
        public string FluidViscosity { get; set; }

        [StringLength(10)]
        public string LaborSkillLevel { get; set; }

        public double LaborHour { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public bool NoOverwrite { get; set; }

        public int? OemPartTypeId { get; set; }

        public virtual Car Car { get; set; }

        public virtual OemComponent OemComponent { get; set; }

        public virtual OemPartType OemPartType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemServicePart> OemServiceParts { get; set; }
    }
}
