namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Role
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Role()
        {
            RolePermissions = new HashSet<RolePermission>();
            UserCompanyRoles = new HashSet<UserCompanyRole>();
        }

        public int RoleId { get; set; }

        [Column("Role")]
        [Required]
        [StringLength(100)]
        public string Role1 { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        public DateTime TimeCreated { get; set; }

        [Column(TypeName = "xml")]
        [Required]
        public string Configuration { get; set; }

        public string Comment { get; set; }

        public DateTime TimeUpdated { get; set; }

        public bool IsSystemRole { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RolePermission> RolePermissions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserCompanyRole> UserCompanyRoles { get; set; }
    }
}
