namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("rprint.PrnSpool")]
    public partial class PrnSpool
    {
        public int id { get; set; }

        [Required]
        [StringLength(45)]
        public string account { get; set; }

        public int idhist { get; set; }

        [Required]
        [StringLength(45)]
        public string doctype { get; set; }

        [Required]
        [StringLength(255)]
        public string docname { get; set; }

        [Required]
        [StringLength(255)]
        public string prnname { get; set; }

        [Required]
        [StringLength(255)]
        public string prndriver { get; set; }

        public int prncopy { get; set; }

        [Required]
        [StringLength(255)]
        public string prnport { get; set; }

        [Required]
        [StringLength(45)]
        public string prntray { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime datecreated { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? dateprinted { get; set; }

        [Required]
        [StringLength(45)]
        public string status { get; set; }

        [Required]
        [StringLength(255)]
        public string comment { get; set; }

        [Required]
        [StringLength(45)]
        public string assno { get; set; }

        [Required]
        [StringLength(255)]
        public string session { get; set; }

        public DateTime TimeUpdated { get; set; }

        public DateTime TimeCreated { get; set; }
    }
}
