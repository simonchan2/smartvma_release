namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dealer.Inspection")]
    public partial class Inspection
    {
        public long Id { get; set; }

        public bool LFRimScratch { get; set; }

        public bool LRRimScratch { get; set; }

        public bool RFRimScratch { get; set; }

        public bool RRRimScratch { get; set; }

        public int? LFTireTypeId { get; set; }

        public int? LRTireTypeId { get; set; }

        public int? RFTireTypeId { get; set; }

        public int? RRTireTypeId { get; set; }

        public int? Fuel { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public long? AppointmentPresentationId { get; set; }

        public int DealerId { get; set; }

        public virtual Company Company { get; set; }

        public virtual AppointmentPresentation AppointmentPresentation { get; set; }
    }
}
