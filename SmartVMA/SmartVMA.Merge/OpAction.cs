namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("amam.OpActions")]
    public partial class OpAction
    {
        public int OpActionId { get; set; }

        [Column("OpAction")]
        [Required]
        [StringLength(50)]
        public string OpAction1 { get; set; }

        public bool IsRequired { get; set; }

        public string Comment { get; set; }

        public DateTime TimeUpdated { get; set; }

        public DateTime TimeCreated { get; set; }
    }
}
