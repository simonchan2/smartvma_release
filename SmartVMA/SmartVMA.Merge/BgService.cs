namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("bg.BgServices")]
    public partial class BgService
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BgService()
        {
            BgProtectionPlanVehicles = new HashSet<BgProtectionPlanVehicle>();
            BgServiceParts = new HashSet<BgServicePart>();
            DealerBgServices = new HashSet<DealerBgService>();
        }

        public int BgProdSubcategoryId { get; set; }

        public int BgServiceId { get; set; }

        [Required]
        [StringLength(50)]
        public string OpCode { get; set; }

        [Required]
        [StringLength(255)]
        public string OpDescription { get; set; }

        public double LaborHour { get; set; }

        [Column(TypeName = "money")]
        public decimal Price { get; set; }

        public bool WithProtectionPlan { get; set; }

        public short? SortOrder { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual BgProdSubcategory BgProdSubcategory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BgProtectionPlanVehicle> BgProtectionPlanVehicles { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BgServicePart> BgServiceParts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DealerBgService> DealerBgServices { get; set; }
    }
}
