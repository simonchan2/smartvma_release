namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("amam.OemComponentRules")]
    public partial class OemComponentRule
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OemComponentRule()
        {
            OemComponents = new HashSet<OemComponent>();
            OemComponents1 = new HashSet<OemComponent>();
        }

        public int OemComponentRuleId { get; set; }

        public int OemComponentRuleTypeId { get; set; }

        [Required]
        [StringLength(250)]
        public string Description { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual OemComponentRuleType OemComponentRuleType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemComponent> OemComponents { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemComponent> OemComponents1 { get; set; }
    }
}
