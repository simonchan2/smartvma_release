namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ilife.ILife")]
    public partial class ILife
    {
        public int ILifeId { get; set; }

        [Required]
        [StringLength(25)]
        public string Store { get; set; }

        [Required]
        [StringLength(100)]
        public string CoName { get; set; }

        [Column("Record ID")]
        [Required]
        [StringLength(15)]
        public string Record_ID { get; set; }

        [Column("Past Due")]
        public bool Past_Due { get; set; }

        public bool Active { get; set; }

        [Column("Lease Term Date", TypeName = "date")]
        public DateTime Lease_Term_Date { get; set; }

        [Required]
        [StringLength(15)]
        public string Prefix { get; set; }

        [Column("Contract Number")]
        [Required]
        [StringLength(15)]
        public string Contract_Number { get; set; }

        [Column("Contract Date", TypeName = "date")]
        public DateTime? Contract_Date { get; set; }

        [Column("Cancel Date", TypeName = "date")]
        public DateTime? Cancel_Date { get; set; }

        [Column("Deal No")]
        [StringLength(15)]
        public string Deal_No { get; set; }

        [Column("Stock Num")]
        [StringLength(25)]
        public string Stock_Num { get; set; }

        [Column("Sale Price", TypeName = "money")]
        public decimal Sale_Price { get; set; }

        [Column(TypeName = "money")]
        public decimal Surcharge { get; set; }

        [Required]
        [StringLength(15)]
        public string STD_EXE { get; set; }

        [Required]
        [StringLength(15)]
        public string GDS { get; set; }

        [Column("First Name")]
        [StringLength(150)]
        public string First_Name { get; set; }

        [Column("Middle Initial")]
        [StringLength(25)]
        public string Middle_Initial { get; set; }

        [Column("Last Name")]
        [StringLength(150)]
        public string Last_Name { get; set; }

        [Column("Contract Holder 2")]
        [StringLength(150)]
        public string Contract_Holder_2 { get; set; }

        [StringLength(100)]
        public string Address { get; set; }

        [StringLength(100)]
        public string City { get; set; }

        [StringLength(50)]
        public string State { get; set; }

        [StringLength(50)]
        public string Zip { get; set; }

        [Column("Sold As (New/Used)")]
        [StringLength(15)]
        public string Sold_As__New_Used_ { get; set; }

        public int? Mileage { get; set; }

        [Required]
        [StringLength(25)]
        public string VIN { get; set; }

        [Column("Vin Last 8")]
        [StringLength(15)]
        public string Vin_Last_8 { get; set; }

        [StringLength(50)]
        public string Make { get; set; }

        [StringLength(50)]
        public string Model { get; set; }

        [Column("Model Year")]
        public int? Model_Year { get; set; }

        [StringLength(15)]
        public string LastRONum { get; set; }

        public int? LastClaimMiles { get; set; }

        [Column(TypeName = "date")]
        public DateTime? LastClaimDate { get; set; }

        [StringLength(15)]
        public string ClaimsThisContractYear { get; set; }

        [StringLength(255)]
        public string EligibilityMessage { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }
    }
}
