namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("rprint.PrnJobTypes")]
    public partial class PrnJobType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PrnJobType()
        {
            PrnConfigurations = new HashSet<PrnConfiguration>();
        }

        public int PrnJobTypeId { get; set; }

        [Required]
        [StringLength(100)]
        public string PrnJobTypeName { get; set; }

        [Required]
        [StringLength(45)]
        public string PrnJobTypeCode { get; set; }

        public DateTime TimeUpdated { get; set; }

        public DateTime TimeCreated { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PrnConfiguration> PrnConfigurations { get; set; }
    }
}
