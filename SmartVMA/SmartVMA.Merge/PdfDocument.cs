namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("document.PdfDocuments")]
    public partial class PdfDocument
    {
        public int PdfDocumentId { get; set; }

        public int DealerCustomerId { get; set; }

        public long AppointmentPresentationId { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [StringLength(255)]
        public string FilePathName { get; set; }

        [StringLength(25)]
        public string VIN { get; set; }

        [StringLength(50)]
        public string InvoiceNumber { get; set; }

        public int DocumentType { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual AppointmentPresentation AppointmentPresentation { get; set; }

        public virtual DealerCustomer DealerCustomer { get; set; }
    }
}
