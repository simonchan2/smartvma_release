namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("bg.BgProtectionPlans")]
    public partial class BgProtectionPlan
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BgProtectionPlan()
        {
            BgProtectionPlanDocs = new HashSet<BgProtectionPlanDoc>();
            BgProtectionPlanLevels = new HashSet<BgProtectionPlanLevel>();
            BgProtectionPlanMus = new HashSet<BgProtectionPlanMu>();
            BgProtectionPlanVehicles = new HashSet<BgProtectionPlanVehicle>();
        }

        public int BgProtectionPlanId { get; set; }

        public int BgProdSubcategoryId { get; set; }

        [Required]
        [StringLength(50)]
        public string ProtectionPlanName { get; set; }

        [Required]
        [StringLength(50)]
        public string OpCode { get; set; }

        public int EstYearsToBeExpired { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual BgProdSubcategory BgProdSubcategory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BgProtectionPlanDoc> BgProtectionPlanDocs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BgProtectionPlanLevel> BgProtectionPlanLevels { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BgProtectionPlanMu> BgProtectionPlanMus { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BgProtectionPlanVehicle> BgProtectionPlanVehicles { get; set; }
    }
}
