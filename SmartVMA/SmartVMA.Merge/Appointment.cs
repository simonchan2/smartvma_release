namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dealer.Appointments")]
    public partial class Appointment
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Appointment()
        {
            AppointmentDetails = new HashSet<AppointmentDetail>();
            AppointmentPresentations = new HashSet<AppointmentPresentation>();
        }

        public long AppointmentId { get; set; }

        public int DealerCustomerId { get; set; }

        public int DealerId { get; set; }

        public int? DealerVehicleId { get; set; }

        public long? UserId { get; set; }

        [StringLength(50)]
        public string UserDmsId { get; set; }

        [StringLength(50)]
        public string AppointmentDmsId { get; set; }

        [StringLength(25)]
        public string VIN { get; set; }

        public int? VehicleYear { get; set; }

        [StringLength(50)]
        public string VehicleMake { get; set; }

        [StringLength(50)]
        public string VehicleModel { get; set; }

        [StringLength(120)]
        public string VehicleTransmission { get; set; }

        [StringLength(50)]
        public string VehicleDriveline { get; set; }

        [StringLength(50)]
        public string VehicleSteering { get; set; }

        [StringLength(50)]
        public string VehicleColor { get; set; }

        [StringLength(15)]
        public string VehicleLicense { get; set; }

        public int? Mileage { get; set; }

        public DateTime AppointmentTime { get; set; }

        [Required]
        [StringLength(10)]
        public string AppointmentStatus { get; set; }

        [StringLength(5)]
        public string AppointmentType { get; set; }

        [Required]
        [StringLength(5)]
        public string AppUse { get; set; }

        public double? AppointmentDuration { get; set; }

        public DateTime? PromisedTime { get; set; }

        [StringLength(50)]
        public string CustomerPhone { get; set; }

        public DateTime? ContactTime { get; set; }

        public bool Confirmed { get; set; }

        public bool Waiter { get; set; }

        public DateTime? ServiceTime { get; set; }

        [StringLength(25)]
        public string AdvCode { get; set; }

        [StringLength(50)]
        public string RoNumber { get; set; }

        public DateTime? DateRoCreated { get; set; }

        [StringLength(50)]
        public string NewRo { get; set; }

        [StringLength(25)]
        public string RoOpenBy { get; set; }

        [StringLength(50)]
        public string InvoiceNumber { get; set; }

        [StringLength(15)]
        public string TagNumber { get; set; }

        [StringLength(15)]
        public string TagColor { get; set; }

        public int PotNum { get; set; }

        [Required]
        [StringLength(5)]
        public string ComeBack { get; set; }

        public string Comment { get; set; }

        public int? AppointmentExtId { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [Column(TypeName = "date")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime AppointmentDate { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? PartitionElement { get; set; }

        public virtual Company Company { get; set; }

        public virtual User User { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppointmentDetail> AppointmentDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppointmentPresentation> AppointmentPresentations { get; set; }

        public virtual DealerCustomer DealerCustomer { get; set; }

        public virtual DealerVehicle DealerVehicle { get; set; }
    }
}
