namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("kb.ArticleArticleCategory")]
    public partial class ArticleArticleCategory
    {
        public int Id { get; set; }

        public int ArticleId { get; set; }

        public int ArticleCategoryId { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual ArticleCategory ArticleCategory { get; set; }

        public virtual Article Article { get; set; }
    }
}
