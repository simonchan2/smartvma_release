namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("vi.DamageIndications")]
    public partial class DamageIndication
    {
        public long Id { get; set; }

        public int TypeId { get; set; }

        [StringLength(1000)]
        public string Comment { get; set; }

        public decimal OffsetLeft { get; set; }

        public decimal OffsetTop { get; set; }

        public int ViewPointId { get; set; }

        public long InspectionId { get; set; }

        public long AppointmentPresentationId { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }
    }
}
