namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("amam.VehicleModels")]
    public partial class VehicleModel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public VehicleModel()
        {
            Cars = new HashSet<Car>();
            OemLofMenuExpandedORules = new HashSet<OemLofMenuExpandedORule>();
            OemMenuExpandedORules = new HashSet<OemMenuExpandedORule>();
            OemServiceExpandedORules = new HashSet<OemServiceExpandedORule>();
        }

        public int VehicleModelId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Required]
        [StringLength(4000)]
        public string VehicleModelShort { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [StringLength(3)]
        public string VehicleDriveline { get; set; }

        public int VehicleModelExtId { get; set; }

        [Column("VehicleModel")]
        [Required]
        [StringLength(50)]
        public string VehicleModel1 { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Car> Cars { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemLofMenuExpandedORule> OemLofMenuExpandedORules { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemMenuExpandedORule> OemMenuExpandedORules { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OemServiceExpandedORule> OemServiceExpandedORules { get; set; }
    }
}
