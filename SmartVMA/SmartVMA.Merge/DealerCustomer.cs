namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dealer.DealerCustomers")]
    public partial class DealerCustomer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DealerCustomer()
        {
            AppointmentPresentations = new HashSet<AppointmentPresentation>();
            Appointments = new HashSet<Appointment>();
            Invoices = new HashSet<Invoice>();
            PdfDocuments = new HashSet<PdfDocument>();
        }

        public int DealerCustomerId { get; set; }

        public int DealerId { get; set; }

        public int DealerCustomerStatusCodeId { get; set; }

        [StringLength(50)]
        public string CustomerDmsId { get; set; }

        [StringLength(25)]
        public string CustomerNumber { get; set; }

        [StringLength(50)]
        public string Salutation { get; set; }

        [StringLength(150)]
        public string LastName { get; set; }

        [StringLength(150)]
        public string FirstName { get; set; }

        [StringLength(25)]
        public string MiddleName { get; set; }

        [StringLength(50)]
        public string Gender { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Birthdate { get; set; }

        [StringLength(150)]
        public string Company { get; set; }

        public int? CountryId { get; set; }

        public int? CountryStateId { get; set; }

        [StringLength(100)]
        public string City { get; set; }

        [StringLength(50)]
        public string Zip { get; set; }

        [StringLength(100)]
        public string AddressLine1 { get; set; }

        [StringLength(100)]
        public string AddressLine2 { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        [StringLength(35)]
        public string CellPhone { get; set; }

        [StringLength(35)]
        public string HomePhone { get; set; }

        [StringLength(35)]
        public string WorkPhone { get; set; }

        [StringLength(35)]
        public string Fax { get; set; }

        [StringLength(10)]
        public string Credit { get; set; }

        [StringLength(255)]
        public string Action { get; set; }

        [StringLength(255)]
        public string Instructions { get; set; }

        [StringLength(1000)]
        public string Comment { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Required]
        [StringLength(327)]
        public string FullName { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? PartitionElement { get; set; }

        public int LanguageId { get; set; }

        public int? CustomerExtId { get; set; }

        public virtual Company Company1 { get; set; }

        public virtual Country Country { get; set; }

        public virtual CountryState CountryState { get; set; }

        public virtual DealerCustomerStatusCode DealerCustomerStatusCode { get; set; }

        public virtual Language Language { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppointmentPresentation> AppointmentPresentations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Appointment> Appointments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Invoice> Invoices { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PdfDocument> PdfDocuments { get; set; }
    }
}
