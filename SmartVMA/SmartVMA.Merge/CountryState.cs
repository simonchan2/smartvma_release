namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CountryState
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CountryState()
        {
            DealerCustomers = new HashSet<DealerCustomer>();
        }

        public int CountryStateId { get; set; }

        public int CountryId { get; set; }

        [Column("CountryState")]
        [Required]
        [StringLength(50)]
        public string CountryState1 { get; set; }

        [Required]
        [StringLength(10)]
        public string CountryStateShort { get; set; }

        [StringLength(10)]
        public string CountryStateCode { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual Country Country { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DealerCustomer> DealerCustomers { get; set; }
    }
}
