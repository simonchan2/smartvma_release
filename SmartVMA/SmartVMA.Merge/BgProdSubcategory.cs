namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("bg.BgProdSubcategories")]
    public partial class BgProdSubcategory
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BgProdSubcategory()
        {
            AppointmentServices = new HashSet<AppointmentService>();
            BgProtectionPlans = new HashSet<BgProtectionPlan>();
            BgServices = new HashSet<BgService>();
        }

        public int BgProdSubcategoryId { get; set; }

        public int BgProdCategoryId { get; set; }

        [Required]
        [StringLength(50)]
        public string BgSubtype { get; set; }

        [Required]
        [StringLength(100)]
        public string Description { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual BgProdCategory BgProdCategory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppointmentService> AppointmentServices { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BgProtectionPlan> BgProtectionPlans { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BgService> BgServices { get; set; }
    }
}
