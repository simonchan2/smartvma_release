namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("bg.BgProtectionPlanMus")]
    public partial class BgProtectionPlanMu
    {
        public int MeasurementUnitId { get; set; }

        public int BgProtectionPlanMuId { get; set; }

        public int BgProtectionPlanId { get; set; }

        public int ServiceInterval { get; set; }

        public int MaxMileage { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual BgProtectionPlan BgProtectionPlan { get; set; }

        public virtual MeasurementUnit MeasurementUnit { get; set; }
    }
}
