namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("amam.EngineOilORules")]
    public partial class EngineOilORule
    {
        public int EngineOilORuleId { get; set; }

        public int EngineOilProductId { get; set; }

        public int DealerId { get; set; }

        [StringLength(100)]
        public string FluidType { get; set; }

        [StringLength(25)]
        public string FluidViscosity { get; set; }

        public byte? MenuLevel { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? PartitionElement { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual Company Company { get; set; }

        public virtual EngineOilProduct EngineOilProduct { get; set; }
    }
}
