namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("bg.BgProtectionPlanLevels")]
    public partial class BgProtectionPlanLevel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BgProtectionPlanLevel()
        {
            BgProtectionPlanLevelMus = new HashSet<BgProtectionPlanLevelMu>();
        }

        public int BgProtectionPlanLevelId { get; set; }

        public int BgProtectionPlanId { get; set; }

        public byte ProtectionPlanLevel { get; set; }

        [Column(TypeName = "money")]
        public decimal MaxReimbursement { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BgProtectionPlanLevelMu> BgProtectionPlanLevelMus { get; set; }

        public virtual BgProtectionPlan BgProtectionPlan { get; set; }
    }
}
