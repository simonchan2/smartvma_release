namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Country
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Country()
        {
            CountryStates = new HashSet<CountryState>();
            DealerCustomers = new HashSet<DealerCustomer>();
        }

        public int CountryId { get; set; }

        [Column("Country")]
        [Required]
        [StringLength(25)]
        public string Country1 { get; set; }

        [Required]
        [StringLength(50)]
        public string CountryName { get; set; }

        [Required]
        [StringLength(2)]
        public string Iso3166A2 { get; set; }

        [Required]
        [StringLength(3)]
        public string Iso3166A3 { get; set; }

        [Required]
        [StringLength(3)]
        public string Iso3166Number { get; set; }

        public int? SortOrder { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CountryState> CountryStates { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DealerCustomer> DealerCustomers { get; set; }
    }
}
