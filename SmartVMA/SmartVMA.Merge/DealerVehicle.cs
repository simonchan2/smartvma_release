namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dealer.DealerVehicles")]
    public partial class DealerVehicle
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DealerVehicle()
        {
            AppointmentPresentations = new HashSet<AppointmentPresentation>();
            Appointments = new HashSet<Appointment>();
            Invoices = new HashSet<Invoice>();
        }

        public int DealerVehicleId { get; set; }

        public int? DealerCustomerId { get; set; }

        public int DealerVehicleStatusCodeId { get; set; }

        public int DealerId { get; set; }

        public int? VehicleId { get; set; }

        [StringLength(100)]
        public string VehicleDmsId { get; set; }

        [StringLength(50)]
        public string StockNo { get; set; }

        [StringLength(25)]
        public string VIN { get; set; }

        public int? VehicleYear { get; set; }

        [StringLength(50)]
        public string VehicleMake { get; set; }

        [StringLength(50)]
        public string VehicleModel { get; set; }

        [StringLength(50)]
        public string VehicleEngine { get; set; }

        [StringLength(120)]
        public string VehicleTransmission { get; set; }

        [StringLength(50)]
        public string VehicleDriveline { get; set; }

        [StringLength(50)]
        public string VehicleSteering { get; set; }

        [StringLength(50)]
        public string VehicleColor { get; set; }

        [StringLength(50)]
        public string VehicleLicense { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DeliveryDate { get; set; }

        [StringLength(1024)]
        public string Options { get; set; }

        public string Comment { get; set; }

        public int? VehicleExtId { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? PartitionElement { get; set; }

        public virtual Company Company { get; set; }

        public virtual DealerVehicleStatusCode DealerVehicleStatusCode { get; set; }

        public virtual Vehicle Vehicle { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppointmentPresentation> AppointmentPresentations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Appointment> Appointments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Invoice> Invoices { get; set; }
    }
}
