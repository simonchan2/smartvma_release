namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dealer.DealerDmsUsers")]
    public partial class DealerDmsUser
    {
        public int DealerDmsUserId { get; set; }

        public int DealerId { get; set; }

        public int? DmsUserTypeId { get; set; }

        public int? DmsUserExtId { get; set; }

        [StringLength(45)]
        public string DmsUserNumber { get; set; }

        [StringLength(100)]
        public string DmsUserName { get; set; }

        [StringLength(45)]
        public string DmsUserStatus { get; set; }

        [StringLength(45)]
        public string Location { get; set; }

        [StringLength(15)]
        public string DispCode { get; set; }

        [StringLength(15)]
        public string LaborType { get; set; }

        public byte? SkillMant { get; set; }

        public byte? SkillRep { get; set; }

        public byte? SkillWar { get; set; }

        public byte? Shift { get; set; }

        public DateTime? LunchStart { get; set; }

        public DateTime? LunchEnd { get; set; }

        [StringLength(255)]
        public string Email { get; set; }

        [StringLength(50)]
        public string Phone { get; set; }

        [StringLength(50)]
        public string Team { get; set; }

        public bool? Display { get; set; }

        [StringLength(50)]
        public string ServiceType { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public long? UserId { get; set; }

        public virtual Company Company { get; set; }

        public virtual DmsUserType DmsUserType { get; set; }

        public virtual User User { get; set; }
    }
}
