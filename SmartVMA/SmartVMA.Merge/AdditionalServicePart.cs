namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dealer.AdditionalServiceParts")]
    public partial class AdditionalServicePart
    {
        public int AdditionalServicePartId { get; set; }

        public long AdditionalServicesId { get; set; }

        public int DealerId { get; set; }

        [StringLength(20)]
        public string PartNo { get; set; }

        [Required]
        [StringLength(100)]
        public string PartName { get; set; }

        public double Quantity { get; set; }

        [Required]
        [StringLength(10)]
        public string Unit { get; set; }

        [Column(TypeName = "money")]
        public decimal UnitPrice { get; set; }

        [StringLength(100)]
        public string FluidType { get; set; }

        [StringLength(25)]
        public string FluidViscosity { get; set; }

        public bool IsEngineOil { get; set; }

        public bool IsFluid { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? PartitionElement { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual Company Company { get; set; }

        public virtual AdditionalService AdditionalService { get; set; }
    }
}
