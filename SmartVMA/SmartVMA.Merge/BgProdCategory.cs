namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("bg.BgProdCategories")]
    public partial class BgProdCategory
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BgProdCategory()
        {
            BgProdSubcategories = new HashSet<BgProdSubcategory>();
        }

        public int BgProdCategoryId { get; set; }

        [Required]
        [StringLength(20)]
        public string VehicleSystem { get; set; }

        [Required]
        [StringLength(20)]
        public string BgType { get; set; }

        [Required]
        [StringLength(100)]
        public string Description { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BgProdSubcategory> BgProdSubcategories { get; set; }
    }
}
