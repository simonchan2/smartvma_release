namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class StateTimeZone
    {
        public int StateTimeZoneId { get; set; }

        public int CountryStateId { get; set; }

        public int TimeZoneId { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }

        public virtual TimeZone TimeZone { get; set; }
    }
}
