namespace SmartVMA.Merge
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("bg.BgVideoClips")]
    public partial class BgVideoClip
    {
        public int BgVideoClipId { get; set; }

        public int LanguageId { get; set; }

        public int VideoStatusCodeId { get; set; }

        public int? BgServiceId { get; set; }

        [Required]
        [StringLength(255)]
        public string Description { get; set; }

        [Required]
        [StringLength(1000)]
        public string VideoUrl { get; set; }

        public string Comment { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeUpdated { get; set; }
    }
}
