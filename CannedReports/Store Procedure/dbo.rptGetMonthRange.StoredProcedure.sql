USE [SmartVmaPlus]
GO
/****** Object:  StoredProcedure [dbo].[rptGetMonthRange]    Script Date: 2016-08-04 8:28:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[rptGetMonthRange]
@StartDate			datetime,
@EndDate			datetime
AS
BEGIN
    SET NOCOUNT ON;

WITH CTE AS
(
    SELECT @StartDate AS cte_start_date
    UNION ALL
    SELECT DATEADD(MONTH, 1, cte_start_date)
    FROM CTE
    WHERE DATEADD(MONTH, 1, cte_start_date) <= @EndDate   
)
SELECT 
LEFT(DATENAME(MONTH,[cte_start_date]),3) +  ' ' +  CAST ( YEAR([cte_start_date])AS varchar(100) )  [Range]
FROM CTE


end




GO
