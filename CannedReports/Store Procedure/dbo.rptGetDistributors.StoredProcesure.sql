USE [SmartVmaPlus]
GO

/****** Object:  StoredProcedure [dbo].[rptGetDistributors]    Script Date: 2016-08-16 11:27:20 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[rptGetDistributors]
	@UserRoleCompany varchar(25) = '0_0_0'
AS 
BEGIN
	if @UserRoleCompany is null or @UserRoleCompany = ''
		select -1 as companyid, 'Not available' companyname, 0 sort  
	else
	begin
		declare @UserId int
		declare @RoleId int
		declare @CompanyId int

		set @userId = convert(int,substring(@UserRoleCompany,1,CHARINDEX('_',@UserRoleCompany) - 1))

		set @UserRoleCompany = substring(@UserRoleCompany,CHARINDEX('_',@UserRoleCompany) + 1,len(@UserRoleCompany) - CHARINDEX('_',@UserRoleCompany))

		set @RoleId = convert(int,substring(@UserRoleCompany,1,CHARINDEX('_',@UserRoleCompany) - 1))

		set @CompanyId = convert(int,substring(@UserRoleCompany,CHARINDEX('_',@UserRoleCompany) + 1,len(@UserRoleCompany) - CHARINDEX('_',@UserRoleCompany)))

		declare @Role nvarchar(100)

		select @Role = Role from Roles where RoleId = @RoleId

		if (@Role is null)
			select null as companyid, 'Not available' companyname, 0 sort  
		else
		begin
			if (@Role = 'SystemAdministrator' or @Role = 'BGExecutive')
				select 0 companyid, 'All' companyname, 0 sort  
				union all
				select distinct companyid, companyname, 1 sort from companies where parentid in (select CompanyId from Companies where parentId is null)
			else if (@Role = 'DistributorAdmin' or @Role = 'DistributorExecutive')
			begin
				if exists (select t1.companyId, t1.companyname, 1 sort from companies t1 
					inner join Companies t2 on t1.ParentId = t2.CompanyId 
					where t1.companyId = @CompanyId and
						t2.ParentId is null)
				
					select t1.companyId, t1.companyname, 1 sort from companies t1 
						inner join Companies t2 on t1.ParentId = t2.CompanyId 
						where t1.companyId = @CompanyId and
						t2.ParentId is null
				else
					select -1 as companyid, 'Not available' companyname, 0 sort  
			end
			else if (@Role = 'DealerAdmin' or @Role = 'Manager')
			begin
				if exists (select t1.CompanyId, t1.CompanyName, 1 sort from companies t1 
					inner join Companies t2 on t1.CompanyId = t2.ParentId
					where t2.CompanyId = @CompanyId and t1.ParentId in (select CompanyId from Companies where ParentId is null))
				begin
					select t1.CompanyId, t1.CompanyName, 1 sort from companies t1 
					inner join Companies t2 on t1.CompanyId = t2.ParentId
					where t2.CompanyId = @CompanyId and t1.ParentId in (select CompanyId from Companies where ParentId is null)
				end
				else
					select -1 as companyid, 'Not available' companyname, 0 sort  
			end
			else
				select -1 as companyid, 'Not available' companyname, 0 sort  
		end
			
	end
end
GO


