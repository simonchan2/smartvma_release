USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[rptMenuActivityTrendDetail]    Script Date: 2016-09-09 2:50:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[rptMenuActivityTrendDetail]
@DistributorId int,
@DealerCompanyId int,
@Advisor	int,
@StartDate  DATETIME,
@EndDate    DATETIME,
@CPOnly				bit=0,
@UserOnly			bit=0,
@ExclLabor0			bit=0
AS 
BEGIN

--exec [dbo].[rptMenuActivityTrendDetail] '2746', '2015-03-01','2016-07-01'
-- 1. Create a set of date range with level
WITH CTE AS
(
    SELECT @StartDate AS monthYear
    UNION ALL
    SELECT DATEADD(MONTH, 1, monthYear)
    FROM CTE
    WHERE DATEADD(MONTH, 1, monthYear) <= @EndDate   
)

SELECT 
	monthYear DateRange, 0 [Preseneted], 0 [Accepted]
into #temp
FROM CTE
option (maxrecursion 1000);


create table #tempMenLevel
(    
    MenuLevel int
)
insert into  #tempMenLevel
select 0;
insert into  #tempMenLevel
select 1;
insert into  #tempMenLevel
select 2;
insert into  #tempMenLevel
select 3;
  
select * 
into #tempDateRangeSet
from #temp tt
cross join  #tempMenLevel l

-- End of 1. End of Create a set of date range with level
 declare @AdvisorName nvarchar(100) 
 IF (@Advisor = -88 or @Advisor = -99)
 BEGIN
	set @AdvisorName = 'AllAdvisors'	
 END
 else
 BEGIN
	
	select @AdvisorName = (isnull(u.FirstName,'') + ' ' + isnull(u.LastName,'')) from Users u where userid = @Advisor
 END
-- 2. Create  data file
--Status of the meny presenrtation; V: Void; C: Closed; M: Menu Presented; MC: Menu Accepted; MD: Menu Declined; MK: Menu Parked
select distinct 
	   u.UserId,
       dmsu.[DmsUserNumber] AdvisorCode,       
	   (isnull(u.FirstName,'') + ' ' + isnull(u.LastName,'')) AdvisorName,
       1 Presented,
       (case when PresentationStatus='MC'  then 1 else 0 end) Accepted,
	   (MenuLevel) [MenuLevel],
       [AppointmentTime]    
into #Data
from 
	[dealer].[AppointmentPresentations] ap (nolock)
	join Users u (nolock) on u.UserId = ap.AdvisorId
	left join [dealer].[AppointmentServices] aps on ap.AppointmentPresentationId = aps.AppointmentPresentationId 
	left join [dealer].[DealerDmsUsers] dmsu (nolock) on dmsu.UserId  = ap.[AdvisorId] 
	left join [dealer].[Invoices] i (nolock) on i.InvoiceNumber = ap.InvoiceNumber
	left join [dealer].[InvoiceDetails] d (nolock)on d.InvoiceId = i.InvoiceId
where	[AppointmentTime] between @StartDate and @EndDate and
		((@Advisor=-88 or @Advisor=-99) or (@Advisor!=-88 and @Advisor!=-99 and u.UserId =@Advisor)) and
		(@ExclLabor0 = 0 or (@ExclLabor0 = 1 and aps.LaborHour != 0)) and
		(@UserOnly = 0 or (@UserOnly = 1 and dmsu.DmsUserName is not null)) and
		(@CPOnly = 0 or (@CPOnly=1 and d.[LaborType] = 'CP'))		
		and ((@DistributorId = 0 and @DealerCompanyId = 0) or (@DealerCompanyId > 0 and ap.DealerId = @DealerCompanyId) or (@DistributorId > 0 and @DealerCompanyId = 0 and ap.DealerId in (select CompanyId from Companies where parentId = @DistributorId)))

  --select * from #Data

  	declare @curUserId int
	declare @menulevel int
	declare @presented int
	declare @accepted  int
	declare @apptdate datetime
	declare @AdvisorCode nvarchar(45)
	declare @curAdvisorName nvarchar(45)

	declare CustList cursor for
	select * from #Data
	OPEN CustList
	FETCH NEXT FROM CustList 
	INTO @curUserId, @AdvisorCode, @curAdvisorName, @presented, @accepted, @menulevel, @apptdate
	
	WHILE @@FETCH_STATUS = 0
	BEGIN		
		  begin
		  
	  		update t2
				set t2.Preseneted = (t2.Preseneted + @presented),
				t2.Accepted = (t2.Accepted + @accepted)
			from #tempDateRangeSet t2
			where
				t2.MenuLevel =  @menulevel and
				Month(t2.DateRange) = Month(@apptdate) and
				Year(t2.DateRange) = Year(@apptdate) 

		
		  end
	  
	  FETCH NEXT FROM CustList 
	    INTO @curUserId, @AdvisorCode, @curAdvisorName, @presented, @accepted,@menulevel, @apptdate
	END
	CLOSE CustList
	DEALLOCATE CustList

--select * from #tempDateRangeSet
--return

begin    
	select 	
		t1.DateRange,
		t1.Preseneted, 
		t1.Accepted,
		t1.Accepted result, 
		t1.MenuLevel,
		@AdvisorName as AdvisorName
	into #tempDateRangeSet2
	from #tempDateRangeSet t1	
	order by DateRange
	
	--select * from #tempDateRangeSet2 order by DateRange
	--return
	select *
	into #tempDateRangeSet3
	from #tempDateRangeSet2
	pivot (sum(result) for MenuLevel in ([1],[2],[3])) as [MenuLevel1]
	order by DateRange


	update #tempDateRangeSet3
	set [1]=ISNULL([1] ,0), [2]=ISNULL([2] ,0),[3]=ISNULL([3] ,0)

	select DateRange, sum(Preseneted) Preseneted, sum(Accepted) Accepted, AdvisorName, sum([1]) [1], sum([2]) [2], sum([3]) [3]
	into #tempDateRangeSet4
	 from #tempDateRangeSet3 
	 group by DateRange, AdvisorName
	 order by DateRange
	
	
--	update t3 
--		set Preseneted = sumPreseneted,
--			Accepted = sumAccepted
--	from #tempDateRangeSet4 t3
--	join 	
--	(select DateRange, sum(Preseneted) sumPreseneted, sum(Accepted) sumAccepted
--	from #tempDateRangeSet2 t2 
--	group by DateRange) ss on ss.DateRange = t3.DateRange

----	select * from #Data

--	update t4
--		set t4.[1] = l.Level
--	from #tempDateRangeSet4 t4
--	join (select  Month(AppointmentTime) [month],Year(AppointmentTime) [year], count(*) [Level] from #Data where MenuLevel=1 
--	group by Month(AppointmentTime),Year(AppointmentTime)
--	) l
--	on Month(t4.DateRange) = l.[month] and
--				Year(t4.DateRange) = l.[year]
	
--	update t4
--		set t4.[2] = l.Level
--	from #tempDateRangeSet4 t4
--	join (select  Month(AppointmentTime) [month],Year(AppointmentTime) [year], count(*) [Level] from #Data where MenuLevel=2 
--	group by Month(AppointmentTime),Year(AppointmentTime)
--	) l
--	on Month(t4.DateRange) = l.[month] and
--				Year(t4.DateRange) = l.[year]

--	update t4
--		set t4.[3] = l.Level
--	from #tempDateRangeSet4 t4
--	join (select  Month(AppointmentTime) [month],Year(AppointmentTime) [year], count(*) [Level] from #Data where MenuLevel=3 
--	group by Month(AppointmentTime),Year(AppointmentTime)
--	) l
--	on Month(t4.DateRange) = l.[month] and
--				Year(t4.DateRange) = l.[year]


	select 			
		   LEFT(DATENAME(MONTH,DateRange),3) +  ' ' +  CAST ( YEAR(DateRange)AS varchar(100) )  [RangeLabel],
		   AdvisorName,
		   (Preseneted) Preseneted,
		   (Accepted) Accepted,
		   ([1]) [1],
		   ([2]) [2],
		   ([3]) [3],
		   DateRange
		   from #tempDateRangeSet4		
		   
		   union
		   	select 	
			'Total' [RangeLabel],				   
			'Total' AdvisorName,
		   sum(Preseneted) Preseneted,
		   sum(Accepted) Accepted,
		   sum([1]) [1],
		   sum([2]) [2],
		   sum([3]) [3],
		   '9999-01-01' DateRange
		   from #tempDateRangeSet4		
		   order by DateRange		

end

end




