USE [SmartVmaPlus]
GO
/****** Object:  StoredProcedure [dbo].[rptMissedOpportunity]    Script Date: 2016-08-04 8:28:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rptMissedOpportunity]
-- Missed Opportunity Report
@Content	nvarchar(45), -- services, Labor$, Part$, LaborParts$, Hours, ELR, %Pen
@StartDate  DATETIME,
@EndDate    DATETIME,
@CPOnly		bit,
@UserOnly	bit,
@ExclLabor0	bit
AS
BEGIN
    SET NOCOUNT ON;

 
select distinct 
	   i.InvoiceNumber,
       dmsu.[DmsUserNumber] AdvisorCode,
       dmsu.[DmsUserName] AdvisorName, 
       (MenuLevel) [MenuLevel],          
       (1) Presented,
	   --V: Void; C: Closed; M: Menu Presented; MC: Menu Accepted; MD: Menu Declined; MK: Menu Parked
       (case when PresentationStatus = 'MC'  then 1 else 0 end) Accepted,       
	   CASE @Content		 
		WHEN 'Service' THEN (PartQty)					-- clarify
		WHEN 'Labor$' THEN ([LaborSale])
		WHEN 'Part$' THEN (d.[PartSale])
		WHEN 'LaborParts$' THEN ([LaborSale] + d.[PartSale]) 
		WHEN 'Hours' THEN (d.[LaborHour])
		WHEN 'ELR' THEN ([LaborSale]/d.[LaborHour])		-- clarify		
		WHEN '%Pen' THEN ([LaborSale])					-- clarify
	END as content
into #Data	          
from [dealer].[DealerDmsUsers] dmsu
join [dealer].[AppointmentPresentations] ap on dmsu.UserId  = ap.[AdvisorId] 
join [dealer].[DealerVehicles] dv on dv.[DealerVehicleId] = ap.DealerVehicleId
left join dealer.Invoices i on i.InvoiceNumber = ap.InvoiceNumber
left join [dealer].[InvoiceDetails] d (nolock)on d.InvoiceId = i.InvoiceId
left join [dealer].[InvoiceDetailParts] p (nolock)on p.InvoiceDetailId = d.InvoiceDetailId
where [AppointmentTime] between @StartDate and @EndDate and
(@ExclLabor0 = 0 or (@ExclLabor0 = 1 and d.LaborHour != 0)) and
(@UserOnly = 0 or (@UserOnly = 1 and dmsu.DmsUserName is not null)) and
(@CPOnly = 0 or (@CPOnly=1 and d.[LaborType] = 'CP'))

--select * from #Data

select 
	AdvisorCode,
	AdvisorName,
	InvoiceNumber,
	MenuLevel,
	Presented,
	Accepted,
	sum(isnull(content,0)) content
into #Data2	 
from #Data
group by AdvisorCode, AdvisorName,InvoiceNumber,[MenuLevel], Presented, Accepted

select 
	d1.AdvisorCode, 
	d1.AdvisorName,
	count(d1.InvoiceNumber) InvoiceNumber,	
	count(d1.MenuLevel) MenuLevel#,
	sum(Presented) Presented,
	sum(Accepted) Accepted,	
	sum(Presented-Accepted) Missed,
	sum(Presented) * sum(content) potentialContent,
	sum(Accepted) * sum(content) soldContent,
	sum(Presented-Accepted) * sum(content) missedContent,	
	MenuLevel	
into #Data3
from #Data2 d1
group by d1.AdvisorCode, d1.AdvisorName, d1.MenuLevel

--select * from #Data3

select * 
into #Data4
from 
(
select *, 's'+ cast([MenuLevel] as varchar) sMenuLevel 
	, 'm'+ cast([MenuLevel] as varchar) mMenuLevel 
	from #Data3
	) s
pivot
( sum(potentialContent)
		for [MenuLevel] in ([1], [2], [3])
) as p1
pivot
( sum(soldContent)
		for sMenuLevel in (s1, s2, s3)
) as p2
pivot
( sum(missedContent)
		for mMenuLevel in (m1, m2, m3)
) as p3

UPdate  #Data4
set 
[1] = ISNULL([1], 0 ),
[2] = ISNULL([2], 0 ),
[3] = ISNULL([3], 0), 
[s1] = ISNULL([s1], 0 ),
[s2] = ISNULL([s2], 0 ),
[s3] = ISNULL([s3], 0 ),
[m1] = ISNULL([m1], 0 ),
[m2] = ISNULL([m2], 0 ),
[m3] = ISNULL([m3], 0 )
--from #temp3 t3

select
	AdvisorCode, 
	AdvisorName,
	sum(InvoiceNumber) Ros,
	sum(MenuLevel#) MenuLevel#,
	sum([1]) [1],
	sum([2]) [2],
	sum([3]) [3],
	sum([1] + [2] + [3]) TotalPresented, 
	sum([s1]) [s1],
	sum([s2]) [s2],
	sum([s3]) [s3],
	sum([s1] + [s2] + [s3]) TotalSold,
	sum([m1]) [m1],
	sum([m2]) [m2],
	sum([m3]) [m3],
	sum([m1] + [m2] + [m3]) TotalMissed
from #Data4
group by AdvisorCode, AdvisorName
order by AdvisorName

END




GO
