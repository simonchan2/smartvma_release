/****** Object:  StoredProcedure [dbo].[rptMenuSalesTrend]    Script Date: 2016-08-26 1:47:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec [dbo].[rptMenuSalesTrend] 'AllAdvisors','20160301' , '20160701', 1
CREATE PROCEDURE [dbo].[rptMenuSalesTrend]
@DistributorId		int,
@DealerCompanyId	int,
@Advisor			int, --1. All Advisors: Consolidated + All Advisors 2.Consolidated 3. Each Advisor
@StartDate			datetime,
@EndDate			datetime,
@CPOnly				bit=0,
@ExclLabor0			bit=0,
@UserOnly			bit=0
AS
BEGIN
    SET NOCOUNT ON;
WITH CTE AS
(
    SELECT @StartDate AS monthYear
    UNION ALL
    SELECT DATEADD(MONTH, 1, monthYear)
    FROM CTE
    WHERE DATEADD(MONTH, 1, monthYear) <= @EndDate   
)

SELECT 
	monthYear DateRane, 0 [Preseneted], 0 [Accepted]
into #temp
FROM CTE

create table #tempMenLevel
(    
    MenuLevel int
)

insert into  #tempMenLevel
select 1;
insert into  #tempMenLevel
select 2;
insert into  #tempMenLevel
select 3;
  
select * 
into #tempDateRangeSet
from #temp tt
cross join  #tempMenLevel l

-- End of 1. End of Create a set of date range with level
  
-- 2. Create  data file
select distinct 
	   i.InvoiceNumber,
       dmsu.[DmsUserNumber] AdvisorCode,
       dmsu.[DmsUserName] AdvisorName, 
       (MenuLevel) [MenuLevel],          
       (case when [MenuLevel] is null then 0  else 1 end) Presented,
       (case when i.InvoiceNumber is null  then 0 else 1 end) Accepted,
       [AppointmentTime],
	   dv.vin,
	   p.PartQty,
	   p.PartSale	       
into #Data
from [dealer].[AppointmentPresentations] ap (nolock)
	join Users u (nolock) on u.UserId = ap.AdvisorId
	left join [dealer].[DealerDmsUsers] dmsu (nolock) on dmsu.UserId  = ap.[AdvisorId] 
	join [dealer].[DealerVehicles] dv on dv.[DealerVehicleId] = ap.DealerVehicleId
	join [dealer].[Invoices] i (nolock) on i.InvoiceNumber = ap.InvoiceNumber
	join [dealer].[InvoiceDetails] d (nolock)on d.InvoiceId = i.InvoiceId
	left join [dealer].[InvoiceDetailParts] p (nolock)on p.InvoiceDetailId = d.InvoiceDetailId
where 
((@DistributorId = 0 and @DealerCompanyId = 0) or (@DealerCompanyId > 0 and i.DealerId = @DealerCompanyId) or (@DistributorId > 0 and @DealerCompanyId = 0 and i.DealerId in (select CompanyId from dbo.Companies where parentId = @DistributorId)))	
and [AppointmentTime] between @StartDate and @EndDate and
((@Advisor=-88 or @Advisor=-99) or (@Advisor!=-88 and @Advisor!=-99 and u.UserId =@Advisor))


select 
count(InvoiceNumber) InvoiceNumber,
[MenuLevel],
sum(PartQty) PartQty,
sum(PartSale) PartSale
,CONVERT(date, [AppointmentTime]) [AppointmentTime]
into #Data2
from #Data
group by InvoiceNumber, [MenuLevel],CONVERT(date, [AppointmentTime])


select * from #Data2 order by AppointmentTime, MenuLevel

select * from
( select *, 'p'+ cast([MenuLevel] as varchar) pMenuLevel from #Data2
) s
pivot
( sum(PartQty)
 for [MenuLevel] in ([1], [2], [3])
) as p1
pivot
( sum(PartSale)
for pMenuLevel in (p1, p2, p3)
) as p2






END

