USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[rptMenuSalesXAdvisor]    Script Date: 2016-10-03 11:30:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[rptMenuSalesXAdvisor]
@Content			varchar(255),
@DistributorId		int,
@DealerCompanyId	int,
@StartDate			datetime,
@EndDate			datetime,
@Level				int=0, -- 0=All, 1=Level 1, 2=Level 2, 3=Level 3
@CPOnly				bit=0,
@ExclLabor0			bit=0,
@UserOnly			bit=0
AS
BEGIN
    SET NOCOUNT ON;

select distinct 
	u.Userid,
	i.AdvisorCode, 		
	(isnull(u.FirstName,'') + ' ' + isnull(u.LastName,'')) AdvisorName,       
	i.invoiceid,
	i.Mileage,
	finalMileage = dbo.udf_GetMileage(i.Mileage),	
	d.InvoiceDetailId as KeyId,
	p.InvoiceDetailPartId as KeyId2,
	round(CASE @Content		 
		WHEN 'QTY' THEN (PartQty)
		WHEN 'Labor$' THEN ([LaborSale])
		WHEN 'Part$' THEN (d.[PartSale])
		WHEN 'LaborParts$' THEN ([LaborSale] + d.[PartSale]) 
		WHEN 'Hours' THEN (d.[LaborHour])
		WHEN 'ELR' THEN (case when d.LaborHour = 0 then 0 else [LaborSale]/d.[LaborHour] end)	
		WHEN 'GPLabor' THEN (d.[LaborSale] - d.[LaborCost])
		WHEN 'GPParts' THEN (d.[PartSale] - d.[PartCost])
		WHEN 'GPLaborParts' THEN (d.[LaborSale] - d.[LaborCost] + d.[PartSale] - d.[PartCost])						
	END, 1) as content	
into #temp1
from 
	[dealer].[AppointmentPresentations] ap (nolock)
	join Users u (nolock) on u.UserId = ap.AdvisorId
	join [dealer].[DealerVehicles] dv on dv.[DealerVehicleId] = ap.DealerVehicleId	
	join [dealer].[DealerCustomers] dc (nolock)on dc.[DealerCustomerId] = ap.DealerCustomerId
	left join [dealer].[DealerDmsUsers] dmsu (nolock) on dmsu.UserId  = ap.[AdvisorId] 
	left join [dealer].[Invoices] i (nolock) on i.InvoiceNumber = ap.InvoiceNumber
	left join [dealer].[InvoiceDetails] d (nolock)on d.InvoiceId = i.InvoiceId
	left join [dealer].[InvoiceDetailParts] p (nolock)on p.InvoiceDetailId = d.InvoiceDetailId
where 	
	((@DistributorId = 0 and @DealerCompanyId = 0) or (@DealerCompanyId > 0 and i.DealerId = @DealerCompanyId) or (@DistributorId > 0 and @DealerCompanyId = 0 and i.DealerId in (select CompanyId from Companies where parentId = @DistributorId)))
	and i.InvoiceDate between @StartDate and @EndDate 
	and (@ExclLabor0 = 0 or (@ExclLabor0 = 1 and d.LaborHour != 0)) 
	and (@UserOnly = 0 or (@UserOnly = 1 and dmsu.DmsUserName is not null)) 
	and (@CPOnly = 0 or (@CPOnly=1 and d.[LaborType] = 'CP')) 
	and (@Level = 0 or (@Level != 0 and MenuLevel=@Level) )
		
--select *from #temp1
	

select	Userid,
		AdvisorCode, 	
		[AdvisorName] ,
		[InvoiceId],
		[finalMileage],
		round(sum(content),1) content,
		round(sum(content),1) as contentForSum
into #temp2
from #temp1
group by userid, AdvisorCode, AdvisorName, InvoiceId, [finalMileage]

--select * from #temp2

--select sum(invoiceid), sum(content) from #temp2

select *, 1 sort 
into #temp3
from #temp2 t
pivot (sum(contentForSum) for [finalMileage] in ([5000],[10000],[15000],[20000],[25000],[30000],[35000],[40000],[45000],[50000],[55000],[60000],[65000],[70000],[75000],[80000],[85000],[90000],[95000],[100000],[105000],[110000],[115000],[120000],[125000],[130000],[135000],[140000],[145000],[150000],[155000],[160000],[165000],[170000],[175000],[180000],[185000],[190000],[195000],[200000])) as [Mileage]

update t3
	set t3.content = t2.sumContent
from #temp3 t3
join (
	select userId,AdvisorCode,InvoiceId, sum(content) sumContent from #temp2 group by UserId,AdvisorCode, InvoiceId
) t2 on t2.AdvisorCode = t3.AdvisorCode and t2.UserId = t3.UserId and t2.InvoiceId = t3.InvoiceId

--select * from #temp3

select 
	Userid,
	AdvisorCode, 
	[AdvisorName], 
	count(*) Ro, 
	round(sum(content)/count(*),1) percentageP, 
	round(sum(content),1) column5,
	0 Total,
	isnull(sum([5000]),0)[5000],
	isnull(sum([10000]),0)[10000],
	isnull(sum([15000]),0)[15000],
	isnull(sum([20000]),0)[20000],
	isnull(sum([25000]),0)[25000],
	isnull(sum([30000]),0)[30000],
	isnull(sum([35000]),0)[35000],
	isnull(sum([40000]),0)[40000],
	isnull(sum([45000]),0)[45000],
	isnull(sum([50000]),0)[50000],
	isnull(sum([55000]),0)[55000],
	isnull(sum([60000]),0)[60000],
	isnull(sum([65000]),0)[65000],
	isnull(sum([70000]),0)[70000],
	isnull(sum([75000]),0)[75000],
	isnull(sum([80000]),0)[80000],
	isnull(sum([85000]),0)[85000],
	isnull(sum([90000]),0)[90000],
	isnull(sum([95000]),0)[95000],
	isnull(sum([100000]),0)[100000],
	isnull(sum([105000]),0)[105000],
	isnull(sum([110000]),0)[110000],
	isnull(sum([115000]),0)[115000],
	isnull(sum([120000]),0)[120000],
	isnull(sum([125000]),0)[125000],
	isnull(sum([130000]),0)[130000],
	isnull(sum([135000]),0)[135000],
	isnull(sum([140000]),0)[140000],
	isnull(sum([145000]),0)[145000],
	isnull(sum([150000]),0)[150000],
	isnull(sum([155000]),0)[155000],
	isnull(sum([160000]),0)[160000],
	isnull(sum([165000]),0)[165000],
	isnull(sum([170000]),0)[170000],
	isnull(sum([175000]),0)[175000],
	isnull(sum([180000]),0)[180000],
	isnull(sum([185000]),0)[185000],
	isnull(sum([190000]),0)[190000],
	isnull(sum([195000]),0)[195000],
	isnull(sum([200000]),0)[200000],
	max(sort) sort  
into #temp4
from #temp3
group by Userid, AdvisorCode, [AdvisorName]

select
	Userid, 
	AdvisorCode,
	isnull(
	sum([5000])+
	sum([10000])+
	sum([15000])+
	sum([20000])+
	sum([25000])+
	sum([30000])+
	sum([35000])+
	sum([40000])+
	sum([45000])+
	sum([50000])+
	sum([55000])+
	sum([60000])+
	sum([65000])+
	sum([70000])+
	sum([75000])+
	sum([80000])+
	sum([85000])+
	sum([90000])+
	sum([95000])+
	sum([100000])+
	sum([105000])+
	sum([110000])+
	sum([115000])+
	sum([120000])+
	sum([125000])+
	sum([130000])+
	sum([135000])+
	sum([140000])+
	sum([145000])+
	sum([150000])+
	sum([155000])+
	sum([160000])+
	sum([165000])+
	sum([170000])+
	sum([175000])+
	sum([180000])+
	sum([185000])+
	sum([190000])+
	sum([195000])+
	sum([200000]),0) Total
	into #temmoTotal
 from #temp4
 group by userid, AdvisorCode

 update t4
 set t4.Total = tot.Total
 from #temp4 t4
 join #temmoTotal tot on tot.AdvisorCode = t4.AdvisorCode and tot.UserId = t4.UserId

 if @Level = 0
  select 
	0 Userid,
	' ', 
	'Total' [Advisor Name],
	sum(Ro) Ro, 
	round(sum(Column5)/sum(Ro),1) percentageP , 
	sum(column5) column5,
	sum(Total) Total,
	sum([5000])[5000],
	sum([10000])[10000],
	sum([15000])[15000],
	sum([20000])[20000],
	sum([25000])[25000],
	sum([30000])[30000],
	sum([35000])[35000],
	sum([40000])[40000],
	sum([45000])[45000],
	sum([50000])[50000],
	sum([55000])[55000],
	sum([60000])[60000],
	sum([65000])[65000],
	sum([70000])[70000],
	sum([75000])[75000],
	sum([80000])[80000],
	sum([85000])[85000],
	sum([90000])[90000],
	sum([95000])[95000],
	sum([100000])[100000],
	sum([105000])[105000],
	sum([110000])[110000],
	sum([115000])[115000],
	sum([120000])[120000],
	sum([125000])[125000],
	sum([130000])[130000],
	sum([135000])[135000],
	sum([140000])[140000],
	sum([145000])[145000],
	sum([150000])[150000],
	sum([155000])[155000],
	sum([160000])[160000],
	sum([165000])[165000],
	sum([170000])[170000],
	sum([175000])[175000],
	sum([180000])[180000],
	sum([185000])[185000],
	sum([190000])[190000],
	sum([195000])[195000],
	sum([200000])[200000],
	2 sort  
from #temp4
else
 select * from #temp4
 union
 select 
	0 Userid,
	' ', 
	'Total' [Advisor Name],
	sum(Ro) Ro, 
	round(sum(column5)/sum(Ro),1) percentageP , 
	sum(column5) column5,
	sum(Total) Total,
	sum([5000])[5000],
	sum([10000])[10000],
	sum([15000])[15000],
	sum([20000])[20000],
	sum([25000])[25000],
	sum([30000])[30000],
	sum([35000])[35000],
	sum([40000])[40000],
	sum([45000])[45000],
	sum([50000])[50000],
	sum([55000])[55000],
	sum([60000])[60000],
	sum([65000])[65000],
	sum([70000])[70000],
	sum([75000])[75000],
	sum([80000])[80000],
	sum([85000])[85000],
	sum([90000])[90000],
	sum([95000])[95000],
	sum([100000])[100000],
	sum([105000])[105000],
	sum([110000])[110000],
	sum([115000])[115000],
	sum([120000])[120000],
	sum([125000])[125000],
	sum([130000])[130000],
	sum([135000])[135000],
	sum([140000])[140000],
	sum([145000])[145000],
	sum([150000])[150000],
	sum([155000])[155000],
	sum([160000])[160000],
	sum([165000])[165000],
	sum([170000])[170000],
	sum([175000])[175000],
	sum([180000])[180000],
	sum([185000])[185000],
	sum([190000])[190000],
	sum([195000])[195000],
	sum([200000])[200000],
	2 sort  
from #temp4
order by sort, [AdvisorName]


END