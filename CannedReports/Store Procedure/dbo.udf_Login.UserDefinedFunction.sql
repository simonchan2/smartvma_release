USE [SmartVmaPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[udf_Login]    Script Date: 2016-07-11 3:51:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Author:		 Simon Chan
Create date: 2016-05-30
Description: Log in with login and password
				Result:
				1: Log in success
				2: login does not exist
				3: Inactive login
				4: Password has to be reset
				5: Password does not match

Usage:
	SELECT dbo.udf_Login('abc', '123');
*/

CREATE FUNCTION [dbo].[udf_Login] (@Login				nvarchar(20)
, @Password			nvarchar(50))
RETURNS tinyint
AS
BEGIN
    DECLARE    
            @UserStatusCodeId        int = NULL
            , @PasswordHash            varbinary(MAX) = NULL
            , @PasswordHashComputed    varbinary(MAX) = NULL
    ;

    SELECT    
            @UserStatusCodeId = UserStatusCodeId
            , @PasswordHash = PasswordHash
    FROM    dbo.Users
    WHERE    [Login] = @Login;

--    Login record does not exist
    IF    @UserStatusCodeId IS NULL
        RETURN 2;

--    Inactive login
    IF    @UserStatusCodeId = 1
        RETURN 3;

--    Password has to be reset
    IF    @UserStatusCodeId = 2
        RETURN 4;
    
--    Get the salt from the hash
    DECLARE @Salt    VARBINARY(4) = SUBSTRING(@PasswordHash, 3, 4);    

--    Get the hash with salt
    SET @PasswordHashComputed = 0x0200 + @Salt + HASHBYTES('SHA2_512', CAST(ISNULL(@Password, '') AS VARBINARY(MAX)) + @Salt);

    RETURN    (CASE WHEN @PasswordHash = @PasswordHashComputed THEN 1 ELSE 5 END);
END

GO
