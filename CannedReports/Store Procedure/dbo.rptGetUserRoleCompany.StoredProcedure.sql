USE [SmartVmaPlus]
GO

/****** Object:  StoredProcedure [dbo].[rptGetUserRoleCompany]    Script Date: 2016-08-16 2:46:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[rptGetUserRoleCompany]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select '0_0_0' as UserRoleCompany
	union all
	select distinct convert(nvarchar(10),UserId) + '_' + convert(nvarchar(10),RoleId) + '_' + convert(nvarchar(10),companyId) as UserRoleCompany from UserCompanyRoles 
END

GO


