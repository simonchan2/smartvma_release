USE [SmartVmaPlus]
GO
/****** Object:  StoredProcedure [dbo].[spGetDateByOption]    Script Date: 2016-08-22 2:08:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[spGetDateByOption](@DateOption varchar(100))
AS 
BEGIN

declare @stareDate datetime, @endDate datetime

SET @stareDate = GETDATE() 
SET @endDate = GETDATE() 

IF @DateOption = 'Today'  ----Today
BEGIN
	SET @stareDate = dateadd(day, datediff(day, 0, getdate()), 0) 
	SET @endDate = GETDATE() 
END
ELSE
IF @DateOption = 'Yesterday'  ----Yesterday
BEGIN
	SET @stareDate = DATEADD(d,-1,dateadd(day, datediff(day, 0, getdate()), 0)) 
	SET @EndDate = dateadd(second, -1, dateadd(day, datediff(day, 0, getdate()), 0))	
END
ELSE
IF @DateOption = 'ThisWeek'  ----This week
BEGIN
	SET @stareDate = DATEADD(wk,DATEDIFF(wk,0,GETDATE()),0)	
END
IF @DateOption = 'LastWeek'  ----last week
BEGIN
	SET @stareDate = DATEADD(wk,DATEDIFF(wk,7,GETDATE()),0)
	SET @endDate = dateadd(second, -1, DATEADD(wk,DATEDIFF(wk,7,GETDATE()),7))
END
ELSE
IF @DateOption = 'ThisMonth'  ----This month
BEGIN
	SET @stareDate = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)	
END
ELSE
IF @DateOption = 'LastMonth'  ----Last month
BEGIN
	SET @stareDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0))
	SET @endDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)))
END
ELSE
IF @DateOption = 'ThisYear'  ----This year
BEGIN
	SET @stareDate = DATEADD(yy,DATEDIFF(yy,0,GETDATE()),0)	
END
ELSE
IF @DateOption = 'LastYear'  ----Last year
BEGIN
	SET @stareDate = DATEADD(yy,-1,DATEADD(yy,DATEDIFF(yy,0,GETDATE()),0)) 
	SET @endDate = DATEADD(ms,-3,DATEADD(yy,0,DATEADD(yy,DATEDIFF(yy,0,GETDATE()),0)))
END


select @stareDate StartDate, @endDate EndDate

return
end
