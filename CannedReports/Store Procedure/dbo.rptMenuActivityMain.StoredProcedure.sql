USE [SmartVmaPlus]
GO
/****** Object:  StoredProcedure [dbo].[rptMenuActivityMain]    Script Date: 2016-08-29 2:47:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[rptMenuActivityMain]
@DistributorId int,
@DealerCompanyId int,
@Advisor	int,
@StartDate  DATETIME,
@EndDate    DATETIME,
@CPOnly				bit=0,
@UserOnly			bit=0,
@ExclLabor0			bit=0
AS 
BEGIN

if (@Advisor = -99) -- 'Consolidated'
begin
	select @Advisor  UserId, 'Consolidated' AdvisorCode, 'Consolidated' AdvisorName, 1 sort
end
else
if (@Advisor = -88) -- 'AllAdvisors'
begin
	if (@UserOnly = 0)
	begin
		select @Advisor UserId, 'AllAdvisors' AdvisorCode, 'AllAdvisors' AdvisorName, 0 sort
		union
		select  distinct u.UserId, dmsu.[DmsUserNumber] AdvisorCode, case when  dmsu.[DmsUserNumber] is null then u.FirstName + ' ' + u.LastName else u.FirstName + ' ' + u.LastName + ' (' +  dmsu.[DmsUserNumber] + ')' end as AdvisorName,1 sort
		  from dbo.Users as u
		  inner join [dealer].[AppointmentPresentations] a on u.userId = a.AdvisorId
		  left join [dealer].[DealerDmsUsers] dmsu on dmsu.UserId = u.UserId 
		  where
			((@DistributorId = 0 and @DealerCompanyId = 0) or (@DealerCompanyId > 0 and a.DealerId = @DealerCompanyId) or (@DistributorId > 0 and @DealerCompanyId = 0 and a.DealerId in (select CompanyId from Companies where parentId = @DistributorId)))
		order by sort, 3
	end
	else
	begin -- Dms user only
		select @Advisor UserId, 'AllAdvisors' AdvisorCode, 'AllAdvisors' AdvisorName, 0 sort
		union
		select @Advisor UserId, 'AllAdvisors' AdvisorCode, 'AllAdvisors' AdvisorName, 0 sort
		union
		select  distinct u.UserId, dmsu.[DmsUserNumber] AdvisorCode, case when  dmsu.[DmsUserNumber] is null then u.FirstName + ' ' + u.LastName else u.FirstName + ' ' + u.LastName + ' (' +  dmsu.[DmsUserNumber] + ')' end as AdvisorName,1 sort
		  from dbo.Users as u
		  inner join [dealer].[AppointmentPresentations] a on u.userId = a.AdvisorId
		  inner join [dealer].[DealerDmsUsers] dmsu on dmsu.UserId = u.UserId 
		  where
			((@DistributorId = 0 and @DealerCompanyId = 0) or (@DealerCompanyId > 0 and a.DealerId = @DealerCompanyId) or (@DistributorId > 0 and @DealerCompanyId = 0 and a.DealerId in (select CompanyId from Companies where parentId = @DistributorId)))
		order by sort, 3
	end
end
else
begin
	select distinct 
       u.UserId, 
	   dmsu.[DmsUserNumber] AdvisorCode, 	   
	   (isnull(u.FirstName,'') + ' ' + isnull(u.LastName,'')) AdvisorName,	
	   0 sort
	from users u (nolock)	
	left join [dealer].[DealerDmsUsers] dmsu (nolock) on u.userid = dmsu.userid	
	where 
		(@UserOnly = 0 or (@UserOnly = 1 and dmsu.DmsUserName is not null)) and
		(u.UserId = @Advisor)
		order by (isnull(u.FirstName,'') + ' ' + isnull(u.LastName,''))
end

end





