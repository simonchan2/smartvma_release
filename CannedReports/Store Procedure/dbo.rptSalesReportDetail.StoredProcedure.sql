USE [SmartVmaPlus]
GO
/****** Object:  StoredProcedure [dbo].[rptSalesReportDetail]    Script Date: 2016-08-16 5:18:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rptSalesReportDetail]
@AdvisorCode		nvarchar(45),
@StartDate			datetime,
@EndDate			datetime,
@DistributorId		int,
@DealerCompanyId	int,
@LaborType		    nvarchar(20)=null,
@CPOnly				bit=0,
@ExclLabor0			bit=0,
@UserOnly			bit=0
AS
BEGIN
    SET NOCOUNT ON;

if (@LaborType = ' ' or @LaborType = '999')
begin
	set @LaborType = null
end

select distinct 
	i.AdvisorCode, 
	i.AdvisorName, 
	i.InvoiceDate,		
	(dc.FirstName + ' ' + dc.LastName) CustomerName,
	i.Mileage,
	isnull(dv.VehicleMake, '') + ' ' + isnull(dv.VehicleModel, '') + ' ' + isnull(cast(dv.VehicleYear as varchar),'') CarDesc,
	(i.InvoiceNumber) InvoiceNumber, 
	ROUND((d.LaborHour),2) LaborHour, 
	Round(d.PartSale,2) PartSale,
	Round(d.LaborSale,2) LaborSale,
	d.LaborType,
	d.InvoiceDetailId
into #temp
from dealer.Invoices i 
	join [dealer].[InvoiceDetails] d (nolock)on d.InvoiceId = i.InvoiceId	
	left join [dealer].[DealerDmsUsers] u (nolock)on u.DmsUserNumber = i.AdvisorCode
	left join [dealer].[DealerCustomers] dc (nolock)on dc.[DealerCustomerId] = i.DealerCustomerId
	left join [dealer].[DealerVehicles] dv (nolock)on dv.DealerVehicleId = i.DealerVehicleId
	left join [dbo].[Vehicles] v (nolock)on v.[VehicleId] = dv.VehicleId
where i.InvoiceDate between @StartDate and @EndDate and
(@ExclLabor0 = 0 or (@ExclLabor0 = 1 and d.LaborHour != 0)) and
(@UserOnly = 0 or (@UserOnly = 1 and u.DmsUserName is not null)) and
(@CPOnly = 0 or (@CPOnly=1 and d.[LaborType] = 'CP'))
 and ((@LaborType is null and d.[LaborType] is null) or (@LaborType is not null and d.[LaborType]=@LaborType))
and i.AdvisorCode = @AdvisorCode
and ((@DistributorId = 0 and @DealerCompanyId = 0) or (@DealerCompanyId > 0 and i.DealerId = @DealerCompanyId) or (@DistributorId > 0 and @DealerCompanyId = 0 and i.DealerId in (select CompanyId from Companies where parentId = @DistributorId)))
order by AdvisorName
--select * from #temp

select distinct 
max(InvoiceDate) InvoiceDate,		
max(InvoiceNumber) InvoiceNumber, 
max(CustomerName) CustomerName,
max(Mileage) Mileage,
max(CarDesc) CarDesc,
LaborType,
ROUND(sum(LaborHour),2) LaborHour, 
ROUND(sum(PartSale),2) PartSale,
ROUND(sum(LaborSale),2) LaborSale,  
ROUND((sum(PartSale) + sum(LaborSale)),2) Total,
Round((sum(PartSale) + sum(LaborSale))/count(InvoiceNumber),2) Avg$Ro,
Round(sum(LaborHour)/count(InvoiceNumber),2) AvgHrRo,
CASE
  WHEN sum(LaborHour)= 0 THEN 0
  else
  Round(sum(LaborSale) / sum(LaborHour),2)
END
ELR
into #temp2
from #temp 
group by InvoiceNumber, LaborType

select * from #temp2



END
