USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[rptMenuActivityTrendDetail2]    Script Date: 2016-09-28 2:05:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[rptMenuActivityTrendDetail2]
@DistributorId int,
@DealerCompanyId int,
@MonthYear		varchar(30),
@StartDate datetime,
@EndDate	datetime,
@Advisor			int,
@CPOnly				bit=0,
@UserOnly			bit=0,
@ExclLabor0			bit=0
AS 
BEGIN

if (@MonthYear='Total')
begin
	return
end

declare @StartDate1 datetime, @EndDate1 datetime
select @StartDate1	= cast(@MonthYear as date)
select @EndDate1		= dateadd(S,-1, dateadd(m, 1, @StartDate1))

if @StartDate1 > @StartDate
	set @StartDate = @StartDate1

if @EndDate1 < @EndDate
	set @EndDate = @EndDate1

--select @StartDate, @EndDate
	
	select distinct 
		ap.AppointmentPresentationId,
		i.InvoiceDate,	
		i.invoicenumber,
		(isnull(dc.FirstName,'') + ' ' + isnull(dc.LastName,'')) CustomerName,
		i.Mileage,		
		isnull(dv.VehicleMake, '') + ' ' + isnull(dv.VehicleModel, '') + ' ' + isnull(cast(dv.VehicleYear as varchar),'') CarDesc,
       dmsu.[DmsUserNumber] AdvisorCode,
	   (isnull(u.FirstName,'') + ' ' + isnull(u.LastName,'')) AdvisorName,       
       1 Presented,
       (case when PresentationStatus='MC'  then 1 else 0 end) Accepted,
	   (MenuLevel) [MenuLevel],
       [AppointmentTime]    
into #Data
from 
	[dealer].[AppointmentPresentations] ap (nolock)
	join Users u (nolock) on u.UserId = ap.AdvisorId
	join [dealer].[DealerVehicles] dv on dv.[DealerVehicleId] = ap.DealerVehicleId	
	join [dealer].[DealerCustomers] dc (nolock)on dc.[DealerCustomerId] = ap.DealerCustomerId
	left join [dealer].[AppointmentServices] aps on ap.AppointmentPresentationId = aps.AppointmentPresentationId 
	left join [dealer].[DealerDmsUsers] dmsu (nolock) on dmsu.UserId  = ap.[AdvisorId] 
	left join [dealer].[Invoices] i (nolock) on i.InvoiceNumber = ap.InvoiceNumber
	left join [dealer].[InvoiceDetails] d (nolock)on d.InvoiceId = i.InvoiceId
where	[AppointmentTime] between @StartDate and @EndDate and		
		((@Advisor=-88 or @Advisor=-99) or (@Advisor!=-88 and @Advisor!=-99 and u.UserId =@Advisor)) and
		(@ExclLabor0 = 0 or (@ExclLabor0 = 1 and aps.LaborHour != 0)) and
		(@UserOnly = 0 or (@UserOnly = 1 and dmsu.DmsUserName is not null)) and
		(@CPOnly = 0 or (@CPOnly=1 and d.[LaborType] = 'CP'))		
		and ((@DistributorId = 0 and @DealerCompanyId = 0) or (@DealerCompanyId > 0 and ap.DealerId = @DealerCompanyId) or (@DistributorId > 0 and @DealerCompanyId = 0 and ap.DealerId in (select CompanyId from Companies where parentId = @DistributorId)))

--select * from #Data order by AdvisorCode 

	select 
		AppointmentPresentationId,
		InvoiceDate,
		invoicenumber,	
		CustomerName,
		Mileage,
		CarDesc,		
		sum(Presented) Presented, 
		sum(Accepted) Accepted,
		sum(Accepted) result,
		MenuLevel		
	into #Data2
	from #Data 
	group by AppointmentPresentationId,
		InvoiceDate,
		invoicenumber,	
		CustomerName,
		Mileage,
		CarDesc,		
		MenuLevel	
	
	--select * from #tempDateRangeSet2 order by DateRange
	--return
	select *
	into #Data3
	from #Data2
	pivot (sum(result) for MenuLevel in ([1],[2],[3])) as [MenuLevel1]
	
	update #Data3
	set [1]=ISNULL([1] ,0), [2]=ISNULL([2] ,0),[3]=ISNULL([3] ,0)

	select * from #Data3

end