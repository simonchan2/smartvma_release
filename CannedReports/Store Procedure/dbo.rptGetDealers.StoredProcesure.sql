USE [SmartVmaPlus]
GO

/****** Object:  StoredProcedure [dbo].[rptGetDealers]    Script Date: 2016-08-16 11:32:32 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[rptGetDealers]
	@UserRoleCompany varchar(25) = null, --'User-Role-Company'
	@DistributorId int
AS 
BEGIN
	if @UserRoleCompany is null or @UserRoleCompany = '0_0_0' or @DistributorId < 0 
		select -1 companyid, 'Not available' companyname, 0 sort  
	else
	begin
		declare @UserId int
		declare @RoleId int
		declare @CompanyId int

		set @userId = convert(int,substring(@UserRoleCompany,1,CHARINDEX('_',@UserRoleCompany) - 1))

		set @UserRoleCompany = substring(@UserRoleCompany,CHARINDEX('_',@UserRoleCompany) + 1,len(@UserRoleCompany) - CHARINDEX('_',@UserRoleCompany))

		set @RoleId = convert(int,substring(@UserRoleCompany,1,CHARINDEX('_',@UserRoleCompany) - 1))

		set @CompanyId = convert(int,substring(@UserRoleCompany,CHARINDEX('_',@UserRoleCompany) + 1,len(@UserRoleCompany) - CHARINDEX('_',@UserRoleCompany)))
		declare @Role nvarchar(100)
		select @Role = Role from Roles where RoleId = @RoleId
		if (@Role is null)
			select -1 companyid, 'Not available' companyname, 0 sort  
		else
		begin
			if (@Role = 'SystemAdministrator' or @Role = 'BGExecutive' or @Role = 'DistributorAdmin' or @Role = 'DistributorExecutive')
				select 0 CompanyId, 'All' CompanyName, 0 sort
				union
				select distinct t1.CompanyId, t1.CompanyName, 1 sort
				from companies t1
				join [dbo].[Companies] t2 on t1.ParentId = t2.CompanyId
				where t2.ParentId in (select CompanyId from Companies where parentId is null)
					and (@DistributorId = 0 or t2.CompanyId = @DistributorId)		
				order by sort, CompanyName
			else if (@Role = 'DealerAdmin' or @Role = 'Manager')
				select distinct companyid, companyname, 1 sort from companies where CompanyId = @CompanyId
			else 
				select -1 companyid, 'Not available' companyname, 0 sort  
		end
	end
	
end

GO


