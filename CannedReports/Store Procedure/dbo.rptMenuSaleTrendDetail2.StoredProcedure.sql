USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[rptMenuSaleTrendDetail2]    Script Date: 2016-09-30 4:28:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[rptMenuSaleTrendDetail2]
@DistributorId int,
@DealerCompanyId int,
@Advisor	int,
@MonthYear	varchar(20),
@StartDate datetime,
@EndDate	datetime,
@CPOnly		bit=0,
@UserOnly	bit=0,
@ExclLabor0	bit=0
AS 
BEGIN

if (@MonthYear='Total')
begin
	return
end

declare @StartDate1 datetime, @EndDate1 datetime
select @StartDate1	= cast(@MonthYear as date)
select @EndDate1		= dateadd(S,-1, dateadd(m, 1, @StartDate1))

if @StartDate1 > @StartDate
	set @StartDate = @StartDate1

if @EndDate1 < @EndDate
	set @EndDate = @EndDate1

--select @StartDate, @EndDate

select distinct 	   		
		i.InvoiceDate,	
		i.invoicenumber,
		(dc.FirstName + ' ' + dc.LastName) CustomerName,
		i.Mileage,
		isnull(dv.VehicleMake, '') + ' ' + isnull(dv.VehicleModel, '') + ' ' + isnull(cast(dv.VehicleYear as varchar),'') CarDesc,
		(case when PresentationStatus='MC'  then 1 else 0 end) MenuSold,
		(MenuLevel) [MenuLevel],                        
		[AppointmentTime],	   
		isnull(p.PartQty,0) PartQty,
		isnull(p.PartSale,0) PartSale,
		isnull(d.LaborSale,0) LaborSale	 
into #Data
from 
	[dealer].[AppointmentPresentations] ap (nolock)
	join Users u (nolock) on u.UserId = ap.AdvisorId
	join [dealer].[DealerVehicles] dv on dv.[DealerVehicleId] = ap.DealerVehicleId	
	join [dealer].[DealerCustomers] dc (nolock)on dc.[DealerCustomerId] = ap.DealerCustomerId
	left join [dealer].[DealerDmsUsers] dmsu (nolock) on dmsu.UserId  = ap.[AdvisorId] 
	join [dealer].[Invoices] i (nolock) on i.InvoiceNumber = ap.InvoiceNumber
	join [dealer].[InvoiceDetails] d (nolock)on d.InvoiceId = i.InvoiceId
	left join [dealer].[InvoiceDetailParts] p (nolock)on p.InvoiceDetailId = d.InvoiceDetailId
where	[AppointmentTime] between @StartDate and @EndDate and		
		((@Advisor=-88 or @Advisor=-99) or (@Advisor!=-88 and @Advisor!=-99 and u.UserId =@Advisor)) and
		(@ExclLabor0 = 0 or (@ExclLabor0 = 1 and d.LaborHour != 0)) and
		(@UserOnly = 0 or (@UserOnly = 1 and dmsu.DmsUserName is not null)) and
		(@CPOnly = 0 or (@CPOnly=1 and d.[LaborType] = 'CP'))		
		and ((@DistributorId = 0 and @DealerCompanyId = 0) or (@DealerCompanyId > 0 and i.DealerId = @DealerCompanyId) or (@DistributorId > 0 and @DealerCompanyId = 0 and i.DealerId in (select CompanyId from Companies where parentId = @DistributorId)))

--select* from #Data
select 	
	max(InvoiceDate) InvoiceDate,	
	invoicenumber,
	max(CustomerName) CustomerName,
	max(Mileage) Mileage,
	max(CarDesc) CarDesc,
	AppointmentTime,		
	round(sum(PartQty),1) PartQty,
	round(sum(PartSale),1) PartSale,
	round(sum(LaborSale),1) LaborSale,
	MenuSold,		
	round(sum(PartSale) + sum(LaborSale),1) Amount,	
	round(sum(PartSale) + sum(LaborSale),1) Menu$,					
	case when MenuSold = 0 then 0 else round(((sum(PartSale) + sum(LaborSale)) / MenuSold),1) end Permenu$,
	1	Pen,
	1	PencentagePresent,
	[MenuLevel] 
 into #Data2
from #Data
group by InvoiceNumber,AppointmentTime, MenuSold, [MenuLevel]

--select * from #Data2

select * 
into #Data3
from
( 
	select *, 'a'+ cast([MenuLevel] as varchar) aMenuLevel, 	
	'm'+ cast([MenuLevel] as varchar) mMenuLevel
	from #Data2
) s
pivot
( sum(PartQty)
		for [MenuLevel] in ([1], [2], [3])
) as p1
pivot
( sum(Amount)
		for [aMenuLevel] in ([a1], [a2], [a3])
) as p2
pivot
( sum(Permenu$)
		for mMenuLevel in ([m1], [m2], [m3])
) as p2

update #Data3
set [1] = isnull([1],0),[2] = isnull([2],0), [3] = isnull([3],0),
	[a1] = isnull([a1],0),[a2] = isnull([a2],0),[a3] = isnull([a3],0)
,[m1] = isnull([m1],0),[m2] = isnull([m2],0),[m3] = isnull([m3],0)

select * from #Data3

END



