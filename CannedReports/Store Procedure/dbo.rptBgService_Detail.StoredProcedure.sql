USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[rptBgService_Detail]    Script Date: 2016-10-31 9:56:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec [dbo].[rptBgService_Detail] 'nikco','QTY', 1041,'2015-01-01', '2016-01-01',1,0,0,0,0
ALTER PROCEDURE [dbo].[rptBgService_Detail]
	@AdvisorCode nvarchar(45),
	@Content			varchar(255),
	@DistributorId		int,
	@DealerCompanyId	int,
	@StartDate			datetime,
	@EndDate			datetime,
	@By					int,
	@CPOnly				bit,
	@ExclLabor0			bit,
	@UserOnly			bit,
	@ALaCateOnly		bit
AS
BEGIN
    SET NOCOUNT ON;

select distinct i.AdvisorCode,
	i.AdvisorName,
	i.InvoiceId,
	i.dealerId,
	d.InvoiceDetailId,
	p.InvoiceDetailPartId, 
	d.OpCode,
	i.InvoiceDate,
	i.InvoiceNumber ro,
	dc.FullName Customer,
	i.mileage odom,
	dv.VehicleMake + '-' +  dv.VehicleModel + '-' + SUBSTRING(CAST(dv.VehicleYear AS varchar(4)),3,2) [Make],
	isnull(d.LaborSale,0) LaborSale,
	isnull(d.PartSale,0) PartSale,
	isnull(d.LaborCost,0) LaborCost,
	isnull(d.PartCost,0) PartCost,
	isnull(d.LaborHour,0) LaborHour,
	isnull(p.partQty,0) PartQty,
	isnull(adds.AdvisorIncentive,0) AdvisorIncentive
	--sum(isnull(p.PartQty,0)) as Qty
into #temp1
from [dealer].[Invoices] i (nolock)
left join [dealer].[DealerDmsUsers] u (nolock)on u.DmsUserNumber = i.AdvisorCode
join [dealer].[InvoiceDetails] d (nolock)on d.InvoiceId = i.InvoiceId
join [dealer].[InvoiceDetailParts] p (nolock)on p.InvoiceDetailId = d.InvoiceDetailId
join [dealer].[DealerCustomers] dc (nolock)on dc.[DealerCustomerId] = i.DealerCustomerId
join [dealer].[DealerVehicles] dv (nolock)on dv.DealerVehicleId = i.DealerVehicleId
join [dbo].[Vehicles] v (nolock)on v.[VehicleId] = dv.VehicleId
join [dealer].[AppointmentServices] aps (nolock) on aps.DealerId = i.[DealerId] and aps.OpCode = d.[OpCode]
join [dealer].[AdditionalServices] adds (nolock)on adds.[DealerId] = i.[DealerId] and adds.[OpCode] = d.[OpCode]
join [dealer].[AdditionalServiceParts] addsp (nolock) on adds.AdditionalServiceId = addsp.AdditionalServiceId
join [bg].[BgProductsCategories] prodsc (nolock) on prodsc.[BgProductId] = addsp.[BgProductId]
join [bg].[BgProdCategories] bgpc (nolock) on bgpc.[BgProdCategoryId] = prodsc.[BgProdCategoryId] 
where i.AdvisorCode=@AdvisorCode  
and ((@DistributorId = 0 and @DealerCompanyId = 0) or (@DealerCompanyId > 0 and i.DealerId = @DealerCompanyId) or (@DistributorId > 0 and @DealerCompanyId = 0 and i.DealerId in (select CompanyId from Companies where parentId = @DistributorId)))
	--(@DealerCompanyId=0 or (@DealerCompanyId<>0 and i.DealerId = @DealerCompanyId))
and i.InvoiceDate between @StartDate and @EndDate
and (u.[DmsUserTypeId] is null or u.[DmsUserTypeId] = @By) -- advisor or tech
and (@ExclLabor0 = 0 or (@ExclLabor0 = 1 and d.LaborHour != 0))
and (@UserOnly = 0 or (@UserOnly = 1 and u.DmsUserName is not null))
and (@CPOnly = 0 or (@CPOnly=1 and d.[LaborType] = 'CP'))
and (@ALaCateOnly = 0 or (@ALaCateOnly=1 and aps.IsAdditionService=1))


--select * from #Temp1 order by InvoiceId,InvoiceDetailId,InvoiceDetailPartId

--select AdvisorCode,
--	AdvisorName,
--	InvoiceId,
--	sum(PartQty) as QtySum,
--	sum(LaborSale) as LaborSaleSum,
--	sum(PartSale) as PartSaleSum,
--	sum(LaborHour) as LaborHourSum,
--	sum(PartCost) as PartCostSum,
--	sum(LaborCost) as LaborCostSum,
--	sum(AdvisorIncentive) as AdvisorIncentiveSum,
--	1 as PenSum
--	into #temp2
--	from #temp1
--group by AdvisorCode,
--	AdvisorName,
--	InvoiceId

--select * from #temp2

select t1.AdvisorCode,
	t1.AdvisorName,
	t1.InvoiceId,
	t1.InvoiceDate,
	t1.ro,
	t1.Customer,
	t1.odom,
	t1.[Make],
	bgpc.VehicleSystem,
	t1.LaborSale,
	t1.PartSale,
	t1.LaborCost,
	t1.PartCost,
	t1.LaborHour,
	adds.AdvisorIncentive,
	1 as Pen,
	sum(t1.PartQty) as PartQty
	into #temp3
	from #temp1 t1 
	join [dealer].[AdditionalServices] adds (nolock)on adds.[DealerId] = t1.[DealerId] and adds.[OpCode] = t1.[OpCode]
	join [dealer].[AdditionalServiceParts] addsp (nolock) on adds.AdditionalServiceId = addsp.AdditionalServiceId
	join [bg].[BgProductsCategories] prodsc (nolock) on prodsc.[BgProductId] = addsp.[BgProductId]
	join [bg].[BgProdCategories] bgpc (nolock) on bgpc.[BgProdCategoryId] = prodsc.[BgProdCategoryId] 
	group by t1.AdvisorCode,
		t1.AdvisorName,
		t1.InvoiceId,
		t1.InvoiceDate,
		t1.ro,
		t1.Customer,
		t1.odom,
		t1.[Make],
		bgpc.VehicleSystem,
		t1.LaborSale,
		t1.PartSale,
		t1.LaborCost,
		t1.PartCost,
		t1.LaborHour,
		adds.AdvisorIncentive

--select * from #temp3 

if @Content = 'QTY'
begin
	select InvoiceDate, Ro, Customer, odom, Make, VehicleSystem, sum(PartQty) as Content 
		into #temp4
		from #temp3 group by InvoiceDate, Ro, Customer, odom, Make, VehicleSystem

		--select * from #temp4
	
	select *, 1 sort 
		into #temp5
		from #temp4 t
		pivot (sum(Content) for [VehicleSystem] in ([AC],[Batt],[Brake],[Cool],[Diff],[DSL Emiss],[DSL Induct],[DSL Inject],[Ethanol],[Fuel],[GDI],[Oil],[PS],[Trans])) as [VehicleSystem]

	--select * from #temp5

	select 
	InvoiceDate,
	ro,
	Customer,
	odom,
	Make,
	--opcode,
	isnull(AC,0) AC,	
	isnull(Batt,0) Batt,	
	isnull(Brake,0) Brake,	
	isnull(Cool,0) Cool,	
	isnull(Diff,0) Diff,
	isnull(([DSL Emiss]),0) [DSL Emiss],
	isnull(([DSL Induct]),0) [DSL Induct],
	isnull(([DSL Inject]),0) [DSL Inject],
	isnull(([Ethanol]),0) [Ethanol],
	isnull(Fuel,0) Fuel,	
	isnull(Oil,0) Oil,	
	isnull(([GDI]),0) [GDI],
	isnull(PS,0) PS,	
	isnull(Trans,0)	Trans
from #temp5


end

if @Content='Labor$'
begin
	select InvoiceDate, Ro, Customer, odom, Make, VehicleSystem, sum(LaborSale) as Content 
		into #temp6
		from #temp3 group by InvoiceDate, Ro, Customer, odom, Make, VehicleSystem

		--select * from #temp6
	
	select *, 1 sort 
		into #temp7
		from #temp6 t
		pivot (sum(Content) for [VehicleSystem] in ([AC],[Batt],[Brake],[Cool],[Diff],[DSL Emiss],[DSL Induct],[DSL Inject],[Ethanol],[Fuel],[GDI],[Oil],[PS],[Trans])) as [VehicleSystem]

	--select * from #temp7

	select 
		InvoiceDate,
		ro,
		Customer,
		odom,
		Make,
		--opcode,
		isnull(AC,0) AC,	
		isnull(Batt,0) Batt,	
		isnull(Brake,0) Brake,	
		isnull(Cool,0) Cool,	
		isnull(Diff,0) Diff,
		isnull(([DSL Emiss]),0) [DSL Emiss],
		isnull(([DSL Induct]),0) [DSL Induct],
		isnull(([DSL Inject]),0) [DSL Inject],
		isnull(([Ethanol]),0) [Ethanol],
		isnull(Fuel,0) Fuel,	
		isnull(Oil,0) Oil,	
		isnull(([GDI]),0) [GDI],
		isnull(PS,0) PS,	
		isnull(Trans,0)	Trans
	from #temp7
end

if @Content = 'Part$'
begin
	select InvoiceDate, Ro, Customer, odom, Make, VehicleSystem, sum(PartSale) as Content 
		into #temp8
		from #temp3 group by InvoiceDate, Ro, Customer, odom, Make, VehicleSystem

		--select * from #temp8
	
	select *, 1 sort 
		into #temp9
		from #temp8 t
		pivot (sum(Content) for [VehicleSystem] in ([AC],[Batt],[Brake],[Cool],[Diff],[DSL Emiss],[DSL Induct],[DSL Inject],[Ethanol],[Fuel],[GDI],[Oil],[PS],[Trans])) as [VehicleSystem]

	--select * from #temp9

	select 
		InvoiceDate,
		ro,
		Customer,
		odom,
		Make,
		--opcode,
		isnull(AC,0) AC,	
		isnull(Batt,0) Batt,	
		isnull(Brake,0) Brake,	
		isnull(Cool,0) Cool,	
		isnull(Diff,0) Diff,
		isnull(([DSL Emiss]),0) [DSL Emiss],
		isnull(([DSL Induct]),0) [DSL Induct],
		isnull(([DSL Inject]),0) [DSL Inject],
		isnull(([Ethanol]),0) [Ethanol],
		isnull(Fuel,0) Fuel,	
		isnull(Oil,0) Oil,	
		isnull(([GDI]),0) [GDI],
		isnull(PS,0) PS,	
		isnull(Trans,0)	Trans
	from #temp9 

end

if @Content = 'LaborParts$'
begin
		select InvoiceDate, Ro, Customer, odom, Make, VehicleSystem, sum(PartSale) + sum(LaborSale)  as Content 
		into #temp10
		from #temp3 group by InvoiceDate, Ro, Customer, odom, Make, VehicleSystem

		--select * from #temp10
	
	select *, 1 sort 
		into #temp11
		from #temp10 t
		pivot (sum(Content) for [VehicleSystem] in ([AC],[Batt],[Brake],[Cool],[Diff],[DSL Emiss],[DSL Induct],[DSL Inject],[Ethanol],[Fuel],[GDI],[Oil],[PS],[Trans])) as [VehicleSystem]

	--select * from #temp11

	select 
		InvoiceDate,
		ro,
		Customer,
		odom,
		Make,
		--opcode,
		isnull(AC,0) AC,	
		isnull(Batt,0) Batt,	
		isnull(Brake,0) Brake,	
		isnull(Cool,0) Cool,	
		isnull(Diff,0) Diff,
		isnull(([DSL Emiss]),0) [DSL Emiss],
		isnull(([DSL Induct]),0) [DSL Induct],
		isnull(([DSL Inject]),0) [DSL Inject],
		isnull(([Ethanol]),0) [Ethanol],
		isnull(Fuel,0) Fuel,	
		isnull(Oil,0) Oil,	
		isnull(([GDI]),0) [GDI],
		isnull(PS,0) PS,	
		isnull(Trans,0)	Trans
	from #temp11 

end

if @Content = 'Hours'
begin
		select InvoiceDate, Ro, Customer, odom, Make, VehicleSystem, sum(LaborHour)  as Content 
		into #temp12
		from #temp3 group by InvoiceDate, Ro, Customer, odom, Make, VehicleSystem

		--select * from #temp12
	
	select *, 1 sort 
		into #temp13
		from #temp12 t
		pivot (sum(Content) for [VehicleSystem] in ([AC],[Batt],[Brake],[Cool],[Diff],[DSL Emiss],[DSL Induct],[DSL Inject],[Ethanol],[Fuel],[GDI],[Oil],[PS],[Trans])) as [VehicleSystem]

	--select * from #temp13

	select 
		InvoiceDate,
		ro,
		Customer,
		odom,
		Make,
		--opcode,
		isnull(AC,0) AC,	
		isnull(Batt,0) Batt,	
		isnull(Brake,0) Brake,	
		isnull(Cool,0) Cool,	
		isnull(Diff,0) Diff,
		isnull(([DSL Emiss]),0) [DSL Emiss],
		isnull(([DSL Induct]),0) [DSL Induct],
		isnull(([DSL Inject]),0) [DSL Inject],
		isnull(([Ethanol]),0) [Ethanol],
		isnull(Fuel,0) Fuel,	
		isnull(Oil,0) Oil,	
		isnull(([GDI]),0) [GDI],
		isnull(PS,0) PS,	
		isnull(Trans,0)	Trans
	from #temp13 

end

if @Content = 'ELR'
begin
		select InvoiceId, InvoiceDate, Ro, Customer, odom, Make, VehicleSystem, sum(LaborSale) as LaborSaleTotal
		into #temp14
		from #temp3 group by InvoiceId, InvoiceDate, Ro, Customer, odom, Make, VehicleSystem

		--select * from #temp14

		select *, 1 sort 
		into #temp15
		from #temp14 t
		pivot (sum(LaborSaleTotal) for [VehicleSystem] in ([AC],[Batt],[Brake],[Cool],[Diff],[DSL Emiss],[DSL Induct],[DSL Inject],[Ethanol],[Fuel],[GDI],[Oil],[PS],[Trans])) as [VehicleSystem]

		--select * from #temp15

		select InvoiceId, InvoiceDate, Ro, Customer, odom, Make, VehicleSystem, sum(LaborHour) as LaborHourTotal
		into #temp16
		from #temp3 group by InvoiceId, InvoiceDate, Ro, Customer, odom, Make, VehicleSystem

		--select * from #temp16

		select *, 1 sort 
		into #temp17
		from #temp16 t
		pivot (sum(LaborHourTotal) for [VehicleSystem] in ([AC],[Batt],[Brake],[Cool],[Diff],[DSL Emiss],[DSL Induct],[DSL Inject],[Ethanol],[Fuel],[GDI],[Oil],[PS],[Trans])) as [VehicleSystem]

		--select * from #temp17
	

	select 
		t1.InvoiceDate
		,t1.ro
		,t1.Customer
		,t1.odom
		,t1.Make
		,case when isnull(t1.AC,0) = 0 or isnull(t3.AC,0) = 0 then 0 else t1.AC / t3.AC end AC
		,case when isnull(t1.Batt,0) = 0 or isnull(t3.Batt,0) = 0 then 0 else t1.Batt / t3.Batt end [Batt]
		,case when isnull(t1.Brake,0) = 0 or isnull(t3.Brake,0) = 0 then 0 else t1.Brake / t3.Brake end [Brake]
		,case when isnull(t1.Cool,0) = 0 or isnull(t3.Cool,0) = 0 then 0 else t1.Cool / t3.Cool end [Cool]
		,case when isnull(t1.Diff,0) = 0 or isnull(t3.Diff,0) = 0 then 0 else t1.Diff / t3.Diff end [Diff]
		,case when isnull(t1.[DSL Emiss],0) = 0 or isnull(t3.[DSL Emiss],0) = 0 then 0 else t1.[DSL Emiss] / t3.[DSL Emiss] end [DSL Emiss]
		,case when isnull(t1.[DSL Induct],0) = 0 or isnull(t3.[DSL Induct],0) = 0 then 0 else t1.[DSL Induct] / t3.[DSL Induct] end [DSL Induct]
		,case when isnull(t1.[DSL Inject],0) = 0 or isnull(t3.[DSL Inject],0) = 0 then 0 else t1.[DSL Inject] / t3.[DSL Inject] end [DSL Inject]
		,case when isnull(t1.[Ethanol],0) = 0 or isnull(t3.[Ethanol],0) = 0 then 0 else t1.[Ethanol] / t3.[Ethanol] end [Ethanol]
		,case when isnull(t1.[Fuel],0) = 0 or isnull(t3.[Fuel],0) = 0 then 0 else t1.[Fuel] / t3.[Fuel] end [Fuel]
		,case when isnull(t1.[GDI],0) = 0 or isnull(t3.[GDI],0) = 0 then 0 else t1.[GDI] / t3.[GDI] end [GDI]
		,case when isnull(t1.[Oil],0) = 0 or isnull(t3.[Oil],0) = 0 then 0 else t1.[Oil] / t3.[Oil] end [Oil]
		,case when isnull(t1.[PS],0) = 0 or isnull(t3.[PS],0) = 0 then 0 else t1.[PS] / t3.[PS] end [PS]
		,case when isnull(t1.[Trans],0) = 0 or isnull(t3.[Trans],0) = 0 then 0 else t1.[Trans] / t3.[Trans] end [Trans]
	from #temp15 t1 
	inner join #temp17 t3 on t1.InvoiceId = t3.InvoiceId 
	
end

if @Content = 'Commission'
begin
	select InvoiceDate, Ro, Customer, odom, Make, VehicleSystem, sum(AdvisorIncentive) as Content 
		into #temp18
		from #temp3 group by InvoiceDate, Ro, Customer, odom, Make, VehicleSystem

		--select * from #temp18
	
	select *, 1 sort 
		into #temp19
		from #temp18 t
		pivot (sum(Content) for [VehicleSystem] in ([AC],[Batt],[Brake],[Cool],[Diff],[DSL Emiss],[DSL Induct],[DSL Inject],[Ethanol],[Fuel],[GDI],[Oil],[PS],[Trans])) as [VehicleSystem]

	--select * from #temp19

	select 
	InvoiceDate,
		ro,
		Customer,
		odom,
		Make,
		--opcode,
		isnull(AC,0) AC,	
		isnull(Batt,0) Batt,	
		isnull(Brake,0) Brake,	
		isnull(Cool,0) Cool,	
		isnull(Diff,0) Diff,
		isnull(([DSL Emiss]),0) [DSL Emiss],
		isnull(([DSL Induct]),0) [DSL Induct],
		isnull(([DSL Inject]),0) [DSL Inject],
		isnull(([Ethanol]),0) [Ethanol],
		isnull(Fuel,0) Fuel,	
		isnull(Oil,0) Oil,	
		isnull(([GDI]),0) [GDI],
		isnull(PS,0) PS,	
		isnull(Trans,0)	Trans
from #temp19 

end

if @Content = 'Pen'
begin
	select InvoiceDate, Ro, Customer, odom, Make, VehicleSystem, 1 as Content 
		into #temp20
		from #temp3 group by InvoiceDate, Ro, Customer, odom, Make, VehicleSystem

		--select * from #temp20
	
	select *, 1 sort 
		into #temp21
		from #temp20 t
		pivot (sum(Content) for [VehicleSystem] in ([AC],[Batt],[Brake],[Cool],[Diff],[DSL Emiss],[DSL Induct],[DSL Inject],[Ethanol],[Fuel],[GDI],[Oil],[PS],[Trans])) as [VehicleSystem]

	--select * from #temp21

	select 
	InvoiceDate
		,ro
		,Customer
		,odom
		,Make
	,case when isnull([AC],0) = 0 then 0 else 1 end AC
	,case when isnull([Batt],0) = 0 then 0 else 1 end [Batt]
	,case when isnull([Brake],0) = 0 then 0 else 1 end [Brake]
	,case when isnull([Cool],0) = 0 then 0 else 1 end [Cool]
	,case when isnull([Diff],0) = 0 then 0 else 1 end [Diff]
	,case when isnull([DSL Emiss],0) = 0 then 0 else 1 end [DSL Emiss]
	,case when isnull([DSL Induct],0) = 0 then 0 else 1 end [DSL Induct]
	,case when isnull([DSL Inject],0) = 0 then 0 else 1 end [DSL Inject]
	,case when isnull([Ethanol],0) = 0 then 0 else 1 end [Ethanol]
	,case when isnull([Fuel],0) = 0 then 0 else 1 end [Fuel]
	,case when isnull([GDI],0) = 0 then 0 else 1 end [GDI]
	,case when isnull([Oil],0) = 0 then 0 else 1 end [Oil]
	,case when isnull([PS],0) = 0 then 0 else 1 end [PS]
	,case when isnull([Trans],0) = 0 then 0 else 1 end [Trans]
	from #temp21 

end

if @Content = 'GPLabor'
begin
	select InvoiceDate, Ro, Customer, odom, Make, VehicleSystem, sum(LaborSale)-sum(LaborCost) as Content 
		into #temp22
		from #temp3 group by InvoiceDate, Ro, Customer, odom, Make, VehicleSystem

		--select * from #temp22
	
	select *, 1 sort 
		into #temp23
		from #temp22 t
		pivot (sum(Content) for [VehicleSystem] in ([AC],[Batt],[Brake],[Cool],[Diff],[DSL Emiss],[DSL Induct],[DSL Inject],[Ethanol],[Fuel],[GDI],[Oil],[PS],[Trans])) as [VehicleSystem]

	--select * from #temp23

	select 
	InvoiceDate,
		ro,
		Customer,
		odom,
		Make,
		--opcode,
		isnull(AC,0) AC,	
		isnull(Batt,0) Batt,	
		isnull(Brake,0) Brake,	
		isnull(Cool,0) Cool,	
		isnull(Diff,0) Diff,
		isnull(([DSL Emiss]),0) [DSL Emiss],
		isnull(([DSL Induct]),0) [DSL Induct],
		isnull(([DSL Inject]),0) [DSL Inject],
		isnull(([Ethanol]),0) [Ethanol],
		isnull(Fuel,0) Fuel,	
		isnull(Oil,0) Oil,	
		isnull(([GDI]),0) [GDI],
		isnull(PS,0) PS,	
		isnull(Trans,0)	Trans
	from #temp23 


end

if @Content = 'GPParts'
begin
		select InvoiceDate, Ro, Customer, odom, Make, VehicleSystem, sum(PartSale)-sum(PartCost) as Content 
		into #temp24
		from #temp3 group by InvoiceDate, Ro, Customer, odom, Make, VehicleSystem

		--select * from #temp24
	
	select *, 1 sort 
		into #temp25
		from #temp24 t
		pivot (sum(Content) for [VehicleSystem] in ([AC],[Batt],[Brake],[Cool],[Diff],[DSL Emiss],[DSL Induct],[DSL Inject],[Ethanol],[Fuel],[GDI],[Oil],[PS],[Trans])) as [VehicleSystem]

	--select * from #temp25

	select 
	InvoiceDate,
		ro,
		Customer,
		odom,
		Make,
		--opcode,
		isnull(AC,0) AC,	
		isnull(Batt,0) Batt,	
		isnull(Brake,0) Brake,	
		isnull(Cool,0) Cool,	
		isnull(Diff,0) Diff,
		isnull(([DSL Emiss]),0) [DSL Emiss],
		isnull(([DSL Induct]),0) [DSL Induct],
		isnull(([DSL Inject]),0) [DSL Inject],
		isnull(([Ethanol]),0) [Ethanol],
		isnull(Fuel,0) Fuel,	
		isnull(Oil,0) Oil,	
		isnull(([GDI]),0) [GDI],
		isnull(PS,0) PS,	
		isnull(Trans,0)	Trans
	from #temp25 

end

if @Content = 'GPLaborParts'
begin
	select InvoiceDate, Ro, Customer, odom, Make, VehicleSystem, sum(PartSale)-sum(PartCost) + sum(LaborSale) - sum(LaborCost) as Content 
		into #temp26
		from #temp3 group by InvoiceDate, Ro, Customer, odom, Make, VehicleSystem

		--select * from #temp26
	
	select *, 1 sort 
		into #temp27
		from #temp26 t
		pivot (sum(Content) for [VehicleSystem] in ([AC],[Batt],[Brake],[Cool],[Diff],[DSL Emiss],[DSL Induct],[DSL Inject],[Ethanol],[Fuel],[GDI],[Oil],[PS],[Trans])) as [VehicleSystem]

	--select * from #temp27

	select 
	InvoiceDate,
		ro,
		Customer,
		odom,
		Make,
		--opcode,
		isnull(AC,0) AC,	
		isnull(Batt,0) Batt,	
		isnull(Brake,0) Brake,	
		isnull(Cool,0) Cool,	
		isnull(Diff,0) Diff,
		isnull(([DSL Emiss]),0) [DSL Emiss],
		isnull(([DSL Induct]),0) [DSL Induct],
		isnull(([DSL Inject]),0) [DSL Inject],
		isnull(([Ethanol]),0) [Ethanol],
		isnull(Fuel,0) Fuel,	
		isnull(Oil,0) Oil,	
		isnull(([GDI]),0) [GDI],
		isnull(PS,0) PS,	
		isnull(Trans,0)	Trans
	from #temp27 

end

END


