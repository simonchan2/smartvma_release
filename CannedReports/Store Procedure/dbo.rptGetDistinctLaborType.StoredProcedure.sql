
/****** Object:  StoredProcedure [dbo].[rptGetDistinctLaborType]    Script Date: 2016-08-19 3:30:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rptGetDistinctLaborType]
@CPOnly				bit=0
AS
BEGIN
    SET NOCOUNT ON;

IF @CPOnly = 1 
BEGIN
	SELECT 'CP' [LaborType] 
END
ELSE
BEGIN
	select distinct isnull([LaborType], ' ') [LaborType] from  [dealer].[InvoiceDetails]  
END

END
