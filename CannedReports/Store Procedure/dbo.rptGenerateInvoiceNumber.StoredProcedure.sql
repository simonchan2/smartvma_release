USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[rptGenerateInvoiceNumber]    Script Date: 2016-11-03 1:09:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Max Zheng
-- Create date: 2016-09-06
-- Description:	Generate a new InvoiceNumber for DMS invoice process simulation
-- =============================================
ALTER PROCEDURE [dbo].[rptGenerateInvoiceNumber]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    declare @InvoiceNumber int

	select @InvoiceNumber = max(case when IsNumeric(InvoiceNumber) = 1 then convert(int,InvoiceNumber) else 0 end) from dealer.Invoices where InvoiceNumber like '9000%'

	if @InvoiceNumber < 9000000
		set @InvoiceNumber = 9000001
	else
		set @InvoiceNumber = @InvoiceNumber + 1
	select @InvoiceNumber 
END