USE [SmartVmaPlus]
GO
/****** Object:  StoredProcedure [dbo].[rptGetAdvisors]    Script Date: 2016-08-29 4:01:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[rptGetAdvisors]
@DistributorId		int,
@DealerCompanyId	int
AS 
BEGIN		
		  select  distinct u.UserId, dmsu.[DmsUserNumber] AdvisorCode, case when  dmsu.[DmsUserNumber] is null then u.FirstName + ' ' + u.LastName else u.FirstName + ' ' + u.LastName + ' (' +  dmsu.[DmsUserNumber] + ')' end as AdvisorName,2 sort
		  from dbo.Users as u
		  inner join [dealer].[AppointmentPresentations] a on u.userId = a.AdvisorId
		  left join [dealer].[DealerDmsUsers] dmsu on dmsu.UserId = u.UserId 
		  where
			((@DistributorId = 0 and @DealerCompanyId = 0) or (@DealerCompanyId > 0 and a.DealerId = @DealerCompanyId) or (@DistributorId > 0 and @DealerCompanyId = 0 and a.DealerId in (select CompanyId from Companies where parentId = @DistributorId)))
          union
          select -88 UserId, 'AllAdvisors' AdvisorCode, 'All Advisors' AdvisorName, 0 sort
          union
          select -99 UserId, 'Consolidated' AdvisorCode, 'Consolidated' AdvisorName, 1 sort
          order by sort, AdvisorName
END
