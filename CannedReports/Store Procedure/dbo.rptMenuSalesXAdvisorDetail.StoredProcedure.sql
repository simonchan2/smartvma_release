USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[rptMenuSalesXAdvisorDetail]    Script Date: 2016-10-03 11:07:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[rptMenuSalesXAdvisorDetail]
@AdvisorCode		int,
@Content			varchar(255),
@DistributorId		int,
@DealerCompanyId	int,
@StartDate			datetime,
@EndDate			datetime,
@Level				int=0, -- 0=All, 1=Level 1, 2=Level 2, 3=Level 3
@CPOnly				bit=0,
@ExclLabor0			bit=0,
@UserOnly			bit=0
AS
BEGIN
    SET NOCOUNT ON;

select distinct 
	i.InvoiceDate,	
	i.invoicenumber,
	(dc.FirstName + ' ' + dc.LastName) CustomerName,
	i.Mileage,
	isnull(dv.VehicleMake, '') + ' ' + isnull(dv.VehicleModel, '') + ' ' + isnull(cast(dv.VehicleYear as varchar),'') CarDesc,
	finalMileage = dbo.udf_GetMileage(i.Mileage),	
	d.InvoiceDetailId as KeyId,
	p.InvoiceDetailPartId as KeyId2,
	round(CASE @Content		 
		WHEN 'QTY' THEN (PartQty)
		WHEN 'Labor$' THEN ([LaborSale])
		WHEN 'Part$' THEN (d.[PartSale])
		WHEN 'LaborParts$' THEN ([LaborSale] + d.[PartSale]) 
		WHEN 'Hours' THEN (d.[LaborHour])
		WHEN 'ELR' THEN case when isnull(d.[LaborHour],0) = 0 then 0 else ([LaborSale]/d.[LaborHour]) end		
		WHEN 'GPLabor' THEN (d.[LaborSale] - d.[LaborCost])
		WHEN 'GPParts' THEN (d.[PartSale] - d.[PartCost])
		WHEN 'GPLaborParts' THEN (d.[LaborSale] - d.[LaborCost] + d.[PartSale] - d.[PartCost])						
	END,1) as content,
	isnull(LaborSale,0) as LaborSale,
	isnull(LaborHour,0) as LaborHour		
into #temp1
from
	[dealer].[AppointmentPresentations] ap (nolock)
	join Users u (nolock) on u.UserId = ap.AdvisorId
	join [dealer].[DealerVehicles] dv on dv.[DealerVehicleId] = ap.DealerVehicleId	
	join [dealer].[DealerCustomers] dc (nolock)on dc.[DealerCustomerId] = ap.DealerCustomerId
	left join [dealer].[DealerDmsUsers] dmsu (nolock) on dmsu.UserId  = ap.[AdvisorId] 
	left join [dealer].[Invoices] i (nolock) on i.InvoiceNumber = ap.InvoiceNumber
	left join [dealer].[InvoiceDetails] d (nolock)on d.InvoiceId = i.InvoiceId
	left join [dealer].[InvoiceDetailParts] p (nolock)on p.InvoiceDetailId = d.InvoiceDetailId
where 	
	((@DistributorId = 0 and @DealerCompanyId = 0) or (@DealerCompanyId > 0 and i.DealerId = @DealerCompanyId) or (@DistributorId > 0 and @DealerCompanyId = 0 and i.DealerId in (select CompanyId from dbo.Companies where parentId = @DistributorId))) and
	i.InvoiceDate between @StartDate and @EndDate and
	(@ExclLabor0 = 0 or (@ExclLabor0 = 1 and d.LaborHour != 0)) and
	(@UserOnly = 0 or (@UserOnly = 1 and dmsu.DmsUserName is not null)) and
	(@CPOnly = 0 or (@CPOnly=1 and d.[LaborType] = 'CP')) 
	and (@Level = 0 or (@Level != 0 and MenuLevel=@Level) )	
	and u.Userid = @AdvisorCode

--select * from #temp1 order by InvoiceNumber

select InvoiceDate, InvoiceNumber, CustomerName, Mileage, CarDesc, FinalMileage, round(case @Content when 'ELR' then (case when sum(LaborHour) = 0 then 0 else sum(LaborSale)/sum(laborHour) end) else sum(Content) end,1) as ContentSum
	into #temp2
	from #temp1
group by InvoiceDate, InvoiceNumber, CustomerName, Mileage, CarDesc, FinalMileage

select *, 1 sort 
into #temp3
from #temp2 t
pivot (sum(contentSum) for [finalMileage] in ([5000],[10000],[15000],[20000],[25000],[30000],[35000],[40000],[45000],[50000],[55000],[60000],[65000],[70000],[75000],[80000],[85000],[90000],[95000],[100000],[105000],[110000],[115000],[120000],[125000],[130000],[135000],[140000],[145000],[150000],[155000],[160000],[165000],[170000],[175000],[180000],[185000],[190000],[195000],[200000])) as [Mileage]

select * from #temp3

END


