USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[rptCreateInvoices]    Script Date: 2016-09-12 11:05:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Max Zheng
-- Create date: 2016-09-06
-- Description:	Create simulation of DMS invoice for specified InvoiceNumber which is used in SmartVma web application
-- =============================================
ALTER PROCEDURE [dbo].[rptCreateInvoices]
	@InvoiceNumber int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @AppointmentPresentationId int
select @AppointmentPresentationId = AppointmentPresentationId from [dealer].[AppointmentPresentations] where InvoiceNumber = convert(nvarchar(50),@InvoiceNumber) and  [PresentationStatus]= 'MC'


begin tran
	declare @RoNumber nvarchar(50) = null
	select @RoNumber=RoNumber from (select top 1 RoNumber from dealer.AppointmentPresentations where RoNumber <> '' and InvoiceNumber = @InvoiceNumber) tt
	if @RoNumber = null 
	begin
		update dealer.AppointmentPresentations set RoNumber = @InvoiceNumber where InvoiceNumber = @InvoiceNumber
		if @@Error > 0
		begin
			rollback tran
			print 'rollback tran 0'
			return
		end
	end
	insert into [dealer].[Invoices](
						DealerId, 
						DealerVehicleId, 
						DealerCustomerId, 
						InvoiceDmsId, 
						InvoiceNumber, 
						RoNumber, 
						InvoiceDate, 
						InvoiceType, 
						CustomerName, 
						AddressLine1, 
						AddressLine2, 
						City, 
						State, 
						Zip, 
						Phone1, 
						Phone2, 
						CellPhone, 
						AdvisorCode, 
						AdvisorName, 
						VehicleLicense, 
						Mileage, 
						TimeCreated, 
						TimeUpdated, 
						InvoiceExtId
						)
	select t1.DealerId,
			t1.DealerVehicleId,
			t1.DealerCustomerId,
			InvoiceNumber,
			InvoiceNumber,
			InvoiceNumber,
			GetDate(),
			'',
			t3.[FirstName] + ' ' + t3.[LastName],
			t3.[AddressLine1],
			t3.[AddressLine2],
			t3.[City],
			t4.[CountryState],
			t3.Zip,
			t3.HomePhone,
			t3.WorkPhone,
			t3.CellPhone,
			convert(nvarchar(45),AdvisorId),
			t2.FirstName + ' ' + t2.LastName,
			VehicleLicense,
			Mileage,
			getdate(),
			getdate(),
			@InvoiceNumber
	from [dealer].[AppointmentPresentations] t1
	inner join [dbo].[Users] t2 on t1.[AdvisorId] = t2.userId
	left join [dealer].[DealerCustomers] t3 on t1.[DealerCustomerId] = t3.[DealerCustomerId]
	left join [dbo].[CountryStates] t4 on t3.[CountryStateId] = t4.[CountryStateId]
	where t1.AppointmentPresentationId = @AppointmentPresentationId

	if @@error = 0 and @@rowcount = 1
	begin
		begin
			declare @InvoiceId int
			declare @InvoiceDetailId int
			declare @AppointmentServiceId int
			set @InvoiceId = SCOPE_IDENTITY()

			declare AppointmentServiceCursor Cursor for
			select [AppointmentServicesId] from [dealer].[AppointmentServices] where AppointmentPresentationId = @AppointmentPresentationId

			Open AppointmentServiceCursor
			Fetch next from AppointmentServiceCursor into @AppointmentServiceId

			while @@FETCH_STATUS = 0
			begin
				insert into [dealer].[InvoiceDetails](
									DealerId, 
									InvoiceId, 
									InvoiceLineNo, 
									InvoiceLineCode, 
									LineAfter, 
									OpCode, 
									OpDescription, 
									LaborType, 
									LaborHour, 
									LaborCost, 
									LaborSale, 
									PartSale, 
									PartCost, 
									TechCode, 
									TechName, 
									Complaint, 
									TimeCreated, 
									TimeUpdated, 
									InvoiceDetailExtId)
					select t1.DealerId,
						@InvoiceId,
						null,
						'A', --dummy 
						null,
						t2.OpCode,
						t2.OpDescription,
						null,
						t2.LaborHour,
						0,
						t2.LaborPrice,
						t2.PartsPrice,
						0,
						null,
						null,
						t2.Comment,
						getutcdate(),
						getutcdate(),
						null
					from [dealer].[AppointmentPresentations] t1
					inner join [dealer].[AppointmentServices] t2 on t1.AppointmentPresentationId = t2.AppointmentPresentationId
					where t2.AppointmentServicesId = @AppointmentServiceId
				
				if @@error = 0 
				begin
					set @InvoiceDetailId = SCOPE_IDENTITY()

					insert into [dealer].[InvoiceDetailParts](
								DealerId, 
								InvoiceDetailId, 
								SequenceNumber, 
								PartCode, 
								PartDescription, 
								PartQty, 
								PartCost, 
								PartSale, 
								PartDiscount, 
								PartDate, 
								Comment, 
								TimeCreated, 
								TimeUpdated, 
								InvoiceDetailPartExtId)
						select t3.DealerId,
								@InvoiceDetailId,
								convert(nvarchar(50),ROW_NUMBER() over(order by t3.AppointmentServicePartId)),
								isnull(t3.PartNo,''),
								isnull(t3.PartName,'') + isnull(t3.FluidType,''),
								t3.Quantity,
								0,
								t3.UnitPrice*t3.Quantity,
								null,
								t3.TimeUpdated,
								null,
								GetUtcDate(),
								GetUtcDate(),
								null
							from [dealer].[AppointmentServices] t2 
							inner join [dealer].[AppointmentServiceParts] t3 on t2.AppointmentServicesId = t3.AppointmentServicesId 
						where t2.AppointmentServicesId = @AppointmentServiceId
					if @@error > 0
					begin
						rollback tran
						print 'rollback tran 4'
					end
				end
				else
				begin
					rollback tran 
					print 'rollback tran 3'
				end

				Fetch next from AppointmentServiceCursor into @AppointmentServiceId
				
			end

			CLOSE AppointmentServiceCursor   
			DEALLOCATE AppointmentServiceCursor
		end
		if @@error = 0 
		begin
			commit tran
			print 'commit tran'
			
		end
		else
		begin
			rollback tran
			print 'rollback tran 2'
		end
	end
	else
	begin
		rollback tran
		print 'rollback tran 1'
	end

END
