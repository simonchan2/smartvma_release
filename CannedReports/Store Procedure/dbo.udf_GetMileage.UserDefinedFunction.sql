USE [SmartVmaPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[udf_GetMileage]    Script Date: 2016-07-11 3:51:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[udf_GetMileage] 
(
	@mileage		int
)
RETURNS int
AS
BEGIN

	declare @sInterval int,@eInterval int, @finalMileage int, @minMileage int
	set @sInterval = 5000;
	set @eInterval = 200000;

	DECLARE @Interval TABLE (
        [Interval] [int]         
	);
			
	WITH CTE AS
	(
		SELECT @sInterval AS interval
		UNION ALL
		SELECT interval + @sInterval
		FROM CTE
		WHERE interval + @sInterval <= @eInterval   
	)
	insert into @Interval
	SELECT
		interval interval	
	FROM CTE
	
	select @finalMileage = interval
	from @Interval where interval=@mileage
	if @finalMileage is null
	begin	
		select @minMileage= max(interval)
		from @Interval i1
		where i1.interval < @mileage	

		if ((@minMileage +2500) > @mileage)
		begin
			set @finalMileage = @minMileage 
		end
		else
		begin
			select @finalMileage=min(interval)
			from @Interval i1
			where i1.interval > @mileage
		end
	end
	if @finalMileage is null
	begin
		if @mileage > @eInterval
		begin
			set @finalMileage = @eInterval
		end
		else
		begin
			set @finalMileage = @sInterval
		end

	end
	return @finalMileage

END

GO
