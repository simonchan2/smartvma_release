USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[rptBgService]    Script Date: 2016-10-31 9:48:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec [dbo].[rptBgService] 'QTY', 1041,'2015-01-01', '2016-01-01',1,0,0,0,0
ALTER PROCEDURE [dbo].[rptBgService]
@Content			varchar(255),
@DistributorId		int,
@DealerCompanyId	int,
@StartDate			datetime,
@EndDate			datetime,
@By					int=1,
@CPOnly				bit=0,
@ExclLabor0			bit=0,
@UserOnly			bit=0,
@ALaCateOnly		bit=0
AS
BEGIN
    SET NOCOUNT ON;

-- Get distinct rows
select distinct i.AdvisorCode,
	i.AdvisorName,
	i.InvoiceId,
	i.dealerId,
	d.InvoiceDetailId,
	p.InvoiceDetailPartId, 
	d.OpCode,
	isnull(d.LaborSale,0) LaborSale,
	isnull(d.PartSale,0) PartSale,
	isnull(d.LaborCost,0) LaborCost,
	isnull(d.PartCost,0) PartCost,
	isnull(d.LaborHour,0) LaborHour,
	isnull(p.partQty,0) PartQty,
	isnull(adds.AdvisorIncentive,0) AdvisorIncentive
	--sum(isnull(p.PartQty,0)) as Qty
into #temp0
from [dealer].[Invoices] i (nolock)
left join [dealer].[DealerDmsUsers] u (nolock)on u.DmsUserNumber = i.AdvisorCode
join [dealer].[InvoiceDetails] d (nolock)on d.InvoiceId = i.InvoiceId
join [dealer].[InvoiceDetailParts] p (nolock)on p.InvoiceDetailId = d.InvoiceDetailId
join [dealer].[DealerCustomers] dc (nolock)on dc.[DealerCustomerId] = i.DealerCustomerId
join [dealer].[DealerVehicles] dv (nolock)on dv.DealerVehicleId = i.DealerVehicleId
join [dbo].[Vehicles] v (nolock)on v.[VehicleId] = dv.VehicleId
join [dealer].[AppointmentServices] aps (nolock) on aps.DealerId = i.[DealerId] and aps.OpCode = d.[OpCode]
join [dealer].[AdditionalServices] adds (nolock)on adds.[DealerId] = i.[DealerId] and adds.[OpCode] = d.[OpCode]
join [dealer].[AdditionalServiceParts] addsp (nolock) on adds.AdditionalServiceId = addsp.AdditionalServiceId
join [bg].[BgProductsCategories] prodsc (nolock) on prodsc.[BgProductId] = addsp.[BgProductId]
join [bg].[BgProdCategories] bgpc (nolock) on bgpc.[BgProdCategoryId] = prodsc.[BgProdCategoryId] 
where ((@DistributorId = 0 and @DealerCompanyId = 0) or (@DealerCompanyId > 0 and i.DealerId = @DealerCompanyId) or (@DistributorId > 0 and @DealerCompanyId = 0 and i.DealerId in (select CompanyId from Companies where parentId = @DistributorId)))
	--(@DealerCompanyId=0 or (@DealerCompanyId<>0 and i.DealerId = @DealerCompanyId))
and i.InvoiceDate between @StartDate and @EndDate
and (u.[DmsUserTypeId] is null or u.[DmsUserTypeId] = @By) -- advisor or tech
and (@ExclLabor0 = 0 or (@ExclLabor0 = 1 and d.LaborHour != 0))
and (@UserOnly = 0 or (@UserOnly = 1 and u.DmsUserName is not null))
and (@CPOnly = 0 or (@CPOnly=1 and d.[LaborType] = 'CP'))
and (@ALaCateOnly = 0 or (@ALaCateOnly=1 and aps.IsAdditionService=1))


select AdvisorCode,
		AdvisorName,
		InvoiceId,
		dealerId,
		InvoiceDetailId,
		OpCode,
		LaborSale,
		PartSale,
		LaborCost,
		PartCost,
		LaborHour,
		AdvisorIncentive,
		sum(PartQty) as PartQty
	into #temp1
	from #temp0 
	group by AdvisorCode,
		AdvisorName,
		InvoiceId,
		dealerId,
		InvoiceDetailId,
		OpCode,
		LaborSale,
		PartSale,
		LaborCost,
		PartCost,
		LaborHour,
		AdvisorIncentive


--select * from #Temp1 order by InvoiceId,InvoiceDetailId,InvoiceDetailPartId

select AdvisorCode,
	AdvisorName,
	InvoiceId,
	sum(PartQty) as QtySum,
	sum(LaborSale) as LaborSaleSum,
	sum(PartSale) as PartSaleSum,
	sum(LaborHour) as LaborHourSum,
	sum(PartCost) as PartCostSum,
	sum(LaborCost) as LaborCostSum,
	sum(AdvisorIncentive) as AdvisorIncentiveSum,
	1 as PenSum
	into #temp2
	from #temp1
group by AdvisorCode,
	AdvisorName,
	InvoiceId

--select * from #temp2

select distinct t1.AdvisorCode,
	t1.AdvisorName,
	t1.InvoiceId,
	--t1.dealerId,
	--t1.InvoiceDetailId,
	--t1.InvoiceDetailPartId, 
	--t1.OpCode,
	bgpc.VehicleSystem,
	t1.LaborSale,
	t1.PartSale,
	t1.LaborCost,
	t1.PartCost,
	t1.LaborHour,
	t1.PartQty,
	adds.AdvisorIncentive,
	1 as Pen
	into #temp3
	from #temp1 t1 
	join [dealer].[AdditionalServices] adds (nolock)on adds.[DealerId] = t1.[DealerId] and adds.[OpCode] = t1.[OpCode]
	join [dealer].[AdditionalServiceParts] addsp (nolock) on adds.AdditionalServiceId = addsp.AdditionalServiceId
	join [bg].[BgProductsCategories] prodsc (nolock) on prodsc.[BgProductId] = addsp.[BgProductId]
	join [bg].[BgProdCategories] bgpc (nolock) on bgpc.[BgProdCategoryId] = prodsc.[BgProdCategoryId] 

--select * from #temp3 

if @Content = 'QTY'
begin
	select AdvisorCode, AdvisorName, InvoiceId, VehicleSystem, sum(PartQty) as Content 
		into #temp4
		from #temp3 group by AdvisorCode, AdvisorName, InvoiceId, VehicleSystem

--select * from #temp4
	
	select *, 1 sort 
		into #temp5
		from #temp4 t
		pivot (sum(Content) for [VehicleSystem] in ([AC],[Batt],[Brake],[Cool],[Diff],[DSL Emiss],[DSL Induct],[DSL Inject],[Ethanol],[Fuel],[GDI],[Oil],[PS],[Trans])) as [VehicleSystem]

--select * from #temp5

	select 
	t1.AdvisorCode
	,t1.[AdvisorName]
	, count(t1.invoiceid) Ro
	,sum(t2.QtySum) Content 
	,isnull(sum([AC]),0) AC
	,isnull(sum([Batt]),0) [Batt]
	,isnull(sum([Brake]),0) [Brake]
	,isnull(sum([Cool]),0) [Cool]
	,isnull(sum([Diff]),0) [Diff]
	,isnull(sum([DSL Emiss]),0) [DSL Emiss]
	,isnull(sum([DSL Induct]),0) [DSL Induct]
	,isnull(sum([DSL Inject]),0) [DSL Inject]
	,isnull(sum([Ethanol]),0) [Ethanol]
	,isnull(sum([Fuel]),0) [Fuel]
	,isnull(sum([GDI]),0) [GDI]
	,isnull(sum([Oil]),0) [Oil]
	,isnull(sum([PS]),0) [PS]
	,isnull(sum([Trans]),0) [Trans]
	,max(sort) sort  
from #temp5 t1 inner join #temp2 t2 on t1.InvoiceId = t2.InvoiceId 
group by t1.AdvisorCode, t1.[AdvisorName]
union
select 
	' '
	,'Total' [Advisor Name]
	,count(t1.invoiceid) Ro
	,sum(t2.QtySum) Content
	,isnull(sum([AC]),0) AC
	,isnull(sum([Batt]),0) [Batt]
	,isnull(sum([Brake]),0) [Brake]
	,isnull(sum([Cool]),0) [Cool]
	,isnull(sum([Diff]),0) [Diff]
	,isnull(sum([DSL Emiss]),0) [DSL Emiss]
	,isnull(sum([DSL Induct]),0) [DSL Induct]
	,isnull(sum([DSL Inject]),0) [DSL Inject]
	,isnull(sum([Ethanol]),0) [Ethanol]
	,isnull(sum([Fuel]),0) [Fuel]
	,isnull(sum([GDI]),0) [GDI]
	,isnull(sum([Oil]),0) [Oil]
	,isnull(sum([PS]),0) [PS]
	,isnull(sum([Trans]),0) [Trans]
	,2 sort  
from #temp5 t1 inner join #temp2 t2 on t1.InvoiceId = t2.InvoiceId 
order by sort, [AdvisorName]

end

if @Content='Labor$'
begin
	select AdvisorCode, AdvisorName, InvoiceId, VehicleSystem, sum(LaborSale) as Content 
		into #temp6
		from #temp3 group by AdvisorCode, AdvisorName, InvoiceId, VehicleSystem

		--select * from #temp6
	
	select *, 1 sort 
		into #temp7
		from #temp6 t
		pivot (sum(Content) for [VehicleSystem] in ([AC],[Batt],[Brake],[Cool],[Diff],[DSL Emiss],[DSL Induct],[DSL Inject],[Ethanol],[Fuel],[GDI],[Oil],[PS],[Trans])) as [VehicleSystem]

	--select * from #temp7

	select 
	t1.AdvisorCode
	,t1.[AdvisorName]
	, count(t1.invoiceid) Ro
	,sum(t2.LaborSaleSum) Content 
	,isnull(sum([AC]),0) AC
	,isnull(sum([Batt]),0) [Batt]
	,isnull(sum([Brake]),0) [Brake]
	,isnull(sum([Cool]),0) [Cool]
	,isnull(sum([Diff]),0) [Diff]
	,isnull(sum([DSL Emiss]),0) [DSL Emiss]
	,isnull(sum([DSL Induct]),0) [DSL Induct]
	,isnull(sum([DSL Inject]),0) [DSL Inject]
	,isnull(sum([Ethanol]),0) [Ethanol]
	,isnull(sum([Fuel]),0) [Fuel]
	,isnull(sum([GDI]),0) [GDI]
	,isnull(sum([Oil]),0) [Oil]
	,isnull(sum([PS]),0) [PS]
	,isnull(sum([Trans]),0) [Trans]
	,max(sort) sort  
	from #temp7 t1 inner join #temp2 t2 on t1.InvoiceId = t2.InvoiceId 
	group by t1.AdvisorCode, t1.[AdvisorName]
	union
	select 
		' '
		,'Total' [Advisor Name]
		,count(t1.invoiceid) Ro
		,sum(t2.LaborSaleSum) Content
		,isnull(sum([AC]),0) AC
		,isnull(sum([Batt]),0) [Batt]
		,isnull(sum([Brake]),0) [Brake]
		,isnull(sum([Cool]),0) [Cool]
		,isnull(sum([Diff]),0) [Diff]
		,isnull(sum([DSL Emiss]),0) [DSL Emiss]
		,isnull(sum([DSL Induct]),0) [DSL Induct]
		,isnull(sum([DSL Inject]),0) [DSL Inject]
		,isnull(sum([Ethanol]),0) [Ethanol]
		,isnull(sum([Fuel]),0) [Fuel]
		,isnull(sum([GDI]),0) [GDI]
		,isnull(sum([Oil]),0) [Oil]
		,isnull(sum([PS]),0) [PS]
		,isnull(sum([Trans]),0) [Trans]
		,2 sort  
	from #temp7 t1 inner join #temp2 t2 on t1.InvoiceId = t2.InvoiceId 
	order by sort, [AdvisorName]

end

if @Content = 'Part$'
begin
	select AdvisorCode, AdvisorName, InvoiceId, VehicleSystem, sum(PartSale) as Content 
		into #temp8
		from #temp3 group by AdvisorCode, AdvisorName, InvoiceId, VehicleSystem

		--select * from #temp8
	
	select *, 1 sort 
		into #temp9
		from #temp8 t
		pivot (sum(Content) for [VehicleSystem] in ([AC],[Batt],[Brake],[Cool],[Diff],[DSL Emiss],[DSL Induct],[DSL Inject],[Ethanol],[Fuel],[GDI],[Oil],[PS],[Trans])) as [VehicleSystem]

	--select * from #temp9

	select 
		t1.AdvisorCode
		,t1.[AdvisorName]
		, count(t1.invoiceid) Ro
		,sum(t2.PartSaleSum) Content 
		,isnull(sum([AC]),0) AC
		,isnull(sum([Batt]),0) [Batt]
		,isnull(sum([Brake]),0) [Brake]
		,isnull(sum([Cool]),0) [Cool]
		,isnull(sum([Diff]),0) [Diff]
		,isnull(sum([DSL Emiss]),0) [DSL Emiss]
		,isnull(sum([DSL Induct]),0) [DSL Induct]
		,isnull(sum([DSL Inject]),0) [DSL Inject]
		,isnull(sum([Ethanol]),0) [Ethanol]
		,isnull(sum([Fuel]),0) [Fuel]
		,isnull(sum([GDI]),0) [GDI]
		,isnull(sum([Oil]),0) [Oil]
		,isnull(sum([PS]),0) [PS]
		,isnull(sum([Trans]),0) [Trans]
		,max(sort) sort  
	from #temp9 t1 inner join #temp2 t2 on t1.InvoiceId = t2.InvoiceId 
	group by t1.AdvisorCode, t1.[AdvisorName]
	union
	select 
		' '
		,'Total' [Advisor Name]
		,count(t1.invoiceid) Ro
		,sum(t2.PartSaleSum) Content
		,isnull(sum([AC]),0) AC
		,isnull(sum([Batt]),0) [Batt]
		,isnull(sum([Brake]),0) [Brake]
		,isnull(sum([Cool]),0) [Cool]
		,isnull(sum([Diff]),0) [Diff]
		,isnull(sum([DSL Emiss]),0) [DSL Emiss]
		,isnull(sum([DSL Induct]),0) [DSL Induct]
		,isnull(sum([DSL Inject]),0) [DSL Inject]
		,isnull(sum([Ethanol]),0) [Ethanol]
		,isnull(sum([Fuel]),0) [Fuel]
		,isnull(sum([GDI]),0) [GDI]
		,isnull(sum([Oil]),0) [Oil]
		,isnull(sum([PS]),0) [PS]
		,isnull(sum([Trans]),0) [Trans]
		,2 sort  
	from #temp9 t1 inner join #temp2 t2 on t1.InvoiceId = t2.InvoiceId 
	order by sort, [AdvisorName]

end

if @Content = 'LaborParts$'
begin
		select AdvisorCode, AdvisorName, InvoiceId, VehicleSystem, sum(PartSale) + sum(LaborSale)  as Content 
		into #temp10
		from #temp3 group by AdvisorCode, AdvisorName, InvoiceId, VehicleSystem

		--select * from #temp10
	
	select *, 1 sort 
		into #temp11
		from #temp10 t
		pivot (sum(Content) for [VehicleSystem] in ([AC],[Batt],[Brake],[Cool],[Diff],[DSL Emiss],[DSL Induct],[DSL Inject],[Ethanol],[Fuel],[GDI],[Oil],[PS],[Trans])) as [VehicleSystem]

	--select * from #temp11

	select 
		t1.AdvisorCode
		,t1.[AdvisorName]
		, count(t1.invoiceid) Ro
		,sum(t2.PartSaleSum) + sum(t2.LaborSaleSum) Content 
		,isnull(sum([AC]),0) AC
		,isnull(sum([Batt]),0) [Batt]
		,isnull(sum([Brake]),0) [Brake]
		,isnull(sum([Cool]),0) [Cool]
		,isnull(sum([Diff]),0) [Diff]
		,isnull(sum([DSL Emiss]),0) [DSL Emiss]
		,isnull(sum([DSL Induct]),0) [DSL Induct]
		,isnull(sum([DSL Inject]),0) [DSL Inject]
		,isnull(sum([Ethanol]),0) [Ethanol]
		,isnull(sum([Fuel]),0) [Fuel]
		,isnull(sum([GDI]),0) [GDI]
		,isnull(sum([Oil]),0) [Oil]
		,isnull(sum([PS]),0) [PS]
		,isnull(sum([Trans]),0) [Trans]
		,max(sort) sort  
	from #temp11 t1 inner join #temp2 t2 on t1.InvoiceId = t2.InvoiceId 
	group by t1.AdvisorCode, t1.[AdvisorName]
	union
	select 
		' '
		,'Total' [Advisor Name]
		,count(t1.invoiceid) Ro
		,sum(t2.PartSaleSum) + sum(t2.LaborSaleSum) Content
		,isnull(sum([AC]),0) AC
		,isnull(sum([Batt]),0) [Batt]
		,isnull(sum([Brake]),0) [Brake]
		,isnull(sum([Cool]),0) [Cool]
		,isnull(sum([Diff]),0) [Diff]
		,isnull(sum([DSL Emiss]),0) [DSL Emiss]
		,isnull(sum([DSL Induct]),0) [DSL Induct]
		,isnull(sum([DSL Inject]),0) [DSL Inject]
		,isnull(sum([Ethanol]),0) [Ethanol]
		,isnull(sum([Fuel]),0) [Fuel]
		,isnull(sum([GDI]),0) [GDI]
		,isnull(sum([Oil]),0) [Oil]
		,isnull(sum([PS]),0) [PS]
		,isnull(sum([Trans]),0) [Trans]
		,2 sort  
	from #temp11 t1 inner join #temp2 t2 on t1.InvoiceId = t2.InvoiceId 
	order by sort, [AdvisorName]

end

if @Content = 'Hours'
begin
		select AdvisorCode, AdvisorName, InvoiceId, VehicleSystem, sum(LaborHour)  as Content 
		into #temp12
		from #temp3 group by AdvisorCode, AdvisorName, InvoiceId, VehicleSystem

		--select * from #temp12
	
	select *, 1 sort 
		into #temp13
		from #temp12 t
		pivot (sum(Content) for [VehicleSystem] in ([AC],[Batt],[Brake],[Cool],[Diff],[DSL Emiss],[DSL Induct],[DSL Inject],[Ethanol],[Fuel],[GDI],[Oil],[PS],[Trans])) as [VehicleSystem]

	--select * from #temp13

	select 
		t1.AdvisorCode
		,t1.[AdvisorName]
		, count(t1.invoiceid) Ro
		,sum(t2.LaborHourSum) Content 
		,isnull(sum([AC]),0) AC
		,isnull(sum([Batt]),0) [Batt]
		,isnull(sum([Brake]),0) [Brake]
		,isnull(sum([Cool]),0) [Cool]
		,isnull(sum([Diff]),0) [Diff]
		,isnull(sum([DSL Emiss]),0) [DSL Emiss]
		,isnull(sum([DSL Induct]),0) [DSL Induct]
		,isnull(sum([DSL Inject]),0) [DSL Inject]
		,isnull(sum([Ethanol]),0) [Ethanol]
		,isnull(sum([Fuel]),0) [Fuel]
		,isnull(sum([GDI]),0) [GDI]
		,isnull(sum([Oil]),0) [Oil]
		,isnull(sum([PS]),0) [PS]
		,isnull(sum([Trans]),0) [Trans]
		,max(sort) sort  
	from #temp13 t1 inner join #temp2 t2 on t1.InvoiceId = t2.InvoiceId 
	group by t1.AdvisorCode, t1.[AdvisorName]
	union
	select 
		' '
		,'Total' [Advisor Name]
		,count(t1.invoiceid) Ro
		,sum(t2.LaborHourSum) Content
		,isnull(sum([AC]),0) AC
		,isnull(sum([Batt]),0) [Batt]
		,isnull(sum([Brake]),0) [Brake]
		,isnull(sum([Cool]),0) [Cool]
		,isnull(sum([Diff]),0) [Diff]
		,isnull(sum([DSL Emiss]),0) [DSL Emiss]
		,isnull(sum([DSL Induct]),0) [DSL Induct]
		,isnull(sum([DSL Inject]),0) [DSL Inject]
		,isnull(sum([Ethanol]),0) [Ethanol]
		,isnull(sum([Fuel]),0) [Fuel]
		,isnull(sum([GDI]),0) [GDI]
		,isnull(sum([Oil]),0) [Oil]
		,isnull(sum([PS]),0) [PS]
		,isnull(sum([Trans]),0) [Trans]
		,2 sort  
	from #temp13 t1 inner join #temp2 t2 on t1.InvoiceId = t2.InvoiceId 
	order by sort, [AdvisorName]

end

if @Content = 'ELR'
begin
		select AdvisorCode, AdvisorName, InvoiceId, VehicleSystem, sum(LaborSale) as LaborSaleTotal
		into #temp14
		from #temp3 group by AdvisorCode, AdvisorName, InvoiceId, VehicleSystem

		--select * from #temp14

		select *, 1 sort 
		into #temp15
		from #temp14 t
		pivot (sum(LaborSaleTotal) for [VehicleSystem] in ([AC],[Batt],[Brake],[Cool],[Diff],[DSL Emiss],[DSL Induct],[DSL Inject],[Ethanol],[Fuel],[GDI],[Oil],[PS],[Trans])) as [VehicleSystem]

		--select * from #temp15

		select AdvisorCode, AdvisorName, InvoiceId, VehicleSystem, sum(LaborHour) as LaborHourTotal
		into #temp16
		from #temp3 group by AdvisorCode, AdvisorName, InvoiceId, VehicleSystem

		--select * from #temp16

		select *, 1 sort 
		into #temp17
		from #temp16 t
		pivot (sum(LaborHourTotal) for [VehicleSystem] in ([AC],[Batt],[Brake],[Cool],[Diff],[DSL Emiss],[DSL Induct],[DSL Inject],[Ethanol],[Fuel],[GDI],[Oil],[PS],[Trans])) as [VehicleSystem]

		--select * from #temp17
	

	select 
		t1.AdvisorCode
		,t1.[AdvisorName]
		, count(t1.invoiceid) Ro
		,case when sum(t2.LaborHourSum) = 0 then 0 else sum(t2.LaborSaleSum)/sum(t2.laborHourSum) end Content 
		,case when isnull(sum(t1.AC),0) = 0 or isnull(sum(t3.AC),0) = 0 then 0 else sum(t1.AC) / sum(t3.AC) end AC
		,case when isnull(sum(t1.Batt),0) = 0 or isnull(sum(t3.Batt),0) = 0 then 0 else sum(t1.Batt) / sum(t3.Batt) end [Batt]
		,case when isnull(sum(t1.Brake),0) = 0 or isnull(sum(t3.Brake),0) = 0 then 0 else sum(t1.Brake) / sum(t3.Brake) end [Brake]
		,case when isnull(sum(t1.Cool),0) = 0 or isnull(sum(t3.Cool),0) = 0 then 0 else sum(t1.Cool) / sum(t3.Cool) end [Cool]
		,case when isnull(sum(t1.Diff),0) = 0 or isnull(sum(t3.Diff),0) = 0 then 0 else sum(t1.Diff) / sum(t3.Diff) end [Diff]
		,case when isnull(sum(t1.[DSL Emiss]),0) = 0 or isnull(sum(t3.[DSL Emiss]),0) = 0 then 0 else sum(t1.[DSL Emiss]) / sum(t3.[DSL Emiss]) end [DSL Emiss]
		,case when isnull(sum(t1.[DSL Induct]),0) = 0 or isnull(sum(t3.[DSL Induct]),0) = 0 then 0 else sum(t1.[DSL Induct]) / sum(t3.[DSL Induct]) end [DSL Induct]
		,case when isnull(sum(t1.[DSL Inject]),0) = 0 or isnull(sum(t3.[DSL Inject]),0) = 0 then 0 else sum(t1.[DSL Inject]) / sum(t3.[DSL Inject]) end [DSL Inject]
		,case when isnull(sum(t1.[Ethanol]),0) = 0 or isnull(sum(t3.[Ethanol]),0) = 0 then 0 else sum(t1.[Ethanol]) / sum(t3.[Ethanol]) end [Ethanol]
		,case when isnull(sum(t1.[Fuel]),0) = 0 or isnull(sum(t3.[Fuel]),0) = 0 then 0 else sum(t1.[Fuel]) / sum(t3.[Fuel]) end [Fuel]
		,case when isnull(sum(t1.[GDI]),0) = 0 or isnull(sum(t3.[GDI]),0) = 0 then 0 else sum(t1.[GDI]) / sum(t3.[GDI]) end [GDI]
		,case when isnull(sum(t1.[Oil]),0) = 0 or isnull(sum(t3.[Oil]),0) = 0 then 0 else sum(t1.[Oil]) / sum(t3.[Oil]) end [Oil]
		,case when isnull(sum(t1.[PS]),0) = 0 or isnull(sum(t3.[PS]),0) = 0 then 0 else sum(t1.[PS]) / sum(t3.[PS]) end [PS]
		,case when isnull(sum(t1.[Trans]),0) = 0 or isnull(sum(t3.[Trans]),0) = 0 then 0 else sum(t1.[Trans]) / sum(t3.[Trans]) end [Trans]
		,max(t1.sort) sort  
	from #temp15 t1 
	inner join #temp17 t3 on t1.InvoiceId = t3.InvoiceId
	inner join #temp2 t2 on t1.InvoiceId = t2.InvoiceId 
	group by t1.AdvisorCode, t1.[AdvisorName]
	union
	select 
		' '
		,'Total' [Advisor Name]
		,count(t1.invoiceid) Ro
		,case when sum(t2.LaborHourSum) = 0 then 0 else sum(t2.LaborSaleSum)/sum(t2.laborHourSum) end Content 
		,case when isnull(sum(t1.AC),0) = 0 or isnull(sum(t3.AC),0) = 0 then 0 else sum(t1.AC) / sum(t3.AC) end AC
		,case when isnull(sum(t1.Batt),0) = 0 or isnull(sum(t3.Batt),0) = 0 then 0 else sum(t1.Batt) / sum(t3.Batt) end [Batt]
		,case when isnull(sum(t1.Brake),0) = 0 or isnull(sum(t3.Brake),0) = 0 then 0 else sum(t1.Brake) / sum(t3.Brake) end [Brake]
		,case when isnull(sum(t1.Cool),0) = 0 or isnull(sum(t3.Cool),0) = 0 then 0 else sum(t1.Cool) / sum(t3.Cool) end [Cool]
		,case when isnull(sum(t1.Diff),0) = 0 or isnull(sum(t3.Diff),0) = 0 then 0 else sum(t1.Diff) / sum(t3.Diff) end [Diff]
		,case when isnull(sum(t1.[DSL Emiss]),0) = 0 or isnull(sum(t3.[DSL Emiss]),0) = 0 then 0 else sum(t1.[DSL Emiss]) / sum(t3.[DSL Emiss]) end [DSL Emiss]
		,case when isnull(sum(t1.[DSL Induct]),0) = 0 or isnull(sum(t3.[DSL Induct]),0) = 0 then 0 else sum(t1.[DSL Induct]) / sum(t3.[DSL Induct]) end [DSL Induct]
		,case when isnull(sum(t1.[DSL Inject]),0) = 0 or isnull(sum(t3.[DSL Inject]),0) = 0 then 0 else sum(t1.[DSL Inject]) / sum(t3.[DSL Inject]) end [DSL Inject]
		,case when isnull(sum(t1.[Ethanol]),0) = 0 or isnull(sum(t3.[Ethanol]),0) = 0 then 0 else sum(t1.[Ethanol]) / sum(t3.[Ethanol]) end [Ethanol]
		,case when isnull(sum(t1.[Fuel]),0) = 0 or isnull(sum(t3.[Fuel]),0) = 0 then 0 else sum(t1.[Fuel]) / sum(t3.[Fuel]) end [Fuel]
		,case when isnull(sum(t1.[GDI]),0) = 0 or isnull(sum(t3.[GDI]),0) = 0 then 0 else sum(t1.[GDI]) / sum(t3.[GDI]) end [GDI]
		,case when isnull(sum(t1.[Oil]),0) = 0 or isnull(sum(t3.[Oil]),0) = 0 then 0 else sum(t1.[Oil]) / sum(t3.[Oil]) end [Oil]
		,case when isnull(sum(t1.[PS]),0) = 0 or isnull(sum(t3.[PS]),0) = 0 then 0 else sum(t1.[PS]) / sum(t3.[PS]) end [PS]
		,case when isnull(sum(t1.[Trans]),0) = 0 or isnull(sum(t3.[Trans]),0) = 0 then 0 else sum(t1.[Trans]) / sum(t3.[Trans]) end [Trans]
		,2 sort  
	from #temp15 t1 
	inner join #temp17 t3 on t1.InvoiceId = t3.InvoiceId
	inner join #temp2 t2 on t1.InvoiceId = t2.InvoiceId 
	order by sort, [AdvisorName]

end

if @Content = 'Commission'
begin
	select AdvisorCode, AdvisorName, InvoiceId, VehicleSystem, sum(AdvisorIncentive) as Content 
		into #temp18
		from #temp3 group by AdvisorCode, AdvisorName, InvoiceId, VehicleSystem

		--select * from #temp18
	
	select *, 1 sort 
		into #temp19
		from #temp18 t
		pivot (sum(Content) for [VehicleSystem] in ([AC],[Batt],[Brake],[Cool],[Diff],[DSL Emiss],[DSL Induct],[DSL Inject],[Ethanol],[Fuel],[GDI],[Oil],[PS],[Trans])) as [VehicleSystem]

	--select * from #temp19

	select 
	t1.AdvisorCode
	,t1.[AdvisorName]
	, count(t1.invoiceid) Ro
	,sum(t2.AdvisorIncentiveSum) Content 
	,isnull(sum([AC]),0) AC
	,isnull(sum([Batt]),0) [Batt]
	,isnull(sum([Brake]),0) [Brake]
	,isnull(sum([Cool]),0) [Cool]
	,isnull(sum([Diff]),0) [Diff]
	,isnull(sum([DSL Emiss]),0) [DSL Emiss]
	,isnull(sum([DSL Induct]),0) [DSL Induct]
	,isnull(sum([DSL Inject]),0) [DSL Inject]
	,isnull(sum([Ethanol]),0) [Ethanol]
	,isnull(sum([Fuel]),0) [Fuel]
	,isnull(sum([GDI]),0) [GDI]
	,isnull(sum([Oil]),0) [Oil]
	,isnull(sum([PS]),0) [PS]
	,isnull(sum([Trans]),0) [Trans]
	,max(sort) sort  
from #temp19 t1 inner join #temp2 t2 on t1.InvoiceId = t2.InvoiceId 
group by t1.AdvisorCode, t1.[AdvisorName]
union
select 
	' '
	,'Total' [Advisor Name]
	,count(t1.invoiceid) Ro
	,sum(t2.AdvisorIncentiveSum) Content
	,isnull(sum([AC]),0) AC
	,isnull(sum([Batt]),0) [Batt]
	,isnull(sum([Brake]),0) [Brake]
	,isnull(sum([Cool]),0) [Cool]
	,isnull(sum([Diff]),0) [Diff]
	,isnull(sum([DSL Emiss]),0) [DSL Emiss]
	,isnull(sum([DSL Induct]),0) [DSL Induct]
	,isnull(sum([DSL Inject]),0) [DSL Inject]
	,isnull(sum([Ethanol]),0) [Ethanol]
	,isnull(sum([Fuel]),0) [Fuel]
	,isnull(sum([GDI]),0) [GDI]
	,isnull(sum([Oil]),0) [Oil]
	,isnull(sum([PS]),0) [PS]
	,isnull(sum([Trans]),0) [Trans]
	,2 sort  
from #temp19 t1 inner join #temp2 t2 on t1.InvoiceId = t2.InvoiceId 
order by sort, [AdvisorName]

end

if @Content = 'Pen'
begin
	select AdvisorCode, AdvisorName, InvoiceId, VehicleSystem, 1 as Content 
		into #temp20
		from #temp3 group by AdvisorCode, AdvisorName, InvoiceId, VehicleSystem

		--select * from #temp20
	
	select *, 1 sort 
		into #temp21
		from #temp20 t
		pivot (sum(Content) for [VehicleSystem] in ([AC],[Batt],[Brake],[Cool],[Diff],[DSL Emiss],[DSL Induct],[DSL Inject],[Ethanol],[Fuel],[GDI],[Oil],[PS],[Trans])) as [VehicleSystem]

	--select * from #temp21

	select 
	t1.AdvisorCode
	,t1.[AdvisorName]
	, count(t1.invoiceid) Ro
	,1 Content 
	,case when isnull(sum([AC]),0) = 0 then 0 else 1 end AC
	,case when isnull(sum([Batt]),0) = 0 then 0 else 1 end [Batt]
	,case when isnull(sum([Brake]),0) = 0 then 0 else 1 end [Brake]
	,case when isnull(sum([Cool]),0) = 0 then 0 else 1 end [Cool]
	,case when isnull(sum([Diff]),0) = 0 then 0 else 1 end [Diff]
	,case when isnull(sum([DSL Emiss]),0) = 0 then 0 else 1 end [DSL Emiss]
	,case when isnull(sum([DSL Induct]),0) = 0 then 0 else 1 end [DSL Induct]
	,case when isnull(sum([DSL Inject]),0) = 0 then 0 else 1 end [DSL Inject]
	,case when isnull(sum([Ethanol]),0) = 0 then 0 else 1 end [Ethanol]
	,case when isnull(sum([Fuel]),0) = 0 then 0 else 1 end [Fuel]
	,case when isnull(sum([GDI]),0) = 0 then 0 else 1 end [GDI]
	,case when isnull(sum([Oil]),0) = 0 then 0 else 1 end [Oil]
	,case when isnull(sum([PS]),0) = 0 then 0 else 1 end [PS]
	,case when isnull(sum([Trans]),0) = 0 then 0 else 1 end [Trans]
	,max(sort) sort  
	from #temp21 t1 inner join #temp2 t2 on t1.InvoiceId = t2.InvoiceId 
	group by t1.AdvisorCode, t1.[AdvisorName]
	union
	select 
	' '
	,'Total' [Advisor Name]
	,count(t1.invoiceid) Ro
	,1 Content
	,case when isnull(sum([AC]),0) = 0 then 0 else 1 end AC
	,case when isnull(sum([Batt]),0) = 0 then 0 else 1 end [Batt]
	,case when isnull(sum([Brake]),0) = 0 then 0 else 1 end [Brake]
	,case when isnull(sum([Cool]),0) = 0 then 0 else 1 end [Cool]
	,case when isnull(sum([Diff]),0) = 0 then 0 else 1 end [Diff]
	,case when isnull(sum([DSL Emiss]),0) = 0 then 0 else 1 end [DSL Emiss]
	,case when isnull(sum([DSL Induct]),0) = 0 then 0 else 1 end [DSL Induct]
	,case when isnull(sum([DSL Inject]),0) = 0 then 0 else 1 end [DSL Inject]
	,case when isnull(sum([Ethanol]),0) = 0 then 0 else 1 end [Ethanol]
	,case when isnull(sum([Fuel]),0) = 0 then 0 else 1 end [Fuel]
	,case when isnull(sum([GDI]),0) = 0 then 0 else 1 end [GDI]
	,case when isnull(sum([Oil]),0) = 0 then 0 else 1 end [Oil]
	,case when isnull(sum([PS]),0) = 0 then 0 else 1 end [PS]
	,case when isnull(sum([Trans]),0) = 0 then 0 else 1 end [Trans]
	,2 sort  
	from #temp21 t1 inner join #temp2 t2 on t1.InvoiceId = t2.InvoiceId 
	order by sort, [AdvisorName]


end

if @Content = 'GPLabor'
begin
	select AdvisorCode, AdvisorName, InvoiceId, VehicleSystem, sum(LaborSale)-sum(LaborCost) as Content 
		into #temp22
		from #temp3 group by AdvisorCode, AdvisorName, InvoiceId, VehicleSystem

		--select * from #temp22
	
	select *, 1 sort 
		into #temp23
		from #temp22 t
		pivot (sum(Content) for [VehicleSystem] in ([AC],[Batt],[Brake],[Cool],[Diff],[DSL Emiss],[DSL Induct],[DSL Inject],[Ethanol],[Fuel],[GDI],[Oil],[PS],[Trans])) as [VehicleSystem]

	--select * from #temp23

	select 
	t1.AdvisorCode
	,t1.[AdvisorName]
	, count(t1.invoiceid) Ro
	,sum(t2.LaborSaleSum) - sum(t2.LaborCostSum) Content 
	,isnull(sum([AC]),0) AC
	,isnull(sum([Batt]),0) [Batt]
	,isnull(sum([Brake]),0) [Brake]
	,isnull(sum([Cool]),0) [Cool]
	,isnull(sum([Diff]),0) [Diff]
	,isnull(sum([DSL Emiss]),0) [DSL Emiss]
	,isnull(sum([DSL Induct]),0) [DSL Induct]
	,isnull(sum([DSL Inject]),0) [DSL Inject]
	,isnull(sum([Ethanol]),0) [Ethanol]
	,isnull(sum([Fuel]),0) [Fuel]
	,isnull(sum([GDI]),0) [GDI]
	,isnull(sum([Oil]),0) [Oil]
	,isnull(sum([PS]),0) [PS]
	,isnull(sum([Trans]),0) [Trans]
	,max(sort) sort  
	from #temp23 t1 inner join #temp2 t2 on t1.InvoiceId = t2.InvoiceId 
	group by t1.AdvisorCode, t1.[AdvisorName]
	union
	select 
		' '
		,'Total' [Advisor Name]
		,count(t1.invoiceid) Ro
		,sum(t2.LaborSaleSum) - sum(t2.LaborCostSum) Content 
		,isnull(sum([AC]),0) AC
		,isnull(sum([Batt]),0) [Batt]
		,isnull(sum([Brake]),0) [Brake]
		,isnull(sum([Cool]),0) [Cool]
		,isnull(sum([Diff]),0) [Diff]
		,isnull(sum([DSL Emiss]),0) [DSL Emiss]
		,isnull(sum([DSL Induct]),0) [DSL Induct]
		,isnull(sum([DSL Inject]),0) [DSL Inject]
		,isnull(sum([Ethanol]),0) [Ethanol]
		,isnull(sum([Fuel]),0) [Fuel]
		,isnull(sum([GDI]),0) [GDI]
		,isnull(sum([Oil]),0) [Oil]
		,isnull(sum([PS]),0) [PS]
		,isnull(sum([Trans]),0) [Trans]
		,2 sort  
	from #temp23 t1 inner join #temp2 t2 on t1.InvoiceId = t2.InvoiceId 
	order by sort, [AdvisorName]


end

if @Content = 'GPParts'
begin
		select AdvisorCode, AdvisorName, InvoiceId, VehicleSystem, sum(PartSale)-sum(PartCost) as Content 
		into #temp24
		from #temp3 group by AdvisorCode, AdvisorName, InvoiceId, VehicleSystem

		--select * from #temp24
	
	select *, 1 sort 
		into #temp25
		from #temp24 t
		pivot (sum(Content) for [VehicleSystem] in ([AC],[Batt],[Brake],[Cool],[Diff],[DSL Emiss],[DSL Induct],[DSL Inject],[Ethanol],[Fuel],[GDI],[Oil],[PS],[Trans])) as [VehicleSystem]

	--select * from #temp25

	select 
	t1.AdvisorCode
	,t1.[AdvisorName]
	, count(t1.invoiceid) Ro
	,sum(t2.PartSaleSum) - sum(t2.PartCostSum) Content 
	,isnull(sum([AC]),0) AC
	,isnull(sum([Batt]),0) [Batt]
	,isnull(sum([Brake]),0) [Brake]
	,isnull(sum([Cool]),0) [Cool]
	,isnull(sum([Diff]),0) [Diff]
	,isnull(sum([DSL Emiss]),0) [DSL Emiss]
	,isnull(sum([DSL Induct]),0) [DSL Induct]
	,isnull(sum([DSL Inject]),0) [DSL Inject]
	,isnull(sum([Ethanol]),0) [Ethanol]
	,isnull(sum([Fuel]),0) [Fuel]
	,isnull(sum([GDI]),0) [GDI]
	,isnull(sum([Oil]),0) [Oil]
	,isnull(sum([PS]),0) [PS]
	,isnull(sum([Trans]),0) [Trans]
	,max(sort) sort  
	from #temp25 t1 inner join #temp2 t2 on t1.InvoiceId = t2.InvoiceId 
	group by t1.AdvisorCode, t1.[AdvisorName]
	union
	select 
		' '
		,'Total' [Advisor Name]
		,count(t1.invoiceid) Ro
		,sum(t2.PartSaleSum) - sum(t2.PartCostSum) Content 
		,isnull(sum([AC]),0) AC
		,isnull(sum([Batt]),0) [Batt]
		,isnull(sum([Brake]),0) [Brake]
		,isnull(sum([Cool]),0) [Cool]
		,isnull(sum([Diff]),0) [Diff]
		,isnull(sum([DSL Emiss]),0) [DSL Emiss]
		,isnull(sum([DSL Induct]),0) [DSL Induct]
		,isnull(sum([DSL Inject]),0) [DSL Inject]
		,isnull(sum([Ethanol]),0) [Ethanol]
		,isnull(sum([Fuel]),0) [Fuel]
		,isnull(sum([GDI]),0) [GDI]
		,isnull(sum([Oil]),0) [Oil]
		,isnull(sum([PS]),0) [PS]
		,isnull(sum([Trans]),0) [Trans]
		,2 sort  
	from #temp25 t1 inner join #temp2 t2 on t1.InvoiceId = t2.InvoiceId 
	order by sort, [AdvisorName]

end

if @Content = 'GPLaborParts'
begin
	select AdvisorCode, AdvisorName, InvoiceId, VehicleSystem, sum(PartSale)-sum(PartCost) + sum(LaborSale) - sum(LaborCost) as Content 
		into #temp26
		from #temp3 group by AdvisorCode, AdvisorName, InvoiceId, VehicleSystem

		--select * from #temp26
	
	select *, 1 sort 
		into #temp27
		from #temp26 t
		pivot (sum(Content) for [VehicleSystem] in ([AC],[Batt],[Brake],[Cool],[Diff],[DSL Emiss],[DSL Induct],[DSL Inject],[Ethanol],[Fuel],[GDI],[Oil],[PS],[Trans])) as [VehicleSystem]

	--select * from #temp27

	select 
	t1.AdvisorCode
	,t1.[AdvisorName]
	, count(t1.invoiceid) Ro
	,sum(t2.PartSaleSum) - sum(t2.PartCostSum) + sum(t2.LaborSaleSum) - sum(t2.LaborCostSum) Content 
	,isnull(sum([AC]),0) AC
	,isnull(sum([Batt]),0) [Batt]
	,isnull(sum([Brake]),0) [Brake]
	,isnull(sum([Cool]),0) [Cool]
	,isnull(sum([Diff]),0) [Diff]
	,isnull(sum([DSL Emiss]),0) [DSL Emiss]
	,isnull(sum([DSL Induct]),0) [DSL Induct]
	,isnull(sum([DSL Inject]),0) [DSL Inject]
	,isnull(sum([Ethanol]),0) [Ethanol]
	,isnull(sum([Fuel]),0) [Fuel]
	,isnull(sum([GDI]),0) [GDI]
	,isnull(sum([Oil]),0) [Oil]
	,isnull(sum([PS]),0) [PS]
	,isnull(sum([Trans]),0) [Trans]
	,max(sort) sort  
	from #temp27 t1 inner join #temp2 t2 on t1.InvoiceId = t2.InvoiceId 
	group by t1.AdvisorCode, t1.[AdvisorName]
	union
	select 
		' '
		,'Total' [Advisor Name]
		,count(t1.invoiceid) Ro
		,sum(t2.PartSaleSum) - sum(t2.PartCostSum) + sum(t2.LaborSaleSum) - sum(t2.LaborCostSum) Content 
		,isnull(sum([AC]),0) AC
		,isnull(sum([Batt]),0) [Batt]
		,isnull(sum([Brake]),0) [Brake]
		,isnull(sum([Cool]),0) [Cool]
		,isnull(sum([Diff]),0) [Diff]
		,isnull(sum([DSL Emiss]),0) [DSL Emiss]
		,isnull(sum([DSL Induct]),0) [DSL Induct]
		,isnull(sum([DSL Inject]),0) [DSL Inject]
		,isnull(sum([Ethanol]),0) [Ethanol]
		,isnull(sum([Fuel]),0) [Fuel]
		,isnull(sum([GDI]),0) [GDI]
		,isnull(sum([Oil]),0) [Oil]
		,isnull(sum([PS]),0) [PS]
		,isnull(sum([Trans]),0) [Trans]
		,2 sort  
	from #temp27 t1 inner join #temp2 t2 on t1.InvoiceId = t2.InvoiceId 
	order by sort, [AdvisorName]

end

End




