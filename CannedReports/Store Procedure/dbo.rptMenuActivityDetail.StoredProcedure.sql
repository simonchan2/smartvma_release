USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[rptMenuActivityDetail]    Script Date: 2016-11-03 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[rptMenuActivityDetail]
@DistributorId		int,
@DealerCompanyId	int,
@UserId				int,
@StartDate			datetime,
@EndDate			datetime,
@UserOnly			bit
AS
BEGIN
    SET NOCOUNT ON;
-- MENU ACTIVITY REPORT

select distinct
	u.UserId, 
	dmsu.[DmsUserNumber] AdvisorCode,	
	(isnull(u.FirstName,'') + ' ' + isnull(u.LastName,'')) AdvisorName,	
	i.InvoiceDate,	
	i.invoicenumber,	
	(isnull(dc.FirstName,'') + ' ' + isnull(dc.LastName,'')) CustomerName,	
	i.Mileage,
	isnull(dv.VehicleMake, '') + ' ' + isnull(dv.VehicleModel, '') + ' ' + isnull(cast(dv.VehicleYear as varchar),'') CarDesc,
	(MenuLevel) [MenuLevel],		
	(1) Presented,
	(case when PresentationStatus='MC' then 1 else 0 end) Accepted,
	(case when PresentationStatus='MD' then 1 else 0 end) Declined
	--Status of the meny presenrtation; V: Void; C: Closed; M: Menu Presented; MC: Menu Accepted; MD: Menu Declined; MK: Menu Parked			
into #temp1
from [dealer].[AppointmentPresentations] ap 
join Users u on u.UserId = ap.[AdvisorId]
left join [dealer].[DealerDmsUsers] dmsu on dmsu.UserId = ap.[AdvisorId] 
join [dealer].[DealerVehicles] dv on dv.[DealerVehicleId] = ap.DealerVehicleId
join [dealer].[DealerCustomers] dc (nolock)on dc.[DealerCustomerId] = ap.DealerCustomerId
left join [dealer].[Invoices] i (nolock) on i.InvoiceNumber = ap.InvoiceNumber
left join [dealer].[InvoiceDetails] d (nolock)on d.InvoiceId = i.InvoiceId
left join [dealer].[InvoiceDetailParts] p (nolock)on p.InvoiceDetailId = d.InvoiceDetailId
where 
[AppointmentTime] between @StartDate and @EndDate
and (@UserOnly = 0 or (@UserOnly = 1 and dmsu.DmsUserName is not null)) 
and ((@DistributorId = 0 and @DealerCompanyId = 0) or (@DealerCompanyId > 0 and ap.DealerId = @DealerCompanyId) or (@DistributorId > 0 and @DealerCompanyId = 0 and ap.DealerId in (select CompanyId from dbo.Companies where parentId = @DistributorId)))
and u.UserId = @UserId


select
	UserId,
	AdvisorCode,
	AdvisorName,
	max(InvoiceDate) InvoiceDate,	
	invoicenumber,
	max(CustomerName) CustomerName,
	max(Mileage) Mileage,
	max(CarDesc) CarDesc,
	sum(Presented) Presented, 
	sum(Accepted) Accepted, 
	sum(Accepted) Result, 
	(sum(Accepted) /sum(Presented) * 100) Closing, 
	sum(Declined) Declined, 
	(sum(Presented) - sum(Accepted) - sum(Declined)) UnFinsihed,
	MenuLevel
into #tempAdvisorData
from #temp1 
group by UserId, AdvisorCode, AdvisorName, invoicenumber, CustomerName, MenuLevel



select *
into #tempAdvisorData2
from #tempAdvisorData
pivot (count([result]) for MenuLevel in ([1],[2],[3])) as [MenuLevel]


select * from #tempAdvisorData2


END