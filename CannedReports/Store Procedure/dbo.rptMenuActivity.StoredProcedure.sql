/****** Object:  StoredProcedure [dbo].[rptMenuActivity]    Script Date: 2016-08-26 11:44:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rptMenuActivity]
@DistributorId		int,
@DealerCompanyId	int,
@StartDate			datetime,
@EndDate			datetime,
@UserOnly			bit
AS
BEGIN
    SET NOCOUNT ON;
-- MENU ACTIVITY REPORT
select distinct 
	u.UserId,
	dmsu.[DmsUserNumber] AdvisorCode,
	u.FirstName + ' ' + u.LastName AdvisorName, 
	--dmsu.[DmsUserName] AdvisorName, 
	ap.InvoiceNumber,	
	(MenuLevel) [MenuLevel],		
	(1) Presented,
	(case when PresentationStatus='MC' then 1 else 0 end) Accepted, 
	(case when PresentationStatus='MD' then 1 else 0 end) Declined 	--Status of the meny presenrtation; V: Void; C: Closed; M: Menu Presented; MC: Menu Accepted; MD: Menu Declined; MK: Menu Parked				
	,dc.DealerCustomerid
into #temp1
from [dealer].[AppointmentPresentations] ap 
join Users u on u.UserId = ap.[AdvisorId]
left join [dealer].[DealerDmsUsers] dmsu on dmsu.UserId = ap.[AdvisorId] 
join [dealer].[DealerCustomers] dc (nolock)on dc.[DealerCustomerId] = ap.DealerCustomerId
where 
((@DistributorId = 0 and @DealerCompanyId = 0) or (@DealerCompanyId > 0 and ap.DealerId = @DealerCompanyId) or (@DistributorId > 0 and @DealerCompanyId = 0 and ap.DealerId in (select CompanyId from Companies where parentId = @DistributorId))) 
and [AppointmentTime] between @StartDate and @EndDate
and (@UserOnly = 0 or (@UserOnly = 1 and dmsu.DmsUserName is not null)) 
--and u.userid = 31

--select * from #temp1

select 
	UserId,
	AdvisorCode,
	AdvisorName,	
	sum(Presented) Presented, 
	sum(Accepted) Accepted, 
	sum(Accepted) Result, 
	sum(Declined) Declined,
	MenuLevel	
into #tempAdvisorData
from #temp1
group by UserId, AdvisorCode, AdvisorName, MenuLevel


--select * from #tempAdvisorData

select *
into #tempAdvisorData2
from #tempAdvisorData
pivot (count([result]) for MenuLevel in ([1],[2],[3])) as [MenuLevel]


select 
	UserId,
	AdvisorCode,
	AdvisorName,	
	sum(Presented) Presented, 
	sum(Accepted) Accepted, 	
	FORMAT(CAST(sum(Accepted) AS decimal(10,2))/CAST(sum(Presented) AS decimal(10,2)) * 100, 'N2') Closing,
	sum(Declined) Declined,
	(sum(Presented) - sum(Accepted) - sum(Declined)) UnFinished,
	sum([1]) [1],
	sum([2]) [2],
	sum([3]) [3]
into #tempAdvisorData3
from #tempAdvisorData2
group by UserId, AdvisorCode, AdvisorName



update t2
	set  t2.Presented = m.sumPresented,
		t2.Accepted = m.sumAccepted
from #tempAdvisorData3 t2
join (
select UserId, sum(Presented) sumPresented, sum(Accepted) sumAccepted from #tempAdvisorData
group by UserId) m on m.UserId = t2.UserId


update t3
set t3.[1] = L1.Level1
from #tempAdvisorData3 t3
join (select userid, count(*) Level1 from #temp1 where MenuLevel=1 group by UserId)
L1 on L1.UserId = t3.UserId

update t3
set t3.[2] = L2.Level1
from #tempAdvisorData3 t3
join (select userid, count(*) Level1 from #temp1 where MenuLevel=2 group by UserId)
L2 on L2.UserId = t3.UserId

update t3
set t3.[3] = L3.Level1
from #tempAdvisorData3 t3
join (select userid, count(*) Level1 from #temp1 where MenuLevel=3 group by UserId)
L3 on L3.UserId = t3.UserId

select 
	0 as UserId,
	' '  AdvisorCode
	,'Total' AdvisorName 
	,sum(Presented) Presented
	,sum(Accepted) Accepted
	,FORMAT(CAST(sum(Accepted) AS decimal(10,2))/CAST(sum(Presented) AS decimal(10,2)) * 100, 'N2') Closing			
	,sum(Declined) Declined
	,sum(UnFinished) UnFinished
	,sum([1]) [1]
	,sum([2]) [2]
	,sum([3]) [3]
	,2 sort	
into #tempTotal
from #tempAdvisorData3



select userid, AdvisorCode, (AdvisorName COLLATE Latin1_General_CI_AS) AdvisorName, Presented, Accepted, Closing, Declined, UnFinished, [1], [2], [3], 1 sort
from #tempAdvisorData3 t2
union
select userid, AdvisorCode, (AdvisorName COLLATE Latin1_General_CI_AS) AdvisorName, Presented, Accepted, Closing, Declined, UnFinished, [1], [2], [3], 2 sort 
from #tempTotal 
order by sort


END

