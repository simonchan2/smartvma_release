USE [SmartVmaDev]
GO
/****** Object:  StoredProcedure [dbo].[rptMenuSaleTrendDetail]    Script Date: 2016-09-30 4:33:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[rptMenuSaleTrendDetail]
@DistributorId int,
@DealerCompanyId int,
@Advisor	int,
@StartDate  DATETIME,
@EndDate    DATETIME,
@CPOnly				bit=0,
@UserOnly			bit=0,
@ExclLabor0			bit=0
AS 
BEGIN
WITH CTE AS
(
    SELECT @StartDate AS monthYear
    UNION ALL
    SELECT DATEADD(MONTH, 1, monthYear)
    FROM CTE
    WHERE DATEADD(MONTH, 1, monthYear) <= @EndDate   
)
SELECT 
	monthYear DateRange, 
	0 ROs,
	0 MenuPresented,
	0 MenusSold,
	0 MenuSale,
	0 LaborSale,
	0 LaborHour,
	0 Qty,
	0 TotalMenu,
	0 ELR
into #temp
FROM CTE order by monthYear
option (maxrecursion 1000);


create table #tempMenLevel
(    
    MenuLevel int
)
insert into  #tempMenLevel
select 0;
insert into  #tempMenLevel
select 1;
insert into  #tempMenLevel
select 2;
insert into  #tempMenLevel
select 3;
  
select * 
into #tempDateRangeSet
from #temp tt
cross join  #tempMenLevel l

select distinct
		ap.AppointmentPresentationId,
		ap.DealerId,
		u.UserId, 	   
	   (case when ap.PresentationStatus='MC'  then 1 else 0 end) MenuSold,
       ap.MenuLevel,                        
	   month(ap.AppointmentTime) as ApptMonth,
	   Year(ap.AppointmentTime) as ApptYear,
	   i.RoNumber,
	   isnull(p.PartSale,0) PartSale,
	   isnull(d.LaborSale,0) LaborSale,
	   isnull(d.LaborHour,0) LaborHour	 
into #Data
	from [dealer].[AppointmentPresentations] ap (nolock)
	join Users u (nolock) on u.UserId = ap.AdvisorId
	left join [dealer].[Invoices] i (nolock) on i.InvoiceNumber = ap.InvoiceNumber --and i.RoNumber = ap.RoNumber
	left join [dealer].[InvoiceDetails] d (nolock)on d.InvoiceId = i.InvoiceId
	left join [dealer].[DealerDmsUsers] dmsu (nolock) on dmsu.UserId  = ap.[AdvisorId] 
	left join [dealer].[InvoiceDetailParts] p (nolock)on p.InvoiceDetailId = d.InvoiceDetailId
where	[AppointmentTime] between @StartDate and @EndDate and
		((@Advisor=-88 or @Advisor=-99) or (@Advisor!=-88 and @Advisor!=-99 and u.UserId =@Advisor)) and
		(@ExclLabor0 = 0 or (@ExclLabor0 = 1 and d.LaborHour != 0)) and
		(@UserOnly = 0 or (@UserOnly = 1 and dmsu.DmsUserName is not null)) and
		(@CPOnly = 0 or (@CPOnly=1 and d.[LaborType] = 'CP'))		
		and ((@DistributorId = 0 and @DealerCompanyId = 0) or (@DealerCompanyId > 0 and ap.DealerId = @DealerCompanyId) or (@DistributorId > 0 and @DealerCompanyId = 0 and ap.DealerId in (select CompanyId from Companies where parentId = @DistributorId)))

--select * from #Data
--return
update tt1 set tt1.MenuPresented = tt2.MenuPresented, tt1.MenusSold = tt2.MenuSold, tt1.LaborSale = tt2.LaborSale, tt1.LaborHour = tt2.LaborHour, tt1.MenuSale = tt2.LaborSale + tt2.PartSale, tt1.Qty = tt2.Qty from #tempDateRangeSet tt1
	inner join
	(
	select t1.MenuLevel, t1.DateRange, count(*) as MenuPresented, sum(t2.MenuSold) as MenuSold, sum(t2.PartSale) as PartSale, sum(t2.LaborSale) as LaborSale, sum(t2.LaborHour) as LaborHour, sum(t2.MenuSold) as Qty
	from #tempDateRangeSet t1 inner join #Data t2 
	on t1.MenuLevel =  t2.menulevel 
	and	Month(t1.DateRange) = t2.ApptMonth
	and	Year(t1.DateRange) = t2.ApptYear
	group by t1.MenuLevel, t1.DateRange
	) tt2 on tt1.MenuLevel = tt2.MenuLevel and tt1.DateRange = tt2.DateRange 



update tt1 set tt1.ROs = RoCount from #tempDateRangeSet tt1
	inner join
	(
	select t1.MenuLevel, t1.DateRange, count(*) as RoCount 
	from #tempDateRangeSet t1 inner join 
	(select distinct DealerId, MenuLevel, ApptMonth, ApptYear, RONumber from #Data) t2 
	on t1.MenuLevel =  t2.menulevel 
	and	Month(t1.DateRange) = t2.ApptMonth 
	and	Year(t1.DateRange) = t2.ApptYear
	where t2.RoNumber <> '' group by t1.MenuLevel, t1.DateRange 
	) tt2 on  tt1.MenuLevel = tt2.MenuLevel and tt1.DateRange = tt2.DateRange 



--select * from #tempDateRangeSet
	
	


	--select DateRange, sum(ROs) Ros, sum(Pres) Pres, sum(MenusSold) MenusSold, sum(Pen) Pen,
	--sum(Menu$) Menu$, sum(Qty) Qty, sum(Amount) Amount
	--from #tempDateRangeSet 
	--group by DateRange


select * 
into #tempDateRangeSet2
from
( 
	select *, 'a'+ cast([MenuLevel] as varchar) aMenuLevel, 	
	'm'+ cast([MenuLevel] as varchar) mMenuLevel
	from #tempDateRangeSet
) s
pivot
( sum(MenusSold)
		for [MenuLevel] in ([1], [2], [3])
) as p1
pivot
( sum(MenuSale)
		for [aMenuLevel] in ([a1], [a2], [a3])
) as p2
pivot
( sum(Qty)
		for mMenuLevel in ([m1], [m2], [m3])
) as p2

update #tempDateRangeSet2
set [1] = isnull([1],0),[2] = isnull([2],0), [3] = isnull([3],0),
	[a1] = isnull([a1],0),[a2] = isnull([a2],0),[a3] = isnull([a3],0)
,[m1] = isnull([m1],0),[m2] = isnull([m2],0),[m3] = isnull([m3],0)
	
--select * from #tempDateRangeSet2

select *
into #tempDateRangeSet3
from
(
select
LEFT(DATENAME(MONTH,DateRange),3) +  ' ' +  CAST ( YEAR(DateRange)AS varchar(100) )  [RangeLabel], 
sum(ROs) ROs,
sum(MenuPresented) MenuPresented,
sum([1] + [2] + [3]) MenuSold,
sum([a1] + [a2] + [a3]) MenuSale,
sum(LaborHour) LaborHour,
sum(LaborSale) laborSale,
0 Pres,
0 Pen,
0 TotalMenu,
0 ELR,
sum([1]) [1],
sum([2]) [2],
sum([3]) [3],
sum([a1]) [d1],
sum([a2]) [d2],
sum([a3]) [d3],
sum([m1]) [m1],
sum([m2]) [m2],
sum([m3]) [m3],
DateRange
from #tempDateRangeSet2 
group by DateRange
union
select
'Total', 
sum(ROs) ROs,
sum(MenuPresented) MenuPresented,
sum([1] + [2] + [3]) MenuSold,
sum([a1] + [a2] + [a3]) MenuSale,
sum(LaborHour) LaborHour,
sum(LaborSale) LaborSale,
0 Pres,
0 Pen,
0 TotalMenu,
0 ELR,
sum([1]) [1],
sum([2]) [2],
sum([3]) [3],
sum([a1]) [d1],
sum([a2]) [d2],
sum([a3]) [d3],
sum([m1]) [m1],
sum([m2]) [m2],
sum([m3]) [m3],
'9999-01-01' DateRange
from #tempDateRangeSet2
) m

--select * from #tempDateRangeSet3


update #tempDateRangeSet3
set Pres = round(case when Ros = 0 then 0 else COALESCE(MenuPresented / Ros, 0) end,1), 
	Pen= round(case when Ros = 0 then 0 else COALESCE(MenuSold / Ros, 0) end,1),
	TotalMenu = round(case when MenuSold = 0 then 0 else COALESCE(MenuSale / MenuSold,0) end,1),
	ELR = round(case when LaborHour = 0 then 0 else COALESCE(LaborSale / LaborHour, 0) end,1),
	[m1] = round(case when [1] = 0 then 0 else COALESCE([d1]/[1], 0) end,1),
	[m2] = round(case when [2] = 0 then 0 else COALESCE([d2]/[2], 0) end,1),
	[m3] = round(case when [3] = 0 then 0 else COALESCE([d3]/[3], 0) end,1)


--update t3
--set t3.Pres = (select sum(pres) from #tempDateRangeSet3),
--	t3.Pen = (select sum(Pen) from #tempDateRangeSet3)
--from #tempDateRangeSet3 t3
--where RangeLabel='Total'


select * from #tempDateRangeSet3 ORDER BY DateRange

END


