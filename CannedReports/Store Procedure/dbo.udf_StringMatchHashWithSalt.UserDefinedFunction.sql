USE [SmartVmaPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[udf_StringMatchHashWithSalt]    Script Date: 2016-07-11 3:51:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create functions section -------------------------------------------------

/*
Author:		 Simon Chan
Create date: 2016-05-30
Description: Returned 1 if the string matches the hash (with salt)
Usage:
	DECLARE		@Password			nvarchar(255);
	DECLARE		@PasswordHash		varbinary(max);

	SET		@Password = '123456';

	EXECUTE	dbo.usp_GetHashWithSalt @Password, @PasswordHash OUTPUT
	
	--	Should return 1
	SELECT dbo.udf_StringMatchHashWithSalt(@Password, @PasswordHash);

	--	Should return 0
	SELECT dbo.udf_StringMatchHashWithSalt(@Password, @PasswordHash + 0x0200);
*/

CREATE FUNCTION [dbo].[udf_StringMatchHashWithSalt] (@String				nvarchar(255)
, @StringHash		varbinary(MAX))
RETURNS bit
AS
BEGIN
    DECLARE    @StringHashComputed        varbinary(MAX);

--    Get the salt from the hash
    DECLARE @Salt    VARBINARY(4) = SUBSTRING(@StringHash, 3, 4);    

--    Get the hash with salt
    SET @StringHashComputed = 0x0200 + @Salt + HASHBYTES('SHA2_512', CAST(ISNULL(@String, '') AS VARBINARY(MAX)) + @Salt);

    RETURN    CAST((CASE WHEN @StringHash = @StringHashComputed THEN 1 ELSE 0 END) as bit);
END

GO
