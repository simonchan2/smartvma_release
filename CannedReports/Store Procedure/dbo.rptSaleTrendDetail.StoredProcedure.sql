USE [SmartVmaPlus]
GO
/****** Object:  StoredProcedure [dbo].[rptSaleTrendDetail]    Script Date: 2016-08-04 8:28:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[rptSaleTrendDetail]
@Advisor	nvarchar(45),
@StartDate  DATETIME,
@EndDate    DATETIME,
@CPOnly				bit=0,
@UserOnly			bit=0,
@ExclLabor0			bit=0
AS 
BEGIN

-- 1. Create a set of date range with level
WITH CTE AS
(
    SELECT @StartDate AS monthYear
    UNION ALL
    SELECT DATEADD(MONTH, 1, monthYear)
    FROM CTE
    WHERE DATEADD(MONTH, 1, monthYear) <= @EndDate   
)
SELECT 
	monthYear DateRange, 
	0 ROs,
	0 Pres,
	0 MenusSold,
	0 Pen,
	0 Menu$,
	0 Qty,
	0 Amount,
	0 MenuLevel	
into #temp
FROM CTE


create table #tempMenLevel
(    
    MenuLevel int
)
insert into  #tempMenLevel
select 1;
insert into  #tempMenLevel
select 2;
insert into  #tempMenLevel
select 3;
  
select * 
into #tempDateRangeSet
from #temp tt
cross join  #tempMenLevel l

-- End of 1. End of Create a set of date range with level
  
-- 2. Create  data file
--Status of the meny presenrtation; V: Void; C: Closed; M: Menu Presented; MC: Menu Accepted; MD: Menu Declined; MK: Menu Parked
select distinct 
	   i.InvoiceNumber,
       dmsu.[DmsUserNumber] AdvisorCode,
       dmsu.[DmsUserName] AdvisorName, 
       (MenuLevel) [MenuLevel],                 
       [AppointmentTime],
	   dv.vin,
	   p.PartQty,
	   p.PartSale	       
into #Data
from [dealer].[DealerDmsUsers] dmsu
join [dealer].[AppointmentPresentations] ap on dmsu.UserId  = ap.[AdvisorId] 
join [dealer].[DealerVehicles] dv on dv.[DealerVehicleId] = ap.DealerVehicleId
left join dealer.Invoices i on i.InvoiceNumber = ap.InvoiceNumber
left join [dealer].[InvoiceDetails] d (nolock)on d.InvoiceId = i.InvoiceId
left join [dealer].[InvoiceDetailParts] p (nolock)on p.InvoiceDetailId = d.InvoiceDetailId
where	[AppointmentTime] between @StartDate and @EndDate and
		((@Advisor='Consolidated' or @Advisor='AllAdvisors') or (@Advisor!='Consolidated' and @Advisor!='AllAdvisors' and dmsu.DmsUserNumber =@Advisor)) and
		(@ExclLabor0 = 0 or (@ExclLabor0 = 1 and d.LaborHour != 0)) and
		(@UserOnly = 0 or (@UserOnly = 1 and dmsu.DmsUserName is not null)) and
		(@CPOnly = 0 or (@CPOnly=1 and d.[LaborType] = 'CP'))

--select * from #Data order by AdvisorCode
 
  --select * from #tempDateRangeSet order by DateRange

	declare @menulevel int
	declare @presented int
	declare @accepted int
	declare @apptdate datetime
	declare @AdvisorCode nvarchar(45)
	declare @AdvisorName nvarchar(45)

	declare CustList cursor for
	select * from #Data
	OPEN CustList
	FETCH NEXT FROM CustList 
	INTO @AdvisorCode, @AdvisorName, @presented, @accepted, @menulevel, @apptdate
	
	WHILE @@FETCH_STATUS = 0
	BEGIN		
		  begin
		  
	  		update t2
				set t2.Preseneted = (t2.Preseneted + @presented),
				t2.Accepted = (t2.Accepted + @accepted)
			from #tempDateRangeSet t2
			where
				t2.MenuLevel =  @menulevel and
				Month(t2.DateRange) = Month(@apptdate) and
				Year(t2.DateRange) = Year(@apptdate) 

		
		  end
	  
	  FETCH NEXT FROM CustList 
	    INTO @AdvisorCode, @AdvisorName, @presented, @accepted,@menulevel, @apptdate
	END
	CLOSE CustList
	DEALLOCATE CustList


begin
	select 	
		t1.DateRange,
		t1.Preseneted, 
		t1.Accepted,
		t1.Accepted result, 
		t1.MenuLevel,
		CASE WHEN @Advisor='Consolidated' THEN 'AllAdvisors' 
			ELSE @Advisor 
		END as AdvisorName
	into #tempDateRangeSet2
	from #tempDateRangeSet t1
	order by DateRange
	
	--select * from #tempDateRangeSet2 order by DateRange
	--return
	select *
	into #tempDateRangeSet3
	from #tempDateRangeSet2
	pivot (sum(result) for MenuLevel in ([1],[2],[3])) as [MenuLevel1]
	order by DateRange


	update #tempDateRangeSet3
	set [1]=ISNULL([1] ,0), [2]=ISNULL([2] ,0),[3]=ISNULL([3] ,0)

	select DateRange, sum(Preseneted) Preseneted, sum(Accepted) Accepted, AdvisorName, sum([1]) [1], sum([2]) [2], sum([3]) [3]
	into #tempDateRangeSet4
	 from #tempDateRangeSet3 
	 group by DateRange, AdvisorName
	 order by DateRange
	
	
	update t3 
		set Preseneted = sumPreseneted,
			Accepted = sumAccepted
	from #tempDateRangeSet4 t3
	join 	
	(select DateRange, sum(Preseneted) sumPreseneted, sum(Accepted) sumAccepted
	from #tempDateRangeSet2 t2 
	group by DateRange) ss on ss.DateRange = t3.DateRange

	--select * from #tempDateRangeSet3

	select 			
		   LEFT(DATENAME(MONTH,DateRange),3) +  ' ' +  CAST ( YEAR(DateRange)AS varchar(100) )  [RangeLabel],
		   AdvisorName,
		   (Preseneted) Preseneted,
		   (Accepted) Accepted,
		   ([1]) [1],
		   ([2]) [2],
		   ([3]) [3]
		   from #tempDateRangeSet4			
			
end
end




GO
