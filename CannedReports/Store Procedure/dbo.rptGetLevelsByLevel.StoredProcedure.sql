USE [SmartVmaPlus]
GO
/****** Object:  StoredProcedure [dbo].[rptGetLevelsByLevel]    Script Date: 2016-08-04 8:28:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[rptGetLevelsByLevel]
	@Level int = 0
AS 
BEGIN

if @Level = 0
begin
	SELECT 'Level 1' levelLabel, 1 levelValue
	UNION
	SELECT 'Level 2' levelLabel, 2 levelValue
	UNION
	SELECT 'Level 3'	levelLabel, 3 levelValue
end
else
begin
	SELECT 'Level ' +CAST(@Level AS varchar) levelLabel, @Level levelValue
end

end




GO
