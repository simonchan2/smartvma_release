﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMAPlus.AllData.WebScrape.Models
{
    public class RequestInfo
    {
        
        public string BaseUrl;
        public string LoginPath;
        public string LogoutPath;
        public string UserId;
        public string Password;
        public Nullable<int> CarExtId;
        public RequestType RequestType;
        public int YearStart;
        public int MenuComponentId;
        public int SevereSvcItypeId;
        public int NormalSvcItypeId;
        public string OperationScope;
        public int LaborItypeId;
        public int PartsItypeId;
        public int FluidCapacityItypeId;
        public int FluidTypeItypeId;
    }

    public enum RequestType
    {
        Year,
        Make,
        Model,
        Engine,
        Car,
        Menu,
        Service,
        Parts,
        Labor,
        Fluid,
        Component,
        CarVin,
    }

    public class Part
    {
        public string PartName;
        public List<PartNumber> PartNumberList; 
    }

    public class PartNumber
    {
        public int CountryId;
        public string PartNo;

    }
}
