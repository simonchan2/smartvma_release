﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using HtmlAgilityPack;

namespace SmartVMAPlus.AllData.WebScrape.Parse
{
    public class ParseEngine : IParse 
    {
        public void ParseHtml(string pageContent, string param, ref DataTable dt)
        {

            if (pageContent != null)
            {
                HtmlDocument htmlDoc = new HtmlDocument();

                htmlDoc.LoadHtml(pageContent);

                List<HtmlNode> engineList = htmlDoc.DocumentNode.Descendants().Where
                  (x => (x.Name == "a" && x.Attributes["class"] != null &&
                  x.Attributes["class"].Value.Contains("vehicle"))).ToList();

                if (engineList != null && engineList.Count > 0)
                {

                    int year = int.Parse(param.Split('|')[0]);
                    int makeExtId = int.Parse(param.Split('|')[1]);
                    int modelExtId = int.Parse(param.Split('|')[2]);
                    int engineExtId = -1;
                    string engine = "";
                    Nullable<Byte> numberOfCylinders = null;
                    string href = "";
                    DataRow dr;

                    foreach (HtmlNode htmlNode in engineList)
                    {
                        href = htmlNode.Attributes["href"].Value;
                        engineExtId = int.Parse(href.Split('/')[9]);
                        engine = System.Net.WebUtility.HtmlDecode(htmlNode.InnerText);
                        numberOfCylinders = RegularExpression.GetNumberOfCylinders(engine);

                        dr = dt.NewRow();
                        dr["MakeExtId"] = makeExtId;
                        dr["year"] = year;
                        dr["ModelExtId"] = modelExtId;
                        dr["EngineExtId"] = engineExtId;
                        dr["Engine"] = engine;
                        if (numberOfCylinders == null)
                            dr["NumberOfCylinders"] = DBNull.Value;
                        else
                            dr["NumberOfCylinders"] = numberOfCylinders;

                        dt.Rows.Add(dr);
                    }
                }


            }

        }

    }
}
