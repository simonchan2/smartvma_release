﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using HtmlAgilityPack;

namespace SmartVMAPlus.AllData.WebScrape.Parse
{
    public class ParseCar : IParse 
    {
        public void ParseHtml(string pageContent, string param, ref DataTable dt)
        {

            if (pageContent != null)
            {
                HtmlDocument htmlDoc = new HtmlDocument();

                htmlDoc.LoadHtml(pageContent);

                List<HtmlNode> carList = htmlDoc.DocumentNode.Descendants().Where
                  (x => (x.Name == "a" && x.Attributes["class"] != null &&
                  x.Attributes["class"].Value.Contains("vehicle"))).ToList();

                if (carList != null && carList.Count > 0)
                {
                    if (dt == null)
                    {
                        dt = new DataTable();

                        dt.Columns.Add("Year", typeof(Int32));
                        dt.Columns.Add("MakeExtId", typeof(Int32));
                        dt.Columns.Add("ModelExtId", typeof(Int32));
                        dt.Columns.Add("EngineExtId", typeof(Int32));
                        dt.Columns.Add("CarExtId", typeof(Int32));
                        dt.Columns.Add("Description", typeof(string));
                    }

                    int year = int.Parse(param.Split('|')[0]);
                    int makeExtId = int.Parse(param.Split('|')[1]);
                    int modelExtId = int.Parse(param.Split('|')[2]);
                    int engineExtId = int.Parse(param.Split('|')[3]);
                    int carExtId = -1;
                    string description = "";
                    string href = "";
                    DataRow dr;

                    foreach (HtmlNode htmlNode in carList)
                    {
                        href = htmlNode.Attributes["href"].Value;
                        carExtId = int.Parse(href.Split('/')[3]);
                        description = System.Net.WebUtility.HtmlDecode(htmlNode.InnerText.Replace("\n",""));

                        dr = dt.NewRow();
                        dr["MakeExtId"] = makeExtId;
                        dr["year"] = year;
                        dr["ModelExtId"] = modelExtId;
                        dr["EngineExtId"] = engineExtId;
                        dr["CarExtId"] = carExtId;
                        dr["Description"] = description;
                        dt.Rows.Add(dr);
                    }
                }


            }

        }
    }
}
