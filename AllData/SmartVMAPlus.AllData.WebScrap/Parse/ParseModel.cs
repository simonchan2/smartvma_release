﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using HtmlAgilityPack;

namespace SmartVMAPlus.AllData.WebScrape.Parse
{
    public class ParseModel : IParse
    {
        public void ParseHtml(string pageContent, string param, ref DataTable dt)
        {

            if (pageContent != null)
            {
                HtmlDocument htmlDoc = new HtmlDocument();

                htmlDoc.LoadHtml(pageContent);

                List<HtmlNode> modelList = htmlDoc.DocumentNode.Descendants().Where
                  (x => (x.Name == "a" && x.Attributes["class"] != null &&
                  x.Attributes["class"].Value.Contains("engines_list"))).ToList();

                if (modelList != null && modelList.Count > 0)
                {
                    if (dt == null)
                    {
                        dt = new DataTable();

                        dt.Columns.Add("Year", typeof(Int32));
                        dt.Columns.Add("MakeExtId", typeof(Int32));
                        dt.Columns.Add("ModelExtId", typeof(Int32));
                        dt.Columns.Add("Model", typeof(string));
                    }

                    int year = int.Parse(param.Split('|')[0]);
                    int makeExtId = int.Parse(param.Split('|')[1]);
                    int modelExtId = -1;
                    string model = "";
                    string href = "";
                    DataRow dr;

                    foreach (HtmlNode htmlNode in modelList)
                    {
                        href = htmlNode.Attributes["href"].Value;
                        modelExtId = int.Parse(href.Split('/')[7]);
                        model = System.Net.WebUtility.HtmlDecode(htmlNode.InnerText);

                        dr = dt.NewRow();
                        dr["MakeExtId"] = makeExtId;
                        dr["year"] = year;
                        dr["ModelExtId"] = modelExtId;
                        dr["Model"] = model;
                        dt.Rows.Add(dr);
                    }
                }


            }

        }
    }
}
