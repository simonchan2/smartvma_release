﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using HtmlAgilityPack;

namespace SmartVMAPlus.AllData.WebScrape.Parse
{
    public class ParseMenu : IParse 
    {
        public void ParseHtml(string pageContent, string param, ref DataTable dt)
        {

            if (pageContent != null)
            {
                HtmlDocument htmlDoc = new HtmlDocument();

                htmlDoc.LoadHtml(pageContent);

                List<HtmlNode> menuList = htmlDoc.DocumentNode.Descendants().Where
                  (x => (x.Name == "a" && x.Attributes["class"] != null &&
                  x.Attributes["class"].Value.Contains("nonstandard"))).ToList();

                if (menuList != null && menuList.Count > 0)
                {
                    

                    string href = "";
                    string[] dataValues;
                    string[] dataValues2;

                    DataRow dr;

                    foreach (HtmlNode htmlNode in menuList)
                    {
                        href = htmlNode.Attributes["href"].Value;
                        dataValues = href.Split('/');
                        dataValues2 = htmlNode.InnerText.Split(' ');

                        dr = dt.NewRow();
                        dr["CarExtId"] = int.Parse(dataValues[3]);
                        dr["Interval"] = int.Parse(dataValues2[0]);
                        dr["Mileage"] = htmlNode.InnerText;
                        dr["Miles"] = int.Parse(dataValues2[0]);
                        dr["Kilometers"] = int.Parse(dataValues2[3]);
                        dr["OemComponentId"] = int.Parse(dataValues[5]);
                        dr["iType"] = int.Parse(dataValues[7]);
                        dr["IntervalUrl"] = "/" + dataValues[8] + "/" + dataValues[9];
                        dt.Rows.Add(dr);
                    }
                }


            }

        }

    }
}
