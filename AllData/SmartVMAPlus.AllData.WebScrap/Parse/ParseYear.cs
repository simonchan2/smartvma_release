﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using HtmlAgilityPack;



namespace SmartVMAPlus.AllData.WebScrape.Parse
{
    public class ParseYear : IParse
    {
        public void ParseHtml(string pageContent, string param, ref DataTable dt)
        {

            if (pageContent != null)
            {
                HtmlDocument htmlDoc = new HtmlDocument();

                htmlDoc.LoadHtml(pageContent);

                List<HtmlNode> yearList = htmlDoc.DocumentNode.Descendants().Where
                  (x => (x.Name == "a" && x.Attributes["class"] != null &&
                  x.Attributes["class"].Value.Contains("makes_list"))).ToList();

                if (yearList != null && yearList.Count > 0)
                {
                    if (dt == null)
                    {
                        dt = new DataTable();

                        dt.Columns.Add("Year", typeof(Int32));
                    }

                    int year = 0;

                    foreach (HtmlNode htmlNode in yearList)
                    {
                        year = int.Parse(htmlNode.InnerText);
                        if (year >= int.Parse(param))
                            dt.Rows.Add(year);
                    }
                }
    

            }

        }
    }
}
