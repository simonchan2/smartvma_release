﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace SmartVMAPlus.AllData.WebScrape.Parse
{
    public static class RegularExpression
    {
        public static MatchCollection ParseString(string pattern, string input)
        {
            MatchCollection m = Regex.Matches(input, pattern, RegexOptions.IgnoreCase);
            return m;
        }


        public static Nullable<Byte> GetNumberOfCylinders(string engine)
        {
            Nullable<Byte> noOfCylinders = null;

            if (engine.Contains("2RTR"))
            {
                noOfCylinders = 4;
            }
            else
            {
                string regPattern = @"([VLFW])\d{1,2}[ ]?[-]";

                MatchCollection matches = RegularExpression.ParseString(regPattern, engine);
                if (matches != null && matches.Count > 0)
                    try
                    {
                        noOfCylinders = Byte.Parse(matches[0].Value.Replace("V", "").Replace("L", "").Replace("F", "").Replace("W", "").Replace("-", ""));
                    }
                    catch (Exception ex)
                    {
                        string errorStr = ex.Message;
                    }


            }

            return noOfCylinders;

        }

        public static Nullable<float> ParseFluidCapacity(string capacityText)
        {

            string regPattern = @"\d+(\.\d{1,2})";// @"[\d{1,2}][\.]?[\d{1,2}]?";

            string regPattern2 = @"\d+(\.\d{1,2})[ ]?(qt|quart|Quart)";//@"[\d{1,2}][\.][\d{1,2}]?[ ]?(qt)";

            MatchCollection matches = RegularExpression.ParseString(regPattern2, capacityText);
            if (matches != null && matches.Count > 0)
            {
                matches = RegularExpression.ParseString(regPattern, matches[0].Value);
                if (matches != null && matches.Count > 0)
                    return float.Parse(matches[0].Value);
            }


            regPattern2 = @"\d+(\.\d{1,2})[ ]?(pt|pint|Pint)";

            matches = RegularExpression.ParseString(regPattern2, capacityText);
            if (matches != null && matches.Count > 0)
            {
                matches = RegularExpression.ParseString(regPattern, matches[0].Value);
                if (matches != null && matches.Count > 0)
                    return float.Parse(matches[0].Value) / 2;
            }

            regPattern2 = @"\d+(\.\d{1,2})[ ]?(L|liter|Liter)";

            matches = RegularExpression.ParseString(regPattern2, capacityText);
            if (matches != null && matches.Count > 0)
            {
                matches = RegularExpression.ParseString(regPattern, matches[0].Value);
                if (matches != null && matches.Count > 0)
                    return (float.Parse(matches[0].Value)) * (float)1.05669;
            }

            regPattern2 = @"\d+(\.\d{1,2})[ ]?(gal|GAL|Gallons)";

            matches = RegularExpression.ParseString(regPattern2, capacityText);
            if (matches != null && matches.Count > 0)
            {
                matches = RegularExpression.ParseString(regPattern, matches[0].Value);
                if (matches != null && matches.Count > 0)
                    return (float.Parse(matches[0].Value)) * (float)4;
            }

            return null;
        }

        public static string ParseFluidType(string fluidTypeText, List<string> viscosityList)
        {
            string fluidType = "";
            string regPattern = "";

            MatchCollection matches = null;

            if (viscosityList != null && viscosityList.Count > 0)
            {
                fluidType = fluidTypeText;

                foreach (string viscosity in viscosityList)
                {
                    fluidType = fluidType.Replace(viscosity, "");
                }
            
                fluidType = fluidType.Replace("SAE","").Replace("preferred", "").Replace("(","").Replace(")","").Replace(", ", "").Replace(" ", "");

                if (fluidType == "" || fluidType.ToUpper() == "OR" || fluidType.ToUpper() == "OIL" || fluidType.ToUpper() == "ENGINEOILISRECOMMENDED" || fluidType.ToUpper() == "ENGINEOIL" || fluidType.ToUpper() == "SYNTHETICOIL" || fluidType == "*" || fluidType.ToUpper() == "MOTOROIL" || fluidType.ToUpper() == "GEAROIL" || fluidType == "AllTempRange" || fluidType.ToUpper() == "AND" || fluidType == "orCheckOilCapforproperusage")
                    return null;

                regPattern = @"if (over)?(above)? \d{1,2}° F";
                matches = ParseString(regPattern, fluidType);
                if (matches != null && matches.Count > 0)
                    return null;

                regPattern = @"SAE  \[Above \d{1,2}°F\]";
                matches = ParseString(regPattern, fluidType);
                if (matches != null && matches.Count > 0)
                    return null; 

            }
   

            regPattern = @"\w.*";
            matches = ParseString(regPattern, fluidTypeText);

            if (matches != null && matches.Count > 0)
                fluidType = matches[0].Value;

            return fluidType;
        }

        public static void ParseViscosity(string viscosityText, ref List<string> viscosityList)
        {
            string regPattern = "";

            regPattern = @"\d{1,3}W(-)?(/)?\d{1,3}[ ]?(\(preferred\))";
            MatchCollection matches = ParseString(regPattern, viscosityText);

            if (matches != null && matches.Count > 0)
            {
                foreach (Match match in matches)
                {
                    viscosityList.Add(match.Value);
                }

            }


            regPattern = @"\d{1,3}W(-)?(/)?\d{1,3}[ ]?(\(recommended\))";
            matches = ParseString(regPattern, viscosityText);

            if (matches != null && matches.Count > 0)
            {
                foreach (Match match in matches)
                {
                    viscosityList.Add(match.Value);
                }

            }

            regPattern = @"\d{1,3}W(-)?(/)?\d{1,3}";
            matches = ParseString(regPattern, viscosityText);

            if (matches != null && matches.Count > 0)
            {
                foreach (Match match in matches)
                {
                    viscosityList.Add(match.Value);
                }

            }

        }
        public static string ParseHttpErrorCode(string exceptionMessage)
        {
            string regPattern = @"\d{3}";
            MatchCollection matches = ParseString(regPattern, exceptionMessage);

            if (matches != null && matches.Count > 0)
            {
                return matches[0].Value;
            }

            return "Not Available";
        }
    }
}
