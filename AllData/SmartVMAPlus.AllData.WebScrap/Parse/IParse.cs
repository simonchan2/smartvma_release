﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace SmartVMAPlus.AllData.WebScrape.Parse
{
    interface IParse
    {
        void ParseHtml(string pageContent, string param, ref DataTable dt);
    }
}
