﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using HtmlAgilityPack;
using SmartVMAPlus.AllData.WebScrape.Models;
using System.Net;



namespace SmartVMAPlus.AllData.WebScrape.Parse
{
    public class ParseFluid : IParse
    {
        public void ParseHtml(string pageContent, string param, ref DataTable dt)
        {

            if (pageContent != null)
            {
                HtmlDocument htmlDoc = new HtmlDocument();

                htmlDoc.LoadHtml(pageContent);

                List<HtmlNode> articleList = htmlDoc.DocumentNode.Descendants().Where
                  (x => (x.Name == "div" && x.Attributes["class"] != null &&
                  x.Attributes["class"].Value.Contains("article_body"))).ToList();

                string[] parameters = param.Split('|');
                int carExtId = int.Parse(parameters[0]);
                int oemComponentId = int.Parse(parameters[1]);
                int iType = int.Parse(parameters[2]);
                DataRow dr;


                if (articleList != null && articleList.Count == 1)
                {
                    string articleAll = "";
                    if (articleList[0].InnerHtml.Contains("<!--"))
                    {
                        articleAll = articleList[0].InnerHtml.Remove(articleList[0].InnerHtml.IndexOf("<!--"), articleList[0].InnerHtml.IndexOf("-->") - articleList[0].InnerHtml.IndexOf("<!--") + 3);
                        while (articleAll.Contains("<!--"))
                            articleAll = articleAll.Remove(articleAll.IndexOf("<!--"), articleAll.IndexOf("-->") - articleAll.IndexOf("<!--") + 3);
                    }
                    else
                        articleAll = articleList[0].InnerHtml;
                    //article = articleList[0].InnerHtml.Remove(articleList[0].InnerHtml.IndexOf("<!--"),articleList[0].InnerHtml.IndexOf("-->"));
                    string[] articles = System.Net.WebUtility.HtmlDecode(articleAll.Replace("<br>", "|")).Split('|');


                    if (articles != null && articles.Length > 0)
                    {
                        if (iType == 30) //Capacity
                        {
                            Nullable<float> capacity;
                            foreach (string article in articles)
                            {
                                if (article.Trim() != "")
                                {
                                    capacity = RegularExpression.ParseFluidCapacity(article);
                                    if (capacity != null)
                                    {
                                        dr = dt.NewRow();
                                        dr["CarExtId"] = carExtId;
                                        dr["OemComponentId"] = oemComponentId;
                                        dr["iType"] = iType;
                                        dr["Capacity"] = (Decimal)capacity;
                                        dr["Unit"] = "Quart";
                                        //dr["ExceptionMessage"] = articleAll;

                                        dt.Rows.Add(dr);

                                        return;
                                    }
                                }
                            }
                        }

                        if (iType == 31) //FluidType / Viscosity /PartNo
                        {
                            string fluidType = null;
                            string viscosity = null;
                            string partName = null;
                            List<string> fluidTypeList = null;
                            List<string> viscosityList = null;
                            List<Part> partList = null;
                            List<string> fluidTypeList2 = null;

                            try
                            {

                                ParseByComponentId(oemComponentId, articleList[0], articles, out fluidTypeList, out viscosityList, out partList);

                                if (viscosityList != null && viscosityList.Count > 0)
                                    viscosity = GetViscosityFromList(viscosityList);

                                if (fluidTypeList != null && fluidTypeList.Count > 0)
                                {
                                    fluidTypeList2 = new List<string>();

                                    foreach (string fluidType2 in fluidTypeList)
                                    {
                                        fluidType = RegularExpression.ParseFluidType(fluidType2, viscosityList);
                                        if (fluidType != null)
                                            fluidTypeList2.Add(fluidType);
                                    }

                                    fluidType = fluidTypeList2[0];
                                }

                                if (((fluidTypeList2 == null || fluidTypeList2.Count == 0) && (viscosityList == null || viscosityList.Count == 0) && (partList == null || partList.Count == 0)) || (fluidTypeList2 != null && fluidTypeList2.Count > 1) || (partList != null && partList.Count > 1))
                                {
                                    dr = dt.NewRow();
                                    dr["CarExtId"] = carExtId;
                                    dr["OemComponentId"] = oemComponentId;
                                    dr["iType"] = iType;
                                    if (fluidType != null)
                                        dr["FluidType"] = fluidType;
                                    if (viscosity != null)
                                        dr["Viscosity"] = viscosity;
                                    dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("Fluid", articleAll);

                                    dt.Rows.Add(dr);

                                    return;
                                }
                                else
                                {

                                    if (partList != null && partList.Count > 0)
                                    {
                                        if (partList.Count == 1)
                                        {
                                            partName = partList[0].PartName;
                                            if (partList[0].PartNumberList != null && (partList[0].PartNumberList.Count > 2 || (partList[0].PartNumberList.Count == 2 && partList[0].PartNumberList[0].CountryId == partList[0].PartNumberList[1].CountryId)))
                                            {
                                                dr = dt.NewRow();

                                                dr["CarExtId"] = carExtId;
                                                dr["OemComponentId"] = oemComponentId;
                                                dr["iType"] = iType;
                                                dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("FluidType", articleAll);

                                                dt.Rows.Add(dr);

                                                return;
                                            }
                                            else if (partList[0].PartNumberList != null && (partList[0].PartNumberList.Count == 1 || (partList[0].PartNumberList.Count == 2 && partList[0].PartNumberList[0].CountryId != partList[0].PartNumberList[1].CountryId)))
                                            {
                                                foreach (PartNumber partNumber in partList[0].PartNumberList)
                                                {
                                                    dr = dt.NewRow();

                                                    dr["CarExtId"] = carExtId;
                                                    dr["OemComponentId"] = oemComponentId;
                                                    dr["iType"] = iType;
                                                    if (fluidType != null)
                                                        dr["FluidType"] = fluidType;
                                                    if (viscosity != null)
                                                        dr["Viscosity"] = viscosity;
                                                    if (partNumber.PartNo != null)
                                                        dr["OemPartNo"] = partNumber.PartNo;
                                                    if (partName != null)
                                                        dr["OemPartName"] = partName;
                                                    dr["CountryId"] = partNumber.CountryId;

                                                    if (fluidType == null || viscosity == null)
                                                        dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("FluidType", articleAll);

                                                    dt.Rows.Add(dr);

                                                }

                                                return;
                                            }
                                        }
                                        else
                                        {
                                            dr = dt.NewRow();

                                            dr["CarExtId"] = carExtId;
                                            dr["OemComponentId"] = oemComponentId;
                                            dr["iType"] = iType;
                                            dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("FluidType", articleAll);

                                            dt.Rows.Add(dr);

                                            return;
                                        }
                                    }

                                    dr = dt.NewRow();

                                    dr["CarExtId"] = carExtId;
                                    dr["OemComponentId"] = oemComponentId;
                                    dr["iType"] = iType;
                                    if (fluidType != null)
                                        dr["FluidType"] = fluidType;
                                    if (viscosity != null)
                                        dr["Viscosity"] = viscosity;

                                    if (fluidType == null || viscosity == null)
                                        dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("FluidType", articleAll);

                                    dt.Rows.Add(dr);

                                    return;
                                }
                            }
                            catch (Exception ex)
                            {
                                dr = dt.NewRow();
                                dr["CarExtId"] = carExtId;
                                dr["OemComponentId"] = oemComponentId;
                                dr["iType"] = iType;
                                dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("FluidType", articleAll + "<div>" + ex.Message + "</div>");

                                dt.Rows.Add(dr);
                                return;
                            }




                        }
                    }

                    dr = dt.NewRow();
                    dr["CarExtId"] = carExtId;
                    dr["OemComponentId"] = oemComponentId;
                    dr["iType"] = iType;
                    dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("Fluid", articleAll);

                    dt.Rows.Add(dr);
                }
                else
                {
                    List<HtmlNode> contentList = htmlDoc.DocumentNode.Descendants().Where
                      (x => (x.Name == "ul" && x.Attributes["class"] != null &&
                      x.Attributes["class"].Value.Contains("article"))).ToList();

                    dr = dt.NewRow();
                    dr["CarExtId"] = carExtId;
                    dr["OemComponentId"] = oemComponentId;
                    dr["iType"] = iType;
                    if (contentList != null && contentList.Count > 0)
                        dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("Fluid", contentList[0].OuterHtml);
                    else
                        dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("Fluid", "No Message");
                    dt.Rows.Add(dr);
                }




            }


        }




        private void ParseByComponentId(int oemComponentId, HtmlNode articleBody, string[] articles, out List<string> fluidTypeList, out List<string> viscosityList, out List<Part> partList)
        {
            int lineCount = 0;
            string fluidType = null;
            string partName = null;
           
            fluidTypeList = null;
            viscosityList = new List<string>();
            partList = null;
            Part part = null;
            PartNumber partNumber;
            List<PartNumber> partNumberList = null;
            

            switch (oemComponentId)
            {
                case 800: //Engine Oil
                    foreach (string article in articles)
                    {
                        if (article.Trim() != "")
                        {
                            lineCount++;

                            fluidType = null;
                            partNumber = null;
                           
                            //FluidType
                            if (article.ToUpper().Contains("ENGINE</A> OIL ...") || article.ToUpper().Contains("ENGINE</A> OIL* ...") || article.ToUpper().Contains("TYPE ...") || article.Contains("Grade* ...") || article.Contains("Type of Material ...") || article.Contains("BMW High Performance Synthetic Oil ..") || article.Contains("Fluid Specification ...") || article.Contains("Oil Specification ...") || article.Contains("API Rating ...") || article.Contains("API Certified ...") || article.Contains("Mini Synthetic Oil SAE ...") || article.Contains("Porsche recommends ...") || article.Contains("Classification ...") || article.Contains("Ford Specification ..."))
                                fluidType = article.Remove(0, article.LastIndexOf("...")).Replace(".", "").Trim();
                            else if (article.ToUpper().Contains("GRADE ...") || article.ToUpper().Contains("API CLASSIFICATION"))
                            {
                                if (article.Contains("..."))
                                    fluidType = article.Remove(0, article.LastIndexOf("...")).Replace(".", "").Trim();
                                else
                                    fluidType = article.Replace("Grade", "").Replace("API Classification", "").Replace("\"", "");
                            }
                            if (fluidType != null)
                            {
                                while (fluidType.Contains("<"))
                                    fluidType = fluidType.Remove(fluidType.IndexOf("<"), fluidType.IndexOf(">") - fluidType.IndexOf("<") + 1);

                                if (fluidTypeList == null)
                                    fluidTypeList = new List<string>();

                                fluidTypeList.Add(fluidType);
                            }

                            //Viscosity
                            RegularExpression.ParseViscosity(article, ref viscosityList);

                            //Part
                            if (article.ToUpper().Contains("PART NAME ..."))
                            {
                                partName = article.Remove(0, article.LastIndexOf("...")).Replace(".", "").Trim();

                                if (partName != null)
                                {
                                    while (partName.Contains("<"))
                                        partName = partName.Remove(partName.IndexOf("<"), partName.IndexOf(">") - partName.IndexOf("<") + 1);
                                }

                                if (partList == null)
                                    partList = new List<Part>();

                                if (part != null)
                                    partList.Add(part);

                                part = new Part();

                                part.PartName = partName;

                                partNumberList = null;
                            }

                            if ((article.ToUpper().Contains("PART NUMBER ...") || article.ToUpper().Contains("PART NO. ...")) || article.Contains("Part Number/Specification ..."))
                            {
                                if (partList == null)
                                    partList = new List<Part>();

                                if (part == null)
                                    part = new Part();

                                if (partNumberList == null)
                                    partNumberList = new List<PartNumber>();

                                partNumber = new PartNumber();
                                if (article.ToUpper().Contains("CANADA"))
                                    partNumber.CountryId = 1;
                                else
                                    partNumber.CountryId = 0;

                                partNumber.PartNo = article.Remove(0, article.LastIndexOf("...")).Replace(".", "").Trim();
                                if (partNumber.PartNo.Split(',').Length > 1)
                                    partNumber.PartNo = partNumber.PartNo.Split(',')[1].ToUpper().Replace("BMW", "").Replace("PART NUMBER", "").Trim();


                                while (partNumber.PartNo.Contains("<"))
                                    partNumber.PartNo = partNumber.PartNo.Remove(partNumber.PartNo.IndexOf("<"), partNumber.PartNo.IndexOf(">") - partNumber.PartNo.IndexOf("<") + 1);

                                partNumberList.Add(partNumber);

                                part.PartNumberList = partNumberList;
                            }

                        }
                    }

                    if (part != null)
                        partList.Add(part);

                    break;

                case 926: //Transmision Fluid

                    foreach (string article in articles)
                    {
                        if (article.Trim() != "")
                        {
                            lineCount++;
                            fluidType = null;
                            partNumber = null;

                            if (article.Contains("Transmission Fluid ...") && article.Contains("BMW"))
                            {
                                fluidType = article.Remove(0, article.LastIndexOf("...")).Replace(".", "").Trim().Split(',')[0];
                            }
                            else if (article.ToUpper().Contains("TYPE ...") || article.ToUpper().Contains("TYPE  ...") || article.ToUpper().Contains("FLUID ...") || article.ToUpper().Contains("TYPE: ...") || article.ToUpper().Contains("FLUID  ...") || article.ToUpper().Contains("AW5 ...") || article.ToUpper().Contains("AW4 ...") || article.Contains("Automatic Transmission ...") || article.Contains("ATF (Automatic Transmission Fluid) ...") || article.Contains("Ford Specification ..."))
                            {
                                fluidType = article.Remove(0, article.LastIndexOf("...")).Replace(".", "").Trim();
                            }
                            if (fluidType != null)
                            {
                                while (fluidType.Contains("<"))
                                    fluidType = fluidType.Remove(fluidType.IndexOf("<"), fluidType.IndexOf(">") - fluidType.IndexOf("<") + 1);

                                if (fluidTypeList == null)
                                    fluidTypeList = new List<string>();

                                fluidTypeList.Add(fluidType);
                            }

                            //Viscosity
                            RegularExpression.ParseViscosity(article, ref viscosityList);

                            //Part
                            if (article.ToUpper().Contains("PART NAME ..."))
                            {
                                partName = article.Remove(0, article.LastIndexOf("...")).Replace(".", "").Trim();

                                if (partName != null)
                                {
                                    while (partName.Contains("<"))
                                        partName = partName.Remove(partName.IndexOf("<"), partName.IndexOf(">") - partName.IndexOf("<") + 1);
                                }

                                if (partList == null)
                                    partList = new List<Part>();

                                if (part != null)
                                    partList.Add(part);

                                part = new Part();

                                part.PartName = partName;

                                partNumberList = null;
                            }

                            if ((article.ToUpper().Contains("PART NUMBER ...") || article.ToUpper().Contains("PART NO. ...")) || article.Contains("Part Number/Specification ..."))
                            {
                                if (partList == null)
                                    partList = new List<Part>();

                                if (part == null)
                                    part = new Part();

                                if (partNumberList == null)
                                    partNumberList = new List<PartNumber>();

                                partNumber = new PartNumber();
                                if (article.ToUpper().Contains("CANADA"))
                                    partNumber.CountryId = 1;
                                else
                                    partNumber.CountryId = 0;

                                partNumber.PartNo = article.Remove(0, article.LastIndexOf("...")).Replace(".", "").Trim();
                                if (partNumber.PartNo.Split(',').Length > 1)
                                    partNumber.PartNo = partNumber.PartNo.Split(',')[1].ToUpper().Replace("BMW", "").Replace("PART NUMBER", "").Trim();


                                while (partNumber.PartNo.Contains("<"))
                                    partNumber.PartNo = partNumber.PartNo.Remove(partNumber.PartNo.IndexOf("<"), partNumber.PartNo.IndexOf(">") - partNumber.PartNo.IndexOf("<") + 1);

                                partNumberList.Add(partNumber);

                                part.PartNumberList = partNumberList;
                            }

                            

                        }
                    }

                    if (part != null)
                        partList.Add(part);

                    break;

                case 807: //Coolant
                    foreach (string article in articles)
                    {
                        if (article.Trim() != "")
                        {
                            lineCount++;
                            fluidType = null;
                            partNumber = null;

                            if (article.Contains("..."))
                            {
                                fluidType = article.Remove(0, article.LastIndexOf("...")).Replace(".", "").Trim();
                            }
                            else if (article.Contains("Only use"))
                            {
                                fluidType = article.Replace("Only use", "").Replace(".", "").Trim();
                            }


                            if (fluidType != null)
                            {
                                while (fluidType.Contains("<"))
                                    fluidType = fluidType.Remove(fluidType.IndexOf("<"), fluidType.IndexOf(">") - fluidType.IndexOf("<") + 1);

                                if (fluidTypeList == null)
                                    fluidTypeList = new List<string>();

                                fluidTypeList.Add(fluidType);
                            }

                            //Viscosity
                            RegularExpression.ParseViscosity(article, ref viscosityList);

                            //Part
                            if (article.ToUpper().Contains("PART NAME ..."))
                            {
                                partName = article.Remove(0, article.LastIndexOf("...")).Replace(".", "").Trim();

                                if (partName != null)
                                {
                                    while (partName.Contains("<"))
                                        partName = partName.Remove(partName.IndexOf("<"), partName.IndexOf(">") - partName.IndexOf("<") + 1);
                                }

                                if (partList == null)
                                    partList = new List<Part>();

                                if (part != null)
                                    partList.Add(part);

                                part = new Part();

                                part.PartName = partName;

                                partNumberList = null;
                            }

                            if ((article.ToUpper().Contains("PART NUMBER ...") || article.ToUpper().Contains("PART NO. ...")) || article.Contains("Part Number/Specification ..."))
                            {
                                if (partList == null)
                                    partList = new List<Part>();

                                if (part == null)
                                    part = new Part();

                                if (partNumberList == null)
                                    partNumberList = new List<PartNumber>();

                                partNumber = new PartNumber();
                                if (article.ToUpper().Contains("CANADA"))
                                    partNumber.CountryId = 1;
                                else
                                    partNumber.CountryId = 0;

                                partNumber.PartNo = article.Remove(0, article.LastIndexOf("...")).Replace(".", "").Trim();
                                if (partNumber.PartNo.Split(',').Length > 1)
                                    partNumber.PartNo = partNumber.PartNo.Split(',')[1].ToUpper().Replace("BMW", "").Replace("PART NUMBER", "").Trim();


                                while (partNumber.PartNo.Contains("<"))
                                    partNumber.PartNo = partNumber.PartNo.Remove(partNumber.PartNo.IndexOf("<"), partNumber.PartNo.IndexOf(">") - partNumber.PartNo.IndexOf("<") + 1);

                                partNumberList.Add(partNumber);

                                part.PartNumberList = partNumberList;
                            }



                        }
                    }

                    if (part != null)
                        partList.Add(part);


                    break;

                case 3073: //Fluid - M/T

                    foreach (string article in articles)
                    {
                        if (article.Trim() != "")
                        {
                            lineCount++;
                            fluidType = null;
                            partNumber = null;

                            if (article.ToUpper().Contains("TYPE ...") || article.ToUpper().Contains("TYPE  ...") || article.ToUpper().Contains("FLUID ...") || article.ToUpper().Contains("FLUID  ...") || article.ToUpper().Contains("GRADE ...") || article.ToUpper().Contains("GRADE  ...") || article.Contains("Type of Material ...") || article.Contains("API classification ...") || article.Contains("Manual Transmission ...") || article.Contains("Ford Specification ..."))
                            {
                                fluidType = article.Remove(0, article.LastIndexOf("...")).Replace(".", "").Trim();

                            }


                            if (fluidType != null)
                            {
                                while (fluidType.Contains("<"))
                                    fluidType = fluidType.Remove(fluidType.IndexOf("<"), fluidType.IndexOf(">") - fluidType.IndexOf("<") + 1);

                                if (fluidTypeList == null)
                                    fluidTypeList = new List<string>();

                                fluidTypeList.Add(fluidType);
                            }

                            //Viscosity
                            RegularExpression.ParseViscosity(article, ref viscosityList);

                            //Part
                            if (article.ToUpper().Contains("PART NAME ..."))
                            {
                                partName = article.Remove(0, article.LastIndexOf("...")).Replace(".", "").Trim();

                                if (partName != null)
                                {
                                    while (partName.Contains("<"))
                                        partName = partName.Remove(partName.IndexOf("<"), partName.IndexOf(">") - partName.IndexOf("<") + 1);
                                }

                                if (partList == null)
                                    partList = new List<Part>();

                                if (part != null)
                                    partList.Add(part);

                                part = new Part();

                                part.PartName = partName;

                                partNumberList = null;
                            }

                            if ((article.ToUpper().Contains("PART NUMBER ...") || article.ToUpper().Contains("PART NO. ...")) || article.Contains("Part Number/Specification ..."))
                            {
                                if (partList == null)
                                    partList = new List<Part>();

                                if (part == null)
                                    part = new Part();

                                if (partNumberList == null)
                                    partNumberList = new List<PartNumber>();

                                partNumber = new PartNumber();
                                if (article.ToUpper().Contains("CANADA"))
                                    partNumber.CountryId = 1;
                                else
                                    partNumber.CountryId = 0;

                                partNumber.PartNo = article.Remove(0, article.LastIndexOf("...")).Replace(".", "").Trim();
                                if (partNumber.PartNo.Split(',').Length > 1)
                                    partNumber.PartNo = partNumber.PartNo.Split(',')[1].ToUpper().Replace("BMW", "").Replace("PART NUMBER", "").Trim();


                                while (partNumber.PartNo.Contains("<"))
                                    partNumber.PartNo = partNumber.PartNo.Remove(partNumber.PartNo.IndexOf("<"), partNumber.PartNo.IndexOf(">") - partNumber.PartNo.IndexOf("<") + 1);

                                partNumberList.Add(partNumber);

                                part.PartNumberList = partNumberList;
                            }



                        }
                    }

                    if (part != null)
                        partList.Add(part);

                    break;

                case 2056: //Differential Fluid

                    foreach (string article in articles)
                    {
                        if (article.Trim() != "")
                        {
                            lineCount++;
                            fluidType = null;
                            partNumber = null;
                            
                            if (!article.ToUpper().Contains("REAR AXLE") && !article.ToUpper().Contains("FRONT AXLE"))
                            {
                                if (article.ToUpper().Contains("TYPE ...") || article.ToUpper().Contains("TYPE  ...") || article.ToUpper().Contains("FLUID ...") || article.ToUpper().Contains("FLUID  ...") || article.ToUpper().Contains("GRADE ...") || article.ToUpper().Contains("GRADE  ...") || article.Contains("Type of Material ...") || article.Contains("Ford Specification ..."))
                                {
                                    fluidType = article.Remove(0, article.LastIndexOf("...")).Replace(".", "").Trim();
                                       

                                }
                            }


                            if (fluidType != null)
                            {
                                while (fluidType.Contains("<"))
                                    fluidType = fluidType.Remove(fluidType.IndexOf("<"), fluidType.IndexOf(">") - fluidType.IndexOf("<") + 1);

                                if (fluidTypeList == null)
                                    fluidTypeList = new List<string>();

                                fluidTypeList.Add(fluidType);
                            }

                            //Viscosity
                            RegularExpression.ParseViscosity(article, ref viscosityList);

                            //Part
                            if (article.ToUpper().Contains("PART NAME ..."))
                            {
                                partName = article.Remove(0, article.LastIndexOf("...")).Replace(".", "").Trim();

                                if (partName != null)
                                {
                                    while (partName.Contains("<"))
                                        partName = partName.Remove(partName.IndexOf("<"), partName.IndexOf(">") - partName.IndexOf("<") + 1);
                                }

                                if (partList == null)
                                    partList = new List<Part>();

                                if (part != null)
                                    partList.Add(part);

                                part = new Part();

                                part.PartName = partName;

                                partNumberList = null;
                            }

                            if ((article.ToUpper().Contains("PART NUMBER ...") || article.ToUpper().Contains("PART NO. ...")) || article.Contains("Part Number/Specification ..."))
                            {
                                if (partList == null)
                                    partList = new List<Part>();

                                if (part == null)
                                    part = new Part();

                                if (partNumberList == null)
                                    partNumberList = new List<PartNumber>();

                                partNumber = new PartNumber();
                                if (article.ToUpper().Contains("CANADA"))
                                    partNumber.CountryId = 1;
                                else
                                    partNumber.CountryId = 0;

                                partNumber.PartNo = article.Remove(0, article.LastIndexOf("...")).Replace(".", "").Trim();
                                if (partNumber.PartNo.Split(',').Length > 1)
                                    partNumber.PartNo = partNumber.PartNo.Split(',')[1].ToUpper().Replace("BMW", "").Replace("PART NUMBER", "").Trim();


                                while (partNumber.PartNo.Contains("<"))
                                    partNumber.PartNo = partNumber.PartNo.Remove(partNumber.PartNo.IndexOf("<"), partNumber.PartNo.IndexOf(">") - partNumber.PartNo.IndexOf("<") + 1);

                                partNumberList.Add(partNumber);

                                part.PartNumberList = partNumberList;
                            }



                        }
                    }

                    if (part != null)
                        partList.Add(part);

                    break;


                case 2055: //Transfer case Fluid

                    foreach (string article in articles)
                    {
                        if (article.Trim() != "")
                        {
                            lineCount++;
                            fluidType = null;
                            partNumber = null;

                            if (article.ToUpper().Contains("TYPE ...") || article.ToUpper().Contains("TYPE  ...") || article.ToUpper().Contains("FLUID ...") || article.ToUpper().Contains("FLUID  ...") || article.ToUpper().Contains("GRADE ...") || article.ToUpper().Contains("GRADE  ...") || article.Contains("Type of Material ...") || article.Contains("Ford Specification ..."))
                            {
                                fluidType = article.Remove(0, article.LastIndexOf("...")).Replace(".", "").Trim();

                            }

                            if (fluidType != null)
                            {
                                while (fluidType.Contains("<"))
                                    fluidType = fluidType.Remove(fluidType.IndexOf("<"), fluidType.IndexOf(">") - fluidType.IndexOf("<") + 1);

                                if (fluidTypeList == null)
                                    fluidTypeList = new List<string>();

                                fluidTypeList.Add(fluidType);
                            }

                            //Viscosity
                            RegularExpression.ParseViscosity(article, ref viscosityList);

                            //Part
                            if (article.ToUpper().Contains("PART NAME ..."))
                            {
                                partName = article.Remove(0, article.LastIndexOf("...")).Replace(".", "").Trim();

                                if (partName != null)
                                {
                                    while (partName.Contains("<"))
                                        partName = partName.Remove(partName.IndexOf("<"), partName.IndexOf(">") - partName.IndexOf("<") + 1);
                                }

                                if (partList == null)
                                    partList = new List<Part>();

                                if (part != null)
                                    partList.Add(part);

                                part = new Part();

                                part.PartName = partName;

                                partNumberList = null;
                            }

                            if ((article.ToUpper().Contains("PART NUMBER ...") || article.ToUpper().Contains("PART NO. ...")) || article.Contains("Part Number/Specification ..."))
                            {
                                if (partList == null)
                                    partList = new List<Part>();

                                if (part == null)
                                    part = new Part();

                                if (partNumberList == null)
                                    partNumberList = new List<PartNumber>();

                                partNumber = new PartNumber();
                                if (article.ToUpper().Contains("CANADA"))
                                    partNumber.CountryId = 1;
                                else
                                    partNumber.CountryId = 0;

                                partNumber.PartNo = article.Remove(0, article.LastIndexOf("...")).Replace(".", "").Trim();
                                if (partNumber.PartNo.Split(',').Length > 1)
                                    partNumber.PartNo = partNumber.PartNo.Split(',')[1].ToUpper().Replace("BMW", "").Replace("PART NUMBER", "").Trim();


                                while (partNumber.PartNo.Contains("<"))
                                    partNumber.PartNo = partNumber.PartNo.Remove(partNumber.PartNo.IndexOf("<"), partNumber.PartNo.IndexOf(">") - partNumber.PartNo.IndexOf("<") + 1);

                                partNumberList.Add(partNumber);

                                part.PartNumberList = partNumberList;
                            }



                        }
                    }

                    if (part != null)
                        partList.Add(part);

                    break;

                case 1320: //Brake Fluid

                    foreach (string article in articles)
                    {
                        if (article.Trim() != "")
                        {
                            lineCount++;
                            fluidType = null;
                            partNumber = null;

                            if (article.ToUpper().Contains("TYPE ...") || article.ToUpper().Contains("TYPE  ...") || article.ToUpper().Contains("FLUID ...") || article.ToUpper().Contains("FLUID  ...") || article.ToUpper().Contains("GRADE ...") || article.ToUpper().Contains("GRADE  ...") || article.Contains("Type of Material ...") || article.Contains("Ford Specification ..."))
                            {
                                fluidType = article.Remove(0, article.LastIndexOf("...")).Replace(".", "").Trim();
                                    

                            }

                            if (fluidType != null)
                            {
                                while (fluidType.Contains("<"))
                                    fluidType = fluidType.Remove(fluidType.IndexOf("<"), fluidType.IndexOf(">") - fluidType.IndexOf("<") + 1);

                                if (fluidTypeList == null)
                                    fluidTypeList = new List<string>();

                                fluidTypeList.Add(fluidType);
                            }

                            //Viscosity
                            RegularExpression.ParseViscosity(article, ref viscosityList);

                            //Part
                            if (article.ToUpper().Contains("PART NAME ..."))
                            {
                                partName = article.Remove(0, article.LastIndexOf("...")).Replace(".", "").Trim();

                                if (partName != null)
                                {
                                    while (partName.Contains("<"))
                                        partName = partName.Remove(partName.IndexOf("<"), partName.IndexOf(">") - partName.IndexOf("<") + 1);
                                }

                                if (partList == null)
                                    partList = new List<Part>();

                                if (part != null)
                                    partList.Add(part);

                                part = new Part();

                                part.PartName = partName;

                                partNumberList = null;
                            }

                            if ((article.ToUpper().Contains("PART NUMBER ...") || article.ToUpper().Contains("PART NO. ...")) || article.Contains("Part Number/Specification ..."))
                            {
                                if (partList == null)
                                    partList = new List<Part>();

                                if (part == null)
                                    part = new Part();

                                if (partNumberList == null)
                                    partNumberList = new List<PartNumber>();

                                partNumber = new PartNumber();
                                if (article.ToUpper().Contains("CANADA"))
                                    partNumber.CountryId = 1;
                                else
                                    partNumber.CountryId = 0;

                                partNumber.PartNo = article.Remove(0, article.LastIndexOf("...")).Replace(".", "").Trim();
                                if (partNumber.PartNo.Split(',').Length > 1)
                                    partNumber.PartNo = partNumber.PartNo.Split(',')[1].ToUpper().Replace("BMW", "").Replace("PART NUMBER", "").Trim();


                                while (partNumber.PartNo.Contains("<"))
                                    partNumber.PartNo = partNumber.PartNo.Remove(partNumber.PartNo.IndexOf("<"), partNumber.PartNo.IndexOf(">") - partNumber.PartNo.IndexOf("<") + 1);

                                partNumberList.Add(partNumber);

                                part.PartNumberList = partNumberList;
                            }



                        }
                    }

                    if (part != null)
                        partList.Add(part);

                    break;

                case 3394: //Brake Fluid

                    foreach (string article in articles)
                    {
                        if (article.Trim() != "")
                        {
                            lineCount++;
                            fluidType = null;
                            partNumber = null;

                           
                            if (article.ToUpper().Contains("TYPE ...") || article.ToUpper().Contains("TYPE  ...") || article.ToUpper().Contains("FLUID ...") || article.ToUpper().Contains("FLUID  ...") || article.ToUpper().Contains("GRADE ...") || article.ToUpper().Contains("GRADE  ...") || article.Contains("Type of Material ...") || article.Contains("Ford Specification ..."))
                            {
                                fluidType = article.Remove(0, article.LastIndexOf("...")).Replace(".", "").Trim();
                                   

                            }

                            if (fluidType != null)
                            {
                                while (fluidType.Contains("<"))
                                    fluidType = fluidType.Remove(fluidType.IndexOf("<"), fluidType.IndexOf(">") - fluidType.IndexOf("<") + 1);

                                if (fluidTypeList == null)
                                    fluidTypeList = new List<string>();

                                fluidTypeList.Add(fluidType);
                            }

                            //Viscosity
                            RegularExpression.ParseViscosity(article, ref viscosityList);

                            //Part
                            if (article.ToUpper().Contains("PART NAME ..."))
                            {
                                partName = article.Remove(0, article.LastIndexOf("...")).Replace(".", "").Trim();

                                if (partName != null)
                                {
                                    while (partName.Contains("<"))
                                        partName = partName.Remove(partName.IndexOf("<"), partName.IndexOf(">") - partName.IndexOf("<") + 1);
                                }

                                if (partList == null)
                                    partList = new List<Part>();

                                if (part != null)
                                    partList.Add(part);

                                part = new Part();

                                part.PartName = partName;

                                partNumberList = null;
                            }

                            if ((article.ToUpper().Contains("PART NUMBER ...") || article.ToUpper().Contains("PART NO. ...")) || article.Contains("Part Number/Specification ..."))
                            {
                                if (partList == null)
                                    partList = new List<Part>();

                                if (part == null)
                                    part = new Part();

                                if (partNumberList == null)
                                    partNumberList = new List<PartNumber>();

                                partNumber = new PartNumber();
                                if (article.ToUpper().Contains("CANADA"))
                                    partNumber.CountryId = 1;
                                else
                                    partNumber.CountryId = 0;

                                partNumber.PartNo = article.Remove(0, article.LastIndexOf("...")).Replace(".", "").Trim();
                                if (partNumber.PartNo.Split(',').Length > 1)
                                    partNumber.PartNo = partNumber.PartNo.Split(',')[1].ToUpper().Replace("BMW", "").Replace("PART NUMBER", "").Trim();


                                while (partNumber.PartNo.Contains("<"))
                                    partNumber.PartNo = partNumber.PartNo.Remove(partNumber.PartNo.IndexOf("<"), partNumber.PartNo.IndexOf(">") - partNumber.PartNo.IndexOf("<") + 1);

                                partNumberList.Add(partNumber);

                                part.PartNumberList = partNumberList;
                            }



                        }
                    }

                    if (part != null)
                        partList.Add(part);

                    break;

                default:
                    foreach (string article in articles)
                    {
                        if (article.Trim() != "")
                        {
                            lineCount++;
                            fluidType = null;
                            partNumber = null;

                            if (article.ToUpper().Contains("TYPE ...") || article.ToUpper().Contains("TYPE  ...") || article.ToUpper().Contains("FLUID ...") || article.ToUpper().Contains("FLUID  ...") || article.ToUpper().Contains("GRADE ...") || article.ToUpper().Contains("GRADE  ...") || article.Contains("Type of Material ..."))
                            {
                                fluidType = article.Remove(0, article.LastIndexOf("...")).Replace(".", "").Trim();
                                   

                            }


                            if (fluidType != null)
                            {
                                while (fluidType.Contains("<"))
                                    fluidType = fluidType.Remove(fluidType.IndexOf("<"), fluidType.IndexOf(">") - fluidType.IndexOf("<") + 1);

                                if (fluidTypeList == null)
                                    fluidTypeList = new List<string>();

                                fluidTypeList.Add(fluidType);
                            }

                            //Viscosity
                            RegularExpression.ParseViscosity(article, ref viscosityList);

                            //Part
                            if (article.ToUpper().Contains("PART NAME ..."))
                            {
                                partName = article.Remove(0, article.LastIndexOf("...")).Replace(".", "").Trim();

                                if (partName != null)
                                {
                                    while (partName.Contains("<"))
                                        partName = partName.Remove(partName.IndexOf("<"), partName.IndexOf(">") - partName.IndexOf("<") + 1);
                                }

                                if (partList == null)
                                    partList = new List<Part>();

                                if (part != null)
                                    partList.Add(part);

                                part = new Part();

                                part.PartName = partName;

                                partNumberList = null;
                            }

                            if ((article.ToUpper().Contains("PART NUMBER ...") || article.ToUpper().Contains("PART NO. ...")) || article.Contains("Part Number/Specification ..."))
                            {
                                if (partList == null)
                                    partList = new List<Part>();

                                if (part == null)
                                    part = new Part();

                                if (partNumberList == null)
                                    partNumberList = new List<PartNumber>();

                                partNumber = new PartNumber();
                                if (article.ToUpper().Contains("CANADA"))
                                    partNumber.CountryId = 1;
                                else
                                    partNumber.CountryId = 0;

                                partNumber.PartNo = article.Remove(0, article.LastIndexOf("...")).Replace(".", "").Trim();
                                if (partNumber.PartNo.Split(',').Length > 1)
                                    partNumber.PartNo = partNumber.PartNo.Split(',')[1].ToUpper().Replace("BMW", "").Replace("PART NUMBER", "").Trim();


                                while (partNumber.PartNo.Contains("<"))
                                    partNumber.PartNo = partNumber.PartNo.Remove(partNumber.PartNo.IndexOf("<"), partNumber.PartNo.IndexOf(">") - partNumber.PartNo.IndexOf("<") + 1);

                                partNumberList.Add(partNumber);

                                part.PartNumberList = partNumberList;
                            }



                        }
                    }

                    if (part != null)
                        partList.Add(part);

                    return;
            }



        }


        private string GetViscosityFromList(List<string> viscosityList)
        {
            string viscosity = null; 

            if (viscosityList != null && viscosityList.Count > 0)
            {
                foreach (string viscosityItem in viscosityList)
                {
                    if (viscosityItem.ToUpper().Contains("PREFERRED") || viscosityItem.ToUpper().Contains("RECOMMENDED"))
                        viscosity = viscosityItem.ToUpper().Replace("(", "").Replace("PREFERRED", "").Replace("RECOMMENDED", "").Replace(")", "");
                }
                if (viscosity == null || viscosity == "")
                    viscosity = viscosityList[0];
            }

            return viscosity;

        }

    }
}

