﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using HtmlAgilityPack;

namespace SmartVMAPlus.AllData.WebScrape.Parse
{
    public class ParseComponent : IParse 
    {
        public void ParseHtml(string pageContent, string param, ref DataTable dt)
        {

            if (pageContent != null)
            {
                HtmlDocument htmlDoc = new HtmlDocument();

                htmlDoc.LoadHtml(pageContent);

                List<HtmlNode> componentList = htmlDoc.DocumentNode.Descendants().Where
                  (x => (x.Name == "ul" && x.Attributes["class"] != null &&
                  x.Attributes["class"].Value.Contains("components"))).ToList();

                if (componentList != null && componentList.Count > 0)
                {
                    List<HtmlNode> childList = componentList[0].Descendants().Where
                        (x => (x.Name == "a" && x.Attributes["class"] != null &&
                        x.Attributes["class"].Value.Contains("component"))).ToList();

                    if (childList != null && childList.Count > 0)
                    {
                        string[] parameters = param.Split('|');
                        int carExtId = int.Parse(parameters[0]);
                        int oemComponentId = int.Parse(parameters[1]);

                        string href;
                        int subOemComponentId = 0;

                        foreach (HtmlNode htmlNode in childList)
                        {
                            href = htmlNode.Attributes["href"].Value;
                            subOemComponentId = int.Parse(href.Split('/')[5]);
                            if (subOemComponentId == 807)
                                break;
                        }
            

                        DataRow dr;
                        dr = dt.NewRow();
                        dr["CarExtId"] = carExtId;
                        dr["OemComponentId"] = subOemComponentId;

                        if (oemComponentId != 9 || subOemComponentId != 807)
                        {
                            dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("SubComponent", componentList[0].InnerHtml);
                        }

                        dt.Rows.Add(dr);
                    }
                }

            }

        }

    }
}
