﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using HtmlAgilityPack;

namespace SmartVMAPlus.AllData.WebScrape.Parse
{
    public class ParseService : IParse 
    {
        public void ParseHtml(string pageContent, string param, ref DataTable dt)
        {

            if (pageContent != null)
            {
                HtmlDocument htmlDoc = new HtmlDocument();

                htmlDoc.LoadHtml(pageContent);

                List<HtmlNode> serviceScopeList = htmlDoc.DocumentNode.Descendants().Where
                  (x => (x.Name == "div" && x.Attributes["class"] != null &&
                  x.Attributes["class"].Value.Contains("operation"))).ToList();

                if (serviceScopeList != null && serviceScopeList.Count > 0)
                {
                    List<HtmlNode> serviceList;
                    List<HtmlNode> opDescList;
                    List<HtmlNode> componentList;
                    string[] operationScopes = param.ToUpper().Split('|');
                    int iType = int.Parse(operationScopes[0]);
                    int interval = int.Parse(operationScopes[1]);
                    string oemComponent;
                    string opAction = "";
                    string opDescription = "";
                    string href = "";
                    string[] dataValues;
                    bool isValidOperation = false;

                    DataRow dr;


                    foreach (HtmlNode serviceScope in serviceScopeList)
                    {
                        if (serviceScope.ChildNodes != null && serviceScope.ChildNodes[0] != null && serviceScope.ChildNodes[0].ChildNodes[0] != null)
                        {
                            isValidOperation = false;

                            opAction = serviceScope.ChildNodes[0].ChildNodes[0].InnerText.ToUpper();

                            foreach (string operation in operationScopes)
                            {
                                if (opAction == operation.ToUpper())
                                {
                                    isValidOperation = true;
                                    break;
                                }
                            }


                            if (isValidOperation)
                            {
                                serviceList = serviceScope.Descendants().Where(y => (y.Name == "div" && y.Attributes["class"] != null && y.Attributes["class"].Value.Contains("component"))).ToList();

                                if (serviceList != null && serviceList.Count > 0)
                                {
                                    foreach (HtmlNode service in serviceList)
                                    {
                                        opDescList = service.Descendants().Where(z => (z.Name == "div" && z.Attributes["class"] != null && z.Attributes["class"].Value.Contains("notes"))).ToList();
                                        if (opDescList != null && opDescList.Count > 0)
                                        {
                                            opDescription = opDescList[0].InnerText;
                                        }
                                        else
                                            opDescription = "";

                                        componentList = service.Descendants().Where(z => (z.Name == "a" && z.Attributes["href"] != null)).ToList();

                                        if (componentList != null && componentList.Count > 0)
                                        {
                                            if (dt == null)
                                            {
                                                dt = new DataTable();

                                                dt.Columns.Add("CarExtId", typeof(Int32));
                                                dt.Columns.Add("iType", typeof(Int32));
                                                dt.Columns.Add("Interval", typeof(Int32));
                                                dt.Columns.Add("OemComponentId", typeof(Int32));
                                                dt.Columns.Add("OemComponent", typeof(string));
                                                dt.Columns.Add("OpAction", typeof(string));
                                                dt.Columns.Add("OpDescription", typeof(string));
                                            }

                                            href = componentList[0].Attributes["href"].Value;
                                            dataValues = href.Split('/');
                                            oemComponent = componentList[0].InnerText.Replace("\n", "").Replace("\t", "").Trim();

                                            dr = dt.NewRow();
                                            dr["CarExtId"] = int.Parse(dataValues[3]);
                                            dr["iType"] = iType;
                                            dr["Interval"] = interval;
                                            dr["OemComponentId"] = int.Parse(dataValues[5]);
                                            dr["OemComponent"] = oemComponent;
                                            dr["OpAction"] = opAction;
                                            if (opDescription.Trim() == "")
                                                dr["OpDescription"] = opAction + " " + oemComponent;
                                            else
                                                dr["OpDescription"] = opDescription;

                                            dt.Rows.Add(dr);
                                        }
                                    }
                                }

                            }


                        }
                    }
                }

            }

        }

    }
}
