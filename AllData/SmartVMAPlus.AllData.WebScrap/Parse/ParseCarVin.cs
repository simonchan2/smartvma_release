﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using HtmlAgilityPack;
using System.Xml;

namespace SmartVMAPlus.AllData.WebScrape.Parse
{
    public class ParseCarVin : IParse 
    {
        public void ParseHtml(string pageContent, string param, ref DataTable dt)
        {

            if (pageContent != null)
            {
                string vin = param.Split('|')[1];

                HtmlDocument htmlDoc = new HtmlDocument();

                htmlDoc.LoadHtml(pageContent);

                string xPath;

                if (param.Split('|')[0] == "SingleVin")
                    xPath = "//span[@class='carid']";
                else
                    xPath = "//dd[@class='items']//a";

                string carExtId;

                HtmlNodeCollection htmlNodeCollection = htmlDoc.DocumentNode.SelectNodes(xPath);

                if (htmlNodeCollection != null && htmlNodeCollection.Count > 0)
                {
                    DataRow dr;
                    if (param.Split('|')[0] == "SingleVin")
                    {
                        carExtId = htmlNodeCollection[0].InnerText;

                        dr = dt.NewRow();
                        dr["VIN"] = vin;
                        dr["Sequence"] = 1;
                        dr["CarExtId"] = carExtId;

                        dt.Rows.Add(dr);
                    }
                    else
                    {
                        int sequence = 0;
                        foreach (HtmlNode htmlNode in htmlNodeCollection)
                        {
                            carExtId = htmlNode.GetAttributeValue("href", "").Split('/')[3];
                            sequence++;

                            dr = dt.NewRow();
                            dr["VIN"] = vin;
                            dr["Sequence"] = sequence;
                            dr["CarExtId"] = carExtId;

                            dt.Rows.Add(dr);
                        }
                    }

                 
                }


            }

        }
    }
}
