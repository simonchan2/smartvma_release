﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using HtmlAgilityPack;

namespace SmartVMAPlus.AllData.WebScrape.Parse
{
    public class ParseLabor : IParse 
    {
        public void ParseHtml(string pageContent, string param, ref DataTable dt)
        {

            if (pageContent != null)
            {
                string[] parameters = param.Split('|');
                int carExtId = int.Parse(parameters[0]);
                int oemComponentId = int.Parse(parameters[1]);
                int actualOemComponentId = 0;
                if (int.Parse(parameters[2]) > 0)
                    actualOemComponentId = int.Parse(parameters[2]);
                else
                    actualOemComponentId = oemComponentId;

                DataRow dr;

                decimal laborHourResult = 0;

                HtmlDocument htmlDoc = new HtmlDocument();

                htmlDoc.LoadHtml(pageContent);

                List<HtmlNode> articleList = htmlDoc.DocumentNode.Descendants().Where
                  (x => (x.Name == "dd" && x.Attributes["class"] != null &&
                  x.Attributes["class"].Value.Contains("article_body"))).ToList();

                if (articleList != null && articleList.Count == 1)
                {
                    List<HtmlNode> laborList = articleList[0].Descendants().Where
                      (x => (x.Name == "tr" && x.Attributes["class"] != null &&
                      x.Attributes["class"].Value.Contains("labor"))).ToList();

                    if (laborList != null && laborList.Count == 1)
                    {
                        List<HtmlNode> tdList = laborList[0].Descendants().Where(x => (x.Name == "td")).ToList();
                        if (tdList != null && tdList.Count > 4 && decimal.TryParse(tdList[4].InnerText, out laborHourResult))
                        {
                            dr = dt.NewRow();
                            dr["CarExtId"] = carExtId;
                            dr["OemComponentId"] = oemComponentId;
                            dr["LaborSkillLevel"] = tdList[2].InnerText;
                            dr["LaborHour"] = laborHourResult;

                            dt.Rows.Add(dr);
                        }
                        else
                        {
                            dr = dt.NewRow();
                            dr["CarExtId"] = carExtId;
                            dr["OemComponentId"] = oemComponentId;
                            dr["ExceptionMessage"] = articleList[0].InnerText;
                            dt.Rows.Add(dr);
                        }

                    }
                    else if (laborList != null && laborList.Count > 1)
                    {
                        HandleMultiLineLabor(carExtId, oemComponentId, actualOemComponentId, laborList, articleList[0], ref dt);
                    }

                }
                else
                {
                    List<HtmlNode> contentList = htmlDoc.DocumentNode.Descendants().Where
                      (x => (x.Name == "ul" && x.Attributes["class"] != null &&
                      x.Attributes["class"].Value.Contains("article"))).ToList();

                    dr = dt.NewRow();
                    dr["CarExtId"] = carExtId;
                    dr["OemComponentId"] = oemComponentId;
                    if (contentList != null && contentList.Count > 0)
                        dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("LaborHour", contentList[0].OuterHtml);
                    else
                        dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("LaborHour", "No Message");
                    dt.Rows.Add(dr);
                }

            }
        }

        private void HandleMultiLineLabor(int carExtId,int oemComponentId,int actualOemComponentId, List<HtmlNode> laborList, HtmlNode articleBody, ref DataTable dt)
        {
            DataRow dr;
            List<HtmlNode> tdList;
            List<HtmlNode> divList;
            List<HtmlNode> subList;
            decimal laborHourResult = 0;

            switch (actualOemComponentId)
            {
                case 9: //Cooling System
                case 64: //Timing Belt
                case 195: //Fluid Filter - A/T
                case 381: //Positive Crankcase Ventilation Valve
                case 807: //Coolant
                case 1086: //Balance Shaft Belt
                case 1138: //Suspension Fluid Reservoir
                case 1320: //Brake Fluid
                case 1962: //Seat Temperature Element
                case 3283: //Cabin Air Filter / Purifier
                case 7892: //Fluid - CVT
                case 8033: //Fluid Filter - ASMT
                    tdList = laborList[0].Descendants().Where(x => (x.Name == "td")).ToList();
                    if (tdList != null && tdList.Count > 4 && decimal.TryParse(tdList[4].InnerText, out laborHourResult))
                    {
                        dr = dt.NewRow();
                        dr["CarExtId"] = carExtId;
                        dr["OemComponentId"] = oemComponentId;
                        dr["LaborSkillLevel"] = tdList[2].InnerText;
                        dr["LaborHour"] = laborHourResult;
                        dt.Rows.Add(dr);

                    }
                    else
                    {
                        dr = dt.NewRow();
                        dr["CarExtId"] = carExtId;
                        dr["OemComponentId"] = oemComponentId;
                        dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("LaborHour", articleBody.InnerHtml);
                        dt.Rows.Add(dr);
                    }
                    return;
                case 19: //Fuel Filter
                    foreach (HtmlNode htmlNode in laborList)
                    {
                        tdList = htmlNode.Descendants().Where(x => (x.Name == "td")).ToList();
                        if (tdList != null && tdList.Count > 4 && decimal.TryParse(tdList[4].InnerText, out laborHourResult))
                        {
                            if (tdList[1].InnerText.ToUpper().Contains("BOTH"))
                            {
                                dr = dt.NewRow();
                                dr["CarExtId"] = carExtId;
                                dr["OemComponentId"] = oemComponentId;
                                dr["LaborSkillLevel"] = tdList[2].InnerText;
                                dr["LaborHour"] = laborHourResult;
                                dt.Rows.Add(dr);

                                return;
                            }
                         
                            
                        }

                    }

                    foreach (HtmlNode htmlNode in laborList)
                    {
                        tdList = htmlNode.Descendants().Where(x => (x.Name == "td")).ToList();
                        if (tdList != null && tdList.Count > 4 && decimal.TryParse(tdList[4].InnerText, out laborHourResult))
                        {
                          
                            if (tdList[1].InnerText.ToUpper().Contains("INLINE") || tdList[1].InnerText.ToUpper().Contains("IN LINE"))
                            {
                                dr = dt.NewRow();
                                dr["CarExtId"] = carExtId;
                                dr["OemComponentId"] = oemComponentId;
                                dr["LaborSkillLevel"] = tdList[2].InnerText;
                                dr["LaborHour"] = laborHourResult;
                                dt.Rows.Add(dr);

                                return;
                            }

                        }

                    }

                    dr = dt.NewRow();
                    dr["CarExtId"] = carExtId;
                    dr["OemComponentId"] = oemComponentId;
                    dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("LaborHour", articleBody.InnerHtml);
                    dt.Rows.Add(dr);

                    return;
                case 21: //Brake Pad
                    foreach (HtmlNode htmlNode in laborList)
                    {
                        tdList = htmlNode.Descendants().Where(x => (x.Name == "td")).ToList();
                        if (tdList != null && tdList.Count > 4 && decimal.TryParse(tdList[4].InnerText, out laborHourResult) && tdList[1].InnerText.ToUpper().Contains("FRONT PADS"))
                        {
                            dr = dt.NewRow();
                            dr["CarExtId"] = carExtId;
                            dr["OemComponentId"] = oemComponentId;
                            dr["LaborSkillLevel"] = tdList[2].InnerText;
                            dr["LaborHour"] = laborHourResult;
                            dt.Rows.Add(dr);

                            return;
                        }

                    }

                    divList = articleBody.Descendants().Where(x => (x.Name == "div" && x.Attributes["class"] != null && x.Attributes["class"].Value.Contains("labor_qualifier"))).ToList();
                    if (divList != null && divList.Count > 0)
                    {
                        foreach (HtmlNode htmlNode in divList)
                        {
                            subList = htmlNode.Descendants().Where(x => (x.Name == "span" && x.Attributes["class"] != null && x.Attributes["class"].Value.Contains("labor_qual_name"))).ToList();
                            if (subList != null && subList.Count > 0 && subList[0].InnerText.ToUpper().Contains("FRONT PADS"))
                            {
                                tdList = htmlNode.Descendants().Where(x => (x.Name == "td")).ToList();
                                if (tdList != null && tdList.Count > 4 && decimal.TryParse(tdList[4].InnerText, out laborHourResult))
                                {
                                    dr = dt.NewRow();
                                    dr["CarExtId"] = carExtId;
                                    dr["OemComponentId"] = oemComponentId;
                                    dr["LaborSkillLevel"] = tdList[2].InnerText;
                                    dr["LaborHour"] = laborHourResult;
                                    dt.Rows.Add(dr);

                                    return;
                                }
                            }
                        }
                    }

                    dr = dt.NewRow();
                    dr["CarExtId"] = carExtId;
                    dr["OemComponentId"] = oemComponentId;
                    dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("LaborHour", articleBody.InnerHtml);
                    dt.Rows.Add(dr);

                    return;
                case 27: //Spark Plug
                    string laborSkillLevel = null;
                    decimal laborHour = 0;
                    foreach (HtmlNode htmlNode in laborList)
                    {
                        tdList = htmlNode.Descendants().Where(x => (x.Name == "td")).ToList();
                        if (tdList != null && tdList.Count > 4 && decimal.TryParse(tdList[4].InnerText,out laborHourResult))
                        {
                            if (laborHourResult > laborHour)
                            {
                                laborSkillLevel = tdList[2].InnerText;
                                laborHour = laborHourResult;
                            }
                        }
                    }

                    if (laborHour > 0)
                    {
                        dr = dt.NewRow();
                        dr["CarExtId"] = carExtId;
                        dr["OemComponentId"] = oemComponentId;
                        dr["LaborSkillLevel"] = laborSkillLevel;
                        dr["LaborHour"] = laborHour;
                        dt.Rows.Add(dr);
                    }
                    else
                    {
                        dr = dt.NewRow();
                        dr["CarExtId"] = carExtId;
                        dr["OemComponentId"] = oemComponentId;
                        dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("LaborHour", articleBody.InnerHtml);
                        dt.Rows.Add(dr);
                    }

                    return;
                case 45: //Wheel Bearing
                    divList = articleBody.Descendants().Where(x => (x.Name == "div" && x.Attributes["class"] != null && x.Attributes["class"].Value.Contains("operation"))).ToList();
                    if (divList != null && divList.Count > 0)
                    foreach (HtmlNode htmlNode in divList)
                    {
                        subList = htmlNode.Descendants().Where(x => (x.Name == "span" && x.Attributes["class"] != null && x.Attributes["class"].Value.Contains("operation_name"))).ToList();
                        if (subList != null && subList.Count > 0 && subList[0].InnerText.ToUpper().Contains("CLEAN/REPACK"))
                        {
                            laborList = htmlNode.Descendants().Where(x => (x.Name == "tr" && x.Attributes["class"] != null && x.Attributes["class"].Value.Contains("labor"))).ToList();
                            if (laborList != null && laborList.Count > 0)
                            {
                                foreach (HtmlNode htmlNode2 in laborList)
                                {
                                    tdList = htmlNode2.Descendants().Where(x => (x.Name == "td")).ToList();
                                    if (tdList != null && tdList.Count > 4 && tdList[1].InnerText.ToUpper().Contains("BOTH SIDES") && decimal.TryParse(tdList[4].InnerText, out laborHourResult))
                                    {
                                        dr = dt.NewRow();
                                        dr["CarExtId"] = carExtId;
                                        dr["OemComponentId"] = oemComponentId;
                                        dr["LaborSkillLevel"] = tdList[2].InnerText;
                                        dr["LaborHour"] = laborHourResult;
                                        dt.Rows.Add(dr);

                                        return;
                                    }
                                }
                            }
                            
                            
                        }

                    }

                    dr = dt.NewRow();
                    dr["CarExtId"] = carExtId;
                    dr["OemComponentId"] = oemComponentId;
                    dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("LaborHour", articleBody.InnerHtml);
                    dt.Rows.Add(dr);

                    return;

                case 142: //Wheels. Wait for rule clarification.
                case 154: //Tires
                    foreach (HtmlNode htmlNode in laborList)
                    {
                        tdList = htmlNode.Descendants().Where(x => (x.Name == "td")).ToList();

                        if (tdList != null && tdList.Count > 4 && decimal.TryParse(tdList[4].InnerText,out laborHourResult))
                        {
                            if (tdList[1].InnerText.ToUpper().Contains("4 WHEELS") || tdList[1].InnerText.ToUpper().Contains("4-WHEELS") || tdList[1].InnerText.ToUpper().Contains("FOUR WHEELS"))
                            {
                                dr = dt.NewRow();
                                dr["CarExtId"] = carExtId;
                                dr["OemComponentId"] = oemComponentId;
                                dr["LaborSkillLevel"] = tdList[2].InnerText;
                                dr["LaborHour"] = laborHourResult;
                                dt.Rows.Add(dr);
                                return;
                            }
                        }
                        
                    }

                    foreach (HtmlNode htmlNode in laborList)
                    {
                        tdList = htmlNode.Descendants().Where(x => (x.Name == "td")).ToList();

                        if (tdList != null && tdList.Count > 4 && decimal.TryParse(tdList[4].InnerText, out laborHourResult))
                        {
                            if (tdList[1].InnerText.ToUpper().Contains("ROTATE"))
                            {
                                dr = dt.NewRow();
                                dr["CarExtId"] = carExtId;
                                dr["OemComponentId"] = oemComponentId;
                                dr["LaborSkillLevel"] = tdList[2].InnerText;
                                dr["LaborHour"] = laborHourResult;
                                dt.Rows.Add(dr);
                                return;
                            }
                        }

                    }
                    

                    dr = dt.NewRow();
                    dr["CarExtId"] = carExtId;
                    dr["OemComponentId"] = oemComponentId;
                    dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("LaborHour", articleBody.InnerHtml);
                    dt.Rows.Add(dr);
    
                    return;
                case 205: //Fluid Line/Hose, A/T
                    foreach (HtmlNode htmlNode in laborList)
                    {
                        tdList = htmlNode.Descendants().Where(x => (x.Name == "td")).ToList();
                        if (tdList != null && tdList.Count > 4 && decimal.TryParse(tdList[4].InnerText, out laborHourResult) && tdList[1].InnerText.ToUpper().Contains("BOTH"))
                        {
                            dr = dt.NewRow();
                            dr["CarExtId"] = carExtId;
                            dr["OemComponentId"] = oemComponentId;
                            dr["LaborSkillLevel"] = tdList[2].InnerText;
                            dr["LaborHour"] = laborHourResult;
                            dt.Rows.Add(dr);
                            return;
                        }
                    }

                    dr = dt.NewRow();
                    dr["CarExtId"] = carExtId;
                    dr["OemComponentId"] = oemComponentId;
                    dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("LaborHour", articleBody.InnerHtml);
                    dt.Rows.Add(dr);

                    return;
                case 366: //Air Filter Element
                    foreach (HtmlNode htmlNode in laborList)
                    {
                        tdList = htmlNode.Descendants().Where(x => (x.Name == "td")).ToList();
                        if (tdList != null && tdList.Count > 4 && decimal.TryParse(tdList[4].InnerText, out laborHourResult) && (tdList[1].InnerText.ToUpper().Contains("REPLACE") || tdList[1].InnerText.ToUpper().Contains("BOTH")))
                        {
                            dr = dt.NewRow();
                            dr["CarExtId"] = carExtId;
                            dr["OemComponentId"] = oemComponentId;
                            dr["LaborSkillLevel"] = tdList[2].InnerText;
                            dr["LaborHour"] = laborHourResult;
                            dt.Rows.Add(dr);
                            return;
                        }

                    }

                    dr = dt.NewRow();
                    dr["CarExtId"] = carExtId;
                    dr["OemComponentId"] = oemComponentId;
                    dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("LaborHour", articleBody.InnerHtml);
                    dt.Rows.Add(dr);

                    return;
                case 484: //Oil Filter, Engine
                case 89: //Timing Belt Tensioner
                case 95: //Drive Belt Tensioner
                    return;
                case 640: //Glow Plug
                    foreach (HtmlNode htmlNode in laborList)
                    {
                        tdList = htmlNode.Descendants().Where(x => (x.Name == "td")).ToList();
                        if (tdList != null && tdList.Count > 4 && decimal.TryParse(tdList[4].InnerText, out laborHourResult) && tdList[1].InnerText.ToUpper().Contains("ALL"))
                        {
                            dr = dt.NewRow();
                            dr["CarExtId"] = carExtId;
                            dr["OemComponentId"] = oemComponentId;
                            dr["LaborSkillLevel"] = tdList[2].InnerText;
                            dr["LaborHour"] = laborHourResult;
                            dt.Rows.Add(dr);
                            return;
                        }

                    }

                    dr = dt.NewRow();
                    dr["CarExtId"] = carExtId;
                    dr["OemComponentId"] = oemComponentId;
                    dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("LaborHour", articleBody.InnerHtml);
                    dt.Rows.Add(dr);

                    return;
                case 800: //Engine Oil
                    foreach (HtmlNode htmlNode in laborList)
                    {
                        tdList = htmlNode.Descendants().Where(x => (x.Name == "td")).ToList();
                        if (tdList != null && tdList.Count > 4 && decimal.TryParse(tdList[4].InnerText, out laborHourResult) && tdList[1].InnerText.ToUpper().Contains("OIL CHANGE"))
                        {
                            dr = dt.NewRow();
                            dr["CarExtId"] = carExtId;
                            dr["OemComponentId"] = oemComponentId;
                            dr["LaborSkillLevel"] = tdList[2].InnerText;
                            dr["LaborHour"] = laborHourResult;
                            dt.Rows.Add(dr);

                            return;
                        }

                    }

                    divList = articleBody.Descendants().Where(x => (x.Name == "div" && x.Attributes["class"] != null && x.Attributes["class"].Value.Contains("labor_qualifier"))).ToList();
                    if (divList != null && divList.Count > 0)
                    {
                        foreach (HtmlNode htmlNode in divList)
                        {
                            subList = htmlNode.Descendants().Where(x => (x.Name == "span" && x.Attributes["class"] != null && x.Attributes["class"].Value.Contains("labor_qual_name"))).ToList();
                            if (subList != null && subList.Count > 0 && subList[0].InnerText.ToUpper().Contains("OIL CHANGE"))
                            {
                                tdList = htmlNode.Descendants().Where(x => (x.Name == "td")).ToList();
                                if (tdList != null && tdList.Count > 4 && decimal.TryParse(tdList[4].InnerText, out laborHourResult))
                                {
                                    dr = dt.NewRow();
                                    dr["CarExtId"] = carExtId;
                                    dr["OemComponentId"] = oemComponentId;
                                    dr["LaborSkillLevel"] = tdList[2].InnerText;
                                    dr["LaborHour"] = laborHourResult;
                                    dt.Rows.Add(dr);

                                    return;
                                }
                            }
                        }
                    }

                    dr = dt.NewRow();
                    dr["CarExtId"] = carExtId;
                    dr["OemComponentId"] = oemComponentId;
                    dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("LaborHour", articleBody.InnerHtml);
                    dt.Rows.Add(dr);

                    return;
                case 920: //Drive Belt
                case 926: //Fluid - A/T
                    foreach (HtmlNode htmlNode in laborList)
                    {
                        tdList = htmlNode.Descendants().Where(x => (x.Name == "td")).ToList();
                        if (tdList != null && tdList.Count > 4 && decimal.TryParse(tdList[4].InnerText, out laborHourResult) && tdList[1].InnerText.ToUpper().Contains("REPLACE"))
                        {
                            dr = dt.NewRow();
                            dr["CarExtId"] = carExtId;
                            dr["OemComponentId"] = oemComponentId;
                            dr["LaborSkillLevel"] = tdList[2].InnerText;
                            dr["LaborHour"] = laborHourResult;
                            dt.Rows.Add(dr);

                            return;
                        }

                    }

                    divList = articleBody.Descendants().Where(x => (x.Name == "div" && x.Attributes["class"] != null && x.Attributes["class"].Value.Contains("operation"))).ToList();
                    if (divList != null && divList.Count > 0)
                    {
                        foreach (HtmlNode htmlNode in divList)
                        {
                            subList = htmlNode.Descendants().Where(x => (x.Name == "span" && x.Attributes["class"] != null && x.Attributes["class"].Value.Contains("operation_name"))).ToList();
                            if (subList != null && subList.Count == 1 && subList[0].InnerText.ToUpper().Contains("REPLACE"))
                            {
                                laborList = htmlNode.Descendants().Where(x => (x.Name == "tr" && x.Attributes["class"] != null && x.Attributes["class"].Value.Contains("labor"))).ToList();
                                if (laborList != null && laborList.Count > 0)
                                {
                                    tdList = laborList[0].Descendants().Where(x => (x.Name == "td")).ToList();

                                    if (tdList != null && tdList.Count > 4 && decimal.TryParse(tdList[4].InnerText, out laborHourResult))
                                    {
                                        dr = dt.NewRow();
                                        dr["CarExtId"] = carExtId;
                                        dr["OemComponentId"] = oemComponentId;
                                        dr["LaborSkillLevel"] = tdList[2].InnerText;
                                        dr["LaborHour"] = laborHourResult;
                                        dt.Rows.Add(dr);
                                        return;
                                    }
                                }
                                
                            }
                            
                        }
                    }

                    dr = dt.NewRow();
                    dr["CarExtId"] = carExtId;
                    dr["OemComponentId"] = oemComponentId;
                    dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("LaborHour", articleBody.InnerHtml);
                    dt.Rows.Add(dr);
                    
                    return;
                case 1039: //Ignition Cable
                case 3073: //Fluid - M/T
                    foreach (HtmlNode htmlNode in laborList)
                    {
                        tdList = htmlNode.Descendants().Where(x => (x.Name == "td")).ToList();
                        if (tdList != null && tdList.Count > 4 && decimal.TryParse(tdList[4].InnerText, out laborHourResult) && tdList[1].InnerText.ToUpper().Contains("REPLACE"))
                        {
                            dr = dt.NewRow();
                            dr["CarExtId"] = carExtId;
                            dr["OemComponentId"] = oemComponentId;
                            dr["LaborSkillLevel"] = tdList[2].InnerText;
                            dr["LaborHour"] = laborHourResult;
                            dt.Rows.Add(dr);
                            return;
                        }
                    }

                    dr = dt.NewRow();
                    dr["CarExtId"] = carExtId;
                    dr["OemComponentId"] = oemComponentId;
                    dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("LaborHour", articleBody.InnerHtml);
                    dt.Rows.Add(dr);

                    return;
                case 2056: //Fluid - Differential
                    foreach (HtmlNode htmlNode in laborList)
                    {
                        tdList = htmlNode.Descendants().Where(x => (x.Name == "td")).ToList();
                        if (tdList != null && tdList.Count > 4 && decimal.TryParse(tdList[4].InnerText, out laborHourResult) && tdList[1].InnerText.ToUpper().Contains("REAR"))
                        {
                            dr = dt.NewRow();
                            dr["CarExtId"] = carExtId;
                            dr["OemComponentId"] = oemComponentId;
                            dr["LaborSkillLevel"] = tdList[2].InnerText;
                            dr["LaborHour"] = laborHourResult;
                            dt.Rows.Add(dr);
                            return;
                        }
                    }

                    dr = dt.NewRow();
                    dr["CarExtId"] = carExtId;
                    dr["OemComponentId"] = oemComponentId;
                    dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("LaborHour", articleBody.InnerHtml);
                    dt.Rows.Add(dr);

                    return;
                default:
                    dr = dt.NewRow();
                    dr["CarExtId"] = carExtId;
                    dr["OemComponentId"] = oemComponentId;
                    dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("LaborHour", articleBody.InnerHtml);
                    dt.Rows.Add(dr);
                    return;
            } 
        }

    }
}
