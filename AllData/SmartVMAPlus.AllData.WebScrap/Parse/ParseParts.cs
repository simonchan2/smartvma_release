﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using HtmlAgilityPack;


namespace SmartVMAPlus.AllData.WebScrape.Parse
{
    public class ParseParts : IParse 
    {
        public void ParseHtml(string pageContent, string param, ref DataTable dt)
        {

            if (pageContent != null)
            {
                HtmlDocument htmlDoc = new HtmlDocument();

                htmlDoc.LoadHtml(pageContent);

                List<HtmlNode> articleList = htmlDoc.DocumentNode.Descendants().Where
                  (x => (x.Name == "dd" && x.Attributes["class"] != null &&
                  x.Attributes["class"].Value.Contains("article_body"))).ToList();

                string[] parameters = param.Split('|');
                int carExtId = int.Parse(parameters[0]);
                int oemComponentId = int.Parse(parameters[1]);
                DataRow dr;


                if (articleList != null && articleList.Count == 1)
                {
                    List<HtmlNode> partList = articleList[0].Descendants().Where
                        (x => (x.Name == "tr" && x.Attributes["class"] != null &&
                        x.Attributes["class"].Value.Contains("part"))).ToList();

                    if (partList != null && partList.Count == 1)
                    {
                        List<HtmlNode> tdList = partList[0].Descendants().Where(x => (x.Name == "td")).ToList();

                        dr = dt.NewRow();
                        dr["CarExtId"] = carExtId;
                        dr["OemComponentId"] = oemComponentId;
                        dr["OemPartNo"] = tdList[2].InnerText;
                        dr["OemPartName"] = tdList[1].InnerText;
                        decimal result;
                        if (tdList.Count > 3)
                        {
                            if (decimal.TryParse(tdList[3].InnerText, out result))
                                dr["Quantity"] = result == -1 || result > 1000 ? 1 : result;
                            else
                                dr["Quantity"] = 1;
                        }
                        if (tdList.Count > 4)
                        {
                            if (decimal.TryParse(tdList[4].InnerText, System.Globalization.NumberStyles.Currency, null, out result))
                                dr["UnitPrice"] = result;
                            if (result <= 0)
                                dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("Parts", articleList[0].InnerHtml);
                        }
                        if (oemComponentId == 27)
                        {
                            List<HtmlNode> vehicleList = htmlDoc.DocumentNode.Descendants().Where
                                (x => (x.Name == "a" && x.Attributes["class"] != null &&
                                x.Attributes["class"].Value.Contains("vehicle"))).ToList();

                            if (vehicleList != null && vehicleList.Count > 0)
                            {
                                Nullable<byte> NoOfCylinder = RegularExpression.GetNumberOfCylinders(vehicleList[0].InnerText);
                                if (NoOfCylinder != null)
                                    dr["Quantity"] = (int)NoOfCylinder;
                            }
                        }


                        dt.Rows.Add(dr);

                    }
                    else
                    {
                        HandleMultiLineParts(carExtId, oemComponentId, partList, articleList[0], ref dt);
                    }

                }
                else
                {
                    List<HtmlNode> contentList = htmlDoc.DocumentNode.Descendants().Where
                      (x => (x.Name == "ul" && x.Attributes["class"] != null &&
                      x.Attributes["class"].Value.Contains("article"))).ToList();

                    dr = dt.NewRow();
                    dr["CarExtId"] = carExtId;
                    dr["OemComponentId"] = oemComponentId;
                    if (contentList != null && contentList.Count > 0)
                        dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("Parts", contentList[0].OuterHtml);
                    else
                        dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("Parts", "No Message");
                    dt.Rows.Add(dr);
                }
                

            }

        }

        private void HandleMultiLineParts(int carExtId, int oemComponentId, List<HtmlNode> partList, HtmlNode articleBody, ref DataTable dt)
        {
            DataRow dr;
            List<HtmlNode> tdList;
            decimal quantityResult;
            decimal unitPriceResult;

            switch (oemComponentId)
            {
                case 19: //Fuel Filter
                case 21: //Brake Pad
                case 64: //Timing Belt
                case 89: //Timing Belt Tensioner
                case 95: //Drive Belt Tensioner
                case 195: //Fluid Filter - A/T
                case 366: //Air Filter Element
                case 381: //Positive Crankcase Ventilation Valve
                case 386: //Evaporative Emission Control Canister
                case 484: //Oil Filter, Engine
                case 920: //Drive Belt
                case 1086: //Balance Shaft Belt
                case 2353: //Tire Pressure Sensor
                case 3283: //Cabin Air Filter / Purifier
                    foreach (HtmlNode htmlNode in partList)
                    {
                        tdList = htmlNode.Descendants().Where(x => (x.Name == "td")).ToList();
                        if (tdList != null && tdList.Count > 4)
                        {
                            if (decimal.TryParse(tdList[4].InnerText, System.Globalization.NumberStyles.Currency, null, out unitPriceResult))
                            {
                                if (unitPriceResult > 0)
                                {
                                    dr = dt.NewRow();
                                    dr["CarExtId"] = carExtId;
                                    dr["OemComponentId"] = oemComponentId;

                                    dr["OemPartNo"] = tdList[2].InnerText;
                                    dr["OemPartName"] = tdList[1].InnerText;

                                    if (decimal.TryParse(tdList[3].InnerText, out quantityResult))
                                        dr["Quantity"] = quantityResult == -1 || quantityResult > 1000 ? 1 : quantityResult;
                                    else
                                        dr["Quantity"] = 1;
                                    dr["UnitPrice"] = unitPriceResult;

                                    dt.Rows.Add(dr);
                                    return;
                                }
                                
                            }
                        }
                       
                    }

                    dr = dt.NewRow();
                    dr["CarExtId"] = carExtId;
                    dr["OemComponentId"] = oemComponentId;
                    dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("Parts", articleBody.InnerHtml);
                    dt.Rows.Add(dr);

                    return;
                case 745: //Wiper Blade
                    dr = dt.NewRow();
                    dr["CarExtId"] = carExtId;
                    dr["OemComponentId"] = oemComponentId;
                    dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("Parts", articleBody.InnerHtml);
                    dt.Rows.Add(dr);

                    return;
                
                case 1039: //Ignition Cable
                    foreach (HtmlNode htmlNode in partList)
                    {
                        tdList = htmlNode.Descendants().Where(x => (x.Name == "td")).ToList();
                        if (tdList[1].InnerText.ToUpper().Contains("CABLE SET"))
                        {
                            if (tdList != null && tdList.Count > 4)
                            {
                                dr = dt.NewRow();
                                dr["CarExtId"] = carExtId;
                                dr["OemComponentId"] = oemComponentId;
                                dr["OemPartNo"] = tdList[2].InnerText;
                                dr["OemPartName"] = tdList[1].InnerText;
                                if (tdList.Count > 3)
                                {
                                    if (decimal.TryParse(tdList[3].InnerText, out quantityResult))
                                        dr["Quantity"] = quantityResult == -1 || quantityResult > 1000 ? 1 : quantityResult;
                                    else
                                        dr["Quantity"] = 1;
                                }
                                if (tdList.Count > 4)
                                {
                                    if (decimal.TryParse(tdList[4].InnerText, System.Globalization.NumberStyles.Currency, null, out unitPriceResult))
                                    {
                                        dr["UnitPrice"] = unitPriceResult;
                                        if (unitPriceResult <= 0)
                                            dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("Parts", articleBody.InnerHtml);
                                    }
                                }
                                dt.Rows.Add(dr);
                                return;

                            }
                            else //Save as exception for analysis
                            {
                                dr = dt.NewRow();
                                dr["CarExtId"] = carExtId;
                                dr["OemComponentId"] = oemComponentId;
                                dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("Parts", articleBody.InnerHtml);
                                dt.Rows.Add(dr);

                                return;
                            }
                        }
                    
                    }

                    foreach (HtmlNode htmlNode in partList)
                    {
                        tdList = htmlNode.Descendants().Where(x => (x.Name == "td")).ToList();
                        if (tdList != null && tdList.Count > 4)
                        {
                            if (decimal.TryParse(tdList[4].InnerText, System.Globalization.NumberStyles.Currency, null, out unitPriceResult))
                            {

                                dr = dt.NewRow();
                                dr["CarExtId"] = carExtId;
                                dr["OemComponentId"] = oemComponentId;
                                dr["OemPartNo"] = tdList[2].InnerText;
                                dr["OemPartName"] = tdList[1].InnerText;
                                if (decimal.TryParse(tdList[3].InnerText, out quantityResult))
                                    dr["Quantity"] = quantityResult == -1 || quantityResult > 1000 ? 1 : quantityResult;
                                else
                                    dr["Quantity"] = 1;
                                dr["UnitPrice"] = unitPriceResult;

                                if (unitPriceResult <= 0)
                                    dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("Parts", articleBody.InnerHtml);

                                dt.Rows.Add(dr);
                                return;

                            }
                        }

                    }

                    dr = dt.NewRow();
                    dr["CarExtId"] = carExtId;
                    dr["OemComponentId"] = oemComponentId;
                    dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("Parts", articleBody.InnerHtml);
                    dt.Rows.Add(dr);

                    return;
                case 1962: //Seat Temperature Element
                    foreach (HtmlNode htmlNode in partList)
                    {
                        tdList = htmlNode.Descendants().Where(x => (x.Name == "td")).ToList();
                        if (tdList != null && tdList.Count > 0)
                        {
                            if (tdList[1].InnerText.ToUpper().Contains("FRONT SEAT") )
                            { 
                                dr = dt.NewRow();
                                dr["CarExtId"] = carExtId;
                                dr["OemComponentId"] = oemComponentId;
                                dr["OemPartNo"] = tdList[2].InnerText;
                                dr["OemPartName"] = tdList[1].InnerText;
                                if (tdList.Count > 3)
                                {
                                    if (decimal.TryParse(tdList[3].InnerText, out quantityResult))
                                        dr["Quantity"] = quantityResult == -1 || quantityResult > 1000 ? 1 : quantityResult;
                                    else
                                        dr["Quantity"] = 1;
                                }
                                if (tdList.Count > 4)
                                {
                                    if (decimal.TryParse(tdList[4].InnerText, System.Globalization.NumberStyles.Currency, null, out unitPriceResult))
                                    {
                                        dr["UnitPrice"] = unitPriceResult;

                                        if (unitPriceResult <= 0)
                                            dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("Parts", articleBody.InnerHtml);
                                    }
                                }
                                dt.Rows.Add(dr);
                                return;
                                
                            }
                        }

                    }

                    dr = dt.NewRow();
                    dr["CarExtId"] = carExtId;
                    dr["OemComponentId"] = oemComponentId;
                    dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("Parts", articleBody.InnerHtml);
                    dt.Rows.Add(dr);

                    return;

                default:
                    dr = dt.NewRow();
                    dr["CarExtId"] = carExtId;
                    dr["OemComponentId"] = oemComponentId;
                    dr["ExceptionMessage"] = SmartVMAPlus.AllData.WebScrape.Processes.CreateException.BuildHtml("Parts", articleBody.InnerHtml);
                    dt.Rows.Add(dr);
                    return;
            }
        }

    }
}
