﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using HtmlAgilityPack;



namespace SmartVMAPlus.AllData.WebScrape.Parse
{
    public class ParseMake :IParse
    {
        public void ParseHtml(string pageContent, string param, ref DataTable dt)
        {

            if (pageContent != null)
            {
                HtmlDocument htmlDoc = new HtmlDocument();

                htmlDoc.LoadHtml(pageContent);

                List<HtmlNode> makeList = htmlDoc.DocumentNode.Descendants().Where
                  (x => (x.Name == "a" && x.Attributes["class"] != null &&
                  x.Attributes["class"].Value.Contains("models_list"))).ToList();

                if (makeList != null && makeList.Count > 0)
                {
                    if (dt == null)
                    {
                        dt = new DataTable();

                        dt.Columns.Add("Year", typeof(Int32));
                        dt.Columns.Add("MakeExtId", typeof(Int32));
                        dt.Columns.Add("Make", typeof(string));
                    }

                    int makeExtId = -1;
                    int year = int.Parse(param);
                    string make = "";
                    string href = "";
                    DataRow dr;


                    foreach (HtmlNode htmlNode in makeList)
                    {
                        href = htmlNode.Attributes["href"].Value;
                        makeExtId = int.Parse(href.Split('/')[5]);
                        make = System.Net.WebUtility.HtmlDecode(htmlNode.InnerText);

                        dr = dt.NewRow();
                        dr["MakeExtId"] = makeExtId;
                        dr["year"] = year;
                        dr["Make"] = make;
                        dt.Rows.Add(dr);
                    }
                }
    

            }

        }
    }
}
