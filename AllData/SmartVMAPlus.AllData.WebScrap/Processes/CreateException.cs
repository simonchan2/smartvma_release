﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartVMAPlus.AllData.WebScrape.Processes
{
    public static class CreateException
    {
        public static string BuildHtml(string name, string exception)
        {
            if (exception.Contains("404"))
                return "<div><strong>" + name + "</strong></div><div>" + name + " is not exist.</div>";
            else
                return "<div><strong>" + name + "</strong></div><div>" + exception + "</div>";
        }

    }
}
