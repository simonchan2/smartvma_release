﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using SmartVMAPlus.AllData.WebScrape.Models;
using SmartVMAPlus.AllData.WebScrape.Parse;

namespace SmartVMAPlus.AllData.WebScrape.Processes
{
    public class ScrapeMake : IScrape
    {
        public DataTable ScrapeWeb(RequestInfo requestInfo, ref WebClientSession clientSession, ref DataTable yearDt)
        {
            DataTable makeDt = null;

            AllDataRequest allData = new AllDataRequest();

            Dictionary<string, string> queryParams;

            string requestPath;
            string pageContent;

            if (yearDt != null && yearDt.Rows != null && yearDt.Rows.Count > 0)
            {
                requestInfo.RequestType = RequestType.Make;

                foreach (DataRow yearRow in yearDt.Rows)
                {
                    queryParams = new Dictionary<string, string>();
                    queryParams.Add("years", yearRow["year"].ToString());

                    requestPath = GetRequestPath.GetPathByRequestType(requestInfo.RequestType, queryParams);

                    try
                    {
                        pageContent = allData.UrlRequest(requestInfo.BaseUrl + requestPath, clientSession);

                        ParseMake parse = new ParseMake();

                        parse.ParseHtml(pageContent, yearRow["Year"].ToString(), ref makeDt);
                    }
                    catch (Exception ex)
                    {
                        //Log into exception (under contruction)
                    }
                }

            }

            return makeDt;
        }
    }
}
