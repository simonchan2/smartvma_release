﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using SmartVMAPlus.AllData.WebScrape.Models;

namespace SmartVMAPlus.AllData.WebScrape.Processes
{
    public interface IScrape
    {
        DataTable ScrapeWeb(RequestInfo requestInfo, ref WebClientSession clientSession, ref DataTable parentDataTable);
    }
}
