﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using SmartVMAPlus.AllData.WebScrape.Models;
using SmartVMAPlus.AllData.WebScrape.Parse;
using System.Threading;

namespace SmartVMAPlus.AllData.WebScrape.Processes
{
    public class ScrapeService : IScrape 
    {
        public DataTable ScrapeWeb(RequestInfo requestInfo, ref WebClientSession clientSession, ref DataTable menuDt)
        {
            DataTable serviceDt = null;

            AllDataRequest allData = new AllDataRequest();

            Dictionary<string, string> queryParams;

            string requestPath;
            string pageContent;

            if (menuDt != null && menuDt.Rows != null && menuDt.Rows.Count > 0)
            {

                foreach (DataRow menuRow in menuDt.Rows)
                {
                    requestInfo.RequestType = RequestType.Service;

                    ParseService_XPath parse = new ParseService_XPath();

                    queryParams = new Dictionary<string, string>();
                    queryParams.Add("carids", menuRow["CarExtId"].ToString());
                    queryParams.Add("menuComponents", menuRow["OemComponentId"].ToString());
                    queryParams.Add("itypes", menuRow["itype"].ToString());
                    queryParams.Add("intervalUrl", menuRow["IntervalUrl"].ToString());

                    requestPath = GetRequestPath.GetPathByRequestType(requestInfo.RequestType, queryParams);

                    try
                    {
                        pageContent = allData.UrlRequest(requestInfo.BaseUrl + requestPath, clientSession);

                        parse.ParseHtml(pageContent, menuRow["itype"].ToString() + "|" + menuRow["Interval"].ToString() + "|" + requestInfo.OperationScope, ref serviceDt);
                    }
                    catch (Exception ex)
                    {
                        //build serviceDt here to pass the exception info.
                    }


                    if ((DateTime.Now - GetRequestPath.SessionStartTime).TotalMinutes > 30)
                    {
                        clientSession.DownloadData(requestInfo.BaseUrl + requestInfo.LogoutPath);
                        Thread.Sleep(120000); //120 seconds
                        clientSession = allData.Login(requestInfo.BaseUrl + requestInfo.LoginPath, requestInfo.UserId, requestInfo.Password);
                        GetRequestPath.SessionStartTime = DateTime.Now;
                    }
                }

            }

            return serviceDt;
        }
    }
}
