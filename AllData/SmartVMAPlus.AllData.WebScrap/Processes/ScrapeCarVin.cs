﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using SmartVMAPlus.AllData.WebScrape.Models;
using SmartVMAPlus.AllData.WebScrape.Parse;
using System.Threading;

namespace SmartVMAPlus.AllData.WebScrape.Processes
{
    public class ScrapeCarVin : IScrape 
    {
        public DataTable ScrapeWeb(RequestInfo requestInfo, ref WebClientSession clientSession, ref DataTable inputDt)
        {
            DataTable outputDt = null;

            AllDataRequest allData = new AllDataRequest();

            Dictionary<string, string> queryParams;

            string requestPath;
            string pageContent;

            if (inputDt != null && inputDt.Rows != null && inputDt.Rows.Count > 0)
            {

                outputDt = new DataTable();

                outputDt.Columns.Add("VIN", typeof(string));
                outputDt.Columns.Add("CarExtId", typeof(Int32));
                outputDt.Columns.Add("Sequence", typeof(Int32));
                outputDt.Columns.Add("Comment", typeof(String));

                DataRow dr;

                requestInfo.RequestType = RequestType.CarVin;

                foreach (DataRow inputRow in inputDt.Rows)
                {
                    queryParams = new Dictionary<string, string>();
                    queryParams.Add("vins", inputRow["VIN"].ToString());

                    requestPath = GetRequestPath.GetPathByRequestType(requestInfo.RequestType, queryParams);

                    try
                    {

                        clientSession.AllowAutoRedirect = true;

                        pageContent = allData.UrlRequest(requestInfo.BaseUrl + requestPath, clientSession);

                        IParse parse = (IParse)Activator.CreateInstance(typeof(SmartVMAPlus.AllData.WebScrape.Parse.ParseCarVin));

                        parse.ParseHtml(pageContent, "SingleVin|" + inputRow["VIN"].ToString(), ref outputDt);

                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.Contains("300"))
                        {
                            try
                            {
                                clientSession.AllowAutoRedirect = false;
                                pageContent = allData.UrlRequest(requestInfo.BaseUrl + requestPath, clientSession);

                                IParse parse = (IParse)Activator.CreateInstance(typeof(SmartVMAPlus.AllData.WebScrape.Parse.ParseCarVin));

                                parse.ParseHtml(pageContent, "MultiVin|" + inputRow["VIN"].ToString(), ref outputDt);
                            }
                            catch (Exception ex1)
                            {
                                dr = outputDt.NewRow();
                                dr["VIN"] = inputRow["VIN"].ToString();
                                dr["Sequence"] = 1;
                                dr["Comment"] = ex1.Message;

                                outputDt.Rows.Add(dr);
                            }
                        }
                        else
                        {
                            dr = outputDt.NewRow();
                            dr["VIN"] = inputRow["VIN"].ToString();
                            dr["Sequence"] = 1;
                            dr["Comment"] = ex.Message;

                            outputDt.Rows.Add(dr);
                        }

                    }
                    if ((DateTime.Now - GetRequestPath.SessionStartTime).TotalMinutes > 30)
                    {
                        clientSession.DownloadData(requestInfo.BaseUrl + requestInfo.LogoutPath);
                        Thread.Sleep(120000); //120 seconds
                        clientSession = allData.Login(requestInfo.BaseUrl + requestInfo.LoginPath, requestInfo.UserId, requestInfo.Password);
                        GetRequestPath.SessionStartTime = DateTime.Now;
                    }
                }

            }

            return outputDt;
        }
    }
}
