﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using SmartVMAPlus.AllData.WebScrape.Models;
using SmartVMAPlus.AllData.WebScrape.Parse;
using System.Threading;

namespace SmartVMAPlus.AllData.WebScrape.Processes
{
    public class ScrapeCar : IScrape 
    {
        public DataTable ScrapeWeb(RequestInfo requestInfo, ref WebClientSession clientSession, ref DataTable engineDt)
        {
            DataTable carDt = null;

            AllDataRequest allData = new AllDataRequest();

           

            Dictionary<string, string> queryParams;

            string requestPath;
            string pageContent;

            if (engineDt != null && engineDt.Rows != null && engineDt.Rows.Count > 0)
            {
                requestInfo.RequestType = RequestType.Car;

                foreach (DataRow engineRow in engineDt.Rows)
                {
                    queryParams = new Dictionary<string, string>();
                    queryParams.Add("years", engineRow["Year"].ToString());
                    queryParams.Add("makes", engineRow["MakeExtId"].ToString());
                    queryParams.Add("models", engineRow["ModelExtId"].ToString());
                    queryParams.Add("engines", engineRow["EngineExtId"].ToString());

                    requestPath = GetRequestPath.GetPathByRequestType(requestInfo.RequestType, queryParams);

                    try
                    {
                        pageContent = allData.UrlRequest(requestInfo.BaseUrl + requestPath, clientSession);

                        ParseCar parse = new ParseCar();

                        parse.ParseHtml(pageContent, engineRow["Year"].ToString() + "|" + engineRow["MakeExtId"].ToString() + "|" + engineRow["ModelExtId"].ToString() + "|" + engineRow["EngineExtId"].ToString(), ref carDt);
                    }
                    catch (Exception ex)
                    {
                        //Log into exception (under contruction)
                    }
                    if ((DateTime.Now - GetRequestPath.SessionStartTime).TotalMinutes > 30)
                    {
                        clientSession.DownloadData(requestInfo.BaseUrl + requestInfo.LogoutPath);
                        Thread.Sleep(120000); //120 seconds
                        clientSession = allData.Login(requestInfo.BaseUrl + requestInfo.LoginPath, requestInfo.UserId, requestInfo.Password);
                        GetRequestPath.SessionStartTime = DateTime.Now;
                    }
                }

            }

            return carDt;
        }
    }
}
