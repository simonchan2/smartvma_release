﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using SmartVMAPlus.AllData.WebScrape.Models;
using SmartVMAPlus.AllData.WebScrape.Parse;
using System.Threading;

namespace SmartVMAPlus.AllData.WebScrape.Processes
{
    public class ScrapeEngine : IScrape
    {
        public DataTable ScrapeWeb(RequestInfo requestInfo, ref WebClientSession clientSession, ref DataTable modelDt)
        {

            DataTable engineDt = new DataTable();
            engineDt.Columns.Add("Year", typeof(Int32));
            engineDt.Columns.Add("MakeExtId", typeof(Int32));
            engineDt.Columns.Add("ModelExtId", typeof(Int32));
            engineDt.Columns.Add("EngineExtId", typeof(Int32));
            engineDt.Columns.Add("Engine", typeof(string));
            engineDt.Columns.Add("NumberOfCylinders", typeof(Int16));
            //engineDt.Columns.Add("Type", typeof(string));
            //engineDt.Columns.Add("HttpError", typeof(string));
            //engineDt.Columns.Add("Url", typeof(string));
            //engineDt.Columns.Add("Message", typeof(string));

            AllDataRequest allData = new AllDataRequest();

          

            Dictionary<string, string> queryParams;

            string requestPath;
            string pageContent;

            if (modelDt != null && modelDt.Rows != null && modelDt.Rows.Count > 0)
            {
                requestInfo.RequestType = RequestType.Engine;

                foreach (DataRow modelRow in modelDt.Rows)
                {
                  
                    queryParams = new Dictionary<string, string>();
                    queryParams.Add("years", modelRow["Year"].ToString());
                    queryParams.Add("makes", modelRow["MakeExtId"].ToString());
                    queryParams.Add("models", modelRow["ModelExtId"].ToString());

                    requestPath = GetRequestPath.GetPathByRequestType(requestInfo.RequestType, queryParams);

                    try
                    {
                        pageContent = allData.UrlRequest(requestInfo.BaseUrl + requestPath, clientSession);

                        ParseEngine parse = new ParseEngine();

                        parse.ParseHtml(pageContent, modelRow["Year"].ToString() + "|" + modelRow["MakeExtId"].ToString() + "|" + modelRow["ModelExtId"].ToString(), ref engineDt);
                    }
                    catch (Exception ex)
                    {
                        ////Log into exception (under contruction)
                        //DataRow dr = engineDt.NewRow();
                        //dr["MakeExtId"] = modelRow["MakeExtId"];
                        //dr["year"] = modelRow["Year"];
                        //dr["ModelExtId"] = modelRow["ModelExtId"];
                        //dr["Type"] = "Engine";
                        //dr["HttpError"] = RegularExpression.ParseHttpErrorCode(ex.Message);
                        //dr["Url"] = requestPath;
                        //dr["Message"] = ex.Message;

                        //engineDt.Rows.Add(dr);
                    }
                    if ((DateTime.Now - GetRequestPath.SessionStartTime).TotalMinutes > 30)
                    {
                        clientSession.DownloadData(requestInfo.BaseUrl + requestInfo.LogoutPath);
                        Thread.Sleep(120000); //120 seconds
                        clientSession = allData.Login(requestInfo.BaseUrl + requestInfo.LoginPath, requestInfo.UserId, requestInfo.Password);
                        GetRequestPath.SessionStartTime = DateTime.Now;
                    }
                }

            }

            return engineDt;
        }
    }
}
