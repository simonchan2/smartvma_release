﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using SmartVMAPlus.AllData.WebScrape.Models;
using SmartVMAPlus.AllData.WebScrape.Parse;
using System.Threading;

namespace SmartVMAPlus.AllData.WebScrape.Processes
{
    public class ScrapeLabor : IScrape
    {
        public DataTable ScrapeWeb(RequestInfo requestInfo, ref WebClientSession clientSession, ref DataTable inputDt)
        {
            DataTable outputDt = null;

            AllDataRequest allData = new AllDataRequest();

            Dictionary<string, string> queryParams;

            string requestPath;
            string pageContent;

            if (inputDt != null && inputDt.Rows != null && inputDt.Rows.Count > 0)
            {
                outputDt = new DataTable();

                outputDt.Columns.Add("CarExtId", typeof(Int32));
                outputDt.Columns.Add("OemComponentId", typeof(Int32));
                outputDt.Columns.Add("LaborSkillLevel", typeof(string));
                outputDt.Columns.Add("MfgWarranty", typeof(decimal));
                outputDt.Columns.Add("LaborHour", typeof(decimal));
                outputDt.Columns.Add("ExceptionMessage", typeof(string));

                DataTable subComponentDt = null;

                string subOemComponentId;


                DataRow dr;

                foreach (DataRow inputRow in inputDt.Rows)
                {
                    if (int.Parse(inputRow["OemComponentId"].ToString()) != 89 && int.Parse(inputRow["OemComponentId"].ToString()) != 95 && int.Parse(inputRow["OemComponentId"].ToString()) != 484)
                    {
                        subOemComponentId = "0";

                       

                        queryParams = new Dictionary<string, string>();
                        queryParams.Add("carids", inputRow["CarExtId"].ToString());
                        queryParams.Add("components", inputRow["OemComponentId"].ToString());
                        queryParams.Add("itypes", requestInfo.LaborItypeId.ToString());


                        try
                        {
                            if (inputRow["OemComponentId"].ToString() == "9")
                            {
                                subComponentDt = outputDt.Clone();

                                requestInfo.RequestType = RequestType.Component;

                                ParseComponent parseComponent = new ParseComponent();

                                requestPath = GetRequestPath.GetPathByRequestType(requestInfo.RequestType, queryParams);

                                pageContent = allData.UrlRequest(requestInfo.BaseUrl + requestPath, clientSession);

                                parseComponent.ParseHtml(pageContent, inputRow["CarExtId"].ToString() + "|" + inputRow["OemComponentId"].ToString(), ref subComponentDt);

                                if (subComponentDt != null && subComponentDt.Rows.Count > 0)
                                {
                                    if (subComponentDt.Rows[0]["ExceptionMessage"] != null && subComponentDt.Rows[0]["ExceptionMessage"].ToString() != "")
                                    {
                                        dr = outputDt.NewRow();
                                        dr["CarExtId"] = int.Parse(inputRow["CarExtId"].ToString());
                                        dr["OemComponentId"] = int.Parse(inputRow["OemComponentId"].ToString());
                                        dr["ExceptionMessage"] = subComponentDt.Rows[0]["ExceptionMessage"].ToString();

                                        outputDt.Rows.Add(dr);
                                    }
                                    else
                                    {
                                        subOemComponentId = subComponentDt.Rows[0]["OemComponentId"].ToString();
                                        queryParams.Remove("components");
                                        queryParams.Add("components", subOemComponentId);

                                    }

                                }
                            }

                            requestInfo.RequestType = RequestType.Labor;

                            ParseLabor parse = new ParseLabor();

                            requestPath = GetRequestPath.GetPathByRequestType(requestInfo.RequestType, queryParams);

                            pageContent = allData.UrlRequest(requestInfo.BaseUrl + requestPath, clientSession);

                            parse.ParseHtml(pageContent, inputRow["CarExtId"].ToString() + "|" + inputRow["OemComponentId"].ToString() + "|" + subOemComponentId, ref outputDt);

                        }
                        catch (Exception ex)
                        {
                            
                            dr = outputDt.NewRow();
                            dr["CarExtId"] = int.Parse(inputRow["CarExtId"].ToString());
                            dr["OemComponentId"] = int.Parse(inputRow["OemComponentId"].ToString());
                            dr["ExceptionMessage"] = CreateException.BuildHtml("LaborHour", ex.Message);

                            outputDt.Rows.Add(dr);
                           
                        }

                        if ((DateTime.Now - GetRequestPath.SessionStartTime).TotalMinutes > 30)
                        {
                            clientSession.DownloadData(requestInfo.BaseUrl + requestInfo.LogoutPath);
                            Thread.Sleep(120000); //120 seconds
                            clientSession = allData.Login(requestInfo.BaseUrl + requestInfo.LoginPath, requestInfo.UserId, requestInfo.Password);
                            GetRequestPath.SessionStartTime = DateTime.Now;
                        }
                    }
                }

            }

            return outputDt;
        }
    }
}
