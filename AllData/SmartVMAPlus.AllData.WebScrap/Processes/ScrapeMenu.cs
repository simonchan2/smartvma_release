﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using SmartVMAPlus.AllData.WebScrape.Models;
using SmartVMAPlus.AllData.WebScrape.Parse;
using System.Threading;

namespace SmartVMAPlus.AllData.WebScrape.Processes
{
    public class ScrapeMenu : IScrape 
    {
        public DataTable ScrapeWeb(RequestInfo requestInfo, ref WebClientSession clientSession, ref DataTable carDt)
        {
            DataTable menuDt = null;

            AllDataRequest allData = new AllDataRequest();

            Dictionary<string, string> queryParams;

            string requestPath;
            string pageContent;

            if (carDt != null && carDt.Rows != null && carDt.Rows.Count > 0)
            {
                requestInfo.RequestType = RequestType.Menu;

                int severeCount = 0;
                int normalCount = 0;

                DataTable severeDt;
                DataTable normalDt;

                menuDt = new DataTable();

                menuDt.Columns.Add("CarExtId", typeof(Int32));
                menuDt.Columns.Add("Interval", typeof(Int32));
                menuDt.Columns.Add("Mileage", typeof(string));
                menuDt.Columns.Add("Miles", typeof(Int32));
                menuDt.Columns.Add("Kilometers", typeof(Int32));
                menuDt.Columns.Add("OemComponentId", typeof(Int32));
                menuDt.Columns.Add("iType", typeof(Int32));
                menuDt.Columns.Add("IntervalUrl", typeof(string));

               

                foreach (DataRow carRow in carDt.Rows)
                {
                    severeDt = menuDt.Clone();
                    normalDt = menuDt.Clone();

                    severeCount = 0;
                    normalCount = 0;

                    ParseMenu parse = new ParseMenu();

                    queryParams = new Dictionary<string, string>();
                    queryParams.Add("carids", carRow["CarExtId"].ToString());
                    queryParams.Add("components", requestInfo.MenuComponentId.ToString());
                    queryParams.Add("itypes", requestInfo.SevereSvcItypeId.ToString());

                    requestPath = GetRequestPath.GetPathByRequestType(requestInfo.RequestType, queryParams);

                    try
                    {
                        pageContent = allData.UrlRequest(requestInfo.BaseUrl + requestPath, clientSession);

                        parse.ParseHtml(pageContent, "", ref severeDt);

                        if (severeDt.Rows != null)
                            severeCount = severeDt.Rows.Count;
                    }
                    catch
                    {
                        //do nothing if page error
                    }

                    queryParams.Remove("itypes");
                    queryParams.Add("itypes", requestInfo.NormalSvcItypeId.ToString());

                    requestPath = GetRequestPath.GetPathByRequestType(requestInfo.RequestType, queryParams);

                    try
                    {
                        pageContent = allData.UrlRequest(requestInfo.BaseUrl + requestPath, clientSession);

                        parse.ParseHtml(pageContent, "", ref normalDt);

                        if (normalDt.Rows != null)
                            normalCount = normalDt.Rows.Count;
                    }
                    catch
                    {
                        //do nothing if page error
                    }

                    if (severeCount > 0 || normalCount > 0)
                    {
                        if (severeCount >= normalCount)
                            AddRowsToMenu(severeDt, ref menuDt);
                        else
                            AddRowsToMenu(normalDt, ref menuDt);

                    }


                    if ((DateTime.Now - GetRequestPath.SessionStartTime).TotalMinutes > 30)
                    {
                        clientSession.DownloadData(requestInfo.BaseUrl + requestInfo.LogoutPath);
                        Thread.Sleep(120000); //120 seconds
                        clientSession = allData.Login(requestInfo.BaseUrl + requestInfo.LoginPath, requestInfo.UserId, requestInfo.Password);
                        GetRequestPath.SessionStartTime = DateTime.Now;
                    }
                }

            }

            return menuDt;
        }

        private void AddRowsToMenu(DataTable inputDt, ref DataTable outputDt)
        {
            DataRow dr;
            foreach (DataRow dr1 in inputDt.Rows)
            {
                dr = outputDt.NewRow();
                dr["CarExtId"] = dr1["CarExtId"];
                dr["Interval"] = dr1["Interval"];
                dr["Mileage"] = dr1["Mileage"];
                dr["Miles"] = dr1["Miles"];
                dr["Kilometers"] = dr1["Kilometers"];
                dr["OemComponentId"] = dr1["OemComponentId"];
                dr["iType"] = dr1["iType"];
                dr["IntervalUrl"] = dr1["IntervalUrl"];
                outputDt.Rows.Add(dr);
            }
        }
    }
}
