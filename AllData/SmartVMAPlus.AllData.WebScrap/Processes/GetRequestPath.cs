﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartVMAPlus.AllData.WebScrape.Models;

namespace SmartVMAPlus.AllData.WebScrape.Processes
{
    public static class GetRequestPath
    {
        public static string GetPathByRequestType(RequestType requestType, Dictionary<string, string> queryParams)
        {
            string yearValue = "";
            string makeValue = "";
            string modelValue = "";
            string engineValue = "";
            string carValue = "";
            string componentValue = "";
            string itypeValue = "";
            string intervalUrlValue = "";
            string vinValue = "";
       

            switch (requestType)
            {
                case RequestType.Year:
                    return "/" + "years";
                case RequestType.Make:
                    queryParams.TryGetValue("years", out yearValue);
                    return "/years/" + yearValue;
                case RequestType.Model:
                    queryParams.TryGetValue("years", out yearValue);
                    queryParams.TryGetValue("makes", out makeValue);
                    return "/years/" + yearValue + "/makes/" + makeValue;
                case RequestType.Engine:
                    queryParams.TryGetValue("years", out yearValue);
                    queryParams.TryGetValue("makes", out makeValue);
                    queryParams.TryGetValue("models", out modelValue);
                    return "/years/" + yearValue + "/makes/" + makeValue + "/models/" + modelValue;
                case RequestType.Car:
                    queryParams.TryGetValue("years", out yearValue);
                    queryParams.TryGetValue("makes", out makeValue);
                    queryParams.TryGetValue("models", out modelValue);
                    queryParams.TryGetValue("engines", out engineValue);
                    return "/years/" + yearValue + "/makes/" + makeValue + "/models/" + modelValue + "/engines/" + engineValue;
                case RequestType.Menu :
                case RequestType.Labor:
                case RequestType.Parts:
                case RequestType.Fluid:
                    queryParams.TryGetValue("carids", out carValue);
                    queryParams.TryGetValue("components", out componentValue);
                    queryParams.TryGetValue("itypes", out itypeValue);
                    return "/carids/" + carValue + "/components/" + componentValue + "/itypes/" + itypeValue;
                case RequestType.Service:
                    queryParams.TryGetValue("carids", out carValue);
                    queryParams.TryGetValue("menuComponents", out componentValue);
                    queryParams.TryGetValue("itypes", out itypeValue);
                    queryParams.TryGetValue("intervalUrl", out intervalUrlValue);
                    return "/carids/" + carValue + "/components/" + componentValue + "/itypes/" + itypeValue + intervalUrlValue;
                case RequestType.Component:
                    queryParams.TryGetValue("carids", out carValue);
                    queryParams.TryGetValue("components", out componentValue);
                    return "/carids/" + carValue + "/components/" + componentValue;
                case RequestType.CarVin:
                    queryParams.TryGetValue("vins", out vinValue);
                    return "/vins/" + vinValue + "/";
                default:
                    return "";
            }

            
        }

        public static DateTime SessionStartTime;
    }
}
