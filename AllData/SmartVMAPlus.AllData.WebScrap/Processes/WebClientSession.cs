﻿using System;
using System.Net;


namespace SmartVMAPlus.AllData.WebScrape.Processes
{
    public class WebClientSession : WebClient
    {
        private Uri _responseUri;
        private bool _allowAutoRedirect = true;
        public WebClientSession()
        {
            CookieContainer = new CookieContainer();
        }
        public CookieContainer CookieContainer { get; private set; }

        public bool AllowAutoRedirect
        {
            get
            {
                return _allowAutoRedirect;
            }
            set
            {
                _allowAutoRedirect = value;
            }
        }


        protected override WebRequest GetWebRequest(Uri address)
        {
            var request = (HttpWebRequest)base.GetWebRequest(address);
            request.CookieContainer = CookieContainer;
            request.AllowAutoRedirect = AllowAutoRedirect;
            return request;
        }

        public Uri ResponseUri
        {
            get { return _responseUri; }
        }

        protected override WebResponse GetWebResponse(WebRequest request)
        {
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string redirUrl = response.Headers["Location"];

            _responseUri = response.ResponseUri;

            return response;
        }

     

    }

    
}
