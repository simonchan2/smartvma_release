﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using SmartVMAPlus.AllData.WebScrape.Models;
using SmartVMAPlus.AllData.WebScrape.Parse;

namespace SmartVMAPlus.AllData.WebScrape.Processes
{
    public class ScrapeYear : IScrape
    {
        public DataTable ScrapeWeb(RequestInfo requestInfo, ref WebClientSession clientSession, ref DataTable parentDataTable)
        {
            //WebClientSession webClient = new WebClientSession();
            //webClient.Login(requestInfo.baseUrl + requestInfo.loginPath, requestInfo.userId, requestInfo.password);

            AllDataRequest allData = new AllDataRequest();

            Dictionary<string, string> queryParams = new Dictionary<string, string>();

            queryParams.Add("years", "");

            string requestPath = GetRequestPath.GetPathByRequestType(requestInfo.RequestType, queryParams);

            string pageContent = allData.UrlRequest(requestInfo.BaseUrl + requestPath, clientSession);

            ParseYear parse = new ParseYear();

            DataTable dt = null;

            parse.ParseHtml(pageContent, requestInfo.YearStart.ToString(), ref dt);

            return dt;
        }
    }
}
