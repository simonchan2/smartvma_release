﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using SmartVMAPlus.AllData.WebScrape.Models;
using SmartVMAPlus.AllData.WebScrape.Parse;

namespace SmartVMAPlus.AllData.WebScrape.Processes
{
    public class ScrapeModel : IScrape
    {
        public DataTable ScrapeWeb(RequestInfo requestInfo, ref WebClientSession clientSession, ref DataTable makeDt)
        {
            DataTable modelDt = null;

            AllDataRequest allData = new AllDataRequest();

            Dictionary<string, string> queryParams;

            string requestPath;
            string pageContent;

            if (makeDt != null && makeDt.Rows != null && makeDt.Rows.Count > 0)
            {
                requestInfo.RequestType = RequestType.Model;

                foreach (DataRow makeRow in makeDt.Rows)
                {
                    queryParams = new Dictionary<string, string>();
                    queryParams.Add("years", makeRow["Year"].ToString());
                    queryParams.Add("makes", makeRow["MakeExtId"].ToString());

                    requestPath = GetRequestPath.GetPathByRequestType(requestInfo.RequestType, queryParams);

                    try
                    {
                        pageContent = allData.UrlRequest(requestInfo.BaseUrl + requestPath, clientSession);

                        ParseModel parse = new ParseModel();

                        parse.ParseHtml(pageContent, makeRow["Year"].ToString() + "|" + makeRow["MakeExtId"].ToString(), ref modelDt);
                    }
                    catch (Exception ex)
                    {
                        //Log into exception (under contruction)
                    }

                }

            }

            return modelDt;
        }
    }
}
