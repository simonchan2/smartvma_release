﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartVMAPlus.AllData.WebScrape.Models;
using System.Collections.Specialized;

namespace SmartVMAPlus.AllData.WebScrape.Processes
{
    public class AllDataRequest
    {
       
        public WebClientSession Login(string url, string userId, string password)
        {
            WebClientSession clientSession = new WebClientSession();

            var values = new NameValueCollection
            {
                { "login", userId },
                { "password", password },
                // { "redirect_ref", "https://rest.alldataishop.com/v1/years" },
            };

            clientSession.UploadValues(url, values);

            return clientSession;
        }

        public string UrlRequest(string url, WebClientSession clientSession)
        {
            return clientSession.DownloadString(url);
        }

        public string GetResponseUrl(string url, WebClientSession clientSession)
        {
            clientSession.DownloadData(url);
            return clientSession.ResponseUri.ToString();
        }
     
    }
}
