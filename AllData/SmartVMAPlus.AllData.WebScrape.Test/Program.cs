﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartVMAPlus.AllData.WebScrape.Models;
using SmartVMAPlus.AllData.WebScrape.Processes;
using SmartVMAPlus.AllData.WebScrape.Parse;
using System.Data;

namespace SmartVMAPlus.AllData.WebScrape.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                RequestInfo requestInfo = new RequestInfo();

                requestInfo.BaseUrl = "https://rest.alldataishop.com/v1";
                requestInfo.LoginPath = "/login";
                requestInfo.LogoutPath = "/logout";
                requestInfo.RequestType = RequestType.Year;
                requestInfo.UserId = "5852691318";
                requestInfo.Password = "5852691318";
                requestInfo.YearStart = 2002;
                requestInfo.MenuComponentId = 3074;
                requestInfo.SevereSvcItypeId = 187;
                requestInfo.NormalSvcItypeId = 185;
                requestInfo.OperationScope = "Clean/Repack|Clean / Repack|Flush|Overhaul/Rebuild|Overhaul / Rebuild|Replace|Reseal|Reset|Rotate|Service or Charge|Tune-Up";
                requestInfo.LaborItypeId = 38;
                requestInfo.PartsItypeId = 12;
                requestInfo.FluidCapacityItypeId = 30;
                requestInfo.FluidTypeItypeId = 31;

                AllDataRequest allData = new AllDataRequest();
                WebClientSession clientSession = allData.Login(requestInfo.BaseUrl + requestInfo.LoginPath, requestInfo.UserId, requestInfo.Password);

                GetRequestPath.SessionStartTime = DateTime.Now;

                //DataTable parentDt = null;

                //DataTable yearDt = GetVehicleYear(requestInfo, ref clientSession, ref parentDt);

                //DataTable makeDt = GetVehicleMake(requestInfo, ref clientSession, ref yearDt);

                //yearDt.Dispose();
                //yearDt = null;

                ////show results for test
                //string makeExtId;
                //string year;
                //string make;
                //foreach (DataRow dr in makeDt.Rows)
                //{
                //    makeExtId = dr["MakeExtId"].ToString();
                //    year = dr["Year"].ToString();
                //    make = dr["Make"].ToString();
                //}

                ////------------------------------------------------------
                ////VehicleModel Test
                //DataTable dt = new DataTable();
                //dt.Columns.Add("Year", typeof(Int32));
                //dt.Columns.Add("MakeExtId", typeof(Int32));

                //DataRow dr = dt.NewRow();
                //dr["Year"] = 2015;
                //dr["MakeExtId"] = 13;
                //dt.Rows.Add(dr);


                //DataTable modelDt = GetVehicleModel(requestInfo, ref clientSession, ref dt);

                //dt.Dispose();
                //dt = null;

                ////show results for test
                //string makeExtId;
                //string year;
                //string modelExtId;
                //string model;

                //foreach (DataRow dr1 in modelDt.Rows)
                //{
                //    makeExtId = dr1["MakeExtId"].ToString();
                //    year = dr1["Year"].ToString();
                //    modelExtId = dr1["ModelExtId"].ToString();
                //    model = dr1["Model"].ToString();
                //}

                //GetRequestPath.SessionStartTime = DateTime.Now;
                //DataTable engineDt = GetVehicleEngine(requestInfo, ref clientSession, ref modelDt);

                //modelDt.Dispose();
                //modelDt = null;

                ////show results for test
                //string engineExtId;
                //string engine;

                //foreach (DataRow dr2 in engineDt.Rows)
                //{
                //    makeExtId = dr2["MakeExtId"].ToString();
                //    year = dr2["Year"].ToString();
                //    modelExtId = dr2["ModelExtId"].ToString();
                //    engineExtId = dr2["EngineExtId"].ToString();
                //    engine = dr2["Engine"].ToString();
                //}

                ////---------------------------------------------------------------------
                ////Car test

                //DataTable dt = new DataTable();
                //dt.Columns.Add("Year", typeof(Int32));
                //dt.Columns.Add("MakeExtId", typeof(Int32));
                //dt.Columns.Add("ModelExtId", typeof(Int32));
                //dt.Columns.Add("EngineExtId", typeof(Int32));

                //DataRow dr = dt.NewRow();
                //dr["Year"] = 2015;
                //dr["MakeExtId"] = 13;
                //dr["ModelExtId"] = 3440;
                //dr["EngineExtId"] = 4443;
                //dt.Rows.Add(dr);
                //DataTable carDt = GetVehicleCar(requestInfo, ref clientSession, ref dt);

                //dt.Dispose();
                //dt = null;

                ////show results for test
                //string carExtId;
                //string description;

                //foreach (DataRow dr in carDt.Rows)
                //{
                //    makeExtId = dr["MakeExtId"].ToString();
                //    year = dr["Year"].ToString();
                //    modelExtId = dr["ModelExtId"].ToString();
                //    engineExtId = dr["EngineExtId"].ToString();
                //    carExtId = dr["CarExtId"].ToString();
                //    description = dr["Description"].ToString();
                //}

                //------------------------------------------------------------------------
                ////menu test


                //DataTable dt = new DataTable();

                ////dt.Columns.Add("Year", typeof(Int32));
                ////dt.Columns.Add("MakeExtId", typeof(Int32));
                ////dt.Columns.Add("ModelExtId", typeof(Int32));
                ////dt.Columns.Add("EngineExtId", typeof(Int32));
                //dt.Columns.Add("CarExtId", typeof(Int32));
                ////dt.Columns.Add("Description", typeof(string));

                //DataRow dr = dt.NewRow();
                ////dr["MakeExtId"] = 11;
                ////dr["year"] = 2015;
                ////dr["ModelExtId"] = 2841;
                ////dr["EngineExtId"] = 4050;
                //dr["CarExtId"] = 55022;
                ////dr["Description"] = "2015 Chevy Truck Silverado 1500 2WD V6-4.3L";
                //dt.Rows.Add(dr);

                //dr = dt.NewRow();
                ////dr["MakeExtId"] = 6;
                ////dr["year"] = 2015;
                ////dr["ModelExtId"] = 5166;
                ////dr["EngineExtId"] = 4499;
                //dr["CarExtId"] = 39268;
                ////dr["Description"] = "2015 BMW 320i xDrive Sedan AWD (F30) L4-2.0L Turbo (N20)";
                //dt.Rows.Add(dr);

                //dr = dt.NewRow();
                ////dr["MakeExtId"] = 6;
                ////dr["year"] = 2015;
                ////dr["ModelExtId"] = 5166;
                ////dr["EngineExtId"] = 4499;
                //dr["CarExtId"] = 36686;
                ////dr["Description"] = "2015 BMW 320i xDrive Sedan AWD (F30) L4-2.0L Turbo (N20)";
                //dt.Rows.Add(dr);

                //GetRequestPath.SessionStartTime = DateTime.Now;
                //DataTable menuDt = GetMenu(requestInfo, ref clientSession, ref dt);

                //dt.Dispose();
                //dt = null;

                ////show results for test
                //string carExtId;
                //string interval;
                //string mileage;
                //string menuComponentId;
                //string miles;
                //string kilometers;
                //string iType;
                //string intervalUrl;

                //foreach (DataRow dr1 in menuDt.Rows)
                //{
                //    carExtId = dr1["CarExtId"].ToString();
                //    iType = dr1["iType"].ToString();
                //    interval = dr1["Interval"].ToString();
                //    mileage = dr1["Mileage"].ToString();
                //    menuComponentId = dr1["OemComponentId"].ToString();
                //    miles = dr1["Miles"].ToString();
                //    kilometers = dr1["Kilometers"].ToString();
                //    intervalUrl = dr1["IntervalUrl"].ToString();
                //}

                ////--------------------------------------------------------------------
                ////Service Test
                //DataTable dt = new DataTable();

                //dt.Columns.Add("CarExtId", typeof(Int32));
                //dt.Columns.Add("iType", typeof(Int32));
                //dt.Columns.Add("Interval", typeof(Int32));
                //dt.Columns.Add("OemComponentId", typeof(Int32));
                //dt.Columns.Add("IntervalUrl", typeof(string));


                //DataRow dr = dt.NewRow();
                //dr["CarExtId"] = 36249;
                //dr["iType"] = 187;
                //dr["Interval"] = 30000;
                //dr["OemComponentId"] = 3074;
                //dr["IntervalUrl"] = "/nonstandards/-30001";
                //dt.Rows.Add(dr);


                //dr = dt.NewRow();
                //dr["CarExtId"] = 54467;
                //dr["iType"] = 185;
                //dr["Interval"] = 135000;
                //dr["OemComponentId"] = 3074;
                //dr["IntervalUrl"] = "/nonstandards/-135000";
                //dt.Rows.Add(dr);


                //DataTable serviceDt = GetService(requestInfo, ref clientSession, ref dt);

                //dt.Dispose();
                //dt = null;

                ////show results for test
                //string carExtId;
                //string interval;
                //string oemComponent;
                //string oemComponentId;
                //string opAction;
                //string opDescription;
                //string iType;


                //foreach (DataRow dr1 in serviceDt.Rows)
                //{
                //    carExtId = dr1["CarExtId"].ToString();
                //    iType = dr1["iType"].ToString();
                //    interval = dr1["Interval"].ToString();
                //    oemComponentId = dr1["OemComponentId"].ToString();
                //    oemComponent = dr1["OemComponent"].ToString();
                //    opAction = dr1["OpAction"].ToString();
                //    opDescription = dr1["OpDescription"].ToString();
                //}

                ////-------------------------------------------------------------------
                ////Labor test
                //DataTable dt = new DataTable();

                //dt.Columns.Add("CarExtId", typeof(Int32));
                //dt.Columns.Add("OemComponentId", typeof(Int32));

                ////DataRow dr = dt.NewRow();
                ////dr["CarExtId"] = 36840;
                ////dr["OemComponentId"] = 1039;
                ////dt.Rows.Add(dr);

                ////dr = dt.NewRow();
                ////dr["CarExtId"] = 36221;
                ////dr["OemComponentId"] = 9;
                ////dt.Rows.Add(dr);

                //DataRow dr = dt.NewRow();
                //dr["CarExtId"] = 36221;
                //dr["OemComponentId"] = 9;
                //dt.Rows.Add(dr);

                ////dr = dt.NewRow();
                ////dr["CarExtId"] = 37255;
                ////dr["OemComponentId"] = 21;
                ////dt.Rows.Add(dr);

                ////dr = dt.NewRow();
                ////dr["CarExtId"] = 38323;
                ////dr["OemComponentId"] = 21;
                ////dt.Rows.Add(dr);

                ////dr = dt.NewRow();
                ////dr["CarExtId"] = 45008;
                ////dr["OemComponentId"] = 21;
                ////dt.Rows.Add(dr);

                //dr = dt.NewRow();
                //dr["CarExtId"] = 36186;
                //dr["OemComponentId"] = 27;
                //dt.Rows.Add(dr);

                //dr = dt.NewRow();
                //dr["CarExtId"] = 39235;
                //dr["OemComponentId"] = 64;
                //dt.Rows.Add(dr);

                //dr = dt.NewRow();
                //dr["CarExtId"] = 38212;
                //dr["OemComponentId"] = 154;
                //dt.Rows.Add(dr);

                //dr = dt.NewRow();
                //dr["CarExtId"] = 42760;
                //dr["OemComponentId"] = 484;
                //dt.Rows.Add(dr);

                //dr = dt.NewRow();
                //dr["CarExtId"] = 38862;
                //dr["OemComponentId"] = 800;
                //dt.Rows.Add(dr);


                //dr = dt.NewRow();
                //dr["CarExtId"] = 38862;
                //dr["OemComponentId"] = 807;
                //dt.Rows.Add(dr);


                //DataTable serviceDt = GetLabor(requestInfo, ref clientSession, ref dt);

                //dt.Dispose();
                //dt = null;

                ////show results for test
                //string carExtId;
                //string laborSkillLevel;
                //string mfgWarranty;
                //string oemComponentId;
                //string laborHour;
                //string exceptionMessage;



                //foreach (DataRow dr1 in serviceDt.Rows)
                //{
                //    carExtId = dr1["CarExtId"].ToString();
                //    oemComponentId = dr1["OemComponentId"].ToString();
                //    laborSkillLevel = dr1["LaborSkillLevel"].ToString();
                //    mfgWarranty = dr1["MfgWarranty"].ToString();
                //    laborHour = dr1["LaborHour"].ToString();
                //    exceptionMessage = dr1["ExceptionMessage"].ToString();
                //}


                ////-------------------------------------------------------------------
                ////Parts test
                //DataTable dt = new DataTable();

                //dt.Columns.Add("CarExtId", typeof(Int32));
                //dt.Columns.Add("OemComponentId", typeof(Int32));

                //DataRow dr = dt.NewRow();
                //dr["CarExtId"] = 36311;
                //dr["OemComponentId"] = 9;
                //dt.Rows.Add(dr);

                //dr = dt.NewRow();
                //dr["CarExtId"] = 42578;
                //dr["OemComponentId"] = 19;
                //dt.Rows.Add(dr);

                //dr = dt.NewRow();
                //dr["CarExtId"] = 39235;
                //dr["OemComponentId"] = 64;
                //dt.Rows.Add(dr);

                //dr = dt.NewRow();
                //dr["CarExtId"] = 38212;
                //dr["OemComponentId"] = 154;
                //dt.Rows.Add(dr);

                //dr = dt.NewRow();
                //dr["CarExtId"] = 42760;
                //dr["OemComponentId"] = 484;
                //dt.Rows.Add(dr);

                //dr = dt.NewRow();
                //dr["CarExtId"] = 38862;
                //dr["OemComponentId"] = 800;
                //dt.Rows.Add(dr);


                //dr = dt.NewRow();
                //dr["CarExtId"] = 38862;
                //dr["OemComponentId"] = 807;
                //dt.Rows.Add(dr);




                //DataTable serviceDt = GetParts(requestInfo, ref clientSession, ref dt);

                //dt.Dispose();
                //dt = null;

                ////show results for test
                //string carExtId;
                //string oemComponentId;
                //string partNumber;
                //string partName;
                //string quantity;
                //string unitPrice;
                //string exceptionMessage;



                //foreach (DataRow dr1 in serviceDt.Rows)
                //{
                //    carExtId = dr1["CarExtId"].ToString();
                //    oemComponentId = dr1["OemComponentId"].ToString();
                //    partNumber = dr1["OemPartNo"].ToString();
                //    partName = dr1["OemPartName"].ToString();
                //    quantity = dr1["Quantity"].ToString();
                //    unitPrice = dr1["UnitPrice"].ToString();
                //    exceptionMessage = dr1["ExceptionMessage"].ToString();
                //}


                //-------------------------------------------------------------------
                //Fluid test
                DataTable dt = new DataTable();

                dt.Columns.Add("CarExtId", typeof(Int32));
                dt.Columns.Add("OemComponentId", typeof(Int32));

                DataRow dr = dt.NewRow();
                dr["CarExtId"] = 50823;
                dr["OemComponentId"] = 807;
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["CarExtId"] = 38064;
                dr["OemComponentId"] = 2056;
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["CarExtId"] = 39235;
                dr["OemComponentId"] = 64;
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["CarExtId"] = 38212;
                dr["OemComponentId"] = 154;
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["CarExtId"] = 42760;
                dr["OemComponentId"] = 484;
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["CarExtId"] = 38862;
                dr["OemComponentId"] = 800;
                dt.Rows.Add(dr);


                dr = dt.NewRow();
                dr["CarExtId"] = 38862;
                dr["OemComponentId"] = 807;
                dt.Rows.Add(dr);




                DataTable serviceDt = GetFluid(requestInfo, ref clientSession, ref dt);

                dt.Dispose();
                dt = null;

                //show results for test
                string carExtId;
                string oemComponentId;
                string partNumber;
                string partName;
                string quantity;
                string unitPrice;
                string exceptionMessage;



                foreach (DataRow dr1 in serviceDt.Rows)
                {
                    carExtId = dr1["CarExtId"].ToString();
                    oemComponentId = dr1["OemComponentId"].ToString();
                    partNumber = dr1["OemPartNo"].ToString();
                    partName = dr1["OemPartName"].ToString();
                    quantity = dr1["Quantity"].ToString();
                    unitPrice = dr1["UnitPrice"].ToString();
                    exceptionMessage = dr1["ExceptionMessage"].ToString();
                }


                ////-------------------------------------------------------------------
                ////CarVin test
                //DataTable dt = new DataTable();

                //dt.Columns.Add("VIN", typeof(String));

                //DataRow dr = dt.NewRow();
                //dr["VIN"] = "19UUA56612A058320";
                //dt.Rows.Add(dr);

                //dr = dt.NewRow();
                //dr["VIN"] = "19UUA56613A035668";
                //dt.Rows.Add(dr);


                //DataTable modelDt = GetCarVin(requestInfo, ref clientSession, ref dt);

                clientSession.DownloadData(requestInfo.BaseUrl + requestInfo.LogoutPath);
            }
            catch (Exception ex)
            {
                string httpError = RegularExpression.ParseHttpErrorCode(ex.Message);
                string errorMsg = ex.Message;
            }

        }

        static DataTable GetVehicleYear(RequestInfo requestInfo, ref WebClientSession clientSession, ref DataTable parentDataTable)
        {
            ScrapeYear scrape = new ScrapeYear();



            DataTable dt = scrape.ScrapeWeb(requestInfo, ref clientSession, ref parentDataTable);

            return dt;
        }

        static DataTable GetVehicleMake(RequestInfo requestInfo, ref WebClientSession clientSession, ref DataTable parentDataTable)
        {
            ScrapeMake scrape = new ScrapeMake();

            DataTable dt = scrape.ScrapeWeb(requestInfo, ref clientSession, ref parentDataTable);

            return dt;
        }

        static DataTable GetVehicleModel(RequestInfo requestInfo, ref WebClientSession clientSession, ref DataTable parentDataTable)
        {
            ScrapeModel scrape = new ScrapeModel();

            DataTable dt = scrape.ScrapeWeb(requestInfo, ref clientSession, ref parentDataTable);

            return dt;
        }

        static DataTable GetVehicleEngine(RequestInfo requestInfo, ref WebClientSession clientSession, ref DataTable parentDataTable)
        {
            ScrapeEngine scrape = new ScrapeEngine();

            DataTable dt = scrape.ScrapeWeb(requestInfo, ref clientSession, ref parentDataTable);

            return dt;
        }

        static DataTable GetVehicleCar(RequestInfo requestInfo, ref WebClientSession clientSession, ref DataTable parentDataTable)
        {
            ScrapeCar scrape = new ScrapeCar();

            DataTable dt = scrape.ScrapeWeb(requestInfo, ref clientSession, ref parentDataTable);

            return dt;
        }

        static DataTable GetMenu(RequestInfo requestInfo, ref WebClientSession clientSession, ref DataTable parentDataTable)
        {
            ScrapeMenu scrape = new ScrapeMenu();

            DataTable dt = scrape.ScrapeWeb(requestInfo, ref clientSession, ref parentDataTable);

            return dt;
        }

        static DataTable GetService(RequestInfo requestInfo, ref WebClientSession clientSession, ref DataTable parentDataTable)
        {
            ScrapeService scrape = new ScrapeService();

            DataTable dt = scrape.ScrapeWeb(requestInfo, ref clientSession, ref parentDataTable);

            return dt;
        }

        static DataTable GetLabor(RequestInfo requestInfo, ref WebClientSession clientSession, ref DataTable parentDataTable)
        {
            ScrapeLabor scrape = new ScrapeLabor();

            DataTable dt = scrape.ScrapeWeb(requestInfo, ref clientSession, ref parentDataTable);

            return dt;
        }

        static DataTable GetParts(RequestInfo requestInfo, ref WebClientSession clientSession, ref DataTable parentDataTable)
        {
            ScrapeParts scrape = new ScrapeParts();

            DataTable dt = scrape.ScrapeWeb(requestInfo, ref clientSession, ref parentDataTable);

            return dt;
        }

        static DataTable GetFluid(RequestInfo requestInfo, ref WebClientSession clientSession, ref DataTable parentDataTable)
        {
            ScrapeFluid scrape = new ScrapeFluid();

            DataTable dt = scrape.ScrapeWeb(requestInfo, ref clientSession, ref parentDataTable);

            return dt;
        }

        static DataTable GetCarVin(RequestInfo requestInfo, ref WebClientSession clientSession, ref DataTable parentDataTable)
        {
            ScrapeCarVin scrape = new ScrapeCarVin();

            DataTable dt = scrape.ScrapeWeb(requestInfo, ref clientSession, ref parentDataTable);

            return dt;
        }
    }
}
